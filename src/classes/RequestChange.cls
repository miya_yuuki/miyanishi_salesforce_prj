global with sharing class RequestChange extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public CustomObject1__c record {get{return (CustomObject1__c)mainRecord;}}
	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	{
	setApiVersion(42.0);
	}
	public RequestChange(ApexPages.StandardController controller) {
		super(controller);
		appComponentProperty = new Map<String, Map<String, Object>>();
		appComponentProperty.put('Component468',new Map<String,Object>{'id'=>'','top'=>'','left'=>'','width'=>'','height'=>'','SIncludeOrder'=>'System.Object','noneTextOn'=>'true','noneText'=>'--なし--','editableOn'=>'true','targetField1'=>'Component355','makeSelOp1'=>'','targetField2'=>'','makeSelOp2'=>'','targetField3'=>'','makeSelOp3'=>'','targetField4'=>'','makeSelOp4'=>'','targetField5'=>'','makeSelOp5'=>'','targetField6'=>'','makeSelOp6'=>'','targetField7'=>'','makeSelOp7'=>'','targetField8'=>'','makeSelOp8'=>'','targetField9'=>'','makeSelOp9'=>'','targetField10'=>'','makeSelOp10'=>'','Component__Width'=>'50','Component__Height'=>'50','Component__id'=>'Component468','Component__Name'=>'ChangeSelectList','Component__NameSpace'=>'appcom','Component__Top'=>'0','Component__Left'=>'0','settings'=>'{"targetField1":"Component355","makeSelOp1":"","targetField2":null,"makeSelOp2":"","targetField3":null,"makeSelOp3":"","targetField4":null,"makeSelOp4":"","targetField5":null,"makeSelOp5":"","targetField6":null,"makeSelOp6":"","targetField7":null,"makeSelOp7":"","targetField8":null,"makeSelOp8":"","targetField9":null,"makeSelOp9":"","targetField10":null,"makeSelOp10":""}'});

		SObjectField f;

		f = CustomObject1__c.fields.Name;
		f = CustomObject1__c.fields.Status__c;
		f = CustomObject1__c.fields.unit__c;
		f = CustomObject1__c.fields.RequestDay__c;
		f = CustomObject1__c.fields.OwnerId;
		f = CustomObject1__c.fields.RequestEndDay__c;
		f = CustomObject1__c.fields.SalesSupportPerson__c;
		f = CustomObject1__c.fields.SupportJudgePerson__c;
		f = CustomObject1__c.fields.ProductGroupgazou__c;
		f = CustomObject1__c.fields.SupportDay__c;
		f = CustomObject1__c.fields.OrderNo__c;
		f = CustomObject1__c.fields.SupportMember__c;
		f = CustomObject1__c.fields.OrderNo2__c;
		f = CustomObject1__c.fields.RDDay__c;
		f = CustomObject1__c.fields.Account__c;
		f = CustomObject1__c.fields.RecordTypeId;
		f = CustomObject1__c.fields.Sign__c;
		f = CustomObject1__c.fields.OrderProvisional1__c;
		f = CustomObject1__c.fields.OrderProvisionalMemo1__c;
		f = CustomObject1__c.fields.SalvageScheduledDate__c;
		f = CustomObject1__c.fields.ProductGroup__c;
		f = CustomObject1__c.fields.sentaku__c;
		f = CustomObject1__c.fields.MemoPrint__c;
		f = CustomObject1__c.fields.ChangeType__c;
		f = CustomObject1__c.fields.BillStopType1__c;
		f = CustomObject1__c.fields.BillStopType__c;
		f = CustomObject1__c.fields.henkoumae__c;
		f = CustomObject1__c.fields.henkougo__c;
		f = CustomObject1__c.fields.etcChangeDay__c;
		f = CustomObject1__c.fields.RequestMemo__c;
		f = CustomObject1__c.fields.Patnername1__c;
		f = CustomObject1__c.fields.Patnername2__c;
		f = CustomObject1__c.fields.SalesPerson__c;
		f = CustomObject1__c.fields.UnitSalesPerson2__c;
		f = CustomObject1__c.fields.SalesPerson1__c;
		f = CustomObject1__c.fields.SalesPerson2__c;
		f = CustomObject1__c.fields.SalesSupportPersonView__c;
		f = CustomObject1__c.fields.SupportPerson2__c;
		f = CustomObject1__c.fields.SalesPersonDay__c;
		f = CustomObject1__c.fields.Account2__c;
		f = CustomObject1__c.fields.Customer__c;
		f = CustomObject1__c.fields.Billtantou__c;
		f = CustomObject1__c.fields.BillDepartment__c;
		f = CustomObject1__c.fields.BillDepartment2__c;
		f = CustomObject1__c.fields.BillTitle__c;
		f = CustomObject1__c.fields.BillTitle2__c;
		f = CustomObject1__c.fields.BillAccountNameEdit1__c;
		f = CustomObject1__c.fields.BillAccountNameEdit__c;
		f = CustomObject1__c.fields.CustomerChangeDay__c;
		f = Order__c.fields.AccountName__c;
		f = Order__c.fields.Order_PaymentSiteMaster2__c;
		f = Order__c.fields.unit__c;
		f = Order__c.fields.Contract_SalesPerson1__c;
		f = Contact.fields.Contact_BillCompany__c;
		f = Contact.fields.Department;
		f = Contact.fields.Title;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = CustomObject1__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'RequestChange';
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(CustomObject1__c.SObjectType);
			mainQuery = new SkyEditor2.Query('CustomObject1__c');
			mainQuery.addField('Status__c');
			mainQuery.addField('OwnerId');
			mainQuery.addField('SalesSupportPerson__c');
			mainQuery.addField('OrderNo__c');
			mainQuery.addField('OrderNo2__c');
			mainQuery.addField('Account__c');
			mainQuery.addField('Sign__c');
			mainQuery.addField('OrderProvisional1__c');
			mainQuery.addField('OrderProvisionalMemo1__c');
			mainQuery.addField('SalvageScheduledDate__c');
			mainQuery.addField('ProductGroup__c');
			mainQuery.addField('sentaku__c');
			mainQuery.addField('MemoPrint__c');
			mainQuery.addField('ChangeType__c');
			mainQuery.addField('BillStopType1__c');
			mainQuery.addField('BillStopType__c');
			mainQuery.addField('henkoumae__c');
			mainQuery.addField('henkougo__c');
			mainQuery.addField('etcChangeDay__c');
			mainQuery.addField('RequestMemo__c');
			mainQuery.addField('Patnername2__c');
			mainQuery.addField('SalesPerson2__c');
			mainQuery.addField('SupportPerson2__c');
			mainQuery.addField('SalesPersonDay__c');
			mainQuery.addField('Customer__c');
			mainQuery.addField('Billtantou__c');
			mainQuery.addField('CustomerChangeDay__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('unit__c');
			mainQuery.addFieldAsOutput('RequestDay__c');
			mainQuery.addFieldAsOutput('RequestEndDay__c');
			mainQuery.addFieldAsOutput('SupportJudgePerson__c');
			mainQuery.addFieldAsOutput('ProductGroupgazou__c');
			mainQuery.addFieldAsOutput('SupportDay__c');
			mainQuery.addFieldAsOutput('SupportMember__c');
			mainQuery.addFieldAsOutput('RDDay__c');
			mainQuery.addFieldAsOutput('RecordType.Name');
			mainQuery.addFieldAsOutput('Patnername1__c');
			mainQuery.addFieldAsOutput('SalesPerson__c');
			mainQuery.addFieldAsOutput('UnitSalesPerson2__c');
			mainQuery.addFieldAsOutput('SalesPerson1__c');
			mainQuery.addFieldAsOutput('SalesSupportPersonView__c');
			mainQuery.addFieldAsOutput('Account__c');
			mainQuery.addFieldAsOutput('Account2__c');
			mainQuery.addFieldAsOutput('BillDepartment__c');
			mainQuery.addFieldAsOutput('BillDepartment2__c');
			mainQuery.addFieldAsOutput('BillTitle__c');
			mainQuery.addFieldAsOutput('BillTitle2__c');
			mainQuery.addFieldAsOutput('BillAccountNameEdit1__c');
			mainQuery.addFieldAsOutput('BillAccountNameEdit__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			if (record.Id == null) {
				saveOldValues();
				if(record.RecordTypeId == null) recordTypeSelector.applyDefault(record);
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}


	public void loadReferenceValues_Component412() {
		if (record.OrderNo__c == null) {
if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.Account__c)) {
				record.Account__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.Patnername1__c)) {
				record.Patnername1__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.SalesPerson__c)) {
				record.SalesPerson__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.SalesPerson1__c)) {
				record.SalesPerson1__c = null;
			}
				return;
		}
		Order__c[] referenceTo = [SELECT AccountName__c,Order_PaymentSiteMaster2__c,unit__c,Contract_SalesPerson1__c FROM Order__c WHERE Id=:record.OrderNo__c];
		if (referenceTo.size() == 0) {
			record.OrderNo__c.addError(SkyEditor2.Messages.referenceDataNotFound(record.OrderNo__c));
			return;
		}
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.Account__c)) {
			record.Account__c = referenceTo[0].AccountName__c;
		}
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.Patnername1__c)) {
			record.Patnername1__c = referenceTo[0].Order_PaymentSiteMaster2__c;
		}
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.SalesPerson__c)) {
			record.SalesPerson__c = referenceTo[0].unit__c;
		}
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.SalesPerson1__c)) {
			record.SalesPerson1__c = referenceTo[0].Contract_SalesPerson1__c;
		}
		
	}

	public void loadReferenceValues_Component162() {
		if (record.Customer__c == null) {
if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.Account__c)) {
				record.Account__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.BillDepartment__c)) {
				record.BillDepartment__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.BillTitle__c)) {
				record.BillTitle__c = null;
			}
				return;
		}
		Contact[] referenceTo = [SELECT Contact_BillCompany__c,Department,Title FROM Contact WHERE Id=:record.Customer__c];
		if (referenceTo.size() == 0) {
			record.Customer__c.addError(SkyEditor2.Messages.referenceDataNotFound(record.Customer__c));
			return;
		}
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.Account__c)) {
			record.Account__c = referenceTo[0].Contact_BillCompany__c;
		}
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.BillDepartment__c)) {
			record.BillDepartment__c = referenceTo[0].Department;
		}
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.BillTitle__c)) {
			record.BillTitle__c = referenceTo[0].Title;
		}
		
	}

	public void loadReferenceValues_Component94() {
		if (record.Billtantou__c == null) {
if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.Account2__c)) {
				record.Account2__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.BillDepartment2__c)) {
				record.BillDepartment2__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.BillTitle2__c)) {
				record.BillTitle2__c = null;
			}
				return;
		}
		Contact[] referenceTo = [SELECT Contact_BillCompany__c,Department,Title FROM Contact WHERE Id=:record.Billtantou__c];
		if (referenceTo.size() == 0) {
			record.Billtantou__c.addError(SkyEditor2.Messages.referenceDataNotFound(record.Billtantou__c));
			return;
		}
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.Account2__c)) {
			record.Account2__c = referenceTo[0].Contact_BillCompany__c;
		}
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.BillDepartment2__c)) {
			record.BillDepartment2__c = referenceTo[0].Department;
		}
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.BillTitle2__c)) {
			record.BillTitle2__c = referenceTo[0].Title;
		}
		
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}