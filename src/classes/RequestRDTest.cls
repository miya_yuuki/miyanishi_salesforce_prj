@isTest
private with sharing class RequestRDTest{
		private static testMethod void testPageMethods() {	
			RequestRD page = new RequestRD(new ApexPages.StandardController(new CustomObject1__c()));	
			page.getOperatorOptions_CustomObject1_c_OrderNo_c();	
			page.getOperatorOptions_CustomObject1_c_OwnerId();	
			page.getOperatorOptions_CustomObject1_c_RecordTypeId();	
			page.getOperatorOptions_CustomObject1_c_AccountName_c();	
			page.getOperatorOptions_CustomObject1_c_StatusRD_c_multi();	
			page.getOperatorOptions_CustomObject1_c_Name();	
			page.getOperatorOptions_CustomObject1_c_Status_c_multi();	
			page.getOperatorOptions_CustomObject1_c_ProductGroup_c_multi();	
			System.assert(true);
		}	
			
	private static testMethod void testComponent3() {
		RequestRD.Component3 Component3 = new RequestRD.Component3(new List<CustomObject1__c>(), new List<RequestRD.Component3Item>(), new List<CustomObject1__c>(), new SkyEditor2.RecordTypeSelector(CustomObject1__c.SObjectType));
		Component3.create(new CustomObject1__c());
		Component3.doDeleteSelectedItems();
		System.assert(true);
	}
	
}