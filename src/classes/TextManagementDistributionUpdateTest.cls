@isTest
private class TextManagementDistributionUpdateTest {
	static testMethod void executeTest() {
		// Start Create TestData
		test.startTest();   
		
		// get recordType
		RecordType rt = [select id From RecordType Where SobjectType = 'TextManagement2__c' LIMIT 1];
		
		// Create Account
		Account account = New Account();
		account.Name = 'testAccount';
		insert account;
		
		// Create Opportunity
		Opportunity opportunity = New Opportunity();
		opportunity.Name = 'testOpportunity';
		opportunity.CloseDate = Date.Today();
		opportunity.AccountID = account.id;
		opportunity.StageName = 'Closed';
		insert opportunity;
		
		// Create Contract
		Contract contract = New Contract();
		contract.AccountID = account.id;
		contract.opportunity__c = opportunity.id;
		insert contract;
		
		// Create Order__c
		Order__c order = New Order__c();
		order.Order_Relation__c = contract.id;
		insert order;
		
		// Create OrderItem__c
		OrderItem__c orderItem = New OrderItem__c();
		orderItem.Item2_Relation__c = order.id;
		orderItem.BillingTiming__c = '受注';
		orderItem.ContractMonths__c = 6;
		orderItem.ChangeDay__c = Date.Today();
		insert orderItem;
		
		// Create TextManagement2__c
		TextManagement2__c tm = New TextManagement2__c();
		tm.recordTypeID = rt.id;
		tm.Item2_OrderProduct__c = orderItem.id;
		insert tm;
		
		// 案分担当②変更
		tm.SalesPersonSub__c = [SELECT Id FROM User WHERE Id != :Userinfo.getUserId() LIMIT 1].Id;
		tm.SalesPercentage1__c = 50;
		tm.SalesPercentage2__c = 50;
		update tm;
	}
}