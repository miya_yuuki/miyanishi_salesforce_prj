@isTest
private with sharing class RequestKeywordURLTest{
	private static testMethod void testPageMethods() {		RequestKeywordURL extension = new RequestKeywordURL(new ApexPages.StandardController(new CustomObject1__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testComponent294() {
		RequestKeywordURL.Component294 Component294 = new RequestKeywordURL.Component294(new List<SEOChange__c>(), new List<RequestKeywordURL.Component294Item>(), new List<SEOChange__c>(), null);
		Component294.create(new SEOChange__c());
		System.assert(true);
	}
	
	public static testMethod void test_importByJSON_Component294() {
		RequestKeywordURL.Component294 table = new RequestKeywordURL.Component294(
			new List<SEOChange__c>(),
			new List<RequestKeywordURL.Component294Item>(),
			new List<SEOChange__c>(), null);
		table.hiddenValue = '[{}]';
		table.importByJSON();
		System.assert(true);
	}
	@isTest
	private static void testLightDataTables(){

		System.assert(true);
	}

	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component201() {
		String testReferenceId = '';
		RequestKeywordURL extension = new RequestKeywordURL(new ApexPages.StandardController(new CustomObject1__c()));
		extension.loadReferenceValues_Component201();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(Order__c.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<Order__c> parents = [SELECT Id FROM Order__c LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		Order__c parent = [SELECT Id,AccountName__c FROM Order__c WHERE Id = :testReferenceId];
		extension.record.OrderNo__c = parent.Id;
		extension.loadReferenceValues_Component201();
				
		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.Account__c)) {
			System.assertEquals(parent.AccountName__c, extension.record.Account__c);
		}

		System.assert(true);
	}

	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component351() {
		String testReferenceId = '';
		RequestKeywordURL.Component294 table = new RequestKeywordURL.Component294(new List<SEOChange__c>(), new List<RequestKeywordURL.Component294Item>(), new List<SEOChange__c>(), null);
		table.add();
		RequestKeywordURL.Component294Item item = table.items[0];
		item.loadReferenceValues_Component351();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(OrderItem__c.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<OrderItem__c> parents = [SELECT Id FROM OrderItem__c LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		OrderItem__c parent = [SELECT Id,OrderItem_keywordType__c,Item2_Keyword_strategy_keyword__c,OrderItem_URL__c,Item2_Estimate_level__c,TotalPrice__c FROM OrderItem__c WHERE Id = :testReferenceId];
		item.record.OrderItem__c = parent.Id;
		item.loadReferenceValues_Component351();
				
		if (SkyEditor2.Util.isEditable(item.record, SEOChange__c.fields.KeywordType__c)) {
			System.assertEquals(parent.OrderItem_keywordType__c, item.record.KeywordType__c);
		}


		if (SkyEditor2.Util.isEditable(item.record, SEOChange__c.fields.Keyword__c)) {
			System.assertEquals(parent.Item2_Keyword_strategy_keyword__c, item.record.Keyword__c);
		}


		if (SkyEditor2.Util.isEditable(item.record, SEOChange__c.fields.URL__c)) {
			System.assertEquals(parent.OrderItem_URL__c, item.record.URL__c);
		}


		if (SkyEditor2.Util.isEditable(item.record, SEOChange__c.fields.level__c)) {
			System.assertEquals(parent.Item2_Estimate_level__c, item.record.level__c);
		}


		if (SkyEditor2.Util.isEditable(item.record, SEOChange__c.fields.Amount__c)) {
			System.assertEquals(parent.TotalPrice__c, item.record.Amount__c);
		}

		System.assert(true);
	}
}