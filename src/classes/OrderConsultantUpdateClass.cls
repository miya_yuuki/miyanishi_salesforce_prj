public with sharing class OrderConsultantUpdateClass {
    public static void prcOrderConsultantUpdateClass(List<Order__c> objOrder) {
        List<Order__c> targetOrders = new List<Order__c>();
        // 更新対象のチェック
        for (Order__c newObj : objOrder) {
            // oldとnewの区別がつかないため、担当営業がnullでないものはすべて対象に含める
            // 参考：https://web.plus-idea.net/2017/01/salesforce-trigger-old-new/
            if (newObj.SalesPerson10__c != null) targetOrders.add(newObj);
        }

        List<String> accountIds = new List<String>();
        List<String> accountWebSiteIds = new List<String>();
        Map<String, ID> accountOwner = new Map<String, ID>();
        Map<String, ID> accountWebSiteOwner = new Map<String, ID>();
        if (targetOrders.size() < 1) {
            return;
        }
        for (Order__c targetObj : targetOrders) {
            if (targetObj.ID__c != null) {
                accountIds.add(targetObj.ID__c);
                accountOwner.put(targetObj.ID__c, targetObj.SalesPerson10__c);
            }
            if (targetObj.AccountWebSiteId__c != null) {
                accountWebSiteIds.add(targetObj.AccountWebSiteId__c);
                accountWebSiteOwner.put(targetObj.AccountWebSiteId__c, targetObj.SalesPerson10__c);
            }
        }

        // 取引先の所有者を更新
        if (accountIds.size() > 0) {
            List<Account> updAccountObj = new List<Account>() ;
            List<Account> accounts = [ SELECT Id, OwnerId FROM Account WHERE Id IN :accountIds ];
            for (Account accountObj : accounts) {
                ID ownerId = accountOwner.get(accountObj.Id);
                if (accountObj.OwnerId != ownerId) {
                    accountObj.OwnerId = ownerId;
                    updAccountObj.add(accountObj);
                }
            }
            if (updAccountObj.size() > 0) update updAccountObj;
        }
        // 取引先Webサイトのコンサルタントを更新
        if (accountWebSiteIds.size() > 0) {
            List<AccountWebSite__c> updAccountWebSiteObj = new List<AccountWebSite__c>() ;
            List<AccountWebSite__c> webSites = [ SELECT Id, Consultant__c FROM AccountWebSite__c WHERE Id IN :accountWebSiteIds ];
            for (AccountWebSite__c accountWebSiteObj : webSites) {
                ID consultantId = accountWebSiteOwner.get(accountWebSiteObj.Id);
                if (accountWebSiteObj.Consultant__c != consultantId) {
                    accountWebSiteObj.Consultant__c = consultantId;
                    updAccountWebSiteObj.add(accountWebSiteObj);
                }
            }
            if (updAccountWebSiteObj.size() > 0) update updAccountWebSiteObj;
        }
    }
}