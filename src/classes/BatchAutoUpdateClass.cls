global class BatchAutoUpdateClass implements Database.Batchable<sObject> {

	// SOQL用
	public String query;
	public String dt;
	public List<String> strOrderIds;

	global BatchAutoUpdateClass(Date prmDt, Id prmOrderId)
	{
		// 注文商品オブジェクト
		List<OrderItem__c> objOrderItem = new List<OrderItem__c>();

		// パラメータの日付を保持
		dt = UtilityClass.retStringDt(prmDt);

		// 注文商品のクエリ
		query =
			'SELECT Item2_Relation__c FROM OrderItem__c ' +
			'WHERE Item2_Keyword_phase__c IN (\'契約中\', \'請求停止\') ' +
			'AND Quantity__c >= 0 ' +
			'AND EndDate__c = ' + dt + ' ' +
			'AND AutomaticUpdate__c = \'有\'';

		// 注文直指定
		if (prmOrderId != null) {
			query += 'AND Item2_Relation__c = \'' + prmOrderId + '\' ';
		}

		// 注文データを取得
		objOrderItem = Database.query(query);

		// インスタンスを生成
		strOrderIds = new List<String>();

		// 注文商品のループ
		for(OrderItem__c obj : objOrderItem) strOrderIds.add(obj.Item2_Relation__c);

		// 注文の全項目を取得
		String orderField = 'Id, ' + UtilityClass.getCustomFieldsNames(Order__c.SObjecttype);
		// クエリを生成
		query = 'SELECT ' + orderField + ' FROM Order__c WHERE MaturityClose__c = false AND Id IN :strOrderIds';
	}

    //バッチ開始処理
    //開始するためにqueryを実行する。この実行されたSOQLのデータ分処理する。
    //5千万件以上のレコードになるとエラーになる。
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }

    //バッチ処理内容
    //scopeにgetQueryLocatorの内容がバッチサイズ分格納されてくる
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
		// 作成用：注文商品オブジェクトを作成
		List<OrderItem__c> insOrderItem = new List<OrderItem__c>();
		// 更新用：注文商品オブジェクトを作成
		List<OrderItem__c> updOrderItem = new List<OrderItem__c>();
		// 注文IDの配列
		List<String> strIds = new List<String>();
		// 注文商品オブジェクト
		List<OrderItem__c> objOrderItem = new List<OrderItem__c>();
		// Map
		Map<String, List<OrderItem__c>> objMap = new Map<String, List<OrderItem__c>> ();

		// 注文商品のループ
		for(sObject obj : scope) {
			// キャスト変換
			Order__c od = (Order__c)obj;
			// 配列にIDを追加
			strIds.add(od.Id);
		}

		// 注文商品の全項目を取得
		String itemField = 'Id, Name, RecordTypeId,  SalesPerson1__r.Name, SalesPersonSub__r.Name, Item2_Product__r.Name,' + UtilityClass.getCustomFieldsNames(OrderItem__c.SObjecttype);

		// SOQLを発行
		query =
			'SELECT ' + itemField + ' FROM OrderItem__c ' +
			'WHERE Item2_Keyword_phase__c IN (\'契約中\', \'請求停止\') ' +
			'AND Quantity__c >= 0 ' +
			'AND EndDate__c = ' + dt + ' ' +
			'AND AutomaticUpdate__c = \'有\' ' +
			'AND Item2_Relation__c IN :strIds';

		// 注文データを取得
		objOrderItem = Database.query(query);

		// 注文商品のループ
		for(OrderItem__c obj : objOrderItem) {
			// Mapをチェック
			if (!objMap.containskey(obj.Item2_Relation__c)) {
				// 注文商品をMapに新規追加
				List<OrderItem__c> newOd = new List<OrderItem__c>();
				newOd.add(obj);
				objMap.put(obj.Item2_Relation__c, newOd);
			} else {
				// 注文商品をMapに追加
				objMap.get(obj.Item2_Relation__c).add(obj);
			}
		}

		for(sObject obj : scope) {
			// 注文商品がなければ次へ
			if (!objMap.containskey(obj.Id)) continue;
			// キャスト変換
			Order__c od = (Order__c)obj;

			// 注文のインスタンスを生成
			Order__c newOrder = new Order__c();
			// 注文をコピー
			newOrder = (Order__c)od.clone(false, true);
			// 注文を作成
			insert newOrder;

			for (OrderItem__c itm : objMap.get(od.Id)) {
				// 契約更新完了フラグを立てる
				itm.AutoUpdateComplete__c = true;
				// フェーズを「更新済み」に変更する
				itm.Item2_Keyword_phase__c = '更新済み';
				// 「更新コピー実施日付」にシステム日時をセットする
				itm.Item2_UpdateCopyDay__c = System.now();

				// 既存の注文商品を配列に追加
				updOrderItem.add(itm);

				// 契約期間（月）とNo.が一致していない場合は
				// 既存の注文商品の update しか行わない
				if (UtilityClass.DtoI(itm.ContractMonths__c) != itm.Item2_ProductCount__c) {
					continue;
				}

				// 契約期間（月）の数だけ新しい注文商品を作成
				for (Integer i = 1; i <= UtilityClass.DtoI(itm.ContractMonths__c); i++) {
					// 注文商品のインスタンスを生成
					OrderItem__c newItem = new OrderItem__c();
					// コピーを作成
					newItem = itm.clone(false, true);
					// 注文
					newItem.Item2_Relation__c = newOrder.Id;
					// No.
					newItem.Item2_ProductCount__c = i;
					// 契約開始日
					newItem.ServiceDate__c = UtilityClass.prcAddDays(itm.EndDate__c, 1);
					// 契約終了日
					newItem.EndDate__c = UtilityClass.prcAddDays(UtilityClass.prcAddMonths(newItem.ServiceDate__c, UtilityClass.DtoI(itm.ContractMonths__c)), -1);
					// 請求対象開始日
					newItem.Item2_ServiceDate__c = UtilityClass.prcAddMonths(itm.Item2_ServiceDate__c, i);
					// 請求対象終了日
					newItem.Item2_EndData__c = UtilityClass.prcAddDays(UtilityClass.prcAddMonths(newItem.Item2_ServiceDate__c, 1), -1);
					// 請求停止開始日
					newitem.Item2_Billing_Stop_Start_Date__c = null;
					// 請求停止開始日
					newitem.Item2_Billing_Stop_End_Date__c = null;
					// キャンセル数量
					newitem.Item2_CancelCount__c = null;
					// 納品予定日
					newItem.Item2_DeliveryDate__c = UtilityClass.prcAddMonths(itm.Item2_DeliveryDate__c, i);
					// 契約更新回数
					newItem.Item_Count__c = (itm.Item_Count__c == null) ? 1 : itm.Item_Count__c + 1;
					// 旧注文商品番号
					newItem.Item2_OldNo__c = itm.Name;
					// 請求レコード作成済み
					newItem.Billchek__c = false;
					// 請求レコード作成済み①
					newItem.Billcheck1__c = false;
					// 請求レコード作成済み②
					newItem.Billcheck2__c = false;
					// 請求レコード作成済み③
					newItem.Billcheck3__c = false;
					// 請求レコード作成済み④
					newItem.Billcheck4__c = false;
					// 請求コピー実施日時
					newItem.Item2_BillCopyDay__c = null;
					// 請求対象日 手動設定
					newItem.BillSet__c = false;
					// 作成種類
					newItem.RecordCreateType__c = '(3) 更新';
					// ステータスを「契約中」に変更
					newItem.Item2_Keyword_phase__c = '契約中';
					// 契約獲得担当① ユニット
					newItem.ContractGetPersonUnit__c = String.valueOf(itm.SalesPersonUnit1__c);
					// 契約獲得担当者①
					newItem.ContractGetPerson1__c = String.valueOf(itm.SalesPerson1__r.Name);
					// 獲得担当① 案分
					newItem.ContractGetPersonPercentage1__c = itm.SalesPercentage1__c;
					// 契約獲得担当② ユニット
					newItem.ContractGetPersonUnit2__c = String.valueOf(itm.SalesPersonSubUnit__c);
					// 契約獲得担当者②
					newItem.ContractGetPerson2__c = String.valueOf(itm.SalesPersonSub__r.Name);
					// 獲得担当② 案分
					newItem.ContractGetPersonPercentage2__c = itm.SalesPercentage2__c;
					// 代理店手数料
					newItem.Item2_Commission_rate__c = itm.Item2_Commission_rate__c;
					// 外部リンク施策相当額
					newItem.linkPerformingPrice__c = itm.linkPerformingPrice__c;
					// コンサルティング売上相当額
					newItem.consultingSalesEquivalent__c = itm.consultingSalesEquivalent__c;

					// 自動更新"有"の場合
					if (itm.AnbunJudge__c) {
						// 案分担当②
						newItem.SalesPersonSub__c = null;
						// 担当① 案分比率を100%にセット
						newItem.SalesPercentage1__c = 100;
						// 担当② 案分比率をnullクリア
						newItem.SalesPercentage2__c = null;
					}

					// 発リンク開始月（ヶ月目）が入力されている商品の場合は 1 をセット
					if (itm.makeLinkStartMonth__c != null && itm.makeLinkStartMonth__c > 0) {
						newItem.makeLinkStartMonth__c = 1;
					}
					
					// Web経由のTACT商品が選択されている注文商品の内、単価とデフォルト金額が異なる場合は、
					// 新しい注文商品の単価にデフォルト金額の値をセットする
					if (itm.Item2_Product__r.Name.indexOf('【WEB経由】TACT SEO') != -1 && itm.Item2_Unit_selling_price__c != itm.tact_default_amounts__c) {
						newItem.Item2_Unit_selling_price__c = itm.tact_default_amounts__c;
					}

					// 案件種別が"セールスP（クライアント請求/初回期間支払）"
					// または"セールスP（パートナー請求/初回期間支払）"の場合は、代理店手数料に0をセット
					if (itm.Order_Type__c == 'セールスP（クライアント請求/初回期間支払）'
					    || itm.Order_Type__c == 'セールスP（パートナー請求/初回期間支払）') {
						newItem.Item2_Commission_rate__c = 0;
					}
					// 注文商品の配列に追加
					insOrderItem.add(newItem);
				}
			}
		}
		// 注文商品を作成
		if (insOrderItem.size() > 0) insert insOrderItem;
		// 注文商品を更新
		if (updOrderItem.size() > 0) update updOrderItem;
	}

	global void finish(Database.BatchableContext BC){}
}