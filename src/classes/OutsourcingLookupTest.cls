@isTest
private with sharing class OutsourcingLookupTest{
		private static testMethod void testPageMethods() {	
			OutsourcingLookup page = new OutsourcingLookup(new ApexPages.StandardController(new teamspirit__AtkApply__c()));	
			page.getOperatorOptions_teamspirit_AtkApply_c_OrderProduct_c();	
			page.getOperatorOptions_teamspirit_AtkApply_c_OwnerId();	
			page.getOperatorOptions_teamspirit_AtkApply_c_shinsheino_c();	
			page.getOperatorOptions_teamspirit_AtkApply_c_Name();	
			System.assert(true);
		}	
			
	private static testMethod void testComponent3() {
		OutsourcingLookup.Component3 Component3 = new OutsourcingLookup.Component3(new List<teamspirit__AtkApply__c>(), new List<OutsourcingLookup.Component3Item>(), new List<teamspirit__AtkApply__c>(), null);
		Component3.create(new teamspirit__AtkApply__c());
		Component3.doDeleteSelectedItems();
		System.assert(true);
	}
	
}