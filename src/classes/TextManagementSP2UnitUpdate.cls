public with sharing class TextManagementSP2UnitUpdate {
	public static boolean firstRun = true;
	
	public static void prcSalesPerson2UnitUpdate(List<TextManagement2__c> objTextManagement2, Map<Id, TextManagement2__c> oldMap, Boolean upd) {
		// 納品管理ID
		List<String> strTextManagement2Ids = new List<String>();
		// 納品管理オブジェクト
		List<TextManagement2__c> updTextManagement2 = new List<TextManagement2__c>();
		
		// 更新対象のチェック
		for (TextManagement2__c obj : objTextManagement2) {
			if (prcCheckSalesPersonSub(upd, obj, oldMap) == null) {
				continue;
			}
			strTextManagement2Ids.add(obj.Id);
		}
		
		updTextManagement2 = [SELECT Id, SalesPersonSub__c, SalesPersonSubUnit__c, SalesPersonSub__r.team__c FROM TextManagement2__c WHERE Id IN :strTextManagement2Ids];
		// 案分担当②ユニットを更新
		for (TextManagement2__c objNew : updTextManagement2) {
			if (objNew.SalesPersonSub__c == null) {
				objNew.SalesPersonSubUnit__c = null;
			}
			else {
				objNew.SalesPersonSubUnit__c = objNew.SalesPersonSub__r.team__c;
			}
		}
		if (updTextManagement2.size() > 0) {
			update updTextManagement2;
		}
	}
	
	public static void prcSalesPersonUnitLastUpdate(List<TextManagement2__c> objTextManagement2, Map<Id, TextManagement2__c> oldMap, Boolean upd) {
		// 納品管理ID
		List<String> strTextManagement2Ids = new List<String>();
		// 納品管理オブジェクト
		List<TextManagement2__c> updTextManagement2 = new List<TextManagement2__c>();
		
		// 更新対象のチェック
		for (TextManagement2__c obj : objTextManagement2) {
			if (prcCheckCloudStatus(upd, obj, oldMap) == null) {
				continue;
			}
			strTextManagement2Ids.add(obj.Id);
		}
		
		updTextManagement2 = [SELECT Id, SalesPersonUnit1__c, SalesPerson10team__c FROM TextManagement2__c WHERE Id IN :strTextManagement2Ids];
		// 担当営業ユニットを更新
		for (TextManagement2__c objNew : updTextManagement2) {
			objNew.SalesPersonUnit1__c = String.valueOf(objNew.SalesPerson10team__c);
		}
		if (updTextManagement2.size() > 0) {
			update updTextManagement2;
		}
	}

	private static String prcCheckSalesPersonSub (Boolean upd, TextManagement2__c objNew, Map<Id, TextManagement2__c> oldMap) {
		if (upd) {
			// 古い値を取得
			TextManagement2__c objOld = oldMap.get(objNew.Id);
			// 案分担当②が更新された場合
			if (objNew.SalesPersonSub__c != objOld.SalesPersonSub__c) {
				System.debug('案分担当②更新検知' + objNew.Id);
				String SalesPersonSub = objNew.SalesPersonSub__c;
				return SalesPersonSub;
			}
		}
		// 条件に合致しない
		return null;
	}
	
	private static Boolean prcCheckCloudStatus (Boolean upd, TextManagement2__c objNew, Map<Id, TextManagement2__c> oldMap) {
		if (upd) {
			// 古い値を取得
			TextManagement2__c objOld = oldMap.get(objNew.Id);
			// 納品管理ステータスが社外納品済みに変更された場合
			if (objNew.CloudStatus__c == '社外納品済み' && objOld.CloudStatus__c != '社外納品済み') {
				System.debug('納品管理ステータス→社外納品済み検知' + objNew.Id);
				return true;
			}
		}
		// 条件に合致しない
		return null;
	}
}