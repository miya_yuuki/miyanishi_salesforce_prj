public with sharing class TextManagementRecordCreatorCheck {
	public static boolean firstRun = true;
	
	public static void prcTextManagementRecordCreatorCheck (List<TextManagement2__c> objTextManagement2) {
		// 注文商品ID
		List<String> strOrderItemIds = new List<String>();
		// 納品管理オブジェクト
		List<TextManagement2__c> updTextManagement2 = new List<TextManagement2__c>();
		
		for (TextManagement2__c obj : objTextManagement2) {
			System.debug('######CreatedById: ' + obj.CreatedById);
			if (prcCheckRecordCreator(obj) == null) {
				continue;
			}
			strOrderItemIds.add(obj.Item2_OrderProduct__c);
		}
		
		updTextManagement2 = [SELECT Id, ContentsS_TextCount__c FROM TextManagement2__c WHERE Item2_OrderProduct__c IN :strOrderItemIds];
		// 納品見込数にnullを代入
		for (TextManagement2__c objNew: updTextManagement2) {
			objNew.ContentsS_TextCount__c = null;
		}
		if (updTextManagement2.size() > 0) {
			System.debug('クラウド作成レコード→納品見込数リセット ' + updTextManagement2.size() + '件');
			update updTextManagement2;
		}
	}
	private static Boolean prcCheckRecordCreator (TextManagement2__c obj) {
		// クラウドアカウントでレコード作成された → 分納レコード
		if (obj.CreatedById == '0051000000460F9') {
			System.debug('クラウドユニット作成レコード ' + obj.Id);
			return true;
		}
		// 条件に合致しない
		return null;
	}
}