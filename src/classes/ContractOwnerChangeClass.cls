public with sharing class ContractOwnerChangeClass {

	public static boolean firstRun = true;

	public static void prcOwnerChange(List<Contract> trgContract, Map<Id, Contract> oldContract)
	{
		// 契約IDの配列
		List<String> strContractIds = new List<String>();
		// 契約IDの配列：納品"有"
		List<String> strContractIdsA = new List<String>();
		// 契約IDの配列：納品"無"
		List<String> strContractIdsN = new List<String>();
		// 注文IDの配列
		List<String> strOrderIds = new List<String>();
		// 注文商品IDの配列
		List<String> strOrderItemIds = new List<String>();

		// 注文オブジェクト
		List<Order__c> objOrder = new List<Order__c>();
		// 注文商品オブジェクト
		List<OrderItem__c> objOrderItem = new List<OrderItem__c>();
		// 納品管理オブジェクト
		List<TextManagement2__c> objDeliv = new List<TextManagement2__c>();
		// 注文商品オブジェクト（Bind用）
		List<OrderItem__c> objBind = new List<OrderItem__c>();

		// 契約のMap
		Map<String, Contract> mapContract = new Map<String, Contract>();

		// 契約のループ
		for (Contract obj : trgContract) {
			// old mapが取得できなければ処理しない
			if (!oldContract.containsKey(obj.Id)) continue;
			// 所有者変更あり
			if (obj.OwnerId != oldContract.get(obj.Id).OwnerId) {
				// 契約IDをセット
				strContractIds.add(obj.Id);
				// Mapに追加
				mapContract.put(obj.Id, obj);
			}
		}

		// Bind用の注文商品を取得
		objBind = [
			SELECT Item2_Relation__c, Item2_DeliveryExistence__c
			FROM OrderItem__c
			WHERE Item2_Relation__r.Order_Relation__c IN :strContractIds
		];

		// 納品有無（コード）別に、契約IDを配列にセット
		for (OrderItem__c obj : objBind) {
			if (obj.Item2_DeliveryExistence__c == '有') strContractIdsA.add(obj.Item2_Relation__c);
			if (obj.Item2_DeliveryExistence__c == '無') strContractIdsN.add(obj.Item2_Relation__c);
		}

		System.debug('###########1:' + strContractIdsA);
		System.debug('###########2:' + strContractIdsN);

		// 注文を取得
		objOrder = [
			SELECT Id, Order_Relation__c, SalesPerson10__c
			FROM Order__c
			WHERE
			(Id IN :strContractIdsA AND Order_Phase__c IN ('契約中', '契約期間なし', '契約終了'))
			OR
			(Id IN :strContractIdsN AND Order_Phase__c IN ('契約中', '契約期間なし'))
		];

		System.debug('###########3:' + objOrder);

		// 注文のループ
		for (Order__c obj : objOrder) {
			// 注文IDをセット
			strOrderIds.add(obj.Id);
			// 契約の担当営業を継承
			obj.SalesPerson10__c = mapContract.get(obj.Order_Relation__c).OwnerId;
		}

		// 注文の更新
		if (objOrder.size() > 0) update objOrder;

		// 注文商品を取得
		objOrderItem = [
			SELECT Id, Item2_Relation__r.Order_Relation__c, SalesPerson1__c
			FROM OrderItem__c
			WHERE Item2_Relation__c IN :strOrderIds AND
			(
				(Item2_DeliveryExistence__c = '有'AND ProductionStatus__c != '社外納品済み')
				OR
				(Item2_DeliveryExistence__c = '無'AND Item2_Keyword_phase__c IN ('契約中', '請求停止', '契約期間なし', '途中解約（請求）'))
			)
		];

		// 注文商品のループ
		for (OrderItem__c obj : objOrderItem) {
			// 注文商品IDをセット
			strOrderItemIds.add(obj.Id);
			// 契約の担当営業を継承
			obj.SalesPerson1__c = mapContract.get(obj.Item2_Relation__r.Order_Relation__c).OwnerId;
		}

		// 注文商品の更新
		if (objOrderItem.size() > 0) update objOrderItem;

		// 当月
		Date dtThis = null;
		// 翌月
		Date dtAfter = null;

		// 当月
		dtThis = Date.newInstance(System.today().year(), System.today().month(), 1);
		// 先月
		dtAfter = Date.newInstance(System.today().addMonths(1).year(), System.today().addMonths(1).month(), 1);

		// 納品管理を取得
		objDeliv = [
			SELECT Id, Item2_OrderProduct__r.Item2_Relation__r.Order_Relation__c, SalesPerson10__c
			FROM TextManagement2__c
			WHERE Item2_OrderProduct__c IN :strOrderItemIds AND
			(
				(
					Item2_DeliveryExistence__c = '有'
					AND CloudStatus__c NOT IN ('社外納品済み', '振替', '取消', '売上取消')
				)
				OR
				(
					Item2_DeliveryExistence__c = '無'
					AND Item2_OrderProduct__r.Item2_Keyword_phase__c IN ('契約中', '請求停止', '契約期間なし', '途中解約（請求）')
				)
			)
		];

		System.debug('###########4:' + objDeliv);

		// 納品管理のループ
		for (TextManagement2__c obj : objDeliv) {
			// 契約の担当営業を継承
			obj.SalesPerson10__c = mapContract.get(obj.Item2_OrderProduct__r.Item2_Relation__r.Order_Relation__c).OwnerId;
		}

		// 納品管理の更新
		if (objDeliv.size() > 0) update objDeliv;
	}

	public static void prcSupportOwnerChange(List<Contract> trgContract, Map<Id, Contract> oldContract)
	{
		// 契約IDの配列
		List<String> strContractIds = new List<String>();
		// 契約IDの配列：納品"有"
		List<String> strContractIdsA = new List<String>();
		// 契約IDの配列：納品"無"
		List<String> strContractIdsN = new List<String>();
		// 注文IDの配列
		List<String> strOrderIds = new List<String>();
		// 注文商品IDの配列
		List<String> strOrderItemIds = new List<String>();

		// 注文オブジェクト
		List<Order__c> objOrder = new List<Order__c>();
		// 注文商品オブジェクト
		List<OrderItem__c> objOrderItem = new List<OrderItem__c>();
		// 納品管理オブジェクト
		List<TextManagement2__c> objDeliv = new List<TextManagement2__c>();
		// 注文商品オブジェクト（Bind用）
		List<OrderItem__c> objBind = new List<OrderItem__c>();

		// 契約のMap
		Map<String, Contract> mapContract = new Map<String, Contract>();

		// 契約のループ
		for (Contract obj : trgContract) {
			// old mapが取得できなければ処理しない
			if (!oldContract.containsKey(obj.Id)) continue;
			// サポート担当変更あり
			if (obj.Contract_Support__c != oldContract.get(obj.Id).Contract_Support__c) {
				// 契約IDをセット
				strContractIds.add(obj.Id);
				// Mapに追加
				mapContract.put(obj.Id, obj);
			}
		}

		// Bind用の注文商品を取得
		objBind = [
			SELECT Item2_Relation__c, Item2_DeliveryExistence__c
			FROM OrderItem__c
			WHERE Item2_Relation__r.Order_Relation__c IN :strContractIds
		];

		// 納品有無（コード）別に、契約IDを配列にセット
		for (OrderItem__c obj : objBind) {
			if (obj.Item2_DeliveryExistence__c == '有') strContractIdsA.add(obj.Item2_Relation__c);
			if (obj.Item2_DeliveryExistence__c == '無') strContractIdsN.add(obj.Item2_Relation__c);
		}

		System.debug('###########1:' + strContractIdsA);
		System.debug('###########2:' + strContractIdsN);

		// 注文を取得
		objOrder = [
			SELECT Id, Order_Relation__c, Opportunity_Support__c
			FROM Order__c
			WHERE
			(Id IN :strContractIdsA AND Order_Phase__c IN ('契約中', '契約期間なし'))
			OR
			(Id IN :strContractIdsN AND Order_Phase__c IN ('契約中', '契約期間なし', '契約終了'))
		];

		System.debug('###########3:' + objOrder);

		// 注文のループ
		for (Order__c obj : objOrder) {
			// 注文IDをセット
			strOrderIds.add(obj.Id);
			// 契約の担当営業を継承
			obj.Opportunity_Support__c = mapContract.get(obj.Order_Relation__c).Contract_Support__c;
		}

		// 注文の更新
		if (objOrder.size() > 0) update objOrder;

		// 注文商品を取得
		objOrderItem = [
			SELECT Id, Item2_Relation__r.Order_Relation__c, DeliveryPerson__c
			FROM OrderItem__c
			WHERE Item2_Relation__c IN :strOrderIds AND
			(
				(Item2_DeliveryExistence__c = '有'AND ProductionStatus__c != '社外納品済み')
				OR
				(Item2_DeliveryExistence__c = '無'AND Item2_Keyword_phase__c IN ('契約中', '請求停止', '契約期間なし', '途中解約（請求）'))
			)
		];

		// 注文商品のループ
		for (OrderItem__c obj : objOrderItem) {
			// 注文商品IDをセット
			strOrderItemIds.add(obj.Id);
			// 契約の担当営業を継承
			obj.DeliveryPerson__c = mapContract.get(obj.Item2_Relation__r.Order_Relation__c).Contract_Support__c;
		}

		// 注文商品の更新
		if (objOrderItem.size() > 0) update objOrderItem;

		// 当月
		Date dtThis = null;
		// 翌月
		Date dtAfter = null;

		// 当月
		dtThis = Date.newInstance(System.today().year(), System.today().month(), 1);
		// 先月
		dtAfter = Date.newInstance(System.today().addMonths(1).year(), System.today().addMonths(1).month(), 1);

		// 納品管理を取得
		objDeliv = [
			SELECT Id, Item2_OrderProduct__r.Item2_Relation__r.Order_Relation__c, DeliveryPerson__c
			FROM TextManagement2__c
			WHERE Item2_OrderProduct__c IN :strOrderItemIds AND
			(
				(Item2_DeliveryExistence__c = '有' AND CloudStatus__c != '社外納品済み' AND RecordType.DeveloperName IN ('Text', 'Report'))
				OR
				(
					(
					Item2_DeliveryExistence__c = '有'
					AND CloudStatus__c NOT IN ('社外納品済み', '振替', '取消', '売上取消')
				)
				OR
				(
					Item2_DeliveryExistence__c = '無'
					AND Item2_OrderProduct__r.Item2_Keyword_phase__c IN ('契約中', '請求停止', '契約期間なし', '途中解約（請求）')
				)
				)
			)
		];

		System.debug('###########4:' + objDeliv);

		// 納品管理のループ
		for (TextManagement2__c obj : objDeliv) {
			// 契約の担当営業を継承
			obj.DeliveryPerson__c = mapContract.get(obj.Item2_OrderProduct__r.Item2_Relation__r.Order_Relation__c).Contract_Support__c;
		}

		// 納品管理の更新
		if (objDeliv.size() > 0) update objDeliv;
	}
}