global with sharing class OederItemSearchCleaner_view extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public OrderItem__c record {get{return (OrderItem__c)mainRecord;}}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	{
	setApiVersion(42.0);
	}
	public OederItemSearchCleaner_view(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = OrderItem__c.fields.Link_OrderItem__c;
		f = OrderItem__c.fields.Account__c;
		f = OrderItem__c.fields.sisakukigyou__c;
		f = OrderItem__c.fields.Name;
		f = OrderItem__c.fields.Item2_Keyword_phase__c;
		f = OrderItem__c.fields.Item2_Relation__c;
		f = OrderItem__c.fields.Item2_Unit_selling_price__c;
		f = OrderItem__c.fields.Item2_ProductCount__c;
		f = OrderItem__c.fields.Quantity__c;
		f = OrderItem__c.fields.ProductCode1__c;
		f = OrderItem__c.fields.TotalPrice__c;
		f = OrderItem__c.fields.Item2_Product__c;
		f = OrderItem__c.fields.BuyingupPrice__c;
		f = OrderItem__c.fields.Item2_ProductType1__c;
		f = OrderItem__c.fields.ContractMonths__c;
		f = OrderItem__c.fields.Item2_DeliveryExistence__c;
		f = OrderItem__c.fields.Item2_Commission_rate__c;
		f = OrderItem__c.fields.BillingTiming__c;
		f = OrderItem__c.fields.AutomaticUpdate__c;
		f = OrderItem__c.fields.RecordTypeId;
		f = OrderItem__c.fields.Salesforce_ID__c;
		f = OrderItem__c.fields.syouninn__c;
		f = OrderItem__c.fields.Item2_Entry_date__c;
		f = OrderItem__c.fields.Item2_Keyword_strategy_keyword__c;
		f = OrderItem__c.fields.StartDate_EndDatec2__c;
		f = OrderItem__c.fields.Item2_Special_instruction__c;
		f = OrderItem__c.fields.Item2_ServiceDate_EndDay2__c;
		f = OrderItem__c.fields.EndDateCheck__c;
		f = OrderItem__c.fields.BillStop__c;
		f = OrderItem__c.fields.Item2_CancellationDay__c;
		f = OrderItem__c.fields.ChangeDay__c;
		f = OrderItem__c.fields.CancelDay__c;
		f = OrderItem__c.fields.seo_product_First_update_month__c;
		f = OrderItem__c.fields.RecordCreateType__c;
		f = OrderItem__c.fields.SalesUnit1View__c;
		f = OrderItem__c.fields.SalesUnitSubView__c;
		f = OrderItem__c.fields.SalesPerson1__c;
		f = OrderItem__c.fields.SalesPersonSub__c;
		f = OrderItem__c.fields.SalesPercentage1__c;
		f = OrderItem__c.fields.SalesPercentage2__c;
		f = OrderItem__c.fields.ContractGetPersonUnit__c;
		f = OrderItem__c.fields.ContractGetPersonUnit2__c;
		f = OrderItem__c.fields.ContractGetPerson1__c;
		f = OrderItem__c.fields.ContractGetPerson2__c;
		f = OrderItem__c.fields.ContractGetPersonPercentage1__c;
		f = OrderItem__c.fields.ContractGetPersonPercentage2__c;
		f = OrderItem__c.fields.CreatedDate;
		f = OrderItem__c.fields.CreatedById;
		f = OrderItem__c.fields.LastModifiedDate;
		f = OrderItem__c.fields.LastModifiedById;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = OrderItem__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'OederItemSearchCleaner_view';
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(OrderItem__c.SObjectType);
			mainQuery = new SkyEditor2.Query('OrderItem__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Link_OrderItem__c');
			mainQuery.addFieldAsOutput('Account__c');
			mainQuery.addFieldAsOutput('sisakukigyou__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('Item2_Keyword_phase__c');
			mainQuery.addFieldAsOutput('Item2_Relation__c');
			mainQuery.addFieldAsOutput('Item2_Unit_selling_price__c');
			mainQuery.addFieldAsOutput('Item2_ProductCount__c');
			mainQuery.addFieldAsOutput('Quantity__c');
			mainQuery.addFieldAsOutput('ProductCode1__c');
			mainQuery.addFieldAsOutput('TotalPrice__c');
			mainQuery.addFieldAsOutput('Item2_Product__c');
			mainQuery.addFieldAsOutput('BuyingupPrice__c');
			mainQuery.addFieldAsOutput('Item2_ProductType1__c');
			mainQuery.addFieldAsOutput('ContractMonths__c');
			mainQuery.addFieldAsOutput('Item2_DeliveryExistence__c');
			mainQuery.addFieldAsOutput('Item2_Commission_rate__c');
			mainQuery.addFieldAsOutput('BillingTiming__c');
			mainQuery.addFieldAsOutput('AutomaticUpdate__c');
			mainQuery.addFieldAsOutput('RecordType.Name');
			mainQuery.addFieldAsOutput('Salesforce_ID__c');
			mainQuery.addFieldAsOutput('syouninn__c');
			mainQuery.addFieldAsOutput('Item2_Entry_date__c');
			mainQuery.addFieldAsOutput('Item2_Keyword_strategy_keyword__c');
			mainQuery.addFieldAsOutput('StartDate_EndDatec2__c');
			mainQuery.addFieldAsOutput('Item2_Special_instruction__c');
			mainQuery.addFieldAsOutput('Item2_ServiceDate_EndDay2__c');
			mainQuery.addFieldAsOutput('EndDateCheck__c');
			mainQuery.addFieldAsOutput('BillStop__c');
			mainQuery.addFieldAsOutput('Item2_CancellationDay__c');
			mainQuery.addFieldAsOutput('ChangeDay__c');
			mainQuery.addFieldAsOutput('CancelDay__c');
			mainQuery.addFieldAsOutput('seo_product_First_update_month__c');
			mainQuery.addFieldAsOutput('RecordCreateType__c');
			mainQuery.addFieldAsOutput('SalesUnit1View__c');
			mainQuery.addFieldAsOutput('SalesUnitSubView__c');
			mainQuery.addFieldAsOutput('SalesPerson1__c');
			mainQuery.addFieldAsOutput('SalesPersonSub__c');
			mainQuery.addFieldAsOutput('SalesPercentage1__c');
			mainQuery.addFieldAsOutput('SalesPercentage2__c');
			mainQuery.addFieldAsOutput('ContractGetPersonUnit__c');
			mainQuery.addFieldAsOutput('ContractGetPersonUnit2__c');
			mainQuery.addFieldAsOutput('ContractGetPerson1__c');
			mainQuery.addFieldAsOutput('ContractGetPerson2__c');
			mainQuery.addFieldAsOutput('ContractGetPersonPercentage1__c');
			mainQuery.addFieldAsOutput('ContractGetPersonPercentage2__c');
			mainQuery.addFieldAsOutput('CreatedDate');
			mainQuery.addFieldAsOutput('CreatedById');
			mainQuery.addFieldAsOutput('LastModifiedDate');
			mainQuery.addFieldAsOutput('LastModifiedById');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('Item2_Relation__c', 'CF00N10000005jNKn_lkid');
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			if (record.Id == null) {
				saveOldValues();
				if(record.RecordTypeId == null) recordTypeSelector.applyDefault(record);
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}