public with sharing class InvoiceRecordInvalidation {
	public static boolean firstRun = true;
	
	public static void prcInvoiceRecordInvalidation(List<OrderItem__c> objOrderItem) {
		// 請求商品ID
		List<String> strBillProductIds = new List<String>();
		// 請求商品オブジェクト
		List<BillProduct__c> chkBillProduct = new List<BillProduct__c>();
		List<BillProduct__c> updBillProduct = new List<BillProduct__c>();
		
		// 更新対象のチェック
		for (OrderItem__c obj : objOrderItem) {
			System.debug('注文商品フェーズチェック: ' + obj.Item2_Keyword_phase__c + ' / ' + obj.Billchek__c);
			String strErrMsg = '';
			if ( obj.Item2_Keyword_phase__c == '途中解約'
			  || obj.Item2_Keyword_phase__c == '契約変更'
			  || obj.Item2_Keyword_phase__c == '取消'
			  || obj.Item2_Keyword_phase__c == '売上取消'
			  || obj.Item2_Keyword_phase__c == '請求停止'
			) {
				System.debug('途中解約/契約変更/取消/売上取消/請求停止フェーズ--->>>請求無効対象注文商品: ' + obj.Id);
				chkBillProduct = [SELECT Id, Name, Bill2__c, Bill2__r.Name, BillProductCancel__c, BillProductMemo__c, OrderItem__c, Bill_BillCheck__c, SalesCheck__c FROM BillProduct__c WHERE BillProductCancel__c = false AND OrderItem__c = :obj.Id];
				for (BillProduct__c chk: chkBillProduct) {
					if (chk.Bill_BillCheck__c == true) {
						strBillProductIds.add(chk.Id);
						strErrMsg = '<br />請求商品' + chk.Name + ' を無効化できません！';
						strErrMsg += '<br />請求レコード:<a href="/' + chk.Bill2__c + '" target=”_blank”>' + chk.Bill2__r.Name + '</a>の請求書発行済みフラグを確認して下さい';
						obj.addError(strErrMsg);
					}
					else if (chk.SalesCheck__c) {
						strBillProductIds.add(chk.Id);
						strErrMsg = '<br />請求商品' + chk.Name + ' を無効化できません！';
						strErrMsg += '<br />請求レコード:<a href="/' + chk.Bill2__c + '" target=”_blank>' + chk.Bill2__r.Name + '</a>の営業確認チェックを確認して下さい';
						obj.addError(strErrMsg);
					}
					else {
						chk.BillProductCancel__c = true;
						if (chk.BillProductMemo__c == null) chk.BillProductMemo__c = '';
						chk.BillProductMemo__c += '\n\n注文商品フェーズ変更（途中解約・契約変更・取消・売上取消） --->>> 請求商品・無効化';
						System.debug('請求レコード無効チェック変更: ' + chk.Id);
						updBillProduct.add(chk);
					}
				}
			}
		}
		// 更新対象がある場合のみ
		if (updBillProduct.size() > 0 && strBillProductIds.size() == 0) {
			update updBillProduct;
		}
	}

	public static void prcInvoiceRecordInvalidationFromTM(List<TextManagement2__c> objTextManagement) {
		// 請求商品ID
		List<String> strBillProductIds = new List<String>();
		// 請求商品オブジェクト
		List<BillProduct__c> chkBillProduct = new List<BillProduct__c>();
		List<BillProduct__c> updBillProduct = new List<BillProduct__c>();
		
		// 更新対象のチェック
		for (TextManagement2__c obj : objTextManagement) {
			System.debug('納品管理ステータスチェック: ' + obj.CloudStatus__c + ' / ' + obj.Billchek__c);
			String strErrMsg = '';
			if (obj.CloudStatus__c == '振替' || obj.CloudStatus__c == '契約変更' || obj.CloudStatus__c == '売上取消') {
				System.debug('振替・契約変更・売上取消ステータス--->>>請求無効対象納品管理: ' + obj.Id);
				chkBillProduct = [SELECT Id, Name, Bill2__c, Bill2__r.Name, BillProductCancel__c, BillProductMemo__c, OrderItem__c, Bill_BillCheck__c, SalesCheck__c FROM BillProduct__c WHERE BillProductCancel__c = false AND TextManagement2__c = :obj.Id];
				for (BillProduct__c chk: chkBillProduct) {
					if (chk.Bill_BillCheck__c == true) {
						strBillProductIds.add(chk.Id);
						strErrMsg = '<br />請求商品' + chk.Name + ' を無効化できません！';
						strErrMsg += '<br />請求レコード:<a href="/' + chk.Bill2__c + '" target=”_blank”>' + chk.Bill2__r.Name + '</a>の請求書発行済みフラグを確認して下さい';
						obj.addError(strErrMsg);
					}
					else if (chk.SalesCheck__c) {
						strBillProductIds.add(chk.Id);
						strErrMsg = '<br />請求商品' + chk.Name + ' を無効化できません！';
						strErrMsg += '<br />請求レコード:<a href="/' + chk.Bill2__c + '" target=”_blank>' + chk.Bill2__r.Name + '</a>の営業確認チェックを確認して下さい';
						obj.addError(strErrMsg);
					}
					else {
						chk.BillProductCancel__c = true;
						if (chk.BillProductMemo__c == null) chk.BillProductMemo__c = '';
						chk.BillProductMemo__c += '\n\n納品管理ステータス変更（振替・契約変更・売上取消） --->>> 請求商品・無効化';
						System.debug('請求レコード無効チェック変更: ' + chk.Id);
						updBillProduct.add(chk);
					}
				}
			}
		}
		// 更新対象がある場合のみ
		if (updBillProduct.size() > 0 && strBillProductIds.size() == 0) {
			update updBillProduct;
		}
	}
}