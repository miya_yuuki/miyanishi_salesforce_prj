@isTest
private with sharing class ThemePlanning2Test{
		private static testMethod void testPageMethods() {	
			ThemePlanning2 page = new ThemePlanning2(new ApexPages.StandardController(new ThemePlanning__c()));	
			page.getOperatorOptions_ThemePlanning_c_AccountName_c();	
			page.getOperatorOptions_ThemePlanning_c_ProductItemName_c();	
			page.getOperatorOptions_ThemePlanning_c_SalesPersonUnit_c();	
			page.getOperatorOptions_ThemePlanning_c_PointStatus_c_multi();	
			page.getOperatorOptions_ThemePlanning_c_EditorID_c();	
			page.getOperatorOptions_ThemePlanning_c_Name();	
			page.getOperatorOptions_ThemePlanning_c_Phase_c_multi();	
			page.getOperatorOptions_ThemePlanning_c_TextManagement_c();	
			page.getOperatorOptions_ThemePlanning_c_Questionnaire_c_multi();	
			page.getOperatorOptions_ThemePlanning_c_EditorName_c();	
			page.getOperatorOptions_ThemePlanning_c_DeliveryStatus_c();	
			System.assert(true);
		}	
			
	private static testMethod void testComponent3() {
		ThemePlanning2.Component3 Component3 = new ThemePlanning2.Component3(new List<ThemePlanning__c>(), new List<ThemePlanning2.Component3Item>(), new List<ThemePlanning__c>(), null);
		Component3.create(new ThemePlanning__c());
		Component3.doDeleteSelectedItems();
		System.assert(true);
	}
	
}