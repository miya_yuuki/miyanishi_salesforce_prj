@isTest
private with sharing class NouhinDATAforPSTest{
		private static testMethod void testPageMethods() {	
			NouhinDATAforPS page = new NouhinDATAforPS(new ApexPages.StandardController(new TextManagement2__c()));	
			page.getOperatorOptions_TextManagement2_c_CloudStatus_c_multi();	
			page.getOperatorOptions_TextManagement2_c_Item2_DeliveryExistence_c();	
			page.getOperatorOptions_TextManagement2_c_SalesPerson10_c();	
			page.getOperatorOptions_TextManagement2_c_Account1_c();	
			page.getOperatorOptions_TextManagement2_c_DeliveryPerson_c();	
			page.getOperatorOptions_TextManagement2_c_sisakukigyou_c();	
			page.getOperatorOptions_TextManagement2_c_SalesPersonUnit_c();	
			page.getOperatorOptions_TextManagement2_c_Product_c();	
			page.getOperatorOptions_TextManagement2_c_PSkubun_c();	
			page.getOperatorOptions_TextManagement2_c_DeliveryMonth_c();	
			page.getOperatorOptions_TextManagement2_c_DeliveryMonth2_c();	
			page.getOperatorOptions_TextManagement2_c_StartMonth_c();	
			System.assert(true);
		}	
			
	private static testMethod void testComponent2() {
		NouhinDATAforPS.Component2 Component2 = new NouhinDATAforPS.Component2(new List<TextManagement2__c>(), new List<NouhinDATAforPS.Component2Item>(), new List<TextManagement2__c>(), null, null);
		Component2.create(new TextManagement2__c());
		Component2.doDeleteSelectedItems();
		Component2.setPagesize(10);		Component2.doFirst();
		Component2.doPrevious();
		Component2.doNext();
		Component2.doLast();
		Component2.doSort();
		System.assert(true);
	}
	
}