global with sharing class OrderItemSearchgamen extends SkyEditor2.SkyEditorPageBaseWithSharing{
	public OrderItem__c record{get;set;}
	public Component2 Component2 {get; private set;}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	public SkyEditor2__SkyEditorDummy__c Component333_from{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component333_to{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component333_isNull{get;set;}
	public SkyEditor2.TextHolder.OperatorHolder Component333_isNull_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component335_from{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component335_to{get;set;}
	public OrderItem__c Component69_val {get;set;}
	public SkyEditor2.TextHolder Component69_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component73_val {get;set;}
	public SkyEditor2.TextHolder Component73_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component116_val {get;set;}
	public SkyEditor2.TextHolder Component116_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component118_val {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component118_val_dummy {get;set;}
	public SkyEditor2.TextHolder Component118_op{get;set;}
	public List<SelectOption> valueOptions_OrderItem_c_OrderItem_keywordType_c_multi {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component19_val {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component19_val_dummy {get;set;}
	public SkyEditor2.TextHolder Component19_op{get;set;}
	public List<SelectOption> valueOptions_OrderItem_c_Item2_Keyword_phase_c_multi {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component206_val {get;set;}
	public SkyEditor2.TextHolder Component206_op{get;set;}
	public String recordTypeRecordsJSON_OrderItem_c {get; private set;}
	public String defaultRecordTypeId_OrderItem_c {get; private set;}
	public String metadataJSON_OrderItem_c {get; private set;}
	{
	setApiVersion(42.0);
	}
	public OrderItemSearchgamen(ApexPages.StandardController controller){
		super(controller);

		SObjectField f;

		f = OrderItem__c.fields.Item2_Relation__c;
		f = OrderItem__c.fields.Account__c;
		f = OrderItem__c.fields.Item2_Keyword_strategy_keyword__c;
		f = OrderItem__c.fields.OrderItem_keywordType__c;
		f = OrderItem__c.fields.Item2_Keyword_phase__c;
		f = OrderItem__c.fields.OrderItem_URL__c;
		f = OrderItem__c.fields.Name;
		f = OrderItem__c.fields.Item2_Product__c;
		f = OrderItem__c.fields.ContractMonths__c;
		f = OrderItem__c.fields.Item2_ProductCount__c;
		f = OrderItem__c.fields.Item2_Estimate_level__c;
		f = OrderItem__c.fields.Item2_CancellationDay__c;
		f = OrderItem__c.fields.Item2_Unit_selling_price__c;
		f = OrderItem__c.fields.CancelDay__c;
		f = OrderItem__c.fields.ChangeDay__c;
		f = OrderItem__c.fields.syouninn__c;
		f = OrderItem__c.fields.TotalPrice__c;
		f = OrderItem__c.fields.Quantity__c;
		f = OrderItem__c.fields.ServiceDate__c;
		f = OrderItem__c.fields.EndDate__c;
		f = OrderItem__c.fields.Item2_Commission_rate__c;
		f = OrderItem__c.fields.AutomaticUpdate__c;
		f = OrderItem__c.fields.Item2_Entry_date__c;
		f = OrderItem__c.fields.BillingTiming__c;
		f = OrderItem__c.fields.Item2_ServiceDate__c;
		f = OrderItem__c.fields.Item2_EndData__c;
		f = OrderItem__c.fields.SalesPerson1__c;
		f = OrderItem__c.fields.SalesPercentage1__c;
		f = OrderItem__c.fields.SalesPersonSubUnit__c;
		f = OrderItem__c.fields.SalesPersonSub__c;
		f = OrderItem__c.fields.SalesPercentage2__c;
		f = OrderItem__c.fields.ContractGetPersonUnit__c;
		f = OrderItem__c.fields.ContractGetPerson1__c;
		f = OrderItem__c.fields.ContractGetPersonPercentage1__c;
		f = OrderItem__c.fields.ContractGetPersonUnit2__c;
		f = OrderItem__c.fields.ContractGetPerson2__c;
		f = OrderItem__c.fields.ContractGetPersonPercentage2__c;
 f = OrderItem__c.fields.Item2_ServiceDate__c;
 f = OrderItem__c.fields.Item2_ProductCount__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainRecord = null;
			mainSObjectType = OrderItem__c.SObjectType;
			mode = SkyEditor2.LayoutMode.TempSearch_01; 
			Component333_from = new SkyEditor2__SkyEditorDummy__c();
			Component333_to = new SkyEditor2__SkyEditorDummy__c();
			Component333_isNull = new SkyEditor2__SkyEditorDummy__c();
			Component333_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq');
			Component335_from = new SkyEditor2__SkyEditorDummy__c();
			Component335_to = new SkyEditor2__SkyEditorDummy__c();
			OrderItem__c lookupObjComponent27 = new OrderItem__c();
			Component69_val = lookupObjComponent27;
			Component69_op = new SkyEditor2.TextHolder();
			Component73_val = new SkyEditor2__SkyEditorDummy__c();
			Component73_op = new SkyEditor2.TextHolder();
			Component116_val = new SkyEditor2__SkyEditorDummy__c();
			Component116_op = new SkyEditor2.TextHolder();
			Component118_val = new SkyEditor2__SkyEditorDummy__c();
			Component118_val_dummy = new SkyEditor2__SkyEditorDummy__c();
			Component118_op = new SkyEditor2.TextHolder();
			valueOptions_OrderItem_c_OrderItem_keywordType_c_multi = new List<SelectOption>{new SelectOption('', Label.none)};
			for (PicklistEntry e : OrderItem__c.OrderItem_keywordType__c.getDescribe().getPicklistValues()) {
				if (e.isActive()) {valueOptions_OrderItem_c_OrderItem_keywordType_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));}
			}
			Component19_val = new SkyEditor2__SkyEditorDummy__c();
			Component19_val_dummy = new SkyEditor2__SkyEditorDummy__c();
			Component19_op = new SkyEditor2.TextHolder();
			valueOptions_OrderItem_c_Item2_Keyword_phase_c_multi = new List<SelectOption>{new SelectOption('', Label.none)};
			for (PicklistEntry e : OrderItem__c.Item2_Keyword_phase__c.getDescribe().getPicklistValues()) {
				if (e.isActive()) {valueOptions_OrderItem_c_Item2_Keyword_phase_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));}
			}
			Component206_val = new SkyEditor2__SkyEditorDummy__c();
			Component206_op = new SkyEditor2.TextHolder();
			queryMap.put(
				'Component2',
				new SkyEditor2.Query('OrderItem__c')
					.addField('Item2_Keyword_phase__c')
					.addFieldAsOutput('Item2_Relation__c')
					.addFieldAsOutput('Name')
					.addFieldAsOutput('Account__c')
					.addField('Item2_Product__c')
					.addField('ContractMonths__c')
					.addField('Item2_ProductCount__c')
					.addField('Item2_Keyword_strategy_keyword__c')
					.addField('OrderItem_keywordType__c')
					.addField('Item2_Estimate_level__c')
					.addField('OrderItem_URL__c')
					.addField('Item2_CancellationDay__c')
					.addField('Item2_Unit_selling_price__c')
					.addField('CancelDay__c')
					.addField('ChangeDay__c')
					.addField('syouninn__c')
					.addFieldAsOutput('TotalPrice__c')
					.addFieldAsOutput('Quantity__c')
					.addField('ServiceDate__c')
					.addField('EndDate__c')
					.addField('Item2_Commission_rate__c')
					.addFieldAsOutput('AutomaticUpdate__c')
					.addFieldAsOutput('Item2_Entry_date__c')
					.addFieldAsOutput('BillingTiming__c')
					.addField('Item2_ServiceDate__c')
					.addField('Item2_EndData__c')
					.addFieldAsOutput('SalesPerson1__c')
					.addFieldAsOutput('SalesPercentage1__c')
					.addFieldAsOutput('SalesPersonSubUnit__c')
					.addFieldAsOutput('SalesPersonSub__c')
					.addFieldAsOutput('SalesPercentage2__c')
					.addFieldAsOutput('ContractGetPersonUnit__c')
					.addFieldAsOutput('ContractGetPerson1__c')
					.addFieldAsOutput('ContractGetPersonPercentage1__c')
					.addFieldAsOutput('ContractGetPersonUnit2__c')
					.addFieldAsOutput('ContractGetPerson2__c')
					.addFieldAsOutput('ContractGetPersonPercentage2__c')
					.addFieldAsOutput('RecordTypeId')
					.limitRecords(500)
					.addListener(new SkyEditor2.QueryWhereRegister(Component333_from, 'SkyEditor2__Date__c', 'Item2_ServiceDate__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component333_to, 'SkyEditor2__Date__c', 'Item2_ServiceDate__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component333_isNull, 'SkyEditor2__Date__c', 'Item2_ServiceDate__c', Component333_isNull_op, true,0,false )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component335_from, 'SkyEditor2__Text__c', 'Item2_ProductCount__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component335_to, 'SkyEditor2__Text__c', 'Item2_ProductCount__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component69_val, 'Item2_Relation__c', 'Item2_Relation__c', Component69_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component73_val, 'SkyEditor2__Text__c', 'Account__c', Component73_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component116_val, 'SkyEditor2__Text__c', 'Item2_Keyword_strategy_keyword__c', Component116_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component118_val_dummy, 'SkyEditor2__Text__c','OrderItem_keywordType__c', Component118_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component19_val_dummy, 'SkyEditor2__Text__c','Item2_Keyword_phase__c', Component19_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component206_val, 'SkyEditor2__Text__c', 'OrderItem_URL__c', Component206_op, true, 0, false ))
				);
			Component2 = new Component2(new List<OrderItem__c>(), new List<Component2Item>(), new List<OrderItem__c>(), null);
			 Component2.setPageItems(new List<Component2Item>());
			 Component2.setPageSize(100);
			listItemHolders.put('Component2', Component2);
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(OrderItem__c.SObjectType, true);
			p_showHeader = true;
			p_sidebar = false;
			execInitialSearch = false;
			presetSystemParams();
			Component2.extender = this.extender;
			initSearch();
		} catch (SkyEditor2.Errors.SObjectNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.Errors.FieldNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.ExtenderException e) {
			 e.setMessagesToPage();
		} catch (Exception e) {
			System.Debug(LoggingLevel.Error, e);
			SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);
		}
	}
	public List<SelectOption> getOperatorOptions_OrderItem_c_Item2_Relation_c() { 
		return getOperatorOptions('OrderItem__c', 'Item2_Relation__c');
	}
	public List<SelectOption> getOperatorOptions_OrderItem_c_Account_c() { 
		return getOperatorOptions('OrderItem__c', 'Account__c');
	}
	public List<SelectOption> getOperatorOptions_OrderItem_c_Item2_Keyword_strategy_keyword_c() { 
		return getOperatorOptions('OrderItem__c', 'Item2_Keyword_strategy_keyword__c');
	}
	public List<SelectOption> getOperatorOptions_OrderItem_c_OrderItem_keywordType_c_multi() { 
		return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	}
	public List<SelectOption> getOperatorOptions_OrderItem_c_Item2_Keyword_phase_c_multi() { 
		return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	}
	public List<SelectOption> getOperatorOptions_OrderItem_c_OrderItem_URL_c() { 
		return getOperatorOptions('OrderItem__c', 'OrderItem_URL__c');
	}
	global with sharing class Component2Item extends SkyEditor2.ListItem {
		public OrderItem__c record{get; private set;}
		@TestVisible
		Component2Item(Component2 holder, OrderItem__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component2 extends SkyEditor2.PagingList {
		public List<Component2Item> items{get; private set;}
		@TestVisible
			Component2(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component2Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component2Item(this, (OrderItem__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
		public void doFirst(){first();}
		public void doPrevious(){previous();}
		public void doNext(){next();}
		public void doLast(){last();}
		public void doSort(){sort();}

        public List<Component2Item> getViewItems() {            return (List<Component2Item>) getPageItems();        }
	}

	public OrderItem__c Component2_table_Conversion { get { return new OrderItem__c();}}
	
	public String Component2_table_selectval { get; set; }
	
	
}