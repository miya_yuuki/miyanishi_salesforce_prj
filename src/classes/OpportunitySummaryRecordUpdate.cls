global class OpportunitySummaryRecordUpdate {
    global static PageReference OpportunitySummaryUpdate() {
        List<ContractOpportunity__c> updContractOpportunity = new List<ContractOpportunity__c>();
        List<ContractOpportunity__c> chkContractOpportunity = [
            SELECT
                Id,                                 // 商談集計用ID
                LastModifiedDate,                   // 商談集計用最終更新日
                Opportunity__c,                     // 商談ID
                OpportunityLastModifiedDate__c,     // 商談最終更新日
                Opportunity__r.StageName,           // 商談の商談フェーズ
                OpportunityPhase__c,                // 商談集計用の商談フェーズ
                AmountTotal__c,                     // 商談集計用の商談合計金額
                Opportunity__r.AmountTotal__c,      // 商談の契約額
                syouninn__c,                        // 商談集計用の受注承認日
                Opportunity__r.syouninn__c          // 商談の受注承認日
            FROM ContractOpportunity__c
            WHERE
                OpportunityLastModifiedDate__c = LAST_N_DAYS:7
                AND LastModifiedDate <> TODAY
            order by OpportunityLastModifiedDate__c desc
            limit 2000
        ];

        // 更新対象の商談集計用を更新する
        for(ContractOpportunity__c chk : chkContractOpportunity) {
            Date syouninn = null;
            if (chk.Opportunity__r.syouninn__c != null) {
                syouninn = chk.Opportunity__r.syouninn__c.date();
            }
            ContractOpportunity__c newContractOpportunity = new ContractOpportunity__c(
                Id = chk.Id,                                            // 商談集計用ID
                Opportunity__c = chk.Opportunity__c,                    // 商談ID
                OpportunityPhase__c = chk.Opportunity__r.StageName,     // 商談フェーズ
                AmountTotal__c = chk.Opportunity__r.AmountTotal__c,     // 契約額
                syouninn__c = syouninn                                  // 受注承認日
            );
            updContractOpportunity.add(newContractOpportunity);
            System.debug('商談ID: ' + chk.Opportunity__c + ', 商談最終更新日: ' + chk.OpportunityLastModifiedDate__c + ', 集計用ID: ' + chk.Id + ', 集計用最終更新日: ' + chk.LastModifiedDate);
            System.debug('集計用ID: ' + chk.Id + ', 商談フェーズ: ' + chk.Opportunity__r.StageName + ', 受注承認日: ' + chk.Opportunity__r.syouninn__c);
        }

        if(updContractOpportunity.size() > 0) {
            System.debug('更新対象: ' + updContractOpportunity.size() + ' 件');
            update updContractOpportunity;
        }
        else {
            System.debug('更新対象: 0件');
        }

        return null;
    }
}