public with sharing class CancellationAmountCalculation {
    public static boolean firstRun = true;
    
    public static void prcGetCancellationAmount(List<CustomObject1__c> objRequest) {
        // 申請（案件）ID
        List<String> strCancelIds = new List<String>();
        // 契約内容申請用レコード 
        List<SEOChange__c> objSEOChange = new List<SEOChange__c>();
        // 申請（案件）レコード
        List<CustomObject1__c> CancelRec = new List<CustomObject1__c>();
        List<CustomObject1__c> updCancelAmount = new List<CustomObject1__c>();
        // 解約金額
        Integer cancelAmount = 0;
        // 解約月請求額
        Integer curMonthAmount = 0;

        // 申請（案件）IDをセット
        for (CustomObject1__c obj: objRequest) {
            // 解約申請以外はスキップ
            if (obj.RecordtypeId != '01210000000AQRB') continue;
            strCancelIds.add(obj.Id);

        }
        // 解約申請にセットされた契約内容申請用レコードを抽出
        objSEOChange = [
            SELECT
                Id,
                sinsei__c,
                CancelableAmount__c,
                Amount2__c,
                CancelMonthCheck__c,
                Item2_ServiceDate__c,
                Item2_EndData__c,
                sinsei__r.CancelType2__c
            FROM
                SEOChange__c
            WHERE
                sinsei__c IN :strCancelIds
        ];
        // 契約内容申請用レコードから解約金額を計算
        for (SEOChange__c objOI: objSEOChange) {
            System.debug('CancelType: ' + objOI.sinsei__r.CancelType2__c + ', Check: ' + objOI.CancelMonthCheck__c + ', Amount: ' + objOI.Amount2__c + ' : ' + objOI.CancelableAmount__c);
            if (objOI.sinsei__r.CancelType2__c == '途中解約') { 
                // 解約可能額を解約金額として合算
                if (objOI.CancelableAmount__c != null) {
                    cancelAmount += Integer.valueOf(objOI.CancelableAmount__c);
                    if (objOI.CancelMonthCheck__c == TRUE ) {
                        curMonthAmount += Integer.valueOf(objOI.CancelableAmount__c);
                    }
                }
            }
            else {
                // 契約額を解約金額として合算
                if (objOI.Amount2__c != null) {
                    cancelAmount += Integer.valueOf(objOI.Amount2__c);
                    if (objOI.CancelMonthCheck__c == TRUE ) {
                        curMonthAmount += Integer.valueOf(objOI.Amount2__c);
                    }
                }
            }
        }

        // 申請（案件）レコードの再取得
        CancelRec = [
            SELECT
                Id,
                CancelType2__c,
                CancelMonthBilling__c,
                kaiyakuTotalAmount__c,
                RequestAmount__c,
                ManualSetting_CancelTotalAmount__c,
                ManualSetting_CancelMonthRequestAmount__c
            FROM CustomObject1__c
            WHERE
                Id IN :strCancelIds
        ];
        for (CustomObject1__c upd: CancelRec) {
            // 途中解約、かつ解約月請求有りの場合は解約月の請求額を解約総額から差し引いておく
            if (upd.CancelMonthBilling__c == '有' && upd.CancelType2__c == '途中解約') cancelAmount = cancelAmount - curMonthAmount;
            if (upd.CancelMonthBilling__c == '無') curMonthAmount = 0;

            // 更新するか否かチェック
            Boolean CheckUpdate = prcCheckUpdateCancellationAmount(upd, cancelAmount, curMonthAmount);
            System.debug('CheckUpdate: ' + CheckUpdate);

            // 更新OKならば各解約金額に代入
            if (CheckUpdate) {
                if (!upd.ManualSetting_CancelTotalAmount__c) upd.kaiyakuTotalAmount__c = cancelAmount;
                if (!upd.ManualSetting_CancelMonthRequestAmount__c) upd.RequestAmount__c = curMonthAmount;
                updCancelAmount.add(upd);
            }
        }
        if (updCancelAmount.size() > 0) update updCancelAmount;
    }

    private static boolean prcCheckUpdateCancellationAmount(CustomObject1__c obj, Integer intTotal, Integer intMonth) {
        System.debug(
            'kaiyakuTotalAmount__c: ' + obj.kaiyakuTotalAmount__c
             + ', intTotal: ' + intTotal
             + ', CancelMonthBilling__c: ' + obj.CancelMonthBilling__c
             + ', RequestAmount__c: ' + obj.RequestAmount__c
             + ', intMonth: ' + intMonth
             + ', ManualSetting_CancelTotalAmount__c: ' + obj.ManualSetting_CancelTotalAmount__c
             + ', ManualSetting_CancelMonthRequestAmount__c: ' + obj.ManualSetting_CancelMonthRequestAmount__c
        );

        // 手動設定両方にチェックが入っている時は更新しない
        if (obj.ManualSetting_CancelTotalAmount__c == true && obj.ManualSetting_CancelMonthRequestAmount__c == true) {
            return false;
        }
        // 更新対象が既に計算値と一致する時は何もせず終了する
        else if (obj.kaiyakuTotalAmount__c == intTotal && obj.RequestAmount__c == intMonth) {
            return false;
        }
        else if (obj.ManualSetting_CancelTotalAmount__c == true && obj.RequestAmount__c == intMonth) {
            return false;
        }
        else if (obj.kaiyakuTotalAmount__c == intTotal && obj.ManualSetting_CancelMonthRequestAmount__c == true) {
            return false;
        }
        // いずれにも合致しない場合は更新対象とする
        else {
            return true;
        }
    }
}