global with sharing class textmanager_for_accountant extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public TextManagement2__c record{get;set;}	
			
	
		public Component3 Component3 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component69_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component69_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component69_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component69_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component35_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component35_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component35_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component35_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component72_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component72_to{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component34_val {get;set;}	
		public SkyEditor2.TextHolder Component34_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component76_val {get;set;}	
		public SkyEditor2.TextHolder Component76_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component44_val {get;set;}	
		public SkyEditor2.TextHolder Component44_op{get;set;}	
			
		public TextManagement2__c Component7_val {get;set;}	
		public SkyEditor2.TextHolder Component7_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component9_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component9_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component9_op{get;set;}	
		public List<SelectOption> valueOptions_TextManagement2_c_CloudStatus_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component46_val {get;set;}	
		public SkyEditor2.TextHolder Component46_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component67_val {get;set;}	
		public SkyEditor2.TextHolder Component67_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component81_val {get;set;}	
		public SkyEditor2.TextHolder Component81_op{get;set;}	
			
	public String recordTypeRecordsJSON_TextManagement2_c {get; private set;}
	public String defaultRecordTypeId_TextManagement2_c {get; private set;}
	public String metadataJSON_TextManagement2_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public textmanager_for_accountant(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = TextManagement2__c.fields.Account1__c;
		f = TextManagement2__c.fields.Name;
		f = TextManagement2__c.fields.OrderItemStatus__c;
		f = TextManagement2__c.fields.Item2_OrderProduct__c;
		f = TextManagement2__c.fields.CloudStatus__c;
		f = TextManagement2__c.fields.Product11__c;
		f = TextManagement2__c.fields.ProductRecordType__c;
		f = TextManagement2__c.fields.BillAmountPrint__c;
		f = TextManagement2__c.fields.Bill_Main__c;
		f = TextManagement2__c.fields.DeliveryDay__c;
		f = TextManagement2__c.fields.recorded_date__c;
 f = TextManagement2__c.fields.recorded_date__c;
 f = TextManagement2__c.fields.DeliveryDay__c;
 f = TextManagement2__c.fields.BillAmountPrint__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = TextManagement2__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component69_from = new SkyEditor2__SkyEditorDummy__c();	
				Component69_to = new SkyEditor2__SkyEditorDummy__c();	
				Component69_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component69_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component35_from = new SkyEditor2__SkyEditorDummy__c();	
				Component35_to = new SkyEditor2__SkyEditorDummy__c();	
				Component35_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component35_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component72_from = new SkyEditor2__SkyEditorDummy__c();	
				Component72_to = new SkyEditor2__SkyEditorDummy__c();	
					
				TextManagement2__c lookupObjComponent29 = new TextManagement2__c();	
				Component34_val = new SkyEditor2__SkyEditorDummy__c();	
				Component34_op = new SkyEditor2.TextHolder();	
					
				Component76_val = new SkyEditor2__SkyEditorDummy__c();	
				Component76_op = new SkyEditor2.TextHolder();	
					
				Component44_val = new SkyEditor2__SkyEditorDummy__c();	
				Component44_op = new SkyEditor2.TextHolder();	
					
				Component7_val = lookupObjComponent29;	
				Component7_op = new SkyEditor2.TextHolder();	
					
				Component9_val = new SkyEditor2__SkyEditorDummy__c();	
				Component9_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component9_op = new SkyEditor2.TextHolder();	
				valueOptions_TextManagement2_c_CloudStatus_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : TextManagement2__c.CloudStatus__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_TextManagement2_c_CloudStatus_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component46_val = new SkyEditor2__SkyEditorDummy__c();	
				Component46_op = new SkyEditor2.TextHolder();	
					
				Component67_val = new SkyEditor2__SkyEditorDummy__c();	
				Component67_op = new SkyEditor2.TextHolder();	
					
				Component81_val = new SkyEditor2__SkyEditorDummy__c();	
				Component81_op = new SkyEditor2.TextHolder();	
					
				queryMap.put(	
					'Component3',	
					new SkyEditor2.Query('TextManagement2__c')
						.addFieldAsOutput('Item2_OrderProduct__c')
						.addFieldAsOutput('Name')
						.addFieldAsOutput('BillAmountPrint__c')
						.addFieldAsOutput('Account1__c')
						.addFieldAsOutput('OrderItemStatus__c')
						.addFieldAsOutput('CloudStatus__c')
						.addFieldAsOutput('Bill_Main__c')
						.addFieldAsOutput('DeliveryDay__c')
						.addFieldAsOutput('Product11__c')
						.addField('recorded_date__c')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(1000)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component69_from, 'SkyEditor2__Date__c', 'recorded_date__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component69_to, 'SkyEditor2__Date__c', 'recorded_date__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component69_isNull, 'SkyEditor2__Date__c', 'recorded_date__c', Component69_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component35_from, 'SkyEditor2__Date__c', 'DeliveryDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component35_to, 'SkyEditor2__Date__c', 'DeliveryDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component35_isNull, 'SkyEditor2__Date__c', 'DeliveryDay__c', Component35_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component72_from, 'SkyEditor2__Text__c', 'BillAmountPrint__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component72_to, 'SkyEditor2__Text__c', 'BillAmountPrint__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component34_val, 'SkyEditor2__Text__c', 'Account1__c', Component34_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component76_val, 'SkyEditor2__Text__c', 'Name', Component76_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component44_val, 'SkyEditor2__Text__c', 'OrderItemStatus__c', Component44_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component7_val, 'Item2_OrderProduct__c', 'Item2_OrderProduct__c', Component7_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component9_val_dummy, 'SkyEditor2__Text__c','CloudStatus__c', Component9_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component46_val, 'SkyEditor2__Text__c', 'Product11__c', Component46_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component67_val, 'SkyEditor2__Text__c', 'ProductRecordType__c', Component67_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component81_val, 'SkyEditor2__Text__c', 'BillAmountPrint__c', Component81_op, true, 0, false ))
				);	
					
					Component3 = new Component3(new List<TextManagement2__c>(), new List<Component3Item>(), new List<TextManagement2__c>(), null);
				listItemHolders.put('Component3', Component3);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(TextManagement2__c.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = true;
			execInitialSearch = false;
			presetSystemParams();
			Component3.extender = this.extender;
			initSearch();
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Account1_c() { 
			return getOperatorOptions('TextManagement2__c', 'Account1__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Name() { 
			return getOperatorOptions('TextManagement2__c', 'Name');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_OrderItemStatus_c() { 
			return getOperatorOptions('TextManagement2__c', 'OrderItemStatus__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Item2_OrderProduct_c() { 
			return getOperatorOptions('TextManagement2__c', 'Item2_OrderProduct__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_CloudStatus_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Product11_c() { 
			return getOperatorOptions('TextManagement2__c', 'Product11__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_ProductRecordType_c() { 
			return getOperatorOptions('TextManagement2__c', 'ProductRecordType__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_BillAmountPrint_c() { 
			return getOperatorOptions('TextManagement2__c', 'BillAmountPrint__c');	
		}	
			
			
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public TextManagement2__c record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, TextManagement2__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.ListItemHolder {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (TextManagement2__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	public TextManagement2__c Component3_table_Conversion { get { return new TextManagement2__c();}}
	
	public String Component3_table_selectval { get; set; }
	
	
			
	}