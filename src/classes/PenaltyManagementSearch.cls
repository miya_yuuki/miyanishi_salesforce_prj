global with sharing class PenaltyManagementSearch extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public PenaltyManagement__c record{get;set;}	
			
	
		public Component3 Component3 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component15_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component15_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component15_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component15_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component6_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component6_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component6_op{get;set;}	
		public List<SelectOption> valueOptions_PenaltyManagement_c_regularStatus_c_multi {get;set;}
			
		public karute__c Component12_val {get;set;}	
		public SkyEditor2.TextHolder Component12_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component8_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component8_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component8_op{get;set;}	
		public List<SelectOption> valueOptions_PenaltyManagement_c_penaltyStatus_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component14_val {get;set;}	
		public SkyEditor2.TextHolder Component14_op{get;set;}	
			
		public karute__c Component10_val {get;set;}	
		public SkyEditor2.TextHolder Component10_op{get;set;}	
			
	public String recordTypeRecordsJSON_PenaltyManagement_c {get; private set;}
	public String defaultRecordTypeId_PenaltyManagement_c {get; private set;}
	public String metadataJSON_PenaltyManagement_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public PenaltyManagementSearch(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = PenaltyManagement__c.fields.regularStatus__c;
		f = karute__c.fields.AccountName__c;
		f = PenaltyManagement__c.fields.penaltyStatus__c;
		f = karute__c.fields.URL__c;
		f = karute__c.fields.OwnerId;
		f = PenaltyManagement__c.fields.date__c;
		f = PenaltyManagement__c.fields.detail__c;
		f = PenaltyManagement__c.fields.karute__c;
 f = PenaltyManagement__c.fields.date__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = PenaltyManagement__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component15_from = new SkyEditor2__SkyEditorDummy__c();	
				Component15_to = new SkyEditor2__SkyEditorDummy__c();	
				Component15_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component15_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component6_val = new SkyEditor2__SkyEditorDummy__c();	
				Component6_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component6_op = new SkyEditor2.TextHolder();	
				valueOptions_PenaltyManagement_c_regularStatus_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : PenaltyManagement__c.regularStatus__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_PenaltyManagement_c_regularStatus_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component8_val = new SkyEditor2__SkyEditorDummy__c();	
				Component8_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component8_op = new SkyEditor2.TextHolder();	
				valueOptions_PenaltyManagement_c_penaltyStatus_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : PenaltyManagement__c.penaltyStatus__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_PenaltyManagement_c_penaltyStatus_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				karute__c lookupObjComponent30karutec = new karute__c();	
				Component12_val = lookupObjComponent30karutec;	
				Component12_op = new SkyEditor2.TextHolder();	
					
				Component14_val = new SkyEditor2__SkyEditorDummy__c();	
				Component14_op = new SkyEditor2.TextHolder();	
					
				Component10_val = lookupObjComponent30karutec;	
				Component10_op = new SkyEditor2.TextHolder();	
					
				queryMap.put(	
					'Component3',	
					new SkyEditor2.Query('PenaltyManagement__c')
						.addField('karute__c')
						.addField('date__c')
						.addField('regularStatus__c')
						.addField('penaltyStatus__c')
						.addFieldAsOutput('karute__r.AccountName__c')
						.addFieldAsOutput('karute__r.URL__c')
						.addFieldAsOutput('karute__r.OwnerId')
						.addField('detail__c')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component15_from, 'SkyEditor2__Date__c', 'date__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component15_to, 'SkyEditor2__Date__c', 'date__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component15_isNull, 'SkyEditor2__Date__c', 'date__c', Component15_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component6_val_dummy, 'SkyEditor2__Text__c','regularStatus__c', Component6_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component12_val, 'AccountName__c', 'karute__r.AccountName__c',karute__c.fields.AccountName__c, Component12_op, true, false,true,0,false,'karute__c',PenaltyManagement__c.fields.karute__c )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component8_val_dummy, 'SkyEditor2__Text__c','penaltyStatus__c', Component8_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component14_val, 'SkyEditor2__Url__c', 'karute__r.URL__c',karute__c.fields.URL__c, Component14_op, true, false,true,0,false,'karute__c',PenaltyManagement__c.fields.karute__c )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component10_val, 'OwnerId', 'karute__r.OwnerId',karute__c.fields.OwnerId, Component10_op, true, false,true,0,false,'karute__c',PenaltyManagement__c.fields.karute__c )) 
				);	
					
					Component3 = new Component3(new List<PenaltyManagement__c>(), new List<Component3Item>(), new List<PenaltyManagement__c>(), null);
				listItemHolders.put('Component3', Component3);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(PenaltyManagement__c.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = true;
			presetSystemParams();
			Component3.extender = this.extender;
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_PenaltyManagement_c_regularStatus_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_karute_c_AccountName_c() { 
			return getOperatorOptions('karute__c', 'AccountName__c');	
		}	
		public List<SelectOption> getOperatorOptions_PenaltyManagement_c_penaltyStatus_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_karute_c_URL_c() { 
			return getOperatorOptions('karute__c', 'URL__c');	
		}	
		public List<SelectOption> getOperatorOptions_karute_c_OwnerId() { 
			return getOperatorOptions('karute__c', 'OwnerId');	
		}	
			
			
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public PenaltyManagement__c record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, PenaltyManagement__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.ListItemHolder {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (PenaltyManagement__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	public PenaltyManagement__c Component3_table_Conversion { get { return new PenaltyManagement__c();}}
	
	public String Component3_table_selectval { get; set; }
	
	
			
	}