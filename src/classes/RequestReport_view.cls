global with sharing class RequestReport_view extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public CustomObject1__c record {get{return (CustomObject1__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	
	
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	
	{
	setApiVersion(31.0);
	}
	public RequestReport_view(ApexPages.StandardController controller) {
		super(controller);


		SObjectField f;

		f = CustomObject1__c.fields.Name;
		f = CustomObject1__c.fields.Status__c;
		f = CustomObject1__c.fields.Opportunity__c;
		f = CustomObject1__c.fields.RequestDay__c;
		f = CustomObject1__c.fields.OrderNo2__c;
		f = CustomObject1__c.fields.RequestEndDay__c;
		f = CustomObject1__c.fields.OrderNo__c;
		f = CustomObject1__c.fields.RDDay__c;
		f = CustomObject1__c.fields.unit__c;
		f = CustomObject1__c.fields.RecordTypeId;
		f = CustomObject1__c.fields.OwnerId;
		f = CustomObject1__c.fields.SalesSupportPerson__c;
		f = CustomObject1__c.fields.Account__c;
		f = CustomObject1__c.fields.ReportType__c;
		f = CustomObject1__c.fields.MailAddress__c;
		f = CustomObject1__c.fields.MailPerson__c;
		f = CustomObject1__c.fields.Report11__c;
		f = CustomObject1__c.fields.BillTitle__c;
		f = CustomObject1__c.fields.TOorCC11__c;
		f = CustomObject1__c.fields.ReportType2__c;
		f = CustomObject1__c.fields.MailAddress2__c;
		f = CustomObject1__c.fields.MailPerson2__c;
		f = CustomObject1__c.fields.Report22__c;
		f = CustomObject1__c.fields.BillTitle12__c;
		f = CustomObject1__c.fields.TOorCC22__c;
		f = CustomObject1__c.fields.ReportType3__c;
		f = CustomObject1__c.fields.MailAddress3__c;
		f = CustomObject1__c.fields.MailPerson3__c;
		f = CustomObject1__c.fields.Report33__c;
		f = CustomObject1__c.fields.BillTitle13__c;
		f = CustomObject1__c.fields.TOorCC33__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = CustomObject1__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(CustomObject1__c.SObjectType);
			
			mainQuery = new SkyEditor2.Query('CustomObject1__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('Status__c');
			mainQuery.addFieldAsOutput('Opportunity__c');
			mainQuery.addFieldAsOutput('RequestDay__c');
			mainQuery.addFieldAsOutput('OrderNo2__c');
			mainQuery.addFieldAsOutput('RequestEndDay__c');
			mainQuery.addFieldAsOutput('OrderNo__c');
			mainQuery.addFieldAsOutput('RDDay__c');
			mainQuery.addFieldAsOutput('unit__c');
			mainQuery.addFieldAsOutput('RecordType.Name');
			mainQuery.addFieldAsOutput('OwnerId');
			mainQuery.addFieldAsOutput('SalesSupportPerson__c');
			mainQuery.addFieldAsOutput('Account__c');
			mainQuery.addFieldAsOutput('ReportType__c');
			mainQuery.addFieldAsOutput('MailAddress__c');
			mainQuery.addFieldAsOutput('MailPerson__c');
			mainQuery.addFieldAsOutput('Report11__c');
			mainQuery.addFieldAsOutput('BillTitle__c');
			mainQuery.addFieldAsOutput('TOorCC11__c');
			mainQuery.addFieldAsOutput('ReportType2__c');
			mainQuery.addFieldAsOutput('MailAddress2__c');
			mainQuery.addFieldAsOutput('MailPerson2__c');
			mainQuery.addFieldAsOutput('Report22__c');
			mainQuery.addFieldAsOutput('BillTitle12__c');
			mainQuery.addFieldAsOutput('TOorCC22__c');
			mainQuery.addFieldAsOutput('ReportType3__c');
			mainQuery.addFieldAsOutput('MailAddress3__c');
			mainQuery.addFieldAsOutput('MailPerson3__c');
			mainQuery.addFieldAsOutput('Report33__c');
			mainQuery.addFieldAsOutput('BillTitle13__c');
			mainQuery.addFieldAsOutput('TOorCC33__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			
			if (record.Id == null) {
				
				saveOldValues();
				
				if(record.RecordTypeId == null) recordTypeSelector.applyDefault(record);
				
			}

			
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}