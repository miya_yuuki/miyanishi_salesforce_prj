global with sharing class syoudanngamen_sub extends SkyEditor2.SkyEditorPageBaseWithSharing{
    public PricebookEntry record{get;set;}
    public Component9 Component9 {get; private set;}
    public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
    public SkyEditor2__SkyEditorDummy__c Component3{get;set;}
    public SkyEditor2__SkyEditorDummy__c Component5{get;set;}
    Map<String, String> parameters;
    Id pricebook2Id;
    public syoudanngamen_sub(ApexPages.StandardController controller){
        super(controller);
        SObjectField f;
        f = PricebookEntry.fields.Name;
        f = PricebookEntry.fields.ProductCode;
        f = PricebookEntry.fields.UnitPrice;
        f = PricebookEntry.fields.UseStandardPrice;
        try {
            parameters = ApexPages.currentPage().getParameters();
            pricebook2Id = parameters.get('pbid');
            mainRecord = null;
            mainSObjectType = PricebookEntry.SObjectType;
            mode = SkyEditor2.LayoutMode.TempProductLookup_01;
            Component3 = new SkyEditor2__SkyEditorDummy__c();
            Component5 = new SkyEditor2__SkyEditorDummy__c();
            queryMap.put(
                'Component9',
                new SkyEditor2.Query('PricebookEntry')
                    .addFieldAsOutput('Name')
                    .addFieldAsOutput('ProductCode')
                    .addFieldAsOutput('UnitPrice')
                    .addField('UnitPrice')
                    .addField('Name')
                    .addField('Id')
                    .addField('Product2.Description')
                    .limitRecords(500)
                    .addListener(new SkyEditor2.QueryWhereRegister(Component3, 'SkyEditor2__Text__c', 'Name', new SkyEditor2.TextHolder('co'), false, true, false))
                    .addListener(new SkyEditor2.QueryWhereRegister(Component5, 'SkyEditor2__Text__c', 'ProductCode', new SkyEditor2.TextHolder('co'), false, true, false))
                     .addWhere('Pricebook2Id',(String)pricebook2Id,SkyEditor2.WhereOperator.Eq)
                     .addWhere('AND IsActive = true')
            );
            Component9 = new Component9(new List<PricebookEntry>(), new List<Component9Item>(), new List<PricebookEntry>(), null);
            listItemHolders.put('Component9', Component9);
            recordTypeSelector = new SkyEditor2.RecordTypeSelector(PricebookEntry.SObjectType);
            doSearch();
        } catch (SkyEditor2.Errors.SObjectNotFoundException e) {
            SkyEditor2.Messages.addErrorMessage(e.getMessage());
        } catch (SkyEditor2.Errors.FieldNotFoundException e) {
            SkyEditor2.Messages.addErrorMessage(e.getMessage());
        } catch (SkyEditor2.ExtenderException e){
            e.setMessagesToPage();
        } catch (SkyEditor2.Errors.PricebookNotFoundException e) {
            SkyEditor2.Messages.addErrorMessage(e.getMessage());
            hidePageBody = true;
        }
    }
    public List<SelectOption> getOperatorOptions_PricebookEntry_Name() {
        return getOperatorOptions('PricebookEntry', 'Name');
    }
    public List<SelectOption> getOperatorOptions_PricebookEntry_ProductCode() {
        return getOperatorOptions('PricebookEntry', 'ProductCode');
    }
    @isTest(SeeAllData=true)
    private static void testPageMethods() {
        try {
            syoudanngamen_sub page = new syoudanngamen_sub(new ApexPages.StandardController(new PricebookEntry()));
            page.getOperatorOptions_PricebookEntry_Name();
            page.getOperatorOptions_PricebookEntry_ProductCode();
        } catch (Exception e) {
            System.debug(LoggingLevel.WARN, e);
        }
		System.assert(true);
    }
    global class Component9Item extends SkyEditor2.ListItem {
        public PricebookEntry record{get; private set;}
        Component9Item(Component9 holder, PricebookEntry record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(holder);
            if (record.Id == null ){
                if (recordTypeSelector != null) {
                    recordTypeSelector.applyDefault(record);
                }
                
            }
            this.record = record;
        }
        global override SObject getRecord() {return record;}
    }
    global class Component9 extends SkyEditor2.ListItemHolder {
        public List<Component9Item> items{get; private set;}
        Component9(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(records, items, deleteRecords, recordTypeSelector);
            this.items = (List<Component9Item>)items;
        }
        global override SkyEditor2.ListItem create(SObject data) {
            return new Component9Item(this, (PricebookEntry)data, recordTypeSelector);
        }
    }
    private static testMethod void testComponent9() {
        Component9 Component9 = new Component9(new List<PricebookEntry>(), new List<Component9Item>(), new List<PricebookEntry>(), null);
        Component9.create(new PricebookEntry());
		System.assert(true);
    }
}