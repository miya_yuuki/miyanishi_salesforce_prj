public with sharing class CancellationRecordsCheck {
    public static boolean firstRun = true;
    
    public static void prcGetCancellationRecord(List<CustomObject1__c> objRequest) {
        // 契約内容申請用レコード 
        List<SEOChange__c> objCancel = new List<SEOChange__c>();
        // 注文商品レコード
        List<OrderItem__c> objOrderItem = new List<OrderItem__c>();
        // 登録する注文商品
        List<SEOChange__c> insCancelList = new List<SEOChange__c>();

        for (CustomObject1__c obj: objRequest) {
            // 解約申請以外はスキップ
            if (obj.RecordtypeId != '01210000000AQRB') continue;
            // 申請前じゃなければスキップ （「差戻し」も更新しない）
            if (obj.Status__c != '申請前' && obj.Status__c != '申請者キャンセル' && obj.Status__c != '') continue;
        
            String strCancelId = String.valueOf(obj.Id);                 // 申請（案件）ID
            String strOrderId = String.valueOf(obj.OrderNo__c);          // 解約注文ID
            String CancelMode = String.valueOf(obj.CancelType2__c);      // 解約種別
            Date CancelDay = Date.valueOf(obj.ClauseDay__c);             // 解約日
            Boolean AutoComp = obj.OrderItem_AutoCompletion__c;          // 解約対象注文商品 自動補完
            System.debug('REQUEST_INFO :::: strCancelId: ' + strCancelId + ', strOrderId: ' + strOrderId + ', CancelMode: ' + CancelMode + ', CancelDay: ' + CancelDay);
            
            // 自動補完ONの時のみ実施
            if (!AutoComp) continue;

            // 注文配下の解約対象注文商品を取得
            objOrderItem = prcTargetCandidateOI(strOrderId, CancelMode, CancelDay);
            System.debug('prcTargetCandidateOI_Result: ' + objOrderItem.size() + ' :::: strOrderId: ' + strOrderId + ', CancelMode: ' + CancelMode + ', CancelDay: ' + CancelDay);                if (objOrderItem.size() > 0) {
                for (OrderItem__c objOI: objOrderItem) {
                    // INSERTするデータを整える
                    SEOChange__c insCancel = new SEOChange__c(
                        OrderItem__c = objOI.Id,                                // 注文商品ID
                        sinsei__c = strCancelId,                                // 申請（案件）ID
                        ProductName2__c = objOI.Item2_Product__r.Name,          // 商品名
                        Amount2__c = objOI.TotalPrice__c,                       // 契約金額
                        CancelableAmount__c = objOI.CancelableAmount__c,        // 解約額
                        Item2_ServiceDate__c = objOI.Item2_ServiceDate__c,      // 請求対象開始日
                        Item2_EndData__c = objOI.Item2_EndData__c,              // 請求対象終了日
                        KeywordType1__c = objOI.OrderItem_keywordType__c,       // キーワード種別
                        Keyword2__c = objOI.Item2_Keyword_strategy_keyword__c,  // キーワード
                        URL2__c = objOI.OrderItem_URL__c                        // URL
                    );
                    System.debug('OrderItem_INFO / INSERT :::: insChangeOI: ' + objOI.Id + ' insChangeCancel:' + strCancelId);
                    insCancelList.add(insCancel);
                }
            }

            // 登録済みの契約内容申請用レコードを全件削除 → 登録し直し
            objCancel = [SELECT Id, OrderItem__c FROM SEOChange__c WHERE sinsei__c = :obj.Id];
            if (objCancel.size() > 0) delete objCancel;
            if (insCancelList.size() > 0) insert insCancelList;
        }
    }

    private static List<OrderItem__c> prcTargetCandidateOI(String OrderId, String strMode, Date CancelDay) {
        // 注文商品レコード
        List<OrderItem__c> insOI = new List<OrderItem__c>();

        if (strMode != '途中解約') {
            insOI = [
                SELECT
                    Id,
                    Item2_Product__r.Name,
                    TotalPrice__c,
                    CancelableAmount__c,
                    Item2_ServiceDate__c,
                    Item2_EndData__c,
                    OrderItem_keywordType__c,
                    Item2_Keyword_strategy_keyword__c,
                    OrderItem_URL__c
                FROM OrderItem__c
                WHERE
                    Item2_Relation__c = :OrderId
                    AND Item2_Keyword_phase__c NOT IN ('途中解約', '途中解約（請求）', '契約変更', '取消', '売上取消')
            ];
        }
        else {
            // 中途解約は条件追加
            insOI = [
                SELECT
                    Id,
                    Item2_Product__r.Name,
                    TotalPrice__c,
                    CancelableAmount__c,
                    Item2_ServiceDate__c,
                    Item2_EndData__c,
                    OrderItem_keywordType__c,
                    Item2_Keyword_strategy_keyword__c,
                    OrderItem_URL__c
                FROM OrderItem__c
                WHERE 
                    Item2_Relation__c = :OrderId
                    AND Item2_Keyword_phase__c NOT IN ('途中解約', '途中解約（請求）', '契約変更', '取消', '売上取消', '契約期間なし')
                    AND CancelableAmount__c != 0
                    AND Item2_EndData__c >= :CancelDay
                    AND (Item2_DeliveryExistence__c = '無' OR (Item2_DeliveryExistence__c = '有' AND ProductionStatus__c != '社外納品済み'))
            ];
        }

        return insOI;
    }
}