@isTest
private class TelephoneAppointmentDaySetTest {
    static testMethod void myUnitTest() {
        Test.startTest();

        // リード
        Lead testLead = new Lead(
            OwnerId = Userinfo.getUserId(),
            Company = 'テスト会社名',
            lead_AccountKana__c = 'フリガナ',
            Phone = '03-9999-9999',
            Status1__c = '接続済',
            Status2__c = '担当者資料送付(メール)',
            Website = 'http://www.test.com',
            lead_Relation__c = '顧客',
            lead_lead1__c = '架電',
            lead_lead2__c = '架電',
            LastName = 'テスト姓',
            lead_NextContactDay__c = System.today(),
            Lead_WEBsite_Quality__c = 'A',
            WebSiteType__c = '⑤その他',
            SEOMeasureStatus__c = '内製'
        );
        insert testLead;

        // テレアポ
        task__c testTelappo = new task__c(
            lead__c = testLead.Id,
            Type__c = '架電',
            PhaseBig__c = '接続済',
            PhaseSmall__c = '担当者資料送付(メール)',
            PhoneA__c = System.now()
        );
        insert testTelappo;
    }
}