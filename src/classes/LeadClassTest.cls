@isTest
public with sharing class LeadClassTest {
    private static testMethod void testMethods()
    {
        Lead testLead = new Lead(
            OwnerId = Userinfo.getUserId(),
            Company = 'テスト会社名',
            lead_AccountKana__c = 'フリガナ',
            Phone = '03-9999-9999',
            Status1__c = '接続済',
            Status2__c = '担当者資料送付(メール)',
            Website = 'http://www.test.com',
            lead_Relation__c = '顧客',
            lead_lead1__c = '架電',
            lead_lead2__c = '架電',
            LastName = 'テスト姓',
            lead_NextContactDay__c = System.today(),
            Lead_WEBsite_Quality__c = 'A',
            WebSiteType__c = '⑤その他',
            SEOMeasureStatus__c = '内製'
        );
        insert testLead;

        task__c testTelapo = new task__c(
            lead__c = testLead.Id,
            Type__c = '架電',
            PhaseBig__c = '接続済',
            PhaseSmall__c = '担当者資料送付(メール)',
            PhoneA__c = System.now()
        );
        insert testTelapo;

        // Database.LeadConvertオブジェクトに
        // insertしたリードのIdをセット
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(testLead.id);

        // リードステータスのうち、取引開始済みのものを全部取得してくる
        List<LeadStatus> convertStatusList = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted = true];
        Database.LeadConvertResult lcr;
        for (LeadStatus convertStatus : convertStatusList) {
            try {
                lc.setConvertedStatus(convertStatus.masterLabel);
                lc.setDoNotCreateOpportunity(false); // 商談オブジェクトを作成しない
                // 取引開始
                lcr = Database.convertLead(lc);
                break;
            } catch (DMLException e) {
                // DMLExceptionが発生したら次のリードステータスを使う
            }
        }
    }
}