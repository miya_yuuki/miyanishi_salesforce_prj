global class MaturityCloseCheck {
	global static PageReference prcMaturityCloseCheck() {
		
		List<OrderItem__c> updOrderItem = [
			SELECT Id, Item2_Keyword_phase__c
			FROM OrderItem__c
			WHERE MaturityClose__c = true
			AND Item2_Keyword_phase__c IN ('契約中', '請求停止')
			AND Item2_Automatic_updating1__c = '有'
			AND EndDate__c != null
			AND EndDate__c < TODAY
			limit 300
		];
		
		if (updOrderItem.size() > 0) {
			System.debug('満了解約：「契約終了」フェーズ更新対象' + updOrderItem.size() + '件');
			for (OrderItem__c obj : updOrderItem) {
				obj.Item2_Keyword_phase__c = '契約終了';
			}
			update updOrderItem;
		}
		else {
			System.debug('満了解約：「契約終了」フェーズ更新対象なし');
		}
		
		return null;
	}
}