global class ScheduleAutoUpdateClass implements Schedulable {
	// 注文商品オブジェクト
	public OrderItem__c prmOderItem { get; set; }
	
    //1回のexecuteメソッドで処理される件数
    private final Integer BATCH_SIZE = 10;
    
    // イニシャライズ
    public ScheduleAutoUpdateClass()
    {
    	// 注文商品のインスタンスを生成
    	prmOderItem = new OrderItem__c();
    	// パラメータ日付にシステム日付をセット
    	prmOderItem.EndDate__c = System.today().addDays(-3);
    }
    
    global void execute(SchedulableContext sc)
    {
        //どのオブジェクトのレコードに対し処理を実行するのかSOQLを投げる
        BatchAutoUpdateClass batch = new BatchAutoUpdateClass(System.today(), null);
        Database.executeBatch(batch, BATCH_SIZE);
    }
    
    //バッチ実行
    public PageReference doStartBatch() 
    {    	
        //どのオブジェクトのレコードに対し処理を実行するのかSOQLを投げる
        BatchAutoUpdateClass batch = new BatchAutoUpdateClass(prmOderItem.EndDate__c, prmOderItem.Item2_Relation__c);
        Database.executeBatch(batch, BATCH_SIZE);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '契約更新バッチを実行しました'));
        return null;
    }
}