public class Notice2SlackController {
    
	public static boolean firstRun = true;

    @future (callout=true)
    public static void createMessage(Map<String, String> request) {

        HttpRequest req = new HttpRequest();

        req.setMethod('POST');

        // Set Callout timeout
        // default: 10 secs(that often causes "System.CalloutException: Read timed out")
        req.setTimeout(60000);

        // Set HTTPRequest header properties
        req.setHeader('Content-Type','application/json');
        req.setEndpoint('https://hooks.slack.com/services/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');

        System.debug(LoggingLevel.INFO, 'REQEST_BODY:' + JSON.serialize(request));

       req.setBody(JSON.serialize(request));

        Http http = new Http();

        try {
            // Execute Http Callout
            HTTPResponse res = http.send(req);

            System.debug(LoggingLevel.INFO, res.toString());
            System.debug(LoggingLevel.INFO, 'STATUS:' + res.getStatus());
            System.debug(LoggingLevel.INFO, 'STATUS_CODE:' + res.getStatusCode());
            System.debug(LoggingLevel.INFO, 'BODY:' + res.getBody());

        } catch(System.CalloutException e) {
            // Exception handling goes here....
            System.debug(LoggingLevel.INFO, e);
        }
    }
}