public with sharing class OpportunityProductDistributionUpdate {
	public static boolean firstRun = true;

	public static void prcDistributionUpdate(List<Opportunity> objOpportunity, Map<Id, Opportunity> oldMap, Boolean upd) {
		// 商談ID
		List<String> strOpportunityIds = new List<String>();
		// 商談商品オブジェクト
		List<OpportunityProduct__c> updOpportunityProduct = new List<OpportunityProduct__c>();
		
		// 更新対象のチェック
		for (Opportunity obj : objOpportunity) {
			if (prcCheckDistribution(upd, obj, oldMap) == null) {
				continue;
			}
			else {
				strOpportunityIds.add(obj.Id);
			}
		}
		
		updOpportunityProduct = [
			SELECT Id, Opportunity__r.Owner.Name, Opportunity__r.Owner.team__c, SalesPersonMain__c, SalesPersonMainUnit__c, SalesPercentage1__c
			FROM OpportunityProduct__c
			WHERE Opportunity__c IN :strOpportunityIds
		];
		for (OpportunityProduct__c objProduct : updOpportunityProduct) {
			objProduct.SalesPersonMain__c = String.valueOf(objProduct.Opportunity__r.Owner.Name);
			objProduct.SalesPersonMainUnit__c = String.valueOf(objProduct.Opportunity__r.Owner.team__c);
			objProduct.SalesPercentage1__c = 100;
			objProduct.SalesPercentage2__c = 0;
		}
		
		if (updOpportunityProduct.size() > 0) {
			update updOpportunityProduct;
		}
	}
	private static Boolean prcCheckDistribution (Boolean upd, Opportunity objNew, Map<Id, Opportunity> oldMap) {
		if (upd) {
			// 古い値を取得
			Opportunity objOld = oldMap.get(objNew.Id);
			// 商談所有者が更新された場合
			if (objNew.OwnerId != objOld.OwnerId && objNew.syouninn__c == null && objOld.syouninn__c == null && objNew.StageName != '受注' && objOld.StageName != '受注' && objNew.StageName != 'デッド' && objOld.StageName != 'デッド') {
				System.debug('商談所有者更新検知' + objNew.Id);
				return true;
			}
		}
		// 条件に合致しない
		return null;
	}
}