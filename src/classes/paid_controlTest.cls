@isTest
private with sharing class paid_controlTest{
		private static testMethod void testPageMethods() {	
			paid_control page = new paid_control(new ApexPages.StandardController(new PaymentManagement__c()));	
			page.getOperatorOptions_PaymentManagement_c_contract_company_c();	
			page.getOperatorOptions_PaymentManagement_c_bill_name_c();	
			page.getOperatorOptions_Bill_c_Bill_PaymentMethod_c();	
			page.getOperatorOptions_Bill_c_Bill_PaymentSiteMaster_c();	

		Integer defaultSize;

		defaultSize = page.Component3.items.size();
		page.Component3.add();
		System.assertEquals(defaultSize + 1, page.Component3.items.size());
		page.Component3.items[defaultSize].selected = true;
		page.Component3.doDeleteSelectedItems();
			System.assert(true);
		}	
			
	private static testMethod void testComponent3() {
		paid_control.Component3 Component3 = new paid_control.Component3(new List<PaymentManagement__c>(), new List<paid_control.Component3Item>(), new List<PaymentManagement__c>(), new SkyEditor2.RecordTypeSelector(PaymentManagement__c.SObjectType));
		Component3.create(new PaymentManagement__c());
		Component3.doDeleteSelectedItems();
		System.assert(true);
	}
	
}