@isTest
private with sharing class LEX_karuteTest{
	private static testMethod void testPageMethods() {		LEX_karute extension = new LEX_karute(new ApexPages.StandardController(new karute__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
		extension.getComponent2164OptionsJS();
		extension.getComponent2167OptionsJS();
		extension.getComponent2170OptionsJS();
		extension.getComponent2173OptionsJS();
	}
	private static testMethod void testComponent4314() {
		LEX_karute.Component4314 Component4314 = new LEX_karute.Component4314(new List<PenaltyManagement__c>(), new List<LEX_karute.Component4314Item>(), new List<PenaltyManagement__c>(), null);
		Component4314.create(new PenaltyManagement__c());
		System.assert(true);
	}
	
	private static testMethod void testComponent2716() {
		LEX_karute.Component2716 Component2716 = new LEX_karute.Component2716(new List<Minutes__c>(), new List<LEX_karute.Component2716Item>(), new List<Minutes__c>(), null);
		Component2716.create(new Minutes__c());
		System.assert(true);
	}
	
	private static testMethod void testComponent3891() {
		LEX_karute.Component3891 Component3891 = new LEX_karute.Component3891(new List<AEGIS__c>(), new List<LEX_karute.Component3891Item>(), new List<AEGIS__c>(), null);
		Component3891.create(new AEGIS__c());
		System.assert(true);
	}
	
	private static testMethod void testComponent3317() {
		LEX_karute.Component3317 Component3317 = new LEX_karute.Component3317(new List<CLOVER__c>(), new List<LEX_karute.Component3317Item>(), new List<CLOVER__c>(), null);
		Component3317.create(new CLOVER__c());
		System.assert(true);
	}
	
	@isTest
	private static void testLightDataTables(){

		System.assert(true);
	}
	private static testMethod void testRecordTypeFullNames() {
		Set<String> result = LEX_karute.recordTypeFullNames(new RecordType[] {
			new RecordType(DeveloperName = 'TestRecordType')
		});
		System.assertEquals(result.size(), 1);
		System.assert(result.contains('TestRecordType'));
	}
	
	private static testMethod void testFilterMetadataJSON() {
		String json = '{"CustomObject":{"recordTypes":[{"fullName":"RecordType1","picklistValues":[]},{"fullName":"RecordType2","picklistValues":[]}]}}';		Set<String> recordTypeSet = new Set<String>();
		recordTypeSet.add('RecordType2');
		Object metadata = System.JSON.deserializeUntyped(json);
		Map<String, Object> data = (Map<String, Object>) LEX_karute.filterMetadataJSON(metadata, recordTypeSet, SkyEditor2__SkyEditorDummy__c.SObjectType).data;
		Map<String, Object> customObject = (Map<String, Object>) data.get('CustomObject');
		List<Object> recordTypes = (List<Object>) customObject.get('recordTypes');
		System.assertEquals(recordTypes.size(), 1);
		Map<String, Object> recordType = (Map<String, Object>) recordTypes[0];
		System.assertEquals('RecordType2', recordType.get('fullName'));
	}


	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component13() {
		String testReferenceId = '';
		LEX_karute extension = new LEX_karute(new ApexPages.StandardController(new karute__c()));
		extension.loadReferenceValues_Component13();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(Account.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<Account> parents = [SELECT Id FROM Account LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		Account parent = [SELECT Id,account_CreditLevel__c,account_CreditDay__c FROM Account WHERE Id = :testReferenceId];
		extension.record.AccountName__c = parent.Id;
		extension.loadReferenceValues_Component13();
				
		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.CreditLevel1__c)) {
			System.assertEquals(parent.account_CreditLevel__c, extension.record.CreditLevel1__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.CreditDay1__c)) {
			System.assertEquals(parent.account_CreditDay__c, extension.record.CreditDay1__c);
		}

		System.assert(true);
	}

	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component4729() {
		String testReferenceId = '';
		LEX_karute extension = new LEX_karute(new ApexPages.StandardController(new karute__c()));
		extension.loadReferenceValues_Component4729();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(Opportunity.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<Opportunity> parents = [SELECT Id FROM Opportunity LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		Opportunity parent = [SELECT Id,ServiceWEBSite__c,SEOBudgetPerMonth__c,Products__c,ProposalProducts__c FROM Opportunity WHERE Id = :testReferenceId];
		extension.record.Opportunity__c = parent.Id;
		extension.loadReferenceValues_Component4729();
				
		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.URL__c)) {
			System.assertEquals(parent.ServiceWEBSite__c, extension.record.URL__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.SEOBudget__c)) {
			System.assertEquals(parent.SEOBudgetPerMonth__c, extension.record.SEOBudget__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.Products__c)) {
			System.assertEquals(parent.Products__c, extension.record.Products__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.ProposalProducts__c)) {
			System.assertEquals(parent.ProposalProducts__c, extension.record.ProposalProducts__c);
		}

		System.assert(true);
	}

	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component2862() {
		String testReferenceId = '';
		LEX_karute extension = new LEX_karute(new ApexPages.StandardController(new karute__c()));
		extension.loadReferenceValues_Component2862();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(Contact.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<Contact> parents = [SELECT Id FROM Contact LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		Contact parent = [SELECT Id,Department,Title,Phone,MobilePhone__c,Email,WEBLiteracy__c,HumanType__c,Contact_Role__c,Contact_Type__c FROM Contact WHERE Id = :testReferenceId];
		extension.record.CustomerName__c = parent.Id;
		extension.loadReferenceValues_Component2862();
				
		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.Division__c)) {
			System.assertEquals(parent.Department, extension.record.Division__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.Title10__c)) {
			System.assertEquals(parent.Title, extension.record.Title10__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.Phone1__c)) {
			System.assertEquals(parent.Phone, extension.record.Phone1__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.Mobile__c)) {
			System.assertEquals(parent.MobilePhone__c, extension.record.Mobile__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.Mail1__c)) {
			System.assertEquals(parent.Email, extension.record.Mail1__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.WebLiteracy__c)) {
			System.assertEquals(parent.WEBLiteracy__c, extension.record.WebLiteracy__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.HumanType__c)) {
			System.assertEquals(parent.HumanType__c, extension.record.HumanType__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.BillRole__c)) {
			System.assertEquals(parent.Contact_Role__c, extension.record.BillRole__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.Type__c)) {
			System.assertEquals(parent.Contact_Type__c, extension.record.Type__c);
		}

		System.assert(true);
	}

	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component2865() {
		String testReferenceId = '';
		LEX_karute extension = new LEX_karute(new ApexPages.StandardController(new karute__c()));
		extension.loadReferenceValues_Component2865();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(Contact.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<Contact> parents = [SELECT Id FROM Contact LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		Contact parent = [SELECT Id,Department,Title,Phone,MobilePhone__c,Email,WEBLiteracy__c,HumanType__c,Contact_Role__c,Contact_Type__c FROM Contact WHERE Id = :testReferenceId];
		extension.record.CustomerName2__c = parent.Id;
		extension.loadReferenceValues_Component2865();
				
		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.Division2__c)) {
			System.assertEquals(parent.Department, extension.record.Division2__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.Title20__c)) {
			System.assertEquals(parent.Title, extension.record.Title20__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.Phone2__c)) {
			System.assertEquals(parent.Phone, extension.record.Phone2__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.Mobile2__c)) {
			System.assertEquals(parent.MobilePhone__c, extension.record.Mobile2__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.Mail2__c)) {
			System.assertEquals(parent.Email, extension.record.Mail2__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.WebLiteracy2__c)) {
			System.assertEquals(parent.WEBLiteracy__c, extension.record.WebLiteracy2__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.HumanType2__c)) {
			System.assertEquals(parent.HumanType__c, extension.record.HumanType2__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.BillRole2__c)) {
			System.assertEquals(parent.Contact_Role__c, extension.record.BillRole2__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.Type2__c)) {
			System.assertEquals(parent.Contact_Type__c, extension.record.Type2__c);
		}

		System.assert(true);
	}

	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component2868() {
		String testReferenceId = '';
		LEX_karute extension = new LEX_karute(new ApexPages.StandardController(new karute__c()));
		extension.loadReferenceValues_Component2868();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(Contact.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<Contact> parents = [SELECT Id FROM Contact LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		Contact parent = [SELECT Id,Department,Title,Phone,MobilePhone__c,Email,WEBLiteracy__c,HumanType__c,Contact_Role__c,Contact_Type__c FROM Contact WHERE Id = :testReferenceId];
		extension.record.CustomerName30__c = parent.Id;
		extension.loadReferenceValues_Component2868();
				
		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.Division3__c)) {
			System.assertEquals(parent.Department, extension.record.Division3__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.Title30__c)) {
			System.assertEquals(parent.Title, extension.record.Title30__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.Phone3__c)) {
			System.assertEquals(parent.Phone, extension.record.Phone3__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.Mobile3__c)) {
			System.assertEquals(parent.MobilePhone__c, extension.record.Mobile3__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.Mail3__c)) {
			System.assertEquals(parent.Email, extension.record.Mail3__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.WebLiteracy3__c)) {
			System.assertEquals(parent.WEBLiteracy__c, extension.record.WebLiteracy3__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.HumanType3__c)) {
			System.assertEquals(parent.HumanType__c, extension.record.HumanType3__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.BillRole3__c)) {
			System.assertEquals(parent.Contact_Role__c, extension.record.BillRole3__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, karute__c.fields.Type3__c)) {
			System.assertEquals(parent.Contact_Type__c, extension.record.Type3__c);
		}

		System.assert(true);
	}
}