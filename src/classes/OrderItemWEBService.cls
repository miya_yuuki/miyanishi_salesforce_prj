global with sharing class OrderItemWEBService extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public OrderItem__c record {get{return (OrderItem__c)mainRecord;}}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	{
	setApiVersion(42.0);
	}
	public OrderItemWEBService(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = OrderItem__c.fields.Link_OrderItem__c;
		f = OrderItem__c.fields.Name;
		f = OrderItem__c.fields.Item2_Keyword_phase__c;
		f = OrderItem__c.fields.Item2_Relation__c;
		f = OrderItem__c.fields.Item2_Unit_selling_price__c;
		f = OrderItem__c.fields.Item2_ProductCount__c;
		f = OrderItem__c.fields.Quantity__c;
		f = OrderItem__c.fields.ProductCode1__c;
		f = OrderItem__c.fields.BuyingupPrice__c;
		f = OrderItem__c.fields.Item2_Product__c;
		f = OrderItem__c.fields.TotalPrice__c;
		f = OrderItem__c.fields.Item2_ProductType1__c;
		f = OrderItem__c.fields.ContractMonths__c;
		f = OrderItem__c.fields.Item2_DeliveryExistence__c;
		f = OrderItem__c.fields.Item2_Commission_rate__c;
		f = OrderItem__c.fields.BillingTiming__c;
		f = OrderItem__c.fields.linkPerformingPrice__c;
		f = OrderItem__c.fields.AutomaticUpdate__c;
		f = OrderItem__c.fields.makeLinkStartMonth__c;
		f = OrderItem__c.fields.consultingSalesEquivalent__c;
		f = OrderItem__c.fields.SubmissionWaitingAmount__c;
		f = OrderItem__c.fields.Salesforce_ID__c;
		f = OrderItem__c.fields.RecordTypeId;
		f = OrderItem__c.fields.syouninn__c;
		f = OrderItem__c.fields.Item2_Entry_date__c;
		f = OrderItem__c.fields.Item2_Special_instruction__c;
		f = OrderItem__c.fields.ServiceDate__c;
		f = OrderItem__c.fields.EndDateCheck__c;
		f = OrderItem__c.fields.EndDate__c;
		f = OrderItem__c.fields.Item2_Billing_Stop_Start_Date__c;
		f = OrderItem__c.fields.Item2_Billing_Stop_End_Date__c;
		f = OrderItem__c.fields.Item2_ServiceDate__c;
		f = OrderItem__c.fields.Item2_EndData__c;
		f = OrderItem__c.fields.Item2_CancellationDay__c;
		f = OrderItem__c.fields.ChangeDay__c;
		f = OrderItem__c.fields.CancelDay__c;
		f = OrderItem__c.fields.seo_product_First_update_month__c;
		f = OrderItem__c.fields.RecordCreateType__c;
		f = OrderItem__c.fields.ProductionStatus__c;
		f = OrderItem__c.fields.Item2_DeliveryNumber__c;
		f = OrderItem__c.fields.Item2_DeliveryDate__c;
		f = OrderItem__c.fields.DeliveryCount__c;
		f = OrderItem__c.fields.Item2_Delivery_date__c;
		f = OrderItem__c.fields.DeliveryCountTotal__c;
		f = OrderItem__c.fields.SalesUnit1View__c;
		f = OrderItem__c.fields.SalesUnitSubView__c;
		f = OrderItem__c.fields.SalesPerson1__c;
		f = OrderItem__c.fields.SalesPersonSub__c;
		f = OrderItem__c.fields.SalesPercentage1__c;
		f = OrderItem__c.fields.SalesPercentage2__c;
		f = OrderItem__c.fields.ContractGetPersonUnit__c;
		f = OrderItem__c.fields.ContractGetPersonUnit2__c;
		f = OrderItem__c.fields.ContractGetPerson1__c;
		f = OrderItem__c.fields.ContractGetPerson2__c;
		f = OrderItem__c.fields.ContractGetPersonPercentage1__c;
		f = OrderItem__c.fields.ContractGetPersonPercentage2__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = OrderItem__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'OrderItemWEBService';
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(OrderItem__c.SObjectType);
			mainQuery = new SkyEditor2.Query('OrderItem__c');
			mainQuery.addField('Item2_Keyword_phase__c');
			mainQuery.addField('Item2_Relation__c');
			mainQuery.addField('Item2_Unit_selling_price__c');
			mainQuery.addField('Item2_ProductCount__c');
			mainQuery.addField('Quantity__c');
			mainQuery.addField('BuyingupPrice__c');
			mainQuery.addField('Item2_Product__c');
			mainQuery.addField('ContractMonths__c');
			mainQuery.addField('BillingTiming__c');
			mainQuery.addField('linkPerformingPrice__c');
			mainQuery.addField('AutomaticUpdate__c');
			mainQuery.addField('makeLinkStartMonth__c');
			mainQuery.addField('consultingSalesEquivalent__c');
			mainQuery.addField('syouninn__c');
			mainQuery.addField('Item2_Entry_date__c');
			mainQuery.addField('Item2_Special_instruction__c');
			mainQuery.addField('ServiceDate__c');
			mainQuery.addField('EndDateCheck__c');
			mainQuery.addField('Item2_Billing_Stop_Start_Date__c');
			mainQuery.addField('Item2_Billing_Stop_End_Date__c');
			mainQuery.addField('Item2_ServiceDate__c');
			mainQuery.addField('Item2_EndData__c');
			mainQuery.addField('Item2_CancellationDay__c');
			mainQuery.addField('ChangeDay__c');
			mainQuery.addField('CancelDay__c');
			mainQuery.addField('seo_product_First_update_month__c');
			mainQuery.addField('RecordCreateType__c');
			mainQuery.addField('ProductionStatus__c');
			mainQuery.addField('Item2_DeliveryDate__c');
			mainQuery.addField('Item2_Delivery_date__c');
			mainQuery.addField('SalesPerson1__c');
			mainQuery.addField('SalesPersonSub__c');
			mainQuery.addField('SalesPercentage1__c');
			mainQuery.addField('SalesPercentage2__c');
			mainQuery.addField('ContractGetPersonUnit__c');
			mainQuery.addField('ContractGetPersonUnit2__c');
			mainQuery.addField('ContractGetPerson1__c');
			mainQuery.addField('ContractGetPerson2__c');
			mainQuery.addField('ContractGetPersonPercentage1__c');
			mainQuery.addField('ContractGetPersonPercentage2__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Link_OrderItem__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('ProductCode1__c');
			mainQuery.addFieldAsOutput('TotalPrice__c');
			mainQuery.addFieldAsOutput('Item2_ProductType1__c');
			mainQuery.addFieldAsOutput('Item2_DeliveryExistence__c');
			mainQuery.addFieldAsOutput('Item2_Commission_rate__c');
			mainQuery.addFieldAsOutput('SubmissionWaitingAmount__c');
			mainQuery.addFieldAsOutput('Salesforce_ID__c');
			mainQuery.addFieldAsOutput('RecordType.Name');
			mainQuery.addFieldAsOutput('EndDate__c');
			mainQuery.addFieldAsOutput('Item2_DeliveryNumber__c');
			mainQuery.addFieldAsOutput('DeliveryCount__c');
			mainQuery.addFieldAsOutput('DeliveryCountTotal__c');
			mainQuery.addFieldAsOutput('SalesUnit1View__c');
			mainQuery.addFieldAsOutput('SalesUnitSubView__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('Item2_Relation__c', 'CF00N10000005jNKn_lkid');
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			if (record.Id == null) {
				saveOldValues();
				if(record.RecordTypeId == null) recordTypeSelector.applyDefault(record);
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}