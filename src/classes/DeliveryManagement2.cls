global with sharing class DeliveryManagement2 extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public TextManagement2__c record{get;set;}	
			
	
		public Component2 Component2 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component76_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component76_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component76_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component76_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component215_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component215_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component215_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component215_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component233_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component233_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component233_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component233_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component188_val {get;set;}	
		public SkyEditor2.TextHolder Component188_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component192_val {get;set;}	
		public SkyEditor2.TextHolder Component192_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component190_val {get;set;}	
		public SkyEditor2.TextHolder Component190_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component237_val {get;set;}	
		public SkyEditor2.TextHolder Component237_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component133_val {get;set;}	
		public SkyEditor2.TextHolder Component133_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component11_val {get;set;}	
		public SkyEditor2.TextHolder Component11_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component15_val {get;set;}	
		public SkyEditor2.TextHolder Component15_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component17_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component17_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component17_op{get;set;}	
		public List<SelectOption> valueOptions_TextManagement2_c_CloudStatus_c_multi {get;set;}
			
		public TextManagement2__c Component122_val {get;set;}	
		public SkyEditor2.TextHolder Component122_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component125_val {get;set;}	
		public SkyEditor2.TextHolder Component125_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component128_val {get;set;}	
		public SkyEditor2.TextHolder Component128_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component178_val {get;set;}	
		public SkyEditor2.TextHolder Component178_op{get;set;}	
			
	public String recordTypeRecordsJSON_TextManagement2_c {get; private set;}
	public String defaultRecordTypeId_TextManagement2_c {get; private set;}
	public String metadataJSON_TextManagement2_c {get; private set;}
	public Boolean QueryPagingConfirmationSetting {get;set;}
	{
	setApiVersion(31.0);
	}
		public DeliveryManagement2(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = TextManagement2__c.fields.flagPlatinum__c;
		f = TextManagement2__c.fields.flagQuestionnaire__c;
		f = TextManagement2__c.fields.flagLighting__c;
		f = TextManagement2__c.fields.flagExpert__c;
		f = TextManagement2__c.fields.Account1__c;
		f = TextManagement2__c.fields.Product__c;
		f = TextManagement2__c.fields.Status__c;
		f = TextManagement2__c.fields.CloudStatus__c;
		f = TextManagement2__c.fields.SalesPerson10__c;
		f = TextManagement2__c.fields.Text_Accep_TedNorm__c;
		f = TextManagement2__c.fields.GroupID__c;
		f = TextManagement2__c.fields.Name;
		f = TextManagement2__c.fields.TextType__c;
		f = TextManagement2__c.fields.SplitDeliveryHopeDate__c;
		f = TextManagement2__c.fields.DeliveryScheduleDay__c;
		f = TextManagement2__c.fields.SalesDeliveryCount__c;
		f = TextManagement2__c.fields.Item2_Keyword_letter_count__c;
		f = TextManagement2__c.fields.CMS__c;
		f = TextManagement2__c.fields.ImageContents__c;
		f = TextManagement2__c.fields.PictureCount__c;
		f = TextManagement2__c.fields.PictureTotal__c;
		f = TextManagement2__c.fields.UserEstimationAmount__c;
		f = TextManagement2__c.fields.OutsourcingEstimationAmount__c;
		f = TextManagement2__c.fields.regulationSheetNo__c;
		f = TextManagement2__c.fields.SalesSupport__c;
		f = TextManagement2__c.fields.CloudBox__c;
		f = TextManagement2__c.fields.Memo__c;
		f = TextManagement2__c.fields.SalesMemo1__c;
 f = TextManagement2__c.fields.DeliveryScheduleDay__c;
 f = TextManagement2__c.fields.SplitDeliveryHopeDate__c;
 f = TextManagement2__c.fields.DeliveryDaySales1__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = TextManagement2__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component76_from = new SkyEditor2__SkyEditorDummy__c();	
				Component76_to = new SkyEditor2__SkyEditorDummy__c();	
				Component76_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component76_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component215_from = new SkyEditor2__SkyEditorDummy__c();	
				Component215_to = new SkyEditor2__SkyEditorDummy__c();	
				Component215_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component215_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component233_from = new SkyEditor2__SkyEditorDummy__c();	
				Component233_to = new SkyEditor2__SkyEditorDummy__c();	
				Component233_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component233_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				TextManagement2__c lookupObjComponent61 = new TextManagement2__c();	
				Component188_val = new SkyEditor2__SkyEditorDummy__c();	
				Component188_op = new SkyEditor2.TextHolder();	
					
				Component192_val = new SkyEditor2__SkyEditorDummy__c();	
				Component192_op = new SkyEditor2.TextHolder();	
					
				Component190_val = new SkyEditor2__SkyEditorDummy__c();	
				Component190_op = new SkyEditor2.TextHolder();	
					
				Component237_val = new SkyEditor2__SkyEditorDummy__c();	
				Component237_op = new SkyEditor2.TextHolder();	
					
				Component133_val = new SkyEditor2__SkyEditorDummy__c();	
				Component133_op = new SkyEditor2.TextHolder();	
					
				Component11_val = new SkyEditor2__SkyEditorDummy__c();	
				Component11_op = new SkyEditor2.TextHolder();	
					
				Component15_val = new SkyEditor2__SkyEditorDummy__c();	
				Component15_op = new SkyEditor2.TextHolder();	
					
				Component17_val = new SkyEditor2__SkyEditorDummy__c();	
				Component17_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component17_op = new SkyEditor2.TextHolder();	
				valueOptions_TextManagement2_c_CloudStatus_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : TextManagement2__c.CloudStatus__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_TextManagement2_c_CloudStatus_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component122_val = lookupObjComponent61;	
				Component122_op = new SkyEditor2.TextHolder();	
					
				Component125_val = new SkyEditor2__SkyEditorDummy__c();	
				Component125_op = new SkyEditor2.TextHolder();	
					
				Component128_val = new SkyEditor2__SkyEditorDummy__c();	
				Component128_op = new SkyEditor2.TextHolder();	
					
				Component178_val = new SkyEditor2__SkyEditorDummy__c();	
				Component178_op = new SkyEditor2.TextHolder();	
					
				queryMap.put(	
					'Component2',	
					new SkyEditor2.Query('TextManagement2__c')
						.addField('CloudStatus__c')
						.addFieldAsOutput('Name')
						.addFieldAsOutput('Account1__c')
						.addFieldAsOutput('Text_Accep_TedNorm__c')
						.addFieldAsOutput('TextType__c')
						.addFieldAsOutput('SplitDeliveryHopeDate__c')
						.addFieldAsOutput('DeliveryScheduleDay__c')
						.addFieldAsOutput('SalesDeliveryCount__c')
						.addFieldAsOutput('Item2_Keyword_letter_count__c')
						.addFieldAsOutput('CMS__c')
						.addFieldAsOutput('ImageContents__c')
						.addFieldAsOutput('PictureCount__c')
						.addFieldAsOutput('PictureTotal__c')
						.addFieldAsOutput('UserEstimationAmount__c')
						.addFieldAsOutput('OutsourcingEstimationAmount__c')
						.addFieldAsOutput('regulationSheetNo__c')
						.addField('GroupID__c')
						.addFieldAsOutput('SalesSupport__c')
						.addField('CloudBox__c')
						.addField('Memo__c')
						.addFieldAsOutput('SalesMemo1__c')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component76_from, 'SkyEditor2__Date__c', 'DeliveryScheduleDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component76_to, 'SkyEditor2__Date__c', 'DeliveryScheduleDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component76_isNull, 'SkyEditor2__Date__c', 'DeliveryScheduleDay__c', Component76_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component215_from, 'SkyEditor2__Date__c', 'SplitDeliveryHopeDate__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component215_to, 'SkyEditor2__Date__c', 'SplitDeliveryHopeDate__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component215_isNull, 'SkyEditor2__Date__c', 'SplitDeliveryHopeDate__c', Component215_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component233_from, 'SkyEditor2__Date__c', 'DeliveryDaySales1__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component233_to, 'SkyEditor2__Date__c', 'DeliveryDaySales1__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component233_isNull, 'SkyEditor2__Date__c', 'DeliveryDaySales1__c', Component233_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component188_val, 'SkyEditor2__Checkbox__c', 'flagPlatinum__c', Component188_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component192_val, 'SkyEditor2__Checkbox__c', 'flagQuestionnaire__c', Component192_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component190_val, 'SkyEditor2__Checkbox__c', 'flagLighting__c', Component190_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component237_val, 'SkyEditor2__Checkbox__c', 'flagExpert__c', Component237_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component133_val, 'SkyEditor2__Text__c', 'Account1__c', Component133_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component11_val, 'SkyEditor2__Text__c', 'Product__c', Component11_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component15_val, 'SkyEditor2__Text__c', 'Status__c', Component15_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component17_val_dummy, 'SkyEditor2__Text__c','CloudStatus__c', Component17_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component122_val, 'SalesPerson10__c', 'SalesPerson10__c', Component122_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component125_val, 'SkyEditor2__Text__c', 'Text_Accep_TedNorm__c', Component125_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component128_val, 'SkyEditor2__Textarea__c', 'GroupID__c', Component128_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component178_val, 'SkyEditor2__Text__c', 'Name', Component178_op, true, 0, false ))
				);	
					
					Component2 = new Component2(new List<TextManagement2__c>(), new List<Component2Item>(), new List<TextManagement2__c>(), null, queryMap.get('Component2'));
				 Component2.setPageSize(100);
				listItemHolders.put('Component2', Component2);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(TextManagement2__c.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = false;
					Cookie c = ApexPages.currentPage().getCookies().get('QueryPagingConfirm');
					QueryPagingConfirmationSetting = c != null && c.getValue() == '0';
			execInitialSearch = false;
			presetSystemParams();
			Component2.extender = this.extender;
			initSearch();
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_TextManagement2_c_flagPlatinum_c() { 
			return getOperatorOptions('TextManagement2__c', 'flagPlatinum__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_flagQuestionnaire_c() { 
			return getOperatorOptions('TextManagement2__c', 'flagQuestionnaire__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_flagLighting_c() { 
			return getOperatorOptions('TextManagement2__c', 'flagLighting__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_flagExpert_c() { 
			return getOperatorOptions('TextManagement2__c', 'flagExpert__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Account1_c() { 
			return getOperatorOptions('TextManagement2__c', 'Account1__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Product_c() { 
			return getOperatorOptions('TextManagement2__c', 'Product__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Status_c() { 
			return getOperatorOptions('TextManagement2__c', 'Status__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_CloudStatus_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_SalesPerson10_c() { 
			return getOperatorOptions('TextManagement2__c', 'SalesPerson10__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Text_Accep_TedNorm_c() { 
			return getOperatorOptions('TextManagement2__c', 'Text_Accep_TedNorm__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_GroupID_c() { 
			return getOperatorOptions('TextManagement2__c', 'GroupID__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Name() { 
			return getOperatorOptions('TextManagement2__c', 'Name');	
		}	
			
			
	global with sharing class Component2Item extends SkyEditor2.ListItem {
		public TextManagement2__c record{get; private set;}
		@TestVisible
		Component2Item(Component2 holder, TextManagement2__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component2 extends SkyEditor2.QueryPagingList {
		public List<Component2Item> items{get; private set;}
		@TestVisible
			Component2(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector, SkyEditor2.Query query) {
			super(records, items, deleteRecords, recordTypeSelector, query);
			this.items = (List<Component2Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component2Item(this, (TextManagement2__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
		public void doFirst(){first();}
		public void doPrevious(){previous();}
		public void doNext(){next();}
		public void doLast(){last();}
		public void doSort(){sort();}

	}

	public TextManagement2__c Component2_table_Conversion { get { return new TextManagement2__c();}}
	
	public String Component2_table_selectval { get; set; }
	
	
			
	}