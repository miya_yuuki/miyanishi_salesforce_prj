global with sharing class paid_control extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public PaymentManagement__c record{get;set;}	
			
	
		public Component3 Component3 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component11_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component11_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component11_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component11_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component5_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component5_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component5_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component5_isNull_op{get;set;}	
			
		public PaymentManagement__c Component10_val {get;set;}	
		public SkyEditor2.TextHolder Component10_op{get;set;}	
			
		public PaymentManagement__c Component50_val {get;set;}	
		public SkyEditor2.TextHolder Component50_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component33_val {get;set;}	
		public SkyEditor2.TextHolder Component33_op{get;set;}	
			
		public Bill__c Component35_val {get;set;}	
		public SkyEditor2.TextHolder Component35_op{get;set;}	
			
	public String recordTypeRecordsJSON_PaymentManagement_c {get; private set;}
	public String defaultRecordTypeId_PaymentManagement_c {get; private set;}
	public String metadataJSON_PaymentManagement_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public paid_control(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = PaymentManagement__c.fields.contract_company__c;
		f = PaymentManagement__c.fields.bill_name__c;
		f = Bill__c.fields.Bill_PaymentMethod__c;
		f = Bill__c.fields.Bill_PaymentSiteMaster__c;
		f = PaymentManagement__c.fields.Name;
		f = PaymentManagement__c.fields.Bill_PaymentDay__c;
		f = PaymentManagement__c.fields.Bill_Payment__c;
		f = PaymentManagement__c.fields.RecordTypeId;
 f = Bill__c.fields.Bill_PaymentDueDate__c;
 f = PaymentManagement__c.fields.Bill_PaymentDay__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = PaymentManagement__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component11_from = new SkyEditor2__SkyEditorDummy__c();	
				Component11_to = new SkyEditor2__SkyEditorDummy__c();	
				Component11_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component11_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component5_from = new SkyEditor2__SkyEditorDummy__c();	
				Component5_to = new SkyEditor2__SkyEditorDummy__c();	
				Component5_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component5_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				PaymentManagement__c lookupObjComponent28 = new PaymentManagement__c();	
				Component10_val = lookupObjComponent28;	
				Component10_op = new SkyEditor2.TextHolder();	
					
				Component50_val = lookupObjComponent28;	
				Component50_op = new SkyEditor2.TextHolder();	
					
				Bill__c lookupObjComponent28billnamec = new Bill__c();	
				Component33_val = new SkyEditor2__SkyEditorDummy__c();	
				Component33_op = new SkyEditor2.TextHolder();	
					
				Component35_val = lookupObjComponent28billnamec;	
				Component35_op = new SkyEditor2.TextHolder();	
					
				queryMap.put(	
					'Component3',	
					new SkyEditor2.Query('PaymentManagement__c')
						.addFieldAsOutput('bill_name__c')
						.addFieldAsOutput('contract_company__c')
						.addFieldAsOutput('Name')
						.addField('Bill_PaymentDay__c')
						.addField('Bill_Payment__c')
						.addField('RecordTypeId')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component11_from, 'SkyEditor2__Date__c', 'bill_name__r.Bill_PaymentDueDate__c',Bill__c.fields.Bill_PaymentDueDate__c, new SkyEditor2.TextHolder('ge'), false, false,true,0,false,'bill_name__c',PaymentManagement__c.fields.bill_name__c )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component11_to, 'SkyEditor2__Date__c', 'bill_name__r.Bill_PaymentDueDate__c',Bill__c.fields.Bill_PaymentDueDate__c, new SkyEditor2.TextHolder('le'), false, false,true,0,false,'bill_name__c',PaymentManagement__c.fields.bill_name__c )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component11_isNull, 'SkyEditor2__Date__c', 'bill_name__r.Bill_PaymentDueDate__c',Bill__c.fields.Bill_PaymentDueDate__c, Component11_isNull_op, true, false,true,0,false,'bill_name__c',PaymentManagement__c.fields.bill_name__c )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component5_from, 'SkyEditor2__Date__c', 'Bill_PaymentDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component5_to, 'SkyEditor2__Date__c', 'Bill_PaymentDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component5_isNull, 'SkyEditor2__Date__c', 'Bill_PaymentDay__c', Component5_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component10_val, 'contract_company__c', 'contract_company__c', Component10_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component50_val, 'bill_name__c', 'bill_name__c', Component50_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component33_val, 'SkyEditor2__Text__c', 'bill_name__r.Bill_PaymentMethod__c',Bill__c.fields.Bill_PaymentMethod__c, Component33_op, true, false,true,0,false,'bill_name__c',PaymentManagement__c.fields.bill_name__c )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component35_val, 'Bill_PaymentSiteMaster__c', 'bill_name__r.Bill_PaymentSiteMaster__c',Bill__c.fields.Bill_PaymentSiteMaster__c, Component35_op, true, false,true,0,false,'bill_name__c',PaymentManagement__c.fields.bill_name__c )) 
				);	
					
					Component3 = new Component3(new List<PaymentManagement__c>(), new List<Component3Item>(), new List<PaymentManagement__c>(), new SkyEditor2.RecordTypeSelector(PaymentManagement__c.SObjectType));
				listItemHolders.put('Component3', Component3);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(PaymentManagement__c.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = true;
			execInitialSearch = false;
			presetSystemParams();
			Component3.extender = this.extender;
			initSearch();
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_PaymentManagement_c_contract_company_c() { 
			return getOperatorOptions('PaymentManagement__c', 'contract_company__c');	
		}	
		public List<SelectOption> getOperatorOptions_PaymentManagement_c_bill_name_c() { 
			return getOperatorOptions('PaymentManagement__c', 'bill_name__c');	
		}	
		public List<SelectOption> getOperatorOptions_Bill_c_Bill_PaymentMethod_c() { 
			return getOperatorOptions('Bill__c', 'Bill_PaymentMethod__c');	
		}	
		public List<SelectOption> getOperatorOptions_Bill_c_Bill_PaymentSiteMaster_c() { 
			return getOperatorOptions('Bill__c', 'Bill_PaymentSiteMaster__c');	
		}	
			
			
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public PaymentManagement__c record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, PaymentManagement__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.ListItemHolder {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (PaymentManagement__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	public PaymentManagement__c Component3_table_Conversion { get { return new PaymentManagement__c();}}
	
	public String Component3_table_selectval { get; set; }
	
	
			
	}