public with sharing class TextManagementDistributionUpdate {
	public static boolean firstRun = true;
	
	public static void prcDistributionUpdate(List<TextManagement2__c> objTextManagement2, Map<Id, TextManagement2__c> oldMap, Boolean upd) {
		// 注文商品ID
		List<String> strOrderItemIds = new List<String>();
		// 注文商品オブジェクト
		List<OrderItem__c> updOrderItem = new List<OrderItem__c>();
		
		// 更新対象のチェック
		for (TextManagement2__c obj : objTextManagement2) {
			if (prcCheckDistribution(upd, obj, oldMap) == true) {
				strOrderItemIds.add(obj.Item2_OrderProduct__c);
				updOrderItem = [SELECT Id, SalesPercentage1__c, SalesPersonSubUnit__c, SalesPersonSub__c, SalesPercentage2__c FROM OrderItem__c WHERE Id = :strOrderItemIds]; 
				for (OrderItem__c objNew : updOrderItem) {
					objNew.SalesPercentage1__c = obj.SalesPercentage1__c;
					objNew.SalesPersonSub__c = obj.SalesPersonSub__c;
					objNew.SalesPercentage2__c = obj.SalesPercentage2__c;
					if (obj.SalesPersonSub__c == null) {
						objNew.SalesPersonSubUnit__c = null;
					}
					else {
						objNew.SalesPersonSubUnit__c = [SELECT Id, team__c FROM User WHERE Id = :obj.SalesPersonSub__c].team__c;
					}
				}
				if (updOrderItem.size() > 0) {
					update updOrderItem;
				}
			}
			else {
				continue;
			}
		}
	}
	private static Boolean prcCheckDistribution (Boolean upd, TextManagement2__c objNew, Map<Id, TextManagement2__c> oldMap) {
		if (upd) {
			// 古い値を取得
			TextManagement2__c objOld = oldMap.get(objNew.Id);
			// 案分担当①比率・案分担当②系の何かが更新された場合
			if (objNew.SalesPercentage1__c != objOld.SalesPercentage1__c || objNew.SalesPersonSubUnit__c != objOld.SalesPersonSubUnit__c || objNew.SalesPersonSub__c != objOld.SalesPersonSub__c || objNew.SalesPercentage2__c != objOld.SalesPercentage2__c) {
				System.debug('案分関連更新検知' + objNew.Id);
				return true;
			}
		}
		// 条件に合致しない
		return null;
	}
}