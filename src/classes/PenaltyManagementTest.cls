@isTest
private class PenaltyManagementTest {

    static testMethod void testMethods() {
        // 入力チェックを停止
		User stopCheck = new User(Id = Userinfo.getUserId(), StopInputCheck__c = true);
		update stopCheck;

		// 取引先
		Account testAccount = new Account(
			Name = 'テスト株式会社',
			account_localName__c = 'テストフリガナ',
			account_Lead__c = '架電',
			account_Lead2__c = '架電',
			Type = '顧客'
		);
		insert testAccount;

		// 顧客カルテ
		karute__c testKarute = new karute__c(
			AccountName__c = testAccount.Id
		);
		insert testKarute;

		// ペナルティ管理
		PenaltyManagement__c testPenalty = new PenaltyManagement__c(
			karute__c = testKarute.Id,
			date__c = System.today()
		);
		insert testPenalty;
    }
}