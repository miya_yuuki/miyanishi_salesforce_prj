global with sharing class syoudanLifeMedia extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public Opportunity record {get{return (Opportunity)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public String recordTypeRecordsJSON_Opportunity {get; private set;}
	public String defaultRecordTypeId_Opportunity {get; private set;}
	public String metadataJSON_Opportunity {get; private set;}
	public String picklistValuesJSON_Opportunity_Case_c {get; private set;}
	public String Component3951_hidden { get; set; }
	
	
	{
	setApiVersion(31.0);
	}
	public syoudanLifeMedia(ApexPages.StandardController controller) {
		super(controller);


		SObjectField f;

		f = Opportunity.fields.Name;
		f = Opportunity.fields.Opportunity_PMMember_del__c;
		f = Opportunity.fields.AmountTotal__c;
		f = Opportunity.fields.Opportunity_Type__c;
		f = Opportunity.fields.Type;
		f = Opportunity.fields.CloseDate;
		f = Opportunity.fields.Opportunity_endClient1__c;
		f = Opportunity.fields.Opportunity_lead1__c;
		f = Opportunity.fields.StageName;
		f = Opportunity.fields.Opportunity_Competition__c;
		f = Opportunity.fields.Opportunity_lead2__c;
		f = Opportunity.fields.Opportunity_dead__c;
		f = Opportunity.fields.kurashiMail__c;
		f = Opportunity.fields.AddressCheck__c;
		f = Opportunity.fields.Opportunity_DeadMemo__c;
		f = Opportunity.fields.Case__c;
		f = Opportunity.fields.syouninn__c;
		f = Opportunity.fields.Opportunity_deadDay__c;
		f = Opportunity.fields.CasePrintCheck__c;
		f = Opportunity.fields.OwnerId;
		f = Opportunity.fields.AccountId;
		f = Opportunity.fields.Opportunity_BillPostalCode2__c;
		f = Opportunity.fields.account_Relation__c;
		f = Opportunity.fields.account_BusinessModel__c;
		f = Opportunity.fields.Opportunity_BillPrefecture2__c;
		f = Opportunity.fields.Phone__c;
		f = Opportunity.fields.account_Industry__c;
		f = Opportunity.fields.Opportunity_BillCity2__c;
		f = Opportunity.fields.Website__c;
		f = Opportunity.fields.account_Industrycategory__c;
		f = Opportunity.fields.Opportunity_BillBuilding2__c;
		f = Opportunity.fields.ServiceWEBSite2__c;
		f = Opportunity.fields.AccountEdit__c;
		f = Opportunity.fields.Commission_rate__c;
		f = Opportunity.fields.TransferAccountNumber1__c;
		f = Opportunity.fields.account_CreditLevel__c;
		f = Opportunity.fields.CompanyCheck__c;
		f = Opportunity.fields.CreditSpace__c;
		f = Opportunity.fields.hansyaMemo__c;
		f = Opportunity.fields.CreditBalance__c;
		f = Opportunity.fields.hansyaDay__c;
		f = Opportunity.fields.account_CreditDay__c;
		f = Opportunity.fields.Opportunity_Title__c;
		f = Opportunity.fields.Opportunity_BillPhone__c;
		f = Opportunity.fields.Opportunity_Billaccount__c;
		f = Opportunity.fields.Opportunity_BillKana__c;
		f = Opportunity.fields.Opportunity_BillFax__c;
		f = Opportunity.fields.Opportunity_BillPostalCode__c;
		f = Opportunity.fields.Opportunity_BillDepartment__c;
		f = Opportunity.fields.Opportunity_BillEmail__c;
		f = Opportunity.fields.Opportunity_BillPrefecture__c;
		f = Opportunity.fields.Opportunity_BillTitle__c;
		f = Opportunity.fields.TransferAccountNumber__c;
		f = Opportunity.fields.Opportunity_BillCity__c;
		f = Opportunity.fields.Opportunity_BillRole__c;
		f = Opportunity.fields.Opportunity_BillAddress__c;
		f = Opportunity.fields.ContacEdit__c;
		f = Opportunity.fields.Opportunity_BillBuilding__c;
		f = Opportunity.fields.Opportunity_BillingMethod__c;
		f = Opportunity.fields.Opportunity_PaymentSiteMasterView__c;
		f = Opportunity.fields.Opportunity_Memo__c;
		f = Opportunity.fields.Opportunity_PaymentMethod__c;
		f = Opportunity.fields.QuoteExpirationDate__c;
		f = Opportunity.fields.Opportunity_EstimateMemo__c;
		f = Account.fields.BillingPostalCode;
		f = Account.fields.Type;
		f = Account.fields.account_BusinessModel__c;
		f = Account.fields.BillingState;
		f = Account.fields.Phone;
		f = Account.fields.account_Industry__c;
		f = Account.fields.Bill_Address__c;
		f = Account.fields.Website;
		f = Account.fields.account_Industrycategory__c;
		f = Account.fields.account_BuildingName__c;
		f = Account.fields.ServiceWEBSite__c;
		f = Account.fields.Commission_rate__c;
		f = Account.fields.kouza2__c;
		f = Account.fields.account_CreditLevel__c;
		f = Account.fields.CompanyCheck__c;
		f = Account.fields.CreditSpace__c;
		f = Account.fields.hansyaMemo__c;
		f = Account.fields.CreditBalance__c;
		f = Account.fields.hansyaDay__c;
		f = Account.fields.account_CreditDay__c;
		f = Account.fields.BillingMethod__c;
		f = Account.fields.PaymentMethodView__c;
		f = Account.fields.PaymentMethod__c;
		f = Contact.fields.Phone;
		f = Contact.fields.Contact_BillCompany__c;
		f = Contact.fields.Contact_kana__c;
		f = Contact.fields.Fax;
		f = Contact.fields.MailingPostalCode;
		f = Contact.fields.Department;
		f = Contact.fields.Email;
		f = Contact.fields.MailingState;
		f = Contact.fields.Title;
		f = Contact.fields.kouza__c;
		f = Contact.fields.MailingCity;
		f = Contact.fields.Contact_Role__c;
		f = Contact.fields.MailingStreet;
		f = Contact.fields.Contact_BuildingName__c;

		List<RecordTypeInfo> recordTypes;
		FilterMetadataResult filterResult;
		List<RecordType> recordTypeRecords_Opportunity = [SELECT Id, DeveloperName, NamespacePrefix FROM RecordType WHERE SobjectType = 'Opportunity'];
		Map<Id, RecordType> recordTypeMap_Opportunity = new Map<Id, RecordType>(recordTypeRecords_Opportunity);
		List<RecordType> availableRecordTypes_Opportunity = new List<RecordType>();
		recordTypes = SObjectType.Opportunity.getRecordTypeInfos();

		for (RecordTypeInfo t: recordTypes) {
			if (t.isDefaultRecordTypeMapping()) {
				defaultRecordTypeId_Opportunity = t.getRecordTypeId();
			}
			if (t.isAvailable()) {
				RecordType rtype = recordTypeMap_Opportunity.get(t.getRecordTypeId());
				if (rtype != null) {
					availableRecordTypes_Opportunity.add(rtype);
				}
			}
		}
		recordTypeRecordsJSON_Opportunity = System.JSON.serialize(availableRecordTypes_Opportunity);
		filterResult = filterMetadataJSON(
			System.JSON.deserializeUntyped('{"CustomObject":{"recordTypes":[{"fullName":"AP","picklistValues":[{"picklist":"Case__c","values":[{"fullName":"CPAの改善の実績","default":false},{"fullName":"なし_アプローチしていない","default":false},{"fullName":"なし_アプローチ済","default":false},{"fullName":"アクセス向上の実績","default":false},{"fullName":"ロゴ","default":false},{"fullName":"上位表示実績","default":false},{"fullName":"会社名","default":false},{"fullName":"成約数","default":false}]}]},{"fullName":"IntroductionFee","picklistValues":[{"picklist":"Case__c","values":[{"fullName":"CPAの改善の実績","default":false},{"fullName":"アクセス向上の実績","default":false},{"fullName":"ロゴ","default":false},{"fullName":"上位表示実績","default":false},{"fullName":"会社名","default":false},{"fullName":"成約数","default":false}]}]},{"fullName":"LifeMedia","picklistValues":[{"picklist":"Case__c","values":[{"fullName":"CPAの改善の実績","default":false},{"fullName":"なし_アプローチしていない","default":false},{"fullName":"なし_アプローチ済","default":false},{"fullName":"アクセス向上の実績","default":false},{"fullName":"ロゴ","default":false},{"fullName":"上位表示実績","default":false},{"fullName":"会社名","default":false},{"fullName":"成約数","default":false}]}]},{"fullName":"old","picklistValues":[{"picklist":"Case__c","values":[{"fullName":"CPAの改善の実績","default":false},{"fullName":"アクセス向上の実績","default":false},{"fullName":"ロゴ","default":false},{"fullName":"上位表示実績","default":false},{"fullName":"会社名","default":false},{"fullName":"成約数","default":false}]}]},{"fullName":"syanaiRD","picklistValues":[{"picklist":"Case__c","values":[{"fullName":"CPAの改善の実績","default":false},{"fullName":"アクセス向上の実績","default":false},{"fullName":"ロゴ","default":false},{"fullName":"上位表示実績","default":false},{"fullName":"会社名","default":false},{"fullName":"成約数","default":false}]}]}]}}'),
			recordTypeFullNames(availableRecordTypes_Opportunity),
			Opportunity.SObjectType
		);
		metadataJSON_Opportunity = System.JSON.serialize(filterResult.data);
		picklistValuesJSON_Opportunity_Case_c = System.JSON.serialize(filterPricklistEntries(Opportunity.SObjectType.Case__c.getDescribe(), filterResult));
		try {
			mainSObjectType = Opportunity.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('Opportunity');
			mainQuery.addField('Name');
			mainQuery.addField('Opportunity_PMMember_del__c');
			mainQuery.addField('Opportunity_Type__c');
			mainQuery.addField('Type');
			mainQuery.addField('CloseDate');
			mainQuery.addField('Opportunity_endClient1__c');
			mainQuery.addField('Opportunity_lead1__c');
			mainQuery.addField('StageName');
			mainQuery.addField('Opportunity_Competition__c');
			mainQuery.addField('Opportunity_lead2__c');
			mainQuery.addField('Opportunity_dead__c');
			mainQuery.addField('kurashiMail__c');
			mainQuery.addField('AddressCheck__c');
			mainQuery.addField('Opportunity_DeadMemo__c');
			mainQuery.addField('Case__c');
			mainQuery.addField('Opportunity_deadDay__c');
			mainQuery.addField('CasePrintCheck__c');
			mainQuery.addField('AccountId');
			mainQuery.addField('Opportunity_Title__c');
			mainQuery.addField('Opportunity_Memo__c');
			mainQuery.addField('Opportunity_PaymentMethod__c');
			mainQuery.addField('QuoteExpirationDate__c');
			mainQuery.addField('Opportunity_EstimateMemo__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('AmountTotal__c');
			mainQuery.addFieldAsOutput('syouninn__c');
			mainQuery.addFieldAsOutput('OwnerId');
			mainQuery.addFieldAsOutput('Opportunity_BillPostalCode2__c');
			mainQuery.addFieldAsOutput('account_Relation__c');
			mainQuery.addFieldAsOutput('account_BusinessModel__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPrefecture2__c');
			mainQuery.addFieldAsOutput('Phone__c');
			mainQuery.addFieldAsOutput('account_Industry__c');
			mainQuery.addFieldAsOutput('Opportunity_BillCity2__c');
			mainQuery.addFieldAsOutput('Website__c');
			mainQuery.addFieldAsOutput('account_Industrycategory__c');
			mainQuery.addFieldAsOutput('Opportunity_BillBuilding2__c');
			mainQuery.addFieldAsOutput('ServiceWEBSite2__c');
			mainQuery.addFieldAsOutput('AccountEdit__c');
			mainQuery.addFieldAsOutput('Commission_rate__c');
			mainQuery.addFieldAsOutput('TransferAccountNumber1__c');
			mainQuery.addFieldAsOutput('account_CreditLevel__c');
			mainQuery.addFieldAsOutput('CompanyCheck__c');
			mainQuery.addFieldAsOutput('CreditSpace__c');
			mainQuery.addFieldAsOutput('hansyaMemo__c');
			mainQuery.addFieldAsOutput('CreditBalance__c');
			mainQuery.addFieldAsOutput('hansyaDay__c');
			mainQuery.addFieldAsOutput('account_CreditDay__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPhone__c');
			mainQuery.addFieldAsOutput('Opportunity_Billaccount__c');
			mainQuery.addFieldAsOutput('Opportunity_BillKana__c');
			mainQuery.addFieldAsOutput('Opportunity_BillFax__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPostalCode__c');
			mainQuery.addFieldAsOutput('Opportunity_BillDepartment__c');
			mainQuery.addFieldAsOutput('Opportunity_BillEmail__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPrefecture__c');
			mainQuery.addFieldAsOutput('Opportunity_BillTitle__c');
			mainQuery.addFieldAsOutput('TransferAccountNumber__c');
			mainQuery.addFieldAsOutput('Opportunity_BillCity__c');
			mainQuery.addFieldAsOutput('Opportunity_BillRole__c');
			mainQuery.addFieldAsOutput('Opportunity_BillAddress__c');
			mainQuery.addFieldAsOutput('ContacEdit__c');
			mainQuery.addFieldAsOutput('Opportunity_BillBuilding__c');
			mainQuery.addFieldAsOutput('Opportunity_BillingMethod__c');
			mainQuery.addFieldAsOutput('Opportunity_PaymentSiteMasterView__c');
			mainQuery.addFieldAsOutput('LeadSource');
			mainQuery.addFieldAsOutput('Id');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}


	public void loadReferenceValues_Component3680() {
		if (record.AccountId == null) {

			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillPostalCode2__c)) {
				record.Opportunity_BillPostalCode2__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.account_Relation__c)) {
				record.account_Relation__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.account_BusinessModel__c)) {
				record.account_BusinessModel__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillPrefecture2__c)) {
				record.Opportunity_BillPrefecture2__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Phone__c)) {
				record.Phone__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.account_Industry__c)) {
				record.account_Industry__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillCity2__c)) {
				record.Opportunity_BillCity2__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Website__c)) {
				record.Website__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.account_Industrycategory__c)) {
				record.account_Industrycategory__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillBuilding2__c)) {
				record.Opportunity_BillBuilding2__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.ServiceWEBSite2__c)) {
				record.ServiceWEBSite2__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Commission_rate__c)) {
				record.Commission_rate__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.TransferAccountNumber1__c)) {
				record.TransferAccountNumber1__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.account_CreditLevel__c)) {
				record.account_CreditLevel__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.CompanyCheck__c)) {
				record.CompanyCheck__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.CreditSpace__c)) {
				record.CreditSpace__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.hansyaMemo__c)) {
				record.hansyaMemo__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.CreditBalance__c)) {
				record.CreditBalance__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.hansyaDay__c)) {
				record.hansyaDay__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.account_CreditDay__c)) {
				record.account_CreditDay__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillingMethod__c)) {
				record.Opportunity_BillingMethod__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_PaymentSiteMasterView__c)) {
				record.Opportunity_PaymentSiteMasterView__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_PaymentMethod__c)) {
				record.Opportunity_PaymentMethod__c = null;
			}
				return;
		}
		Account[] referenceTo = [SELECT BillingPostalCode,Type,account_BusinessModel__c,BillingState,Phone,account_Industry__c,Bill_Address__c,Website,account_Industrycategory__c,account_BuildingName__c,ServiceWEBSite__c,Commission_rate__c,kouza2__c,account_CreditLevel__c,CompanyCheck__c,CreditSpace__c,hansyaMemo__c,CreditBalance__c,hansyaDay__c,account_CreditDay__c,BillingMethod__c,PaymentMethodView__c,PaymentMethod__c FROM Account WHERE Id=:record.AccountId];
		if (referenceTo.size() == 0) {
			record.AccountId.addError(SkyEditor2.Messages.referenceDataNotFound(record.AccountId));
			return;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillPostalCode2__c)) {
			record.Opportunity_BillPostalCode2__c = referenceTo[0].BillingPostalCode;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.account_Relation__c)) {
			record.account_Relation__c = referenceTo[0].Type;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.account_BusinessModel__c)) {
			record.account_BusinessModel__c = referenceTo[0].account_BusinessModel__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillPrefecture2__c)) {
			record.Opportunity_BillPrefecture2__c = referenceTo[0].BillingState;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Phone__c)) {
			record.Phone__c = referenceTo[0].Phone;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.account_Industry__c)) {
			record.account_Industry__c = referenceTo[0].account_Industry__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillCity2__c)) {
			record.Opportunity_BillCity2__c = referenceTo[0].Bill_Address__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Website__c)) {
			record.Website__c = referenceTo[0].Website;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.account_Industrycategory__c)) {
			record.account_Industrycategory__c = referenceTo[0].account_Industrycategory__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillBuilding2__c)) {
			record.Opportunity_BillBuilding2__c = referenceTo[0].account_BuildingName__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.ServiceWEBSite2__c)) {
			record.ServiceWEBSite2__c = referenceTo[0].ServiceWEBSite__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Commission_rate__c)) {
			record.Commission_rate__c = referenceTo[0].Commission_rate__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.TransferAccountNumber1__c)) {
			record.TransferAccountNumber1__c = referenceTo[0].kouza2__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.account_CreditLevel__c)) {
			record.account_CreditLevel__c = referenceTo[0].account_CreditLevel__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.CompanyCheck__c)) {
			record.CompanyCheck__c = referenceTo[0].CompanyCheck__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.CreditSpace__c)) {
			record.CreditSpace__c = referenceTo[0].CreditSpace__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.hansyaMemo__c)) {
			record.hansyaMemo__c = referenceTo[0].hansyaMemo__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.CreditBalance__c)) {
			record.CreditBalance__c = referenceTo[0].CreditBalance__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.hansyaDay__c)) {
			record.hansyaDay__c = referenceTo[0].hansyaDay__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.account_CreditDay__c)) {
			record.account_CreditDay__c = referenceTo[0].account_CreditDay__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillingMethod__c)) {
			record.Opportunity_BillingMethod__c = referenceTo[0].BillingMethod__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_PaymentSiteMasterView__c)) {
			record.Opportunity_PaymentSiteMasterView__c = referenceTo[0].PaymentMethodView__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_PaymentMethod__c)) {
			record.Opportunity_PaymentMethod__c = referenceTo[0].PaymentMethod__c;
		}
		
	}

	public void loadReferenceValues_Component3694() {
		if (record.Opportunity_Title__c == null) {

			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillPhone__c)) {
				record.Opportunity_BillPhone__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_Billaccount__c)) {
				record.Opportunity_Billaccount__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillKana__c)) {
				record.Opportunity_BillKana__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillFax__c)) {
				record.Opportunity_BillFax__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillPostalCode__c)) {
				record.Opportunity_BillPostalCode__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillDepartment__c)) {
				record.Opportunity_BillDepartment__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillEmail__c)) {
				record.Opportunity_BillEmail__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillPrefecture__c)) {
				record.Opportunity_BillPrefecture__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillTitle__c)) {
				record.Opportunity_BillTitle__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.TransferAccountNumber__c)) {
				record.TransferAccountNumber__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillCity__c)) {
				record.Opportunity_BillCity__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillRole__c)) {
				record.Opportunity_BillRole__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillAddress__c)) {
				record.Opportunity_BillAddress__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillBuilding__c)) {
				record.Opportunity_BillBuilding__c = null;
			}
				return;
		}
		Contact[] referenceTo = [SELECT Phone,Contact_BillCompany__c,Contact_kana__c,Fax,MailingPostalCode,Department,Email,MailingState,Title,kouza__c,MailingCity,Contact_Role__c,MailingStreet,Contact_BuildingName__c FROM Contact WHERE Id=:record.Opportunity_Title__c];
		if (referenceTo.size() == 0) {
			record.Opportunity_Title__c.addError(SkyEditor2.Messages.referenceDataNotFound(record.Opportunity_Title__c));
			return;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillPhone__c)) {
			record.Opportunity_BillPhone__c = referenceTo[0].Phone;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_Billaccount__c)) {
			record.Opportunity_Billaccount__c = referenceTo[0].Contact_BillCompany__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillKana__c)) {
			record.Opportunity_BillKana__c = referenceTo[0].Contact_kana__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillFax__c)) {
			record.Opportunity_BillFax__c = referenceTo[0].Fax;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillPostalCode__c)) {
			record.Opportunity_BillPostalCode__c = referenceTo[0].MailingPostalCode;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillDepartment__c)) {
			record.Opportunity_BillDepartment__c = referenceTo[0].Department;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillEmail__c)) {
			record.Opportunity_BillEmail__c = referenceTo[0].Email;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillPrefecture__c)) {
			record.Opportunity_BillPrefecture__c = referenceTo[0].MailingState;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillTitle__c)) {
			record.Opportunity_BillTitle__c = referenceTo[0].Title;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.TransferAccountNumber__c)) {
			record.TransferAccountNumber__c = referenceTo[0].kouza__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillCity__c)) {
			record.Opportunity_BillCity__c = referenceTo[0].MailingCity;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillRole__c)) {
			record.Opportunity_BillRole__c = referenceTo[0].Contact_Role__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillAddress__c)) {
			record.Opportunity_BillAddress__c = referenceTo[0].MailingStreet;
		}
		
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillBuilding__c)) {
			record.Opportunity_BillBuilding__c = referenceTo[0].Contact_BuildingName__c;
		}
		
	}
	public String getComponent3951OptionsJS() {
		return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
		Opportunity.getSObjectType(),
		SObjectType.Opportunity.fields.Case__c.getSObjectField()
		));
		}
	@TestVisible		static Set<String> recordTypeFullNames(RecordType[] records) {
		Set<String> result = new Set<String>();
		for (RecordType r : records) {
			result.add(r.DeveloperName);
			if (r.NamespacePrefix != null) {
				result.add(r.NamespacePrefix + '__' + r.DeveloperName);
			}
		}
		return result;
	}
	
	@TestVisible		static FilterMetadataResult filterMetadataJSON(Object metadata, Set<String> recordTypeFullNames, SObjectType soType) {
		Map<String, Object> metadataMap = (Map<String, Object>) metadata;
		Map<String, Object> customObject = (Map<String, Object>) metadataMap.get('CustomObject');
		List<Object> recordTypes = (List<Object>) customObject.get('recordTypes');
		Map<String, Set<String>> availableEntries = new Map<String, Set<String>>();
		for (Integer i = recordTypes.size() - 1; i >= 0; i--) {
			Map<String, Object> recordType = (Map<String, Object>)recordTypes[i];
			String fullName = (String)recordType.get('fullName');
			if (! recordTypeFullNames.contains(fullName)) {
				recordTypes.remove(i);
			} else {
				addAll(availableEntries, getOutEntries(recordType, soType));
			}
		}	
		return new FilterMetadataResult(metadataMap, availableEntries, recordTypes.size() == 0);
	}
	public class FilterMetadataResult {
		public Map<String, Object> data {get; private set;}
		public Map<String, Set<String>> availableEntries {get; private set;}
		public Boolean master {get; private set;}
		public FilterMetadataResult(Map<String, Object> data, Map<String, Set<String>> availableEntries, Boolean master) {
			this.data = data;
			this.availableEntries = availableEntries;
			this.master = master;
		}
	}
	
	static void addAll(Map<String, Set<String>> toMap, Map<String, Set<String>> fromMap) {
		for (String key : fromMap.keySet()) {
			Set<String> fromSet = fromMap.get(key);
			Set<String> toSet = toMap.get(key);
			if (toSet == null) {
				toSet = new Set<String>();
				toMap.put(key, toSet);
			}
			toSet.addAll(fromSet);
		}
	}

	static Map<String, Set<String>> getOutEntries(Map<String, Object> recordType, SObjectType soType) {
		Map<String, Set<String>> result = new Map<String, Set<String>>();
		List<Object> entries = (List<Object>)recordType.get('picklistValues');
		Map<String, SObjectField> fields = soType.getDescribe().fields.getMap();
		for (Object e : entries) {
			Map<String, Object> entry = (Map<String, Object>) e;
			String picklist = (String) entry.get('picklist');
			SObjectField f = fields.get(picklist);
			List<Object> values = (List<Object>)(entry.get('values'));
			if (f != null && f.getDescribe().isAccessible()) {
				Set<String> entrySet = new Set<String>();
				for (Object v : values) {
					Map<String, Object> value = (Map<String, Object>) v;
					entrySet.add(EncodingUtil.urlDecode((String)value.get('fullName'), 'utf-8'));
				}
				result.put(picklist, entrySet);
			} else { 
				values.clear(); 
			}
		}
		return result;
	}
	
	static List<PicklistEntry> filterPricklistEntries(DescribeFieldResult f, FilterMetadataResult parseResult) {
		List<PicklistEntry> all = f.getPicklistValues();
		if (parseResult.master) {
			return all;
		}
		Set<String> availables = parseResult.availableEntries.get(f.getName());
		List<PicklistEntry> result = new List<PicklistEntry>();
		if(availables == null) return result;
		for (PicklistEntry e : all) {
			if (e.isActive() && availables.contains(e.getValue())) {
				result.add(e);
			}
		}
		return result;
	}
	
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}