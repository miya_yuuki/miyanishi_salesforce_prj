@isTest
private with sharing class syoudanLifeMediaTest{
	private static testMethod void testPageMethods() {		syoudanLifeMedia extension = new syoudanLifeMedia(new ApexPages.StandardController(new Opportunity()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
		extension.getComponent3951OptionsJS();
	}
	private static testMethod void testRecordTypeFullNames() {
		Set<String> result = syoudanLifeMedia.recordTypeFullNames(new RecordType[] {
			new RecordType(DeveloperName = 'TestRecordType')
		});
		System.assertEquals(result.size(), 1);
		System.assert(result.contains('TestRecordType'));
	}
	
	private static testMethod void testFilterMetadataJSON() {
		String json = '{"CustomObject":{"recordTypes":[{"fullName":"RecordType1","picklistValues":[]},{"fullName":"RecordType2","picklistValues":[]}]}}';		Set<String> recordTypeSet = new Set<String>();
		recordTypeSet.add('RecordType2');
		Object metadata = System.JSON.deserializeUntyped(json);
		Map<String, Object> data = (Map<String, Object>) syoudanLifeMedia.filterMetadataJSON(metadata, recordTypeSet, SkyEditor2__SkyEditorDummy__c.SObjectType).data;
		Map<String, Object> customObject = (Map<String, Object>) data.get('CustomObject');
		List<Object> recordTypes = (List<Object>) customObject.get('recordTypes');
		System.assertEquals(recordTypes.size(), 1);
		Map<String, Object> recordType = (Map<String, Object>) recordTypes[0];
		System.assertEquals('RecordType2', recordType.get('fullName'));
	}


	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component3680() {
		String testReferenceId = '';
		syoudanLifeMedia extension = new syoudanLifeMedia(new ApexPages.StandardController(new Opportunity()));
		extension.loadReferenceValues_Component3680();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(Account.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<Account> parents = [SELECT Id FROM Account LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		Account parent = [SELECT Id,BillingPostalCode,Type,account_BusinessModel__c,BillingState,Phone,account_Industry__c,Bill_Address__c,Website,account_Industrycategory__c,account_BuildingName__c,ServiceWEBSite__c,Commission_rate__c,kouza2__c,account_CreditLevel__c,CompanyCheck__c,CreditSpace__c,hansyaMemo__c,CreditBalance__c,hansyaDay__c,account_CreditDay__c,BillingMethod__c,PaymentMethodView__c,PaymentMethod__c FROM Account WHERE Id = :testReferenceId];
		extension.record.AccountId = parent.Id;
		extension.loadReferenceValues_Component3680();
				
		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_BillPostalCode2__c)) {
			System.assertEquals(parent.BillingPostalCode, extension.record.Opportunity_BillPostalCode2__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.account_Relation__c)) {
			System.assertEquals(parent.Type, extension.record.account_Relation__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.account_BusinessModel__c)) {
			System.assertEquals(parent.account_BusinessModel__c, extension.record.account_BusinessModel__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_BillPrefecture2__c)) {
			System.assertEquals(parent.BillingState, extension.record.Opportunity_BillPrefecture2__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Phone__c)) {
			System.assertEquals(parent.Phone, extension.record.Phone__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.account_Industry__c)) {
			System.assertEquals(parent.account_Industry__c, extension.record.account_Industry__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_BillCity2__c)) {
			System.assertEquals(parent.Bill_Address__c, extension.record.Opportunity_BillCity2__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Website__c)) {
			System.assertEquals(parent.Website, extension.record.Website__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.account_Industrycategory__c)) {
			System.assertEquals(parent.account_Industrycategory__c, extension.record.account_Industrycategory__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_BillBuilding2__c)) {
			System.assertEquals(parent.account_BuildingName__c, extension.record.Opportunity_BillBuilding2__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.ServiceWEBSite2__c)) {
			System.assertEquals(parent.ServiceWEBSite__c, extension.record.ServiceWEBSite2__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Commission_rate__c)) {
			System.assertEquals(parent.Commission_rate__c, extension.record.Commission_rate__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.TransferAccountNumber1__c)) {
			System.assertEquals(parent.kouza2__c, extension.record.TransferAccountNumber1__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.account_CreditLevel__c)) {
			System.assertEquals(parent.account_CreditLevel__c, extension.record.account_CreditLevel__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.CompanyCheck__c)) {
			System.assertEquals(parent.CompanyCheck__c, extension.record.CompanyCheck__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.CreditSpace__c)) {
			System.assertEquals(parent.CreditSpace__c, extension.record.CreditSpace__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.hansyaMemo__c)) {
			System.assertEquals(parent.hansyaMemo__c, extension.record.hansyaMemo__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.CreditBalance__c)) {
			System.assertEquals(parent.CreditBalance__c, extension.record.CreditBalance__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.hansyaDay__c)) {
			System.assertEquals(parent.hansyaDay__c, extension.record.hansyaDay__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.account_CreditDay__c)) {
			System.assertEquals(parent.account_CreditDay__c, extension.record.account_CreditDay__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_BillingMethod__c)) {
			System.assertEquals(parent.BillingMethod__c, extension.record.Opportunity_BillingMethod__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_PaymentSiteMasterView__c)) {
			System.assertEquals(parent.PaymentMethodView__c, extension.record.Opportunity_PaymentSiteMasterView__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_PaymentMethod__c)) {
			System.assertEquals(parent.PaymentMethod__c, extension.record.Opportunity_PaymentMethod__c);
		}

		System.assert(true);
	}

	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component3694() {
		String testReferenceId = '';
		syoudanLifeMedia extension = new syoudanLifeMedia(new ApexPages.StandardController(new Opportunity()));
		extension.loadReferenceValues_Component3694();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(Contact.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<Contact> parents = [SELECT Id FROM Contact LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		Contact parent = [SELECT Id,Phone,Contact_BillCompany__c,Contact_kana__c,Fax,MailingPostalCode,Department,Email,MailingState,Title,kouza__c,MailingCity,Contact_Role__c,MailingStreet,Contact_BuildingName__c FROM Contact WHERE Id = :testReferenceId];
		extension.record.Opportunity_Title__c = parent.Id;
		extension.loadReferenceValues_Component3694();
				
		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_BillPhone__c)) {
			System.assertEquals(parent.Phone, extension.record.Opportunity_BillPhone__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_Billaccount__c)) {
			System.assertEquals(parent.Contact_BillCompany__c, extension.record.Opportunity_Billaccount__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_BillKana__c)) {
			System.assertEquals(parent.Contact_kana__c, extension.record.Opportunity_BillKana__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_BillFax__c)) {
			System.assertEquals(parent.Fax, extension.record.Opportunity_BillFax__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_BillPostalCode__c)) {
			System.assertEquals(parent.MailingPostalCode, extension.record.Opportunity_BillPostalCode__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_BillDepartment__c)) {
			System.assertEquals(parent.Department, extension.record.Opportunity_BillDepartment__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_BillEmail__c)) {
			System.assertEquals(parent.Email, extension.record.Opportunity_BillEmail__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_BillPrefecture__c)) {
			System.assertEquals(parent.MailingState, extension.record.Opportunity_BillPrefecture__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_BillTitle__c)) {
			System.assertEquals(parent.Title, extension.record.Opportunity_BillTitle__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.TransferAccountNumber__c)) {
			System.assertEquals(parent.kouza__c, extension.record.TransferAccountNumber__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_BillCity__c)) {
			System.assertEquals(parent.MailingCity, extension.record.Opportunity_BillCity__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_BillRole__c)) {
			System.assertEquals(parent.Contact_Role__c, extension.record.Opportunity_BillRole__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_BillAddress__c)) {
			System.assertEquals(parent.MailingStreet, extension.record.Opportunity_BillAddress__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_BillBuilding__c)) {
			System.assertEquals(parent.Contact_BuildingName__c, extension.record.Opportunity_BillBuilding__c);
		}

		System.assert(true);
	}
}