global class ScheduledARSummary2 implements Schedulable {
	global void execute(SchedulableContext sc) {
		CronTrigger ct = [SELECT id, CronExpression, StartTime, EndTime FROM CronTrigger WHERE id = :sc.getTriggerId()];
		// 予実集計処理を呼び出す
		System.debug('**** スケジュール開始時刻 : ' + ct.StartTime);
		System.debug('**** START ****');
		AccountReceivableSummary2.AccountReceivableSummary2();
		System.debug('**** E N D ****');
	}
}