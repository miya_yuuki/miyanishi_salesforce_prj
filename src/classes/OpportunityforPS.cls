global with sharing class OpportunityforPS extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public OpportunityProduct__c record{get;set;}	
			
	
		public Component3 Component3 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component107_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component107_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component107_op{get;set;}	
		public List<SelectOption> valueOptions_OpportunityProduct_c_Phase_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component109_val {get;set;}	
		public SkyEditor2.TextHolder Component109_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component17_val {get;set;}	
		public SkyEditor2.TextHolder Component17_op{get;set;}	
			
		public OpportunityProduct__c Component11_val {get;set;}	
		public SkyEditor2.TextHolder Component11_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component163_val {get;set;}	
		public SkyEditor2.TextHolder Component163_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component7_val {get;set;}	
		public SkyEditor2.TextHolder Component7_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component28_val {get;set;}	
		public SkyEditor2.TextHolder Component28_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component15_val {get;set;}	
		public SkyEditor2.TextHolder Component15_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component30_val {get;set;}	
		public SkyEditor2.TextHolder Component30_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component13_val {get;set;}	
		public SkyEditor2.TextHolder Component13_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component122_val {get;set;}	
		public SkyEditor2.TextHolder Component122_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component133_val {get;set;}	
		public SkyEditor2.TextHolder Component133_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component124_val {get;set;}	
		public SkyEditor2.TextHolder Component124_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component135_val {get;set;}	
		public SkyEditor2.TextHolder Component135_op{get;set;}	
			
	public String recordTypeRecordsJSON_OpportunityProduct_c {get; private set;}
	public String defaultRecordTypeId_OpportunityProduct_c {get; private set;}
	public String metadataJSON_OpportunityProduct_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public OpportunityforPS(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = OpportunityProduct__c.fields.Phase__c;
		f = OpportunityProduct__c.fields.Opportunity_Phase__c;
		f = OpportunityProduct__c.fields.SalesPerson__c;
		f = OpportunityProduct__c.fields.Opportunity__c;
		f = OpportunityProduct__c.fields.SalesPersonMainUnit__c;
		f = OpportunityProduct__c.fields.Name;
		f = OpportunityProduct__c.fields.AccountName__c;
		f = OpportunityProduct__c.fields.sisakuompany__c;
		f = OpportunityProduct__c.fields.Opportunity_Competition__c;
		f = OpportunityProduct__c.fields.ProductName_Detail__c;
		f = OpportunityProduct__c.fields.AmountCountMonth__c;
		f = OpportunityProduct__c.fields.CloseMonth__c;
		f = OpportunityProduct__c.fields.AmountCountMonth2__c;
		f = OpportunityProduct__c.fields.StartMonth__c;
		f = OpportunityProduct__c.fields.sisakuompany_kubun__c;
		f = OpportunityProduct__c.fields.CloseDate__c;
		f = OpportunityProduct__c.fields.OrderPercentage__c;
		f = OpportunityProduct__c.fields.Price__c;
		f = OpportunityProduct__c.fields.Quantity__c;
		f = OpportunityProduct__c.fields.Item_keyword_letter_count__c;
		f = OpportunityProduct__c.fields.SalesAmount__c;
		f = OpportunityProduct__c.fields.AmountCountDay__c;
		f = OpportunityProduct__c.fields.SalesForecast01__c;
		f = OpportunityProduct__c.fields.AmountCountDay2__c;
		f = OpportunityProduct__c.fields.SalesForecast02__c;
		f = OpportunityProduct__c.fields.Item_Contract_start_date__c;
		f = OpportunityProduct__c.fields.Item_Contract_end_date__c;
		f = OpportunityProduct__c.fields.Opportunity_kadai1__c;
		f = OpportunityProduct__c.fields.SalesPercentage1__c;
		f = OpportunityProduct__c.fields.AutomaticUpdate__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = OpportunityProduct__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				OpportunityProduct__c lookupObjComponent23 = new OpportunityProduct__c();	
				Component107_val = new SkyEditor2__SkyEditorDummy__c();	
				Component107_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component107_op = new SkyEditor2.TextHolder();	
				valueOptions_OpportunityProduct_c_Phase_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : OpportunityProduct__c.Phase__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_OpportunityProduct_c_Phase_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component109_val = new SkyEditor2__SkyEditorDummy__c();	
				Component109_op = new SkyEditor2.TextHolder();	
					
				Component17_val = new SkyEditor2__SkyEditorDummy__c();	
				Component17_op = new SkyEditor2.TextHolder();	
					
				Component11_val = lookupObjComponent23;	
				Component11_op = new SkyEditor2.TextHolder();	
					
				Component163_val = new SkyEditor2__SkyEditorDummy__c();	
				Component163_op = new SkyEditor2.TextHolder();	
					
				Component7_val = new SkyEditor2__SkyEditorDummy__c();	
				Component7_op = new SkyEditor2.TextHolder();	
					
				Component28_val = new SkyEditor2__SkyEditorDummy__c();	
				Component28_op = new SkyEditor2.TextHolder();	
					
				Component15_val = new SkyEditor2__SkyEditorDummy__c();	
				Component15_op = new SkyEditor2.TextHolder();	
					
				Component30_val = new SkyEditor2__SkyEditorDummy__c();	
				Component30_op = new SkyEditor2.TextHolder();	
					
				Component13_val = new SkyEditor2__SkyEditorDummy__c();	
				Component13_op = new SkyEditor2.TextHolder();	
					
				Component122_val = new SkyEditor2__SkyEditorDummy__c();	
				Component122_op = new SkyEditor2.TextHolder();	
					
				Component133_val = new SkyEditor2__SkyEditorDummy__c();	
				Component133_op = new SkyEditor2.TextHolder();	
					
				Component124_val = new SkyEditor2__SkyEditorDummy__c();	
				Component124_op = new SkyEditor2.TextHolder();	
					
				Component135_val = new SkyEditor2__SkyEditorDummy__c();	
				Component135_op = new SkyEditor2.TextHolder();	
					
				queryMap.put(	
					'Component3',	
					new SkyEditor2.Query('OpportunityProduct__c')
						.addFieldAsOutput('Opportunity__c')
						.addFieldAsOutput('sisakuompany_kubun__c')
						.addFieldAsOutput('Name')
						.addFieldAsOutput('ProductName_Detail__c')
						.addFieldAsOutput('AutomaticUpdate__c')
						.addFieldAsOutput('CloseDate__c')
						.addFieldAsOutput('Opportunity_Phase__c')
						.addFieldAsOutput('OrderPercentage__c')
						.addField('Price__c')
						.addField('Quantity__c')
						.addField('Item_keyword_letter_count__c')
						.addFieldAsOutput('SalesAmount__c')
						.addField('AmountCountDay__c')
						.addField('SalesForecast01__c')
						.addField('AmountCountDay2__c')
						.addField('SalesForecast02__c')
						.addFieldAsOutput('Item_Contract_start_date__c')
						.addFieldAsOutput('Item_Contract_end_date__c')
						.addFieldAsOutput('Opportunity_kadai1__c')
						.addFieldAsOutput('SalesPerson__c')
						.addFieldAsOutput('SalesPercentage1__c')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component107_val_dummy, 'SkyEditor2__Text__c','Phase__c', Component107_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component109_val, 'SkyEditor2__Text__c', 'Opportunity_Phase__c', Component109_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component17_val, 'SkyEditor2__Text__c', 'SalesPerson__c', Component17_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component11_val, 'Opportunity__c', 'Opportunity__c', Component11_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component163_val, 'SkyEditor2__Text__c', 'SalesPersonMainUnit__c', Component163_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component7_val, 'SkyEditor2__Text__c', 'Name', Component7_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component28_val, 'SkyEditor2__Text__c', 'AccountName__c', Component28_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component15_val, 'SkyEditor2__Text__c', 'sisakuompany__c', Component15_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component30_val, 'SkyEditor2__Text__c', 'Opportunity_Competition__c', Component30_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component13_val, 'SkyEditor2__Text__c', 'ProductName_Detail__c', Component13_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component122_val, 'SkyEditor2__Text__c', 'AmountCountMonth__c', Component122_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component133_val, 'SkyEditor2__Text__c', 'CloseMonth__c', Component133_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component124_val, 'SkyEditor2__Text__c', 'AmountCountMonth2__c', Component124_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component135_val, 'SkyEditor2__Text__c', 'StartMonth__c', Component135_op, true, 0, false ))
				);	
					
					Component3 = new Component3(new List<OpportunityProduct__c>(), new List<Component3Item>(), new List<OpportunityProduct__c>(), null);
				listItemHolders.put('Component3', Component3);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(OpportunityProduct__c.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = false;
			execInitialSearch = false;
			presetSystemParams();
			Component3.extender = this.extender;
			initSearch();
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_Phase_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_Opportunity_Phase_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'Opportunity_Phase__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_SalesPerson_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'SalesPerson__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_Opportunity_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'Opportunity__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_SalesPersonMainUnit_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'SalesPersonMainUnit__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_Name() { 
			return getOperatorOptions('OpportunityProduct__c', 'Name');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_AccountName_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'AccountName__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_sisakuompany_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'sisakuompany__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_Opportunity_Competition_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'Opportunity_Competition__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_ProductName_Detail_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'ProductName_Detail__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_AmountCountMonth_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'AmountCountMonth__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_CloseMonth_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'CloseMonth__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_AmountCountMonth2_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'AmountCountMonth2__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_StartMonth_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'StartMonth__c');	
		}	
			
			
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public OpportunityProduct__c record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, OpportunityProduct__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.ListItemHolder {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (OpportunityProduct__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	public OpportunityProduct__c Component3_table_Conversion { get { return new OpportunityProduct__c();}}
	
	public String Component3_table_selectval { get; set; }
	
	
			
	}