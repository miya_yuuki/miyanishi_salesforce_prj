@isTest
private class AgreementMasterRefreshTest {
	static testMethod void testAgreementMasterRefresh() {
		Test.startTest();
		String jobId = System.schedule('test ScheduledAgreementMasterRefresh', '0 0 * * * ?', new ScheduledAgreementMasterRefresh());
		System.debug('**** job id : ' + jobId + ' ***** ***** *****');
		Test.stopTest();
	}
}