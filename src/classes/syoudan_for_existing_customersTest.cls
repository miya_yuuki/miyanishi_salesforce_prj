@isTest
private with sharing class syoudan_for_existing_customersTest{
	private static testMethod void testPageMethods() {		syoudan_for_existing_customers extension = new syoudan_for_existing_customers(new ApexPages.StandardController(new Opportunity()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testComponent6643() {
		syoudan_for_existing_customers.Component6643 Component6643 = new syoudan_for_existing_customers.Component6643(new List<AccountCustomer__c>(), new List<syoudan_for_existing_customers.Component6643Item>(), new List<AccountCustomer__c>(), null);
		Component6643.create(new AccountCustomer__c());
		System.assert(true);
	}
	
	@isTest
	private static void testLightDataTables(){

		System.assert(true);
	}

	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component4751() {
		String testReferenceId = '';
		syoudan_for_existing_customers extension = new syoudan_for_existing_customers(new ApexPages.StandardController(new Opportunity()));
		extension.loadReferenceValues_Component4751();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(Account.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<Account> parents = [SELECT Id FROM Account LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		Account parent = [SELECT Id,BillingMethod__c,PaymentMethod__c FROM Account WHERE Id = :testReferenceId];
		extension.record.AccountId = parent.Id;
		extension.loadReferenceValues_Component4751();
				
		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_BillingMethod__c)) {
			System.assertEquals(parent.BillingMethod__c, extension.record.Opportunity_BillingMethod__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, Opportunity.fields.Opportunity_PaymentMethod__c)) {
			System.assertEquals(parent.PaymentMethod__c, extension.record.Opportunity_PaymentMethod__c);
		}

		System.assert(true);
	}

	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component6659() {
		String testReferenceId = '';
		syoudan_for_existing_customers.Component6643 table = new syoudan_for_existing_customers.Component6643(new List<AccountCustomer__c>(), new List<syoudan_for_existing_customers.Component6643Item>(), new List<AccountCustomer__c>(), null);
		table.add();
		syoudan_for_existing_customers.Component6643Item item = table.items[0];
		item.loadReferenceValues_Component6659();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(Contact.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<Contact> parents = [SELECT Id FROM Contact LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		Contact parent = [SELECT Id,Contact_BillCompany__c,Department,Title,Contact_Role__c,Email,report__c,TOorCC__c FROM Contact WHERE Id = :testReferenceId];
		item.record.CustomerName__c = parent.Id;
		item.loadReferenceValues_Component6659();
				
		if (SkyEditor2.Util.isEditable(item.record, AccountCustomer__c.fields.account__c)) {
			System.assertEquals(parent.Contact_BillCompany__c, item.record.account__c);
		}


		if (SkyEditor2.Util.isEditable(item.record, AccountCustomer__c.fields.department__c)) {
			System.assertEquals(parent.Department, item.record.department__c);
		}


		if (SkyEditor2.Util.isEditable(item.record, AccountCustomer__c.fields.Title__c)) {
			System.assertEquals(parent.Title, item.record.Title__c);
		}


		if (SkyEditor2.Util.isEditable(item.record, AccountCustomer__c.fields.yakuwari__c)) {
			System.assertEquals(parent.Contact_Role__c, item.record.yakuwari__c);
		}


		if (SkyEditor2.Util.isEditable(item.record, AccountCustomer__c.fields.Mail__c)) {
			System.assertEquals(parent.Email, item.record.Mail__c);
		}


		if (SkyEditor2.Util.isEditable(item.record, AccountCustomer__c.fields.Report1__c)) {
			System.assertEquals(parent.report__c, item.record.Report1__c);
		}


		if (SkyEditor2.Util.isEditable(item.record, AccountCustomer__c.fields.ReportSend1__c)) {
			System.assertEquals(parent.TOorCC__c, item.record.ReportSend1__c);
		}

		System.assert(true);
	}
}