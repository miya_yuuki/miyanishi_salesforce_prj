public with sharing class TaskInformationSet {
    public static boolean firstRun = true;

    public static void prcTelephoneAppointmentInformationUpdate(List<Task> objTask) {
        // 商談ID
        List<String> strOpportunityIds = new List<String>();
        // 商談オブジェクト
        List<Opportunity> updOpportunity = new List<Opportunity>();
        // リードID
        List<String> strLeadIds = new List<String>();
        // リードオブジェクト
        List<Lead> updLead = new List<Lead>();
        
        // タスクのループ
        for (Task obj: objTask) {
        	// タスクの種別が「電話」以外の場合は、何も更新しない
        	// (最終架電日・最終接続日などは「電話」の場合のみ更新したいため)	
        	if (obj.summary_type__c != '電話') continue;
        	
            // 商談の参照関係が存在する場合
            if (obj.WhatId != null) {
                strOpportunityIds.add(obj.WhatId);
                updOpportunity = [SELECT Id, lastTereapoDay__c FROM Opportunity WHERE Id = :strOpportunityIds];
                for (Opportunity objNew: updOpportunity) {
                    // テレアポの架電日時を代入
                    if (obj.ActivityDate != null) {
                        if (obj.detail_type__c == '接続' || obj.detail_type__c == 'レコード' || obj.detail_type__c == 'アポ') {
                            objNew.lastConnectionDay__c = obj.ActivityDate;
                        }
                        objNew.lastTereapoDay__c = obj.ActivityDate;
                    }
                    else {
                        if (obj.detail_type__c == '接続' || obj.detail_type__c == 'レコード' || obj.detail_type__c == 'アポ') {
                            objNew.lastConnectionDay__c = obj.CreatedDate;
                        }
                        objNew.lastTereapoDay__c = obj.CreatedDate;
                    }
                    // タスクの通話メモを代入(nullの場合はnullを挿入)
                    if (obj.detail_type__c == '接続' || obj.detail_type__c == 'レコード' || obj.detail_type__c == 'アポ') {
                        objNew.lastConnectionMemo__c = obj.Description;
                    }
                    objNew.lastTereapoMemo__c = obj.Description;
                }
                if (updOpportunity.size() > 0) {
                    update updOpportunity;
                }
            } else if (obj.WhoId != null) {
            	// ParentI
        		String strWhoId = '';
        		strWhoId = obj.WhoId;
        		
        		// WhoIdがリードであった場合
                if (strWhoId.substring(0, 3) != '00Q') {
                    continue;
                }
                strLeadIds.add(obj.WhoId);
                updLead = [SELECT Id, lastTereapoDay__c FROM Lead WHERE Id = :strLeadIds];
                for (Lead objNew: updLead) {
                    // テレアポの架電日時を代入
                    if (obj.ActivityDate != null) {
                        if (obj.detail_type__c == '接続' || obj.detail_type__c == 'レコード' || obj.detail_type__c == 'アポ') {
                            objNew.lastConnectionDay__c = obj.ActivityDate;
                        }
                        objNew.lastTereapoDay__c = obj.ActivityDate;
                    }
                    else {
                        if (obj.detail_type__c == '接続' || obj.detail_type__c == 'レコード' || obj.detail_type__c == 'アポ') {
                            objNew.lastConnectionDay__c = obj.CreatedDate;
                        }
                        objNew.lastTereapoDay__c = obj.CreatedDate;
                    }
                    // テレアポの通話メモを代入
                    if (obj.detail_type__c == '接続' || obj.detail_type__c == 'レコード' || obj.detail_type__c == 'アポ') {
                        objNew.lastConnectionMemo__c = obj.Description;
                    }
                    objNew.lastTereapoMemo__c = obj.Description;
                }
                if (updLead.size() > 0) {
              		update updLead;
            	}
            }
        }
    }
}