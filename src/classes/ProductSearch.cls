global with sharing class ProductSearch extends SkyEditor2.SkyEditorPageBaseWithSharing{
	
	public Product__c record{get;set;}
	public Component2 Component2 {get; private set;}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	public SkyEditor2__SkyEditorDummy__c Component48{get;set;}
	public List<SelectOption> Component48_options {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component65{get;set;}
	public List<SelectOption> Component65_options {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component6{get;set;}
	{
	setApiVersion(31.0);
	}
	public ProductSearch(ApexPages.StandardController controller){
		super(controller);

		SObjectField f;

		f = Product__c.fields.Name;
		f = Product__c.fields.code__c;
		f = Product__c.fields.Product_DeliveryExistence__c;
		f = Product__c.fields.ProductSeparateUMU__c;
		f = Product__c.fields.Classification__c;
		f = Product__c.fields.ProductContent__c;
        f = Product__c.fields.ProductRecordType__c;
        f = Product__c.fields.Product_DeliveryExistence__c;
		f = Product__c.fields.validity__c;

		try {
			mainRecord = null;
			mainSObjectType = Product__c.SObjectType;
			mode = SkyEditor2.LayoutMode.TempProductLookup_01;
			
			Component48 = new SkyEditor2__SkyEditorDummy__c();
			Component65 = new SkyEditor2__SkyEditorDummy__c();
			Component6 = new SkyEditor2__SkyEditorDummy__c();
			
			queryMap.put(
				'Component2',
				new SkyEditor2.Query('Product__c')
					.addFieldAsOutput('code__c')
					.addFieldAsOutput('Name')
					.addFieldAsOutput('Product_DeliveryExistence__c')
					.addFieldAsOutput('ProductSeparateUMU__c')
					.addFieldAsOutput('Classification__c')
					.addFieldAsOutput('ProductContent__c')
					.addField('Name')
					.limitRecords(500)
					.addListener(new SkyEditor2.QueryWhereRegister(Component48, 'SkyEditor2__Text__c', 'ProductRecordType__c', new SkyEditor2.TextHolder('eq'), false, true, false))
					.addListener(new SkyEditor2.QueryWhereRegister(Component65, 'SkyEditor2__Text__c', 'Product_DeliveryExistence__c', new SkyEditor2.TextHolder('eq'), false, true, false))
					.addListener(new SkyEditor2.QueryWhereRegister(Component6, 'SkyEditor2__Text__c', 'Name', new SkyEditor2.TextHolder('co'), false, true, false))
					 .addWhere(' ( validity__c = true)')
			);
			
			Component2 = new Component2(new List<Product__c>(), new List<Component2Item>(), new List<Product__c>(), null);
			listItemHolders.put('Component2', Component2);
			Component2.ignoredOnSave = true;
			
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(Product__c.SObjectType);
			
			p_showHeader = false;
			p_sidebar = false;
			presetSystemParams();
			update_Component48_options();
			update_Component65_options();
			initSearch();
			
		} catch (SkyEditor2.Errors.SObjectNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.Errors.FieldNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.ExtenderException e){
			e.setMessagesToPage();
		} catch (SkyEditor2.Errors.PricebookNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
			hidePageBody = true;
		}
	}

	public List<SelectOption> getOperatorOptions_Product_c_Name() {
		return getOperatorOptions('Product__c', 'Name');
	}
	
	
	global with sharing class Component2Item extends SkyEditor2.ListItem {
		public Product__c record{get; private set;}
		@TestVisible
		Component2Item(Component2 holder, Product__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component2 extends SkyEditor2.ListItemHolder {
		public List<Component2Item> items{get; private set;}
		@TestVisible
			Component2(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component2Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component2Item(this, (Product__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	
	public void update_Component48_options() {
		Component48_options = new SelectOption[]{ new SelectOption('', label.none) };
			SObject[] results;
			String soql = 'SELECT ProductRecordType__c FROM Product__c WHERE ProductRecordType__c != null ';
			soql += 'AND( validity__c = true)';
			soql += 'GROUP BY ProductRecordType__c ORDER BY ProductRecordType__c LIMIT 999';
			results = Database.query(soql);
			for (SObject r : results) {
				String value = (String)(r.get('ProductRecordType__c'));
				Component48_options.add(new SelectOption(value, value));
			}
	}
	
	public void update_Component65_options() {
		Component65_options = new SelectOption[]{ new SelectOption('', label.none) };
		if (Component48.SkyEditor2__Text__c != null && Component48.SkyEditor2__Text__c != '') {
			SObject[] results;
			String soql = 'SELECT Product_DeliveryExistence__c FROM Product__c WHERE Product_DeliveryExistence__c != null ';
			soql += 'AND ProductRecordType__c=\'' + Component48.SkyEditor2__Text__c + '\' ';
			soql += 'AND( validity__c = true)';
			soql += 'GROUP BY Product_DeliveryExistence__c ORDER BY Product_DeliveryExistence__c LIMIT 999';
			results = Database.query(soql);
			for (SObject r : results) {
				String value = (String)(r.get('Product_DeliveryExistence__c'));
				Component65_options.add(new SelectOption(value, value));
			}
		}
	}
	
	public void Component48_ChangeListener() {
		Component65.SkyEditor2__Text__c = '';
		doSearch();
		update_Component65_options();
	}
}