global with sharing class RequestChange_view extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public CustomObject1__c record {get{return (CustomObject1__c)mainRecord;}}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	{
	setApiVersion(42.0);
	}
	public RequestChange_view(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = CustomObject1__c.fields.Name;
		f = CustomObject1__c.fields.Status__c;
		f = CustomObject1__c.fields.unit__c;
		f = CustomObject1__c.fields.RequestDay__c;
		f = CustomObject1__c.fields.OwnerId;
		f = CustomObject1__c.fields.RequestEndDay__c;
		f = CustomObject1__c.fields.SalesSupportPerson__c;
		f = CustomObject1__c.fields.SupportJudgePerson__c;
		f = CustomObject1__c.fields.ProductGroupgazou__c;
		f = CustomObject1__c.fields.SupportDay__c;
		f = CustomObject1__c.fields.OrderNo__c;
		f = CustomObject1__c.fields.SupportMember__c;
		f = CustomObject1__c.fields.OrderNo2__c;
		f = CustomObject1__c.fields.RDDay__c;
		f = CustomObject1__c.fields.Account__c;
		f = CustomObject1__c.fields.RecordTypeId;
		f = CustomObject1__c.fields.Sign__c;
		f = CustomObject1__c.fields.OrderProvisional1__c;
		f = CustomObject1__c.fields.OrderProvisionalMemo1__c;
		f = CustomObject1__c.fields.SalvageScheduledDate__c;
		f = CustomObject1__c.fields.ProductGroup__c;
		f = CustomObject1__c.fields.sentaku__c;
		f = CustomObject1__c.fields.MemoPrint__c;
		f = CustomObject1__c.fields.ChangeType__c;
		f = CustomObject1__c.fields.BillStopType1__c;
		f = CustomObject1__c.fields.BillStopType__c;
		f = CustomObject1__c.fields.henkoumae__c;
		f = CustomObject1__c.fields.henkougo__c;
		f = CustomObject1__c.fields.etcChangeDay__c;
		f = CustomObject1__c.fields.RequestMemo__c;
		f = CustomObject1__c.fields.Patnername1__c;
		f = CustomObject1__c.fields.Patnername2__c;
		f = CustomObject1__c.fields.SalesPerson__c;
		f = CustomObject1__c.fields.UnitSalesPerson2__c;
		f = CustomObject1__c.fields.SalesPerson1__c;
		f = CustomObject1__c.fields.SalesPerson2__c;
		f = CustomObject1__c.fields.SalesSupportPersonView__c;
		f = CustomObject1__c.fields.SupportPerson2__c;
		f = CustomObject1__c.fields.SalesPersonDay__c;
		f = CustomObject1__c.fields.Account2__c;
		f = CustomObject1__c.fields.Customer__c;
		f = CustomObject1__c.fields.Billtantou__c;
		f = CustomObject1__c.fields.BillDepartment__c;
		f = CustomObject1__c.fields.BillDepartment2__c;
		f = CustomObject1__c.fields.BillTitle__c;
		f = CustomObject1__c.fields.BillTitle2__c;
		f = CustomObject1__c.fields.CustomerChangeDay__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = CustomObject1__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'RequestChange_view';
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(CustomObject1__c.SObjectType);
			mainQuery = new SkyEditor2.Query('CustomObject1__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('Status__c');
			mainQuery.addFieldAsOutput('unit__c');
			mainQuery.addFieldAsOutput('RequestDay__c');
			mainQuery.addFieldAsOutput('OwnerId');
			mainQuery.addFieldAsOutput('RequestEndDay__c');
			mainQuery.addFieldAsOutput('SalesSupportPerson__c');
			mainQuery.addFieldAsOutput('SupportJudgePerson__c');
			mainQuery.addFieldAsOutput('ProductGroupgazou__c');
			mainQuery.addFieldAsOutput('SupportDay__c');
			mainQuery.addFieldAsOutput('OrderNo__c');
			mainQuery.addFieldAsOutput('SupportMember__c');
			mainQuery.addFieldAsOutput('OrderNo2__c');
			mainQuery.addFieldAsOutput('RDDay__c');
			mainQuery.addFieldAsOutput('Account__c');
			mainQuery.addFieldAsOutput('RecordType.Name');
			mainQuery.addFieldAsOutput('Sign__c');
			mainQuery.addFieldAsOutput('OrderProvisional1__c');
			mainQuery.addFieldAsOutput('OrderProvisionalMemo1__c');
			mainQuery.addFieldAsOutput('SalvageScheduledDate__c');
			mainQuery.addFieldAsOutput('ProductGroup__c');
			mainQuery.addFieldAsOutput('sentaku__c');
			mainQuery.addFieldAsOutput('MemoPrint__c');
			mainQuery.addFieldAsOutput('ChangeType__c');
			mainQuery.addFieldAsOutput('BillStopType1__c');
			mainQuery.addFieldAsOutput('BillStopType__c');
			mainQuery.addFieldAsOutput('henkoumae__c');
			mainQuery.addFieldAsOutput('henkougo__c');
			mainQuery.addFieldAsOutput('etcChangeDay__c');
			mainQuery.addFieldAsOutput('RequestMemo__c');
			mainQuery.addFieldAsOutput('Patnername1__c');
			mainQuery.addFieldAsOutput('Patnername2__c');
			mainQuery.addFieldAsOutput('SalesPerson__c');
			mainQuery.addFieldAsOutput('UnitSalesPerson2__c');
			mainQuery.addFieldAsOutput('SalesPerson1__c');
			mainQuery.addFieldAsOutput('SalesPerson2__c');
			mainQuery.addFieldAsOutput('SalesSupportPersonView__c');
			mainQuery.addFieldAsOutput('SupportPerson2__c');
			mainQuery.addFieldAsOutput('SalesPersonDay__c');
			mainQuery.addFieldAsOutput('Account2__c');
			mainQuery.addFieldAsOutput('Customer__c');
			mainQuery.addFieldAsOutput('Billtantou__c');
			mainQuery.addFieldAsOutput('BillDepartment__c');
			mainQuery.addFieldAsOutput('BillDepartment2__c');
			mainQuery.addFieldAsOutput('BillTitle__c');
			mainQuery.addFieldAsOutput('BillTitle2__c');
			mainQuery.addFieldAsOutput('CustomerChangeDay__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			if (record.Id == null) {
				saveOldValues();
				if(record.RecordTypeId == null) recordTypeSelector.applyDefault(record);
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}