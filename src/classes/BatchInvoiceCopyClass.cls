global class BatchInvoiceCopyClass implements Database.Batchable<sObject> {

	// SOQL用
	public String query;
    //1回のexecuteメソッドで処理される件数
    public final Integer BATCH_SIZE = 1;
    
	// 請求データ作成バッチオブジェクト
	public BillCreateBatch__c objBatch { get; set; }
    // 件数チェック用
    public List<OrderItem__c> objCheck { get; set; }

    // イニシャライズ
	global BatchInvoiceCopyClass()
	{	
		// 請求データ作成バッチオブジェクトのインスタンスを生成
		objBatch = new BillCreateBatch__c();
	}
	
    //バッチ実行
    public PageReference doStartBatch() 
    {
    	// エラーチェック
    	if (prcErrCheck()) return null;
    	
 		// クエリ作成
 		query = prcCreateQuery();
 		
        //どのオブジェクトのレコードに対し処理を実行するのかSOQLを投げる
        BatchInvoiceCopyClass batch = new BatchInvoiceCopyClass();
        
        // パラメータをコピー
        batch.objBatch = objBatch.clone(false, true);
        
        // バッチ実行
        Database.executeBatch(batch, BATCH_SIZE);
        // 完了メッセージ
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '請求データ作成バッチを実行しました'));
        
        return null;
    }
	
	// 件数チェック
	public PageReference doCountCheck()
	{
    	// エラーチェック
    	if (prcErrCheck()) return null;
 		// クエリ作成
 		query = prcCreateQuery();
 		// 件数チェック
 		objCheck = Database.query(query);
 		
 		// 追加メッセージ
 		String strOrderItemIds = '<BR><BR>■対象注文商品<BR>';
 		for (OrderItem__c obj :objCheck) {
 			strOrderItemIds += obj.Item2_RelationView__c + ' : ' + obj.Name + '<BR>';
 		}
 		// 50件まで
 		if (objCheck.size() > 50) {
 			strOrderItemIds = '';
 		}
        // 完了メッセージ
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, objCheck.size() + '件の注文商品が請求データにコピーされます' + strOrderItemIds));
        
        
 		return null;
	}
	
	public PageReference doCompletedCheck() {
		// 納品管理オブジェクト
		List<TextManagement2__c> updTextManagement = new List<TextManagement2__c>();
		
		updTextManagement = [
			SELECT Id
			FROM TextManagement2__c
			WHERE CloudStatus__c = '社外納品済み'
			AND Item2_DeliveryExistence__c = '有'
			AND OrderItem_PaymentDueDate__c = null
			AND OrderItem_DeliveryCountTotal__c >= 0
			AND ProductSeparateUMU__c = '無'
			AND OrderItemStatus__c NOT IN ('契約変更', '途中解約', '請求停止')
			AND (NOT Product__c like '%社内発注%')
			AND (NOT Account1__c like '%テスト用%')
			AND CreatedDate > 2015-08-31T10:00:00+09:00
			limit 300
		];
		
		if (updTextManagement.size() > 0) {
			update updTextManagement;
			System.debug('納品管理：更新対象' + updTextManagement.size() + '件');
		}
		else {
			System.debug('納品管理：更新対象なし');
		}
		
		// 完了メッセージ
		String resultText = '納品管理：更新対象 ' + updTextManagement.size() + '件';
		try {
			if(ApexPages.currentPage() != null) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, resultText));
			}
			else {
				System.debug(resultText);
			}
		}
		catch (Exception ex) {
			System.debug(resultText);
		}
		
		return null;
	}
	
	// エラーチェック
	private Boolean prcErrCheck()
	{
    	// エラーメッセージ
    	String strErrMsg = '';
    	
    	// 支払期日Fromは入力必須
    	if (objBatch.From__c == null) strErrMsg += '支払期日: Fromを入力して下さい<br/>';
    	// 支払期日Toは入力必須
    	if (objBatch.To__c == null) strErrMsg += '支払期日: Fromを入力して下さい<br/>';
    	// From > Toはエラー
    	if (objBatch.From__c > objBatch.To__c) strErrMsg += '支払期日のToは、Fromより過去の日付を入力して下さい';
    	
     	// エラーメッセージを表示
    	if (strErrMsg != '') {
	        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, strErrMsg));
	        return true;
    	}
    	
    	return false;
	}
	
	// クエリ作成
	private String prcCreateQuery()
	{
		// Queryを生成
		String q = 
			'SELECT ' +
				'Id, ' +
				'Name, ' +
				'ProductSeparateUMU__c, ' +
				'BillingTiming__c, ' +
				'Item2_RelationView__c, ' +
				'Billchek__c, ' +
				'Billcheck1__c, ' +
				'Billcheck2__c, ' +
				'Billcheck3__c, ' +
				'Billcheck4__c, ' +
				'Bill_Main__c, ' +
				'Bill_Day1__c, ' +
				'Bill_Day2__c, ' +
				'Bill_Day3__c, ' +
				'Bill_Day4__c ' +
			'FROM ' +
				'OrderItem__c ' +
			'WHERE ' +
			'(' +
				'(ProductSeparateUMU__c = \'無\' AND Bill_Main__c >= ' + ur(objBatch.From__c) + ' AND Bill_Main__c <=' + ur(objBatch.To__c) + ' AND Billchek__c = false) OR ' +
				'(ProductSeparateUMU__c = \'有\' AND BillingTiming__c = \'受注\' AND Bill_Main__c >= ' + ur(objBatch.From__c) + ' AND Bill_Main__c <=' + ur(objBatch.To__c) + ' AND Billchek__c = false) OR ' +
				'(ProductSeparateUMU__c = \'有\' AND BillingTiming__c != \'受注\' AND Bill_Day1__c >= ' + ur(objBatch.From__c) + ' AND Bill_Day1__c <=' + ur(objBatch.To__c) + ' AND Billcheck1__c = false) OR ' +
				'(ProductSeparateUMU__c = \'有\' AND BillingTiming__c != \'受注\' AND Bill_Day2__c >= ' + ur(objBatch.From__c) + ' AND Bill_Day2__c <=' + ur(objBatch.To__c) + ' AND Billcheck2__c = false) OR ' +
				'(ProductSeparateUMU__c = \'有\' AND BillingTiming__c != \'受注\' AND Bill_Day3__c >= ' + ur(objBatch.From__c) + ' AND Bill_Day3__c <=' + ur(objBatch.To__c) + ' AND Billcheck3__c = false) OR ' +
				'(ProductSeparateUMU__c = \'有\' AND BillingTiming__c != \'受注\' AND Bill_Day4__c >= ' + ur(objBatch.From__c) + ' AND Bill_Day4__c <=' + ur(objBatch.To__c) + ' AND Billcheck4__c = false)' +
			') ' +
			'AND InvoiceJoken__c = true ';
		
		// 取引先責任者のID
		List<String> contactIds = new List<String>();
		
		if (objBatch.Contact1__c != null) contactIds.add(objBatch.Contact1__c);	// 取引先責任者1の指定
		if (objBatch.Contact2__c != null) contactIds.add(objBatch.Contact2__c);	// 取引先責任者2の指定
		if (objBatch.Contact3__c != null) contactIds.add(objBatch.Contact3__c);	// 取引先責任者3の指定
		if (objBatch.Contact4__c != null) contactIds.add(objBatch.Contact4__c);	// 取引先責任者4の指定
		if (objBatch.Contact5__c != null) contactIds.add(objBatch.Contact5__c);	// 取引先責任者5の指定
		if (objBatch.Contact6__c != null) contactIds.add(objBatch.Contact6__c);	// 取引先責任者6の指定
		if (objBatch.Contact7__c != null) contactIds.add(objBatch.Contact7__c);	// 取引先責任者7の指定
		if (objBatch.Contact8__c != null) contactIds.add(objBatch.Contact8__c);	// 取引先責任者8の指定
		if (objBatch.Contact9__c != null) contactIds.add(objBatch.Contact9__c);	// 取引先責任者9の指定
		if (objBatch.Contact10__c != null) contactIds.add(objBatch.Contact10__c);	// 取引先責任者10の指定
		
		if (contactIds.size() > 0) {
			q += 'AND (Item2_Relation__r.BillTo_Contact__c IN ' + prcCreateIds(contactIds) + ')';
		}
		return q;
	}
	
	// IDを文字列化
	private static String prcCreateIds(List<String> strIds)
	{
		if (strIds.size() == 0) return '';
		String q = '(';
		for (String str : strIds) q += '\'' + str + '\',';
		q = q.substring(0, q.length() - 1);
		q += ')';
		return q;
	}
	
	// Utillクラスの日付を文字列に変換を省略
	private static String ur(Date dt)
	{
		
		return UtilityClass.retStringDt(dt);
	}
	
	// 日付が指定の期間内かをチェック
	private static Boolean betDt(Date dt, BillCreateBatch__c obj)
	{
		if (dt >= obj.From__c && dt <= obj.To__c) return true;
		return false;
	}
	
    //バッチ開始処理
    //開始するためにqueryを実行する。この実行されたSOQLのデータ分処理する。
    //5千万件以上のレコードになるとエラーになる。
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
    	query = prcCreateQuery();
    	System.debug('*************s***:' + query);
        return Database.getQueryLocator(query);
    }
    
    //バッチ処理内容
    //scopeにgetQueryLocatorの内容がバッチサイズ分格納されてくる
    global void execute(Database.BatchableContext BC, List<sObject> scope) 
    {
    	// 更新用の注文商品
    	List<OrderItem__c> updOrderItem = new List<OrderItem__c>();
    	
		for(sObject obj : scope) {
			// 注文商品のインスタンス
			OrderItem__c res = new OrderItem__c();
			// 注文商品をコピー
			res = (OrderItem__c)obj.clone(true, true);
			
	    	Boolean flg = res.Billchek__c;		// 分割納品無＆支払期日期間内
	    	Boolean flg1 = res.Billcheck1__c;	// 分割納品有＆支払期日①期間内
	    	Boolean flg2 = res.Billcheck2__c;	// 分割納品有＆支払期日②期間内
	    	Boolean flg3 = res.Billcheck3__c;	// 分割納品有＆支払期日③期間内
	    	Boolean flg4 = res.Billcheck4__c;	// 分割納品有＆支払期日④期間内
    	
    		// 分割納品無＆支払期日期間内
			if (res.ProductSeparateUMU__c == '無' && betDt(res.Bill_Main__c, objBatch)) flg = true;
    		// 分割納品無＆支払期日期間内
			if (res.ProductSeparateUMU__c == '有' && res.BillingTiming__c == '受注' && betDt(res.Bill_Main__c, objBatch)) flg = true;
			// 分割納品有＆支払期日①期間内
			if (res.ProductSeparateUMU__c == '有' && res.BillingTiming__c != '受注' && betDt(res.Bill_Day1__c, objBatch)) flg1 = true;
			// 分割納品有＆支払期日②期間内
			if (res.ProductSeparateUMU__c == '有' && res.BillingTiming__c != '受注' && betDt(res.Bill_Day2__c, objBatch)) flg2 = true;
			// 分割納品有＆支払期日③期間内
			if (res.ProductSeparateUMU__c == '有' && res.BillingTiming__c != '受注' && betDt(res.Bill_Day3__c, objBatch)) flg3 = true;
			// 分割納品有＆支払期日④期間内
			if (res.ProductSeparateUMU__c == '有' && res.BillingTiming__c != '受注' && betDt(res.Bill_Day4__c, objBatch)) flg4 = true;
			
			// 注文商品のインスタンスを生成
			OrderItem__c newItm = new OrderItem__c(
				Id = res.Id,
				Billchek__c = flg,
				Billcheck1__c = flg1,
				Billcheck2__c = flg2,
				Billcheck3__c = flg3,
				Billcheck4__c = flg4
			);
			// 配列に追加
			updOrderItem.add(newItm);
		}
		// 注文商品を更新
		if (updOrderItem.size() > 0) update updOrderItem;
	}
	    
	global void finish(Database.BatchableContext BC){}
}