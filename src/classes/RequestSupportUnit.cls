global with sharing class RequestSupportUnit extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public CustomObject1__c record{get;set;}	
			
	
		public Component3 Component3 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component97_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component97_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component97_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component97_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component88_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component88_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component88_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component88_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component137_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component137_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component137_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component137_isNull_op{get;set;}	
			
		public CustomObject1__c Component6_val {get;set;}	
		public SkyEditor2.TextHolder Component6_op{get;set;}	
			
		public CustomObject1__c Component8_val {get;set;}	
		public SkyEditor2.TextHolder Component8_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component10_val {get;set;}	
		public SkyEditor2.TextHolder Component10_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component52_val {get;set;}	
		public SkyEditor2.TextHolder Component52_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component47_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component47_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component47_op{get;set;}	
		public List<SelectOption> valueOptions_CustomObject1_c_StatusSupport_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component63_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component63_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component63_op{get;set;}	
		public List<SelectOption> valueOptions_CustomObject1_c_Status_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component136_val {get;set;}	
		public SkyEditor2.TextHolder Component136_op{get;set;}	
		public List<SelectOption> valueOptions_CustomObject1_c_SupportJudgePerson_c {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component66_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component66_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component66_op{get;set;}	
		public List<SelectOption> valueOptions_CustomObject1_c_SupportMember_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component123_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component123_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component123_op{get;set;}	
		public List<SelectOption> valueOptions_CustomObject1_c_NextCheck_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component111_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component111_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component111_op{get;set;}	
		public List<SelectOption> valueOptions_CustomObject1_c_ProductGroup_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component74_val {get;set;}	
		public SkyEditor2.TextHolder Component74_op{get;set;}	
			
	public String recordTypeRecordsJSON_CustomObject1_c {get; private set;}
	public String defaultRecordTypeId_CustomObject1_c {get; private set;}
	public String metadataJSON_CustomObject1_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public RequestSupportUnit(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = CustomObject1__c.fields.OrderNo__c;
		f = CustomObject1__c.fields.OwnerId;
		f = CustomObject1__c.fields.RecordTypeId;
		f = CustomObject1__c.fields.AccountName__c;
		f = CustomObject1__c.fields.StatusSupport__c;
		f = CustomObject1__c.fields.Status__c;
		f = CustomObject1__c.fields.SupportJudgePerson__c;
		f = CustomObject1__c.fields.SupportMember__c;
		f = CustomObject1__c.fields.NextCheck__c;
		f = CustomObject1__c.fields.ProductGroup__c;
		f = CustomObject1__c.fields.Name;
		f = CustomObject1__c.fields.SupportDay__c;
		f = CustomObject1__c.fields.sentaku__c;
 f = CustomObject1__c.fields.RequestEndDay__c;
 f = CustomObject1__c.fields.SupportDay__c;
 f = CustomObject1__c.fields.NextCheckDay__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = CustomObject1__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component97_from = new SkyEditor2__SkyEditorDummy__c();	
				Component97_to = new SkyEditor2__SkyEditorDummy__c();	
				Component97_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component97_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component88_from = new SkyEditor2__SkyEditorDummy__c();	
				Component88_to = new SkyEditor2__SkyEditorDummy__c();	
				Component88_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component88_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component137_from = new SkyEditor2__SkyEditorDummy__c();	
				Component137_to = new SkyEditor2__SkyEditorDummy__c();	
				Component137_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component137_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				CustomObject1__c lookupObjComponent30 = new CustomObject1__c();	
				Component6_val = lookupObjComponent30;	
				Component6_op = new SkyEditor2.TextHolder();	
					
				Component8_val = lookupObjComponent30;	
				Component8_op = new SkyEditor2.TextHolder();	
					
				Component10_val = new SkyEditor2__SkyEditorDummy__c();	
				Component10_op = new SkyEditor2.TextHolder();	
					
				Component52_val = new SkyEditor2__SkyEditorDummy__c();	
				Component52_op = new SkyEditor2.TextHolder();	
					
				Component47_val = new SkyEditor2__SkyEditorDummy__c();	
				Component47_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component47_op = new SkyEditor2.TextHolder();	
				valueOptions_CustomObject1_c_StatusSupport_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : CustomObject1__c.StatusSupport__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_CustomObject1_c_StatusSupport_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component63_val = new SkyEditor2__SkyEditorDummy__c();	
				Component63_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component63_op = new SkyEditor2.TextHolder();	
				valueOptions_CustomObject1_c_Status_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : CustomObject1__c.Status__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_CustomObject1_c_Status_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component136_val = new SkyEditor2__SkyEditorDummy__c();	
				Component136_op = new SkyEditor2.TextHolder();	
				valueOptions_CustomObject1_c_SupportJudgePerson_c = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : CustomObject1__c.SupportJudgePerson__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_CustomObject1_c_SupportJudgePerson_c.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component66_val = new SkyEditor2__SkyEditorDummy__c();	
				Component66_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component66_op = new SkyEditor2.TextHolder();	
				valueOptions_CustomObject1_c_SupportMember_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : CustomObject1__c.SupportMember__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_CustomObject1_c_SupportMember_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component123_val = new SkyEditor2__SkyEditorDummy__c();	
				Component123_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component123_op = new SkyEditor2.TextHolder();	
				valueOptions_CustomObject1_c_NextCheck_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : CustomObject1__c.NextCheck__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_CustomObject1_c_NextCheck_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component111_val = new SkyEditor2__SkyEditorDummy__c();	
				Component111_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component111_op = new SkyEditor2.TextHolder();	
				valueOptions_CustomObject1_c_ProductGroup_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : CustomObject1__c.ProductGroup__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_CustomObject1_c_ProductGroup_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component74_val = new SkyEditor2__SkyEditorDummy__c();	
				Component74_op = new SkyEditor2.TextHolder();	
					
				queryMap.put(	
					'Component3',	
					new SkyEditor2.Query('CustomObject1__c')
						.addField('StatusSupport__c')
						.addField('SupportJudgePerson__c')
						.addField('SupportMember__c')
						.addFieldAsOutput('SupportDay__c')
						.addField('NextCheck__c')
						.addFieldAsOutput('AccountName__c')
						.addFieldAsOutput('RecordType.Name')
						.addFieldAsOutput('ProductGroup__c')
						.addFieldAsOutput('sentaku__c')
						.addFieldAsOutput('Name')
						.addFieldAsOutput('OwnerId')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component97_from, 'SkyEditor2__Date__c', 'RequestEndDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component97_to, 'SkyEditor2__Date__c', 'RequestEndDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component97_isNull, 'SkyEditor2__Date__c', 'RequestEndDay__c', Component97_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component88_from, 'SkyEditor2__Date__c', 'SupportDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component88_to, 'SkyEditor2__Date__c', 'SupportDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component88_isNull, 'SkyEditor2__Date__c', 'SupportDay__c', Component88_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component137_from, 'SkyEditor2__Date__c', 'NextCheckDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component137_to, 'SkyEditor2__Date__c', 'NextCheckDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component137_isNull, 'SkyEditor2__Date__c', 'NextCheckDay__c', Component137_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component6_val, 'OrderNo__c', 'OrderNo__c', Component6_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component8_val, 'OwnerId', 'OwnerId', Component8_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component10_val, 'SkyEditor2__Text__c', 'RecordTypeId', Component10_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component52_val, 'SkyEditor2__Text__c', 'AccountName__c', Component52_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component47_val_dummy, 'SkyEditor2__Text__c','StatusSupport__c', Component47_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component63_val_dummy, 'SkyEditor2__Text__c','Status__c', Component63_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component136_val, 'SkyEditor2__Text__c', 'SupportJudgePerson__c', Component136_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component66_val_dummy, 'SkyEditor2__Text__c','SupportMember__c', Component66_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component123_val_dummy, 'SkyEditor2__Text__c','NextCheck__c', Component123_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component111_val_dummy, 'SkyEditor2__Text__c','ProductGroup__c', Component111_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component74_val, 'SkyEditor2__Text__c', 'Name', Component74_op, true, 0, false ))
				);	
					
					Component3 = new Component3(new List<CustomObject1__c>(), new List<Component3Item>(), new List<CustomObject1__c>(), null);
				 Component3.setPageItems(new List<Component3Item>());
				 Component3.setPageSize(100);
				listItemHolders.put('Component3', Component3);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(CustomObject1__c.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = true;
			execInitialSearch = false;
			presetSystemParams();
			Component3.extender = this.extender;
			initSearch();
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_CustomObject1_c_OrderNo_c() { 
			return getOperatorOptions('CustomObject1__c', 'OrderNo__c');	
		}	
		public List<SelectOption> getOperatorOptions_CustomObject1_c_OwnerId() { 
			return getOperatorOptions('CustomObject1__c', 'OwnerId');	
		}	
		public List<SelectOption> getOperatorOptions_CustomObject1_c_RecordTypeId() { 
			return getOperatorOptions('CustomObject1__c', 'RecordTypeId');	
		}	
		public List<SelectOption> getOperatorOptions_CustomObject1_c_AccountName_c() { 
			return getOperatorOptions('CustomObject1__c', 'AccountName__c');	
		}	
		public List<SelectOption> getOperatorOptions_CustomObject1_c_StatusSupport_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_CustomObject1_c_Status_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_CustomObject1_c_SupportJudgePerson_c() { 
			return getOperatorOptions('CustomObject1__c', 'SupportJudgePerson__c');	
		}	
		public List<SelectOption> getOperatorOptions_CustomObject1_c_SupportMember_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_CustomObject1_c_NextCheck_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_CustomObject1_c_ProductGroup_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_CustomObject1_c_Name() { 
			return getOperatorOptions('CustomObject1__c', 'Name');	
		}	
			
			
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public CustomObject1__c record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, CustomObject1__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.PagingList {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (CustomObject1__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
		public void doFirst(){first();}
		public void doPrevious(){previous();}
		public void doNext(){next();}
		public void doLast(){last();}
		public void doSort(){sort();}

        public List<Component3Item> getViewItems() {            return (List<Component3Item>) getPageItems();        }
	}

	public CustomObject1__c Component3_table_Conversion { get { return new CustomObject1__c();}}
	
	public String Component3_table_selectval { get; set; }
	
	
			
	}