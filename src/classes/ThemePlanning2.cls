global with sharing class ThemePlanning2 extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public ThemePlanning__c record{get;set;}	
			
	
		public Component3 Component3 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component22_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component22_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component22_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component22_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component64_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component64_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component64_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component64_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component66_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component66_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component66_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component66_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component68_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component68_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component68_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component68_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component9_val {get;set;}	
		public SkyEditor2.TextHolder Component9_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component11_val {get;set;}	
		public SkyEditor2.TextHolder Component11_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component13_val {get;set;}	
		public SkyEditor2.TextHolder Component13_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component15_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component15_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component15_op{get;set;}	
		public List<SelectOption> valueOptions_ThemePlanning_c_PointStatus_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component17_val {get;set;}	
		public SkyEditor2.TextHolder Component17_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component87_val {get;set;}	
		public SkyEditor2.TextHolder Component87_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component83_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component83_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component83_op{get;set;}	
		public List<SelectOption> valueOptions_ThemePlanning_c_Phase_c_multi {get;set;}
			
		public ThemePlanning__c Component89_val {get;set;}	
		public SkyEditor2.TextHolder Component89_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component21_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component21_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component21_op{get;set;}	
		public List<SelectOption> valueOptions_ThemePlanning_c_Questionnaire_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component94_val {get;set;}	
		public SkyEditor2.TextHolder Component94_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component96_val {get;set;}	
		public SkyEditor2.TextHolder Component96_op{get;set;}	
			
	public String recordTypeRecordsJSON_ThemePlanning_c {get; private set;}
	public String defaultRecordTypeId_ThemePlanning_c {get; private set;}
	public String metadataJSON_ThemePlanning_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public ThemePlanning2(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = ThemePlanning__c.fields.AccountName__c;
		f = ThemePlanning__c.fields.ProductItemName__c;
		f = ThemePlanning__c.fields.SalesPersonUnit__c;
		f = ThemePlanning__c.fields.PointStatus__c;
		f = ThemePlanning__c.fields.EditorID__c;
		f = ThemePlanning__c.fields.Name;
		f = ThemePlanning__c.fields.Phase__c;
		f = ThemePlanning__c.fields.TextManagement__c;
		f = ThemePlanning__c.fields.Questionnaire__c;
		f = ThemePlanning__c.fields.EditorName__c;
		f = ThemePlanning__c.fields.DeliveryStatus__c;
		f = ThemePlanning__c.fields.EditorRequestDay__c;
		f = ThemePlanning__c.fields.DeliveryHopeDay__c;
		f = ThemePlanning__c.fields.EditorDeliveryDate__c;
		f = ThemePlanning__c.fields.EditorTextRequestCount__c;
		f = ThemePlanning__c.fields.EditorEstimationAmount__c;
		f = ThemePlanning__c.fields.PointGrantPlanDay__c;
		f = ThemePlanning__c.fields.PointGrantDay__c;
		f = ThemePlanning__c.fields.EditorMemo__c;
 f = ThemePlanning__c.fields.EditorDeliveryDate__c;
 f = ThemePlanning__c.fields.PointGrantDay__c;
 f = ThemePlanning__c.fields.PointGrantPlanDay__c;
 f = ThemePlanning__c.fields.EditorRequestDay__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = ThemePlanning__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component22_from = new SkyEditor2__SkyEditorDummy__c();	
				Component22_to = new SkyEditor2__SkyEditorDummy__c();	
				Component22_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component22_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component64_from = new SkyEditor2__SkyEditorDummy__c();	
				Component64_to = new SkyEditor2__SkyEditorDummy__c();	
				Component64_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component64_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component66_from = new SkyEditor2__SkyEditorDummy__c();	
				Component66_to = new SkyEditor2__SkyEditorDummy__c();	
				Component66_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component66_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component68_from = new SkyEditor2__SkyEditorDummy__c();	
				Component68_to = new SkyEditor2__SkyEditorDummy__c();	
				Component68_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component68_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				ThemePlanning__c lookupObjComponent57 = new ThemePlanning__c();	
				Component9_val = new SkyEditor2__SkyEditorDummy__c();	
				Component9_op = new SkyEditor2.TextHolder();	
					
				Component11_val = new SkyEditor2__SkyEditorDummy__c();	
				Component11_op = new SkyEditor2.TextHolder();	
					
				Component13_val = new SkyEditor2__SkyEditorDummy__c();	
				Component13_op = new SkyEditor2.TextHolder();	
					
				Component15_val = new SkyEditor2__SkyEditorDummy__c();	
				Component15_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component15_op = new SkyEditor2.TextHolder();	
				valueOptions_ThemePlanning_c_PointStatus_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : ThemePlanning__c.PointStatus__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_ThemePlanning_c_PointStatus_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component17_val = new SkyEditor2__SkyEditorDummy__c();	
				Component17_op = new SkyEditor2.TextHolder();	
					
				Component87_val = new SkyEditor2__SkyEditorDummy__c();	
				Component87_op = new SkyEditor2.TextHolder();	
					
				Component83_val = new SkyEditor2__SkyEditorDummy__c();	
				Component83_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component83_op = new SkyEditor2.TextHolder();	
				valueOptions_ThemePlanning_c_Phase_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : ThemePlanning__c.Phase__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_ThemePlanning_c_Phase_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component89_val = lookupObjComponent57;	
				Component89_op = new SkyEditor2.TextHolder();	
					
				Component21_val = new SkyEditor2__SkyEditorDummy__c();	
				Component21_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component21_op = new SkyEditor2.TextHolder();	
				valueOptions_ThemePlanning_c_Questionnaire_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : ThemePlanning__c.Questionnaire__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_ThemePlanning_c_Questionnaire_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component94_val = new SkyEditor2__SkyEditorDummy__c();	
				Component94_op = new SkyEditor2.TextHolder();	
					
				Component96_val = new SkyEditor2__SkyEditorDummy__c();	
				Component96_op = new SkyEditor2.TextHolder();	
					
				queryMap.put(	
					'Component3',	
					new SkyEditor2.Query('ThemePlanning__c')
						.addField('Phase__c')
						.addFieldAsOutput('Name')
						.addFieldAsOutput('TextManagement__c')
						.addFieldAsOutput('AccountName__c')
						.addFieldAsOutput('SalesPersonUnit__c')
						.addFieldAsOutput('ProductItemName__c')
						.addField('EditorName__c')
						.addField('EditorID__c')
						.addFieldAsOutput('EditorRequestDay__c')
						.addFieldAsOutput('DeliveryHopeDay__c')
						.addField('EditorDeliveryDate__c')
						.addFieldAsOutput('EditorTextRequestCount__c')
						.addField('EditorEstimationAmount__c')
						.addField('PointStatus__c')
						.addField('PointGrantPlanDay__c')
						.addField('PointGrantDay__c')
						.addFieldAsOutput('Questionnaire__c')
						.addField('EditorMemo__c')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component22_from, 'SkyEditor2__Date__c', 'EditorDeliveryDate__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component22_to, 'SkyEditor2__Date__c', 'EditorDeliveryDate__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component22_isNull, 'SkyEditor2__Date__c', 'EditorDeliveryDate__c', Component22_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component64_from, 'SkyEditor2__Date__c', 'PointGrantDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component64_to, 'SkyEditor2__Date__c', 'PointGrantDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component64_isNull, 'SkyEditor2__Date__c', 'PointGrantDay__c', Component64_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component66_from, 'SkyEditor2__Date__c', 'PointGrantPlanDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component66_to, 'SkyEditor2__Date__c', 'PointGrantPlanDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component66_isNull, 'SkyEditor2__Date__c', 'PointGrantPlanDay__c', Component66_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component68_from, 'SkyEditor2__Date__c', 'EditorRequestDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component68_to, 'SkyEditor2__Date__c', 'EditorRequestDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component68_isNull, 'SkyEditor2__Date__c', 'EditorRequestDay__c', Component68_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component9_val, 'SkyEditor2__Text__c', 'AccountName__c', Component9_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component11_val, 'SkyEditor2__Text__c', 'ProductItemName__c', Component11_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component13_val, 'SkyEditor2__Text__c', 'SalesPersonUnit__c', Component13_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component15_val_dummy, 'SkyEditor2__Text__c','PointStatus__c', Component15_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component17_val, 'SkyEditor2__Text__c', 'EditorID__c', Component17_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component87_val, 'SkyEditor2__Text__c', 'Name', Component87_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component83_val_dummy, 'SkyEditor2__Text__c','Phase__c', Component83_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component89_val, 'TextManagement__c', 'TextManagement__c', Component89_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component21_val_dummy, 'SkyEditor2__Text__c','Questionnaire__c', Component21_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component94_val, 'SkyEditor2__Text__c', 'EditorName__c', Component94_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component96_val, 'SkyEditor2__Text__c', 'DeliveryStatus__c', Component96_op, true, 0, false ))
				);	
					
					Component3 = new Component3(new List<ThemePlanning__c>(), new List<Component3Item>(), new List<ThemePlanning__c>(), null);
				listItemHolders.put('Component3', Component3);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(ThemePlanning__c.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = false;
			presetSystemParams();
			Component3.extender = this.extender;
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_ThemePlanning_c_AccountName_c() { 
			return getOperatorOptions('ThemePlanning__c', 'AccountName__c');	
		}	
		public List<SelectOption> getOperatorOptions_ThemePlanning_c_ProductItemName_c() { 
			return getOperatorOptions('ThemePlanning__c', 'ProductItemName__c');	
		}	
		public List<SelectOption> getOperatorOptions_ThemePlanning_c_SalesPersonUnit_c() { 
			return getOperatorOptions('ThemePlanning__c', 'SalesPersonUnit__c');	
		}	
		public List<SelectOption> getOperatorOptions_ThemePlanning_c_PointStatus_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_ThemePlanning_c_EditorID_c() { 
			return getOperatorOptions('ThemePlanning__c', 'EditorID__c');	
		}	
		public List<SelectOption> getOperatorOptions_ThemePlanning_c_Name() { 
			return getOperatorOptions('ThemePlanning__c', 'Name');	
		}	
		public List<SelectOption> getOperatorOptions_ThemePlanning_c_Phase_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_ThemePlanning_c_TextManagement_c() { 
			return getOperatorOptions('ThemePlanning__c', 'TextManagement__c');	
		}	
		public List<SelectOption> getOperatorOptions_ThemePlanning_c_Questionnaire_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_ThemePlanning_c_EditorName_c() { 
			return getOperatorOptions('ThemePlanning__c', 'EditorName__c');	
		}	
		public List<SelectOption> getOperatorOptions_ThemePlanning_c_DeliveryStatus_c() { 
			return getOperatorOptions('ThemePlanning__c', 'DeliveryStatus__c');	
		}	
			
			
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public ThemePlanning__c record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, ThemePlanning__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.ListItemHolder {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (ThemePlanning__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	public ThemePlanning__c Component3_table_Conversion { get { return new ThemePlanning__c();}}
	
	public String Component3_table_selectval { get; set; }
	
	
			
	}