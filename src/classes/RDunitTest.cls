@isTest
private with sharing class RDunitTest{
		private static testMethod void testPageMethods() {	
			RDunit page = new RDunit(new ApexPages.StandardController(new OpportunityProduct__c()));	
			page.getOperatorOptions_OpportunityProduct_c_R_D_c_multi();	
			page.getOperatorOptions_Opportunity_Name();	
			page.getOperatorOptions_OpportunityProduct_c_Name();	
			page.getOperatorOptions_OpportunityProduct_c_Account_c();	
			page.getOperatorOptions_OpportunityProduct_c_sisakuompany_c();	
			page.getOperatorOptions_OpportunityProduct_c_ProductName_Detail_c();	
			page.getOperatorOptions_OpportunityProduct_c_Item_Classification_c();	
			page.getOperatorOptions_OpportunityProduct_c_Item_Keyword_strategy_keyword_c();	
			page.getOperatorOptions_OpportunityProduct_c_URL_RandDType_c();	
			page.getOperatorOptions_OpportunityProduct_c_SEO_sisaku_c();	
			page.getOperatorOptions_OpportunityProduct_c_Item_Automatic_updating_c();	
			page.getOperatorOptions_OpportunityProduct_c_Item_Estimate_level_c();	
			page.getOperatorOptions_OpportunityProduct_c_SalesPerson_c();	
			page.getOperatorOptions_OpportunityProduct_c_Item_Contract_start_date_c();	
			page.getOperatorOptions_OpportunityProduct_c_Item_Contract_end_date_c();	
			page.getOperatorOptions_OpportunityProduct_c_RecordTypeId();	
			page.getOperatorOptions_Opportunity_StageName_multi();	
			page.getOperatorOptions_OpportunityProduct_c_Item_Detail_Product_c();	
			System.assert(true);
		}	
			
	private static testMethod void testComponent2() {
		RDunit.Component2 Component2 = new RDunit.Component2(new List<OpportunityProduct__c>(), new List<RDunit.Component2Item>(), new List<OpportunityProduct__c>(), null);
		Component2.create(new OpportunityProduct__c());
		Component2.doDeleteSelectedItems();
		System.assert(true);
	}
	
}