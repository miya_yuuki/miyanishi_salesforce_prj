@isTest
private with sharing class LEX_Opportunity_SearchCleanerTest{
	private static testMethod void testPageMethods() {		LEX_Opportunity_SearchCleaner extension = new LEX_Opportunity_SearchCleaner(new ApexPages.StandardController(new OpportunityProduct__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	@isTest
	private static void testLightDataTables(){

		System.assert(true);
	}

	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component6() {
		String testReferenceId = '';
		LEX_Opportunity_SearchCleaner extension = new LEX_Opportunity_SearchCleaner(new ApexPages.StandardController(new OpportunityProduct__c()));
		extension.loadReferenceValues_Component6();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(Product__c.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<Product__c> parents = [SELECT Id FROM Product__c LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		Product__c parent = [SELECT Id,ClassificationSales__c,Product_DeliveryExistence__c FROM Product__c WHERE Id = :testReferenceId];
		extension.record.Product__c = parent.Id;
		extension.loadReferenceValues_Component6();
				
		if (SkyEditor2.Util.isEditable(extension.record, OpportunityProduct__c.fields.ProductTypeView__c)) {
			System.assertEquals(parent.ClassificationSales__c, extension.record.ProductTypeView__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, OpportunityProduct__c.fields.Opportunity_DeliveryExistenceView__c)) {
			System.assertEquals(parent.Product_DeliveryExistence__c, extension.record.Opportunity_DeliveryExistenceView__c);
		}

		System.assert(true);
	}
}