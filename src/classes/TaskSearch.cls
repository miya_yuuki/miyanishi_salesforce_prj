global with sharing class TaskSearch extends SkyEditor2.SkyEditorPageBaseWithSharing{
	public Task record{get;set;}
	public Component2 Component2 {get; private set;}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	public SkyEditor2__SkyEditorDummy__c Component6_from{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component6_to{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component6_isNull{get;set;}
	public SkyEditor2.TextHolder.OperatorHolder Component6_isNull_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component51_from{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component51_to{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component51_isNull{get;set;}
	public SkyEditor2.TextHolder.OperatorHolder Component51_isNull_op{get;set;}
	public Task Component67_val {get;set;}
	public SkyEditor2.TextHolder Component67_op{get;set;}
	public Task Component69_val {get;set;}
	public SkyEditor2.TextHolder Component69_op{get;set;}
	public Task Component9_val {get;set;}
	public SkyEditor2.TextHolder Component9_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component40_val {get;set;}
	public SkyEditor2.TextHolder Component40_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component11_val {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component11_val_dummy {get;set;}
	public SkyEditor2.TextHolder Component11_op{get;set;}
	public List<SelectOption> valueOptions_Task_Status_multi {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component13_val {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component13_val_dummy {get;set;}
	public SkyEditor2.TextHolder Component13_op{get;set;}
	public List<SelectOption> valueOptions_Task_Priority_multi {get;set;}
	public Task Component15_val {get;set;}
	public SkyEditor2.TextHolder Component15_op{get;set;}
	public String recordTypeRecordsJSON_Task {get; private set;}
	public String defaultRecordTypeId_Task {get; private set;}
	public String metadataJSON_Task {get; private set;}
	{
	setApiVersion(42.0);
	}
	public TaskSearch(ApexPages.StandardController controller){
		super(controller);

		SObjectField f;

		f = Task.fields.WhatId;
		f = Task.fields.WhoId;
		f = Task.fields.OwnerId;
		f = User.fields.Name;
		f = Task.fields.Status;
		f = Task.fields.Priority;
		f = Task.fields.Subject;
		f = Task.fields.Description;
		f = Task.fields.ActivityDate;
		f = Task.fields.CreatedDate;
		f = Task.fields.CreatedById;
 f = Task.fields.ActivityDate;
 f = Task.fields.CreatedDate;

		List<RecordTypeInfo> recordTypes;
		try {
			mainRecord = null;
			mainSObjectType = Task.SObjectType;
			mode = SkyEditor2.LayoutMode.TempSearch_01; 
			Component6_from = new SkyEditor2__SkyEditorDummy__c();
			Component6_to = new SkyEditor2__SkyEditorDummy__c();
			Component6_isNull = new SkyEditor2__SkyEditorDummy__c();
			Component6_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq');
			Component51_from = new SkyEditor2__SkyEditorDummy__c();
			Component51_to = new SkyEditor2__SkyEditorDummy__c();
			Component51_isNull = new SkyEditor2__SkyEditorDummy__c();
			Component51_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq');
			Task lookupObjComponent35 = new Task();
			Component67_val = lookupObjComponent35;
			Component67_op = new SkyEditor2.TextHolder();
			Component69_val = lookupObjComponent35;
			Component69_op = new SkyEditor2.TextHolder();
			Component9_val = lookupObjComponent35;
			Component9_op = new SkyEditor2.TextHolder();
			Component11_val = new SkyEditor2__SkyEditorDummy__c();
			Component11_val_dummy = new SkyEditor2__SkyEditorDummy__c();
			Component11_op = new SkyEditor2.TextHolder();
			valueOptions_Task_Status_multi = new List<SelectOption>{new SelectOption('', Label.none)};
			for (PicklistEntry e : Task.Status.getDescribe().getPicklistValues()) {
				if (e.isActive()) {valueOptions_Task_Status_multi.add(new SelectOption(e.getValue(), e.getLabel()));}
			}
			Component13_val = new SkyEditor2__SkyEditorDummy__c();
			Component13_val_dummy = new SkyEditor2__SkyEditorDummy__c();
			Component13_op = new SkyEditor2.TextHolder();
			valueOptions_Task_Priority_multi = new List<SelectOption>{new SelectOption('', Label.none)};
			for (PicklistEntry e : Task.Priority.getDescribe().getPicklistValues()) {
				if (e.isActive()) {valueOptions_Task_Priority_multi.add(new SelectOption(e.getValue(), e.getLabel()));}
			}
			Component15_val = new Task();
			Component15_op = new SkyEditor2.TextHolder();
			Component40_val = new SkyEditor2__SkyEditorDummy__c();
			Component40_op = new SkyEditor2.TextHolder();
			queryMap.put(
				'Component2',
				new SkyEditor2.Query('Task')
					.addFieldAsOutput('WhatId')
					.addFieldAsOutput('WhoId')
					.addField('Subject')
					.addField('Description')
					.addField('Status')
					.addField('Priority')
					.addField('ActivityDate')
					.addFieldAsOutput('CreatedDate')
					.addFieldAsOutput('OwnerId')
					.addFieldAsOutput('CreatedById')
					.addFieldAsOutput('RecordTypeId')
					.limitRecords(500)
					.addListener(new SkyEditor2.QueryWhereRegister(Component6_from, 'SkyEditor2__Date__c', 'ActivityDate', new SkyEditor2.TextHolder('ge'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component6_to, 'SkyEditor2__Date__c', 'ActivityDate', new SkyEditor2.TextHolder('le'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component6_isNull, 'SkyEditor2__Date__c', 'ActivityDate', Component6_isNull_op, true,0,false )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component51_from, 'SkyEditor2__Date__c', 'CreatedDate', new SkyEditor2.TextHolder('ge'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component51_to, 'SkyEditor2__Date__c', 'CreatedDate', new SkyEditor2.TextHolder('le'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component51_isNull, 'SkyEditor2__Date__c', 'CreatedDate', Component51_isNull_op, true,0,false )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component67_val, 'WhatId', 'WhatId', Component67_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component69_val, 'WhoId', 'WhoId', Component69_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component9_val, 'OwnerId', 'OwnerId', Component9_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component40_val, 'SkyEditor2__Text__c', 'CreatedBy.Name',User.fields.Name, Component40_op, true, false,true,0,false,'CreatedById',Task.fields.CreatedById )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component11_val_dummy, 'SkyEditor2__Text__c','Status', Component11_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component13_val_dummy, 'SkyEditor2__Text__c','Priority', Component13_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component15_val, 'Subject', 'Subject', Component15_op, true, 0, false ))
				);
			Component2 = new Component2(new List<Task>(), new List<Component2Item>(), new List<Task>(), null);
			listItemHolders.put('Component2', Component2);
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(Task.SObjectType, true);
			p_showHeader = true;
			p_sidebar = true;
			execInitialSearch = false;
			presetSystemParams();
			Component2.extender = this.extender;
			initSearch();
		} catch (SkyEditor2.Errors.SObjectNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.Errors.FieldNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.ExtenderException e) {
			 e.setMessagesToPage();
		} catch (Exception e) {
			System.Debug(LoggingLevel.Error, e);
			SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);
		}
	}
	public List<SelectOption> getOperatorOptions_Task_WhatId() { 
		return getOperatorOptions('Task', 'WhatId');
	}
	public List<SelectOption> getOperatorOptions_Task_WhoId() { 
		return getOperatorOptions('Task', 'WhoId');
	}
	public List<SelectOption> getOperatorOptions_Task_OwnerId() { 
		return getOperatorOptions('Task', 'OwnerId');
	}
	public List<SelectOption> getOperatorOptions_User_Name() { 
		return getOperatorOptions('User', 'Name');
	}
	public List<SelectOption> getOperatorOptions_Task_Status_multi() { 
		return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	}
	public List<SelectOption> getOperatorOptions_Task_Priority_multi() { 
		return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	}
	public List<SelectOption> getOperatorOptions_Task_Subject() { 
		return getOperatorOptions('Task', 'Subject');
	}
	global with sharing class Component2Item extends SkyEditor2.ListItem {
		public Task record{get; private set;}
		@TestVisible
		Component2Item(Component2 holder, Task record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component2 extends SkyEditor2.ListItemHolder {
		public List<Component2Item> items{get; private set;}
		@TestVisible
			Component2(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component2Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component2Item(this, (Task)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	public Task Component2_table_Conversion { get { return new Task();}}
	
	public String Component2_table_selectval { get; set; }
	
	
}