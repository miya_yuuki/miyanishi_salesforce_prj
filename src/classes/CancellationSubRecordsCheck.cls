public with sharing class CancellationSubRecordsCheck {
    public static boolean firstRun = true;

    public static void prcCancellationSubRecordCheck(List<CustomObject1__c> objRequest) {
        // 申請（案件）ID
        List<String> strRequestIds = new List<String>();
        // 契約変更申請用レコード
        List<SEOChange__c> objChange = new List<SEOChange__c>();
        // エラーメッセージ
        String StrErrMsg = '';

        for (CustomObject1__c objReq: objRequest) {
            // 解約申請以外はスキップ
            if (objReq.RecordtypeId != '01210000000AQRB') continue;
        
            strRequestIds.add(objReq.Id);
            // 契約変更申請用レコードを全件取得
            objChange = [
                SELECT
                    Id,
                    OrderItem__c,
                    sinsei__c,
                    Amount2__c,
                    Item2_EndData__c,
                    OrderItem__r.Name,
                    OrderItem__r.Item2_Relation__c,
                    OrderItem__r.TotalPrice__c,
                    OrderItem__r.CancelableAmount__c,
                    OrderItem__r.Item2_Keyword_phase__c,
                    OrderItem__r.Item2_DeliveryExistence__c,
                    OrderItem__r.ProductionStatus__c,
                    sinsei__r.OrderNo__c,
                    sinsei__r.CancelType2__c,
                    sinsei__r.ClauseDay__c,
                    sinsei__r.Skip_OrderItemCheck1__c,
                    sinsei__r.Skip_OrderItemCheck2__c,
                    sinsei__r.Skip_OrderItemCheck3__c,
                    sinsei__r.Skip_OrderItemCheck4__c
                FROM SEOChange__c
                WHERE sinsei__c IN :strRequestIds
            ];

            // 入力規則チェック
            for (SEOChange__c obj: objChange) {
                if (obj.sinsei__r.Skip_OrderItemCheck1__c == FALSE && obj.OrderItem__r.Item2_Relation__c != obj.sinsei__r.OrderNo__c) {
                    StrErrMsg += '<br /><br />注文商品チェック①: （' + obj.OrderItem__r.Name + '）　登録した注文商品の注文番号と、申請の注文番号が合致しません<br /> 　→ 注文番号を確認して下さい';
                }
                if (obj.sinsei__r.Skip_OrderItemCheck2__c == FALSE && obj.sinsei__r.CancelType2__c == '受注キャンセル' && obj.OrderItem__r.CancelableAmount__c != obj.OrderItem__r.TotalPrice__c) {
                    StrErrMsg += '<br /><br />注文商品チェック②: （' + obj.OrderItem__r.Name + '）　不適切な注文商品が含まれているため受注キャンセル出来ません<br /> 　→ 注文商品フェーズ・納品ステータス・納品管理ステータスなどを確認して下さい';
                }
                if (obj.sinsei__r.Skip_OrderItemCheck3__c == FALSE && (obj.OrderItem__r.Item2_Keyword_phase__c == '契約変更' || obj.OrderItem__r.Item2_Keyword_phase__c == '取消' || obj.OrderItem__r.Item2_Keyword_phase__c == '売上取消')) {
                    StrErrMsg += '<br /><br />注文商品チェック③: （' + obj.OrderItem__r.Name + '）　解約可能な注文商品ではありません<br /> 　→ 注文商品フェーズを確認して下さい ';
                }
                if (obj.sinsei__r.Skip_OrderItemCheck4__c == FALSE && obj.sinsei__r.CancelType2__c == '途中解約' && obj.sinsei__r.ClauseDay__c > obj.Item2_EndData__c) {
                    StrErrMsg += '<br /><br />注文商品チェック④: （' + obj.OrderItem__r.Name + '）　途中解約の解約日以前の注文商品が含まれています<br /> 　→ 注文商品の請求対象終了日を確認して下さい ';
                }
            }
            if (strErrMsg != '' && strErrMsg != null) {
                StrErrMsg += '<br /><br /><br />※未登録の注文商品でエラーが出る場合は「解約対象注文商品 自動補完」チェックも併せてご確認下さい';
                objReq.addError(strErrMsg);
            }
        }
    }
}