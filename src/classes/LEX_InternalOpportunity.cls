global with sharing class LEX_InternalOpportunity extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public Opportunity record {get{return (Opportunity)mainRecord;}}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	{
	setApiVersion(42.0);
	}
	public LEX_InternalOpportunity(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = Opportunity.fields.Name;
		f = Opportunity.fields.AccountId;
		f = Opportunity.fields.AmountTotal__c;
		f = Opportunity.fields.CloseDate;
		f = Opportunity.fields.StageName;
		f = Opportunity.fields.innerUnit__c;
		f = Opportunity.fields.OwnerId;
		f = Opportunity.fields.OrderCreate__c;
		f = Opportunity.fields.Opportunity_No__c;
		f = Opportunity.fields.RecordTypeId;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = Opportunity.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'LEX_InternalOpportunity';
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(Opportunity.SObjectType);
			mainQuery = new SkyEditor2.Query('Opportunity');
			mainQuery.addField('Name');
			mainQuery.addField('AccountId');
			mainQuery.addField('CloseDate');
			mainQuery.addField('StageName');
			mainQuery.addField('innerUnit__c');
			mainQuery.addField('OrderCreate__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('AmountTotal__c');
			mainQuery.addFieldAsOutput('OwnerId');
			mainQuery.addFieldAsOutput('Opportunity_No__c');
			mainQuery.addFieldAsOutput('RecordType.Name');
			mainQuery.addFieldAsOutput('LeadSource');
			mainQuery.addFieldAsOutput('Id');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('OpportunityCopy__c', 'OpportunityCopy__c');
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			if (record.Id == null) {
				saveOldValues();
				if(record.RecordTypeId == null) recordTypeSelector.applyDefault(record);
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}