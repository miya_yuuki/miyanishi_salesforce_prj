@isTest
private with sharing class operation_controlTest{
		private static testMethod void testPageMethods() {	
			operation_control page = new operation_control(new ApexPages.StandardController(new operator__c()));	
			page.getOperatorOptions_teamspirit_AtkApply_c_contract_naiyou_c();	
			page.getOperatorOptions_teamspirit_AtkApply_c_OutsourcingAccount_c();	
			page.getOperatorOptions_teamspirit_AtkApply_c_teamspirit_Status_c();	

		Integer defaultSize;

		defaultSize = page.Component3.items.size();
		page.Component3.add();
		System.assertEquals(defaultSize + 1, page.Component3.items.size());
		page.Component3.items[defaultSize].selected = true;
		page.Component3.doDeleteSelectedItems();
			System.assert(true);
		}	
			
	private static testMethod void testComponent3() {
		operation_control.Component3 Component3 = new operation_control.Component3(new List<operator__c>(), new List<operation_control.Component3Item>(), new List<operator__c>(), null);
		Component3.create(new operator__c());
		Component3.doDeleteSelectedItems();
		System.assert(true);
	}
	
}