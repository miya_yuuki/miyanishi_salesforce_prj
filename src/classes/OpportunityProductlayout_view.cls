global with sharing class OpportunityProductlayout_view extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public String recordTypeDevName{get; set;}
	public List<RecordType> recordTypes{get; set;}
	public String getRecordTypesJSON() {
		return System.JSON.serialize(recordTypes);
	}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	{setApiVersion(42.0);}
	public OpportunityProductlayout_view(ApexPages.StandardController controller) {
		super(controller);
		recordTypeSelector = new SkyEditor2.RecordTypeSelector(OpportunityProduct__c.SObjectType);
		Map<String, String> params = ApexPages.currentPage().getParameters();
		String recordTypeId = params.get('RecordType');
		hidePageBody=false;
		recordTypeDevName = params.get('RecordTypeName');
		DescribeSObjectResult describe = SObjectType.OpportunityProduct__c;
		List<Schema.RecordTypeInfo> types = describe.getRecordTypeInfos();
		SkyEditor2.Messages.clear();
		List<Id> ids = new List<Id>();
		Set<Id> availableIds = new Set<Id>();
		for (RecordTypeInfo info : types) {
			Id id = info.getRecordTypeId();
			if (id != '012000000000000AAA' && info.isAvailable()) {
				availableIds.add(id);
			}
		}
		recordTypes = new List<RecordType>();
		p_showHeader = true;
		p_sidebar = true;
		presetSystemParams();
		List<RecordTYpe> allTypes = [SELECT Id, Name, Description, DeveloperName FROM RecordType WHERE SobjectType = 'OpportunityProduct__c' AND IsActive = true];
		for (RecordType type : allTypes) {
			if (recordTypeId != null && type.Id == (Id)recordTypeId) {
				recordTypeDevName = type.DeveloperName;
			}
			if (availableIds.contains(type.Id)) {
				recordTypes.add(type);
			}
		}

		Id recordId = controller.getId();
		if (recordId != null) {
			OpportunityProduct__c record = [SELECT RecordType.DeveloperName FROM OpportunityProduct__c WHERE Id = :controller.getId()];
			recordTypeDevName = record.RecordType.DeveloperName;
		}

	}

	@AuraEnabled
	public static String getAssignmentInfo(String objectType){
		return SkyEditor2.PageAssignmentBaseController.getAssignmentInfo(objectType);
	}
}