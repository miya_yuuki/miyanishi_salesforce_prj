/*
*** T.Kumagai
*** 注文商品の支払期日を算出

*** 2016.02.05
*** J.Gotoh
*** 前払いロジックを追加

*** 2016.02.11
*** J.Gotoh
*** 前払い時には請求レコード作成フラグを上げるロジックを追加

*** 2016.11.29
*** J.Gotoh
*** 納品管理用請求基準日設置に伴い、prcGetDeliveryPaymentDueDateの仕様を変更

*** 2017.01.10
*** J.Gotoh
*** 契約期間なし商材に社外納品日が入らない問題対応のため、社外納品日代入ロジックを追加

*** 2019.10.08
*** Y.Miyanishi
*** 請求タイミングによって請求商品の作成元を変更する影響で、
*** prcGetPaymentDueDateの内容をprcGetDeliveryPaymentDueDateの仕様に合わせる
*/
public with sharing class PaymentCalculationClass {
	public static List<TextManagement2__c> prcGetDeliveryPaymentDueDate(List<TextManagement2__c> objOrderItem) {
		// 請求基準日・支払期日
		Date dt = null; 
		// 支払い方法
		String pay = '';
		// 締め日
		String fast = '';
		// 支払月
		String second = '';
		// 支払い日
		String third = '';
		// 1年分の土日のMap
		Map<String, String> mapSday = new Map<String, String>();
		// 1年分の祝日のMap
		Map<String, String> mapHday = new Map<String, String>();
		// 土日を取得
		mapSday = prcGetSday();
		// 祝日を取得
		mapHday = prcGetHday();
		
		for (TextManagement2__c obj : objOrderItem) {
			// 納品有無・支払方法・支払サイトがブランクである場合、
			// 支払サイトが前払いの場合、
			// 請求タイミングが納品以外の場合は処理しない
			if (obj.Item2_DeliveryExistence__c == null
			  || obj.Order_PaymentMethod__c == null
			  || obj.Order_PaymentSiteMaster__c == null
			  || obj.Order_PaymentSiteMaster__c == '前払い'
			  || obj.BillingTiming__c != '納品') continue;
			
			// 請求基準日をセット
			dt = obj.DeliveryDateCalc__c;
			System.debug('1***************dt:' + dt);
			
			// 請求書発行不要フラグがtrueだったら請求対象日と支払期日もnullセット
			if (obj.is_invoice_unnecessary__c == true) {
				obj.Bill_Main__c = null;
				obj.Item2_PaymentDueDate__c = null;
				continue;
			}
			
			// 請求基準日がブランクだったら請求対象日・支払期日にnullセットして処理しない
			if (dt == null) {
				obj.Bill_Main__c = null;
				obj.Item2_PaymentDueDate__c = null;
				continue;
			}
			
			// 支払い方法
			pay = obj.Order_PaymentMethod__c;
			// 締め日
			fast = obj.fast__c;
			// 支払月
			second = obj.second__c;
			// 支払い日
			third = obj.Third__c;
			
			System.debug('1***************pay:' + pay);
			System.debug('1***************fast:' + fast);
			System.debug('1***************second:' + second);
			System.debug('1***************third:' + third);
			
			// 締め日を元に請求対象日を算出して上書き
			if (! obj.BillSet__c) {
				obj.Bill_Main__c = prcRetBillDate(dt, fast);
			}
			// 請求対象日を上書きせず、日付に入力された請求対象日をセットする
			else {
				dt = obj.Bill_Main__c;
			}
			
			// 支払いサイトを元に支払い期日を算出
			dt = prcRetPaymentSite(dt, pay, fast, second, third);
			System.debug('1***************dt2:' + dt);
			
			// 休日・祝日を元に支払期日をリスケ
			obj.Item2_PaymentDueDate__c = prcReDate(dt, mapSday, mapHday, pay);
			System.debug('1***************Item2_PaymentDueDate__c:' + obj.Item2_PaymentDueDate__c);
			
			// 納品管理から請求商品を作成する場合は必ず納品時請求
			if (obj.Order_PaymentSiteMaster__c == '前払い') {
				obj.Billchek__c = true;					 // 請求レコード作成済み
			}
			// レコードタイプ・未納品、かつ契約期間なしの場合はここで社外納品日に代入する
			if (obj.RecordTypeId == '01210000000APip' && obj.Item2_StartData__c == null) {
				obj.DeliveryDay__c = obj.Bill_Main__c;
			}
		}
		// 戻り値
		return objOrderItem;
	}
	
	// 注文商品から取得
	public static List<OrderItem__c> prcGetPaymentDueDate(List<OrderItem__c> objOrderItem) {
		// 請求基準日・支払期日
		Date dt = null; 
		// 支払い方法
		String pay = '';
		// 締め日
		String fast = '';
		// 支払月
		String second = '';
		// 支払い日
		String third = '';
		// 1年分の土日のMap
		Map<String, String> mapSday = new Map<String, String>();
		// 1年分の祝日のMap
		Map<String, String> mapHday = new Map<String, String>();
		// 土日を取得
		mapSday = prcGetSday();
		// 祝日を取得
		mapHday = prcGetHday();
		
		for (OrderItem__c obj : objOrderItem) {
			// 納品有無・支払方法・支払サイトがブランクである場合、
			// 支払いサイトが前払いでなく、かつ、請求タイミングも受注でない場合は処理しない
			if (obj.Item2_DeliveryExistence__c == null
			  || obj.Order_PaymentMethod__c == null
			  || obj.Order_PaymentSiteMaster__c == null
			  || (obj.Order_PaymentSiteMaster__c != '前払い' && obj.BillingTiming__c != '受注')
			) continue;

			// 請求基準日をセット
			dt = obj.DeliveryDateCalc1__c;
			System.debug('1***************dt:' + dt);
			
			// 請求書発行不要フラグがtrueだったら請求対象日と支払期日もnullセット
			if (obj.is_invoice_unnecessary__c == true) {
				obj.Bill_Main__c = null;
				obj.Item2_PaymentDueDate__c = null;
				continue;
			}
			
			// 請求基準日がブランクだったら請求対象日・支払期日にnullセットして処理しない
			if (dt == null) {
				obj.Bill_Main__c = null;
				obj.Item2_PaymentDueDate__c = null;
				continue;
			}
			
			// 支払い方法
			pay = obj.Order_PaymentMethod__c;
			// 締め日
			fast = obj.fast__c;
			// 支払月
			second = obj.second__c;
			// 支払い日
			third = obj.Third__c;
			
			System.debug('1***************pay:' + pay);
			System.debug('1***************fast:' + fast);
			System.debug('1***************second:' + second);
			System.debug('1***************third:' + third);
			
			// 締め日を元に請求対象日を算出して上書き
			// 注文商品の画面に出すことになったのでここはOK
			if (! obj.BillSet__c) {
				obj.Bill_Main__c = prcRetBillDate(dt, fast);
			}
			// 請求対象日を上書きせず、日付に入力された請求対象日をセットする
			else {
				dt = obj.Bill_Main__c;
			}
			
			// 支払いサイトを元に支払い期日を算出
			dt = prcRetPaymentSite(dt, pay, fast, second, third);
			System.debug('1***************dt2:' + dt);
			
			// 休日・祝日を元に支払期日をリスケ
			obj.Item2_PaymentDueDate__c = prcReDate(dt, mapSday, mapHday, pay);
			System.debug('1***************Item2_PaymentDueDate__c:' + obj.Item2_PaymentDueDate__c);
			
			// 注文商品から請求を作成する場合は必ず受注時請求
			if (obj.Order_PaymentSiteMaster__c == '前払い' || obj.Item2_DeliveryExistence__c == '有') {
				obj.Billchek__c = true;	// 請求レコード作成済み
			}
		}
		// 戻り値
		return objOrderItem;
	}
	
	// 休日・祝日を元に支払期日をリスケ
	private static Date prcReDate(Date dt, Map<String, String> sday, Map<String, String> hday, String pay)
	{
		// 加算・減算日数
		Integer i = 0;
		// 祝日
		while (true) {
			System.debug('*****************:' + dt.addDays(i).format());
			// 祝祭日を抜けたらBreak
			if (sday.get(dt.addDays(i).format()) == null &&
				hday.get(dt.addDays(i).format()) == null) break;
				
			// 日数を加算
			if (pay == '振り込み【通常】') i--;
			if (pay == '振り込み（WG手数料負担）') i--;
			// 日数を減算
			if (pay == '口座振替') i++;
		}
		// 日数を変更して返却
		return dt.addDays(i);
	}
	
	// 支払いサイトを元に請求対象日を算出
	private static Date prcRetBillDate(Date dt, String fast)
	{
		Date retDt = null;  // 戻り値の日付
		Integer f = 0;	  // 締め日：加算月
		
		// 戻り値に日付をセット
		retDt = dt;
		
		// 締め日判定
		if (fast == '月末締め') f = 0;
		if (fast == '10日締め') f = prcRetFast(dt, 10);
		if (fast == '15日締め') f = prcRetFast(dt, 15);
		if (fast == '20日締め') f = prcRetFast(dt, 20);
		if (fast == '25日締め') f = prcRetFast(dt, 25);
		// 月を加算
		retDt = UtilityClass.prcAddMonths(retDt, f);
		
		// 戻り値
		return Date.newInstance(retDt.year(), retDt.month(), 1);
	}
	
	
	// 支払いサイトを元に支払期日を算出
	private static Date prcRetPaymentSite(Date dt, String pay, String fast, String second, String third)
	{
		Date retDt = null;  // 戻り値の日付
		Integer f = 0;	  // 締め日：加算月
		Integer s = 0;	  // 支払月：加算月
		Integer t = 0;	  // 支払い日
		
		// 戻り値に日付をセット
		retDt = dt;
		
		// 締め日判定
		if (fast == '月末締め') f = 0;
		if (fast == '10日締め') f = prcRetFast(dt, 10);
		if (fast == '15日締め') f = prcRetFast(dt, 15);
		if (fast == '20日締め') f = prcRetFast(dt, 20);
		if (fast == '25日締め') f = prcRetFast(dt, 25);
		// 月を加算
		retDt = UtilityClass.prcAddMonths(retDt, f);
		
		// 支払月
		if (second == '翌月') s = 1;
		if (second == '翌々月') s = 2;
		if (second == '翌翌々月') s = 3;
		// 月を加算
		retDt = UtilityClass.prcAddMonths(retDt, s);
		
		// 支払い日
		if (third == '5日払い') t = 5;
		if (third == '10日払い') t = 10;
		if (third == '15日払い') t = 15;
		if (third == '20日払い') t = 20;
		if (third == '25日払い') t = 25;
		if (third == '末払い') t = prcRetEndDay(retDt);
		if (pay == '口座振替') t = 27;
		
		// 前払い (= 締め日と支払月がnull)
		if (fast == null && second == null) {
			retDt = dt.addDays(14);		// 基準日+14日
			t = retDt.day();
		}
		// 戻り値
		return Date.newInstance(retDt.year(), retDt.month(), t);
	}
	
	// 月末日を算出
	public static Integer prcRetEndDay(Date dt)
	{
		// ブランクの場合は終了
		if (dt == null) return null;
		// 日付を1日にセット
		dt = Date.newInstance(dt.year(), dt.month(), 1);
		// 月末を返す
		return dt.addMonths(1).addDays(-1).day();
	}
	
	private static Integer prcRetFast(Date dt, Integer i)
	{
		// 戻り値
		Integer ret = 0;
		// 以降
		if (dt.day() > i) ret = 1;
		// 以前
		if (dt.day() <= i) ret = 0;
		// 戻り値
		return ret;
	}
	
	// 祝祭日マスタを取得
	private static Map<String, String> prcGetHday()
	{
		// 祝祭日マスタオブジェクト
		List<maturi__c> objHday = new List<maturi__c>();
		// 祝祭日のMap
		Map<String, String> mapHday = new Map<String, String>();
		// 祝祭日を取得
		objHday = [SELECT Day__c FROM maturi__c];
		// Mapに追加
		for (maturi__c obj : objHday) mapHday.put(obj.Day__c.format(), obj.Day__c.format());
		// 戻り値
		return mapHday;
	}
	
	// 土日判定
	private static Boolean prcGetYobi(Date vdDate)
	{
		// 日付型を日時型に変換
		datetime tDate = datetime.newInstance(vdDate.year(), vdDate.month(), vdDate.day());
		// 曜日を取得
		String week = tDate.format('E');
		// 土曜日判定
		if(week == 'Sun') return true;
		// 日曜日判定
		if(week =='Sat') return true;
		// 戻り値
		return false;
	}
	
	// 1年分の土日を取得
	private static Map<String, String> prcGetSday()
	{
		// 土日のMap
		Map<String, String> mapSday = new Map<String, String>();
		// 2年間の土日を取得
		for (Integer i = 0; i < 730; i++) {
			if (prcGetYobi(System.today().addDays(i))) {
				mapSday.put(System.today().addDays(i).format(), System.today().addDays(i).format());
			}
		}
		// 戻り値
		return mapSday;
	}
	
}