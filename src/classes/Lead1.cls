global with sharing class Lead1 extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public Lead record {get{return (Lead)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public String recordTypeRecordsJSON_Lead {get; private set;}
	public String defaultRecordTypeId_Lead {get; private set;}
	public String metadataJSON_Lead {get; private set;}
	public String picklistValuesJSON_Lead_Products_c {get; private set;}
	public String picklistValuesJSON_Lead_Contact_Role_c {get; private set;}
	public String Component759_hidden { get; set; }
	public String Component284_hidden { get; set; }
	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	
	
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	
	{
	setApiVersion(31.0);
	}
	public Lead1(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('noneTextOn','true');
        tmpPropMap.put('noneText','--選択--');
        tmpPropMap.put('editableOn','true');
        tmpPropMap.put('targetField1','Component173');
        tmpPropMap.put('makeSelOp1','');
        tmpPropMap.put('targetField2','');
        tmpPropMap.put('makeSelOp2','');
        tmpPropMap.put('targetField3','');
        tmpPropMap.put('makeSelOp3','');
        tmpPropMap.put('targetField4','');
        tmpPropMap.put('makeSelOp4','');
        tmpPropMap.put('targetField5','');
        tmpPropMap.put('makeSelOp5','');
        tmpPropMap.put('targetField6','');
        tmpPropMap.put('makeSelOp6','');
        tmpPropMap.put('targetField7','');
        tmpPropMap.put('makeSelOp7','');
        tmpPropMap.put('targetField8','');
        tmpPropMap.put('makeSelOp8','');
        tmpPropMap.put('targetField9','');
        tmpPropMap.put('makeSelOp9','');
        tmpPropMap.put('targetField10','');
        tmpPropMap.put('makeSelOp10','');
		tmpPropMap.put('Component__Width','188');
		tmpPropMap.put('Component__Height','50');
		tmpPropMap.put('Component__id','Component233');
		tmpPropMap.put('Component__Name','ChangeSelectList');
		tmpPropMap.put('Component__NameSpace','appcom');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		tmpPropMap.put('settings','{"targetField1":"Component173","makeSelOp1":"","targetField2":null,"makeSelOp2":"","targetField3":null,"makeSelOp3":"","targetField4":null,"makeSelOp4":"","targetField5":null,"makeSelOp5":"","targetField6":null,"makeSelOp6":"","targetField7":null,"makeSelOp7":"","targetField8":null,"makeSelOp8":"","targetField9":null,"makeSelOp9":"","targetField10":null,"makeSelOp10":""}');
		appComponentProperty.put('Component233',tmpPropMap);


		SObjectField f;

		f = Lead.fields.OwnerId;
		f = Lead.fields.AppointmentDay__c;
		f = Lead.fields.lead_Timing1__c;
		f = Lead.fields.Company;
		f = Lead.fields.lead_NGmemo__c;
		f = Lead.fields.lead_NextContactDay__c;
		f = Lead.fields.lead_AccountKana__c;
		f = Lead.fields.WEBTitle__c;
		f = Lead.fields.Lead_WEBsite_Quality__c;
		f = Lead.fields.lead_BusinessModel__c;
		f = Lead.fields.Website;
		f = Lead.fields.WebSiteType__c;
		f = Lead.fields.lead_Industry__c;
		f = Lead.fields.lead_ServiceWEBSite2__c;
		f = Lead.fields.lead_lead1__c;
		f = Lead.fields.lead_Industrycategory__c;
		f = Lead.fields.SEOMeasureStatus__c;
		f = Lead.fields.lead_lead2__c;
		f = Lead.fields.tag__c;
		f = Lead.fields.Products__c;
		f = Lead.fields.Startcheck__c;
		f = Lead.fields.SEOBudgetPerMonth__c;
		f = Lead.fields.Capital__c;
		f = Lead.fields.lastTereapoDate__c;
		f = Lead.fields.lastTereapoMemo__c;
		f = Lead.fields.lastConnectionDate__c;
		f = Lead.fields.lastConnectionMemo__c;
		f = Lead.fields.LastName;
		f = Lead.fields.FirstName;
		f = Lead.fields.lead_kana__c;
		f = Lead.fields.Phone;
		f = Lead.fields.Phone2__c;
		f = Lead.fields.MobilePhone__c;
		f = Lead.fields.Email;
		f = Lead.fields.lead_Division__c;
		f = Lead.fields.Title;
		f = Lead.fields.Contact_Role__c;
		f = Lead.fields.RivalCompany1__c;
		f = Lead.fields.RivalCompany2__c;
		f = Lead.fields.PostalCode;
		f = Lead.fields.State;
		f = Lead.fields.City;
		f = Lead.fields.Street;
		f = Lead.fields.building__c;
		f = Lead.fields.RecordTypeId;
		f = Lead.fields.MailNGDay__c;
		f = Lead.fields.Memo__c;
		f = Lead.fields.lead_AttackProduct__c;
		f = Lead.fields.lead_toiawase__c;
		f = Lead.fields.FunnelStage__c;
		f = Lead.fields.SmartViscal__NameCardListName__c;
		f = Lead.fields.MarketoList__c;
		f = Lead.fields.MarketoListDay__c;
		f = Lead.fields.NonStyle__c;

		List<RecordTypeInfo> recordTypes;
		FilterMetadataResult filterResult;
		List<RecordType> recordTypeRecords_Lead = [SELECT Id, DeveloperName, NamespacePrefix FROM RecordType WHERE SobjectType = 'Lead'];
		Map<Id, RecordType> recordTypeMap_Lead = new Map<Id, RecordType>(recordTypeRecords_Lead);
		List<RecordType> availableRecordTypes_Lead = new List<RecordType>();
		recordTypes = SObjectType.Lead.getRecordTypeInfos();

		for (RecordTypeInfo t: recordTypes) {
			if (t.isDefaultRecordTypeMapping()) {
				defaultRecordTypeId_Lead = t.getRecordTypeId();
			}
			if (t.isAvailable()) {
				RecordType rtype = recordTypeMap_Lead.get(t.getRecordTypeId());
				if (rtype != null) {
					availableRecordTypes_Lead.add(rtype);
				}
			}
		}
		recordTypeRecordsJSON_Lead = System.JSON.serialize(availableRecordTypes_Lead);
		filterResult = filterMetadataJSON(
			System.JSON.deserializeUntyped('{"CustomObject":{"recordTypes":[{"fullName":"CMS","picklistValues":[{"picklist":"Contact_Role__c","values":[{"fullName":"予算決定者","default":false},{"fullName":"意思決定者","default":false},{"fullName":"業務担当者","default":false},{"fullName":"請求担当者","default":false}]}]},{"fullName":"KPI","picklistValues":[{"picklist":"Contact_Role__c","values":[{"fullName":"予算決定者","default":false},{"fullName":"意思決定者","default":false},{"fullName":"業務担当者","default":false},{"fullName":"請求担当者","default":false}]}]},{"fullName":"Lead","picklistValues":[{"picklist":"Contact_Role__c","values":[{"fullName":"予算決定者","default":false},{"fullName":"意思決定者","default":false},{"fullName":"業務担当者","default":false}]},{"picklist":"Products__c","values":[{"fullName":"コンサル","default":false},{"fullName":"コンテンツ","default":false},{"fullName":"リンク","default":false},{"fullName":"不明","default":false}]}]},{"fullName":"Recruit","picklistValues":[{"picklist":"Contact_Role__c","values":[{"fullName":"予算決定者","default":false},{"fullName":"意思決定者","default":false},{"fullName":"業務担当者","default":false},{"fullName":"請求担当者","default":false}]}]},{"fullName":"X8","picklistValues":[{"picklist":"Contact_Role__c","values":[{"fullName":"予算決定者","default":false},{"fullName":"意思決定者","default":false},{"fullName":"業務担当者","default":false},{"fullName":"請求担当者","default":false}]}]},{"fullName":"analytics","picklistValues":[{"picklist":"Contact_Role__c","values":[{"fullName":"予算決定者","default":false},{"fullName":"意思決定者","default":false},{"fullName":"業務担当者","default":false},{"fullName":"請求担当者","default":false}]}]},{"fullName":"kurashi","picklistValues":[{"picklist":"Contact_Role__c","values":[{"fullName":"予算決定者","default":false},{"fullName":"意思決定者","default":false},{"fullName":"業務担当者","default":false},{"fullName":"請求担当者","default":false}]}]},{"fullName":"listing","picklistValues":[{"picklist":"Contact_Role__c","values":[{"fullName":"予算決定者","default":false},{"fullName":"意思決定者","default":false},{"fullName":"業務担当者","default":false},{"fullName":"請求担当者","default":false}]}]},{"fullName":"taisaku","picklistValues":[{"picklist":"Contact_Role__c","values":[{"fullName":"予算決定者","default":false},{"fullName":"意思決定者","default":false},{"fullName":"業務担当者","default":false},{"fullName":"請求担当者","default":false}]}]}]}}'),
			recordTypeFullNames(availableRecordTypes_Lead),
			Lead.SObjectType
		);
		metadataJSON_Lead = System.JSON.serialize(filterResult.data);
		picklistValuesJSON_Lead_Products_c = System.JSON.serialize(filterPricklistEntries(Lead.SObjectType.Products__c.getDescribe(), filterResult));
		picklistValuesJSON_Lead_Contact_Role_c = System.JSON.serialize(filterPricklistEntries(Lead.SObjectType.Contact_Role__c.getDescribe(), filterResult));
		try {
			mainSObjectType = Lead.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(Lead.SObjectType);
			
			mainQuery = new SkyEditor2.Query('Lead');
			mainQuery.addField('OwnerId');
			mainQuery.addField('AppointmentDay__c');
			mainQuery.addField('lead_Timing1__c');
			mainQuery.addField('Company');
			mainQuery.addField('lead_NGmemo__c');
			mainQuery.addField('lead_NextContactDay__c');
			mainQuery.addField('lead_AccountKana__c');
			mainQuery.addField('WEBTitle__c');
			mainQuery.addField('Lead_WEBsite_Quality__c');
			mainQuery.addField('lead_BusinessModel__c');
			mainQuery.addField('Website');
			mainQuery.addField('WebSiteType__c');
			mainQuery.addField('lead_Industry__c');
			mainQuery.addField('lead_ServiceWEBSite2__c');
			mainQuery.addField('lead_lead1__c');
			mainQuery.addField('lead_Industrycategory__c');
			mainQuery.addField('SEOMeasureStatus__c');
			mainQuery.addField('lead_lead2__c');
			mainQuery.addField('tag__c');
			mainQuery.addField('Products__c');
			mainQuery.addField('Startcheck__c');
			mainQuery.addField('SEOBudgetPerMonth__c');
			mainQuery.addField('Capital__c');
			mainQuery.addField('LastName');
			mainQuery.addField('FirstName');
			mainQuery.addField('lead_kana__c');
			mainQuery.addField('Phone');
			mainQuery.addField('Phone2__c');
			mainQuery.addField('MobilePhone__c');
			mainQuery.addField('Email');
			mainQuery.addField('lead_Division__c');
			mainQuery.addField('Title');
			mainQuery.addField('Contact_Role__c');
			mainQuery.addField('RivalCompany1__c');
			mainQuery.addField('RivalCompany2__c');
			mainQuery.addField('PostalCode');
			mainQuery.addField('State');
			mainQuery.addField('City');
			mainQuery.addField('Street');
			mainQuery.addField('building__c');
			mainQuery.addField('RecordTypeId');
			mainQuery.addField('Memo__c');
			mainQuery.addField('FunnelStage__c');
			mainQuery.addField('NonStyle__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('lastTereapoDate__c');
			mainQuery.addFieldAsOutput('lastTereapoMemo__c');
			mainQuery.addFieldAsOutput('lastConnectionDate__c');
			mainQuery.addFieldAsOutput('lastConnectionMemo__c');
			mainQuery.addFieldAsOutput('MailNGDay__c');
			mainQuery.addFieldAsOutput('lead_AttackProduct__c');
			mainQuery.addFieldAsOutput('lead_toiawase__c');
			mainQuery.addFieldAsOutput('SmartViscal__NameCardListName__c');
			mainQuery.addFieldAsOutput('MarketoList__c');
			mainQuery.addFieldAsOutput('MarketoListDay__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			
			p_showHeader = true;
			p_sidebar = true;
			sve_ClassName = 'Lead1';
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			
			if (record.Id == null) {
				
				saveOldValues();
				
				if(record.RecordTypeId == null) recordTypeSelector.applyDefault(record);
				
			}

			
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	public String getComponent759OptionsJS() {
		return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
		Lead.getSObjectType(),
		SObjectType.Lead.fields.Products__c.getSObjectField()
		));
		}
	public String getComponent284OptionsJS() {
		return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
		Lead.getSObjectType(),
		SObjectType.Lead.fields.Contact_Role__c.getSObjectField()
		));
		}
	@TestVisible		static Set<String> recordTypeFullNames(RecordType[] records) {
		Set<String> result = new Set<String>();
		for (RecordType r : records) {
			result.add(r.DeveloperName);
			if (r.NamespacePrefix != null) {
				result.add(r.NamespacePrefix + '__' + r.DeveloperName);
			}
		}
		return result;
	}
	
	@TestVisible		static FilterMetadataResult filterMetadataJSON(Object metadata, Set<String> recordTypeFullNames, SObjectType soType) {
		Map<String, Object> metadataMap = (Map<String, Object>) metadata;
		Map<String, Object> customObject = (Map<String, Object>) metadataMap.get('CustomObject');
		List<Object> recordTypes = (List<Object>) customObject.get('recordTypes');
		Map<String, Set<String>> availableEntries = new Map<String, Set<String>>();
		for (Integer i = recordTypes.size() - 1; i >= 0; i--) {
			Map<String, Object> recordType = (Map<String, Object>)recordTypes[i];
			String fullName = (String)recordType.get('fullName');
			if (! recordTypeFullNames.contains(fullName)) {
				recordTypes.remove(i);
			} else {
				addAll(availableEntries, getOutEntries(recordType, soType));
			}
		}	
		return new FilterMetadataResult(metadataMap, availableEntries, recordTypes.size() == 0);
	}
	public class FilterMetadataResult {
		public Map<String, Object> data {get; private set;}
		public Map<String, Set<String>> availableEntries {get; private set;}
		public Boolean master {get; private set;}
		public FilterMetadataResult(Map<String, Object> data, Map<String, Set<String>> availableEntries, Boolean master) {
			this.data = data;
			this.availableEntries = availableEntries;
			this.master = master;
		}
	}
	
	static void addAll(Map<String, Set<String>> toMap, Map<String, Set<String>> fromMap) {
		for (String key : fromMap.keySet()) {
			Set<String> fromSet = fromMap.get(key);
			Set<String> toSet = toMap.get(key);
			if (toSet == null) {
				toSet = new Set<String>();
				toMap.put(key, toSet);
			}
			toSet.addAll(fromSet);
		}
	}

	static Map<String, Set<String>> getOutEntries(Map<String, Object> recordType, SObjectType soType) {
		Map<String, Set<String>> result = new Map<String, Set<String>>();
		List<Object> entries = (List<Object>)recordType.get('picklistValues');
		Map<String, SObjectField> fields = soType.getDescribe().fields.getMap();
		for (Object e : entries) {
			Map<String, Object> entry = (Map<String, Object>) e;
			String picklist = (String) entry.get('picklist');
			SObjectField f = fields.get(picklist);
			List<Object> values = (List<Object>)(entry.get('values'));
			if (f != null && f.getDescribe().isAccessible()) {
				Set<String> entrySet = new Set<String>();
				for (Object v : values) {
					Map<String, Object> value = (Map<String, Object>) v;
					entrySet.add(EncodingUtil.urlDecode((String)value.get('fullName'), 'utf-8'));
				}
				result.put(picklist, entrySet);
			} else { 
				values.clear(); 
			}
		}
		return result;
	}
	
	static List<PicklistEntry> filterPricklistEntries(DescribeFieldResult f, FilterMetadataResult parseResult) {
		List<PicklistEntry> all = f.getPicklistValues();
		if (parseResult.master) {
			return all;
		}
		Set<String> availables = parseResult.availableEntries.get(f.getName());
		List<PicklistEntry> result = new List<PicklistEntry>();
		if(availables == null) return result;
		for (PicklistEntry e : all) {
			if (e.isActive() && availables.contains(e.getValue())) {
				result.add(e);
			}
		}
		return result;
	}
	
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}