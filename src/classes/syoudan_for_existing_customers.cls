global with sharing class syoudan_for_existing_customers extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public Opportunity record {get{return (Opportunity)mainRecord;}}
	public Component6643 Component6643 {get; private set;}
	{
	setApiVersion(42.0);
	}
	public syoudan_for_existing_customers(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = Opportunity.fields.Group__c;
		f = Opportunity.fields.Name;
		f = Opportunity.fields.OpportunityCopy__c;
		f = Opportunity.fields.AccountId;
		f = Opportunity.fields.Opportunity_Type__c;
		f = Opportunity.fields.BoardDiscussed__c;
		f = Opportunity.fields.Opportunity_Partner__c;
		f = Opportunity.fields.Opportunity_endClient1__c;
		f = Opportunity.fields.karute__c;
		f = Opportunity.fields.karuteCheck__c;
		f = Opportunity.fields.karutenew__c;
		f = Opportunity.fields.Opportunity_Competition__c;
		f = Opportunity.fields.Opportunity_FastOrdersSales__c;
		f = Opportunity.fields.OwnerId;
		f = Opportunity.fields.Opportunity_Support__c;
		f = Opportunity.fields.MA_access_flag__c;
		f = Opportunity.fields.AmountTotal__c;
		f = Opportunity.fields.CloseDate;
		f = Opportunity.fields.StageName;
		f = Opportunity.fields.account_web_site__c;
		f = Opportunity.fields.account_site_creating_link__c;
		f = Opportunity.fields.account_web_site_unnecessary__c;
		f = Opportunity.fields.ServiceWEBSite__c;
		f = Opportunity.fields.ServiceWEBSite2__c;
		f = Opportunity.fields.CasePrintCheck__c;
		f = Opportunity.fields.CaseApproval__c;
		f = Opportunity.fields.Opportunity_lead1__c;
		f = Opportunity.fields.Opportunity_lead2__c;
		f = Opportunity.fields.AppointmentDay__c;
		f = Opportunity.fields.CampaignId;
		f = Opportunity.fields.Opportunity_BillingMethod__c;
		f = Opportunity.fields.Opportunity_PaymentMethod__c;
		f = Opportunity.fields.Opportunity_PaymentSiteMaster__c;
		f = Opportunity.fields.OrderStamp__c;
		f = Opportunity.fields.Opportunity_Title__c;
		f = Opportunity.fields.CustomerNamePRINT__c;
		f = Opportunity.fields.AddressCheck__c;
		f = Opportunity.fields.Opportunity_Memo__c;
		f = Opportunity.fields.OrderProvisional__c;
		f = Opportunity.fields.OrderProvisionalMemo__c;
		f = Opportunity.fields.SalvageScheduledDate__c;
		f = Opportunity.fields.opportunityBillMemo__c;
		f = Opportunity.fields.notices__c;
		f = Opportunity.fields.QuoteExpirationDate__c;
		f = Opportunity.fields.Opportunity_EstimateMemo__c;
		f = AccountCustomer__c.fields.CustomerName__c;
		f = AccountCustomer__c.fields.account__c;
		f = AccountCustomer__c.fields.department__c;
		f = AccountCustomer__c.fields.Title__c;
		f = AccountCustomer__c.fields.yakuwari__c;
		f = AccountCustomer__c.fields.Mail__c;
		f = AccountCustomer__c.fields.Report1__c;
		f = AccountCustomer__c.fields.PartnerReport__c;
		f = AccountCustomer__c.fields.ReportSend1__c;
		f = AccountCustomer__c.fields.PartnerReport_c__c;
		f = Opportunity.fields.AccountNameView__c;
		f = Opportunity.fields.Opportunity_BillPostalCode2__c;
		f = Opportunity.fields.Opportunity_BillPrefecture2__c;
		f = Opportunity.fields.Opportunity_BillCity2__c;
		f = Opportunity.fields.Opportunity_BillBuilding2__c;
		f = Opportunity.fields.Phone__c;
		f = Opportunity.fields.TransferAccountNumber1__c;
		f = Opportunity.fields.account_BusinessModel__c;
		f = Opportunity.fields.account_Industry__c;
		f = Opportunity.fields.account_Industrycategory__c;
		f = Opportunity.fields.AccountEdit__c;
		f = Opportunity.fields.account_CreditLevel__c;
		f = Opportunity.fields.account_CreditDay__c;
		f = Opportunity.fields.CreditSpace__c;
		f = Opportunity.fields.CreditBalance__c;
		f = Opportunity.fields.CreditDecisionPrice__c;
		f = Opportunity.fields.hansyaDay__c;
		f = Opportunity.fields.hansyaMemo__c;
		f = Opportunity.fields.CompanyCheck__c;
		f = Opportunity.fields.syouninn__c;
		f = Opportunity.fields.Opportunity_Billaccount__c;
		f = Opportunity.fields.Opportunity_BillPostalCode__c;
		f = Opportunity.fields.Opportunity_BillPrefecture__c;
		f = Opportunity.fields.Opportunity_BillCity__c;
		f = Opportunity.fields.Opportunity_BillAddress__c;
		f = Opportunity.fields.Opportunity_BillBuilding__c;
		f = Opportunity.fields.Opportunity_BillKana__c;
		f = Opportunity.fields.Opportunity_BillDepartment__c;
		f = Opportunity.fields.Opportunity_BillTitle__c;
		f = Opportunity.fields.Opportunity_BillRole__c;
		f = Opportunity.fields.Opportunity_BillPhone__c;
		f = Opportunity.fields.Opportunity_BillFax__c;
		f = Opportunity.fields.Opportunity_BillEmail__c;
		f = Opportunity.fields.ContacEdit__c;
		f = Opportunity.fields.Agreement1__c;
		f = Opportunity.fields.Agreement2__c;
		f = Opportunity.fields.Agreement3__c;
		f = Opportunity.fields.Agreement4__c;
		f = Opportunity.fields.Agreement5__c;
		f = Opportunity.fields.Agreement6__c;
		f = Opportunity.fields.Agreement7__c;
		f = Opportunity.fields.Agreement8__c;
		f = Opportunity.fields.Agreement9__c;
		f = Opportunity.fields.Agreement10__c;
		f = Opportunity.fields.Agreement11__c;
		f = Opportunity.fields.Agreement12__c;
		f = Opportunity.fields.Agreement17__c;
		f = Opportunity.fields.Agreement18__c;
		f = Opportunity.fields.Agreement19__c;
		f = Opportunity.fields.Agreement20__c;
		f = Opportunity.fields.Agreement13__c;
		f = Opportunity.fields.Agreement14__c;
		f = Opportunity.fields.Agreement15__c;
		f = Opportunity.fields.CreatedById;
		f = Opportunity.fields.CreatedDate;
		f = Opportunity.fields.LastModifiedById;
		f = Opportunity.fields.LastModifiedDate;
		f = Account.fields.BillingMethod__c;
		f = Account.fields.PaymentMethod__c;
		f = Contact.fields.Contact_BillCompany__c;
		f = Contact.fields.Department;
		f = Contact.fields.Title;
		f = Contact.fields.Contact_Role__c;
		f = Contact.fields.Email;
		f = Contact.fields.report__c;
		f = Contact.fields.TOorCC__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = Opportunity.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'syoudan_for_existing_customers';
			mainQuery = new SkyEditor2.Query('Opportunity');
			mainQuery.addField('Name');
			mainQuery.addField('OpportunityCopy__c');
			mainQuery.addField('AccountId');
			mainQuery.addField('Opportunity_Type__c');
			mainQuery.addField('BoardDiscussed__c');
			mainQuery.addField('Opportunity_Partner__c');
			mainQuery.addField('Opportunity_endClient1__c');
			mainQuery.addField('karute__c');
			mainQuery.addField('karuteCheck__c');
			mainQuery.addField('Opportunity_Competition__c');
			mainQuery.addField('OwnerId');
			mainQuery.addField('Opportunity_Support__c');
			mainQuery.addField('MA_access_flag__c');
			mainQuery.addField('CloseDate');
			mainQuery.addField('StageName');
			mainQuery.addField('account_web_site__c');
			mainQuery.addField('account_web_site_unnecessary__c');
			mainQuery.addField('ServiceWEBSite__c');
			mainQuery.addField('ServiceWEBSite2__c');
			mainQuery.addField('CasePrintCheck__c');
			mainQuery.addField('CaseApproval__c');
			mainQuery.addField('Opportunity_lead1__c');
			mainQuery.addField('Opportunity_lead2__c');
			mainQuery.addField('AppointmentDay__c');
			mainQuery.addField('CampaignId');
			mainQuery.addField('Opportunity_BillingMethod__c');
			mainQuery.addField('Opportunity_PaymentMethod__c');
			mainQuery.addField('Opportunity_PaymentSiteMaster__c');
			mainQuery.addField('OrderStamp__c');
			mainQuery.addField('Opportunity_Title__c');
			mainQuery.addField('CustomerNamePRINT__c');
			mainQuery.addField('AddressCheck__c');
			mainQuery.addField('Opportunity_Memo__c');
			mainQuery.addField('OrderProvisional__c');
			mainQuery.addField('OrderProvisionalMemo__c');
			mainQuery.addField('SalvageScheduledDate__c');
			mainQuery.addField('opportunityBillMemo__c');
			mainQuery.addField('notices__c');
			mainQuery.addField('QuoteExpirationDate__c');
			mainQuery.addField('Opportunity_EstimateMemo__c');
			mainQuery.addField('Agreement1__c');
			mainQuery.addField('Agreement2__c');
			mainQuery.addField('Agreement3__c');
			mainQuery.addField('Agreement4__c');
			mainQuery.addField('Agreement5__c');
			mainQuery.addField('Agreement6__c');
			mainQuery.addField('Agreement7__c');
			mainQuery.addField('Agreement8__c');
			mainQuery.addField('Agreement9__c');
			mainQuery.addField('Agreement10__c');
			mainQuery.addField('Agreement11__c');
			mainQuery.addField('Agreement12__c');
			mainQuery.addField('Agreement17__c');
			mainQuery.addField('Agreement18__c');
			mainQuery.addField('Agreement19__c');
			mainQuery.addField('Agreement20__c');
			mainQuery.addField('Agreement13__c');
			mainQuery.addField('Agreement14__c');
			mainQuery.addField('Agreement15__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Group__c');
			mainQuery.addFieldAsOutput('karutenew__c');
			mainQuery.addFieldAsOutput('Opportunity_FastOrdersSales__c');
			mainQuery.addFieldAsOutput('AmountTotal__c');
			mainQuery.addFieldAsOutput('account_site_creating_link__c');
			mainQuery.addFieldAsOutput('AccountNameView__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPostalCode2__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPrefecture2__c');
			mainQuery.addFieldAsOutput('Opportunity_BillCity2__c');
			mainQuery.addFieldAsOutput('Opportunity_BillBuilding2__c');
			mainQuery.addFieldAsOutput('Phone__c');
			mainQuery.addFieldAsOutput('TransferAccountNumber1__c');
			mainQuery.addFieldAsOutput('account_BusinessModel__c');
			mainQuery.addFieldAsOutput('account_Industry__c');
			mainQuery.addFieldAsOutput('account_Industrycategory__c');
			mainQuery.addFieldAsOutput('AccountEdit__c');
			mainQuery.addFieldAsOutput('account_CreditLevel__c');
			mainQuery.addFieldAsOutput('account_CreditDay__c');
			mainQuery.addFieldAsOutput('CreditSpace__c');
			mainQuery.addFieldAsOutput('CreditBalance__c');
			mainQuery.addFieldAsOutput('CreditDecisionPrice__c');
			mainQuery.addFieldAsOutput('hansyaDay__c');
			mainQuery.addFieldAsOutput('hansyaMemo__c');
			mainQuery.addFieldAsOutput('CompanyCheck__c');
			mainQuery.addFieldAsOutput('syouninn__c');
			mainQuery.addFieldAsOutput('Opportunity_Billaccount__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPostalCode__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPrefecture__c');
			mainQuery.addFieldAsOutput('Opportunity_BillCity__c');
			mainQuery.addFieldAsOutput('Opportunity_BillAddress__c');
			mainQuery.addFieldAsOutput('Opportunity_BillBuilding__c');
			mainQuery.addFieldAsOutput('Opportunity_BillKana__c');
			mainQuery.addFieldAsOutput('Opportunity_BillDepartment__c');
			mainQuery.addFieldAsOutput('Opportunity_BillTitle__c');
			mainQuery.addFieldAsOutput('Opportunity_BillRole__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPhone__c');
			mainQuery.addFieldAsOutput('Opportunity_BillFax__c');
			mainQuery.addFieldAsOutput('Opportunity_BillEmail__c');
			mainQuery.addFieldAsOutput('ContacEdit__c');
			mainQuery.addFieldAsOutput('CreatedById');
			mainQuery.addFieldAsOutput('CreatedDate');
			mainQuery.addFieldAsOutput('LastModifiedById');
			mainQuery.addFieldAsOutput('LastModifiedDate');
			mainQuery.addFieldAsOutput('LeadSource');
			mainQuery.addFieldAsOutput('Id');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			Component6643 = new Component6643(new List<AccountCustomer__c>(), new List<Component6643Item>(), new List<AccountCustomer__c>(), null);
			listItemHolders.put('Component6643', Component6643);
			query = new SkyEditor2.Query('AccountCustomer__c');
			query.addField('CustomerName__c');
			query.addField('Report1__c');
			query.addField('PartnerReport__c');
			query.addField('ReportSend1__c');
			query.addField('PartnerReport_c__c');
			query.addFieldAsOutput('account__c');
			query.addFieldAsOutput('department__c');
			query.addFieldAsOutput('Title__c');
			query.addFieldAsOutput('yakuwari__c');
			query.addFieldAsOutput('Mail__c');
			query.addWhere('Opportunity_customer__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('Component6643', 'Opportunity_customer__c');
			Component6643.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('Component6643', query);
			registRelatedList('Opportunity_customer__r', 'Component6643');
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('OpportunityCopy__c', 'OpportunityCopy__c');
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			Component6643.extender = this.extender;
			if (record.Id == null) {
				saveOldValues();
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class Component6643Item extends SkyEditor2.ListItem {
		public AccountCustomer__c record{get; private set;}
		@TestVisible
		Component6643Item(Component6643 holder, AccountCustomer__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}

	public void loadReferenceValues_Component6659() {
		if (record.CustomerName__c == null) {
if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.account__c)) {
				record.account__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.department__c)) {
				record.department__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.Title__c)) {
				record.Title__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.yakuwari__c)) {
				record.yakuwari__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.Mail__c)) {
				record.Mail__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.Report1__c)) {
				record.Report1__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.ReportSend1__c)) {
				record.ReportSend1__c = null;
			}
				return;
		}
		Contact[] referenceTo = [SELECT Contact_BillCompany__c,Department,Title,Contact_Role__c,Email,report__c,TOorCC__c FROM Contact WHERE Id=:record.CustomerName__c];
		if (referenceTo.size() == 0) {
			record.CustomerName__c.addError(SkyEditor2.Messages.referenceDataNotFound(record.CustomerName__c));
			return;
		}
		if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.account__c)) {
			record.account__c = referenceTo[0].Contact_BillCompany__c;
		}
		if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.department__c)) {
			record.department__c = referenceTo[0].Department;
		}
		if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.Title__c)) {
			record.Title__c = referenceTo[0].Title;
		}
		if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.yakuwari__c)) {
			record.yakuwari__c = referenceTo[0].Contact_Role__c;
		}
		if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.Mail__c)) {
			record.Mail__c = referenceTo[0].Email;
		}
		if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.Report1__c)) {
			record.Report1__c = referenceTo[0].report__c;
		}
		if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.ReportSend1__c)) {
			record.ReportSend1__c = referenceTo[0].TOorCC__c;
		}
		
	}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component6643 extends SkyEditor2.ListItemHolder {
		public List<Component6643Item> items{get; private set;}
		@TestVisible
			Component6643(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component6643Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component6643Item(this, (AccountCustomer__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	public void loadReferenceValues_Component4751() {
		if (record.AccountId == null) {
if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillingMethod__c)) {
				record.Opportunity_BillingMethod__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_PaymentMethod__c)) {
				record.Opportunity_PaymentMethod__c = null;
			}
				return;
		}
		Account[] referenceTo = [SELECT BillingMethod__c,PaymentMethod__c FROM Account WHERE Id=:record.AccountId];
		if (referenceTo.size() == 0) {
			record.AccountId.addError(SkyEditor2.Messages.referenceDataNotFound(record.AccountId));
			return;
		}
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_BillingMethod__c)) {
			record.Opportunity_BillingMethod__c = referenceTo[0].BillingMethod__c;
		}
		if (SkyEditor2.Util.isEditable(record, Opportunity.fields.Opportunity_PaymentMethod__c)) {
			record.Opportunity_PaymentMethod__c = referenceTo[0].PaymentMethod__c;
		}
		
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}