public with sharing class AgreementMasterRefresh {
    public static void prcAgreementMasterRefresh() {
        // 約款ファイルID（ライブラリID）
        List<String> strIds = new List<String>();
        // 約款ファイル（ライブラリ）
        List<ContentDocument> srcDocument = new List<ContentDocument>();
        // 約款ファイルのMAP
        Map<String, ContentDocument> mapDocument = new Map<String, ContentDocument>();
        // 約款マスタ
        List<AgreementMaster__c> mstAgreement = new List<AgreementMaster__c>();
        
        // 約款1 - コンサルティングサービス約款
        String strId1 = '06910000004j6m7AAA';
        strIds.add(strId1);
        // 約款2 - コンテンツ拡散サービス利用規約
        String strId2 = '069100000069thAAAQ';
        strIds.add(strId2);
        // 約款3（約款13） - レポーティングサービス約款
        String strId3 = '069100000069uqwAAA';
        strIds.add(strId3);
        // 約款4 - CMS構築サービス約款
        String strId4 = '0695F0000073QLGQA2';
        strIds.add(strId4);
        // 約款5 - Webサイト運用代行サービス約款
        String strId5 = '06910000004jEj6AAE';
        strIds.add(strId5);
        // 約款6 - SNSアカウント運用代行サービス約款
        String strId6 = '0695F0000073NmYQAU';
        strIds.add(strId6);
        // 約款7 - SNS広告運用代行サービス約款
        String strId7 = '0695F00000731SMQAY';
        strIds.add(strId7);
        // 約款8 - 外部リンクサービス利用約款（その他申込書）
        String strId8 = '06910000004hWleAAE';
        strIds.add(strId8);
        // 約款9 - アドネット広告運用代行サービス約款
        String strId9 = '0695F00000BKjTxQAL';
        strIds.add(strId9);
        // 約款10 - 暮らしニスタ広告約款
        String strId10 = '0695F0000075EcdQAE';
        strIds.add(strId10);
        // 約款11（約款14） - コンテンツ制作サービス約款
        String strId11 = '06910000004jEj1AAE';
        strIds.add(strId11);
        // 約款12 - サービス基本約款 + EASY ENTRY　利用約款
        String strId12 = '06910000004gxkyAAA';
        strIds.add(strId12);
        // 約款13 - レポーティングサービス約款（その他申込書印刷なし）
        // ※ 約款3と同じ
        // 約款14 - コンテンツ制作サービス約款（その他申込書印刷なし）
        // ※ 約款11と同じ
        // 約款15 - コンテンツ監修サービス約款
        String strId15 = '0695F000008opunQAA';
        strIds.add(strId15);
        // 約款17 - Milly広告約款
        String strId17 = '0695F00000AR8aXQAT';
        strIds.add(strId17);
        // 約款18 - マーケティングツール「TACT SEO」サービス約款
        String strId18 = '0695F00000CLYnEQAX';
        strIds.add(strId18);
        // 約款19 - 「Front Desk」サービス約款
        String strId19 = '0695F00000C4voaQAB';
        strIds.add(strId19);
        // 約款20 - エディトルサービス約款
        String strId20 = '0695F00000EG33XQAT';
        strIds.add(strId20);


        // 約款ファイル名を全ID分まとめて取得
        srcDocument = [
            SELECT Id, Title
            FROM ContentDocument
            WHERE Id in :strIds
        ];
        // 取得結果をMapに保存
        for (ContentDocument src: srcDocument) mapDocument.put(src.Id, src);

        // ループを回してそれぞれ対処する
        for (String srcId: strIds) {
            // 約款ファイルが検索出来た 有効な Id のみ処理する 
            if (mapDocument.get(srcId) == null) continue;
            
            // 約款マスタを検索
            mstAgreement = [SELECT Id, Name, AgreementFileID__c FROM AgreementMaster__c WHERE AgreementFileID__c = :srcId LIMIT 1];

            // マスタに既に登録があれば、約款名をアップデート
            if (mstAgreement.size() > 0) {
                for (AgreementMaster__c upd: mstAgreement) {
                    upd.AgreementName__c = mapDocument.get(srcId).Title;
                }
                update mstAgreement;
            }
            // マスタに登録が無い場合は、約款マスタを作成する
            else {
                if (mapDocument.get(srcId).Title != null) {
                    AgreementMaster__c newMST = new AgreementMaster__c(
                        AgreementName__c = mapDocument.get(srcId).Title,
                        AgreementFileID__c = srcId
                    );
                    insert newMST;
                }
            }
        }
    }
}