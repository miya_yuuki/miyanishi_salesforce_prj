global class AccountReceivableSummary2 {
	global static PageReference AccountReceivableSummary2() {
		List <TextManagement2__c> updAccountReceivable = [
			SELECT Id
			FROM TextManagement2__c TM
			WHERE TM.Item2_PaymentDueDate__c <= TODAY
			AND TM.NoBill__c = true
			AND TM.OrderItemStatus__c NOT IN ('途中解約', '請求停止', '更新済み', '契約変更', '取消')
			AND TM.DeliveryCompletedDate__c != null
			limit 75
		];
		if(updAccountReceivable.size() > 0) {
			for(TextManagement2__c thisAccountReceivable: updAccountReceivable) {
				thisAccountReceivable.NoBill__c = false;
			}
			update updAccountReceivable;
		}
		return null;
    }
}