global with sharing class NouhinDATAforPS extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public TextManagement2__c record{get;set;}	
			
	
		public Component2 Component2 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component249_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component249_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component249_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component249_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component251_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component251_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component251_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component251_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component253_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component253_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component253_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component253_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component17_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component17_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component17_op{get;set;}	
		public List<SelectOption> valueOptions_TextManagement2_c_CloudStatus_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component298_val {get;set;}	
		public SkyEditor2.TextHolder Component298_op{get;set;}	
			
		public TextManagement2__c Component122_val {get;set;}	
		public SkyEditor2.TextHolder Component122_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component133_val {get;set;}	
		public SkyEditor2.TextHolder Component133_op{get;set;}	
			
		public TextManagement2__c Component256_val {get;set;}	
		public SkyEditor2.TextHolder Component256_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component319_val {get;set;}	
		public SkyEditor2.TextHolder Component319_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component342_val {get;set;}	
		public SkyEditor2.TextHolder Component342_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component11_val {get;set;}	
		public SkyEditor2.TextHolder Component11_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component199_val {get;set;}	
		public SkyEditor2.TextHolder Component199_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component326_val {get;set;}	
		public SkyEditor2.TextHolder Component326_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component328_val {get;set;}	
		public SkyEditor2.TextHolder Component328_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component336_val {get;set;}	
		public SkyEditor2.TextHolder Component336_op{get;set;}	
			
	public String recordTypeRecordsJSON_TextManagement2_c {get; private set;}
	public String defaultRecordTypeId_TextManagement2_c {get; private set;}
	public String metadataJSON_TextManagement2_c {get; private set;}
	public Boolean QueryPagingConfirmationSetting {get;set;}
	{
	setApiVersion(31.0);
	}
		public NouhinDATAforPS(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = TextManagement2__c.fields.CloudStatus__c;
		f = TextManagement2__c.fields.Item2_DeliveryExistence__c;
		f = TextManagement2__c.fields.SalesPerson10__c;
		f = TextManagement2__c.fields.Account1__c;
		f = TextManagement2__c.fields.DeliveryPerson__c;
		f = TextManagement2__c.fields.sisakukigyou__c;
		f = TextManagement2__c.fields.SalesPersonUnit__c;
		f = TextManagement2__c.fields.Product__c;
		f = TextManagement2__c.fields.PSkubun__c;
		f = TextManagement2__c.fields.DeliveryMonth__c;
		f = TextManagement2__c.fields.DeliveryMonth2__c;
		f = TextManagement2__c.fields.StartMonth__c;
		f = TextManagement2__c.fields.sisakukigyou_kubun__c;
		f = TextManagement2__c.fields.OrderNo__c;
		f = TextManagement2__c.fields.ItemOrderNO__c;
		f = TextManagement2__c.fields.Item2_OrderProduct__c;
		f = TextManagement2__c.fields.Name;
		f = TextManagement2__c.fields.ProspectsAmount__c;
		f = TextManagement2__c.fields.CloudCost__c;
		f = TextManagement2__c.fields.PSGrossProfit__c;
		f = TextManagement2__c.fields.Item2_Keyword_letter_count__c;
		f = TextManagement2__c.fields.Selling_price__c;
		f = TextManagement2__c.fields.FurikaeSet__c;
		f = TextManagement2__c.fields.DeliveryTotalCount__c;
		f = TextManagement2__c.fields.SalesDeliveryCount__c;
		f = TextManagement2__c.fields.SalesDeliveryCountTotal__c;
		f = TextManagement2__c.fields.DeliveryCountOver1__c;
		f = TextManagement2__c.fields.DeliveryTotalAmount__c;
		f = TextManagement2__c.fields.DeliveryDaySales1__c;
		f = TextManagement2__c.fields.DeliveryDay__c;
		f = TextManagement2__c.fields.DeliveryDaySalesOverDay__c;
		f = TextManagement2__c.fields.Item2_DeliveryCount__c;
 f = TextManagement2__c.fields.DeliveryDaySales1__c;
 f = TextManagement2__c.fields.DeliveryDay__c;
 f = TextManagement2__c.fields.SalesHopeDeliveryDay__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = TextManagement2__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component249_from = new SkyEditor2__SkyEditorDummy__c();	
				Component249_to = new SkyEditor2__SkyEditorDummy__c();	
				Component249_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component249_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component251_from = new SkyEditor2__SkyEditorDummy__c();	
				Component251_to = new SkyEditor2__SkyEditorDummy__c();	
				Component251_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component251_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component253_from = new SkyEditor2__SkyEditorDummy__c();	
				Component253_to = new SkyEditor2__SkyEditorDummy__c();	
				Component253_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component253_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				TextManagement2__c lookupObjComponent61 = new TextManagement2__c();	
				Component17_val = new SkyEditor2__SkyEditorDummy__c();	
				Component17_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component17_op = new SkyEditor2.TextHolder();	
				valueOptions_TextManagement2_c_CloudStatus_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : TextManagement2__c.CloudStatus__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_TextManagement2_c_CloudStatus_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component298_val = new SkyEditor2__SkyEditorDummy__c();	
				Component298_op = new SkyEditor2.TextHolder();	
					
				Component122_val = lookupObjComponent61;	
				Component122_op = new SkyEditor2.TextHolder();	
					
				Component133_val = new SkyEditor2__SkyEditorDummy__c();	
				Component133_op = new SkyEditor2.TextHolder();	
					
				Component256_val = lookupObjComponent61;	
				Component256_op = new SkyEditor2.TextHolder();	
					
				Component319_val = new SkyEditor2__SkyEditorDummy__c();	
				Component319_op = new SkyEditor2.TextHolder();	
					
				Component342_val = new SkyEditor2__SkyEditorDummy__c();	
				Component342_op = new SkyEditor2.TextHolder();	
					
				Component11_val = new SkyEditor2__SkyEditorDummy__c();	
				Component11_op = new SkyEditor2.TextHolder();	
					
				Component199_val = new SkyEditor2__SkyEditorDummy__c();	
				Component199_op = new SkyEditor2.TextHolder();	
					
				Component326_val = new SkyEditor2__SkyEditorDummy__c();	
				Component326_op = new SkyEditor2.TextHolder();	
					
				Component328_val = new SkyEditor2__SkyEditorDummy__c();	
				Component328_op = new SkyEditor2.TextHolder();	
					
				Component336_val = new SkyEditor2__SkyEditorDummy__c();	
				Component336_op = new SkyEditor2.TextHolder();	
					
				queryMap.put(	
					'Component2',	
					new SkyEditor2.Query('TextManagement2__c')
						.addFieldAsOutput('SalesPerson10__c')
						.addFieldAsOutput('sisakukigyou_kubun__c')
						.addFieldAsOutput('Product__c')
						.addFieldAsOutput('OrderNo__c')
						.addFieldAsOutput('ItemOrderNO__c')
						.addFieldAsOutput('Item2_OrderProduct__c')
						.addFieldAsOutput('Name')
						.addFieldAsOutput('ProspectsAmount__c')
						.addFieldAsOutput('CloudCost__c')
						.addFieldAsOutput('PSGrossProfit__c')
						.addFieldAsOutput('Item2_Keyword_letter_count__c')
						.addFieldAsOutput('Selling_price__c')
						.addField('FurikaeSet__c')
						.addFieldAsOutput('CloudStatus__c')
						.addFieldAsOutput('DeliveryTotalCount__c')
						.addField('SalesDeliveryCount__c')
						.addFieldAsOutput('SalesDeliveryCountTotal__c')
						.addFieldAsOutput('DeliveryCountOver1__c')
						.addFieldAsOutput('DeliveryTotalAmount__c')
						.addField('DeliveryDaySales1__c')
						.addFieldAsOutput('DeliveryDay__c')
						.addFieldAsOutput('DeliveryDaySalesOverDay__c')
						.addFieldAsOutput('Item2_DeliveryCount__c')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component249_from, 'SkyEditor2__Date__c', 'DeliveryDaySales1__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component249_to, 'SkyEditor2__Date__c', 'DeliveryDaySales1__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component249_isNull, 'SkyEditor2__Date__c', 'DeliveryDaySales1__c', Component249_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component251_from, 'SkyEditor2__Date__c', 'DeliveryDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component251_to, 'SkyEditor2__Date__c', 'DeliveryDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component251_isNull, 'SkyEditor2__Date__c', 'DeliveryDay__c', Component251_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component253_from, 'SkyEditor2__Date__c', 'SalesHopeDeliveryDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component253_to, 'SkyEditor2__Date__c', 'SalesHopeDeliveryDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component253_isNull, 'SkyEditor2__Date__c', 'SalesHopeDeliveryDay__c', Component253_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component17_val_dummy, 'SkyEditor2__Text__c','CloudStatus__c', Component17_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component298_val, 'SkyEditor2__Text__c', 'Item2_DeliveryExistence__c', Component298_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component122_val, 'SalesPerson10__c', 'SalesPerson10__c', Component122_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component133_val, 'SkyEditor2__Text__c', 'Account1__c', Component133_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component256_val, 'DeliveryPerson__c', 'DeliveryPerson__c', Component256_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component319_val, 'SkyEditor2__Text__c', 'sisakukigyou__c', Component319_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component342_val, 'SkyEditor2__Text__c', 'SalesPersonUnit__c', Component342_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component11_val, 'SkyEditor2__Text__c', 'Product__c', Component11_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component199_val, 'SkyEditor2__Text__c', 'PSkubun__c', Component199_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component326_val, 'SkyEditor2__Text__c', 'DeliveryMonth__c', Component326_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component328_val, 'SkyEditor2__Text__c', 'DeliveryMonth2__c', Component328_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component336_val, 'SkyEditor2__Text__c', 'StartMonth__c', Component336_op, true, 0, false ))
				);	
					
					Component2 = new Component2(new List<TextManagement2__c>(), new List<Component2Item>(), new List<TextManagement2__c>(), null, queryMap.get('Component2'));
				 Component2.setPageSize(100);
				listItemHolders.put('Component2', Component2);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(TextManagement2__c.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = false;
					Cookie c = ApexPages.currentPage().getCookies().get('QueryPagingConfirm');
					QueryPagingConfirmationSetting = c != null && c.getValue() == '0';
			presetSystemParams();
			Component2.extender = this.extender;
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_TextManagement2_c_CloudStatus_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Item2_DeliveryExistence_c() { 
			return getOperatorOptions('TextManagement2__c', 'Item2_DeliveryExistence__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_SalesPerson10_c() { 
			return getOperatorOptions('TextManagement2__c', 'SalesPerson10__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Account1_c() { 
			return getOperatorOptions('TextManagement2__c', 'Account1__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_DeliveryPerson_c() { 
			return getOperatorOptions('TextManagement2__c', 'DeliveryPerson__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_sisakukigyou_c() { 
			return getOperatorOptions('TextManagement2__c', 'sisakukigyou__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_SalesPersonUnit_c() { 
			return getOperatorOptions('TextManagement2__c', 'SalesPersonUnit__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Product_c() { 
			return getOperatorOptions('TextManagement2__c', 'Product__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_PSkubun_c() { 
			return getOperatorOptions('TextManagement2__c', 'PSkubun__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_DeliveryMonth_c() { 
			return getOperatorOptions('TextManagement2__c', 'DeliveryMonth__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_DeliveryMonth2_c() { 
			return getOperatorOptions('TextManagement2__c', 'DeliveryMonth2__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_StartMonth_c() { 
			return getOperatorOptions('TextManagement2__c', 'StartMonth__c');	
		}	
			
			
	global with sharing class Component2Item extends SkyEditor2.ListItem {
		public TextManagement2__c record{get; private set;}
		@TestVisible
		Component2Item(Component2 holder, TextManagement2__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component2 extends SkyEditor2.QueryPagingList {
		public List<Component2Item> items{get; private set;}
		@TestVisible
			Component2(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector, SkyEditor2.Query query) {
			super(records, items, deleteRecords, recordTypeSelector, query);
			this.items = (List<Component2Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component2Item(this, (TextManagement2__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
		public void doFirst(){first();}
		public void doPrevious(){previous();}
		public void doNext(){next();}
		public void doLast(){last();}
		public void doSort(){sort();}

	}

	public TextManagement2__c Component2_table_Conversion { get { return new TextManagement2__c();}}
	
	public String Component2_table_selectval { get; set; }
	
	
			
	}