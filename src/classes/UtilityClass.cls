public with sharing class UtilityClass {

	// 置換処理
	public static String prcReplace(String strVal, String bef, String aft)
	{
		if (strVal == null) return null;
		return strVal.replaceAll(bef, aft);
	}
	
	// Decimal型をString型にパース
	public static String DtoS(Decimal decVal)
	{
		if (decVal == null) return null;
		return String.valueOf(decVal);
	}
	
	// String型をDecimal型にパース
	public static Decimal StoD(String strVal)
	{
		if (strVal == null) return null;
		return Decimal.valueOf(strVal);
	}
	
	// DecimalをIntegerにパース
	public static Integer DtoI(Decimal dec)
	{
		if (dec == null) return null;
		return Integer.valueOf(dec);
	}
	
	// 日付に月を加算
	public static Date prcAddMonths(Date dt, Integer add)
	{
		if (dt == null || add == null) return null;
		return dt.addMonths(add);
	}
	
	// 日付に日を加算
	public static Date prcAddDays(Date dt, Integer i)
	{
		if (dt == null) return null;
		return dt.addDays(i);
	}
	
	// オブジェクトのフィールドを取得
	public static String getCustomFieldsNames(Schema.SObjecttype tp){
		String res = '';
		Map<String,Schema.sObjectField> fmap = tp.getDescribe().fields.getMap();
		for (String fn:fmap.keySet()) {
	        if (fn.endswith('__c')) res = res + fn + ',';
		}
        return res.substring(0, res.length() - 1);
	}
	
	// 日付型からyyyy-MM-ddの文字列型を生成
	public static String retStringDt(Date rdt)
	{
		if (rdt == null) return null;
		Datetime rDate = Datetime.newInstance(rdt.year(), rdt.month(), rdt.day()).addHours(9);
		return rDate.formatGmt('yyyy-MM-dd');
	}
	
}