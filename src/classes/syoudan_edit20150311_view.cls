global with sharing class syoudan_edit20150311_view extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public Opportunity record {get{return (Opportunity)mainRecord;}}
	public Component4612 Component4612 {get; private set;}
	{
	setApiVersion(42.0);
	}
	public syoudan_edit20150311_view(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = Opportunity.fields.Group__c;
		f = Opportunity.fields.Name;
		f = Opportunity.fields.Opportunity_Type__c;
		f = Opportunity.fields.CloseDate;
		f = Opportunity.fields.OpportunityCopy__c;
		f = Opportunity.fields.Opportunity_Partner__c;
		f = Opportunity.fields.StageName;
		f = Opportunity.fields.AccountId;
		f = Opportunity.fields.Opportunity_endClient1__c;
		f = Opportunity.fields.AmountTotal__c;
		f = Opportunity.fields.Opportunity_SalesPerson_Unit__c;
		f = Opportunity.fields.account_web_site__c;
		f = Opportunity.fields.BoardDiscussed__c;
		f = Opportunity.fields.Opportunity_Support__c;
		f = Opportunity.fields.account_web_site_unnecessary__c;
		f = Opportunity.fields.Opportunity_Competition__c;
		f = Opportunity.fields.MA_access_flag__c;
		f = Opportunity.fields.AppointmentDay__c;
		f = Opportunity.fields.suggest_date__c;
		f = Opportunity.fields.non_recorded_flg__c;
		f = Opportunity.fields.WebSiteTypeView__c;
		f = Opportunity.fields.business_model_type_view__c;
		f = Opportunity.fields.Post_evaluation_summary__c;
		f = Opportunity.fields.Post_evaluation__c;
		f = Opportunity.fields.AppointmentType__c;
		f = Opportunity.fields.SQLType__c;
		f = Opportunity.fields.contents_order_status__c;
		f = Opportunity.fields.has_not_appointment_counts__c;
		f = Opportunity.fields.Opportunity_lead1__c;
		f = Opportunity.fields.Opportunity_lead2__c;
		f = Opportunity.fields.CampaignId;
		f = Opportunity.fields.not_campaign_flg__c;
		f = Opportunity.fields.Opportunity_deadDay__c;
		f = Opportunity.fields.NA_memo__c;
		f = Opportunity.fields.Dead_type__c;
		f = Opportunity.fields.settled_sales_person_unit__c;
		f = Opportunity.fields.Opportunity_DeadMemo__c;
		f = Opportunity.fields.MQLSalesPerson__c;
		f = Opportunity.fields.AppointmentSalesPerson__c;
		f = Opportunity.fields.LeadEvaluation__c;
		f = Opportunity.fields.MQLDate__c;
		f = Opportunity.fields.MQL_first_visit_date__c;
		f = Opportunity.fields.karute__c;
		f = Opportunity.fields.karuteCheck__c;
		f = Opportunity.fields.review_practitioner__c;
		f = Opportunity.fields.review_status__c;
		f = Opportunity.fields.suggestion_importance_summary__c;
		f = Opportunity.fields.suggestion_importance__c;
		f = Opportunity.fields.document__c;
		f = Opportunity.fields.document_creating_user__c;
		f = Opportunity.fields.documents_delivery_date__c;
		f = Opportunity.fields.agreement_probability__c;
		f = Opportunity.fields.this_month_approach__c;
		f = Opportunity.fields.Opportunity_WEBsiteRank__c;
		f = Opportunity.fields.SEOMeasureStatus__c;
		f = Opportunity.fields.writing_resource_summary__c;
		f = Opportunity.fields.SEO_contents_budget_per_month__c;
		f = Opportunity.fields.Products__c;
		f = Opportunity.fields.contents_measure_status__c;
		f = Opportunity.fields.Budget_acquisition__c;
		f = Opportunity.fields.Customer_needs_level__c;
		f = Opportunity.fields.Settled_type__c;
		f = Opportunity.fields.Customer_level__c;
		f = Opportunity.fields.Introduction_start_timing__c;
		f = Opportunity.fields.Competition_winner__c;
		f = Opportunity.fields.Record_view__c;
		f = Opportunity.fields.Opportunity_NoRecord__c;
		f = Opportunity.fields.account_BusinessModel__c;
		f = Opportunity.fields.Opportunity_BillPostalCode2__c;
		f = Opportunity.fields.Phone__c;
		f = Opportunity.fields.account_Industry__c;
		f = Opportunity.fields.Opportunity_BillPrefecture2__c;
		f = Opportunity.fields.Website__c;
		f = Opportunity.fields.account_Industrycategory__c;
		f = Opportunity.fields.Opportunity_BillCity2__c;
		f = Opportunity.fields.TransferAccountNumber1__c;
		f = Opportunity.fields.account_CreditLevel__c;
		f = Opportunity.fields.Opportunity_BillBuilding2__c;
		f = Opportunity.fields.AccountEdit__c;
		f = Opportunity.fields.account_CreditDay__c;
		f = Opportunity.fields.hansyaDay__c;
		f = Opportunity.fields.CreditSpace__c;
		f = Opportunity.fields.CompanyCheck__c;
		f = Opportunity.fields.CreditBalance__c;
		f = Opportunity.fields.hansyaMemo__c;
		f = Opportunity.fields.CreditDecisionPrice__c;
		f = AccountCustomer__c.fields.CustomerName__c;
		f = AccountCustomer__c.fields.account__c;
		f = AccountCustomer__c.fields.department__c;
		f = AccountCustomer__c.fields.Title__c;
		f = AccountCustomer__c.fields.yakuwari__c;
		f = AccountCustomer__c.fields.Mail__c;
		f = AccountCustomer__c.fields.Report1__c;
		f = AccountCustomer__c.fields.PartnerReport__c;
		f = AccountCustomer__c.fields.ReportSend1__c;
		f = AccountCustomer__c.fields.PartnerReport_c__c;
		f = Opportunity.fields.Opportunity_BillingMethod__c;
		f = Opportunity.fields.Opportunity_PaymentSiteMaster__c;
		f = Opportunity.fields.Opportunity_PaymentMethod__c;
		f = Opportunity.fields.CaseApproval__c;
		f = Opportunity.fields.opportunityBillMemo__c;
		f = Opportunity.fields.AddressCheck__c;
		f = Opportunity.fields.Opportunity_Memo__c;
		f = Opportunity.fields.CustomerNamePRINT__c;
		f = Opportunity.fields.OrderProvisional__c;
		f = Opportunity.fields.OrderProvisionalMemo__c;
		f = Opportunity.fields.SalvageScheduledDate__c;
		f = Opportunity.fields.QuoteExpirationDate__c;
		f = Opportunity.fields.Opportunity_EstimateMemo__c;
		f = Opportunity.fields.notices__c;
		f = Opportunity.fields.Opportunity_Title__c;
		f = Opportunity.fields.Opportunity_BillPhone__c;
		f = Opportunity.fields.Opportunity_Billaccount__c;
		f = Opportunity.fields.Opportunity_BillKana__c;
		f = Opportunity.fields.Opportunity_BillFax__c;
		f = Opportunity.fields.Opportunity_BillPostalCode__c;
		f = Opportunity.fields.Opportunity_BillDepartment__c;
		f = Opportunity.fields.Opportunity_BillEmail__c;
		f = Opportunity.fields.Opportunity_BillPrefecture__c;
		f = Opportunity.fields.Opportunity_BillTitle__c;
		f = Opportunity.fields.Opportunity_BillCity__c;
		f = Opportunity.fields.Opportunity_BillRole__c;
		f = Opportunity.fields.Opportunity_BillAddress__c;
		f = Opportunity.fields.ContacEdit__c;
		f = Opportunity.fields.Opportunity_BillBuilding__c;
		f = Opportunity.fields.Agreement1__c;
		f = Opportunity.fields.Agreement2__c;
		f = Opportunity.fields.Agreement3__c;
		f = Opportunity.fields.Agreement4__c;
		f = Opportunity.fields.Agreement5__c;
		f = Opportunity.fields.Agreement6__c;
		f = Opportunity.fields.Agreement7__c;
		f = Opportunity.fields.Agreement8__c;
		f = Opportunity.fields.Agreement9__c;
		f = Opportunity.fields.Agreement10__c;
		f = Opportunity.fields.Agreement11__c;
		f = Opportunity.fields.Agreement12__c;
		f = Opportunity.fields.Agreement17__c;
		f = Opportunity.fields.Agreement18__c;
		f = Opportunity.fields.Agreement19__c;
		f = Opportunity.fields.Agreement20__c;
		f = Opportunity.fields.Agreement13__c;
		f = Opportunity.fields.Agreement14__c;
		f = Opportunity.fields.Agreement15__c;
		f = Opportunity.fields.CreatedById;
		f = Opportunity.fields.LastModifiedById;
		f = Opportunity.fields.syouninn__c;
		f = Opportunity.fields.CreatedDate;
		f = Opportunity.fields.LastModifiedDate;
		f = Opportunity.fields.Salesforce_ID__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = Opportunity.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'syoudan_edit20150311_view';
			mainQuery = new SkyEditor2.Query('Opportunity');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Group__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('Opportunity_Type__c');
			mainQuery.addFieldAsOutput('CloseDate');
			mainQuery.addFieldAsOutput('OpportunityCopy__c');
			mainQuery.addFieldAsOutput('Opportunity_Partner__c');
			mainQuery.addFieldAsOutput('StageName');
			mainQuery.addFieldAsOutput('AccountId');
			mainQuery.addFieldAsOutput('Opportunity_endClient1__c');
			mainQuery.addFieldAsOutput('AmountTotal__c');
			mainQuery.addFieldAsOutput('Opportunity_SalesPerson_Unit__c');
			mainQuery.addFieldAsOutput('account_web_site__c');
			mainQuery.addFieldAsOutput('BoardDiscussed__c');
			mainQuery.addFieldAsOutput('Opportunity_Support__c');
			mainQuery.addFieldAsOutput('account_web_site_unnecessary__c');
			mainQuery.addFieldAsOutput('Opportunity_Competition__c');
			mainQuery.addFieldAsOutput('MA_access_flag__c');
			mainQuery.addFieldAsOutput('AppointmentDay__c');
			mainQuery.addFieldAsOutput('suggest_date__c');
			mainQuery.addFieldAsOutput('non_recorded_flg__c');
			mainQuery.addFieldAsOutput('WebSiteTypeView__c');
			mainQuery.addFieldAsOutput('business_model_type_view__c');
			mainQuery.addFieldAsOutput('Post_evaluation_summary__c');
			mainQuery.addFieldAsOutput('Post_evaluation__c');
			mainQuery.addFieldAsOutput('AppointmentType__c');
			mainQuery.addFieldAsOutput('SQLType__c');
			mainQuery.addFieldAsOutput('contents_order_status__c');
			mainQuery.addFieldAsOutput('has_not_appointment_counts__c');
			mainQuery.addFieldAsOutput('Opportunity_lead1__c');
			mainQuery.addFieldAsOutput('Opportunity_lead2__c');
			mainQuery.addFieldAsOutput('CampaignId');
			mainQuery.addFieldAsOutput('not_campaign_flg__c');
			mainQuery.addFieldAsOutput('Opportunity_deadDay__c');
			mainQuery.addFieldAsOutput('NA_memo__c');
			mainQuery.addFieldAsOutput('Dead_type__c');
			mainQuery.addFieldAsOutput('settled_sales_person_unit__c');
			mainQuery.addFieldAsOutput('Opportunity_DeadMemo__c');
			mainQuery.addFieldAsOutput('MQLSalesPerson__c');
			mainQuery.addFieldAsOutput('AppointmentSalesPerson__c');
			mainQuery.addFieldAsOutput('LeadEvaluation__c');
			mainQuery.addFieldAsOutput('MQLDate__c');
			mainQuery.addFieldAsOutput('MQL_first_visit_date__c');
			mainQuery.addFieldAsOutput('karute__c');
			mainQuery.addFieldAsOutput('karuteCheck__c');
			mainQuery.addFieldAsOutput('review_practitioner__c');
			mainQuery.addFieldAsOutput('review_status__c');
			mainQuery.addFieldAsOutput('suggestion_importance_summary__c');
			mainQuery.addFieldAsOutput('suggestion_importance__c');
			mainQuery.addFieldAsOutput('document__c');
			mainQuery.addFieldAsOutput('document_creating_user__c');
			mainQuery.addFieldAsOutput('documents_delivery_date__c');
			mainQuery.addFieldAsOutput('agreement_probability__c');
			mainQuery.addFieldAsOutput('this_month_approach__c');
			mainQuery.addFieldAsOutput('Opportunity_WEBsiteRank__c');
			mainQuery.addFieldAsOutput('SEOMeasureStatus__c');
			mainQuery.addFieldAsOutput('writing_resource_summary__c');
			mainQuery.addFieldAsOutput('SEO_contents_budget_per_month__c');
			mainQuery.addFieldAsOutput('Products__c');
			mainQuery.addFieldAsOutput('contents_measure_status__c');
			mainQuery.addFieldAsOutput('Budget_acquisition__c');
			mainQuery.addFieldAsOutput('Customer_needs_level__c');
			mainQuery.addFieldAsOutput('Settled_type__c');
			mainQuery.addFieldAsOutput('Customer_level__c');
			mainQuery.addFieldAsOutput('Introduction_start_timing__c');
			mainQuery.addFieldAsOutput('Competition_winner__c');
			mainQuery.addFieldAsOutput('Record_view__c');
			mainQuery.addFieldAsOutput('Opportunity_NoRecord__c');
			mainQuery.addFieldAsOutput('account_BusinessModel__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPostalCode2__c');
			mainQuery.addFieldAsOutput('Phone__c');
			mainQuery.addFieldAsOutput('account_Industry__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPrefecture2__c');
			mainQuery.addFieldAsOutput('Website__c');
			mainQuery.addFieldAsOutput('account_Industrycategory__c');
			mainQuery.addFieldAsOutput('Opportunity_BillCity2__c');
			mainQuery.addFieldAsOutput('TransferAccountNumber1__c');
			mainQuery.addFieldAsOutput('account_CreditLevel__c');
			mainQuery.addFieldAsOutput('Opportunity_BillBuilding2__c');
			mainQuery.addFieldAsOutput('AccountEdit__c');
			mainQuery.addFieldAsOutput('account_CreditDay__c');
			mainQuery.addFieldAsOutput('hansyaDay__c');
			mainQuery.addFieldAsOutput('CreditSpace__c');
			mainQuery.addFieldAsOutput('CompanyCheck__c');
			mainQuery.addFieldAsOutput('CreditBalance__c');
			mainQuery.addFieldAsOutput('hansyaMemo__c');
			mainQuery.addFieldAsOutput('CreditDecisionPrice__c');
			mainQuery.addFieldAsOutput('Opportunity_BillingMethod__c');
			mainQuery.addFieldAsOutput('Opportunity_PaymentSiteMaster__c');
			mainQuery.addFieldAsOutput('Opportunity_PaymentMethod__c');
			mainQuery.addFieldAsOutput('CaseApproval__c');
			mainQuery.addFieldAsOutput('opportunityBillMemo__c');
			mainQuery.addFieldAsOutput('AddressCheck__c');
			mainQuery.addFieldAsOutput('Opportunity_Memo__c');
			mainQuery.addFieldAsOutput('CustomerNamePRINT__c');
			mainQuery.addFieldAsOutput('OrderProvisional__c');
			mainQuery.addFieldAsOutput('OrderProvisionalMemo__c');
			mainQuery.addFieldAsOutput('SalvageScheduledDate__c');
			mainQuery.addFieldAsOutput('QuoteExpirationDate__c');
			mainQuery.addFieldAsOutput('Opportunity_EstimateMemo__c');
			mainQuery.addFieldAsOutput('notices__c');
			mainQuery.addFieldAsOutput('Opportunity_Title__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPhone__c');
			mainQuery.addFieldAsOutput('Opportunity_Billaccount__c');
			mainQuery.addFieldAsOutput('Opportunity_BillKana__c');
			mainQuery.addFieldAsOutput('Opportunity_BillFax__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPostalCode__c');
			mainQuery.addFieldAsOutput('Opportunity_BillDepartment__c');
			mainQuery.addFieldAsOutput('Opportunity_BillEmail__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPrefecture__c');
			mainQuery.addFieldAsOutput('Opportunity_BillTitle__c');
			mainQuery.addFieldAsOutput('Opportunity_BillCity__c');
			mainQuery.addFieldAsOutput('Opportunity_BillRole__c');
			mainQuery.addFieldAsOutput('Opportunity_BillAddress__c');
			mainQuery.addFieldAsOutput('ContacEdit__c');
			mainQuery.addFieldAsOutput('Opportunity_BillBuilding__c');
			mainQuery.addFieldAsOutput('Agreement1__c');
			mainQuery.addFieldAsOutput('Agreement2__c');
			mainQuery.addFieldAsOutput('Agreement3__c');
			mainQuery.addFieldAsOutput('Agreement4__c');
			mainQuery.addFieldAsOutput('Agreement5__c');
			mainQuery.addFieldAsOutput('Agreement6__c');
			mainQuery.addFieldAsOutput('Agreement7__c');
			mainQuery.addFieldAsOutput('Agreement8__c');
			mainQuery.addFieldAsOutput('Agreement9__c');
			mainQuery.addFieldAsOutput('Agreement10__c');
			mainQuery.addFieldAsOutput('Agreement11__c');
			mainQuery.addFieldAsOutput('Agreement12__c');
			mainQuery.addFieldAsOutput('Agreement17__c');
			mainQuery.addFieldAsOutput('Agreement18__c');
			mainQuery.addFieldAsOutput('Agreement19__c');
			mainQuery.addFieldAsOutput('Agreement20__c');
			mainQuery.addFieldAsOutput('Agreement13__c');
			mainQuery.addFieldAsOutput('Agreement14__c');
			mainQuery.addFieldAsOutput('Agreement15__c');
			mainQuery.addFieldAsOutput('CreatedById');
			mainQuery.addFieldAsOutput('LastModifiedById');
			mainQuery.addFieldAsOutput('syouninn__c');
			mainQuery.addFieldAsOutput('CreatedDate');
			mainQuery.addFieldAsOutput('LastModifiedDate');
			mainQuery.addFieldAsOutput('Salesforce_ID__c');
			mainQuery.addFieldAsOutput('LeadSource');
			mainQuery.addFieldAsOutput('Id');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			Component4612 = new Component4612(new List<AccountCustomer__c>(), new List<Component4612Item>(), new List<AccountCustomer__c>(), null);
			listItemHolders.put('Component4612', Component4612);
			query = new SkyEditor2.Query('AccountCustomer__c');
			query.addFieldAsOutput('CustomerName__c');
			query.addFieldAsOutput('account__c');
			query.addFieldAsOutput('department__c');
			query.addFieldAsOutput('Title__c');
			query.addFieldAsOutput('yakuwari__c');
			query.addFieldAsOutput('Mail__c');
			query.addFieldAsOutput('Report1__c');
			query.addFieldAsOutput('PartnerReport__c');
			query.addFieldAsOutput('ReportSend1__c');
			query.addFieldAsOutput('PartnerReport_c__c');
			query.addWhere('Opportunity_customer__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('Component4612', 'Opportunity_customer__c');
			Component4612.queryRelatedEvent = False;
			query.limitRecords(10);
			queryMap.put('Component4612', query);
			registRelatedList('Opportunity_customer__r', 'Component4612');
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('OpportunityCopy__c', 'OpportunityCopy__c');
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			Component4612.extender = this.extender;
			if (record.Id == null) {
				saveOldValues();
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class Component4612Item extends SkyEditor2.ListItem {
		public AccountCustomer__c record{get; private set;}
		@TestVisible
		Component4612Item(Component4612 holder, AccountCustomer__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component4612 extends SkyEditor2.ListItemHolder {
		public List<Component4612Item> items{get; private set;}
		@TestVisible
			Component4612(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component4612Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component4612Item(this, (AccountCustomer__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}