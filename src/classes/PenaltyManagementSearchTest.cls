@isTest
private with sharing class PenaltyManagementSearchTest{
		private static testMethod void testPageMethods() {	
			PenaltyManagementSearch page = new PenaltyManagementSearch(new ApexPages.StandardController(new PenaltyManagement__c()));	
			page.getOperatorOptions_PenaltyManagement_c_regularStatus_c_multi();	
			page.getOperatorOptions_karute_c_AccountName_c();	
			page.getOperatorOptions_PenaltyManagement_c_penaltyStatus_c_multi();	
			page.getOperatorOptions_karute_c_URL_c();	
			page.getOperatorOptions_karute_c_OwnerId();	
			System.assert(true);
		}	
			
	private static testMethod void testComponent3() {
		PenaltyManagementSearch.Component3 Component3 = new PenaltyManagementSearch.Component3(new List<PenaltyManagement__c>(), new List<PenaltyManagementSearch.Component3Item>(), new List<PenaltyManagement__c>(), null);
		Component3.create(new PenaltyManagement__c());
		Component3.doDeleteSelectedItems();
		System.assert(true);
	}
	
}