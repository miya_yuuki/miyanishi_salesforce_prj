@isTest
private with sharing class SearchOpportunityProductTest{
		private static testMethod void testPageMethods() {	
			SearchOpportunityProduct page = new SearchOpportunityProduct(new ApexPages.StandardController(new OpportunityProduct__c()));	
			page.getOperatorOptions_Opportunity_Opportunity_SalesPerson_Unit_c();	
			page.getOperatorOptions_Opportunity_Name();	
			page.getOperatorOptions_Opportunity_Opportunity_endClient1_c();	
			page.getOperatorOptions_Opportunity_StageName_multi();	
			page.getOperatorOptions_Opportunity_WEBTitle_c();	

		Integer defaultSize;

		defaultSize = page.Component3.items.size();
		page.Component3.add();
		System.assertEquals(defaultSize + 1, page.Component3.items.size());
		page.Component3.items[defaultSize].selected = true;
		page.Component3.doDeleteSelectedItems();
			System.assert(true);
		}	
			
	private static testMethod void testComponent3() {
		SearchOpportunityProduct.Component3 Component3 = new SearchOpportunityProduct.Component3(new List<OpportunityProduct__c>(), new List<SearchOpportunityProduct.Component3Item>(), new List<OpportunityProduct__c>(), null);
		Component3.create(new OpportunityProduct__c());
		Component3.doDeleteSelectedItems();
		System.assert(true);
	}
	
	@isTest
	private static void testLightDataTables(){

		System.assert(true);
	}
}