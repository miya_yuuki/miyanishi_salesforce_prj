global with sharing class nouhinkanriDATA extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public TextManagement2__c record{get;set;}	
			
	
		public Component2 Component2 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component296_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component296_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component296_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component296_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component298_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component298_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component298_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component298_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component251_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component251_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component251_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component251_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component253_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component253_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component253_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component253_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component209_val {get;set;}	
		public SkyEditor2.TextHolder Component209_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component275_val {get;set;}	
		public SkyEditor2.TextHolder Component275_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component198_val {get;set;}	
		public SkyEditor2.TextHolder Component198_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component289_val {get;set;}	
		public SkyEditor2.TextHolder Component289_op{get;set;}	
			
		public TextManagement2__c Component122_val {get;set;}	
		public SkyEditor2.TextHolder Component122_op{get;set;}	
			
		public TextManagement2__c Component291_val {get;set;}	
		public SkyEditor2.TextHolder Component291_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component133_val {get;set;}	
		public SkyEditor2.TextHolder Component133_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component280_val {get;set;}	
		public SkyEditor2.TextHolder Component280_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component305_val {get;set;}	
		public SkyEditor2.TextHolder Component305_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component17_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component17_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component17_op{get;set;}	
		public List<SelectOption> valueOptions_TextManagement2_c_CloudStatus_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component15_val {get;set;}	
		public SkyEditor2.TextHolder Component15_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component213_val {get;set;}	
		public SkyEditor2.TextHolder Component213_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component295_val {get;set;}	
		public SkyEditor2.TextHolder Component295_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component293_val {get;set;}	
		public SkyEditor2.TextHolder Component293_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component244_val {get;set;}	
		public SkyEditor2.TextHolder Component244_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component246_val {get;set;}	
		public SkyEditor2.TextHolder Component246_op{get;set;}	
			
	public String recordTypeRecordsJSON_TextManagement2_c {get; private set;}
	public String defaultRecordTypeId_TextManagement2_c {get; private set;}
	public String metadataJSON_TextManagement2_c {get; private set;}
	public Boolean QueryPagingConfirmationSetting {get;set;}
	{
	setApiVersion(31.0);
	}
		public nouhinkanriDATA(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = TextManagement2__c.fields.Name;
		f = TextManagement2__c.fields.OrderNo__c;
		f = TextManagement2__c.fields.RecordTypeId;
		f = TextManagement2__c.fields.SalesPersonUnit1__c;
		f = TextManagement2__c.fields.SalesPerson10__c;
		f = TextManagement2__c.fields.DeliveryPerson__c;
		f = TextManagement2__c.fields.Account1__c;
		f = TextManagement2__c.fields.Product11__c;
		f = TextManagement2__c.fields.ContractMonths__c;
		f = TextManagement2__c.fields.Status__c;
		f = TextManagement2__c.fields.CloudStatus__c;
		f = TextManagement2__c.fields.Item2_DeliveryExistence__c;
		f = TextManagement2__c.fields.AutomaticUpdate__c;
		f = TextManagement2__c.fields.BillingTiming__c;
		f = TextManagement2__c.fields.StartDate__c;
		f = TextManagement2__c.fields.EndDate__c;
		f = TextManagement2__c.fields.Item2_OrderProduct__c;
		f = TextManagement2__c.fields.ItemOrderNO__c;
		f = TextManagement2__c.fields.SalesDeliveryDay__c;
		f = TextManagement2__c.fields.DeliveryDaySales1__c;
		f = TextManagement2__c.fields.DeliveryDay__c;
		f = TextManagement2__c.fields.SalesDeliveryCount__c;
		f = TextManagement2__c.fields.DeliveryCount__c;
		f = TextManagement2__c.fields.SalesPercentage1__c;
		f = TextManagement2__c.fields.SalesPersonSubUnit__c;
		f = TextManagement2__c.fields.SalesPersonSub__c;
		f = TextManagement2__c.fields.SalesPercentage2__c;
		f = TextManagement2__c.fields.DeliveryPersonUnit__c;
		f = TextManagement2__c.fields.Bill_Main__c;
		f = TextManagement2__c.fields.BillSet__c;
 f = TextManagement2__c.fields.OrderItem_Bill_Main__c;
 f = TextManagement2__c.fields.SalesDeliveryDay__c;
 f = TextManagement2__c.fields.DeliveryDaySales1__c;
 f = TextManagement2__c.fields.DeliveryDay__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = TextManagement2__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component296_from = new SkyEditor2__SkyEditorDummy__c();	
				Component296_to = new SkyEditor2__SkyEditorDummy__c();	
				Component296_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component296_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component298_from = new SkyEditor2__SkyEditorDummy__c();	
				Component298_to = new SkyEditor2__SkyEditorDummy__c();	
				Component298_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component298_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component251_from = new SkyEditor2__SkyEditorDummy__c();	
				Component251_to = new SkyEditor2__SkyEditorDummy__c();	
				Component251_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component251_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component253_from = new SkyEditor2__SkyEditorDummy__c();	
				Component253_to = new SkyEditor2__SkyEditorDummy__c();	
				Component253_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component253_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				TextManagement2__c lookupObjComponent61 = new TextManagement2__c();	
				Component209_val = new SkyEditor2__SkyEditorDummy__c();	
				Component209_op = new SkyEditor2.TextHolder();	
					
				Component275_val = new SkyEditor2__SkyEditorDummy__c();	
				Component275_op = new SkyEditor2.TextHolder();	
					
				Component198_val = new SkyEditor2__SkyEditorDummy__c();	
				Component198_op = new SkyEditor2.TextHolder();	
					
				Component289_val = new SkyEditor2__SkyEditorDummy__c();	
				Component289_op = new SkyEditor2.TextHolder();	
					
				Component122_val = lookupObjComponent61;	
				Component122_op = new SkyEditor2.TextHolder();	
					
				Component291_val = lookupObjComponent61;	
				Component291_op = new SkyEditor2.TextHolder();	
					
				Component133_val = new SkyEditor2__SkyEditorDummy__c();	
				Component133_op = new SkyEditor2.TextHolder();	
					
				Component280_val = new SkyEditor2__SkyEditorDummy__c();	
				Component280_op = new SkyEditor2.TextHolder();	
					
				Component305_val = new SkyEditor2__SkyEditorDummy__c();	
				Component305_op = new SkyEditor2.TextHolder();	
					
				Component17_val = new SkyEditor2__SkyEditorDummy__c();	
				Component17_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component17_op = new SkyEditor2.TextHolder();	
				valueOptions_TextManagement2_c_CloudStatus_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : TextManagement2__c.CloudStatus__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_TextManagement2_c_CloudStatus_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component15_val = new SkyEditor2__SkyEditorDummy__c();	
				Component15_op = new SkyEditor2.TextHolder();	
					
				Component213_val = new SkyEditor2__SkyEditorDummy__c();	
				Component213_op = new SkyEditor2.TextHolder();	
					
				Component295_val = new SkyEditor2__SkyEditorDummy__c();	
				Component295_op = new SkyEditor2.TextHolder();	
					
				Component293_val = new SkyEditor2__SkyEditorDummy__c();	
				Component293_op = new SkyEditor2.TextHolder();	
					
				Component244_val = new SkyEditor2__SkyEditorDummy__c();	
				Component244_op = new SkyEditor2.TextHolder();	
					
				Component246_val = new SkyEditor2__SkyEditorDummy__c();	
				Component246_op = new SkyEditor2.TextHolder();	
					
				queryMap.put(	
					'Component2',	
					new SkyEditor2.Query('TextManagement2__c')
						.addField('CloudStatus__c')
						.addFieldAsOutput('Item2_OrderProduct__c')
						.addFieldAsOutput('Name')
						.addFieldAsOutput('Product11__c')
						.addFieldAsOutput('ContractMonths__c')
						.addFieldAsOutput('ItemOrderNO__c')
						.addFieldAsOutput('Account1__c')
						.addField('SalesDeliveryDay__c')
						.addField('DeliveryDaySales1__c')
						.addField('DeliveryDay__c')
						.addField('Bill_Main__c')
						.addField('BillSet__c')
						.addField('SalesDeliveryCount__c')
						.addField('DeliveryCount__c')
						.addField('SalesPersonUnit1__c')
						.addField('SalesPerson10__c')
						.addField('SalesPercentage1__c')
						.addField('SalesPersonSubUnit__c')
						.addField('SalesPersonSub__c')
						.addField('SalesPercentage2__c')
						.addField('DeliveryPersonUnit__c')
						.addField('DeliveryPerson__c')
						.addField('RecordTypeId')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component296_from, 'SkyEditor2__Date__c', 'OrderItem_Bill_Main__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component296_to, 'SkyEditor2__Date__c', 'OrderItem_Bill_Main__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component296_isNull, 'SkyEditor2__Date__c', 'OrderItem_Bill_Main__c', Component296_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component298_from, 'SkyEditor2__Date__c', 'SalesDeliveryDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component298_to, 'SkyEditor2__Date__c', 'SalesDeliveryDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component298_isNull, 'SkyEditor2__Date__c', 'SalesDeliveryDay__c', Component298_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component251_from, 'SkyEditor2__Date__c', 'DeliveryDaySales1__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component251_to, 'SkyEditor2__Date__c', 'DeliveryDaySales1__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component251_isNull, 'SkyEditor2__Date__c', 'DeliveryDaySales1__c', Component251_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component253_from, 'SkyEditor2__Date__c', 'DeliveryDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component253_to, 'SkyEditor2__Date__c', 'DeliveryDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component253_isNull, 'SkyEditor2__Date__c', 'DeliveryDay__c', Component253_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component209_val, 'SkyEditor2__Text__c', 'Name', Component209_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component275_val, 'SkyEditor2__Text__c', 'OrderNo__c', Component275_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component198_val, 'SkyEditor2__Text__c', 'RecordTypeId', Component198_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component289_val, 'SkyEditor2__Text__c', 'SalesPersonUnit1__c', Component289_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component122_val, 'SalesPerson10__c', 'SalesPerson10__c', Component122_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component291_val, 'DeliveryPerson__c', 'DeliveryPerson__c', Component291_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component133_val, 'SkyEditor2__Text__c', 'Account1__c', Component133_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component280_val, 'SkyEditor2__Text__c', 'Product11__c', Component280_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component305_val, 'SkyEditor2__Text__c', 'ContractMonths__c', Component305_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component17_val_dummy, 'SkyEditor2__Text__c','CloudStatus__c', Component17_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component15_val, 'SkyEditor2__Text__c', 'Status__c', Component15_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component213_val, 'SkyEditor2__Text__c', 'Item2_DeliveryExistence__c', Component213_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component295_val, 'SkyEditor2__Text__c', 'AutomaticUpdate__c', Component295_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component293_val, 'SkyEditor2__Text__c', 'BillingTiming__c', Component293_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component244_val, 'SkyEditor2__Date__c', 'StartDate__c', Component244_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component246_val, 'SkyEditor2__Date__c', 'EndDate__c', Component246_op, true, 0, false ))
				);	
					
					Component2 = new Component2(new List<TextManagement2__c>(), new List<Component2Item>(), new List<TextManagement2__c>(), new SkyEditor2.RecordTypeSelector(TextManagement2__c.SObjectType), queryMap.get('Component2'));
				 Component2.setPageSize(100);
				listItemHolders.put('Component2', Component2);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(TextManagement2__c.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = false;
					Cookie c = ApexPages.currentPage().getCookies().get('QueryPagingConfirm');
					QueryPagingConfirmationSetting = c != null && c.getValue() == '0';
			execInitialSearch = false;
			presetSystemParams();
			Component2.extender = this.extender;
			initSearch();
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Name() { 
			return getOperatorOptions('TextManagement2__c', 'Name');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_OrderNo_c() { 
			return getOperatorOptions('TextManagement2__c', 'OrderNo__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_RecordTypeId() { 
			return getOperatorOptions('TextManagement2__c', 'RecordTypeId');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_SalesPersonUnit1_c() { 
			return getOperatorOptions('TextManagement2__c', 'SalesPersonUnit1__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_SalesPerson10_c() { 
			return getOperatorOptions('TextManagement2__c', 'SalesPerson10__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_DeliveryPerson_c() { 
			return getOperatorOptions('TextManagement2__c', 'DeliveryPerson__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Account1_c() { 
			return getOperatorOptions('TextManagement2__c', 'Account1__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Product11_c() { 
			return getOperatorOptions('TextManagement2__c', 'Product11__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_ContractMonths_c() { 
			return getOperatorOptions('TextManagement2__c', 'ContractMonths__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_CloudStatus_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Status_c() { 
			return getOperatorOptions('TextManagement2__c', 'Status__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Item2_DeliveryExistence_c() { 
			return getOperatorOptions('TextManagement2__c', 'Item2_DeliveryExistence__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_AutomaticUpdate_c() { 
			return getOperatorOptions('TextManagement2__c', 'AutomaticUpdate__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_BillingTiming_c() { 
			return getOperatorOptions('TextManagement2__c', 'BillingTiming__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_StartDate_c() { 
			return getOperatorOptions('TextManagement2__c', 'StartDate__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_EndDate_c() { 
			return getOperatorOptions('TextManagement2__c', 'EndDate__c');	
		}	
			
			
	global with sharing class Component2Item extends SkyEditor2.ListItem {
		public TextManagement2__c record{get; private set;}
		@TestVisible
		Component2Item(Component2 holder, TextManagement2__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component2 extends SkyEditor2.QueryPagingList {
		public List<Component2Item> items{get; private set;}
		@TestVisible
			Component2(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector, SkyEditor2.Query query) {
			super(records, items, deleteRecords, recordTypeSelector, query);
			this.items = (List<Component2Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component2Item(this, (TextManagement2__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
		public void doFirst(){first();}
		public void doPrevious(){previous();}
		public void doNext(){next();}
		public void doLast(){last();}
		public void doSort(){sort();}

	}

	public TextManagement2__c Component2_table_Conversion { get { return new TextManagement2__c();}}
	
	public String Component2_table_selectval { get; set; }
	
	
			
	}