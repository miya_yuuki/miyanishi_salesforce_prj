global with sharing class LEX_Opportunity extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public Opportunity record {get{return (Opportunity)mainRecord;}}
	public String recordTypeRecordsJSON_Opportunity {get; private set;}
	public String defaultRecordTypeId_Opportunity {get; private set;}
	public String metadataJSON_Opportunity {get; private set;}
	public String picklistValuesJSON_Opportunity_Products_c {get; private set;}
	public String picklistValuesJSON_Opportunity_ProposalProducts_c {get; private set;}
	public String Component5937_hidden { get; set; }
	public String Component6003_hidden { get; set; }
	{
	setApiVersion(42.0);
	}
	public LEX_Opportunity(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = Opportunity.fields.Name;
		f = Opportunity.fields.OpportunityCopy__c;
		f = Opportunity.fields.AccountId;
		f = Opportunity.fields.Opportunity_Type__c;
		f = Opportunity.fields.BoardDiscussed__c;
		f = Opportunity.fields.Opportunity_Partner__c;
		f = Opportunity.fields.Opportunity_endClient1__c;
		f = Opportunity.fields.Opportunity_Competition__c;
		f = Opportunity.fields.OwnerId;
		f = Opportunity.fields.MQLSalesPerson__c;
		f = Opportunity.fields.sub_sales_person__c;
		f = Opportunity.fields.settled_sales_person_unit__c;
		f = Opportunity.fields.MQLDate__c;
		f = Opportunity.fields.MQL_first_visit_date__c;
		f = Opportunity.fields.AppointmentDay__c;
		f = Opportunity.fields.first_visit_date__c;
		f = Opportunity.fields.recorded_date__c;
		f = Opportunity.fields.non_recorded_flg__c;
		f = Opportunity.fields.suggest_date__c;
		f = Opportunity.fields.Post_evaluation__c;
		f = Opportunity.fields.LeadEvaluation__c;
		f = Opportunity.fields.Opportunity_lead1__c;
		f = Opportunity.fields.Opportunity_lead2__c;
		f = Opportunity.fields.CampaignId;
		f = Opportunity.fields.not_campaign_flg__c;
		f = Opportunity.fields.AppointmentSalesPerson__c;
		f = Opportunity.fields.AppointmentType__c;
		f = Opportunity.fields.contents_order_status__c;
		f = Opportunity.fields.account_web_site__c;
		f = Opportunity.fields.account_web_site_unnecessary__c;
		f = Opportunity.fields.ServiceWEBSite__c;
		f = Opportunity.fields.WEBTitle__c;
		f = Opportunity.fields.Opportunity_WEBsiteRank__c;
		f = Opportunity.fields.WebSiteTypeView__c;
		f = Opportunity.fields.SEOMeasureStatus__c;
		f = Opportunity.fields.SEO_contents_budget_per_month__c;
		f = Opportunity.fields.Products__c;
		f = Opportunity.fields.writing_resource_summary__c;
		f = Opportunity.fields.contents_measure_status__c;
		f = Opportunity.fields.AmountTotal__c;
		f = Opportunity.fields.CreditDecisionPrice__c;
		f = Opportunity.fields.gross_profit_all__c;
		f = Opportunity.fields.CloseDate;
		f = Opportunity.fields.StageName;
		f = Opportunity.fields.this_month_approach__c;
		f = Opportunity.fields.agreement_probability__c;
		f = Opportunity.fields.karute__c;
		f = Opportunity.fields.karuteCheck__c;
		f = Opportunity.fields.karutenew__c;
		f = Opportunity.fields.suggestion_importance_summary__c;
		f = Opportunity.fields.suggestion_importance__c;
		f = Opportunity.fields.review_practitioner__c;
		f = Opportunity.fields.review_status__c;
		f = Opportunity.fields.roleplaying__c;
		f = Opportunity.fields.document__c;
		f = Opportunity.fields.document_creating_user__c;
		f = Opportunity.fields.documents_delivery_date__c;
		f = Opportunity.fields.Opportunity_Support__c;
		f = Opportunity.fields.lastTereapoDate__c;
		f = Opportunity.fields.Opportunity_deadDay__c;
		f = Opportunity.fields.lastTereapoMemo__c;
		f = Opportunity.fields.NA_memo__c;
		f = Opportunity.fields.Budget_acquisition__c;
		f = Opportunity.fields.ProposalProducts__c;
		f = Opportunity.fields.Opportunity_ActionDay__c;
		f = Opportunity.fields.Customer_level__c;
		f = Opportunity.fields.Competition_winner__c;
		f = Opportunity.fields.Opportunity_NoRecord__c;
		f = Opportunity.fields.Customer_needs_level__c;
		f = Opportunity.fields.Dead_type__c;
		f = Opportunity.fields.Recycle_User__c;
		f = Opportunity.fields.Introduction_start_timing__c;
		f = Opportunity.fields.Opportunity_DeadMemo__c;
		f = Opportunity.fields.Settled_type__c;
		f = Opportunity.fields.Opportunity_BillingMethod__c;
		f = Opportunity.fields.CaseApproval__c;
		f = Opportunity.fields.Opportunity_Title__c;
		f = Opportunity.fields.Opportunity_PaymentSiteMaster__c;
		f = Opportunity.fields.Opportunity_BillKana__c;
		f = Opportunity.fields.Opportunity_PaymentMethod__c;
		f = Opportunity.fields.AddressCheck__c;
		f = Opportunity.fields.Opportunity_Memo__c;
		f = Opportunity.fields.CustomerNamePRINT__c;
		f = Opportunity.fields.QuoteExpirationDate__c;
		f = Opportunity.fields.Opportunity_EstimateMemo__c;
		f = Opportunity.fields.OrderProvisional__c;
		f = Opportunity.fields.OrderProvisionalMemo__c;
		f = Opportunity.fields.SalvageScheduledDate__c;
		f = Opportunity.fields.opportunityBillMemo__c;
		f = Opportunity.fields.notices__c;
		f = Opportunity.fields.Agreement1__c;
		f = Opportunity.fields.Agreement2__c;
		f = Opportunity.fields.Agreement3__c;
		f = Opportunity.fields.Agreement4__c;
		f = Opportunity.fields.Agreement5__c;
		f = Opportunity.fields.Agreement6__c;
		f = Opportunity.fields.Agreement7__c;
		f = Opportunity.fields.Agreement8__c;
		f = Opportunity.fields.Agreement10__c;
		f = Opportunity.fields.Agreement11__c;
		f = Opportunity.fields.Agreement12__c;
		f = Opportunity.fields.Agreement17__c;
		f = Opportunity.fields.Agreement18__c;
		f = Opportunity.fields.Agreement19__c;
		f = Opportunity.fields.Agreement20__c;
		f = Opportunity.fields.Agreement13__c;
		f = Opportunity.fields.Agreement14__c;
		f = Opportunity.fields.Agreement15__c;
		f = Opportunity.fields.Agreement9__c;
		f = Opportunity.fields.Post_evaluation_summary__c;
		f = Opportunity.fields.SQLType__c;

		List<RecordTypeInfo> recordTypes;
		FilterMetadataResult filterResult;
		List<RecordType> recordTypeRecords_Opportunity = [SELECT Id, DeveloperName, NamespacePrefix FROM RecordType WHERE SobjectType = 'Opportunity'];
		Map<Id, RecordType> recordTypeMap_Opportunity = new Map<Id, RecordType>(recordTypeRecords_Opportunity);
		List<RecordType> availableRecordTypes_Opportunity = new List<RecordType>();
		recordTypes = SObjectType.Opportunity.getRecordTypeInfos();

		for (RecordTypeInfo t: recordTypes) {
			if (t.isDefaultRecordTypeMapping()) {
				defaultRecordTypeId_Opportunity = t.getRecordTypeId();
			}
			if (t.isAvailable()) {
				RecordType rtype = recordTypeMap_Opportunity.get(t.getRecordTypeId());
				if (rtype != null) {
					availableRecordTypes_Opportunity.add(rtype);
				}
			}
		}
		recordTypeRecordsJSON_Opportunity = System.JSON.serialize(availableRecordTypes_Opportunity);
		filterResult = filterMetadataJSON(
			System.JSON.deserializeUntyped('{"CustomObject":{"recordTypes":[{"fullName":"AP","picklistValues":[{"picklist":"Products__c","values":[{"fullName":"コンサル","default":false},{"fullName":"コンテンツ","default":false},{"fullName":"リンク","default":false},{"fullName":"不明","default":false}]},{"picklist":"ProposalProducts__c","values":[{"fullName":"なし","default":false},{"fullName":"コンサル","default":false},{"fullName":"コンテンツ","default":false},{"fullName":"リンク","default":false},{"fullName":"不明","default":false}]}]},{"fullName":"ContractChange","picklistValues":[{"picklist":"Products__c","values":[{"fullName":"コンサル","default":false},{"fullName":"コンテンツ","default":false},{"fullName":"リンク","default":false},{"fullName":"不明","default":false},{"fullName":"施策無し","default":false}]},{"picklist":"ProposalProducts__c","values":[{"fullName":"なし","default":false},{"fullName":"コンサル","default":false},{"fullName":"コンテンツ","default":false},{"fullName":"リンク","default":false},{"fullName":"不明","default":false}]}]},{"fullName":"ExistingCustomers","picklistValues":[{"picklist":"Products__c","values":[{"fullName":"コンサル","default":false},{"fullName":"コンテンツ","default":false},{"fullName":"リンク","default":false},{"fullName":"不明","default":false}]},{"picklist":"ProposalProducts__c","values":[{"fullName":"なし","default":false},{"fullName":"コンサル","default":false},{"fullName":"コンテンツ","default":false},{"fullName":"リンク","default":false},{"fullName":"不明","default":false}]}]},{"fullName":"IntroductionFee","picklistValues":[]},{"fullName":"LifeMedia","picklistValues":[]},{"fullName":"old","picklistValues":[]},{"fullName":"syanaiRD","picklistValues":[]}]}}'),
			recordTypeFullNames(availableRecordTypes_Opportunity),
			Opportunity.SObjectType
		);
		metadataJSON_Opportunity = System.JSON.serialize(filterResult.data);
		picklistValuesJSON_Opportunity_Products_c = System.JSON.serialize(filterPricklistEntries(Opportunity.SObjectType.Products__c.getDescribe(), filterResult));
		picklistValuesJSON_Opportunity_ProposalProducts_c = System.JSON.serialize(filterPricklistEntries(Opportunity.SObjectType.ProposalProducts__c.getDescribe(), filterResult));
		try {
			mainSObjectType = Opportunity.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'LEX_Opportunity';
			mainQuery = new SkyEditor2.Query('Opportunity');
			mainQuery.addField('Name');
			mainQuery.addField('OpportunityCopy__c');
			mainQuery.addField('AccountId');
			mainQuery.addField('Opportunity_Type__c');
			mainQuery.addField('BoardDiscussed__c');
			mainQuery.addField('Opportunity_Partner__c');
			mainQuery.addField('Opportunity_endClient1__c');
			mainQuery.addField('Opportunity_Competition__c');
			mainQuery.addField('OwnerId');
			mainQuery.addField('MQLSalesPerson__c');
			mainQuery.addField('sub_sales_person__c');
			mainQuery.addField('MQLDate__c');
			mainQuery.addField('MQL_first_visit_date__c');
			mainQuery.addField('AppointmentDay__c');
			mainQuery.addField('first_visit_date__c');
			mainQuery.addField('recorded_date__c');
			mainQuery.addField('non_recorded_flg__c');
			mainQuery.addField('suggest_date__c');
			mainQuery.addField('Post_evaluation__c');
			mainQuery.addField('LeadEvaluation__c');
			mainQuery.addField('Opportunity_lead1__c');
			mainQuery.addField('Opportunity_lead2__c');
			mainQuery.addField('CampaignId');
			mainQuery.addField('not_campaign_flg__c');
			mainQuery.addField('AppointmentSalesPerson__c');
			mainQuery.addField('AppointmentType__c');
			mainQuery.addField('contents_order_status__c');
			mainQuery.addField('account_web_site__c');
			mainQuery.addField('account_web_site_unnecessary__c');
			mainQuery.addField('ServiceWEBSite__c');
			mainQuery.addField('WEBTitle__c');
			mainQuery.addField('Opportunity_WEBsiteRank__c');
			mainQuery.addField('SEOMeasureStatus__c');
			mainQuery.addField('SEO_contents_budget_per_month__c');
			mainQuery.addField('Products__c');
			mainQuery.addField('writing_resource_summary__c');
			mainQuery.addField('contents_measure_status__c');
			mainQuery.addField('CloseDate');
			mainQuery.addField('StageName');
			mainQuery.addField('this_month_approach__c');
			mainQuery.addField('agreement_probability__c');
			mainQuery.addField('karute__c');
			mainQuery.addField('karuteCheck__c');
			mainQuery.addField('suggestion_importance_summary__c');
			mainQuery.addField('suggestion_importance__c');
			mainQuery.addField('review_practitioner__c');
			mainQuery.addField('review_status__c');
			mainQuery.addField('roleplaying__c');
			mainQuery.addField('document__c');
			mainQuery.addField('document_creating_user__c');
			mainQuery.addField('documents_delivery_date__c');
			mainQuery.addField('Opportunity_Support__c');
			mainQuery.addField('Opportunity_deadDay__c');
			mainQuery.addField('NA_memo__c');
			mainQuery.addField('Budget_acquisition__c');
			mainQuery.addField('ProposalProducts__c');
			mainQuery.addField('Opportunity_ActionDay__c');
			mainQuery.addField('Customer_level__c');
			mainQuery.addField('Competition_winner__c');
			mainQuery.addField('Opportunity_NoRecord__c');
			mainQuery.addField('Customer_needs_level__c');
			mainQuery.addField('Dead_type__c');
			mainQuery.addField('Introduction_start_timing__c');
			mainQuery.addField('Opportunity_DeadMemo__c');
			mainQuery.addField('Settled_type__c');
			mainQuery.addField('Opportunity_BillingMethod__c');
			mainQuery.addField('CaseApproval__c');
			mainQuery.addField('Opportunity_Title__c');
			mainQuery.addField('Opportunity_PaymentSiteMaster__c');
			mainQuery.addField('Opportunity_BillKana__c');
			mainQuery.addField('Opportunity_PaymentMethod__c');
			mainQuery.addField('AddressCheck__c');
			mainQuery.addField('Opportunity_Memo__c');
			mainQuery.addField('CustomerNamePRINT__c');
			mainQuery.addField('QuoteExpirationDate__c');
			mainQuery.addField('Opportunity_EstimateMemo__c');
			mainQuery.addField('OrderProvisional__c');
			mainQuery.addField('OrderProvisionalMemo__c');
			mainQuery.addField('SalvageScheduledDate__c');
			mainQuery.addField('opportunityBillMemo__c');
			mainQuery.addField('notices__c');
			mainQuery.addField('Agreement1__c');
			mainQuery.addField('Agreement2__c');
			mainQuery.addField('Agreement3__c');
			mainQuery.addField('Agreement4__c');
			mainQuery.addField('Agreement5__c');
			mainQuery.addField('Agreement6__c');
			mainQuery.addField('Agreement7__c');
			mainQuery.addField('Agreement8__c');
			mainQuery.addField('Agreement10__c');
			mainQuery.addField('Agreement11__c');
			mainQuery.addField('Agreement12__c');
			mainQuery.addField('Agreement17__c');
			mainQuery.addField('Agreement18__c');
			mainQuery.addField('Agreement19__c');
			mainQuery.addField('Agreement20__c');
			mainQuery.addField('Agreement13__c');
			mainQuery.addField('Agreement14__c');
			mainQuery.addField('Agreement15__c');
			mainQuery.addField('Agreement9__c');
			mainQuery.addField('Post_evaluation_summary__c');
			mainQuery.addField('SQLType__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('settled_sales_person_unit__c');
			mainQuery.addFieldAsOutput('WebSiteTypeView__c');
			mainQuery.addFieldAsOutput('AmountTotal__c');
			mainQuery.addFieldAsOutput('CreditDecisionPrice__c');
			mainQuery.addFieldAsOutput('gross_profit_all__c');
			mainQuery.addFieldAsOutput('karutenew__c');
			mainQuery.addFieldAsOutput('lastTereapoDate__c');
			mainQuery.addFieldAsOutput('lastTereapoMemo__c');
			mainQuery.addFieldAsOutput('Recycle_User__c');
			mainQuery.addFieldAsOutput('LeadSource');
			mainQuery.addFieldAsOutput('Id');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('OpportunityCopy__c', 'OpportunityCopy__c');
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			if (record.Id == null) {
				saveOldValues();
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	public String getComponent5937OptionsJS() {
		return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
		Opportunity.getSObjectType(),
		SObjectType.Opportunity.fields.Products__c.getSObjectField()
		));
		}
	public String getComponent6003OptionsJS() {
		return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
		Opportunity.getSObjectType(),
		SObjectType.Opportunity.fields.ProposalProducts__c.getSObjectField()
		));
		}
	@TestVisible		static Set<String> recordTypeFullNames(RecordType[] records) {
		Set<String> result = new Set<String>();
		for (RecordType r : records) {
			result.add(r.DeveloperName);
			if (r.NamespacePrefix != null) {
				result.add(r.NamespacePrefix + '__' + r.DeveloperName);
			}
		}
		return result;
	}
	
	@TestVisible		static FilterMetadataResult filterMetadataJSON(Object metadata, Set<String> recordTypeFullNames, SObjectType soType) {
		Map<String, Object> metadataMap = (Map<String, Object>) metadata;
		Map<String, Object> customObject = (Map<String, Object>) metadataMap.get('CustomObject');
		List<Object> recordTypes = (List<Object>) customObject.get('recordTypes');
		Map<String, Set<String>> availableEntries = new Map<String, Set<String>>();
		for (Integer i = recordTypes.size() - 1; i >= 0; i--) {
			Map<String, Object> recordType = (Map<String, Object>)recordTypes[i];
			String fullName = (String)recordType.get('fullName');
			if (! recordTypeFullNames.contains(fullName)) {
				recordTypes.remove(i);
			} else {
				addAll(availableEntries, getOutEntries(recordType, soType));
			}
		}	
		return new FilterMetadataResult(metadataMap, availableEntries, recordTypes.size() == 0);
	}
	public class FilterMetadataResult {
		public Map<String, Object> data {get; private set;}
		public Map<String, Set<String>> availableEntries {get; private set;}
		public Boolean master {get; private set;}
		public FilterMetadataResult(Map<String, Object> data, Map<String, Set<String>> availableEntries, Boolean master) {
			this.data = data;
			this.availableEntries = availableEntries;
			this.master = master;
		}
	}
	
	static void addAll(Map<String, Set<String>> toMap, Map<String, Set<String>> fromMap) {
		for (String key : fromMap.keySet()) {
			Set<String> fromSet = fromMap.get(key);
			Set<String> toSet = toMap.get(key);
			if (toSet == null) {
				toSet = new Set<String>();
				toMap.put(key, toSet);
			}
			toSet.addAll(fromSet);
		}
	}

	static Map<String, Set<String>> getOutEntries(Map<String, Object> recordType, SObjectType soType) {
		Map<String, Set<String>> result = new Map<String, Set<String>>();
		List<Object> entries = (List<Object>)recordType.get('picklistValues');
		Map<String, SObjectField> fields = soType.getDescribe().fields.getMap();
		for (Object e : entries) {
			Map<String, Object> entry = (Map<String, Object>) e;
			String picklist = (String) entry.get('picklist');
			SObjectField f = fields.get(picklist);
			List<Object> values = (List<Object>)(entry.get('values'));
			if (f != null && f.getDescribe().isAccessible()) {
				Set<String> entrySet = new Set<String>();
				for (Object v : values) {
					Map<String, Object> value = (Map<String, Object>) v;
					entrySet.add(EncodingUtil.urlDecode((String)value.get('fullName'), 'utf-8'));
				}
				result.put(picklist, entrySet);
			} else { 
				values.clear(); 
			}
		}
		return result;
	}
	
	static List<PicklistEntry> filterPricklistEntries(DescribeFieldResult f, FilterMetadataResult parseResult) {
		List<PicklistEntry> all = f.getPicklistValues();
		if (parseResult.master) {
			return all;
		}
		Set<String> availables = parseResult.availableEntries.get(f.getName());
		List<PicklistEntry> result = new List<PicklistEntry>();
		if(availables == null) return result;
		for (PicklistEntry e : all) {
			if (e.isActive() && availables.contains(e.getValue())) {
				result.add(e);
			}
		}
		return result;
	}
	
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}