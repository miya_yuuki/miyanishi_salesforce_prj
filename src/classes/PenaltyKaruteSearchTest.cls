@isTest
private with sharing class PenaltyKaruteSearchTest{
		private static testMethod void testPageMethods() {	
			PenaltyKaruteSearch page = new PenaltyKaruteSearch(new ApexPages.StandardController(new karute__c()));	
			page.getOperatorOptions_karute_c_PenaltyStatusCS_c_multi();	
			page.getOperatorOptions_karute_c_penaltyStatusRD_c_multi();	
			page.getOperatorOptions_Account_Name();	
			page.getOperatorOptions_karute_c_URL_c();	
			page.getOperatorOptions_karute_c_SalesPersonOwner_c();	
			System.assert(true);
		}	
			
	private static testMethod void testComponent3() {
		PenaltyKaruteSearch.Component3 Component3 = new PenaltyKaruteSearch.Component3(new List<karute__c>(), new List<PenaltyKaruteSearch.Component3Item>(), new List<karute__c>(), null);
		Component3.create(new karute__c());
		Component3.doDeleteSelectedItems();
		System.assert(true);
	}
	
}