global with sharing class OutsourcingLookup extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public teamspirit__AtkApply__c record{get;set;}	
			
	
		public Component3 Component3 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public teamspirit__AtkApply__c Component8_val {get;set;}	
		public SkyEditor2.TextHolder Component8_op{get;set;}	
			
		public teamspirit__AtkApply__c Component10_val {get;set;}	
		public SkyEditor2.TextHolder Component10_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component27_val {get;set;}	
		public SkyEditor2.TextHolder Component27_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component32_val {get;set;}	
		public SkyEditor2.TextHolder Component32_op{get;set;}	
			
	public String recordTypeRecordsJSON_teamspirit_AtkApply_c {get; private set;}
	public String defaultRecordTypeId_teamspirit_AtkApply_c {get; private set;}
	public String metadataJSON_teamspirit_AtkApply_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public OutsourcingLookup(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = teamspirit__AtkApply__c.fields.OrderProduct__c;
		f = teamspirit__AtkApply__c.fields.OwnerId;
		f = teamspirit__AtkApply__c.fields.shinsheino__c;
		f = teamspirit__AtkApply__c.fields.Name;
		f = teamspirit__AtkApply__c.fields.ProductGroup__c;
		f = teamspirit__AtkApply__c.fields.CreatedDate;
		f = teamspirit__AtkApply__c.fields.RecordTypeId;
		f = teamspirit__AtkApply__c.fields.teamspirit__Status__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = teamspirit__AtkApply__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				teamspirit__AtkApply__c lookupObjComponent24 = new teamspirit__AtkApply__c();	
				Component8_val = lookupObjComponent24;	
				Component8_op = new SkyEditor2.TextHolder();	
					
				Component10_val = lookupObjComponent24;	
				Component10_op = new SkyEditor2.TextHolder();	
					
				Component27_val = new SkyEditor2__SkyEditorDummy__c();	
				Component27_op = new SkyEditor2.TextHolder();	
					
				Component32_val = new SkyEditor2__SkyEditorDummy__c();	
				Component32_op = new SkyEditor2.TextHolder();	
					
				queryMap.put(	
					'Component3',	
					new SkyEditor2.Query('teamspirit__AtkApply__c')
						.addFieldAsOutput('Name')
						.addFieldAsOutput('OrderProduct__c')
						.addFieldAsOutput('ProductGroup__c')
						.addFieldAsOutput('OwnerId')
						.addFieldAsOutput('CreatedDate')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component8_val, 'OrderProduct__c', 'OrderProduct__c', Component8_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component10_val, 'OwnerId', 'OwnerId', Component10_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component27_val, 'SkyEditor2__Text__c', 'shinsheino__c', Component27_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component32_val, 'SkyEditor2__Text__c', 'Name', Component32_op, true, 0, false ))
						.addWhere(' ( RecordType.Name like \'%外注申請%\' AND teamspirit__Status__c not in (\'取消済み\' )  AND OrderProduct__r.Name != \'\')')
				);	
					
					Component3 = new Component3(new List<teamspirit__AtkApply__c>(), new List<Component3Item>(), new List<teamspirit__AtkApply__c>(), null);
				Component3.ignoredOnSave = true;
				listItemHolders.put('Component3', Component3);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(teamspirit__AtkApply__c.SObjectType, true);
					
					
			p_showHeader = false;
			p_sidebar = false;
			presetSystemParams();
			Component3.extender = this.extender;
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_teamspirit_AtkApply_c_OrderProduct_c() { 
			return getOperatorOptions('teamspirit__AtkApply__c', 'OrderProduct__c');	
		}	
		public List<SelectOption> getOperatorOptions_teamspirit_AtkApply_c_OwnerId() { 
			return getOperatorOptions('teamspirit__AtkApply__c', 'OwnerId');	
		}	
		public List<SelectOption> getOperatorOptions_teamspirit_AtkApply_c_shinsheino_c() { 
			return getOperatorOptions('teamspirit__AtkApply__c', 'shinsheino__c');	
		}	
		public List<SelectOption> getOperatorOptions_teamspirit_AtkApply_c_Name() { 
			return getOperatorOptions('teamspirit__AtkApply__c', 'Name');	
		}	
			
			
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public teamspirit__AtkApply__c record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, teamspirit__AtkApply__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.ListItemHolder {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (teamspirit__AtkApply__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

			
	}