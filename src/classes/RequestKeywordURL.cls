global with sharing class RequestKeywordURL extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public CustomObject1__c record {get{return (CustomObject1__c)mainRecord;}}
	public Component294 Component294 {get; private set;}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	{
	setApiVersion(42.0);
	}
	public RequestKeywordURL(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = CustomObject1__c.fields.Name;
		f = CustomObject1__c.fields.Status__c;
		f = CustomObject1__c.fields.unit__c;
		f = CustomObject1__c.fields.RequestDay__c;
		f = CustomObject1__c.fields.OwnerId;
		f = CustomObject1__c.fields.RequestEndDay__c;
		f = CustomObject1__c.fields.SalesSupportPerson__c;
		f = CustomObject1__c.fields.OrderNo__c;
		f = CustomObject1__c.fields.SupportDay__c;
		f = CustomObject1__c.fields.Account__c;
		f = CustomObject1__c.fields.SupportMember__c;
		f = CustomObject1__c.fields.Sign__c;
		f = CustomObject1__c.fields.RDDay__c;
		f = CustomObject1__c.fields.OrderProvisional1__c;
		f = CustomObject1__c.fields.RecordTypeId;
		f = CustomObject1__c.fields.OrderProvisionalMemo1__c;
		f = CustomObject1__c.fields.SalvageScheduledDate__c;
		f = CustomObject1__c.fields.ContractChangeDay__c;
		f = CustomObject1__c.fields.comment__c;
		f = CustomObject1__c.fields.RequestMemo__c;
		f = CustomObject1__c.fields.sentaku__c;
		f = CustomObject1__c.fields.henkougo__c;
		f = SEOChange__c.fields.OrderItem__c;
		f = SEOChange__c.fields.Type__c;
		f = SEOChange__c.fields.KeywordType__c;
		f = SEOChange__c.fields.Keyword__c;
		f = SEOChange__c.fields.URL__c;
		f = SEOChange__c.fields.level__c;
		f = SEOChange__c.fields.Amount__c;
		f = SEOChange__c.fields.sinsei__c;
		f = SEOChange__c.fields.KeywordType2__c;
		f = SEOChange__c.fields.Keyword2__c;
		f = SEOChange__c.fields.URL2__c;
		f = SEOChange__c.fields.level2__c;
		f = SEOChange__c.fields.Amount2__c;
		f = CustomObject1__c.fields.SupportJudgePerson__c;
		f = Order__c.fields.AccountName__c;
		f = OrderItem__c.fields.OrderItem_keywordType__c;
		f = OrderItem__c.fields.Item2_Keyword_strategy_keyword__c;
		f = OrderItem__c.fields.OrderItem_URL__c;
		f = OrderItem__c.fields.Item2_Estimate_level__c;
		f = OrderItem__c.fields.TotalPrice__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = CustomObject1__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'RequestKeywordURL';
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(CustomObject1__c.SObjectType);
			mainQuery = new SkyEditor2.Query('CustomObject1__c');
			mainQuery.addField('Status__c');
			mainQuery.addField('OwnerId');
			mainQuery.addField('SalesSupportPerson__c');
			mainQuery.addField('OrderNo__c');
			mainQuery.addField('Account__c');
			mainQuery.addField('Sign__c');
			mainQuery.addField('OrderProvisional1__c');
			mainQuery.addField('OrderProvisionalMemo1__c');
			mainQuery.addField('SalvageScheduledDate__c');
			mainQuery.addField('ContractChangeDay__c');
			mainQuery.addField('comment__c');
			mainQuery.addField('RequestMemo__c');
			mainQuery.addField('sentaku__c');
			mainQuery.addField('henkougo__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('unit__c');
			mainQuery.addFieldAsOutput('RequestDay__c');
			mainQuery.addFieldAsOutput('RequestEndDay__c');
			mainQuery.addFieldAsOutput('SupportDay__c');
			mainQuery.addFieldAsOutput('SupportMember__c');
			mainQuery.addFieldAsOutput('RDDay__c');
			mainQuery.addFieldAsOutput('RecordType.Name');
			mainQuery.addFieldAsOutput('SupportJudgePerson__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			Component294 = new Component294(new List<SEOChange__c>(), new List<Component294Item>(), new List<SEOChange__c>(), null);
			listItemHolders.put('Component294', Component294);
			query = new SkyEditor2.Query('SEOChange__c');
			query.addField('OrderItem__c');
			query.addField('Type__c');
			query.addField('KeywordType2__c');
			query.addField('Keyword2__c');
			query.addField('URL2__c');
			query.addField('level2__c');
			query.addField('Amount2__c');
			query.addFieldAsOutput('KeywordType__c');
			query.addFieldAsOutput('Keyword__c');
			query.addFieldAsOutput('URL__c');
			query.addFieldAsOutput('level__c');
			query.addFieldAsOutput('Amount__c');
			query.addFieldAsOutput('sinsei__c');
			query.addFieldAsOutput('RecordTypeId');
			query.addWhere('sinsei__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('Component294', 'sinsei__c');
			Component294.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('Component294', query);
			registRelatedList('sinsei__r', 'Component294');
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			Component294.extender = this.extender;
			if (record.Id == null) {
				saveOldValues();
				if(record.RecordTypeId == null) recordTypeSelector.applyDefault(record);
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class Component294Item extends SkyEditor2.ListItem {
		public SEOChange__c record{get; private set;}
		@TestVisible
		Component294Item(Component294 holder, SEOChange__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}

	public void loadReferenceValues_Component351() {
		if (record.OrderItem__c == null) {
if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.KeywordType__c)) {
				record.KeywordType__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.Keyword__c)) {
				record.Keyword__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.URL__c)) {
				record.URL__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.level__c)) {
				record.level__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.Amount__c)) {
				record.Amount__c = null;
			}
				return;
		}
		OrderItem__c[] referenceTo = [SELECT OrderItem_keywordType__c,Item2_Keyword_strategy_keyword__c,OrderItem_URL__c,Item2_Estimate_level__c,TotalPrice__c FROM OrderItem__c WHERE Id=:record.OrderItem__c];
		if (referenceTo.size() == 0) {
			record.OrderItem__c.addError(SkyEditor2.Messages.referenceDataNotFound(record.OrderItem__c));
			return;
		}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.KeywordType__c)) {
			record.KeywordType__c = referenceTo[0].OrderItem_keywordType__c;
		}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.Keyword__c)) {
			record.Keyword__c = referenceTo[0].Item2_Keyword_strategy_keyword__c;
		}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.URL__c)) {
			record.URL__c = referenceTo[0].OrderItem_URL__c;
		}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.level__c)) {
			record.level__c = referenceTo[0].Item2_Estimate_level__c;
		}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.Amount__c)) {
			record.Amount__c = referenceTo[0].TotalPrice__c;
		}
		
	}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component294 extends SkyEditor2.ListItemHolder {
		public List<Component294Item> items{get; private set;}
		@TestVisible
			Component294(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component294Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component294Item(this, (SEOChange__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
		global override void importByJSON() {
			List<Object> data = (List<Object>) System.JSON.deserializeUntyped(hiddenValue);
			super.importByJSON(data);
			for (Integer n = items.size() - data.size(); n < items.size(); n++) {
				Component294Item i = items[n];
				if (i.record.OrderItem__c != null) {
					i.loadReferenceValues_Component351();
				}
			}
		}
	}

	public void loadReferenceValues_Component201() {
		if (record.OrderNo__c == null) {
if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.Account__c)) {
				record.Account__c = null;
			}
				return;
		}
		Order__c[] referenceTo = [SELECT AccountName__c FROM Order__c WHERE Id=:record.OrderNo__c];
		if (referenceTo.size() == 0) {
			record.OrderNo__c.addError(SkyEditor2.Messages.referenceDataNotFound(record.OrderNo__c));
			return;
		}
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.Account__c)) {
			record.Account__c = referenceTo[0].AccountName__c;
		}
		
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}