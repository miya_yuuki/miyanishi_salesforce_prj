public with sharing class InvoiceFlagCheck {
    public static boolean firstRun = true;

    public static void prcInvoiceSalesCheck(List<Bill__c> objBill, Map<Id, Bill__c> oldMap, Boolean upd) {
        // 請求ID
        List<String> strBillIds = new List<String>();
        // 請求オブジェクト
        List<Bill__c> updBill = new List<Bill__c>();
        
        // 更新対象のチェック
        for (Bill__c obj : objBill) {
            if (prcCheckSalesFlag(upd, obj, oldMap) == true) {
                strBillIds.add(obj.Id);
            }
        }
        
        if (strBillIds.size() > 0) {
            updBill = [SELECT Id FROM Bill__c WHERE Id IN :strBillIds];
            for (Bill__c objNew : updBill) {
                objNew.SalesCheck__c = Datetime.now();
            }
            if (updBill.size() > 0) {
                update updBill;
            }
        }
    }
    
    public static void prcInvoiceIssuedCheck(List<Bill__c> objBill, Map<Id, Bill__c> oldMap, Boolean upd) {
        // 請求ID
        List<String> strBillIds = new List<String>();
        // 請求オブジェクト
        List<Bill__c> updBill = new List<Bill__c>();
        
        // 更新対象のチェック
        for (Bill__c obj : objBill) {
            if (prcCheckIssuedFlag(upd, obj, oldMap) == true) {
                strBillIds.add(obj.Id);
            }
        }
        
        if (strBillIds.size() > 0) {
            updBill = [SELECT Id FROM Bill__c WHERE Id IN :strBillIds];
            for (Bill__c objNew : updBill) {
                objNew.BillOutputDay__c = Datetime.now();
            }
            if (updBill.size() > 0) {
                update updBill;
            }
        }
    }
    
    public static void prcInvoiceSalesCheckRemoved(List<Bill__c> objBill, Map<Id, Bill__c> oldMap, Boolean upd) {
        // 請求ID
        List<String> strBillIds = new List<String>();
        // 請求オブジェクト
        List<Bill__c> updBill = new List<Bill__c>();
        
        // 更新対象のチェック
        for (Bill__c obj : objBill) {
            if (prcCheckSalesFlagRemoved(upd, obj, oldMap) == true) {
                strBillIds.add(obj.Id);
            }
        }
        
        if (strBillIds.size() > 0) {
            updBill = [SELECT Id FROM Bill__c WHERE Id IN :strBillIds];
            for (Bill__c objNew : updBill) {
                objNew.BillCheckMailAlertFlag__c = true;
            }
            if (updBill.size() > 0) {
                update updBill;
            }
        }
    }
    
    public static void prcInvoiceLastCheck(List<Bill__c> objBill, Map<Id, Bill__c> oldMap, Boolean upd) {
        // 請求ID
        List<String> strBillIds = new List<String>();
        // 請求オブジェクト
        List<Bill__c> updBill = new List<Bill__c>();
        
        // 更新対象のチェック
        for (Bill__c obj : objBill) {
            if (prcCheckDoubleFlag(upd, obj, oldMap) == true) {
                strBillIds.add(obj.Id);
            }
        }
        
        if (strBillIds.size() > 0) {
            updBill = [SELECT Id, Key__c, BillOutputDay__c FROM Bill__c WHERE Id IN :strBillIds];
            for (Bill__c objNew : updBill) {
                String strDt = objNew.BillOutputDay__c.format('yyyy-MM-dd');
                objNew.Key__c = objNew.Key__c + '_' + strDt;
            }
            if (updBill.size() > 0) {
                update updBill;
            }
        }
    }
    
    private static Boolean prcCheckSalesFlag (Boolean upd, Bill__c objNew, Map<Id, Bill__c> oldMap) {
        if (upd) {
            // 古い値を取得
            Bill__c objOld = oldMap.get(objNew.Id);
            // 営業確認チェックが入れられた
            if (objNew.SalesCheck1__c == true && objOld.SalesCheck1__c != true) {
                return true;
            }
            
        }
        // 条件に合致しない
        return null;
    }
    
    private static Boolean prcCheckIssuedFlag (Boolean upd, Bill__c objNew, Map<Id, Bill__c> oldMap) {
        if (upd) {
            // 古い値を取得
            Bill__c objOld = oldMap.get(objNew.Id);
            // 請求書発行済みチェックが入れられた
            if (objNew.Bill_BillCheck__c == true && objOld.Bill_BillCheck__c != true) {
                return true;
            }
            
        }
        // 条件に合致しない
        return null;
    }
    
    private static Boolean prcCheckSalesFlagRemoved (Boolean upd, Bill__c objNew, Map<Id, Bill__c> oldMap) {
        if (upd) {
            // 古い値を取得
            Bill__c objOld = oldMap.get(objNew.Id);
            // 請求書発行済みの状態で営業確認チェックが外された
            if (objNew.Bill_BillCheck__c == true && objOld.SalesCheck1__c == true && objNew.SalesCheck1__c != true) {
                return true;
            }
            
        }
        // 条件に合致しない
        return null;
    }
    
    private static Boolean prcCheckDoubleFlag (Boolean upd, Bill__c objNew, Map<Id, Bill__c> oldMap) {
        if (upd) {
            // 古い値を取得
            Bill__c objOld = oldMap.get(objNew.Id);
            // 営業確認チェック・請求書発行済みチェック どちらも入っている
            if (objNew.SalesCheck1__c == true && objNew.Bill_BillCheck__c == true && (objOld.SalesCheck1__c != true || objOld.Bill_BillCheck__c != true)) {
                return true;
            }
            
        }
        // 条件に合致しない
        return null;
    }
}