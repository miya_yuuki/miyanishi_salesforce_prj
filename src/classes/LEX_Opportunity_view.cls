global with sharing class LEX_Opportunity_view extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public Opportunity record {get{return (Opportunity)mainRecord;}}
	{
	setApiVersion(42.0);
	}
	public LEX_Opportunity_view(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = Opportunity.fields.Name;
		f = Opportunity.fields.OpportunityCopy__c;
		f = Opportunity.fields.AccountId;
		f = Opportunity.fields.Opportunity_Type__c;
		f = Opportunity.fields.Opportunity_Partner__c;
		f = Opportunity.fields.Opportunity_endClient1__c;
		f = Opportunity.fields.Opportunity_Competition__c;
		f = Opportunity.fields.Opportunity_SalesPerson_Unit__c;
		f = Opportunity.fields.MQLSalesPerson__c;
		f = Opportunity.fields.sub_sales_person__c;
		f = Opportunity.fields.settled_sales_person_unit__c;
		f = Opportunity.fields.MQLDate__c;
		f = Opportunity.fields.MQL_first_visit_date__c;
		f = Opportunity.fields.AppointmentDay__c;
		f = Opportunity.fields.first_visit_date__c;
		f = Opportunity.fields.recorded_date__c;
		f = Opportunity.fields.suggest_date__c;
		f = Opportunity.fields.Post_evaluation__c;
		f = Opportunity.fields.LeadEvaluation__c;
		f = Opportunity.fields.Opportunity_lead1__c;
		f = Opportunity.fields.Opportunity_lead2__c;
		f = Opportunity.fields.CampaignId;
		f = Opportunity.fields.AppointmentSalesPerson__c;
		f = Opportunity.fields.AppointmentType__c;
		f = Opportunity.fields.SQLType__c;
		f = Opportunity.fields.contents_order_status__c;
		f = Opportunity.fields.account_web_site__c;
		f = Opportunity.fields.account_web_site_unnecessary__c;
		f = Opportunity.fields.ServiceWEBSite__c;
		f = Opportunity.fields.WEBTitle__c;
		f = Opportunity.fields.Opportunity_WEBsiteRank__c;
		f = Opportunity.fields.WebSiteType__c;
		f = Opportunity.fields.SEOMeasureStatus__c;
		f = Opportunity.fields.SEO_contents_budget_per_month__c;
		f = Opportunity.fields.Products__c;
		f = Opportunity.fields.writing_resource_summary__c;
		f = Opportunity.fields.contents_measure_status__c;
		f = Opportunity.fields.AmountTotal__c;
		f = Opportunity.fields.CreditDecisionPrice__c;
		f = Opportunity.fields.gross_profit_all__c;
		f = Opportunity.fields.CloseDate;
		f = Opportunity.fields.StageName;
		f = Opportunity.fields.this_month_approach__c;
		f = Opportunity.fields.agreement_probability__c;
		f = Opportunity.fields.karute__c;
		f = Opportunity.fields.suggestion_importance_summary__c;
		f = Opportunity.fields.suggestion_importance__c;
		f = Opportunity.fields.review_practitioner__c;
		f = Opportunity.fields.review_status__c;
		f = Opportunity.fields.document__c;
		f = Opportunity.fields.document_creating_user__c;
		f = Opportunity.fields.documents_delivery_date__c;
		f = Opportunity.fields.Opportunity_Support__c;
		f = Opportunity.fields.lastTereapoDate__c;
		f = Opportunity.fields.Opportunity_deadDay__c;
		f = Opportunity.fields.lastTereapoMemo__c;
		f = Opportunity.fields.NA_memo__c;
		f = Opportunity.fields.Budget_acquisition__c;
		f = Opportunity.fields.ProposalProducts__c;
		f = Opportunity.fields.Opportunity_ActionDay__c;
		f = Opportunity.fields.Customer_level__c;
		f = Opportunity.fields.Competition_winner__c;
		f = Opportunity.fields.Customer_needs_level__c;
		f = Opportunity.fields.Dead_type__c;
		f = Opportunity.fields.Recycle_User__c;
		f = Opportunity.fields.Introduction_start_timing__c;
		f = Opportunity.fields.Opportunity_DeadMemo__c;
		f = Opportunity.fields.Settled_type__c;
		f = Opportunity.fields.CreatedById;
		f = Opportunity.fields.LastModifiedById;
		f = Opportunity.fields.syouninn__c;
		f = Opportunity.fields.CreatedDate;
		f = Opportunity.fields.LastModifiedDate;
		f = Opportunity.fields.Salesforce_ID__c;
		f = Opportunity.fields.BoardDiscussed__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = Opportunity.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'LEX_Opportunity_view';
			mainQuery = new SkyEditor2.Query('Opportunity');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('OpportunityCopy__c');
			mainQuery.addFieldAsOutput('AccountId');
			mainQuery.addFieldAsOutput('Opportunity_Type__c');
			mainQuery.addFieldAsOutput('Opportunity_Partner__c');
			mainQuery.addFieldAsOutput('Opportunity_endClient1__c');
			mainQuery.addFieldAsOutput('Opportunity_Competition__c');
			mainQuery.addFieldAsOutput('Opportunity_SalesPerson_Unit__c');
			mainQuery.addFieldAsOutput('MQLSalesPerson__c');
			mainQuery.addFieldAsOutput('sub_sales_person__c');
			mainQuery.addFieldAsOutput('settled_sales_person_unit__c');
			mainQuery.addFieldAsOutput('MQLDate__c');
			mainQuery.addFieldAsOutput('MQL_first_visit_date__c');
			mainQuery.addFieldAsOutput('AppointmentDay__c');
			mainQuery.addFieldAsOutput('first_visit_date__c');
			mainQuery.addFieldAsOutput('recorded_date__c');
			mainQuery.addFieldAsOutput('suggest_date__c');
			mainQuery.addFieldAsOutput('Post_evaluation__c');
			mainQuery.addFieldAsOutput('LeadEvaluation__c');
			mainQuery.addFieldAsOutput('Opportunity_lead1__c');
			mainQuery.addFieldAsOutput('Opportunity_lead2__c');
			mainQuery.addFieldAsOutput('CampaignId');
			mainQuery.addFieldAsOutput('AppointmentSalesPerson__c');
			mainQuery.addFieldAsOutput('AppointmentType__c');
			mainQuery.addFieldAsOutput('SQLType__c');
			mainQuery.addFieldAsOutput('contents_order_status__c');
			mainQuery.addFieldAsOutput('account_web_site__c');
			mainQuery.addFieldAsOutput('account_web_site_unnecessary__c');
			mainQuery.addFieldAsOutput('ServiceWEBSite__c');
			mainQuery.addFieldAsOutput('WEBTitle__c');
			mainQuery.addFieldAsOutput('Opportunity_WEBsiteRank__c');
			mainQuery.addFieldAsOutput('WebSiteType__c');
			mainQuery.addFieldAsOutput('SEOMeasureStatus__c');
			mainQuery.addFieldAsOutput('SEO_contents_budget_per_month__c');
			mainQuery.addFieldAsOutput('Products__c');
			mainQuery.addFieldAsOutput('writing_resource_summary__c');
			mainQuery.addFieldAsOutput('contents_measure_status__c');
			mainQuery.addFieldAsOutput('AmountTotal__c');
			mainQuery.addFieldAsOutput('CreditDecisionPrice__c');
			mainQuery.addFieldAsOutput('gross_profit_all__c');
			mainQuery.addFieldAsOutput('CloseDate');
			mainQuery.addFieldAsOutput('StageName');
			mainQuery.addFieldAsOutput('this_month_approach__c');
			mainQuery.addFieldAsOutput('agreement_probability__c');
			mainQuery.addFieldAsOutput('karute__c');
			mainQuery.addFieldAsOutput('suggestion_importance_summary__c');
			mainQuery.addFieldAsOutput('suggestion_importance__c');
			mainQuery.addFieldAsOutput('review_practitioner__c');
			mainQuery.addFieldAsOutput('review_status__c');
			mainQuery.addFieldAsOutput('document__c');
			mainQuery.addFieldAsOutput('document_creating_user__c');
			mainQuery.addFieldAsOutput('documents_delivery_date__c');
			mainQuery.addFieldAsOutput('Opportunity_Support__c');
			mainQuery.addFieldAsOutput('lastTereapoDate__c');
			mainQuery.addFieldAsOutput('Opportunity_deadDay__c');
			mainQuery.addFieldAsOutput('lastTereapoMemo__c');
			mainQuery.addFieldAsOutput('NA_memo__c');
			mainQuery.addFieldAsOutput('Budget_acquisition__c');
			mainQuery.addFieldAsOutput('ProposalProducts__c');
			mainQuery.addFieldAsOutput('Opportunity_ActionDay__c');
			mainQuery.addFieldAsOutput('Customer_level__c');
			mainQuery.addFieldAsOutput('Competition_winner__c');
			mainQuery.addFieldAsOutput('Customer_needs_level__c');
			mainQuery.addFieldAsOutput('Dead_type__c');
			mainQuery.addFieldAsOutput('Recycle_User__c');
			mainQuery.addFieldAsOutput('Introduction_start_timing__c');
			mainQuery.addFieldAsOutput('Opportunity_DeadMemo__c');
			mainQuery.addFieldAsOutput('Settled_type__c');
			mainQuery.addFieldAsOutput('CreatedById');
			mainQuery.addFieldAsOutput('LastModifiedById');
			mainQuery.addFieldAsOutput('syouninn__c');
			mainQuery.addFieldAsOutput('CreatedDate');
			mainQuery.addFieldAsOutput('LastModifiedDate');
			mainQuery.addFieldAsOutput('Salesforce_ID__c');
			mainQuery.addFieldAsOutput('BoardDiscussed__c');
			mainQuery.addFieldAsOutput('LeadSource');
			mainQuery.addFieldAsOutput('Id');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('OpportunityCopy__c', 'OpportunityCopy__c');
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			if (record.Id == null) {
				saveOldValues();
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}