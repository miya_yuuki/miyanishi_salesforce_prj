global with sharing class syoudan_ContactPerson_view extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public Opportunity record {get{return (Opportunity)mainRecord;}}
	public Component88 Component88 {get; private set;}
	{
	setApiVersion(42.0);
	}
	public syoudan_ContactPerson_view(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = AccountCustomer__c.fields.account__c;
		f = AccountCustomer__c.fields.department__c;
		f = AccountCustomer__c.fields.Title__c;
		f = AccountCustomer__c.fields.yakuwari__c;
		f = AccountCustomer__c.fields.Mail__c;
		f = AccountCustomer__c.fields.CustomerName__c;
		f = AccountCustomer__c.fields.Report1__c;
		f = AccountCustomer__c.fields.ReportSend1__c;
		f = AccountCustomer__c.fields.PartnerReport__c;
		f = AccountCustomer__c.fields.PartnerReport_c__c;
		f = Contact.fields.Department;
		f = Contact.fields.Title;
		f = Contact.fields.Contact_Role__c;
		f = Contact.fields.Email;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = Opportunity.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'syoudan_ContactPerson_view';
			mainQuery = new SkyEditor2.Query('Opportunity');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			Component88 = new Component88(new List<AccountCustomer__c>(), new List<Component88Item>(), new List<AccountCustomer__c>(), null);
			listItemHolders.put('Component88', Component88);
			query = new SkyEditor2.Query('AccountCustomer__c');
			query.addField('CustomerName__c');
			query.addField('Report1__c');
			query.addField('PartnerReport__c');
			query.addField('ReportSend1__c');
			query.addField('PartnerReport_c__c');
			query.addFieldAsOutput('account__c');
			query.addFieldAsOutput('department__c');
			query.addFieldAsOutput('Title__c');
			query.addFieldAsOutput('yakuwari__c');
			query.addFieldAsOutput('Mail__c');
			query.addWhere('Opportunity_customer__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('Component88', 'Opportunity_customer__c');
			Component88.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('Component88', query);
			registRelatedList('Opportunity_customer__r', 'Component88');
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			Component88.extender = this.extender;
			if (record.Id == null) {
				saveOldValues();
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class Component88Item extends SkyEditor2.ListItem {
		public AccountCustomer__c record{get; private set;}
		@TestVisible
		Component88Item(Component88 holder, AccountCustomer__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}

	public void loadReferenceValues_Component117() {
		if (record.CustomerName__c == null) {
if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.department__c)) {
				record.department__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.Title__c)) {
				record.Title__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.yakuwari__c)) {
				record.yakuwari__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.Mail__c)) {
				record.Mail__c = null;
			}
				return;
		}
		Contact[] referenceTo = [SELECT Department,Title,Contact_Role__c,Email FROM Contact WHERE Id=:record.CustomerName__c];
		if (referenceTo.size() == 0) {
			record.CustomerName__c.addError(SkyEditor2.Messages.referenceDataNotFound(record.CustomerName__c));
			return;
		}
		if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.department__c)) {
			record.department__c = referenceTo[0].Department;
		}
		if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.Title__c)) {
			record.Title__c = referenceTo[0].Title;
		}
		if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.yakuwari__c)) {
			record.yakuwari__c = referenceTo[0].Contact_Role__c;
		}
		if (SkyEditor2.Util.isEditable(record, AccountCustomer__c.fields.Mail__c)) {
			record.Mail__c = referenceTo[0].Email;
		}
		
	}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component88 extends SkyEditor2.ListItemHolder {
		public List<Component88Item> items{get; private set;}
		@TestVisible
			Component88(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component88Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component88Item(this, (AccountCustomer__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}