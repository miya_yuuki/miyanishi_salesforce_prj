@isTest
public class AtkApplyTest {
    static testMethod void testInsert() {
        insert new teamspirit__AtkApply__c();
    }

    // 与信チェックの結果が引き継がれること
    public static testMethod void testAtkApplyCreditCheck() {
        // 取引先
		Account testAccount = new Account(
			Name = 'テスト株式会社',
			account_localName__c = 'テストフリガナ',
			account_Lead__c = '架電',
			account_Lead2__c = '架電'
		);

        insert testAccount;

        // 稟議&申請
		teamspirit__AtkApply__c testAtkApply = new teamspirit__AtkApply__c(
			Name = 'aaa',
			RecordTypeId = '01210000000ATZSAA4',
            account_name__c = testAccount.Id,
            teamspirit__Status__c = '承認待ち'
		);

        insert testAtkApply;

        testAtkApply.first_yoshin__c = System.today();
        testAtkApply.yoshin__c = 'A格';
        testAtkApply.teamspirit__Status__c = '承認済み';

        update testAtkApply;
    }

    // 反社申請の結果が引き継がれること
    public static testMethod void testAtkApplyAntiSocialCheck() {
        // 取引先
		Account testAccount = new Account(
			Name = 'テスト株式会社',
			account_localName__c = 'テストフリガナ',
			account_Lead__c = '架電',
			account_Lead2__c = '架電'
		);

        insert testAccount;

        // 稟議&申請
		teamspirit__AtkApply__c testAtkApply = new teamspirit__AtkApply__c(
			Name = 'aaa',
			RecordTypeId = '01210000000ATZSAA4',
            account_name__c = testAccount.Id,
            teamspirit__Status__c = '承認待ち'
		);

        insert testAtkApply;

        testAtkApply.resarch_day__c = System.today();
        testAtkApply.CompanyCheck__c = 'OK';
        testAtkApply.teamspirit__Status__c = '承認済み';

        update testAtkApply;
    }

    // 支払いサイト変更の結果が引き継がれること
    public static testMethod void testAtkApplyPaymentTerms() {
        // 取引先
		Account testAccount = new Account(
			Name = 'テスト株式会社',
			account_localName__c = 'テストフリガナ',
			account_Lead__c = '架電',
			account_Lead2__c = '架電'
		);

        insert testAccount;

        // 稟議&申請
		teamspirit__AtkApply__c testAtkApply = new teamspirit__AtkApply__c(
			Name = 'aaa',
			RecordTypeId = '01210000000ATZSAA4',
            account_name__c = testAccount.Id,
            teamspirit__Status__c = '承認待ち'
		);

        insert testAtkApply;

        testAtkApply.changed_shiharai__c = 'a0w10000004pkzOAAQ'; //当月締め当月末払いに変更
        testAtkApply.change_shiharai__c = true;
        testAtkApply.teamspirit__Status__c = '承認済み';

        update testAtkApply;
    }
}