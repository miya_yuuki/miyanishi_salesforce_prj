public with sharing class OrderItemUpdate {
	public static void prcOrderItemUpdate(List<Order__c> objOrder, Map<Id, Order__c> oldMap, Boolean upd) {
		// 注文ID
		List<String> strOrderIds = new List<String>();
		// 注文商品オブジェクト
		List<OrderItem__c> updOrderItem = new List<OrderItem__c>();
		
		// 更新対象のチェック
		for (Order__c obj : objOrder) {
			if (prcCheckPaymentSiteMaster(upd, obj, oldMap) == null) {
				continue;
			}
			strOrderIds.add(obj.Id);
		}
		
		// 注文商品取得
		updOrderItem = [ SELECT Id FROM OrderItem__c WHERE Item2_Relation__r.Id IN :strOrderIds ];
		if (updOrderItem.size() > 0) {
			update updOrderItem;
			System.debug('更新対象' + updOrderItem.size() + '件');
		}
		else {
			System.debug('更新対象なし');
		}
	}
	
	private static String prcCheckPaymentSiteMaster (Boolean upd, Order__c objNew, Map<Id, Order__c> oldMap) {
		if (upd) {
			// 古い値を取得
			Order__c objOld = oldMap.get(objNew.Id);
			// 注文の支払サイトが更新された場合
			if (objNew.Order_PaymentSiteMaster__c != objOld.Order_PaymentSiteMaster__c) {
				System.debug('支払サイト更新検知' + objNew.Id);
				String PaymentSite = objNew.Order_PaymentSiteMaster__c;
				return PaymentSite;
			}
		}
		// 条件に合致しない
		return null;
	}
}