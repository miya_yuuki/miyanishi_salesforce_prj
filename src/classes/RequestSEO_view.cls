global with sharing class RequestSEO_view extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public CustomObject1__c record {get{return (CustomObject1__c)mainRecord;}}
	public Component559 Component559 {get; private set;}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	{
	setApiVersion(42.0);
	}
	public RequestSEO_view(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = CustomObject1__c.fields.unit__c;
		f = CustomObject1__c.fields.Name;
		f = CustomObject1__c.fields.OwnerId;
		f = CustomObject1__c.fields.Status__c;
		f = CustomObject1__c.fields.SalesSupportPersonView__c;
		f = CustomObject1__c.fields.RequestDay__c;
		f = CustomObject1__c.fields.RecordTypeId;
		f = CustomObject1__c.fields.RequestEndDay__c;
		f = CustomObject1__c.fields.OrderNo__c;
		f = CustomObject1__c.fields.SupportJudgePerson__c;
		f = CustomObject1__c.fields.AccountName__c;
		f = CustomObject1__c.fields.SupportDay__c;
		f = CustomObject1__c.fields.ProductGroupView__c;
		f = CustomObject1__c.fields.SupportMember__c;
		f = CustomObject1__c.fields.OrderProvisional1__c;
		f = CustomObject1__c.fields.RDDay__c;
		f = CustomObject1__c.fields.OrderProvisionalMemo1__c;
		f = CustomObject1__c.fields.Sign__c;
		f = CustomObject1__c.fields.SalvageScheduledDate__c;
		f = CustomObject1__c.fields.RequestMemo__c;
		f = CustomObject1__c.fields.endday__c;
		f = CustomObject1__c.fields.cancelReasonInServiceFlow__c;
		f = CustomObject1__c.fields.ClauseDay__c;
		f = CustomObject1__c.fields.cancelReasonForAP__c;
		f = CustomObject1__c.fields.CancelType2__c;
		f = CustomObject1__c.fields.cancelReasonForCS__c;
		f = CustomObject1__c.fields.CancelType__c;
		f = CustomObject1__c.fields.cancelReasonErrorDetail__c;
		f = CustomObject1__c.fields.CancelMonthBilling__c;
		f = CustomObject1__c.fields.externalCancelReason__c;
		f = CustomObject1__c.fields.RequestAmount__c;
		f = CustomObject1__c.fields.cancelReasonFactorDetail__c;
		f = CustomObject1__c.fields.kaiyakuTotalAmount__c;
		f = CustomObject1__c.fields.OrderNoChange__c;
		f = CustomObject1__c.fields.OrderNoChange2__c;
		f = CustomObject1__c.fields.MemoPrint__c;
		f = CustomObject1__c.fields.OrderItem_AutoCompletion__c;
		f = SEOChange__c.fields.OrderItem__c;
		f = SEOChange__c.fields.ProductName2__c;
		f = SEOChange__c.fields.Amount2__c;
		f = SEOChange__c.fields.CancelableAmount__c;
		f = SEOChange__c.fields.Item2_ServiceDate_EndDay__c;
		f = SEOChange__c.fields.CancelMonthCheck__c;
		f = SEOChange__c.fields.KeywordType1__c;
		f = SEOChange__c.fields.Keyword2__c;
		f = SEOChange__c.fields.URL2__c;
		f = CustomObject1__c.fields.ManualSetting_CancelTotalAmount__c;
		f = CustomObject1__c.fields.Skip_OrderItemCheck1__c;
		f = CustomObject1__c.fields.Skip_OrderItemCheck3__c;
		f = CustomObject1__c.fields.ManualSetting_CancelMonthRequestAmount__c;
		f = CustomObject1__c.fields.Skip_OrderItemCheck2__c;
		f = CustomObject1__c.fields.Skip_OrderItemCheck4__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = CustomObject1__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'RequestSEO_view';
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(CustomObject1__c.SObjectType);
			mainQuery = new SkyEditor2.Query('CustomObject1__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('unit__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('OwnerId');
			mainQuery.addFieldAsOutput('Status__c');
			mainQuery.addFieldAsOutput('SalesSupportPersonView__c');
			mainQuery.addFieldAsOutput('RequestDay__c');
			mainQuery.addFieldAsOutput('RecordType.Name');
			mainQuery.addFieldAsOutput('RequestEndDay__c');
			mainQuery.addFieldAsOutput('OrderNo__c');
			mainQuery.addFieldAsOutput('SupportJudgePerson__c');
			mainQuery.addFieldAsOutput('AccountName__c');
			mainQuery.addFieldAsOutput('SupportDay__c');
			mainQuery.addFieldAsOutput('ProductGroupView__c');
			mainQuery.addFieldAsOutput('SupportMember__c');
			mainQuery.addFieldAsOutput('OrderProvisional1__c');
			mainQuery.addFieldAsOutput('RDDay__c');
			mainQuery.addFieldAsOutput('OrderProvisionalMemo1__c');
			mainQuery.addFieldAsOutput('Sign__c');
			mainQuery.addFieldAsOutput('SalvageScheduledDate__c');
			mainQuery.addFieldAsOutput('RequestMemo__c');
			mainQuery.addFieldAsOutput('endday__c');
			mainQuery.addFieldAsOutput('cancelReasonInServiceFlow__c');
			mainQuery.addFieldAsOutput('ClauseDay__c');
			mainQuery.addFieldAsOutput('cancelReasonForAP__c');
			mainQuery.addFieldAsOutput('CancelType2__c');
			mainQuery.addFieldAsOutput('cancelReasonForCS__c');
			mainQuery.addFieldAsOutput('CancelType__c');
			mainQuery.addFieldAsOutput('cancelReasonErrorDetail__c');
			mainQuery.addFieldAsOutput('CancelMonthBilling__c');
			mainQuery.addFieldAsOutput('externalCancelReason__c');
			mainQuery.addFieldAsOutput('RequestAmount__c');
			mainQuery.addFieldAsOutput('cancelReasonFactorDetail__c');
			mainQuery.addFieldAsOutput('kaiyakuTotalAmount__c');
			mainQuery.addFieldAsOutput('OrderNoChange__c');
			mainQuery.addFieldAsOutput('OrderNoChange2__c');
			mainQuery.addFieldAsOutput('MemoPrint__c');
			mainQuery.addFieldAsOutput('OrderItem_AutoCompletion__c');
			mainQuery.addFieldAsOutput('ManualSetting_CancelTotalAmount__c');
			mainQuery.addFieldAsOutput('Skip_OrderItemCheck1__c');
			mainQuery.addFieldAsOutput('Skip_OrderItemCheck3__c');
			mainQuery.addFieldAsOutput('ManualSetting_CancelMonthRequestAmount__c');
			mainQuery.addFieldAsOutput('Skip_OrderItemCheck2__c');
			mainQuery.addFieldAsOutput('Skip_OrderItemCheck4__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			Component559 = new Component559(new List<SEOChange__c>(), new List<Component559Item>(), new List<SEOChange__c>(), null);
			listItemHolders.put('Component559', Component559);
			query = new SkyEditor2.Query('SEOChange__c');
			query.addFieldAsOutput('OrderItem__c');
			query.addFieldAsOutput('ProductName2__c');
			query.addFieldAsOutput('Amount2__c');
			query.addFieldAsOutput('CancelableAmount__c');
			query.addFieldAsOutput('Item2_ServiceDate_EndDay__c');
			query.addFieldAsOutput('CancelMonthCheck__c');
			query.addFieldAsOutput('KeywordType1__c');
			query.addFieldAsOutput('Keyword2__c');
			query.addFieldAsOutput('URL2__c');
			query.addFieldAsOutput('RecordTypeId');
			query.addWhere('sinsei__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('Component559', 'sinsei__c');
			Component559.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('Component559', query);
			registRelatedList('sinsei__r', 'Component559');
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			Component559.extender = this.extender;
			if (record.Id == null) {
				saveOldValues();
				if(record.RecordTypeId == null) recordTypeSelector.applyDefault(record);
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class Component559Item extends SkyEditor2.ListItem {
		public SEOChange__c record{get; private set;}
		@TestVisible
		Component559Item(Component559 holder, SEOChange__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component559 extends SkyEditor2.ListItemHolder {
		public List<Component559Item> items{get; private set;}
		@TestVisible
			Component559(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component559Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component559Item(this, (SEOChange__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}