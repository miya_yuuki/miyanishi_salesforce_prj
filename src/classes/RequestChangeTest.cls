@isTest
private with sharing class RequestChangeTest{
	private static testMethod void testPageMethods() {		RequestChange extension = new RequestChange(new ApexPages.StandardController(new CustomObject1__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	@isTest
	private static void testLightDataTables(){

		System.assert(true);
	}

	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component412() {
		String testReferenceId = '';
		RequestChange extension = new RequestChange(new ApexPages.StandardController(new CustomObject1__c()));
		extension.loadReferenceValues_Component412();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(Order__c.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<Order__c> parents = [SELECT Id FROM Order__c LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		Order__c parent = [SELECT Id,AccountName__c,Order_PaymentSiteMaster2__c,unit__c,Contract_SalesPerson1__c FROM Order__c WHERE Id = :testReferenceId];
		extension.record.OrderNo__c = parent.Id;
		extension.loadReferenceValues_Component412();
				
		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.Account__c)) {
			System.assertEquals(parent.AccountName__c, extension.record.Account__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.Patnername1__c)) {
			System.assertEquals(parent.Order_PaymentSiteMaster2__c, extension.record.Patnername1__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.SalesPerson__c)) {
			System.assertEquals(parent.unit__c, extension.record.SalesPerson__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.SalesPerson1__c)) {
			System.assertEquals(parent.Contract_SalesPerson1__c, extension.record.SalesPerson1__c);
		}

		System.assert(true);
	}

	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component162() {
		String testReferenceId = '';
		RequestChange extension = new RequestChange(new ApexPages.StandardController(new CustomObject1__c()));
		extension.loadReferenceValues_Component162();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(Contact.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<Contact> parents = [SELECT Id FROM Contact LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		Contact parent = [SELECT Id,Contact_BillCompany__c,Department,Title FROM Contact WHERE Id = :testReferenceId];
		extension.record.Customer__c = parent.Id;
		extension.loadReferenceValues_Component162();
				
		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.Account__c)) {
			System.assertEquals(parent.Contact_BillCompany__c, extension.record.Account__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.BillDepartment__c)) {
			System.assertEquals(parent.Department, extension.record.BillDepartment__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.BillTitle__c)) {
			System.assertEquals(parent.Title, extension.record.BillTitle__c);
		}

		System.assert(true);
	}

	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component94() {
		String testReferenceId = '';
		RequestChange extension = new RequestChange(new ApexPages.StandardController(new CustomObject1__c()));
		extension.loadReferenceValues_Component94();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(Contact.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<Contact> parents = [SELECT Id FROM Contact LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		Contact parent = [SELECT Id,Contact_BillCompany__c,Department,Title FROM Contact WHERE Id = :testReferenceId];
		extension.record.Billtantou__c = parent.Id;
		extension.loadReferenceValues_Component94();
				
		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.Account2__c)) {
			System.assertEquals(parent.Contact_BillCompany__c, extension.record.Account2__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.BillDepartment2__c)) {
			System.assertEquals(parent.Department, extension.record.BillDepartment2__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.BillTitle2__c)) {
			System.assertEquals(parent.Title, extension.record.BillTitle2__c);
		}

		System.assert(true);
	}
}