global class BatchCreateBillRecord implements Database.Batchable<sObject> {
	// SOQL用
	public String query;
	//1回のexecuteメソッドで処理される件数
	public final Integer BATCH_SIZE = 1;
	
	// 請求データ作成バッチオブジェクト
	public BillCreateBatch__c objBatch { get; set; }
	// 件数チェック用
	public List<TextManagement2__c> objCheck { get; set; }
	
	// イニシャライズ
	global BatchCreateBillRecord() {
		// 請求データ作成バッチオブジェクトのインスタンスを生成
		objBatch = new BillCreateBatch__c();
	}
	
	
	//バッチ実行
	public PageReference doStartBatch() {
		// エラーチェック
		if (prcErrCheck()) return null;
		
		// クエリ作成
		query = prcCreateQuery();
		
		//どのオブジェクトのレコードに対し処理を実行するのかSOQLを投げる
		BatchCreateBillRecord batch = new BatchCreateBillRecord();
		
		// パラメータをコピー
		batch.objBatch = objBatch.clone(false, true);
		
		// バッチ実行
		Database.executeBatch(batch, BATCH_SIZE);
		// 完了メッセージ
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '請求データ作成バッチ Ver.2 を実行しました'));
		
		return null;
	}
	
	// 件数チェック
	public PageReference doCountCheck() {
		// エラーチェック
		if (prcErrCheck()) return null;
		// クエリ作成
		query = prcCreateQuery();
		// 件数チェック
		objCheck = Database.query(query);
		
		// 追加メッセージ
		String strTextManagement2 = '<BR><BR>■対象納品管理<BR>注文商品ID : 納品管理ID : 納品番号<BR>';
		for (TextManagement2__c obj :objCheck) {
			strTextManagement2 += obj.Item2_OrderProduct__c + ' : ' + obj.Id + ' : ' + obj.Name + '<BR>';
		}
		// 50件まで
		if (objCheck.size() > 50) {
			strTextManagement2 = '';
		}
		// 完了メッセージ
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, objCheck.size() + '件の納品管理が請求データにコピーされます<BR>' + strTextManagement2));
		
		return null;
	}
	
	// エラーチェック
	private Boolean prcErrCheck() {
		// エラーメッセージ
		String strErrMsg = '';
		
		// 支払期日Fromは入力必須
		if (objBatch.From__c == null) strErrMsg += '請求対象日: From を入力して下さい<br/>';
		// 支払期日Toは入力必須
		if (objBatch.To__c == null) strErrMsg += '請求対象日: To を入力して下さい<br/>';
		// From > Toはエラー
		if (objBatch.From__c > objBatch.To__c) strErrMsg += '請求対象日のToは、Fromより過去の日付を入力して下さい';
		// From < 2016-12-01はエラー
		Date limitDate = date.newinstance(2016, 12, 1);
		if (objBatch.From__c < limitDate) strErrMsg += '請求対象日: From は2016/12/01以降を入力して下さい<br/>';
		
		// エラーメッセージを表示
		if (strErrMsg != '') {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, strErrMsg));
			return true;
		}
		return false;
	}
	
	// クエリ作成
	private String prcCreateQuery() {
		// Queryを生成
		String q = 
			'SELECT ' +
				'Id, ' +
				'Name, ' +
				'ProductSeparateUMU__c, ' +
				'BillingTiming__c, ' +
				'Item2_OrderProduct__r.Item2_RelationView__c, ' +
				'Billchek__c, ' +
				'Bill_Main__c ' +
			'FROM ' +
				'TextManagement2__c ' +
			'WHERE ' +
				'Bill_Main__c >= ' + ur(objBatch.From__c) + ' AND Bill_Main__c <=' + ur(objBatch.To__c) + ' AND Billchek__c = false ' +
				'AND Item2_OrderProduct__r.InvoiceJoken__c = true ' +
				'AND CloudStatus__c = \'社外納品済み\' ' +
				'AND BillingTiming__c = \'納品\'';
		
		// 取引先責任者のID
		List<String> contactIds = new List<String>();
		
		if (objBatch.Contact1__c != null) contactIds.add(objBatch.Contact1__c);			// 取引先責任者1の指定
		if (objBatch.Contact2__c != null) contactIds.add(objBatch.Contact2__c);			// 取引先責任者2の指定
		if (objBatch.Contact3__c != null) contactIds.add(objBatch.Contact3__c);			// 取引先責任者3の指定
		if (objBatch.Contact4__c != null) contactIds.add(objBatch.Contact4__c);			// 取引先責任者4の指定
		if (objBatch.Contact5__c != null) contactIds.add(objBatch.Contact5__c);			// 取引先責任者5の指定
		if (objBatch.Contact6__c != null) contactIds.add(objBatch.Contact6__c);			// 取引先責任者6の指定
		if (objBatch.Contact7__c != null) contactIds.add(objBatch.Contact7__c);			// 取引先責任者7の指定
		if (objBatch.Contact8__c != null) contactIds.add(objBatch.Contact8__c);			// 取引先責任者8の指定
		if (objBatch.Contact9__c != null) contactIds.add(objBatch.Contact9__c);			// 取引先責任者9の指定
		if (objBatch.Contact10__c != null) contactIds.add(objBatch.Contact10__c);		// 取引先責任者10の指定
		
		if (contactIds.size() > 0) {
			q += 'AND (Item2_OrderProduct__r.Item2_Relation__r.BillTo_Contact__c IN ' + prcCreateIds(contactIds) + ')';
		}
		return q;
	}
	
	// IDを文字列化
	private static String prcCreateIds(List<String> strIds) {
		if (strIds.size() == 0) return '';
		String q = '(';
		for (String str : strIds) q += '\'' + str + '\',';
		q = q.substring(0, q.length() - 1);
		q += ')';
		return q;
	}
	
	// Utillクラスの日付を文字列に変換を省略
	private static String ur(Date dt) {
		return UtilityClass.retStringDt(dt);
	}
	
	// 日付が指定の期間内かをチェック
	private static Boolean betDt(Date dt, BillCreateBatch__c obj) {
		if (dt >= obj.From__c && dt <= obj.To__c) return true;
		return false;
	}
	
	//バッチ開始処理
	//開始するためにqueryを実行する。この実行されたSOQLのデータ分処理する。
	//5千万件以上のレコードになるとエラーになる。
	global Database.QueryLocator start(Database.BatchableContext BC) {
		query = prcCreateQuery();
		System.debug('*************s***:' + query);
		return Database.getQueryLocator(query);
	}
	
	//バッチ処理内容
	//scopeにgetQueryLocatorの内容がバッチサイズ分格納されてくる
	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		// 更新用の納品管理
		List<TextManagement2__c> updTextManagement2 = new List<TextManagement2__c>();
		
		for(sObject obj : scope) {
			// 納品管理のインスタンス
			TextManagement2__c res = new TextManagement2__c();
			// 納品管理をコピー
			res = (TextManagement2__c)obj.clone(true, true);
			
			Boolean flg = res.Billchek__c;
		
			// 分割納品無＆支払期日期間内
			if (betDt(res.Bill_Main__c, objBatch)) flg = true;
			
			// 納品管理のインスタンスを生成
			TextManagement2__c newItm = new TextManagement2__c(
				Id = res.Id,
				Billchek__c = flg
			);
			// 配列に追加
			updTextManagement2.add(newItm);
		}
		System.debug('*************updTextManagement2: ' + updTextManagement2.size());
		// 納品管理を更新
		if (updTextManagement2.size() > 0) update updTextManagement2;
	}
	
	global void finish(Database.BatchableContext BC){}
}