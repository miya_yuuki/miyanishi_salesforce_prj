global with sharing class PenaltyKaruteSearch extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public karute__c record{get;set;}	
			
	
		public Component3 Component3 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component36_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component36_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component36_op{get;set;}	
		public List<SelectOption> valueOptions_karute_c_PenaltyStatusCS_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component8_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component8_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component8_op{get;set;}	
		public List<SelectOption> valueOptions_karute_c_penaltyStatusRD_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component48_val {get;set;}	
		public SkyEditor2.TextHolder Component48_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component12_val {get;set;}	
		public SkyEditor2.TextHolder Component12_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component52_val {get;set;}	
		public SkyEditor2.TextHolder Component52_op{get;set;}	
			
	public String recordTypeRecordsJSON_karute_c {get; private set;}
	public String defaultRecordTypeId_karute_c {get; private set;}
	public String metadataJSON_karute_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public PenaltyKaruteSearch(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = karute__c.fields.PenaltyStatusCS__c;
		f = karute__c.fields.penaltyStatusRD__c;
		f = Account.fields.Name;
		f = karute__c.fields.URL__c;
		f = karute__c.fields.SalesPersonOwner__c;
		f = karute__c.fields.AccountName__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = karute__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component36_val = new SkyEditor2__SkyEditorDummy__c();	
				Component36_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component36_op = new SkyEditor2.TextHolder();	
				valueOptions_karute_c_PenaltyStatusCS_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : karute__c.PenaltyStatusCS__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_karute_c_PenaltyStatusCS_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component8_val = new SkyEditor2__SkyEditorDummy__c();	
				Component8_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component8_op = new SkyEditor2.TextHolder();	
				valueOptions_karute_c_penaltyStatusRD_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : karute__c.penaltyStatusRD__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_karute_c_penaltyStatusRD_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component12_val = new SkyEditor2__SkyEditorDummy__c();	
				Component12_op = new SkyEditor2.TextHolder();	
					
				Component52_val = new SkyEditor2__SkyEditorDummy__c();	
				Component52_op = new SkyEditor2.TextHolder();	
					
				Component48_val = new SkyEditor2__SkyEditorDummy__c();	
				Component48_op = new SkyEditor2.TextHolder();	
					
				queryMap.put(	
					'Component3',	
					new SkyEditor2.Query('karute__c')
						.addField('penaltyStatusRD__c')
						.addFieldAsOutput('PenaltyStatusCS__c')
						.addFieldAsOutput('AccountName__c')
						.addFieldAsOutput('URL__c')
						.addFieldAsOutput('SalesPersonOwner__c')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component36_val_dummy, 'SkyEditor2__Text__c','PenaltyStatusCS__c', Component36_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component8_val_dummy, 'SkyEditor2__Text__c','penaltyStatusRD__c', Component8_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component48_val, 'SkyEditor2__Text__c', 'AccountName__r.Name',Account.fields.Name, Component48_op, true, false,true,0,false,'AccountName__c',karute__c.fields.AccountName__c )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component12_val, 'SkyEditor2__Url__c', 'URL__c', Component12_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component52_val, 'SkyEditor2__Text__c', 'SalesPersonOwner__c', Component52_op, true, 0, false ))
				);	
					
					Component3 = new Component3(new List<karute__c>(), new List<Component3Item>(), new List<karute__c>(), null);
				listItemHolders.put('Component3', Component3);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(karute__c.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = true;
			presetSystemParams();
			Component3.extender = this.extender;
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_karute_c_PenaltyStatusCS_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_karute_c_penaltyStatusRD_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_Account_Name() { 
			return getOperatorOptions('Account', 'Name');	
		}	
		public List<SelectOption> getOperatorOptions_karute_c_URL_c() { 
			return getOperatorOptions('karute__c', 'URL__c');	
		}	
		public List<SelectOption> getOperatorOptions_karute_c_SalesPersonOwner_c() { 
			return getOperatorOptions('karute__c', 'SalesPersonOwner__c');	
		}	
			
			
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public karute__c record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, karute__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.ListItemHolder {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (karute__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	public karute__c Component3_table_Conversion { get { return new karute__c();}}
	
	public String Component3_table_selectval { get; set; }
	
	
			
	}