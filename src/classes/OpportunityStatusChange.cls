public with sharing class OpportunityStatusChange {
	public static void prcOpportunityStatusChange(List<task__c> objTelAppo) {
		// リードID
		List<String> strOpportunityIds = new List<String>();
		// リードオブジェクト
		List<Opportunity> updOpportunity = new List<Opportunity>();
		
		// テレアポのループ
		for (task__c obj: objTelAppo) {
			// テレアポにリードの参照関係が無かったら処理しない
			if (obj.Opportunity__c == null) continue;
			strOpportunityIds.add(obj.Opportunity__c);
			Integer StatusNo = 0;
			if (obj.PhaseBig__c == '未接続') StatusNo = 1;
			if (obj.PhaseBig__c == '接続済' || obj.PhaseBig__c == '興味あり' || obj.PhaseBig__c == 'アプローチ対象外') StatusNo = 2;
			if (obj.PhaseBig__c == 'アポ完了') StatusNo = 3;
			
			updOpportunity = [
				SELECT
					Id,
					Status1__c,
					Status2__c
				FROM Opportunity
				WHERE Id = :strOpportunityIds];
			// ステータス（大）に現状以上の進捗があった場合は、テレアポのステータス（大）を代入
			for (Opportunity objNew: updOpportunity) {
				if (objNew.Status1__c == null || objNew.Status1__c == '未対応' || (objNew.Status1__c == '未接続' && StatusNo >= 1) || ((objNew.Status1__c == '接続済' || objNew.Status1__c == '興味あり' || objNew.Status1__c == 'アプローチ対象外') && StatusNo >= 2) || (objNew.Status1__c == 'アポ完了' && StatusNo == 3)) {
					objNew.Status1__c = obj.PhaseBig__c;
					objNew.Status2__c = obj.PhaseSmall__c;
				}
			}
			if (updOpportunity.size() > 0) {
				update updOpportunity;
			}
		}
	}
}