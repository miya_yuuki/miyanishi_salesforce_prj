global with sharing class OrderSearchCS extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public Order__c record{get;set;}	
			
	
		public Component3 Component3 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component6_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component6_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component6_op{get;set;}	
		public List<SelectOption> valueOptions_Order_c_Order_Phase_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component8_val {get;set;}	
		public SkyEditor2.TextHolder Component8_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component66_val {get;set;}	
		public SkyEditor2.TextHolder Component66_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component82_val {get;set;}	
		public SkyEditor2.TextHolder Component82_op{get;set;}	
			
		public Order__c Component115_val {get;set;}	
		public SkyEditor2.TextHolder Component115_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component16_val {get;set;}	
		public SkyEditor2.TextHolder Component16_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component18_val {get;set;}	
		public SkyEditor2.TextHolder Component18_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component99_val {get;set;}	
		public SkyEditor2.TextHolder Component99_op{get;set;}	
			
		public Order__c Component20_val {get;set;}	
		public SkyEditor2.TextHolder Component20_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component101_val {get;set;}	
		public SkyEditor2.TextHolder Component101_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component24_val {get;set;}	
		public SkyEditor2.TextHolder Component24_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component22_val {get;set;}	
		public SkyEditor2.TextHolder Component22_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component26_val {get;set;}	
		public SkyEditor2.TextHolder Component26_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component77_val {get;set;}	
		public SkyEditor2.TextHolder Component77_op{get;set;}	
			
	public String recordTypeRecordsJSON_Order_c {get; private set;}
	public String defaultRecordTypeId_Order_c {get; private set;}
	public String metadataJSON_Order_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public OrderSearchCS(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = Order__c.fields.Order_Phase__c;
		f = Order__c.fields.ProductType__c;
		f = Order__c.fields.Contract_SalesPerson1__c;
		f = Order__c.fields.CSStatusInput__c;
		f = Order__c.fields.Opportunity_Support__c;
		f = Order__c.fields.CSDay__c;
		f = Order__c.fields.AccountName__c;
		f = Order__c.fields.CSUpcellInput__c;
		f = Order__c.fields.karute__c;
		f = Order__c.fields.CSUpcellDay__c;
		f = Order__c.fields.Order_StartDate__c;
		f = Order__c.fields.Order_EndClient1__c;
		f = Order__c.fields.Order_EndDate__c;
		f = Order__c.fields.DeliveryNoTotalAmount__c;
		f = Order__c.fields.CSUpcell__c;
		f = Order__c.fields.CSUpcell_Day__c;
		f = Order__c.fields.CSStatus__c;
		f = Order__c.fields.CSStatus_Day__c;
		f = Order__c.fields.Name;
		f = Order__c.fields.ProductGroupgazou__c;
		f = Order__c.fields.TotalPrice__c;
		f = Order__c.fields.Item2_DeliveryCount__c;
		f = Order__c.fields.DeliveryTotalAmount__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = Order__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Order__c lookupObjComponent58 = new Order__c();	
				Component6_val = new SkyEditor2__SkyEditorDummy__c();	
				Component6_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component6_op = new SkyEditor2.TextHolder();	
				valueOptions_Order_c_Order_Phase_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : Order__c.Order_Phase__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_Order_c_Order_Phase_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component8_val = new SkyEditor2__SkyEditorDummy__c();	
				Component8_op = new SkyEditor2.TextHolder();	
					
				Component66_val = new SkyEditor2__SkyEditorDummy__c();	
				Component66_op = new SkyEditor2.TextHolder();	
					
				Component82_val = new SkyEditor2__SkyEditorDummy__c();	
				Component82_op = new SkyEditor2.TextHolder();	
					
				Component115_val = lookupObjComponent58;	
				Component115_op = new SkyEditor2.TextHolder();	
					
				Component16_val = new SkyEditor2__SkyEditorDummy__c();	
				Component16_op = new SkyEditor2.TextHolder();	
					
				Component18_val = new SkyEditor2__SkyEditorDummy__c();	
				Component18_op = new SkyEditor2.TextHolder();	
					
				Component99_val = new SkyEditor2__SkyEditorDummy__c();	
				Component99_op = new SkyEditor2.TextHolder();	
					
				Component20_val = lookupObjComponent58;	
				Component20_op = new SkyEditor2.TextHolder();	
					
				Component101_val = new SkyEditor2__SkyEditorDummy__c();	
				Component101_op = new SkyEditor2.TextHolder();	
					
				Component24_val = new SkyEditor2__SkyEditorDummy__c();	
				Component24_op = new SkyEditor2.TextHolder();	
					
				Component22_val = new SkyEditor2__SkyEditorDummy__c();	
				Component22_op = new SkyEditor2.TextHolder();	
					
				Component26_val = new SkyEditor2__SkyEditorDummy__c();	
				Component26_op = new SkyEditor2.TextHolder();	
					
				Component77_val = new SkyEditor2__SkyEditorDummy__c();	
				Component77_op = new SkyEditor2.TextHolder();	
					
				queryMap.put(	
					'Component3',	
					new SkyEditor2.Query('Order__c')
						.addField('CSUpcell__c')
						.addFieldAsOutput('CSUpcell_Day__c')
						.addField('CSStatus__c')
						.addFieldAsOutput('CSStatus_Day__c')
						.addFieldAsOutput('Name')
						.addFieldAsOutput('Order_Phase__c')
						.addFieldAsOutput('karute__c')
						.addFieldAsOutput('AccountName__c')
						.addFieldAsOutput('ProductGroupgazou__c')
						.addFieldAsOutput('TotalPrice__c')
						.addFieldAsOutput('Order_StartDate__c')
						.addFieldAsOutput('Order_EndDate__c')
						.addFieldAsOutput('Contract_SalesPerson1__c')
						.addFieldAsOutput('Opportunity_Support__c')
						.addFieldAsOutput('Item2_DeliveryCount__c')
						.addFieldAsOutput('DeliveryTotalAmount__c')
						.addFieldAsOutput('DeliveryNoTotalAmount__c')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component6_val_dummy, 'SkyEditor2__Text__c','Order_Phase__c', Component6_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component8_val, 'SkyEditor2__Text__c', 'ProductType__c', Component8_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component66_val, 'SkyEditor2__Text__c', 'Contract_SalesPerson1__c', Component66_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component82_val, 'SkyEditor2__Text__c', 'CSStatusInput__c', Component82_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component115_val, 'Opportunity_Support__c', 'Opportunity_Support__c', Component115_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component16_val, 'SkyEditor2__Date__c', 'CSDay__c', Component16_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component18_val, 'SkyEditor2__Text__c', 'AccountName__c', Component18_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component99_val, 'SkyEditor2__Text__c', 'CSUpcellInput__c', Component99_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component20_val, 'karute__c', 'karute__c', Component20_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component101_val, 'SkyEditor2__Date__c', 'CSUpcellDay__c', Component101_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component24_val, 'SkyEditor2__Date__c', 'Order_StartDate__c', Component24_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component22_val, 'SkyEditor2__Text__c', 'Order_EndClient1__c', Component22_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component26_val, 'SkyEditor2__Date__c', 'Order_EndDate__c', Component26_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component77_val, 'SkyEditor2__Text__c', 'DeliveryNoTotalAmount__c', Component77_op, true, 0, false ))
				);	
					
					Component3 = new Component3(new List<Order__c>(), new List<Component3Item>(), new List<Order__c>(), null);
				listItemHolders.put('Component3', Component3);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(Order__c.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = false;
			presetSystemParams();
			Component3.extender = this.extender;
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_Order_c_Order_Phase_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_Order_c_ProductType_c() { 
			return getOperatorOptions('Order__c', 'ProductType__c');	
		}	
		public List<SelectOption> getOperatorOptions_Order_c_Contract_SalesPerson1_c() { 
			return getOperatorOptions('Order__c', 'Contract_SalesPerson1__c');	
		}	
		public List<SelectOption> getOperatorOptions_Order_c_CSStatusInput_c() { 
			return getOperatorOptions('Order__c', 'CSStatusInput__c');	
		}	
		public List<SelectOption> getOperatorOptions_Order_c_Opportunity_Support_c() { 
			return getOperatorOptions('Order__c', 'Opportunity_Support__c');	
		}	
		public List<SelectOption> getOperatorOptions_Order_c_CSDay_c() { 
			return getOperatorOptions('Order__c', 'CSDay__c');	
		}	
		public List<SelectOption> getOperatorOptions_Order_c_AccountName_c() { 
			return getOperatorOptions('Order__c', 'AccountName__c');	
		}	
		public List<SelectOption> getOperatorOptions_Order_c_CSUpcellInput_c() { 
			return getOperatorOptions('Order__c', 'CSUpcellInput__c');	
		}	
		public List<SelectOption> getOperatorOptions_Order_c_karute_c() { 
			return getOperatorOptions('Order__c', 'karute__c');	
		}	
		public List<SelectOption> getOperatorOptions_Order_c_CSUpcellDay_c() { 
			return getOperatorOptions('Order__c', 'CSUpcellDay__c');	
		}	
		public List<SelectOption> getOperatorOptions_Order_c_Order_StartDate_c() { 
			return getOperatorOptions('Order__c', 'Order_StartDate__c');	
		}	
		public List<SelectOption> getOperatorOptions_Order_c_Order_EndClient1_c() { 
			return getOperatorOptions('Order__c', 'Order_EndClient1__c');	
		}	
		public List<SelectOption> getOperatorOptions_Order_c_Order_EndDate_c() { 
			return getOperatorOptions('Order__c', 'Order_EndDate__c');	
		}	
		public List<SelectOption> getOperatorOptions_Order_c_DeliveryNoTotalAmount_c() { 
			return getOperatorOptions('Order__c', 'DeliveryNoTotalAmount__c');	
		}	
			
			
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public Order__c record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, Order__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.ListItemHolder {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (Order__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	public Order__c Component3_table_Conversion { get { return new Order__c();}}
	
	public String Component3_table_selectval { get; set; }
	
	
			
	}