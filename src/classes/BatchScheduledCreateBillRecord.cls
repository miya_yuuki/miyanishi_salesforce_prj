global class BatchScheduledCreateBillRecord implements Database.Batchable<sObject> {

	private String query;

	global BatchScheduledCreateBillRecord(Date fromDate, Date toDate)
	{
		// SOQL を生成
		query =
			'SELECT ' +
				'Id, ' +
				'Billchek__c ' +
			'FROM ' +
				'TextManagement2__c ' +
			'WHERE ' +
				'Bill_Main__c >= ' + UtilityClass.retStringDt(fromDate) + ' ' +
				'AND Bill_Main__c <=' + UtilityClass.retStringDt(toDate) + ' ' +
				'AND Billchek__c = false ' +
				'AND Item2_OrderProduct__r.InvoiceJoken__c = true ' +
				'AND CloudStatus__c = \'社外納品済み\' ' +
				'AND BillingTiming__c = \'納品\'';
	}

	// バッチ開始処理
	// 開始するためにqueryを実行する。この実行されたSOQLのデータ分処理する。
	// 5千万件以上のレコードになるとエラーになる。
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		System.debug('*****************:' + query);
		return Database.getQueryLocator(query);
	}

	// バッチ処理内容
	// scopeにgetQueryLocatorの内容がバッチサイズ分格納されてくる
	global void execute(Database.BatchableContext BC, List<sObject> scope) {

		// 更新対象の納品管理リスト
		List<TextManagement2__c> updatingTextManagements = new List<TextManagement2__c>();

		for(sObject obj : scope) {
			// 納品管理のインスタンス
			TextManagement2__c textManagement = (TextManagement2__c)obj.clone(true, true);

			// 納品管理のインスタンスを生成し、配列に追加
			updatingTextManagements.add(new TextManagement2__c(
				Id = textManagement.Id,
				Billchek__c = true
			));
		}

		System.debug('************* updating TextManagement2__c records: ' + updatingTextManagements.size());

		// 納品管理を更新
		if (updatingTextManagements.size() > 0) update updatingTextManagements;
	}

	global void finish(Database.BatchableContext BC){}
}