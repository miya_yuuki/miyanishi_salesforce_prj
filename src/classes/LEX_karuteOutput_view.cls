global with sharing class LEX_karuteOutput_view extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public karute__c record {get{return (karute__c)mainRecord;}}
	public Component3891 Component3891 {get; private set;}
	public Component3317 Component3317 {get; private set;}
	{
	setApiVersion(42.0);
	}
	public LEX_karuteOutput_view(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = karute__c.fields.AEGISNew__c;
		f = AEGIS__c.fields.AegisLinkId__c;
		f = AEGIS__c.fields.session_modified__c;
		f = AEGIS__c.fields.TargetMonth__c;
		f = AEGIS__c.fields.TargetMonthPeriod__c;
		f = AEGIS__c.fields.Name;
		f = AEGIS__c.fields.Company__c;
		f = AEGIS__c.fields.URL__c;
		f = AEGIS__c.fields.ViewName__c;
		f = AEGIS__c.fields.EF_URL__c;
		f = AEGIS__c.fields.Keyword__c;
		f = AEGIS__c.fields.BrandKeyword__c;
		f = karute__c.fields.cloverNew__c;
		f = CLOVER__c.fields.Name;
		f = CLOVER__c.fields.AccountName__c;
		f = CLOVER__c.fields.URL__c;
		f = CLOVER__c.fields.saito__c;
		f = CLOVER__c.fields.karute__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = karute__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'LEX_karuteOutput_view';
			mainQuery = new SkyEditor2.Query('karute__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('AEGISNew__c');
			mainQuery.addFieldAsOutput('cloverNew__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			Component3891 = new Component3891(new List<AEGIS__c>(), new List<Component3891Item>(), new List<AEGIS__c>(), null);
			listItemHolders.put('Component3891', Component3891);
			query = new SkyEditor2.Query('AEGIS__c');
			query.addFieldAsOutput('AegisLinkId__c');
			query.addFieldAsOutput('session_modified__c');
			query.addFieldAsOutput('TargetMonth__c');
			query.addFieldAsOutput('TargetMonthPeriod__c');
			query.addFieldAsOutput('Name');
			query.addFieldAsOutput('Company__c');
			query.addFieldAsOutput('URL__c');
			query.addFieldAsOutput('ViewName__c');
			query.addFieldAsOutput('EF_URL__c');
			query.addFieldAsOutput('Keyword__c');
			query.addFieldAsOutput('BrandKeyword__c');
			query.addWhere('karute__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('Component3891', 'karute__c');
			Component3891.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('Component3891', query);
			Component3317 = new Component3317(new List<CLOVER__c>(), new List<Component3317Item>(), new List<CLOVER__c>(), null);
			listItemHolders.put('Component3317', Component3317);
			query = new SkyEditor2.Query('CLOVER__c');
			query.addFieldAsOutput('Name');
			query.addFieldAsOutput('AccountName__c');
			query.addFieldAsOutput('URL__c');
			query.addFieldAsOutput('saito__c');
			query.addFieldAsOutput('karute__c');
			query.addWhere('karute__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('Component3317', 'karute__c');
			Component3317.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('Component3317', query);
			registRelatedList('AEGISkarute__r', 'Component3891');
			registRelatedList('CLOVERkarute__r', 'Component3317');
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('Opportunity__c', 'OpportunityId');
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			Component3891.extender = this.extender;
			Component3317.extender = this.extender;
			if (record.Id == null) {
				saveOldValues();
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class Component3891Item extends SkyEditor2.ListItem {
		public AEGIS__c record{get; private set;}
		@TestVisible
		Component3891Item(Component3891 holder, AEGIS__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3891 extends SkyEditor2.ListItemHolder {
		public List<Component3891Item> items{get; private set;}
		@TestVisible
			Component3891(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3891Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3891Item(this, (AEGIS__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	global with sharing class Component3317Item extends SkyEditor2.ListItem {
		public CLOVER__c record{get; private set;}
		@TestVisible
		Component3317Item(Component3317 holder, CLOVER__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3317 extends SkyEditor2.ListItemHolder {
		public List<Component3317Item> items{get; private set;}
		@TestVisible
			Component3317(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3317Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3317Item(this, (CLOVER__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}