global with sharing class Requestdecision extends SkyEditor2.SkyEditorPageBaseWithSharing{
	
	public teamspirit__AtkApply__c record{get;set;}
	public Component3 Component3 {get; private set;}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	public SkyEditor2__SkyEditorDummy__c Component5{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component23{get;set;}
	{
	setApiVersion(31.0);
	}
	public Requestdecision(ApexPages.StandardController controller){
		super(controller);

		SObjectField f;

		f = teamspirit__AtkApply__c.fields.Name;
		f = teamspirit__AtkApply__c.fields.AccountName__c;
		f = teamspirit__AtkApply__c.fields.OrderProduct__c;
		f = teamspirit__AtkApply__c.fields.teamspirit__Status__c;
		f = teamspirit__AtkApply__c.fields.submitdate__c;
		f = teamspirit__AtkApply__c.fields.OwnerId;

		try {
			mainRecord = null;
			mainSObjectType = teamspirit__AtkApply__c.SObjectType;
			mode = SkyEditor2.LayoutMode.TempProductLookup_01;
			
			Component5 = new SkyEditor2__SkyEditorDummy__c();
			Component23 = new SkyEditor2__SkyEditorDummy__c();
			
			queryMap.put(
				'Component3',
				new SkyEditor2.Query('teamspirit__AtkApply__c')
					.addFieldAsOutput('Name')
					.addFieldAsOutput('AccountName__c')
					.addFieldAsOutput('OrderProduct__c')
					.addFieldAsOutput('teamspirit__Status__c')
					.addFieldAsOutput('submitdate__c')
					.addFieldAsOutput('OwnerId')
					.addField('Name')
					.limitRecords(500)
					.addListener(new SkyEditor2.QueryWhereRegister(Component5, 'SkyEditor2__Text__c', 'Name', new SkyEditor2.TextHolder('co'), false, true, false))
					.addListener(new SkyEditor2.QueryWhereRegister(Component23, 'SkyEditor2__Text__c', 'AccountName__c', new SkyEditor2.TextHolder('co'), false, true, false))
					 .addWhere(' ( Name like \'%外注%\')')
			);
			
			Component3 = new Component3(new List<teamspirit__AtkApply__c>(), new List<Component3Item>(), new List<teamspirit__AtkApply__c>(), null);
			listItemHolders.put('Component3', Component3);
			Component3.ignoredOnSave = true;
			
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(teamspirit__AtkApply__c.SObjectType);
			
			p_showHeader = false;
			p_sidebar = false;
			presetSystemParams();
			initSearch();
			
		} catch (SkyEditor2.Errors.SObjectNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.Errors.FieldNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.ExtenderException e){
			e.setMessagesToPage();
		} catch (SkyEditor2.Errors.PricebookNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
			hidePageBody = true;
		}
	}

	public List<SelectOption> getOperatorOptions_teamspirit_AtkApply_c_Name() {
		return getOperatorOptions('teamspirit__AtkApply__c', 'Name');
	}
	public List<SelectOption> getOperatorOptions_teamspirit_AtkApply_c_AccountName_c() {
		return getOperatorOptions('teamspirit__AtkApply__c', 'AccountName__c');
	}
	
	
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public teamspirit__AtkApply__c record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, teamspirit__AtkApply__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.ListItemHolder {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (teamspirit__AtkApply__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	
}