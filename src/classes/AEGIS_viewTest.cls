@isTest
private with sharing class AEGIS_viewTest{
	private static testMethod void testPageMethods() {		AEGIS_view extension = new AEGIS_view(new ApexPages.StandardController(new AEGIS__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testComponent115() {
		AEGIS_view.Component115 Component115 = new AEGIS_view.Component115(new List<Landing__c>(), new List<AEGIS_view.Component115Item>(), new List<Landing__c>(), null);
		Component115.create(new Landing__c());
		System.assert(true);
	}
	
	private static testMethod void testComponent50() {
		AEGIS_view.Component50 Component50 = new AEGIS_view.Component50(new List<AEGIS2__c>(), new List<AEGIS_view.Component50Item>(), new List<AEGIS2__c>(), null);
		Component50.create(new AEGIS2__c());
		System.assert(true);
	}
	
}