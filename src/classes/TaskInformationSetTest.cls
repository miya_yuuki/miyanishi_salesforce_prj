@isTest
private class TaskInformationSetTest {
    static testMethod void myUnitTest() {
        Test.startTest();

        // リード
        Lead testLead = new Lead(
            OwnerId = Userinfo.getUserId(),
            Company = 'テスト会社名',
            lead_AccountKana__c = 'フリガナ',
            Phone = '03-9999-9999',
            Status1__c = '接続済',
            Status2__c = '担当者資料送付(メール)',
            Website = 'http://www.test.com',
            lead_Relation__c = '顧客',
            lead_lead1__c = '架電',
            lead_lead2__c = '架電',
            LastName = 'テスト姓',
            lead_NextContactDay__c = System.today(),
            Lead_WEBsite_Quality__c = 'A',
            WebSiteType__c = '⑤その他',
            SEOMeasureStatus__c = '内製'
        );
        insert testLead;

        // テレアポ
        Task testTask = new Task(
	        TaskSubtype = 'Task',
            WhoId = testLead.Id, // リードID
            Subject = '件名1', // 件名
            ActivityDate = System.today(), // 期日
            OwnerId = Userinfo.getUserId(), // 任命先
            summary_type__c = '電話', // 種別(大)
            detail_type__c = '接続', // 種別(小)
            Status = '未着手', //状況
            Priority = '4（重要）' // 優先度
        );
        insert testTask;

    }
}