global with sharing class LEX_karuteHistory_view extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public karute__c record {get{return (karute__c)mainRecord;}}
	public Component2716 Component2716 {get; private set;}
	{
	setApiVersion(42.0);
	}
	public LEX_karuteHistory_view(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = karute__c.fields.NewRireki__c;
		f = Minutes__c.fields.Name;
		f = Minutes__c.fields.Title__c;
		f = Minutes__c.fields.VisitDate__c;
		f = Minutes__c.fields.OwnerId;
		f = Minutes__c.fields.Customer11__c;
		f = Minutes__c.fields.LastModifiedDate;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = karute__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'LEX_karuteHistory_view';
			mainQuery = new SkyEditor2.Query('karute__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('NewRireki__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			Component2716 = new Component2716(new List<Minutes__c>(), new List<Component2716Item>(), new List<Minutes__c>(), null);
			listItemHolders.put('Component2716', Component2716);
			query = new SkyEditor2.Query('Minutes__c');
			query.addFieldAsOutput('Name');
			query.addFieldAsOutput('Title__c');
			query.addFieldAsOutput('VisitDate__c');
			query.addFieldAsOutput('OwnerId');
			query.addFieldAsOutput('Customer11__c');
			query.addFieldAsOutput('LastModifiedDate');
			query.addFieldAsOutput('RecordTypeId');
			query.addWhere('karute__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('Component2716', 'karute__c');
			Component2716.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('Component2716', query);
			Component2716.ignoredOnSave = true;
			registRelatedList('MinutesKarute__r', 'Component2716');
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('Opportunity__c', 'OpportunityId');
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			Component2716.extender = this.extender;
			if (record.Id == null) {
				saveOldValues();
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class Component2716Item extends SkyEditor2.ListItem {
		public Minutes__c record{get; private set;}
		@TestVisible
		Component2716Item(Component2716 holder, Minutes__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component2716 extends SkyEditor2.ListItemHolder {
		public List<Component2716Item> items{get; private set;}
		@TestVisible
			Component2716(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component2716Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component2716Item(this, (Minutes__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}