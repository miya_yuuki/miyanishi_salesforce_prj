@isTest
private with sharing class BillProductSearchTest{
		private static testMethod void testPageMethods() {	
			BillProductSearch page = new BillProductSearch(new ApexPages.StandardController(new BillProduct__c()));	
			page.getOperatorOptions_BillProduct_c_TextManagement2_c();	
			page.getOperatorOptions_BillProduct_c_Bill2_c();	
			page.getOperatorOptions_BillProduct_c_Name();	
			page.getOperatorOptions_BillProduct_c_BillTo_Account_c();	
			page.getOperatorOptions_BillProduct_c_Bill2_Product_c();	
			page.getOperatorOptions_BillProduct_c_Bill2_SalesPerson_c();	
			page.getOperatorOptions_BillProduct_c_BillingTiming_c();	
			page.getOperatorOptions_BillProduct_c_Bill2_DeliveryanExistence_c();	
			page.getOperatorOptions_BillProduct_c_BillProductCancel_c();	
			page.getOperatorOptions_BillProduct_c_SalesCheck_c();	
			page.getOperatorOptions_BillProduct_c_Bill_TargetDay_c();	
			page.getOperatorOptions_BillProduct_c_Bill2_PaymentDueDate_c();	
			System.assert(true);
		}	
			
	private static testMethod void testComponent3() {
		BillProductSearch.Component3 Component3 = new BillProductSearch.Component3(new List<BillProduct__c>(), new List<BillProductSearch.Component3Item>(), new List<BillProduct__c>(), null);
		Component3.create(new BillProduct__c());
		Component3.doDeleteSelectedItems();
		System.assert(true);
	}
	
	@isTest
	private static void testLightDataTables(){

		System.assert(true);
	}
}