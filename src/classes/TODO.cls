global with sharing class TODO extends SkyEditor2.SkyEditorPageBaseWithSharing {	
	    	
	    public Task record{get;set;}	
	    	
    
	    public Component2 Component2 {get; private set;}	
	    	
	    public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
	    	
	    public SkyEditor2__SkyEditorDummy__c Component47_from{get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component47_to{get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component47_isNull{get;set;}	
	    public SkyEditor2.TextHolder.OperatorHolder Component47_isNull_op{get;set;}	
	    	
	    public Task Component19_val {get;set;}	
        public SkyEditor2.TextHolder Component19_op{get;set;}	
	    	
	    public Task Component13_val {get;set;}	
        public SkyEditor2.TextHolder Component13_op{get;set;}	
        public List<SelectOption> valueOptions_Task_Status {get;set;}
	    	
	    public Task Component15_val {get;set;}	
        public SkyEditor2.TextHolder Component15_op{get;set;}	
        public List<SelectOption> valueOptions_Task_Priority {get;set;}
	    	
    public String recordTypeRecordsJSON_Task {get; private set;}
    public String defaultRecordTypeId_Task {get; private set;}
    public String metadataJSON_Task {get; private set;}
    public String picklistValuesJSON_Task_Status {get; private set;}
    public String picklistValuesJSON_Task_Priority {get; private set;}
    public String picklistValuesJSON_Task_CallType {get; private set;}
    public String picklistValuesJSON_Task_RecurrenceTimeZoneSidKey {get; private set;}
    public String picklistValuesJSON_Task_RecurrenceType {get; private set;}
    public String picklistValuesJSON_Task_RecurrenceInstance {get; private set;}
    public String picklistValuesJSON_Task_RecurrenceMonthOfYear {get; private set;}
    public String picklistValuesJSON_Task_event_houmon_c {get; private set;}
    public String picklistValuesJSON_Task_call_c {get; private set;}
    public String picklistValuesJSON_Task_Type_c {get; private set;}
    public String picklistValuesJSON_Task_time_c {get; private set;}
	    public TODO(ApexPages.StandardController controller) {	
	        super(controller);	

            SObjectField f;

            f = Task.fields.OwnerId;
            f = Task.fields.Status;
            f = Task.fields.Priority;
            f = Task.fields.AccountId;
            f = Task.fields.WhatId;
            f = Task.fields.Subject;
            f = Task.fields.ActivityDate;
            f = Task.fields.call__c;
            f = Task.fields.Description;

        List<RecordTypeInfo> recordTypes;
	        try {	
	            	
	            mainRecord = null;	
	            mainSObjectType = Task.SObjectType;	
	            	
	            	
	            mode = SkyEditor2.LayoutMode.TempSearch_01; 
	            	
	            Component47_from = new SkyEditor2__SkyEditorDummy__c();	
	            Component47_to = new SkyEditor2__SkyEditorDummy__c();	
	            Component47_isNull = new SkyEditor2__SkyEditorDummy__c();	
	            Component47_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
	            	
	            Task lookupObjComponent37 = new Task();	
	            Component19_val = lookupObjComponent37;	
	            Component19_op = new SkyEditor2.TextHolder();	
	            	
	            Component13_val = new Task();	
	            Component13_op = new SkyEditor2.TextHolder();	
	            valueOptions_Task_Status = new List<SelectOption>{
	                new SelectOption('', Label.none)
	            };
	            for (PicklistEntry e : Task.Status.getDescribe().getPicklistValues()) {
	                if (e.isActive()) {
	                    valueOptions_Task_Status.add(new SelectOption(e.getValue(), e.getLabel()));
	                }
	            }
	            	
	            Component15_val = new Task();	
	            Component15_op = new SkyEditor2.TextHolder();	
	            valueOptions_Task_Priority = new List<SelectOption>{
	                new SelectOption('', Label.none)
	            };
	            for (PicklistEntry e : Task.Priority.getDescribe().getPicklistValues()) {
	                if (e.isActive()) {
	                    valueOptions_Task_Priority.add(new SelectOption(e.getValue(), e.getLabel()));
	                }
	            }
	            	
	            queryMap.put(	
	                'Component2',	
	                new SkyEditor2.Query('Task')	
	                    .addFieldAsOutput('OwnerId')
	                    .addFieldAsOutput('AccountId')
	                    .addFieldAsOutput('WhatId')
	                    .addFieldAsOutput('Subject')
	                    .addFieldAsOutput('Priority')
	                    .addFieldAsOutput('Status')
	                    .addFieldAsOutput('ActivityDate')
	                    .addFieldAsOutput('call__c')
	                    .addFieldAsOutput('Description')
                        .addFieldAsOutput('RecordTypeId')
	                    .limitRecords(500)	
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component47_from, 'SkyEditor2__Date__c', 'ActivityDate', new SkyEditor2.TextHolder('ge'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component47_to, 'SkyEditor2__Date__c', 'ActivityDate', new SkyEditor2.TextHolder('le'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component47_isNull, 'SkyEditor2__Date__c', 'ActivityDate', Component47_isNull_op, true,0,false )) 
	                    
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component19_val, 'OwnerId', 'OwnerId', Component19_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component13_val, 'Status', 'Status', Component13_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component15_val, 'Priority', 'Priority', Component15_op, true, 0, false ))
	            );	
	            	
	                Component2 = new Component2(new List<Task>(), new List<Component2Item>(), new List<Task>(), null);
	            listItemHolders.put('Component2', Component2);	
	            	
	            	
	            recordTypeSelector = new SkyEditor2.RecordTypeSelector(Task.SObjectType, true);
	            	
	            	
            p_showHeader = true;
            p_sidebar = false;
            presetSystemParams();
            Component2.extender = this.extender;
	        } catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
	            SkyEditor2.Messages.addErrorMessage(e.getMessage());
	        } catch (SkyEditor2.Errors.FieldNotFoundException e) {	
	            SkyEditor2.Messages.addErrorMessage(e.getMessage());
            } catch (SkyEditor2.ExtenderException e) {                 e.setMessagesToPage();
	        } catch (Exception e) {	
	            System.Debug(LoggingLevel.Error, e);	
	            SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
	        }	
	    }	
	    	
        public List<SelectOption> getOperatorOptions_Task_OwnerId() { 
            return getOperatorOptions('Task', 'OwnerId');	
	    }	
        public List<SelectOption> getOperatorOptions_Task_Status() { 
            return getOperatorOptions('Task', 'Status');	
	    }	
        public List<SelectOption> getOperatorOptions_Task_Priority() { 
            return getOperatorOptions('Task', 'Priority');	
	    }	
	    	
	    private static testMethod void testPageMethods() {	
	        TODO page = new TODO(new ApexPages.StandardController(new Task()));	
	        page.getOperatorOptions_Task_OwnerId();	
	        page.getOperatorOptions_Task_Status();	
	        page.getOperatorOptions_Task_Priority();	
            System.assert(true);
	    }	
	    	
	    	
    global with sharing class Component2Item extends SkyEditor2.ListItem {
        public Task record{get; private set;}
        Component2Item(Component2 holder, Task record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(holder);
            if (record.Id == null  && record.RecordTypeId == null){
                if (recordTypeSelector != null) {
                    recordTypeSelector.applyDefault(record);
                }
                
            }
            this.record = record;
        }
        global override SObject getRecord() {return record;}
        public void doDeleteItem(){deleteItem();}
    }
    global with sharing  class Component2 extends SkyEditor2.ListItemHolder {
        public List<Component2Item> items{get; private set;}
        Component2(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(records, items, deleteRecords, recordTypeSelector);
            this.items = (List<Component2Item>)items;
        }
        global override SkyEditor2.ListItem create(SObject data) {
            return new Component2Item(this, (Task)data, recordTypeSelector);
        }
        public void doDeleteSelectedItems(){deleteSelectedItems();}
    }
    private static testMethod void testComponent2() {
        Component2 Component2 = new Component2(new List<Task>(), new List<Component2Item>(), new List<Task>(), null);
        Component2.create(new Task());
        Component2.doDeleteSelectedItems();
        System.assert(true);
    }
    

	    	
	}