@isTest
public with sharing class PaymentCalculationClassTest {
	private static testMethod void testMethods() 
	{
		// 入力チェックを停止
		User stopCheck = new User(Id = Userinfo.getUserId(), StopInputCheck__c = true);
		update stopCheck;
		
		List<Account> testAccounts = new List<Account>();
		
		// 取引先
		Account testAccount = new Account(
			Name = 'テスト株式会社',
			account_localName__c = 'テストフリガナ',
			account_Lead__c = '架電',
			account_Lead2__c = '架電',
			Type = '顧客'
		);
		testAccounts.add(testAccount);
		
		// 取引先（パートナー）
		Account testPartnerAccount = new Account(
			Name = 'テストパートナー株式会社',
			account_localName__c = 'テストパートナーフリガナ',
			account_Lead__c = '架電',
			account_Lead2__c = '架電',
			Type = '顧客'
		);
		testAccounts.add(testPartnerAccount);
		
		insert testAccounts;
		
		// 取引先責任者
		Contact testContact = new Contact(
			FirstName = 'テスト姓',
			LastName = 'テスト名',
			AccountId = testAccount.Id,
			report__c = '送信不要'
		);
		insert testContact;
		
		// 商談
		Opportunity testOpp = new Opportunity(
			Name = 'テスト商談',
			OwnerId = Userinfo.getUserId(),
			AccountId = testAccount.Id,
			Opportunity_Title__c = testContact.Id,
			Opportunity_Type__c = 'セールスP（クライアント請求/初回期間支払）',
			Opportunity_Partner__c = testPartnerAccount.Id,
			Opportunity_lead1__c = '架電',
			Opportunity_lead2__c = '架電',
			CloseDate = System.today().addDays(5),
			StageName = 'D（検討段階）',
			Opportunity_BillEmail__c = 'test@test.com',
			Opportunity_BillRole__c = '役割',
			Opportunity_BillTitle__c = '役職',
			Opportunity_BillPostalCode__c = '999-9999',
			Opportunity_BillPrefecture__c = '都道府県',
			Opportunity_BillPrefecture2__c = '都道府県',
			Opportunity_BillPhone__c = '03-999-9999',
			Opportunity_BillAddress__c = '住所',
			Opportunity_BillCity__c = '市区群'
		);
		insert testOpp;
		
		// 契約
		Contract testContract = new Contract(
			AccountId = testAccount.Id,
			Opportunity__c = testOpp.Id,
			ContractType__c = 'セールスP（クライアント請求/初回期間支払）',
			Contract_Patnername__c = testPartnerAccount.Id,
			Phase__c = '契約中',
			Status = 'ドラフト',
			StartDate = System.today().addDays(5),
			ContractTerm = 6
		);
		insert testContract;
		
		// 支払いサイト
		PaymentSite__c testPayment = new PaymentSite__c(
			Name = '月末締め翌々月末払い',
			fast__c = '25日締め',
			second__c = '翌々月',
			Third__c = '末払い'
		);
		insert testPayment;
		
		// 注文
		Order__c testOrder = new Order__c(
			Order_Relation__c = testContract.Id,
			Order_Phase__c = '契約中',
			Order_PaymentMethod__c = '振り込み【通常】',
			Order_PaymentSiteMaster__c = testPayment.Id
		);
		insert testOrder;

		
		// 商品
		Product__c testProduct = new Product__c(
			Name = 'SEO対策×6ヵ月',
			whet__c = 1,
			Classification__c = 'ストック',
			ClassificationSales__c = 'ストック',
			Category__c = 'ソリューション',
			validity__c = true,
			Update__c = '有',
			Product_DeliveryExistence__c = '有'
		);
		insert testProduct;
		
		// 注文商品
		OrderItem__c testOrderItem1 = new OrderItem__c(
			Item2_Relation__c = testOrder.Id,
			Item2_Product__c = testProduct.Id,
			Item2_Keyword_phase__c = '契約中',
			BillingTiming__c = '受注',
			ContractMonths__c = 6,
			Quantity__c = 100,
			Item2_CancelCount__c = 50,
			ServiceDate__c = Date.valueOf('2015-04-01'),
			Item2_Delivery_date__c = System.today()
		);
		insert testOrderItem1;
		
		// 注文商品
		OrderItem__c testOrderItem2 = new OrderItem__c(
			Item2_Relation__c = testOrder.Id,
			Item2_Keyword_phase__c = '契約中',
			BillingTiming__c = '受注',
			ContractMonths__c = 6,
			Item2_Product__c = testProduct.Id,
			Quantity__c = 100,
			Item2_CancelCount__c = 50,
			ServiceDate__c = Date.valueOf('2015-04-01'),
			Item2_Delivery_date__c = System.today()
		);
		insert testOrderItem2;
		
		// 納品履歴
		TextManagement2__c testTextManagement1 = new TextManagement2__c(
			Item2_OrderProduct__c = testOrderItem1.Id,
			DeliveryDay__c = System.today(),
			DeliveryCount__c = 5
		);
		insert testTextManagement1;
	}
}