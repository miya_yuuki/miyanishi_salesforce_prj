public class AccountWebSiteDuplicationCheck {
	public static void prcAccountWebSiteDuplicationCheckClass(List<AccountWebSite__c> newObjs, Map<Id,AccountWebSite__c> oldMaps, Boolean isInsert) {
    	// 取引先Webサイトの重複確認
        List<String> accountWebSiteNames = new List<String>();
        List<Id> accountWebSiteIds = new List<Id>();
        if (isInsert) {
            for (AccountWebSite__c newObj : newObjs) {
                accountWebSiteNames.add(newObj.Name);
                accountWebSiteIds.add(newObj.Id);
            }
        } else {
            AccountWebSite__c oldObj = new AccountWebSite__c();
            for (AccountWebSite__c newObj : newObjs) {
                oldObj = oldMaps.get(newObj.Id);
                if (newObj.Name != oldObj.Name) {
                    accountWebSiteNames.add(newObj.Name);
                    accountWebSiteIds.add(newObj.Id);
                }
            }
        }
        if (accountWebSiteNames.size() > 0) {
            List<AccountWebSite__c> duplicationObjs = new List<AccountWebSite__c>();
            duplicationObjs = [SELECT Id FROM AccountWebSite__c WHERE Name IN :accountWebSiteNames AND Id NOT IN :accountWebSiteIds];
            if (duplicationObjs.size() > 0) {
                for (AccountWebSite__c errorObj : newObjs) {
                    errorObj.addError('重複した取引先Webサイトが存在しています。既存の取引先Webサイトを使用してください。');
                }
            }
        }
    }
}