@isTest
private with sharing class KijiEstimatesSearch2Test{
		private static testMethod void testPageMethods() {	
			KijiEstimatesSearch2 page = new KijiEstimatesSearch2(new ApexPages.StandardController(new kijimitumori__c()));	
			page.getOperatorOptions_kijimitumori_c_AccountName_c();	
			page.getOperatorOptions_kijimitumori_c_ProductItemName_c();	
			page.getOperatorOptions_kijimitumori_c_UnitSalesPerson_c();	
			page.getOperatorOptions_kijimitumori_c_OpportunityProduct_c();	
			page.getOperatorOptions_kijimitumori_c_EstimatedAmount_c();	
			page.getOperatorOptions_kijimitumori_c_Status_c();	
			page.getOperatorOptions_kijimitumori_c_Text_Accep_TedNorm_c();	
			page.getOperatorOptions_kijimitumori_c_DeliveryCount_c();	
			System.assert(true);
		}	
			
	private static testMethod void testComponent3() {
		KijiEstimatesSearch2.Component3 Component3 = new KijiEstimatesSearch2.Component3(new List<kijimitumori__c>(), new List<KijiEstimatesSearch2.Component3Item>(), new List<kijimitumori__c>(), null);
		Component3.create(new kijimitumori__c());
		Component3.doDeleteSelectedItems();
		System.assert(true);
	}
	
}