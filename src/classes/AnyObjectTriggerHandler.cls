public class AnyObjectTriggerHandler {

    public static void handleAfterUpdate(List<String> recordList) {
        if (Notice2SlackController.firstRun) {  
            for ( String msg : recordList) {
                System.debug(LoggingLevel.INFO, 'target case:' + msg);
    
                Map<String, String> request = new Map<String, String> {
                    'channel' => '#z_sf-notifications',
                    'username' => 'Salesforce',
                    'icon_emoji' => ':salesforce:',
                    'text'  => msg
                };
                Notice2SlackController.createMessage(request);
            }
		    Notice2SlackController.firstRun = false;
        }
    }
}