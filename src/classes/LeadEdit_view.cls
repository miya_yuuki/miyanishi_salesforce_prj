global with sharing class LeadEdit_view extends SkyEditor2.SkyEditorPageBaseWithSharing {
    
    public Lead record {get{return (Lead)mainRecord;}}
    public with sharing class CanvasException extends Exception {}

    
    
    public LeadEdit_view(ApexPages.StandardController controller) {
        super(controller);


        SObjectField f;

        f = Lead.fields.OwnerId;
        f = Lead.fields.Company;
        f = Lead.fields.LastName;
        f = Lead.fields.FirstName;
        f = Lead.fields.lead_kana__c;
        f = Lead.fields.lead_Division__c;
        f = Lead.fields.Title;
        f = Lead.fields.Email;
        f = Lead.fields.Status1__c;
        f = Lead.fields.Status2__c;
        f = Lead.fields.lead_NGmemo__c;
        f = Lead.fields.Website;
        f = Lead.fields.Phone;
        f = Lead.fields.lead_Relation__c;
        f = Lead.fields.lead_lead1__c;
        f = Lead.fields.lead_lead2__c;
        f = Lead.fields.lead_Timing1__c;
        f = Lead.fields.PostalCode;
        f = Lead.fields.State;
        f = Lead.fields.City;
        f = Lead.fields.Street;
        f = Lead.fields.Description;
        f = Lead.fields.lead_BusinessContent__c;
        f = Lead.fields.lead_BusinessModel__c;
        f = Lead.fields.lead_ContactBackground__c;
        f = Lead.fields.lead_Schedule__c;
        f = Lead.fields.lead_Interest__c;
        f = Lead.fields.lead_ServiceWEBSite__c;
        f = Lead.fields.lead_Budget__c;
        f = Lead.fields.lead_PromotionContents__c;
        f = Lead.fields.lead_hope__c;

        List<RecordTypeInfo> recordTypes;
        try {
            mainSObjectType = Lead.SObjectType;
            setPageReferenceFactory(new PageReferenceFactory());
            
            mainQuery = new SkyEditor2.Query('Lead');
            mainQuery.addFieldAsOutput('Name');
            mainQuery.addFieldAsOutput('RecordTypeId');
            mainQuery.addFieldAsOutput('OwnerId');
            mainQuery.addFieldAsOutput('Company');
            mainQuery.addFieldAsOutput('LastName');
            mainQuery.addFieldAsOutput('FirstName');
            mainQuery.addFieldAsOutput('lead_kana__c');
            mainQuery.addFieldAsOutput('lead_Division__c');
            mainQuery.addFieldAsOutput('Title');
            mainQuery.addFieldAsOutput('Email');
            mainQuery.addFieldAsOutput('Status1__c');
            mainQuery.addFieldAsOutput('Status2__c');
            mainQuery.addFieldAsOutput('lead_NGmemo__c');
            mainQuery.addFieldAsOutput('Website');
            mainQuery.addFieldAsOutput('Phone');
            mainQuery.addFieldAsOutput('lead_Relation__c');
            mainQuery.addFieldAsOutput('lead_lead1__c');
            mainQuery.addFieldAsOutput('lead_lead2__c');
            mainQuery.addFieldAsOutput('lead_Timing1__c');
            mainQuery.addFieldAsOutput('PostalCode');
            mainQuery.addFieldAsOutput('State');
            mainQuery.addFieldAsOutput('City');
            mainQuery.addFieldAsOutput('Street');
            mainQuery.addFieldAsOutput('Description');
            mainQuery.addFieldAsOutput('lead_BusinessContent__c');
            mainQuery.addFieldAsOutput('lead_BusinessModel__c');
            mainQuery.addFieldAsOutput('lead_ContactBackground__c');
            mainQuery.addFieldAsOutput('lead_Schedule__c');
            mainQuery.addFieldAsOutput('lead_Interest__c');
            mainQuery.addFieldAsOutput('lead_ServiceWEBSite__c');
            mainQuery.addFieldAsOutput('lead_Budget__c');
            mainQuery.addFieldAsOutput('lead_PromotionContents__c');
            mainQuery.addFieldAsOutput('lead_hope__c');
            mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
                .limitRecords(1);
            
            
            
            mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
            
            queryMap = new Map<String, SkyEditor2.Query>();
            SkyEditor2.Query query;
            
            
            p_showHeader = true;
            p_sidebar = true;
            addInheritParameter('RecordTypeId', 'RecordType');
            init();
            
            if (record.Id == null) {
                
                saveOldValues();
                
            }

            
            
        }  catch (SkyEditor2.Errors.FieldNotFoundException e) {
            fieldNotFound(e);
        } catch (SkyEditor2.Errors.RecordNotFoundException e) {
            recordNotFound(e);
        } catch (SkyEditor2.ExtenderException e) {
            e.setMessagesToPage();
        }
    }
    

    private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    
    private static testMethod void testPageMethods() {        LeadEdit_view extension = new LeadEdit_view(new ApexPages.StandardController(new Lead()));
        SkyEditor2.Messages.clear();
        extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
        SkyEditor2.Messages.clear();
        extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
        SkyEditor2.Messages.clear();
        extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

        Integer defaultSize;
    }
    with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
        public PageReference newPageReference(String url) {
            return new PageReference(url);
        }
    }
}