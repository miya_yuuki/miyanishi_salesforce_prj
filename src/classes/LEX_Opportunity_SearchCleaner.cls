global with sharing class LEX_Opportunity_SearchCleaner extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public OpportunityProduct__c record {get{return (OpportunityProduct__c)mainRecord;}}
	{
	setApiVersion(42.0);
	}
	public LEX_Opportunity_SearchCleaner(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = OpportunityProduct__c.fields.Account__c;
		f = OpportunityProduct__c.fields.sisakuompany__c;
		f = OpportunityProduct__c.fields.Opportunity__c;
		f = OpportunityProduct__c.fields.EditOpportunity__c;
		f = OpportunityProduct__c.fields.Product__c;
		f = OpportunityProduct__c.fields.Item_Detail_Product__c;
		f = OpportunityProduct__c.fields.ProductTypeView__c;
		f = OpportunityProduct__c.fields.Opportunity_DeliveryExistenceView__c;
		f = OpportunityProduct__c.fields.BillingTiming__c;
		f = OpportunityProduct__c.fields.AutomaticUpdate__c;
		f = OpportunityProduct__c.fields.Agreement__c;
		f = OpportunityProduct__c.fields.Price__c;
		f = OpportunityProduct__c.fields.Quantity__c;
		f = OpportunityProduct__c.fields.BuyingupPrice__c;
		f = OpportunityProduct__c.fields.TotalPrice__c;
		f = OpportunityProduct__c.fields.ContractMonths__c;
		f = OpportunityProduct__c.fields.TotalPriceAll__c;
		f = OpportunityProduct__c.fields.Item_Commission_rate__c;
		f = OpportunityProduct__c.fields.SalesPerson__c;
		f = OpportunityProduct__c.fields.Item_Entry_date__c;
		f = OpportunityProduct__c.fields.Item_Contract_start_date__c;
		f = OpportunityProduct__c.fields.Item_Contract_end_date__c;
		f = OpportunityProduct__c.fields.OutsourcingApplication__c;
		f = OpportunityProduct__c.fields.OutsourcingApplicationName__c;
		f = OpportunityProduct__c.fields.Item_Keyword_strategy_keyword__c;
		f = OpportunityProduct__c.fields.Item_Keyword_multiple_strategy_keyword__c;
		f = OpportunityProduct__c.fields.Item_Special_instruction__c;
		f = OpportunityProduct__c.fields.AmountCountDay__c;
		f = OpportunityProduct__c.fields.SalesForecast01__c;
		f = OpportunityProduct__c.fields.gross_profit_view__c;
		f = OpportunityProduct__c.fields.ProfitPercentage__c;
		f = OpportunityProduct__c.fields.IrregularCoefficient__c;
		f = OpportunityProduct__c.fields.SalesPersonMainUnit1__c;
		f = OpportunityProduct__c.fields.SalesPersonMain__c;
		f = OpportunityProduct__c.fields.SalesPercentage1__c;
		f = OpportunityProduct__c.fields.SalesPersonSubUnit__c;
		f = OpportunityProduct__c.fields.SalesPersonSub__c;
		f = OpportunityProduct__c.fields.SalesPercentage2__c;
		f = Product__c.fields.ClassificationSales__c;
		f = Product__c.fields.Product_DeliveryExistence__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = OpportunityProduct__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'LEX_Opportunity_SearchCleaner';
			mainQuery = new SkyEditor2.Query('OpportunityProduct__c');
			mainQuery.addField('Opportunity__c');
			mainQuery.addField('Product__c');
			mainQuery.addField('BillingTiming__c');
			mainQuery.addField('AutomaticUpdate__c');
			mainQuery.addField('Price__c');
			mainQuery.addField('BuyingupPrice__c');
			mainQuery.addField('ContractMonths__c');
			mainQuery.addField('Item_Commission_rate__c');
			mainQuery.addField('Item_Entry_date__c');
			mainQuery.addField('Item_Contract_start_date__c');
			mainQuery.addField('OutsourcingApplication__c');
			mainQuery.addField('OutsourcingApplicationName__c');
			mainQuery.addField('Item_Keyword_strategy_keyword__c');
			mainQuery.addField('Item_Keyword_multiple_strategy_keyword__c');
			mainQuery.addField('Item_Special_instruction__c');
			mainQuery.addField('AmountCountDay__c');
			mainQuery.addField('SalesForecast01__c');
			mainQuery.addField('IrregularCoefficient__c');
			mainQuery.addField('SalesPercentage1__c');
			mainQuery.addField('SalesPersonSub__c');
			mainQuery.addField('SalesPercentage2__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Account__c');
			mainQuery.addFieldAsOutput('sisakuompany__c');
			mainQuery.addFieldAsOutput('EditOpportunity__c');
			mainQuery.addFieldAsOutput('Item_Detail_Product__c');
			mainQuery.addFieldAsOutput('ProductTypeView__c');
			mainQuery.addFieldAsOutput('Opportunity_DeliveryExistenceView__c');
			mainQuery.addFieldAsOutput('Agreement__c');
			mainQuery.addFieldAsOutput('Quantity__c');
			mainQuery.addFieldAsOutput('TotalPrice__c');
			mainQuery.addFieldAsOutput('TotalPriceAll__c');
			mainQuery.addFieldAsOutput('SalesPerson__c');
			mainQuery.addFieldAsOutput('Item_Contract_end_date__c');
			mainQuery.addFieldAsOutput('gross_profit_view__c');
			mainQuery.addFieldAsOutput('ProfitPercentage__c');
			mainQuery.addFieldAsOutput('SalesPersonMainUnit1__c');
			mainQuery.addFieldAsOutput('SalesPersonMain__c');
			mainQuery.addFieldAsOutput('SalesPersonSubUnit__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('Opportunity__c', 'CF00N10000005jJYH_lkid');
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			if (record.Id == null) {
				saveOldValues();
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}


	public void loadReferenceValues_Component6() {
		if (record.Product__c == null) {
if (SkyEditor2.Util.isEditable(record, OpportunityProduct__c.fields.ProductTypeView__c)) {
				record.ProductTypeView__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, OpportunityProduct__c.fields.Opportunity_DeliveryExistenceView__c)) {
				record.Opportunity_DeliveryExistenceView__c = null;
			}
				return;
		}
		Product__c[] referenceTo = [SELECT ClassificationSales__c,Product_DeliveryExistence__c FROM Product__c WHERE Id=:record.Product__c];
		if (referenceTo.size() == 0) {
			record.Product__c.addError(SkyEditor2.Messages.referenceDataNotFound(record.Product__c));
			return;
		}
		if (SkyEditor2.Util.isEditable(record, OpportunityProduct__c.fields.ProductTypeView__c)) {
			record.ProductTypeView__c = referenceTo[0].ClassificationSales__c;
		}
		if (SkyEditor2.Util.isEditable(record, OpportunityProduct__c.fields.Opportunity_DeliveryExistenceView__c)) {
			record.Opportunity_DeliveryExistenceView__c = referenceTo[0].Product_DeliveryExistence__c;
		}
		
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}