public with sharing class OpportunityPhaseCheck {
	public static boolean firstRun = true;
	
	public static void prcOpportunityPhaseCheck(List<Opportunity> objOpportunity, Map<Id, Opportunity> oldMap, Boolean upd) {
		// 商談ID
		List<String> strOpportunityIds = new List<String>();
		// 商談オブジェクト
		List<Opportunity> updOpportunity = new List<Opportunity>();
		
		
		// 更新対象のチェック
		for (Opportunity obj : objOpportunity) {
			if (prcCheckDistribution(upd, obj, oldMap) == null) {
				continue;
			}
			else {
				strOpportunityIds.add(obj.Id);
			}
		}
		
		updOpportunity = [SELECT Id, StageName, PhaseA__c, PhaseB__c, PhaseC__c, PhaseD__c, PhaseLost__c, PhaseWon__c FROM Opportunity WHERE Id IN :strOpportunityIds];
		// 商談を更新
		for (Opportunity objNew : updOpportunity) {
			if (objNew.PhaseD__c == null && (objNew.StageName == 'D（検討段階）' || objNew.StageName == 'D+（担当者内諾条件付）')) {
				objNew.PhaseD__c = date.TODAY();
			}
			if (objNew.PhaseC__c == null && objNew.StageName == 'C（担当者内諾）') {
				objNew.PhaseC__c = date.TODAY();
				if (objNew.PhaseD__c == null) {
					objNew.PhaseD__c = date.TODAY();
				}
			}
			if (objNew.PhaseB__c == null && objNew.StageName == 'B（決済者内諾）') {
				objNew.PhaseB__c = date.TODAY();
				if (objNew.PhaseD__c == null) {
					objNew.PhaseD__c = date.TODAY();
				}
				if (objNew.PhaseC__c == null) {
					objNew.PhaseC__c = date.TODAY();
				}
			}
			if (objNew.PhaseA__c == null && objNew.StageName == 'A（申込書回収日決定）') {
				objNew.PhaseA__c = date.TODAY();
				if (objNew.PhaseD__c == null) {
					objNew.PhaseD__c = date.TODAY();
				}
				if (objNew.PhaseC__c == null) {
					objNew.PhaseC__c = date.TODAY();
				}
				if (objNew.PhaseB__c == null) {
					objNew.PhaseB__c = date.TODAY();
				}
			}
			if (objNew.PhaseLost__c == null && objNew.StageName == 'デッド') {
				objNew.PhaseLost__c = date.TODAY();
			}
			if (objNew.PhaseWon__c == null && objNew.StageName == '受注') {
				objNew.PhaseWon__c = date.TODAY();
			}
		}
		if (updOpportunity.size() > 0) {
			update updOpportunity;
		}
	}
	
	private static Boolean prcCheckDistribution (Boolean upd, Opportunity objNew, Map<Id, Opportunity> oldMap) {
		if (upd) {
			// 古い値を取得
			Opportunity objOld = oldMap.get(objNew.Id);
			// 商談フェーズが更新された場合
			if (objNew.StageName != objOld.StageName && (objNew.StageName == 'D（検討段階）' || objNew.StageName == 'D+（担当者内諾条件付）' || objNew.StageName == 'C（担当者内諾）' || objNew.StageName == 'B（決済者内諾）' || objNew.StageName == 'A（申込書回収日決定）' || objNew.StageName == 'デッド' || objNew.StageName == '受注')) {
				System.debug('商談フェーズ更新検知 : ' + objNew.Id + ' : ' + objNew.StageName);
				return true;
			}
		}
		// 条件に合致しない
		return null;
	}
}