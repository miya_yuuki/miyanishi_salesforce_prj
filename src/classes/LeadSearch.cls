global with sharing class LeadSearch extends SkyEditor2.SkyEditorPageBaseWithSharing {	
	    	
	    public Lead record{get;set;}	
	    	
    
	    public Component2 Component2 {get; private set;}	
	    	
	    public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
	    	
	    public SkyEditor2__SkyEditorDummy__c Component122_from{get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component122_to{get;set;}	
	    	
	    public SkyEditor2__SkyEditorDummy__c Component129_from{get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component129_to{get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component129_isNull{get;set;}	
	    public SkyEditor2.TextHolder.OperatorHolder Component129_isNull_op{get;set;}	
	    	
	    public SkyEditor2__SkyEditorDummy__c Component103_from{get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component103_to{get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component103_isNull{get;set;}	
	    public SkyEditor2.TextHolder.OperatorHolder Component103_isNull_op{get;set;}	
	    	
	    public SkyEditor2__SkyEditorDummy__c Component155_from{get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component155_to{get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component155_isNull{get;set;}	
	    public SkyEditor2.TextHolder.OperatorHolder Component155_isNull_op{get;set;}	
	    	
	    public Lead Component78_val {get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component78_val_dummy {get;set;}	
        public SkyEditor2.TextHolder Component78_op{get;set;}	
        public List<SelectOption> valueOptions_Lead_Status1_c_multi {get;set;}
	    	
	    public Lead Component76_val {get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component76_val_dummy {get;set;}	
        public SkyEditor2.TextHolder Component76_op{get;set;}	
        public List<SelectOption> valueOptions_Lead_Status2_c_multi {get;set;}
	    	
	    public Lead Component98_val {get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component98_val_dummy {get;set;}	
        public SkyEditor2.TextHolder Component98_op{get;set;}	
        public List<SelectOption> valueOptions_Lead_Lead_WEBsite_Quality_c_multi {get;set;}
	    	
	    public SkyEditor2__SkyEditorDummy__c Component15_val {get;set;}	
        public SkyEditor2.TextHolder Component15_op{get;set;}	
	    	
	    public Lead Component71_val {get;set;}	
        public SkyEditor2.TextHolder Component71_op{get;set;}	
	    	
	    public Lead Component142_val {get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component142_val_dummy {get;set;}	
        public SkyEditor2.TextHolder Component142_op{get;set;}	
        public List<SelectOption> valueOptions_Lead_lead_CompanySize_c_multi {get;set;}
	    	
	    public Lead Component133_val {get;set;}	
        public SkyEditor2.TextHolder Component133_op{get;set;}	
	    	
	    public Lead Component135_val {get;set;}	
        public SkyEditor2.TextHolder Component135_op{get;set;}	
	    	
	    public Lead Component137_val {get;set;}	
        public SkyEditor2.TextHolder Component137_op{get;set;}	
	    	
	    public Lead Component139_val {get;set;}	
        public SkyEditor2.TextHolder Component139_op{get;set;}	
	    	
	    public SkyEditor2__SkyEditorDummy__c Component160_val {get;set;}	
        public SkyEditor2.TextHolder Component160_op{get;set;}	
	    	
    public String recordTypeRecordsJSON_Lead {get; private set;}
    public String defaultRecordTypeId_Lead {get; private set;}
    public String metadataJSON_Lead {get; private set;}
    public String picklistValuesJSON_Lead_Salutation {get; private set;}
    public String picklistValuesJSON_Lead_LeadSource {get; private set;}
    public String picklistValuesJSON_Lead_Status {get; private set;}
    public String picklistValuesJSON_Lead_Industry {get; private set;}
    public String picklistValuesJSON_Lead_Rating {get; private set;}
    public String picklistValuesJSON_Lead_lead_ContentsCompetition_c {get; private set;}
    public String picklistValuesJSON_Lead_Contact_Role_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_Industry_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_Industry2_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_Industrycategory_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_Industrycategory2_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_closing_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_BusinessModel_c {get; private set;}
    public String picklistValuesJSON_Lead_Status1_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_lead1_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_lead2_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_ZOHOUser_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_Relation_c {get; private set;}
    public String picklistValuesJSON_Lead_Status2_c {get; private set;}
    public String picklistValuesJSON_Lead_Lead_WEBsite_Quality_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_WebPromotion_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_CompanySize_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_AttackProduct_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_Rival_c {get; private set;}
    public String picklistValuesJSON_Lead_FTP_ana_c {get; private set;}
	    public LeadSearch(ApexPages.StandardController controller) {	
	        super(controller);	

            SObjectField f;

            f = Lead.fields.Status1__c;
            f = Lead.fields.Status2__c;
            f = Lead.fields.Lead_WEBsite_Quality__c;
            f = Lead.fields.Company;
            f = Lead.fields.OwnerId;
            f = Lead.fields.lead_CompanySize__c;
            f = Lead.fields.lead_Rival__c;
            f = Lead.fields.lead_AttackProduct__c;
            f = Lead.fields.lead_ContentsCompetition__c;
            f = Lead.fields.lead_WebPromotion__c;
            f = Lead.fields.tag__c;
            f = Lead.fields.Name;
            f = Lead.fields.Website;
            f = Lead.fields.Phone;
            f = Lead.fields.lead_Timing1__c;
            f = Lead.fields.lead_NextContactDay__c;
            f = Lead.fields.lead_BudgetMonth_Select__c;
            f = Lead.fields.LastModifiedDate;
            f = Lead.fields.lead_BudgetMonth1__c;
            f = Lead.fields.CreatedDate;

        List<RecordTypeInfo> recordTypes;
	        try {	
	            	
	            mainRecord = null;	
	            mainSObjectType = Lead.SObjectType;	
	            	
	            	
	            mode = SkyEditor2.LayoutMode.TempSearch_01; 
	            	
	            Component122_from = new SkyEditor2__SkyEditorDummy__c();	
	            Component122_to = new SkyEditor2__SkyEditorDummy__c();	
	            	
	            Component129_from = new SkyEditor2__SkyEditorDummy__c();	
	            Component129_to = new SkyEditor2__SkyEditorDummy__c();	
	            Component129_isNull = new SkyEditor2__SkyEditorDummy__c();	
	            Component129_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
	            	
	            Component103_from = new SkyEditor2__SkyEditorDummy__c();	
	            Component103_to = new SkyEditor2__SkyEditorDummy__c();	
	            Component103_isNull = new SkyEditor2__SkyEditorDummy__c();	
	            Component103_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
	            	
	            Component155_from = new SkyEditor2__SkyEditorDummy__c();	
	            Component155_to = new SkyEditor2__SkyEditorDummy__c();	
	            Component155_isNull = new SkyEditor2__SkyEditorDummy__c();	
	            Component155_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
	            	
	            Lead lookupObjComponent47 = new Lead();	
	            Component78_val = new Lead();	
	            Component78_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
	            Component78_op = new SkyEditor2.TextHolder();	
	            valueOptions_Lead_Status1_c_multi = new List<SelectOption>{
	                new SelectOption('', Label.none)
	            };
	            for (PicklistEntry e : Lead.Status1__c.getDescribe().getPicklistValues()) {
	                if (e.isActive()) {
	                    valueOptions_Lead_Status1_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
	                }
	            }
	            	
	            Component76_val = new Lead();	
	            Component76_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
	            Component76_op = new SkyEditor2.TextHolder();	
	            valueOptions_Lead_Status2_c_multi = new List<SelectOption>{
	                new SelectOption('', Label.none)
	            };
	            for (PicklistEntry e : Lead.Status2__c.getDescribe().getPicklistValues()) {
	                if (e.isActive()) {
	                    valueOptions_Lead_Status2_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
	                }
	            }
	            	
	            Component98_val = new Lead();	
	            Component98_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
	            Component98_op = new SkyEditor2.TextHolder();	
	            valueOptions_Lead_Lead_WEBsite_Quality_c_multi = new List<SelectOption>{
	                new SelectOption('', Label.none)
	            };
	            for (PicklistEntry e : Lead.Lead_WEBsite_Quality__c.getDescribe().getPicklistValues()) {
	                if (e.isActive()) {
	                    valueOptions_Lead_Lead_WEBsite_Quality_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
	                }
	            }
	            	
	            Component15_val = new SkyEditor2__SkyEditorDummy__c();	
	            Component15_op = new SkyEditor2.TextHolder();	
	            	
	            Component71_val = lookupObjComponent47;	
	            Component71_op = new SkyEditor2.TextHolder('eq');	
	            	
	            Component142_val = new Lead();	
	            Component142_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
	            Component142_op = new SkyEditor2.TextHolder();	
	            valueOptions_Lead_lead_CompanySize_c_multi = new List<SelectOption>{
	                new SelectOption('', Label.none)
	            };
	            for (PicklistEntry e : Lead.lead_CompanySize__c.getDescribe().getPicklistValues()) {
	                if (e.isActive()) {
	                    valueOptions_Lead_lead_CompanySize_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
	                }
	            }
	            	
	            Component133_val = new Lead();	
	            Component133_op = new SkyEditor2.TextHolder();	
	            	
	            Component135_val = new Lead();	
	            Component135_op = new SkyEditor2.TextHolder();	
	            	
	            Component137_val = new Lead();	
	            Component137_op = new SkyEditor2.TextHolder();	
	            	
	            Component139_val = new Lead();	
	            Component139_op = new SkyEditor2.TextHolder();	
	            	
	            Component160_val = new SkyEditor2__SkyEditorDummy__c();	
	            Component160_op = new SkyEditor2.TextHolder();	
	            	
	            queryMap.put(	
	                'Component2',	
	                new SkyEditor2.Query('Lead')	
	                    .addFieldAsOutput('OwnerId')
	                    .addFieldAsOutput('Name')
	                    .addFieldAsOutput('Company')
	                    .addFieldAsOutput('Website')
	                    .addFieldAsOutput('Phone')
	                    .addField('Status1__c')
	                    .addField('Status2__c')
	                    .addFieldAsOutput('lead_AttackProduct__c')
	                    .addFieldAsOutput('lead_Timing1__c')
	                    .addFieldAsOutput('lead_NextContactDay__c')
	                    .addFieldAsOutput('lead_BudgetMonth_Select__c')
	                    .addFieldAsOutput('LastModifiedDate')
                        .addFieldAsOutput('RecordTypeId')
	                    .limitRecords(500)	
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component122_from, 'SkyEditor2__Text__c', 'lead_BudgetMonth1__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component122_to, 'SkyEditor2__Text__c', 'lead_BudgetMonth1__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component129_from, 'SkyEditor2__Date__c', 'lead_Timing1__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component129_to, 'SkyEditor2__Date__c', 'lead_Timing1__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component129_isNull, 'SkyEditor2__Date__c', 'lead_Timing1__c', Component129_isNull_op, true,0,false )) 
	                    
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component103_from, 'SkyEditor2__Date__c', 'lead_NextContactDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component103_to, 'SkyEditor2__Date__c', 'lead_NextContactDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component103_isNull, 'SkyEditor2__Date__c', 'lead_NextContactDay__c', Component103_isNull_op, true,0,false )) 
	                    
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component155_from, 'SkyEditor2__Date__c', 'CreatedDate', new SkyEditor2.TextHolder('ge'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component155_to, 'SkyEditor2__Date__c', 'CreatedDate', new SkyEditor2.TextHolder('le'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component155_isNull, 'SkyEditor2__Date__c', 'CreatedDate', Component155_isNull_op, true,0,false )) 
	                    
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component78_val_dummy, 'SkyEditor2__Text__c','Status1__c', Component78_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component76_val_dummy, 'SkyEditor2__Text__c','Status2__c', Component76_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component98_val_dummy, 'SkyEditor2__Text__c','Lead_WEBsite_Quality__c', Component98_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component15_val, 'SkyEditor2__Text__c', 'Company', Component15_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component71_val, 'OwnerId', 'OwnerId', Component71_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component142_val_dummy, 'SkyEditor2__Text__c','lead_CompanySize__c', Component142_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component133_val, 'lead_Rival__c', 'lead_Rival__c', Component133_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component135_val, 'lead_AttackProduct__c', 'lead_AttackProduct__c', Component135_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component137_val, 'lead_ContentsCompetition__c', 'lead_ContentsCompetition__c', Component137_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component139_val, 'lead_WebPromotion__c', 'lead_WebPromotion__c', Component139_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component160_val, 'SkyEditor2__Text__c', 'tag__c', Component160_op, true, 0, false ))
	            );	
	            	
	                Component2 = new Component2(new List<Lead>(), new List<Component2Item>(), new List<Lead>(), null);
                 Component2.setPageItems(new List<Component2Item>());
                 Component2.setPageSize(50);
	            listItemHolders.put('Component2', Component2);	
	            	
	            	
	            recordTypeSelector = new SkyEditor2.RecordTypeSelector(Lead.SObjectType, true);
	            	
	            	
            p_showHeader = true;
            p_sidebar = false;
            presetSystemParams();
            Component2.extender = this.extender;
	        } catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
	            SkyEditor2.Messages.addErrorMessage(e.getMessage());
	        } catch (SkyEditor2.Errors.FieldNotFoundException e) {	
	            SkyEditor2.Messages.addErrorMessage(e.getMessage());
            } catch (SkyEditor2.ExtenderException e) {                 e.setMessagesToPage();
	        } catch (Exception e) {	
	            System.Debug(LoggingLevel.Error, e);	
	            SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
	        }	
	    }	
	    	
        public List<SelectOption> getOperatorOptions_Lead_Status1_c_multi() { 
            return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	    }	
        public List<SelectOption> getOperatorOptions_Lead_Status2_c_multi() { 
            return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	    }	
        public List<SelectOption> getOperatorOptions_Lead_Lead_WEBsite_Quality_c_multi() { 
            return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	    }	
        public List<SelectOption> getOperatorOptions_Lead_Company() { 
            return getOperatorOptions('Lead', 'Company');	
	    }	
        public List<SelectOption> getOperatorOptions_Lead_OwnerId() { 
            return getOperatorOptions('Lead', 'OwnerId');	
	    }	
        public List<SelectOption> getOperatorOptions_Lead_lead_CompanySize_c_multi() { 
            return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	    }	
        public List<SelectOption> getOperatorOptions_Lead_lead_Rival_c() { 
            return getOperatorOptions('Lead', 'lead_Rival__c');	
	    }	
        public List<SelectOption> getOperatorOptions_Lead_lead_AttackProduct_c() { 
            return getOperatorOptions('Lead', 'lead_AttackProduct__c');	
	    }	
        public List<SelectOption> getOperatorOptions_Lead_lead_ContentsCompetition_c() { 
            return getOperatorOptions('Lead', 'lead_ContentsCompetition__c');	
	    }	
        public List<SelectOption> getOperatorOptions_Lead_lead_WebPromotion_c() { 
            return getOperatorOptions('Lead', 'lead_WebPromotion__c');	
	    }	
        public List<SelectOption> getOperatorOptions_Lead_tag_c() { 
            return getOperatorOptions('Lead', 'tag__c');	
	    }	
	    	
	    private static testMethod void testPageMethods() {	
	        LeadSearch page = new LeadSearch(new ApexPages.StandardController(new Lead()));	
	        page.getOperatorOptions_Lead_Status1_c_multi();	
	        page.getOperatorOptions_Lead_Status2_c_multi();	
	        page.getOperatorOptions_Lead_Lead_WEBsite_Quality_c_multi();	
	        page.getOperatorOptions_Lead_Company();	
	        page.getOperatorOptions_Lead_OwnerId();	
	        page.getOperatorOptions_Lead_lead_CompanySize_c_multi();	
	        page.getOperatorOptions_Lead_lead_Rival_c();	
	        page.getOperatorOptions_Lead_lead_AttackProduct_c();	
	        page.getOperatorOptions_Lead_lead_ContentsCompetition_c();	
	        page.getOperatorOptions_Lead_lead_WebPromotion_c();	
	        page.getOperatorOptions_Lead_tag_c();	
            System.assert(true);
	    }	
	    	
	    	
    global with sharing class Component2Item extends SkyEditor2.ListItem {
        public Lead record{get; private set;}
        Component2Item(Component2 holder, Lead record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(holder);
            if (record.Id == null  && record.RecordTypeId == null){
                if (recordTypeSelector != null) {
                    recordTypeSelector.applyDefault(record);
                }
                
            }
            this.record = record;
        }
        global override SObject getRecord() {return record;}
        public void doDeleteItem(){deleteItem();}
    }
    global with sharing  class Component2 extends SkyEditor2.PagingList {
        public List<Component2Item> items{get; private set;}
        Component2(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(records, items, deleteRecords, recordTypeSelector);
            this.items = (List<Component2Item>)items;
        }
        global override SkyEditor2.ListItem create(SObject data) {
            return new Component2Item(this, (Lead)data, recordTypeSelector);
        }
        public void doDeleteSelectedItems(){deleteSelectedItems();}
        public void doFirst(){first();}
        public void doPrevious(){previous();}
        public void doNext(){next();}
        public void doLast(){last();}
        public void doSort(){sort();}

        public List<Component2Item> getViewItems() {            return (List<Component2Item>) getPageItems();        }
    }
    private static testMethod void testComponent2() {
        Component2 Component2 = new Component2(new List<Lead>(), new List<Component2Item>(), new List<Lead>(), null);
        Component2.setPageItems(new List<Component2Item>());
        Component2.create(new Lead());
        Component2.doDeleteSelectedItems();
        Component2.setPagesize(10);        Component2.doFirst();
        Component2.doPrevious();
        Component2.doNext();
        Component2.doLast();
        Component2.doSort();
        System.assert(true);
    }
    

	    	
	}