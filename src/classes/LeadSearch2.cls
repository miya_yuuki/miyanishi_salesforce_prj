global with sharing class LeadSearch2 extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public Lead record{get;set;}	
			
	
		public Component3 Component3 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component15_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component15_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component15_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component15_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component17_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component17_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component17_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component17_isNull_op{get;set;}	
			
		public Lead Component30_val {get;set;}	
		public SkyEditor2.TextHolder Component30_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component78_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component78_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component78_op{get;set;}	
		public List<SelectOption> valueOptions_Lead_Lead_WEBsite_Quality_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component133_val {get;set;}	
		public SkyEditor2.TextHolder Component133_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component26_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component26_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component26_op{get;set;}	
		public List<SelectOption> valueOptions_Lead_WebSiteType_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component28_val {get;set;}	
		public SkyEditor2.TextHolder Component28_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component164_val {get;set;}	
		public SkyEditor2.TextHolder Component164_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component84_val {get;set;}	
		public SkyEditor2.TextHolder Component84_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component147_val {get;set;}	
		public SkyEditor2.TextHolder Component147_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component174_val {get;set;}	
		public SkyEditor2.TextHolder Component174_op{get;set;}	
		public List<SelectOption> valueOptions_Lead_Status1_c {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component176_val {get;set;}	
		public SkyEditor2.TextHolder Component176_op{get;set;}	
		public List<SelectOption> valueOptions_Lead_Status2_c {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component178_val {get;set;}	
		public SkyEditor2.TextHolder Component178_op{get;set;}	
		public List<SelectOption> valueOptions_Lead_MailNG_c {get;set;}
			
	public String recordTypeRecordsJSON_Lead {get; private set;}
	public String defaultRecordTypeId_Lead {get; private set;}
	public String metadataJSON_Lead {get; private set;}
	{
	setApiVersion(31.0);
	}
		public LeadSearch2(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = Lead.fields.OwnerId;
		f = Lead.fields.Lead_WEBsite_Quality__c;
		f = Lead.fields.SalesUnit__c;
		f = Lead.fields.WebSiteType__c;
		f = Lead.fields.Company;
		f = Lead.fields.Email;
		f = Lead.fields.tag__c;
		f = Lead.fields.IsConverted;
		f = Lead.fields.Status1__c;
		f = Lead.fields.Status2__c;
		f = Lead.fields.MailNG__c;
		f = Lead.fields.Name;
		f = Lead.fields.Phone;
		f = Lead.fields.Website;
		f = Lead.fields.lead_lead1__c;
		f = Lead.fields.lead_lead2__c;
		f = Lead.fields.lead_Timing1__c;
		f = Lead.fields.lead_NextContactDay__c;
		f = Lead.fields.lastTereapoDay__c;
		f = Lead.fields.lastTereapoMemo__c;
		f = Lead.fields.staff_approach_status__c;
		f = Lead.fields.staff_approach_NGreason__c;
		f = Lead.fields.company_approach_status__c;
		f = Lead.fields.company_approach_NGreason__c;
		f = Lead.fields.MailSendStopFlg__c;
 f = Lead.fields.lead_NextContactDay__c;
 f = Lead.fields.lead_Timing1__c;
		f = Lead.fields.RecordTypeId;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = Lead.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component15_from = new SkyEditor2__SkyEditorDummy__c();	
				Component15_to = new SkyEditor2__SkyEditorDummy__c();	
				Component15_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component15_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component17_from = new SkyEditor2__SkyEditorDummy__c();	
				Component17_to = new SkyEditor2__SkyEditorDummy__c();	
				Component17_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component17_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Lead lookupObjComponent72 = new Lead();	
				Component30_val = lookupObjComponent72;	
				Component30_op = new SkyEditor2.TextHolder();	
					
				Component78_val = new SkyEditor2__SkyEditorDummy__c();	
				Component78_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component78_op = new SkyEditor2.TextHolder();	
				valueOptions_Lead_Lead_WEBsite_Quality_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : Lead.Lead_WEBsite_Quality__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_Lead_Lead_WEBsite_Quality_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component133_val = new SkyEditor2__SkyEditorDummy__c();	
				Component133_op = new SkyEditor2.TextHolder();	
					
				Component26_val = new SkyEditor2__SkyEditorDummy__c();	
				Component26_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component26_op = new SkyEditor2.TextHolder();	
				valueOptions_Lead_WebSiteType_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : Lead.WebSiteType__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_Lead_WebSiteType_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component28_val = new SkyEditor2__SkyEditorDummy__c();	
				Component28_op = new SkyEditor2.TextHolder();	
					
				Component164_val = new SkyEditor2__SkyEditorDummy__c();	
				Component164_op = new SkyEditor2.TextHolder();	
					
				Component84_val = new SkyEditor2__SkyEditorDummy__c();	
				Component84_op = new SkyEditor2.TextHolder();	
					
				Component147_val = new SkyEditor2__SkyEditorDummy__c();	
				Component147_op = new SkyEditor2.TextHolder();	
					
				Component174_val = new SkyEditor2__SkyEditorDummy__c();	
				Component174_op = new SkyEditor2.TextHolder();	
				valueOptions_Lead_Status1_c = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : Lead.Status1__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_Lead_Status1_c.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component176_val = new SkyEditor2__SkyEditorDummy__c();	
				Component176_op = new SkyEditor2.TextHolder();	
				valueOptions_Lead_Status2_c = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : Lead.Status2__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_Lead_Status2_c.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component178_val = new SkyEditor2__SkyEditorDummy__c();	
				Component178_op = new SkyEditor2.TextHolder();	
				valueOptions_Lead_MailNG_c = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : Lead.MailNG__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_Lead_MailNG_c.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				queryMap.put(	
					'Component3',	
					new SkyEditor2.Query('Lead')
						.addField('OwnerId')
						.addFieldAsOutput('Name')
						.addFieldAsOutput('Company')
						.addFieldAsOutput('Phone')
						.addField('Website')
						.addField('Lead_WEBsite_Quality__c')
						.addField('WebSiteType__c')
						.addField('lead_lead1__c')
						.addField('lead_lead2__c')
						.addField('lead_Timing1__c')
						.addField('lead_NextContactDay__c')
						.addFieldAsOutput('lastTereapoDay__c')
						.addFieldAsOutput('lastTereapoMemo__c')
						.addField('staff_approach_status__c')
						.addField('staff_approach_NGreason__c')
						.addField('company_approach_status__c')
						.addField('company_approach_NGreason__c')
						.addField('MailSendStopFlg__c')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component15_from, 'SkyEditor2__Date__c', 'lead_NextContactDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component15_to, 'SkyEditor2__Date__c', 'lead_NextContactDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component15_isNull, 'SkyEditor2__Date__c', 'lead_NextContactDay__c', Component15_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component17_from, 'SkyEditor2__Date__c', 'lead_Timing1__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component17_to, 'SkyEditor2__Date__c', 'lead_Timing1__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component17_isNull, 'SkyEditor2__Date__c', 'lead_Timing1__c', Component17_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component30_val, 'OwnerId', 'OwnerId', Component30_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component78_val_dummy, 'SkyEditor2__Text__c','Lead_WEBsite_Quality__c', Component78_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component133_val, 'SkyEditor2__Text__c', 'SalesUnit__c', Component133_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component26_val_dummy, 'SkyEditor2__Text__c','WebSiteType__c', Component26_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component28_val, 'SkyEditor2__Text__c', 'Company', Component28_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component164_val, 'SkyEditor2__Text__c', 'Email', Component164_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component84_val, 'SkyEditor2__Text__c', 'tag__c', Component84_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component147_val, 'SkyEditor2__Checkbox__c', 'IsConverted', Component147_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component174_val, 'SkyEditor2__Text__c', 'Status1__c', Component174_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component176_val, 'SkyEditor2__Text__c', 'Status2__c', Component176_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component178_val, 'SkyEditor2__Text__c', 'MailNG__c', Component178_op, true, 0, false ))
						.addWhere(' ( RecordType.Name like \'%見込客%\')')
				);	
					
					Component3 = new Component3(new List<Lead>(), new List<Component3Item>(), new List<Lead>(), null);
				 Component3.setPageItems(new List<Component3Item>());
				 Component3.setPageSize(100);
				listItemHolders.put('Component3', Component3);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(Lead.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = false;
			execInitialSearch = false;
			presetSystemParams();
			Component3.extender = this.extender;
			initSearch();
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_Lead_OwnerId() { 
			return getOperatorOptions('Lead', 'OwnerId');	
		}	
		public List<SelectOption> getOperatorOptions_Lead_Lead_WEBsite_Quality_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_Lead_SalesUnit_c() { 
			return getOperatorOptions('Lead', 'SalesUnit__c');	
		}	
		public List<SelectOption> getOperatorOptions_Lead_WebSiteType_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_Lead_Company() { 
			return getOperatorOptions('Lead', 'Company');	
		}	
		public List<SelectOption> getOperatorOptions_Lead_Email() { 
			return getOperatorOptions('Lead', 'Email');	
		}	
		public List<SelectOption> getOperatorOptions_Lead_tag_c() { 
			return getOperatorOptions('Lead', 'tag__c');	
		}	
		public List<SelectOption> getOperatorOptions_Lead_IsConverted() { 
			return getOperatorOptions('Lead', 'IsConverted');	
		}	
		public List<SelectOption> getOperatorOptions_Lead_Status1_c() { 
			return getOperatorOptions('Lead', 'Status1__c');	
		}	
		public List<SelectOption> getOperatorOptions_Lead_Status2_c() { 
			return getOperatorOptions('Lead', 'Status2__c');	
		}	
		public List<SelectOption> getOperatorOptions_Lead_MailNG_c() { 
			return getOperatorOptions('Lead', 'MailNG__c');	
		}	
			
			
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public Lead record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, Lead record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.PagingList {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (Lead)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
		public void doFirst(){first();}
		public void doPrevious(){previous();}
		public void doNext(){next();}
		public void doLast(){last();}
		public void doSort(){sort();}

        public List<Component3Item> getViewItems() {            return (List<Component3Item>) getPageItems();        }
	}

	public Lead Component3_table_Conversion { get { return new Lead();}}
	
	public String Component3_table_selectval { get; set; }
	
	
			
	}