global class BatchScheduledCreateBillRecordFromOI implements Database.Batchable<sObject> {

	private String query;

	global BatchScheduledCreateBillRecordFromOI(Date fromDate, Date toDate)
	{
		// SOQL を生成
		query =
			'SELECT ' +
				'Id, ' +
				'Billchek__c ' +
			'FROM ' +
				'OrderItem__c ' +
			'WHERE ' +
				'Bill_Main__c >= ' + UtilityClass.retStringDt(fromDate) + ' ' +
				'AND Bill_Main__c <=' + UtilityClass.retStringDt(toDate) + ' ' +
				'AND Billchek__c = false ' +
				'AND InvoiceJoken__c = true ' +
				'AND BillingTiming__c = \'受注\'';
	}

	// バッチ開始処理
	// 開始するためにqueryを実行する。この実行されたSOQLのデータ分処理する。
	// 5千万件以上のレコードになるとエラーになる。
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		System.debug('*****************:' + query);
		return Database.getQueryLocator(query);
	}

	// バッチ処理内容
	// scopeにgetQueryLocatorの内容がバッチサイズ分格納されてくる
	global void execute(Database.BatchableContext BC, List<sObject> scope) {

		// 更新対象の注文商品リスト
		List<OrderItem__c> updatingOrderItem = new List<OrderItem__c>();

		for(sObject obj : scope) {
			// 注文商品のインスタンス
			OrderItem__c OrderItem = (OrderItem__c)obj.clone(true, true);

			// 注文商品のインスタンスを生成し、配列に追加
			updatingOrderItem.add(new OrderItem__c(
				Id = OrderItem.Id,
				Billchek__c = true
			));
		}

		System.debug('************* updating OrderItem__c records: ' + updatingOrderItem.size());

		// 注文商品を更新
		if (updatingOrderItem.size() > 0) update updatingOrderItem;
	}

	global void finish(Database.BatchableContext BC){}
}