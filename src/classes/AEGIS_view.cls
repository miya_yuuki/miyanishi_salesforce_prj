global with sharing class AEGIS_view extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public AEGIS__c record {get{return (AEGIS__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	
	
	public Component115 Component115 {get; private set;}
	public Component50 Component50 {get; private set;}
	{
	setApiVersion(31.0);
	}
	public AEGIS_view(ApexPages.StandardController controller) {
		super(controller);


		SObjectField f;

		f = AEGIS__c.fields.karute__c;
		f = AEGIS__c.fields.TargetMonth__c;
		f = AEGIS__c.fields.TargetMonthPeriod__c;
		f = AEGIS__c.fields.Company__c;
		f = AEGIS__c.fields.URL__c;
		f = AEGIS__c.fields.AccountID__c;
		f = AEGIS__c.fields.EF_URL__c;
		f = AEGIS__c.fields.Keyword__c;
		f = AEGIS__c.fields.BrandKeyword__c;
		f = AEGIS__c.fields.ViewName__c;
		f = AEGIS__c.fields.CVReport1__c;
		f = AEGIS__c.fields.session_modified__c;
		f = AEGIS__c.fields.CVFoam__c;
		f = AEGIS__c.fields.Session__c;
		f = AEGIS__c.fields.DirectoryURL__c;
		f = Landing__c.fields.URLText__c;
		f = Landing__c.fields.Keyword__c;
		f = AEGIS2__c.fields.Seat__c;
		f = AEGIS2__c.fields.Type01__c;
		f = AEGIS2__c.fields.Input__c;
		f = AEGIS2__c.fields.MuchType__c;
		f = AEGIS2__c.fields.Select__c;
		f = AEGIS2__c.fields.AEGIS__c;
		f = AEGIS__c.fields.CreatedDate;
		f = AEGIS__c.fields.CreatedById;
		f = AEGIS__c.fields.LastModifiedDate;
		f = AEGIS__c.fields.LastModifiedById;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = AEGIS__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('AEGIS__c');
			mainQuery.addField('TargetMonth__c');
			mainQuery.addField('TargetMonthPeriod__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('karute__c');
			mainQuery.addFieldAsOutput('Company__c');
			mainQuery.addFieldAsOutput('URL__c');
			mainQuery.addFieldAsOutput('AccountID__c');
			mainQuery.addFieldAsOutput('EF_URL__c');
			mainQuery.addFieldAsOutput('Keyword__c');
			mainQuery.addFieldAsOutput('BrandKeyword__c');
			mainQuery.addFieldAsOutput('ViewName__c');
			mainQuery.addFieldAsOutput('CVReport1__c');
			mainQuery.addFieldAsOutput('session_modified__c');
			mainQuery.addFieldAsOutput('CVFoam__c');
			mainQuery.addFieldAsOutput('Session__c');
			mainQuery.addFieldAsOutput('DirectoryURL__c');
			mainQuery.addFieldAsOutput('CreatedDate');
			mainQuery.addFieldAsOutput('CreatedById');
			mainQuery.addFieldAsOutput('LastModifiedDate');
			mainQuery.addFieldAsOutput('LastModifiedById');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			Component115 = new Component115(new List<Landing__c>(), new List<Component115Item>(), new List<Landing__c>(), null);
			listItemHolders.put('Component115', Component115);
			query = new SkyEditor2.Query('Landing__c');
			query.addFieldAsOutput('URLText__c');
			query.addFieldAsOutput('Keyword__c');
			query.addWhere('AEGIS__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('Component115', 'AEGIS__c');
			Component115.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('Component115', query);
			
			Component50 = new Component50(new List<AEGIS2__c>(), new List<Component50Item>(), new List<AEGIS2__c>(), null);
			listItemHolders.put('Component50', Component50);
			query = new SkyEditor2.Query('AEGIS2__c');
			query.addFieldAsOutput('Seat__c');
			query.addFieldAsOutput('Type01__c');
			query.addFieldAsOutput('Input__c');
			query.addFieldAsOutput('MuchType__c');
			query.addFieldAsOutput('Select__c');
			query.addFieldAsOutput('AEGIS__c');
			query.addWhere('AEGIS__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('Component50', 'AEGIS__c');
			Component50.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('Component50', query);
			
			registRelatedList('AEGISLanding__r', 'Component115');
			registRelatedList('AEGIS__r', 'Component50');
			
			p_showHeader = true;
			p_sidebar = true;
			init();
			
			Component115.extender = this.extender;
			Component50.extender = this.extender;
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class Component115Item extends SkyEditor2.ListItem {
		public Landing__c record{get; private set;}
		@TestVisible
		Component115Item(Component115 holder, Landing__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component115 extends SkyEditor2.ListItemHolder {
		public List<Component115Item> items{get; private set;}
		@TestVisible
			Component115(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component115Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component115Item(this, (Landing__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	global with sharing class Component50Item extends SkyEditor2.ListItem {
		public AEGIS2__c record{get; private set;}
		@TestVisible
		Component50Item(Component50 holder, AEGIS2__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component50 extends SkyEditor2.ListItemHolder {
		public List<Component50Item> items{get; private set;}
		@TestVisible
			Component50(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component50Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component50Item(this, (AEGIS2__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}