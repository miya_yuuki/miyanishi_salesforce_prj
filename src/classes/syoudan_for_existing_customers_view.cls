global with sharing class syoudan_for_existing_customers_view extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public Opportunity record {get{return (Opportunity)mainRecord;}}
	public Component6643 Component6643 {get; private set;}
	{
	setApiVersion(42.0);
	}
	public syoudan_for_existing_customers_view(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = Opportunity.fields.Group__c;
		f = Opportunity.fields.Name;
		f = Opportunity.fields.OpportunityCopy__c;
		f = Opportunity.fields.AccountId;
		f = Opportunity.fields.Opportunity_Type__c;
		f = Opportunity.fields.BoardDiscussed__c;
		f = Opportunity.fields.Opportunity_Partner__c;
		f = Opportunity.fields.Opportunity_endClient1__c;
		f = Opportunity.fields.karute__c;
		f = Opportunity.fields.karuteCheck__c;
		f = Opportunity.fields.karutenew__c;
		f = Opportunity.fields.Opportunity_Competition__c;
		f = Opportunity.fields.Opportunity_FastOrdersSales__c;
		f = Opportunity.fields.OwnerId;
		f = Opportunity.fields.Opportunity_Support__c;
		f = Opportunity.fields.MA_access_flag__c;
		f = Opportunity.fields.AmountTotal__c;
		f = Opportunity.fields.CloseDate;
		f = Opportunity.fields.StageName;
		f = Opportunity.fields.account_web_site__c;
		f = Opportunity.fields.account_web_site_unnecessary__c;
		f = Opportunity.fields.ServiceWEBSite__c;
		f = Opportunity.fields.ServiceWEBSite2__c;
		f = Opportunity.fields.CasePrintCheck__c;
		f = Opportunity.fields.CaseApproval__c;
		f = Opportunity.fields.Opportunity_lead1__c;
		f = Opportunity.fields.Opportunity_lead2__c;
		f = Opportunity.fields.AppointmentDay__c;
		f = Opportunity.fields.CampaignId;
		f = Opportunity.fields.Opportunity_BillingMethod__c;
		f = Opportunity.fields.Opportunity_PaymentMethod__c;
		f = Opportunity.fields.Opportunity_PaymentSiteMaster__c;
		f = Opportunity.fields.OrderStamp__c;
		f = Opportunity.fields.Opportunity_Title__c;
		f = Opportunity.fields.CustomerNamePRINT__c;
		f = Opportunity.fields.AddressCheck__c;
		f = Opportunity.fields.Opportunity_Memo__c;
		f = Opportunity.fields.OrderProvisional__c;
		f = Opportunity.fields.OrderProvisionalMemo__c;
		f = Opportunity.fields.SalvageScheduledDate__c;
		f = Opportunity.fields.opportunityBillMemo__c;
		f = Opportunity.fields.notices__c;
		f = Opportunity.fields.QuoteExpirationDate__c;
		f = Opportunity.fields.Opportunity_EstimateMemo__c;
		f = AccountCustomer__c.fields.CustomerName__c;
		f = AccountCustomer__c.fields.account__c;
		f = AccountCustomer__c.fields.department__c;
		f = AccountCustomer__c.fields.Title__c;
		f = AccountCustomer__c.fields.yakuwari__c;
		f = AccountCustomer__c.fields.Mail__c;
		f = AccountCustomer__c.fields.Report1__c;
		f = AccountCustomer__c.fields.PartnerReport__c;
		f = AccountCustomer__c.fields.ReportSend1__c;
		f = AccountCustomer__c.fields.PartnerReport_c__c;
		f = Opportunity.fields.AccountNameView__c;
		f = Opportunity.fields.Opportunity_BillPostalCode2__c;
		f = Opportunity.fields.Opportunity_BillPrefecture2__c;
		f = Opportunity.fields.Opportunity_BillCity2__c;
		f = Opportunity.fields.Opportunity_BillBuilding2__c;
		f = Opportunity.fields.Phone__c;
		f = Opportunity.fields.TransferAccountNumber1__c;
		f = Opportunity.fields.account_BusinessModel__c;
		f = Opportunity.fields.account_Industry__c;
		f = Opportunity.fields.account_Industrycategory__c;
		f = Opportunity.fields.AccountEdit__c;
		f = Opportunity.fields.account_CreditLevel__c;
		f = Opportunity.fields.account_CreditDay__c;
		f = Opportunity.fields.CreditSpace__c;
		f = Opportunity.fields.CreditBalance__c;
		f = Opportunity.fields.CreditDecisionPrice__c;
		f = Opportunity.fields.hansyaDay__c;
		f = Opportunity.fields.hansyaMemo__c;
		f = Opportunity.fields.CompanyCheck__c;
		f = Opportunity.fields.syouninn__c;
		f = Opportunity.fields.Opportunity_Billaccount__c;
		f = Opportunity.fields.Opportunity_BillPostalCode__c;
		f = Opportunity.fields.Opportunity_BillPrefecture__c;
		f = Opportunity.fields.Opportunity_BillCity__c;
		f = Opportunity.fields.Opportunity_BillAddress__c;
		f = Opportunity.fields.Opportunity_BillBuilding__c;
		f = Opportunity.fields.Opportunity_BillKana__c;
		f = Opportunity.fields.Opportunity_BillDepartment__c;
		f = Opportunity.fields.Opportunity_BillTitle__c;
		f = Opportunity.fields.Opportunity_BillRole__c;
		f = Opportunity.fields.Opportunity_BillPhone__c;
		f = Opportunity.fields.Opportunity_BillFax__c;
		f = Opportunity.fields.Opportunity_BillEmail__c;
		f = Opportunity.fields.ContacEdit__c;
		f = Opportunity.fields.Agreement1__c;
		f = Opportunity.fields.Agreement2__c;
		f = Opportunity.fields.Agreement3__c;
		f = Opportunity.fields.Agreement4__c;
		f = Opportunity.fields.Agreement5__c;
		f = Opportunity.fields.Agreement6__c;
		f = Opportunity.fields.Agreement7__c;
		f = Opportunity.fields.Agreement8__c;
		f = Opportunity.fields.Agreement9__c;
		f = Opportunity.fields.Agreement10__c;
		f = Opportunity.fields.Agreement11__c;
		f = Opportunity.fields.Agreement12__c;
		f = Opportunity.fields.Agreement17__c;
		f = Opportunity.fields.Agreement18__c;
		f = Opportunity.fields.Agreement19__c;
		f = Opportunity.fields.Agreement20__c;
		f = Opportunity.fields.Agreement13__c;
		f = Opportunity.fields.Agreement14__c;
		f = Opportunity.fields.Agreement15__c;
		f = Opportunity.fields.CreatedById;
		f = Opportunity.fields.CreatedDate;
		f = Opportunity.fields.LastModifiedById;
		f = Opportunity.fields.LastModifiedDate;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = Opportunity.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'syoudan_for_existing_customers_view';
			mainQuery = new SkyEditor2.Query('Opportunity');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Group__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('OpportunityCopy__c');
			mainQuery.addFieldAsOutput('AccountId');
			mainQuery.addFieldAsOutput('Opportunity_Type__c');
			mainQuery.addFieldAsOutput('BoardDiscussed__c');
			mainQuery.addFieldAsOutput('Opportunity_Partner__c');
			mainQuery.addFieldAsOutput('Opportunity_endClient1__c');
			mainQuery.addFieldAsOutput('karute__c');
			mainQuery.addFieldAsOutput('karuteCheck__c');
			mainQuery.addFieldAsOutput('karutenew__c');
			mainQuery.addFieldAsOutput('Opportunity_Competition__c');
			mainQuery.addFieldAsOutput('Opportunity_FastOrdersSales__c');
			mainQuery.addFieldAsOutput('OwnerId');
			mainQuery.addFieldAsOutput('Opportunity_Support__c');
			mainQuery.addFieldAsOutput('MA_access_flag__c');
			mainQuery.addFieldAsOutput('AmountTotal__c');
			mainQuery.addFieldAsOutput('CloseDate');
			mainQuery.addFieldAsOutput('StageName');
			mainQuery.addFieldAsOutput('account_web_site__c');
			mainQuery.addFieldAsOutput('account_web_site_unnecessary__c');
			mainQuery.addFieldAsOutput('ServiceWEBSite__c');
			mainQuery.addFieldAsOutput('ServiceWEBSite2__c');
			mainQuery.addFieldAsOutput('CasePrintCheck__c');
			mainQuery.addFieldAsOutput('CaseApproval__c');
			mainQuery.addFieldAsOutput('Opportunity_lead1__c');
			mainQuery.addFieldAsOutput('Opportunity_lead2__c');
			mainQuery.addFieldAsOutput('AppointmentDay__c');
			mainQuery.addFieldAsOutput('CampaignId');
			mainQuery.addFieldAsOutput('Opportunity_BillingMethod__c');
			mainQuery.addFieldAsOutput('Opportunity_PaymentMethod__c');
			mainQuery.addFieldAsOutput('Opportunity_PaymentSiteMaster__c');
			mainQuery.addFieldAsOutput('OrderStamp__c');
			mainQuery.addFieldAsOutput('Opportunity_Title__c');
			mainQuery.addFieldAsOutput('CustomerNamePRINT__c');
			mainQuery.addFieldAsOutput('AddressCheck__c');
			mainQuery.addFieldAsOutput('Opportunity_Memo__c');
			mainQuery.addFieldAsOutput('OrderProvisional__c');
			mainQuery.addFieldAsOutput('OrderProvisionalMemo__c');
			mainQuery.addFieldAsOutput('SalvageScheduledDate__c');
			mainQuery.addFieldAsOutput('opportunityBillMemo__c');
			mainQuery.addFieldAsOutput('notices__c');
			mainQuery.addFieldAsOutput('QuoteExpirationDate__c');
			mainQuery.addFieldAsOutput('Opportunity_EstimateMemo__c');
			mainQuery.addFieldAsOutput('AccountNameView__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPostalCode2__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPrefecture2__c');
			mainQuery.addFieldAsOutput('Opportunity_BillCity2__c');
			mainQuery.addFieldAsOutput('Opportunity_BillBuilding2__c');
			mainQuery.addFieldAsOutput('Phone__c');
			mainQuery.addFieldAsOutput('TransferAccountNumber1__c');
			mainQuery.addFieldAsOutput('account_BusinessModel__c');
			mainQuery.addFieldAsOutput('account_Industry__c');
			mainQuery.addFieldAsOutput('account_Industrycategory__c');
			mainQuery.addFieldAsOutput('AccountEdit__c');
			mainQuery.addFieldAsOutput('account_CreditLevel__c');
			mainQuery.addFieldAsOutput('account_CreditDay__c');
			mainQuery.addFieldAsOutput('CreditSpace__c');
			mainQuery.addFieldAsOutput('CreditBalance__c');
			mainQuery.addFieldAsOutput('CreditDecisionPrice__c');
			mainQuery.addFieldAsOutput('hansyaDay__c');
			mainQuery.addFieldAsOutput('hansyaMemo__c');
			mainQuery.addFieldAsOutput('CompanyCheck__c');
			mainQuery.addFieldAsOutput('syouninn__c');
			mainQuery.addFieldAsOutput('Opportunity_Billaccount__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPostalCode__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPrefecture__c');
			mainQuery.addFieldAsOutput('Opportunity_BillCity__c');
			mainQuery.addFieldAsOutput('Opportunity_BillAddress__c');
			mainQuery.addFieldAsOutput('Opportunity_BillBuilding__c');
			mainQuery.addFieldAsOutput('Opportunity_BillKana__c');
			mainQuery.addFieldAsOutput('Opportunity_BillDepartment__c');
			mainQuery.addFieldAsOutput('Opportunity_BillTitle__c');
			mainQuery.addFieldAsOutput('Opportunity_BillRole__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPhone__c');
			mainQuery.addFieldAsOutput('Opportunity_BillFax__c');
			mainQuery.addFieldAsOutput('Opportunity_BillEmail__c');
			mainQuery.addFieldAsOutput('ContacEdit__c');
			mainQuery.addFieldAsOutput('Agreement1__c');
			mainQuery.addFieldAsOutput('Agreement2__c');
			mainQuery.addFieldAsOutput('Agreement3__c');
			mainQuery.addFieldAsOutput('Agreement4__c');
			mainQuery.addFieldAsOutput('Agreement5__c');
			mainQuery.addFieldAsOutput('Agreement6__c');
			mainQuery.addFieldAsOutput('Agreement7__c');
			mainQuery.addFieldAsOutput('Agreement8__c');
			mainQuery.addFieldAsOutput('Agreement9__c');
			mainQuery.addFieldAsOutput('Agreement10__c');
			mainQuery.addFieldAsOutput('Agreement11__c');
			mainQuery.addFieldAsOutput('Agreement12__c');
			mainQuery.addFieldAsOutput('Agreement17__c');
			mainQuery.addFieldAsOutput('Agreement18__c');
			mainQuery.addFieldAsOutput('Agreement19__c');
			mainQuery.addFieldAsOutput('Agreement20__c');
			mainQuery.addFieldAsOutput('Agreement13__c');
			mainQuery.addFieldAsOutput('Agreement14__c');
			mainQuery.addFieldAsOutput('Agreement15__c');
			mainQuery.addFieldAsOutput('CreatedById');
			mainQuery.addFieldAsOutput('CreatedDate');
			mainQuery.addFieldAsOutput('LastModifiedById');
			mainQuery.addFieldAsOutput('LastModifiedDate');
			mainQuery.addFieldAsOutput('LeadSource');
			mainQuery.addFieldAsOutput('Id');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			Component6643 = new Component6643(new List<AccountCustomer__c>(), new List<Component6643Item>(), new List<AccountCustomer__c>(), null);
			listItemHolders.put('Component6643', Component6643);
			query = new SkyEditor2.Query('AccountCustomer__c');
			query.addFieldAsOutput('CustomerName__c');
			query.addFieldAsOutput('account__c');
			query.addFieldAsOutput('department__c');
			query.addFieldAsOutput('Title__c');
			query.addFieldAsOutput('yakuwari__c');
			query.addFieldAsOutput('Mail__c');
			query.addFieldAsOutput('Report1__c');
			query.addFieldAsOutput('PartnerReport__c');
			query.addFieldAsOutput('ReportSend1__c');
			query.addFieldAsOutput('PartnerReport_c__c');
			query.addWhere('Opportunity_customer__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('Component6643', 'Opportunity_customer__c');
			Component6643.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('Component6643', query);
			registRelatedList('Opportunity_customer__r', 'Component6643');
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('OpportunityCopy__c', 'OpportunityCopy__c');
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			Component6643.extender = this.extender;
			if (record.Id == null) {
				saveOldValues();
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class Component6643Item extends SkyEditor2.ListItem {
		public AccountCustomer__c record{get; private set;}
		@TestVisible
		Component6643Item(Component6643 holder, AccountCustomer__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component6643 extends SkyEditor2.ListItemHolder {
		public List<Component6643Item> items{get; private set;}
		@TestVisible
			Component6643(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component6643Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component6643Item(this, (AccountCustomer__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}