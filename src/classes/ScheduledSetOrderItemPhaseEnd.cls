global class ScheduledSetOrderItemPhaseEnd implements Schedulable {

	// 1回のexecuteメソッドで処理される件数
	private final Integer BATCH_SIZE = 1;

	global void execute(SchedulableContext sc) {

		System.debug('**** 注文商品フェーズ「契約終了」自動更新バッチ実行開始');
		BatchSetOrderItemPhaseEnd batch = new BatchSetOrderItemPhaseEnd(Date.today());
        Database.executeBatch(batch, BATCH_SIZE);
		System.debug('**** 注文商品フェーズ「契約終了」自動更新バッチ実行完了');
	}
}