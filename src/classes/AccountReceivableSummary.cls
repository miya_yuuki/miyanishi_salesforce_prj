global class AccountReceivableSummary {
	global static PageReference AccountReceivableSummary() {
		List <TextManagement2__c> updAccountReceivable = [
			SELECT Id
			FROM TextManagement2__c TM
			WHERE TM.Item2_StartData__c < TODAY
			AND TM.Item2_PaymentDueDate__c > TODAY
			AND TM.CloudStatus__c = '社外納品済み'
			AND TM.NoBill__c = false
			AND TM.OrderItemStatus__c NOT IN ('途中解約', '請求停止', '更新済み', '契約変更', '取消')
			AND TM.DeliveryCompletedDate__c != null
			limit 120
		];
		if(updAccountReceivable.size() > 0) {
			for(TextManagement2__c thisAccountReceivable: updAccountReceivable) {
				thisAccountReceivable.NoBill__c = true;
			}
			update updAccountReceivable;
		}
		return null;
    }
}