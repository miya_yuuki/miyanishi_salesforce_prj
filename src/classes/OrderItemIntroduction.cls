global with sharing class OrderItemIntroduction extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public OrderItem__c record {get{return (OrderItem__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	
	
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	
	{
	setApiVersion(31.0);
	}
	public OrderItemIntroduction(ApexPages.StandardController controller) {
		super(controller);


		SObjectField f;

		f = OrderItem__c.fields.Link_OrderItem__c;
		f = OrderItem__c.fields.Name;
		f = OrderItem__c.fields.Item2_Keyword_phase__c;
		f = OrderItem__c.fields.Item2_Relation__c;
		f = OrderItem__c.fields.Item2_Unit_selling_price__c;
		f = OrderItem__c.fields.ProductCode1__c;
		f = OrderItem__c.fields.Quantity__c;
		f = OrderItem__c.fields.Item2_Product__c;
		f = OrderItem__c.fields.TotalPrice__c;
		f = OrderItem__c.fields.Item2_Detail_Productlist__c;
		f = OrderItem__c.fields.Billchek__c;
		f = OrderItem__c.fields.Item2_ProductType1__c;
		f = OrderItem__c.fields.RecordTypeId;
		f = OrderItem__c.fields.Item2_DeliveryStandards__c;
		f = OrderItem__c.fields.Salesforce_ID__c;
		f = OrderItem__c.fields.syouninn__c;
		f = OrderItem__c.fields.Item2_Entry_date__c;
		f = OrderItem__c.fields.Item2_PaymentDueDate__c;
		f = OrderItem__c.fields.Item2_Billing_Stop_Start_Date__c;
		f = OrderItem__c.fields.Bill_Main__c;
		f = OrderItem__c.fields.Item2_Billing_Stop_End_Date__c;
		f = OrderItem__c.fields.BillSet__c;
		f = OrderItem__c.fields.Item2_CancellationDay__c;
		f = OrderItem__c.fields.Item2_Special_instruction__c;
		f = OrderItem__c.fields.CancelDay__c;
		f = OrderItem__c.fields.ChangeDay__c;
		f = OrderItem__c.fields.RecordCreateType__c;
		f = OrderItem__c.fields.SalesUnit1View__c;
		f = OrderItem__c.fields.SalesUnitSubView__c;
		f = OrderItem__c.fields.SalesPerson1__c;
		f = OrderItem__c.fields.SalesPersonSub__c;
		f = OrderItem__c.fields.ContractGetPersonPercentage1__c;
		f = OrderItem__c.fields.ContractGetPersonPercentage2__c;
		f = Product__c.fields.ProductDetails__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = OrderItem__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(OrderItem__c.SObjectType);
			
			mainQuery = new SkyEditor2.Query('OrderItem__c');
			mainQuery.addField('Item2_Keyword_phase__c');
			mainQuery.addField('Item2_Relation__c');
			mainQuery.addField('Item2_Unit_selling_price__c');
			mainQuery.addField('Quantity__c');
			mainQuery.addField('Item2_Product__c');
			mainQuery.addField('Item2_Detail_Productlist__c');
			mainQuery.addField('Billchek__c');
			mainQuery.addField('syouninn__c');
			mainQuery.addField('Item2_Entry_date__c');
			mainQuery.addField('Item2_Billing_Stop_Start_Date__c');
			mainQuery.addField('Bill_Main__c');
			mainQuery.addField('Item2_Billing_Stop_End_Date__c');
			mainQuery.addField('BillSet__c');
			mainQuery.addField('Item2_CancellationDay__c');
			mainQuery.addField('Item2_Special_instruction__c');
			mainQuery.addField('CancelDay__c');
			mainQuery.addField('ChangeDay__c');
			mainQuery.addField('RecordCreateType__c');
			mainQuery.addField('SalesPerson1__c');
			mainQuery.addField('SalesPersonSub__c');
			mainQuery.addField('ContractGetPersonPercentage1__c');
			mainQuery.addField('ContractGetPersonPercentage2__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Link_OrderItem__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('ProductCode1__c');
			mainQuery.addFieldAsOutput('TotalPrice__c');
			mainQuery.addFieldAsOutput('Item2_ProductType1__c');
			mainQuery.addFieldAsOutput('RecordType.Name');
			mainQuery.addFieldAsOutput('Item2_DeliveryStandards__c');
			mainQuery.addFieldAsOutput('Salesforce_ID__c');
			mainQuery.addFieldAsOutput('Item2_PaymentDueDate__c');
			mainQuery.addFieldAsOutput('SalesUnit1View__c');
			mainQuery.addFieldAsOutput('SalesUnitSubView__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('Item2_Relation__c', 'CF00N10000005jNKn_lkid');
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			
			if (record.Id == null) {
				
				saveOldValues();
				
				if(record.RecordTypeId == null) recordTypeSelector.applyDefault(record);
				
			}

			
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}


	public void loadReferenceValues_Component182() {
		if (record.Item2_Product__c == null) {

			if (SkyEditor2.Util.isEditable(record, OrderItem__c.fields.Item2_Detail_Productlist__c)) {
				record.Item2_Detail_Productlist__c = null;
			}
				return;
		}
		Product__c[] referenceTo = [SELECT ProductDetails__c FROM Product__c WHERE Id=:record.Item2_Product__c];
		if (referenceTo.size() == 0) {
			record.Item2_Product__c.addError(SkyEditor2.Messages.referenceDataNotFound(record.Item2_Product__c));
			return;
		}
		
		if (SkyEditor2.Util.isEditable(record, OrderItem__c.fields.Item2_Detail_Productlist__c)) {
			record.Item2_Detail_Productlist__c = referenceTo[0].ProductDetails__c;
		}
		
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}