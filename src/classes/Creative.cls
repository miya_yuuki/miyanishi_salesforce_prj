global with sharing class Creative extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public OpportunityProduct__c record{get;set;}	
			
	
		public Component2 Component2 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component117_val {get;set;}	
		public SkyEditor2.TextHolder Component117_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component13_val {get;set;}	
		public SkyEditor2.TextHolder Component13_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component15_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component15_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component15_op{get;set;}	
		public List<SelectOption> valueOptions_Opportunity_StageName_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component96_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component96_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component96_op{get;set;}	
		public List<SelectOption> valueOptions_OpportunityProduct_c_CTStatus_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component67_val {get;set;}	
		public SkyEditor2.TextHolder Component67_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component104_val {get;set;}	
		public SkyEditor2.TextHolder Component104_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component110_val {get;set;}	
		public SkyEditor2.TextHolder Component110_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component119_val {get;set;}	
		public SkyEditor2.TextHolder Component119_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component123_val {get;set;}	
		public SkyEditor2.TextHolder Component123_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component129_val {get;set;}	
		public SkyEditor2.TextHolder Component129_op{get;set;}	
		public List<SelectOption> valueOptions_OpportunityProduct_c_Item_Classification_c {get;set;}
			
	public String recordTypeRecordsJSON_OpportunityProduct_c {get; private set;}
	public String defaultRecordTypeId_OpportunityProduct_c {get; private set;}
	public String metadataJSON_OpportunityProduct_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public Creative(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = OpportunityProduct__c.fields.CTDay__c;
		f = OpportunityProduct__c.fields.Name;
		f = Opportunity.fields.StageName;
		f = OpportunityProduct__c.fields.CTStatus__c;
		f = OpportunityProduct__c.fields.Account__c;
		f = OpportunityProduct__c.fields.RecordTypeName__c;
		f = OpportunityProduct__c.fields.ProductName_Detail__c;
		f = OpportunityProduct__c.fields.SalesPerson__c;
		f = OpportunityProduct__c.fields.OEM__c;
		f = OpportunityProduct__c.fields.Item_Classification__c;
		f = OpportunityProduct__c.fields.sisakuompany__c;
		f = OpportunityProduct__c.fields.Item_Keyword_strategy_keyword__c;
		f = OpportunityProduct__c.fields.URL_RandDType__c;
		f = OpportunityProduct__c.fields.OrderDay__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = OpportunityProduct__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component117_val = new SkyEditor2__SkyEditorDummy__c();	
				Component117_op = new SkyEditor2.TextHolder();	
					
				Component13_val = new SkyEditor2__SkyEditorDummy__c();	
				Component13_op = new SkyEditor2.TextHolder();	
					
				Component96_val = new SkyEditor2__SkyEditorDummy__c();	
				Component96_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component96_op = new SkyEditor2.TextHolder();	
				valueOptions_OpportunityProduct_c_CTStatus_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : OpportunityProduct__c.CTStatus__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_OpportunityProduct_c_CTStatus_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component67_val = new SkyEditor2__SkyEditorDummy__c();	
				Component67_op = new SkyEditor2.TextHolder();	
					
				Component104_val = new SkyEditor2__SkyEditorDummy__c();	
				Component104_op = new SkyEditor2.TextHolder();	
					
				Component110_val = new SkyEditor2__SkyEditorDummy__c();	
				Component110_op = new SkyEditor2.TextHolder();	
					
				Component119_val = new SkyEditor2__SkyEditorDummy__c();	
				Component119_op = new SkyEditor2.TextHolder();	
					
				Component123_val = new SkyEditor2__SkyEditorDummy__c();	
				Component123_op = new SkyEditor2.TextHolder();	
					
				Component129_val = new SkyEditor2__SkyEditorDummy__c();	
				Component129_op = new SkyEditor2.TextHolder();	
				valueOptions_OpportunityProduct_c_Item_Classification_c = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : OpportunityProduct__c.Item_Classification__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_OpportunityProduct_c_Item_Classification_c.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component15_val = new SkyEditor2__SkyEditorDummy__c();	
				Component15_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component15_op = new SkyEditor2.TextHolder();	
				valueOptions_Opportunity_StageName_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : Opportunity.StageName.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_Opportunity_StageName_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				queryMap.put(	
					'Component2',	
					new SkyEditor2.Query('OpportunityProduct__c')
						.addFieldAsOutput('Account__c')
						.addFieldAsOutput('OEM__c')
						.addFieldAsOutput('sisakuompany__c')
						.addFieldAsOutput('SalesPerson__c')
						.addFieldAsOutput('ProductName_Detail__c')
						.addFieldAsOutput('Item_Classification__c')
						.addFieldAsOutput('Item_Keyword_strategy_keyword__c')
						.addFieldAsOutput('URL_RandDType__c')
						.addFieldAsOutput('OrderDay__c')
						.addField('CTStatus__c')
						.addField('CTDay__c')
						.addFieldAsOutput('Name')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component117_val, 'SkyEditor2__Date__c', 'CTDay__c', Component117_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component13_val, 'SkyEditor2__Text__c', 'Name', Component13_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component15_val_dummy, 'SkyEditor2__Text__c','Opportunity__r.StageName',Opportunity.fields.StageName, Component15_op, true, false,true,0,false,'Opportunity__c',OpportunityProduct__c.fields.Opportunity__c )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component96_val_dummy, 'SkyEditor2__Text__c','CTStatus__c', Component96_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component67_val, 'SkyEditor2__Text__c', 'Account__c', Component67_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component104_val, 'SkyEditor2__Text__c', 'RecordTypeName__c', Component104_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component110_val, 'SkyEditor2__Text__c', 'ProductName_Detail__c', Component110_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component119_val, 'SkyEditor2__Text__c', 'SalesPerson__c', Component119_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component123_val, 'SkyEditor2__Text__c', 'OEM__c', Component123_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component129_val, 'SkyEditor2__Text__c', 'Item_Classification__c', Component129_op, true, 0, false ))
				);	
					
					Component2 = new Component2(new List<OpportunityProduct__c>(), new List<Component2Item>(), new List<OpportunityProduct__c>(), null);
				listItemHolders.put('Component2', Component2);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(OpportunityProduct__c.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = true;
			presetSystemParams();
			Component2.extender = this.extender;
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_CTDay_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'CTDay__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_Name() { 
			return getOperatorOptions('OpportunityProduct__c', 'Name');	
		}	
		public List<SelectOption> getOperatorOptions_Opportunity_StageName_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_CTStatus_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_Account_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'Account__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_RecordTypeName_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'RecordTypeName__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_ProductName_Detail_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'ProductName_Detail__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_SalesPerson_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'SalesPerson__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_OEM_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'OEM__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_Item_Classification_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'Item_Classification__c');	
		}	
			
			
	global with sharing class Component2Item extends SkyEditor2.ListItem {
		public OpportunityProduct__c record{get; private set;}
		@TestVisible
		Component2Item(Component2 holder, OpportunityProduct__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component2 extends SkyEditor2.ListItemHolder {
		public List<Component2Item> items{get; private set;}
		@TestVisible
			Component2(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component2Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component2Item(this, (OpportunityProduct__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	public OpportunityProduct__c Component2_table_Conversion { get { return new OpportunityProduct__c();}}
	
	public String Component2_table_selectval { get; set; }
	
	
			
	}