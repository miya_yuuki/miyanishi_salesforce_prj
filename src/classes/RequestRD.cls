global with sharing class RequestRD extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public CustomObject1__c record{get;set;}	
			
	
		public Component3 Component3 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public CustomObject1__c Component6_val {get;set;}	
		public SkyEditor2.TextHolder Component6_op{get;set;}	
			
		public CustomObject1__c Component8_val {get;set;}	
		public SkyEditor2.TextHolder Component8_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component10_val {get;set;}	
		public SkyEditor2.TextHolder Component10_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component46_val {get;set;}	
		public SkyEditor2.TextHolder Component46_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component35_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component35_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component35_op{get;set;}	
		public List<SelectOption> valueOptions_CustomObject1_c_StatusRD_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component54_val {get;set;}	
		public SkyEditor2.TextHolder Component54_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component51_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component51_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component51_op{get;set;}	
		public List<SelectOption> valueOptions_CustomObject1_c_Status_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component64_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component64_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component64_op{get;set;}	
		public List<SelectOption> valueOptions_CustomObject1_c_ProductGroup_c_multi {get;set;}
			
	public String recordTypeRecordsJSON_CustomObject1_c {get; private set;}
	public String defaultRecordTypeId_CustomObject1_c {get; private set;}
	public String metadataJSON_CustomObject1_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public RequestRD(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = CustomObject1__c.fields.OrderNo__c;
		f = CustomObject1__c.fields.OwnerId;
		f = CustomObject1__c.fields.RecordTypeId;
		f = CustomObject1__c.fields.AccountName__c;
		f = CustomObject1__c.fields.StatusRD__c;
		f = CustomObject1__c.fields.Name;
		f = CustomObject1__c.fields.Status__c;
		f = CustomObject1__c.fields.ProductGroup__c;
		f = CustomObject1__c.fields.RequestDay__c;
		f = CustomObject1__c.fields.RequestEndDay__c;
		f = CustomObject1__c.fields.RDtekiyouDay__c;
		f = CustomObject1__c.fields.RDhenkouDay__c;
		f = CustomObject1__c.fields.sentaku__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = CustomObject1__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				CustomObject1__c lookupObjComponent30 = new CustomObject1__c();	
				Component6_val = lookupObjComponent30;	
				Component6_op = new SkyEditor2.TextHolder();	
					
				Component8_val = lookupObjComponent30;	
				Component8_op = new SkyEditor2.TextHolder();	
					
				Component10_val = new SkyEditor2__SkyEditorDummy__c();	
				Component10_op = new SkyEditor2.TextHolder();	
					
				Component46_val = new SkyEditor2__SkyEditorDummy__c();	
				Component46_op = new SkyEditor2.TextHolder();	
					
				Component35_val = new SkyEditor2__SkyEditorDummy__c();	
				Component35_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component35_op = new SkyEditor2.TextHolder();	
				valueOptions_CustomObject1_c_StatusRD_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : CustomObject1__c.StatusRD__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_CustomObject1_c_StatusRD_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component54_val = new SkyEditor2__SkyEditorDummy__c();	
				Component54_op = new SkyEditor2.TextHolder();	
					
				Component51_val = new SkyEditor2__SkyEditorDummy__c();	
				Component51_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component51_op = new SkyEditor2.TextHolder();	
				valueOptions_CustomObject1_c_Status_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : CustomObject1__c.Status__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_CustomObject1_c_Status_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component64_val = new SkyEditor2__SkyEditorDummy__c();	
				Component64_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component64_op = new SkyEditor2.TextHolder();	
				valueOptions_CustomObject1_c_ProductGroup_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : CustomObject1__c.ProductGroup__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_CustomObject1_c_ProductGroup_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				queryMap.put(	
					'Component3',	
					new SkyEditor2.Query('CustomObject1__c')
						.addField('StatusRD__c')
						.addFieldAsOutput('RecordType.Name')
						.addFieldAsOutput('Name')
						.addFieldAsOutput('AccountName__c')
						.addFieldAsOutput('ProductGroup__c')
						.addFieldAsOutput('OwnerId')
						.addFieldAsOutput('OrderNo__c')
						.addFieldAsOutput('RequestDay__c')
						.addFieldAsOutput('RequestEndDay__c')
						.addFieldAsOutput('RDtekiyouDay__c')
						.addFieldAsOutput('RDhenkouDay__c')
						.addFieldAsOutput('sentaku__c')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component6_val, 'OrderNo__c', 'OrderNo__c', Component6_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component8_val, 'OwnerId', 'OwnerId', Component8_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component10_val, 'SkyEditor2__Text__c', 'RecordTypeId', Component10_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component46_val, 'SkyEditor2__Text__c', 'AccountName__c', Component46_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component35_val_dummy, 'SkyEditor2__Text__c','StatusRD__c', Component35_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component54_val, 'SkyEditor2__Text__c', 'Name', Component54_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component51_val_dummy, 'SkyEditor2__Text__c','Status__c', Component51_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component64_val_dummy, 'SkyEditor2__Text__c','ProductGroup__c', Component64_op, true, 0, false ))
				);	
					
					Component3 = new Component3(new List<CustomObject1__c>(), new List<Component3Item>(), new List<CustomObject1__c>(), null);
				listItemHolders.put('Component3', Component3);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(CustomObject1__c.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = false;
			presetSystemParams();
			Component3.extender = this.extender;
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_CustomObject1_c_OrderNo_c() { 
			return getOperatorOptions('CustomObject1__c', 'OrderNo__c');	
		}	
		public List<SelectOption> getOperatorOptions_CustomObject1_c_OwnerId() { 
			return getOperatorOptions('CustomObject1__c', 'OwnerId');	
		}	
		public List<SelectOption> getOperatorOptions_CustomObject1_c_RecordTypeId() { 
			return getOperatorOptions('CustomObject1__c', 'RecordTypeId');	
		}	
		public List<SelectOption> getOperatorOptions_CustomObject1_c_AccountName_c() { 
			return getOperatorOptions('CustomObject1__c', 'AccountName__c');	
		}	
		public List<SelectOption> getOperatorOptions_CustomObject1_c_StatusRD_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_CustomObject1_c_Name() { 
			return getOperatorOptions('CustomObject1__c', 'Name');	
		}	
		public List<SelectOption> getOperatorOptions_CustomObject1_c_Status_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_CustomObject1_c_ProductGroup_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
			
			
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public CustomObject1__c record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, CustomObject1__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.ListItemHolder {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (CustomObject1__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	public CustomObject1__c Component3_table_Conversion { get { return new CustomObject1__c();}}
	
	public String Component3_table_selectval { get; set; }
	
	
			
	}