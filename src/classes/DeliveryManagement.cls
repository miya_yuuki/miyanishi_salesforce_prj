global with sharing class DeliveryManagement extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public TextManagement2__c record{get;set;}	
			
	
		public Component2 Component2 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component76_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component76_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component76_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component76_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component222_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component222_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component222_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component222_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component239_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component239_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component239_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component239_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component195_val {get;set;}	
		public SkyEditor2.TextHolder Component195_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component197_val {get;set;}	
		public SkyEditor2.TextHolder Component197_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component199_val {get;set;}	
		public SkyEditor2.TextHolder Component199_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component243_val {get;set;}	
		public SkyEditor2.TextHolder Component243_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component133_val {get;set;}	
		public SkyEditor2.TextHolder Component133_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component11_val {get;set;}	
		public SkyEditor2.TextHolder Component11_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component15_val {get;set;}	
		public SkyEditor2.TextHolder Component15_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component17_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component17_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component17_op{get;set;}	
		public List<SelectOption> valueOptions_TextManagement2_c_CloudStatus_c_multi {get;set;}
			
		public TextManagement2__c Component122_val {get;set;}	
		public SkyEditor2.TextHolder Component122_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component125_val {get;set;}	
		public SkyEditor2.TextHolder Component125_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component128_val {get;set;}	
		public SkyEditor2.TextHolder Component128_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component184_val {get;set;}	
		public SkyEditor2.TextHolder Component184_op{get;set;}	
			
	public String recordTypeRecordsJSON_TextManagement2_c {get; private set;}
	public String defaultRecordTypeId_TextManagement2_c {get; private set;}
	public String metadataJSON_TextManagement2_c {get; private set;}
	public Boolean QueryPagingConfirmationSetting {get;set;}
	{
	setApiVersion(31.0);
	}
		public DeliveryManagement(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = TextManagement2__c.fields.flagPlatinum__c;
		f = TextManagement2__c.fields.flagQuestionnaire__c;
		f = TextManagement2__c.fields.flagLighting__c;
		f = TextManagement2__c.fields.flagExpert__c;
		f = TextManagement2__c.fields.Account1__c;
		f = TextManagement2__c.fields.Product__c;
		f = TextManagement2__c.fields.Status__c;
		f = TextManagement2__c.fields.CloudStatus__c;
		f = TextManagement2__c.fields.SalesPerson10__c;
		f = TextManagement2__c.fields.Text_Accep_TedNorm__c;
		f = TextManagement2__c.fields.GroupID__c;
		f = TextManagement2__c.fields.Name;
		f = TextManagement2__c.fields.OpportunityItemNoLink__c;
		f = TextManagement2__c.fields.TextType__c;
		f = TextManagement2__c.fields.SplitDeliveryHopeDate__c;
		f = TextManagement2__c.fields.DeliveryCompleteHopeDate__c;
		f = TextManagement2__c.fields.DeliveryScheduleDay__c;
		f = TextManagement2__c.fields.DeliveryDaySales1__c;
		f = TextManagement2__c.fields.SalesDeliveryCount__c;
		f = TextManagement2__c.fields.Item2_Keyword_letter_count__c;
		f = TextManagement2__c.fields.Selling_price__c;
		f = TextManagement2__c.fields.EditorEstimationAmount__c;
		f = TextManagement2__c.fields.UserEstimationAmount__c;
		f = TextManagement2__c.fields.OutsourcingEstimationAmount__c;
		f = TextManagement2__c.fields.CloudCost1__c;
		f = TextManagement2__c.fields.regulationSheetNo__c;
		f = TextManagement2__c.fields.CMS__c;
		f = TextManagement2__c.fields.ImageContents__c;
		f = TextManagement2__c.fields.PictureCount__c;
		f = TextManagement2__c.fields.PictureTotal__c;
		f = TextManagement2__c.fields.CloudBox__c;
		f = TextManagement2__c.fields.Memo__c;
		f = TextManagement2__c.fields.SalesMemo1__c;
 f = TextManagement2__c.fields.DeliveryScheduleDay__c;
 f = TextManagement2__c.fields.SplitDeliveryHopeDate__c;
 f = TextManagement2__c.fields.DeliveryDaySales1__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = TextManagement2__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component76_from = new SkyEditor2__SkyEditorDummy__c();	
				Component76_to = new SkyEditor2__SkyEditorDummy__c();	
				Component76_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component76_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component222_from = new SkyEditor2__SkyEditorDummy__c();	
				Component222_to = new SkyEditor2__SkyEditorDummy__c();	
				Component222_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component222_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component239_from = new SkyEditor2__SkyEditorDummy__c();	
				Component239_to = new SkyEditor2__SkyEditorDummy__c();	
				Component239_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component239_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				TextManagement2__c lookupObjComponent61 = new TextManagement2__c();	
				Component195_val = new SkyEditor2__SkyEditorDummy__c();	
				Component195_op = new SkyEditor2.TextHolder();	
					
				Component197_val = new SkyEditor2__SkyEditorDummy__c();	
				Component197_op = new SkyEditor2.TextHolder();	
					
				Component199_val = new SkyEditor2__SkyEditorDummy__c();	
				Component199_op = new SkyEditor2.TextHolder();	
					
				Component243_val = new SkyEditor2__SkyEditorDummy__c();	
				Component243_op = new SkyEditor2.TextHolder();	
					
				Component133_val = new SkyEditor2__SkyEditorDummy__c();	
				Component133_op = new SkyEditor2.TextHolder();	
					
				Component11_val = new SkyEditor2__SkyEditorDummy__c();	
				Component11_op = new SkyEditor2.TextHolder();	
					
				Component15_val = new SkyEditor2__SkyEditorDummy__c();	
				Component15_op = new SkyEditor2.TextHolder();	
					
				Component17_val = new SkyEditor2__SkyEditorDummy__c();	
				Component17_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component17_op = new SkyEditor2.TextHolder();	
				valueOptions_TextManagement2_c_CloudStatus_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : TextManagement2__c.CloudStatus__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_TextManagement2_c_CloudStatus_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component122_val = lookupObjComponent61;	
				Component122_op = new SkyEditor2.TextHolder();	
					
				Component125_val = new SkyEditor2__SkyEditorDummy__c();	
				Component125_op = new SkyEditor2.TextHolder();	
					
				Component128_val = new SkyEditor2__SkyEditorDummy__c();	
				Component128_op = new SkyEditor2.TextHolder();	
					
				Component184_val = new SkyEditor2__SkyEditorDummy__c();	
				Component184_op = new SkyEditor2.TextHolder();	
					
				queryMap.put(	
					'Component2',	
					new SkyEditor2.Query('TextManagement2__c')
						.addField('CloudStatus__c')
						.addFieldAsOutput('OpportunityItemNoLink__c')
						.addFieldAsOutput('Name')
						.addFieldAsOutput('Account1__c')
						.addFieldAsOutput('Product__c')
						.addFieldAsOutput('Text_Accep_TedNorm__c')
						.addFieldAsOutput('TextType__c')
						.addFieldAsOutput('SplitDeliveryHopeDate__c')
						.addFieldAsOutput('DeliveryCompleteHopeDate__c')
						.addField('DeliveryScheduleDay__c')
						.addField('DeliveryDaySales1__c')
						.addField('SalesDeliveryCount__c')
						.addFieldAsOutput('Item2_Keyword_letter_count__c')
						.addFieldAsOutput('Selling_price__c')
						.addField('EditorEstimationAmount__c')
						.addField('UserEstimationAmount__c')
						.addField('OutsourcingEstimationAmount__c')
						.addFieldAsOutput('CloudCost1__c')
						.addFieldAsOutput('regulationSheetNo__c')
						.addFieldAsOutput('CMS__c')
						.addFieldAsOutput('ImageContents__c')
						.addFieldAsOutput('PictureCount__c')
						.addFieldAsOutput('PictureTotal__c')
						.addFieldAsOutput('SalesPerson10__c')
						.addField('GroupID__c')
						.addField('CloudBox__c')
						.addField('Memo__c')
						.addFieldAsOutput('SalesMemo1__c')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component76_from, 'SkyEditor2__Date__c', 'DeliveryScheduleDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component76_to, 'SkyEditor2__Date__c', 'DeliveryScheduleDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component76_isNull, 'SkyEditor2__Date__c', 'DeliveryScheduleDay__c', Component76_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component222_from, 'SkyEditor2__Date__c', 'SplitDeliveryHopeDate__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component222_to, 'SkyEditor2__Date__c', 'SplitDeliveryHopeDate__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component222_isNull, 'SkyEditor2__Date__c', 'SplitDeliveryHopeDate__c', Component222_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component239_from, 'SkyEditor2__Date__c', 'DeliveryDaySales1__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component239_to, 'SkyEditor2__Date__c', 'DeliveryDaySales1__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component239_isNull, 'SkyEditor2__Date__c', 'DeliveryDaySales1__c', Component239_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component195_val, 'SkyEditor2__Checkbox__c', 'flagPlatinum__c', Component195_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component197_val, 'SkyEditor2__Checkbox__c', 'flagQuestionnaire__c', Component197_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component199_val, 'SkyEditor2__Checkbox__c', 'flagLighting__c', Component199_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component243_val, 'SkyEditor2__Checkbox__c', 'flagExpert__c', Component243_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component133_val, 'SkyEditor2__Text__c', 'Account1__c', Component133_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component11_val, 'SkyEditor2__Text__c', 'Product__c', Component11_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component15_val, 'SkyEditor2__Text__c', 'Status__c', Component15_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component17_val_dummy, 'SkyEditor2__Text__c','CloudStatus__c', Component17_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component122_val, 'SalesPerson10__c', 'SalesPerson10__c', Component122_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component125_val, 'SkyEditor2__Text__c', 'Text_Accep_TedNorm__c', Component125_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component128_val, 'SkyEditor2__Textarea__c', 'GroupID__c', Component128_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component184_val, 'SkyEditor2__Text__c', 'Name', Component184_op, true, 0, false ))
				);	
					
					Component2 = new Component2(new List<TextManagement2__c>(), new List<Component2Item>(), new List<TextManagement2__c>(), null, queryMap.get('Component2'));
				 Component2.setPageSize(100);
				listItemHolders.put('Component2', Component2);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(TextManagement2__c.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = false;
					Cookie c = ApexPages.currentPage().getCookies().get('QueryPagingConfirm');
					QueryPagingConfirmationSetting = c != null && c.getValue() == '0';
			execInitialSearch = false;
			presetSystemParams();
			Component2.extender = this.extender;
			initSearch();
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_TextManagement2_c_flagPlatinum_c() { 
			return getOperatorOptions('TextManagement2__c', 'flagPlatinum__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_flagQuestionnaire_c() { 
			return getOperatorOptions('TextManagement2__c', 'flagQuestionnaire__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_flagLighting_c() { 
			return getOperatorOptions('TextManagement2__c', 'flagLighting__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_flagExpert_c() { 
			return getOperatorOptions('TextManagement2__c', 'flagExpert__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Account1_c() { 
			return getOperatorOptions('TextManagement2__c', 'Account1__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Product_c() { 
			return getOperatorOptions('TextManagement2__c', 'Product__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Status_c() { 
			return getOperatorOptions('TextManagement2__c', 'Status__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_CloudStatus_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_SalesPerson10_c() { 
			return getOperatorOptions('TextManagement2__c', 'SalesPerson10__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Text_Accep_TedNorm_c() { 
			return getOperatorOptions('TextManagement2__c', 'Text_Accep_TedNorm__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_GroupID_c() { 
			return getOperatorOptions('TextManagement2__c', 'GroupID__c');	
		}	
		public List<SelectOption> getOperatorOptions_TextManagement2_c_Name() { 
			return getOperatorOptions('TextManagement2__c', 'Name');	
		}	
			
			
	global with sharing class Component2Item extends SkyEditor2.ListItem {
		public TextManagement2__c record{get; private set;}
		@TestVisible
		Component2Item(Component2 holder, TextManagement2__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component2 extends SkyEditor2.QueryPagingList {
		public List<Component2Item> items{get; private set;}
		@TestVisible
			Component2(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector, SkyEditor2.Query query) {
			super(records, items, deleteRecords, recordTypeSelector, query);
			this.items = (List<Component2Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component2Item(this, (TextManagement2__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
		public void doFirst(){first();}
		public void doPrevious(){previous();}
		public void doNext(){next();}
		public void doLast(){last();}
		public void doSort(){sort();}

	}

	public TextManagement2__c Component2_table_Conversion { get { return new TextManagement2__c();}}
	
	public String Component2_table_selectval { get; set; }
	
	
			
	}