global with sharing class OpportunityCommodity_sub extends SkyEditor2.SkyEditorPageBaseWithSharing{
    
    public Product2 record{get;set;}
    public Component2 Component2 {get; private set;}
    public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
    public SkyEditor2__SkyEditorDummy__c Component3{get;set;}
    public List<SelectOption> Component3_options {get;set;}
    public SkyEditor2__SkyEditorDummy__c Component6{get;set;}
    Map<String, String> parameters;
    Id opportunityId;
    Id pricebook2Id;
    public OpportunityCommodity_sub(ApexPages.StandardController controller){
        super(controller);

        SObjectField f;
        f = Product2.fields.Name;
        f = Product2.fields.ProductCode;
        f = Product2.fields.Family;
        f = Product2.fields.Items_Classification__c;
        f = Product2.fields.IsActive;
        f = Product2.fields.NumberOfRevenueInstallments;

        try {
            parameters = ApexPages.currentPage().getParameters();
            opportunityId = parameters.get('oppId');
            if (opportunityId != null) {
                Opportunity opp = [select Pricebook2Id from Opportunity where Id = :opportunityId];
                if (opp.Pricebook2Id != null) {
                    pricebook2Id = opp.Pricebook2Id;
                } else {
                    throw new SkyEditor2.Errors.PricebookNotFoundException(SkyEditor2.Messages.PRICEBOOK_NOT_FOUND);
                }
            }
            
            mainRecord = null;
            mainSObjectType = Product2.SObjectType;
            mode = SkyEditor2.LayoutMode.TempProductLookup_01;
            
            Component3 = new SkyEditor2__SkyEditorDummy__c();
            Component6 = new SkyEditor2__SkyEditorDummy__c();
            
            queryMap.put(
                'Component2',
                new SkyEditor2.Query('Product2')
                    .addFieldAsOutput('ProductCode')
                    .addFieldAsOutput('Family')
                    .addFieldAsOutput('Name')
                    .addFieldAsOutput('Items_Classification__c')
                    .addFieldAsOutput('IsActive')
                    .addFieldAsOutput('NumberOfRevenueInstallments')
                    .addField('Name')
                    .limitRecords(500)
                    .addListener(new SkyEditor2.QueryWhereRegister(Component3, 'SkyEditor2__Text__c', 'Items_Classification__c', new SkyEditor2.TextHolder('eq'), false, true, false))
                    .addListener(new SkyEditor2.QueryWhereRegister(Component6, 'SkyEditor2__Text__c', 'Name', new SkyEditor2.TextHolder('co'), false, true, false))
            );
            
            if (pricebook2Id != null) {
                queryMap.get('Component2').addWhere(' Id in (SELECT Product2Id FROM PricebookEntry WHERE IsActive = true AND Pricebook2Id=\'' + pricebook2Id + '\') ');
            }
            Component2 = new Component2(new List<Product2>(), new List<Component2Item>(), new List<Product2>(), null);
            listItemHolders.put('Component2', Component2);
            
            recordTypeSelector = new SkyEditor2.RecordTypeSelector(Product2.SObjectType);
            
            p_showHeader = false;
            p_sidebar = false;
            presetSystemParams();
            update_Component3_options();
            initSearch();
            
        } catch (SkyEditor2.Errors.SObjectNotFoundException e) {
            SkyEditor2.Messages.addErrorMessage(e.getMessage());
        } catch (SkyEditor2.Errors.FieldNotFoundException e) {
            SkyEditor2.Messages.addErrorMessage(e.getMessage());
        } catch (SkyEditor2.ExtenderException e){
            e.setMessagesToPage();
        } catch (SkyEditor2.Errors.PricebookNotFoundException e) {
            SkyEditor2.Messages.addErrorMessage(e.getMessage());
            hidePageBody = true;
        }
    }

    public List<SelectOption> getOperatorOptions_Product2_Name() {
        return getOperatorOptions('Product2', 'Name');
    }
    

    @isTest(SeeAllData=true)
    private static void testPageMethods() {
        OpportunityCommodity_sub page;
        page = new OpportunityCommodity_sub(new ApexPages.StandardController(new Product2()));
        page.getOperatorOptions_Product2_Name();
        Opportunity[] opps;
        opps = [SELECT Id FROM Opportunity WHERE Pricebook2Id != null LIMIT 1];
        if (opps.size() > 0) {
            System.Test.setCurrentPage(new PageReference('/test?oppId=' + opps[0].Id));
            page = new OpportunityCommodity_sub(new ApexPages.StandardController(new Product2()));
        }
        opps = [SELECT Id FROM Opportunity WHERE Pricebook2Id = null LIMIT 1];
        if (opps.size() > 0) {
            System.Test.setCurrentPage(new PageReference('/test?oppId=' + opps[0].Id));
            page = new OpportunityCommodity_sub(new ApexPages.StandardController(new Product2()));
            System.assert(page.hidePageBody);
            System.assertNotEquals(null, SkyEditor2.Messages.getErrorMessages());
            System.assertNotEquals('', SkyEditor2.Messages.getErrorMessages());
        }
		System.assert(true);
    }
    
    global with sharing class Component2Item extends SkyEditor2.ListItem {
        public Product2 record{get; private set;}
        Component2Item(Component2 holder, Product2 record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(holder);
            if (record.Id == null  && record.RecordTypeId == null){
                if (recordTypeSelector != null) {
                    recordTypeSelector.applyDefault(record);
                }
                
            }
            this.record = record;
        }
        global override SObject getRecord() {return record;}
        public void doDeleteItem(){deleteItem();}
    }
    global with sharing  class Component2 extends SkyEditor2.ListItemHolder {
        public List<Component2Item> items{get; private set;}
        Component2(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(records, items, deleteRecords, recordTypeSelector);
            this.items = (List<Component2Item>)items;
        }
        global override SkyEditor2.ListItem create(SObject data) {
            return new Component2Item(this, (Product2)data, recordTypeSelector);
        }
        public void doDeleteSelectedItems(){deleteSelectedItems();}
    }
    private static testMethod void testComponent2() {
        Component2 Component2 = new Component2(new List<Product2>(), new List<Component2Item>(), new List<Product2>(), null);
        Component2.create(new Product2());
        Component2.doDeleteSelectedItems();
        System.assert(true);
    }
    

    
    public void update_Component3_options() {
        Component3_options = new SelectOption[]{ new SelectOption('', label.none) };
            SObject[] results;
            String soql = 'SELECT Items_Classification__c FROM Product2 WHERE Items_Classification__c != null ';
            if (pricebook2Id != null) {
                soql += 'AND Id in (SELECT Product2Id FROM PricebookEntry WHERE IsActive = true AND Pricebook2Id=\'' + pricebook2Id + '\') ';
            }
            soql += 'GROUP BY Items_Classification__c ORDER BY Items_Classification__c LIMIT 999';
            results = Database.query(soql);
            for (SObject r : results) {
                String value = (String)(r.get('Items_Classification__c'));
                Component3_options.add(new SelectOption(value, value));
            }
    }
    
}