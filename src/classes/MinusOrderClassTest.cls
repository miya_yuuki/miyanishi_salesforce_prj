@isTest(SeeAllData=true)
public class MinusOrderClassTest {
	private static testMethod void testMethods() 
	{
		// 入力チェックを停止
		User stopCheck = new User(Id = Userinfo.getUserId(), StopInputCheck__c = true);
		update stopCheck;
		
		List<Account> testAccounts = new List<Account>();
		
		// 取引先
		Account testAccount = new Account(
			Name = 'テスト株式会社',
			account_localName__c = 'テストフリガナ',
			account_Lead__c = '架電',
			account_Lead2__c = '架電',
			Type = '顧客'
		);
		testAccounts.add(testAccount);
		
		// 取引先（パートナー）
		Account testPartnerAccount = new Account(
			Name = 'テストパートナー株式会社',
			account_localName__c = 'テストパートナーフリガナ',
			account_Lead__c = '架電',
			account_Lead2__c = '架電',
			Type = '顧客'
		);
		testAccounts.add(testPartnerAccount);
		
		insert testAccounts;
		
		// 取引先責任者
		Contact testContact = new Contact(
			FirstName = 'テスト姓',
			LastName = 'テスト名',
			AccountId = testAccount.Id,
			report__c = '送信不要'
		);
		insert testContact;
		
		// 商談
		Opportunity testOpp = new Opportunity(
			Name = 'テスト商談',
			OwnerId = Userinfo.getUserId(),
			AccountId = testAccount.Id,
			Opportunity_Title__c = testContact.Id,
			Opportunity_Type__c = 'セールスP（クライアント請求/初回期間支払）',
			Opportunity_Partner__c = testPartnerAccount.Id,
			Opportunity_lead1__c = '架電',
			Opportunity_lead2__c = '架電',
			CloseDate = System.today().addDays(5),
			StageName = 'D（検討段階）',
			Opportunity_BillEmail__c = 'test@test.com',
			Opportunity_BillRole__c = '役割',
			Opportunity_BillTitle__c = '役職',
			Opportunity_BillPostalCode__c = '999-9999',
			Opportunity_BillPrefecture__c = '都道府県',
			Opportunity_BillPrefecture2__c = '都道府県',
			Opportunity_BillPhone__c = '03-999-9999',
			Opportunity_BillAddress__c = '住所',
			Opportunity_BillCity__c = '市区群'
		);
		insert testOpp;
		
		// 契約
		Contract testContract = new Contract(
			AccountId = testAccount.Id,
			Opportunity__c = testOpp.Id,
			ContractType__c = 'セールスP（クライアント請求/初回期間支払）',
			Contract_Patnername__c = testPartnerAccount.Id,
			Phase__c = '契約中',
			Status = 'ドラフト',
			StartDate = System.today().addDays(5),
			ContractTerm = 6
		);
		insert testContract;
		
		// 注文
		Order__c testOrder = new Order__c(
			Order_Relation__c = testContract.Id,
			Order_Phase__c = '契約中'
		);
		insert testOrder;
		
		// 注文商品
		OrderItem__c testOrderItem1 = new OrderItem__c(
			Item2_Relation__c = testOrder.Id,
			Item2_Keyword_phase__c = '契約中',
			BillingTiming__c = '受注',
			ContractMonths__c = 6,
			Quantity__c = 100,
			Item2_CancelCount__c = 50
		);
		insert testOrderItem1;
		
		// 注文商品
		OrderItem__c testOrderItem2 = new OrderItem__c(
			Item2_Relation__c = testOrder.Id,
			Item2_Keyword_phase__c = '契約中',
			BillingTiming__c = '受注',
			ContractMonths__c = 6,
			Quantity__c = 100,
			Item2_CancelCount__c = 50
		);
		insert testOrderItem2;
		
        //ページのカレント化
        PageReference pageRef = Page.MinusOrder;
        Test.setCurrentPage(pageRef);

		List<OrderItem__c> objOderItem = [SELECT Id FROM OrderItem__c LIMIT 50];
		
		// 初期レコードを取得
        ApexPages.StandardSetController c = new ApexPages.StandardSetController(objOderItem);
        
        // すべてチェック
        c.setSelected(objOderItem);
        
        //テスト対象Visualforceで使うコントローラのオブジェクト化
        MinusOrderClass controller = new MinusOrderClass(c);
        
        // 戻りURLを設定
        ApexPages.currentPage().getParameters().put('retURL', '/' + testOrder.Id);
        
        // 保存
        controller.prcSave();
	}
	
}