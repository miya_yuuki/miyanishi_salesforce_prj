@isTest
private with sharing class CreativeTest{
		private static testMethod void testPageMethods() {	
			Creative page = new Creative(new ApexPages.StandardController(new OpportunityProduct__c()));	
			page.getOperatorOptions_OpportunityProduct_c_CTDay_c();	
			page.getOperatorOptions_OpportunityProduct_c_Name();	
			page.getOperatorOptions_Opportunity_StageName_multi();	
			page.getOperatorOptions_OpportunityProduct_c_CTStatus_c_multi();	
			page.getOperatorOptions_OpportunityProduct_c_Account_c();	
			page.getOperatorOptions_OpportunityProduct_c_RecordTypeName_c();	
			page.getOperatorOptions_OpportunityProduct_c_ProductName_Detail_c();	
			page.getOperatorOptions_OpportunityProduct_c_SalesPerson_c();	
			page.getOperatorOptions_OpportunityProduct_c_OEM_c();	
			page.getOperatorOptions_OpportunityProduct_c_Item_Classification_c();	
			System.assert(true);
		}	
			
	private static testMethod void testComponent2() {
		Creative.Component2 Component2 = new Creative.Component2(new List<OpportunityProduct__c>(), new List<Creative.Component2Item>(), new List<OpportunityProduct__c>(), null);
		Component2.create(new OpportunityProduct__c());
		Component2.doDeleteSelectedItems();
		System.assert(true);
	}
	
}