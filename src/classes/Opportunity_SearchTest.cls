@isTest
private with sharing class Opportunity_SearchTest{
		private static testMethod void testPageMethods() {	
			Opportunity_Search page = new Opportunity_Search(new ApexPages.StandardController(new Opportunity()));	
			page.getOperatorOptions_Opportunity_OwnerId();	
			page.getOperatorOptions_Opportunity_LeadEvaluation_c_multi();	
			page.getOperatorOptions_Opportunity_suggestion_importance_c_multi();	
			page.getOperatorOptions_Opportunity_Opportunity_SalesPerson_Unit_c();	
			page.getOperatorOptions_Opportunity_Opportunity_lead1_c_multi();	
			page.getOperatorOptions_Opportunity_document_c_multi();	
			page.getOperatorOptions_Opportunity_Name();	
			page.getOperatorOptions_Opportunity_this_month_approach_c_multi();	
			page.getOperatorOptions_Opportunity_Opportunity_lead2_c_multi();	
			page.getOperatorOptions_Opportunity_StageName_multi();	
			page.getOperatorOptions_Opportunity_agreement_probability_c_multi();	
			page.getOperatorOptions_Opportunity_AppointmentType_c_multi();	

		Integer defaultSize;

		defaultSize = page.Component2.items.size();
		page.Component2.add();
		System.assertEquals(defaultSize + 1, page.Component2.items.size());
		page.Component2.items[defaultSize].selected = true;
		page.Component2.doDeleteSelectedItems();
			System.assert(true);
		}	
			
	private static testMethod void testComponent2() {
		Opportunity_Search.Component2 Component2 = new Opportunity_Search.Component2(new List<Opportunity>(), new List<Opportunity_Search.Component2Item>(), new List<Opportunity>(), null);
		Component2.setPageItems(new List<Opportunity_Search.Component2Item>());
		Component2.create(new Opportunity());
		Component2.doDeleteSelectedItems();
		Component2.setPagesize(10);		Component2.doFirst();
		Component2.doPrevious();
		Component2.doNext();
		Component2.doLast();
		Component2.doSort();
		System.assert(true);
	}
	
	@isTest
	private static void testLightDataTables(){

		System.assert(true);
	}
}