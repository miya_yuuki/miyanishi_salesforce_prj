global with sharing class RequestSEO extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public CustomObject1__c record {get{return (CustomObject1__c)mainRecord;}}
	public Component644 Component644 {get; private set;}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	{
	setApiVersion(42.0);
	}
	public RequestSEO(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = CustomObject1__c.fields.unit__c;
		f = CustomObject1__c.fields.Name;
		f = CustomObject1__c.fields.OwnerId;
		f = CustomObject1__c.fields.Status__c;
		f = CustomObject1__c.fields.SalesSupportPersonView__c;
		f = CustomObject1__c.fields.RequestDay__c;
		f = CustomObject1__c.fields.RecordTypeId;
		f = CustomObject1__c.fields.RequestEndDay__c;
		f = CustomObject1__c.fields.OrderNo__c;
		f = CustomObject1__c.fields.SupportJudgePerson__c;
		f = CustomObject1__c.fields.AccountName__c;
		f = CustomObject1__c.fields.SupportDay__c;
		f = CustomObject1__c.fields.ProductGroupView__c;
		f = CustomObject1__c.fields.SupportMember__c;
		f = CustomObject1__c.fields.OrderProvisional1__c;
		f = CustomObject1__c.fields.RDDay__c;
		f = CustomObject1__c.fields.OrderProvisionalMemo1__c;
		f = CustomObject1__c.fields.Sign__c;
		f = CustomObject1__c.fields.SalvageScheduledDate__c;
		f = CustomObject1__c.fields.RequestMemo__c;
		f = CustomObject1__c.fields.endday__c;
		f = CustomObject1__c.fields.ClauseDay__c;
		f = CustomObject1__c.fields.CancelType2__c;
		f = CustomObject1__c.fields.CancelType__c;
		f = CustomObject1__c.fields.CancelMonthBilling__c;
		f = CustomObject1__c.fields.RequestAmount__c;
		f = CustomObject1__c.fields.kaiyakuTotalAmount__c;
		f = CustomObject1__c.fields.OrderNoChange__c;
		f = CustomObject1__c.fields.OrderNoChange2__c;
		f = CustomObject1__c.fields.MemoPrint__c;
		f = CustomObject1__c.fields.cancelReasonInServiceFlow__c;
		f = CustomObject1__c.fields.cancelReasonForAP__c;
		f = CustomObject1__c.fields.cancelReasonForCS__c;
		f = CustomObject1__c.fields.externalCancelReason__c;
		f = CustomObject1__c.fields.cancelReasonErrorDetail__c;
		f = CustomObject1__c.fields.cancelReasonFactorDetail__c;
		f = CustomObject1__c.fields.OrderItem_AutoCompletion__c;
		f = SEOChange__c.fields.OrderItem__c;
		f = SEOChange__c.fields.ProductName2__c;
		f = SEOChange__c.fields.Amount2__c;
		f = SEOChange__c.fields.CancelableAmount__c;
		f = SEOChange__c.fields.Item2_ServiceDate__c;
		f = SEOChange__c.fields.Item2_EndData__c;
		f = SEOChange__c.fields.KeywordType1__c;
		f = SEOChange__c.fields.Keyword2__c;
		f = SEOChange__c.fields.URL2__c;
		f = CustomObject1__c.fields.ManualSetting_CancelTotalAmount__c;
		f = CustomObject1__c.fields.Skip_OrderItemCheck1__c;
		f = CustomObject1__c.fields.Skip_OrderItemCheck3__c;
		f = CustomObject1__c.fields.ManualSetting_CancelMonthRequestAmount__c;
		f = CustomObject1__c.fields.Skip_OrderItemCheck2__c;
		f = CustomObject1__c.fields.Skip_OrderItemCheck4__c;
		f = Order__c.fields.Order_EndDate__c;
		f = OrderItem__c.fields.Item2_Product_naiyou__c;
		f = OrderItem__c.fields.TotalPrice__c;
		f = OrderItem__c.fields.CancelableAmount__c;
		f = OrderItem__c.fields.Item2_ServiceDate__c;
		f = OrderItem__c.fields.Item2_EndData__c;
		f = OrderItem__c.fields.OrderItem_keywordType__c;
		f = OrderItem__c.fields.Item2_Keyword_strategy_keyword__c;
		f = OrderItem__c.fields.OrderItem_URL__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = CustomObject1__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'RequestSEO';
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(CustomObject1__c.SObjectType);
			mainQuery = new SkyEditor2.Query('CustomObject1__c');
			mainQuery.addField('Status__c');
			mainQuery.addField('OrderNo__c');
			mainQuery.addField('OrderProvisional1__c');
			mainQuery.addField('OrderProvisionalMemo1__c');
			mainQuery.addField('Sign__c');
			mainQuery.addField('SalvageScheduledDate__c');
			mainQuery.addField('RequestMemo__c');
			mainQuery.addField('endday__c');
			mainQuery.addField('ClauseDay__c');
			mainQuery.addField('CancelType2__c');
			mainQuery.addField('CancelType__c');
			mainQuery.addField('CancelMonthBilling__c');
			mainQuery.addField('RequestAmount__c');
			mainQuery.addField('kaiyakuTotalAmount__c');
			mainQuery.addField('OrderNoChange__c');
			mainQuery.addField('OrderNoChange2__c');
			mainQuery.addField('MemoPrint__c');
			mainQuery.addField('cancelReasonInServiceFlow__c');
			mainQuery.addField('cancelReasonForAP__c');
			mainQuery.addField('cancelReasonForCS__c');
			mainQuery.addField('externalCancelReason__c');
			mainQuery.addField('cancelReasonErrorDetail__c');
			mainQuery.addField('cancelReasonFactorDetail__c');
			mainQuery.addField('OrderItem_AutoCompletion__c');
			mainQuery.addField('ManualSetting_CancelTotalAmount__c');
			mainQuery.addField('Skip_OrderItemCheck1__c');
			mainQuery.addField('Skip_OrderItemCheck3__c');
			mainQuery.addField('ManualSetting_CancelMonthRequestAmount__c');
			mainQuery.addField('Skip_OrderItemCheck2__c');
			mainQuery.addField('Skip_OrderItemCheck4__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('unit__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('OwnerId');
			mainQuery.addFieldAsOutput('SalesSupportPersonView__c');
			mainQuery.addFieldAsOutput('RequestDay__c');
			mainQuery.addFieldAsOutput('RecordType.Name');
			mainQuery.addFieldAsOutput('RequestEndDay__c');
			mainQuery.addFieldAsOutput('SupportJudgePerson__c');
			mainQuery.addFieldAsOutput('AccountName__c');
			mainQuery.addFieldAsOutput('SupportDay__c');
			mainQuery.addFieldAsOutput('ProductGroupView__c');
			mainQuery.addFieldAsOutput('SupportMember__c');
			mainQuery.addFieldAsOutput('RDDay__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			Component644 = new Component644(new List<SEOChange__c>(), new List<Component644Item>(), new List<SEOChange__c>(), null);
			listItemHolders.put('Component644', Component644);
			query = new SkyEditor2.Query('SEOChange__c');
			query.addField('OrderItem__c');
			query.addFieldAsOutput('ProductName2__c');
			query.addFieldAsOutput('Amount2__c');
			query.addFieldAsOutput('CancelableAmount__c');
			query.addFieldAsOutput('Item2_ServiceDate__c');
			query.addFieldAsOutput('Item2_EndData__c');
			query.addFieldAsOutput('KeywordType1__c');
			query.addFieldAsOutput('Keyword2__c');
			query.addFieldAsOutput('URL2__c');
			query.addFieldAsOutput('RecordTypeId');
			query.addWhere('sinsei__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('Component644', 'sinsei__c');
			Component644.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('Component644', query);
			registRelatedList('sinsei__r', 'Component644');
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			Component644.extender = this.extender;
			if (record.Id == null) {
				saveOldValues();
				if(record.RecordTypeId == null) recordTypeSelector.applyDefault(record);
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class Component644Item extends SkyEditor2.ListItem {
		public SEOChange__c record{get; private set;}
		@TestVisible
		Component644Item(Component644 holder, SEOChange__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}

	public void loadReferenceValues_Component653() {
		if (record.OrderItem__c == null) {
if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.ProductName2__c)) {
				record.ProductName2__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.Amount2__c)) {
				record.Amount2__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.CancelableAmount__c)) {
				record.CancelableAmount__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.Item2_ServiceDate__c)) {
				record.Item2_ServiceDate__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.Item2_EndData__c)) {
				record.Item2_EndData__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.KeywordType1__c)) {
				record.KeywordType1__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.Keyword2__c)) {
				record.Keyword2__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.URL2__c)) {
				record.URL2__c = null;
			}
				return;
		}
		OrderItem__c[] referenceTo = [SELECT Item2_Product_naiyou__c,TotalPrice__c,CancelableAmount__c,Item2_ServiceDate__c,Item2_EndData__c,OrderItem_keywordType__c,Item2_Keyword_strategy_keyword__c,OrderItem_URL__c FROM OrderItem__c WHERE Id=:record.OrderItem__c];
		if (referenceTo.size() == 0) {
			record.OrderItem__c.addError(SkyEditor2.Messages.referenceDataNotFound(record.OrderItem__c));
			return;
		}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.ProductName2__c)) {
			record.ProductName2__c = referenceTo[0].Item2_Product_naiyou__c;
		}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.Amount2__c)) {
			record.Amount2__c = referenceTo[0].TotalPrice__c;
		}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.CancelableAmount__c)) {
			record.CancelableAmount__c = referenceTo[0].CancelableAmount__c;
		}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.Item2_ServiceDate__c)) {
			record.Item2_ServiceDate__c = referenceTo[0].Item2_ServiceDate__c;
		}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.Item2_EndData__c)) {
			record.Item2_EndData__c = referenceTo[0].Item2_EndData__c;
		}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.KeywordType1__c)) {
			record.KeywordType1__c = referenceTo[0].OrderItem_keywordType__c;
		}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.Keyword2__c)) {
			record.Keyword2__c = referenceTo[0].Item2_Keyword_strategy_keyword__c;
		}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.URL2__c)) {
			record.URL2__c = referenceTo[0].OrderItem_URL__c;
		}
		
	}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component644 extends SkyEditor2.ListItemHolder {
		public List<Component644Item> items{get; private set;}
		@TestVisible
			Component644(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component644Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component644Item(this, (SEOChange__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
		global override void importByJSON() {
			List<Object> data = (List<Object>) System.JSON.deserializeUntyped(hiddenValue);
			super.importByJSON(data);
			for (Integer n = items.size() - data.size(); n < items.size(); n++) {
				Component644Item i = items[n];
				if (i.record.OrderItem__c != null) {
					i.loadReferenceValues_Component653();
				}
			}
		}
	}

	public void loadReferenceValues_Component413() {
		if (record.OrderNo__c == null) {
if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.endday__c)) {
				record.endday__c = null;
			}
				return;
		}
		Order__c[] referenceTo = [SELECT Order_EndDate__c FROM Order__c WHERE Id=:record.OrderNo__c];
		if (referenceTo.size() == 0) {
			record.OrderNo__c.addError(SkyEditor2.Messages.referenceDataNotFound(record.OrderNo__c));
			return;
		}
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.endday__c)) {
			record.endday__c = referenceTo[0].Order_EndDate__c;
		}
		
	}
	 public void callRemove_Component644() {
		  for(Integer i = Component644.items.size() - 1; i >= 0; i--){
			   Component644Item item = Component644.items[i];
			   if(item.selected){
					item.remove();
			   }
		  }
	 }
	
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}