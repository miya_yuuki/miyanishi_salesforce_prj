global with sharing class Opportunity_introduction_view extends SkyEditor2.SkyEditorPageBaseWithSharing {
    
    public OpportunityProduct__c record {get{return (OpportunityProduct__c)mainRecord;}}
    public with sharing class CanvasException extends Exception {}

    
    
    public Opportunity_introduction_view(ApexPages.StandardController controller) {
        super(controller);


        SObjectField f;

        f = OpportunityProduct__c.fields.Opportunity__c;
        f = OpportunityProduct__c.fields.Product__c;
        f = OpportunityProduct__c.fields.Item_Detail_Product__c;
        f = OpportunityProduct__c.fields.ProductType__c;
        f = OpportunityProduct__c.fields.Opportunity_DeliveryExistence__c;
        f = OpportunityProduct__c.fields.Update__c;
        f = OpportunityProduct__c.fields.DeliveryStandards__c;
        f = OpportunityProduct__c.fields.Name;
        f = OpportunityProduct__c.fields.Price__c;
        f = OpportunityProduct__c.fields.Quantity__c;
        f = OpportunityProduct__c.fields.TotalPrice__c;
        f = OpportunityProduct__c.fields.Item_Entry_date__c;
        f = OpportunityProduct__c.fields.Item_Special_instruction__c;

        List<RecordTypeInfo> recordTypes;
        try {
            mainSObjectType = OpportunityProduct__c.SObjectType;
            setPageReferenceFactory(new PageReferenceFactory());
            
            mainQuery = new SkyEditor2.Query('OpportunityProduct__c');
            mainQuery.addFieldAsOutput('RecordTypeId');
            mainQuery.addFieldAsOutput('Opportunity__c');
            mainQuery.addFieldAsOutput('Product__c');
            mainQuery.addFieldAsOutput('Item_Detail_Product__c');
            mainQuery.addFieldAsOutput('ProductType__c');
            mainQuery.addFieldAsOutput('Opportunity_DeliveryExistence__c');
            mainQuery.addFieldAsOutput('Update__c');
            mainQuery.addFieldAsOutput('DeliveryStandards__c');
            mainQuery.addFieldAsOutput('Name');
            mainQuery.addFieldAsOutput('Price__c');
            mainQuery.addFieldAsOutput('Quantity__c');
            mainQuery.addFieldAsOutput('TotalPrice__c');
            mainQuery.addFieldAsOutput('Item_Entry_date__c');
            mainQuery.addFieldAsOutput('Item_Special_instruction__c');
            mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
                .limitRecords(1);
            
            
            
            mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
            
            queryMap = new Map<String, SkyEditor2.Query>();
            SkyEditor2.Query query;
            
            
            p_showHeader = true;
            p_sidebar = true;
            addInheritParameter('Opportunity__c', 'CF00N10000005jJYH_lkid');
            addInheritParameter('RecordTypeId', 'RecordType');
            init();
            
            if (record.Id == null) {
                
                saveOldValues();
                
            }

            
            
        }  catch (SkyEditor2.Errors.FieldNotFoundException e) {
            fieldNotFound(e);
        } catch (SkyEditor2.Errors.RecordNotFoundException e) {
            recordNotFound(e);
        } catch (SkyEditor2.ExtenderException e) {
            e.setMessagesToPage();
        }
    }
    

    private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    
    private static testMethod void testPageMethods() {        Opportunity_introduction_view extension = new Opportunity_introduction_view(new ApexPages.StandardController(new OpportunityProduct__c()));
        SkyEditor2.Messages.clear();
        extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
        SkyEditor2.Messages.clear();
        extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
        SkyEditor2.Messages.clear();
        extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

        Integer defaultSize;
    }
    with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
        public PageReference newPageReference(String url) {
            return new PageReference(url);
        }
    }
}