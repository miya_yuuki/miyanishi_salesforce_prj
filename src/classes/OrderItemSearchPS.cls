global with sharing class OrderItemSearchPS extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public OrderItem__c record{get;set;}	
			
	
		public Component2 Component2 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component14_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component14_to{get;set;}	
			
		public OrderItem__c Component168_val {get;set;}	
		public SkyEditor2.TextHolder Component168_op{get;set;}	
			
		public OrderItem__c Component254_val {get;set;}	
		public SkyEditor2.TextHolder Component254_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component19_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component19_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component19_op{get;set;}	
		public List<SelectOption> valueOptions_OrderItem_c_Item2_Keyword_phase_c_multi {get;set;}
			
		public OrderItem__c Component69_val {get;set;}	
		public SkyEditor2.TextHolder Component69_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component21_val {get;set;}	
		public SkyEditor2.TextHolder Component21_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component73_val {get;set;}	
		public SkyEditor2.TextHolder Component73_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component116_val {get;set;}	
		public SkyEditor2.TextHolder Component116_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component258_val {get;set;}	
		public SkyEditor2.TextHolder Component258_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component206_val {get;set;}	
		public SkyEditor2.TextHolder Component206_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component260_val {get;set;}	
		public SkyEditor2.TextHolder Component260_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component176_val {get;set;}	
		public SkyEditor2.TextHolder Component176_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component178_val {get;set;}	
		public SkyEditor2.TextHolder Component178_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component174_val {get;set;}	
		public SkyEditor2.TextHolder Component174_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component180_val {get;set;}	
		public SkyEditor2.TextHolder Component180_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component184_val {get;set;}	
		public SkyEditor2.TextHolder Component184_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component182_val {get;set;}	
		public SkyEditor2.TextHolder Component182_op{get;set;}	
			
	public String recordTypeRecordsJSON_OrderItem_c {get; private set;}
	public String defaultRecordTypeId_OrderItem_c {get; private set;}
	public String metadataJSON_OrderItem_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public OrderItemSearchPS(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = OrderItem__c.fields.SalesPerson1__c;
		f = OrderItem__c.fields.DeliveryPerson__c;
		f = OrderItem__c.fields.Item2_Keyword_phase__c;
		f = OrderItem__c.fields.Item2_Relation__c;
		f = OrderItem__c.fields.Item2_Product_naiyou__c;
		f = OrderItem__c.fields.Account__c;
		f = OrderItem__c.fields.Item2_Keyword_strategy_keyword__c;
		f = OrderItem__c.fields.Opportunity_Competition__c;
		f = OrderItem__c.fields.OrderItem_URL__c;
		f = OrderItem__c.fields.sisakukigyou__c;
		f = OrderItem__c.fields.ServiceDate__c;
		f = OrderItem__c.fields.Item2_ServiceDate__c;
		f = OrderItem__c.fields.EndDate__c;
		f = OrderItem__c.fields.Item2_EndData__c;
		f = OrderItem__c.fields.Bill_Main__c;
		f = OrderItem__c.fields.Item2_CancellationDay__c;
		f = OrderItem__c.fields.Item2_ProductCount__c;
		f = OrderItem__c.fields.Item2_Product__c;
		f = OrderItem__c.fields.Item2_Detail_Productlist__c;
		f = OrderItem__c.fields.OrderItem_keywordType__c;
		f = OrderItem__c.fields.Item2_Estimate_level__c;
		f = OrderItem__c.fields.Item2_Keyword_letter_count__c;
		f = OrderItem__c.fields.Item2_Unit_selling_price__c;
		f = OrderItem__c.fields.Quantity__c;
		f = OrderItem__c.fields.TotalPrice1__c;
		f = OrderItem__c.fields.StartDate_EndDatec2__c;
 f = OrderItem__c.fields.Item2_ProductCount__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = OrderItem__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component14_from = new SkyEditor2__SkyEditorDummy__c();	
				Component14_to = new SkyEditor2__SkyEditorDummy__c();	
					
				OrderItem__c lookupObjComponent27 = new OrderItem__c();	
				Component168_val = lookupObjComponent27;	
				Component168_op = new SkyEditor2.TextHolder();	
					
				Component254_val = lookupObjComponent27;	
				Component254_op = new SkyEditor2.TextHolder();	
					
				Component19_val = new SkyEditor2__SkyEditorDummy__c();	
				Component19_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component19_op = new SkyEditor2.TextHolder();	
				valueOptions_OrderItem_c_Item2_Keyword_phase_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : OrderItem__c.Item2_Keyword_phase__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_OrderItem_c_Item2_Keyword_phase_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component69_val = lookupObjComponent27;	
				Component69_op = new SkyEditor2.TextHolder();	
					
				Component21_val = new SkyEditor2__SkyEditorDummy__c();	
				Component21_op = new SkyEditor2.TextHolder();	
					
				Component73_val = new SkyEditor2__SkyEditorDummy__c();	
				Component73_op = new SkyEditor2.TextHolder();	
					
				Component116_val = new SkyEditor2__SkyEditorDummy__c();	
				Component116_op = new SkyEditor2.TextHolder();	
					
				Component258_val = new SkyEditor2__SkyEditorDummy__c();	
				Component258_op = new SkyEditor2.TextHolder();	
					
				Component206_val = new SkyEditor2__SkyEditorDummy__c();	
				Component206_op = new SkyEditor2.TextHolder();	
					
				Component260_val = new SkyEditor2__SkyEditorDummy__c();	
				Component260_op = new SkyEditor2.TextHolder();	
					
				Component176_val = new SkyEditor2__SkyEditorDummy__c();	
				Component176_op = new SkyEditor2.TextHolder();	
					
				Component178_val = new SkyEditor2__SkyEditorDummy__c();	
				Component178_op = new SkyEditor2.TextHolder();	
					
				Component174_val = new SkyEditor2__SkyEditorDummy__c();	
				Component174_op = new SkyEditor2.TextHolder();	
					
				Component180_val = new SkyEditor2__SkyEditorDummy__c();	
				Component180_op = new SkyEditor2.TextHolder();	
					
				Component184_val = new SkyEditor2__SkyEditorDummy__c();	
				Component184_op = new SkyEditor2.TextHolder();	
					
				Component182_val = new SkyEditor2__SkyEditorDummy__c();	
				Component182_op = new SkyEditor2.TextHolder();	
					
				queryMap.put(	
					'Component2',	
					new SkyEditor2.Query('OrderItem__c')
						.addFieldAsOutput('Item2_Relation__c')
						.addFieldAsOutput('Item2_ProductCount__c')
						.addFieldAsOutput('Item2_Keyword_phase__c')
						.addFieldAsOutput('Account__c')
						.addFieldAsOutput('sisakukigyou__c')
						.addFieldAsOutput('Opportunity_Competition__c')
						.addFieldAsOutput('Item2_Product__c')
						.addFieldAsOutput('Item2_Detail_Productlist__c')
						.addFieldAsOutput('Item2_Keyword_strategy_keyword__c')
						.addFieldAsOutput('OrderItem_keywordType__c')
						.addFieldAsOutput('Item2_Estimate_level__c')
						.addFieldAsOutput('OrderItem_URL__c')
						.addFieldAsOutput('Item2_Keyword_letter_count__c')
						.addFieldAsOutput('Item2_Unit_selling_price__c')
						.addFieldAsOutput('Quantity__c')
						.addFieldAsOutput('TotalPrice1__c')
						.addFieldAsOutput('StartDate_EndDatec2__c')
						.addFieldAsOutput('SalesPerson1__c')
						.addFieldAsOutput('DeliveryPerson__c')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component14_from, 'SkyEditor2__Text__c', 'Item2_ProductCount__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component14_to, 'SkyEditor2__Text__c', 'Item2_ProductCount__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component168_val, 'SalesPerson1__c', 'SalesPerson1__c', Component168_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component254_val, 'DeliveryPerson__c', 'DeliveryPerson__c', Component254_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component19_val_dummy, 'SkyEditor2__Text__c','Item2_Keyword_phase__c', Component19_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component69_val, 'Item2_Relation__c', 'Item2_Relation__c', Component69_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component21_val, 'SkyEditor2__Text__c', 'Item2_Product_naiyou__c', Component21_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component73_val, 'SkyEditor2__Text__c', 'Account__c', Component73_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component116_val, 'SkyEditor2__Text__c', 'Item2_Keyword_strategy_keyword__c', Component116_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component258_val, 'SkyEditor2__Text__c', 'Opportunity_Competition__c', Component258_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component206_val, 'SkyEditor2__Text__c', 'OrderItem_URL__c', Component206_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component260_val, 'SkyEditor2__Text__c', 'sisakukigyou__c', Component260_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component176_val, 'SkyEditor2__Date__c', 'ServiceDate__c', Component176_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component178_val, 'SkyEditor2__Date__c', 'Item2_ServiceDate__c', Component178_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component174_val, 'SkyEditor2__Date__c', 'EndDate__c', Component174_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component180_val, 'SkyEditor2__Date__c', 'Item2_EndData__c', Component180_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component184_val, 'SkyEditor2__Date__c', 'Bill_Main__c', Component184_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component182_val, 'SkyEditor2__Date__c', 'Item2_CancellationDay__c', Component182_op, true, 0, false ))
				);	
					
					Component2 = new Component2(new List<OrderItem__c>(), new List<Component2Item>(), new List<OrderItem__c>(), null);
				listItemHolders.put('Component2', Component2);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(OrderItem__c.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = true;
			presetSystemParams();
			Component2.extender = this.extender;
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_OrderItem_c_SalesPerson1_c() { 
			return getOperatorOptions('OrderItem__c', 'SalesPerson1__c');	
		}	
		public List<SelectOption> getOperatorOptions_OrderItem_c_DeliveryPerson_c() { 
			return getOperatorOptions('OrderItem__c', 'DeliveryPerson__c');	
		}	
		public List<SelectOption> getOperatorOptions_OrderItem_c_Item2_Keyword_phase_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_OrderItem_c_Item2_Relation_c() { 
			return getOperatorOptions('OrderItem__c', 'Item2_Relation__c');	
		}	
		public List<SelectOption> getOperatorOptions_OrderItem_c_Item2_Product_naiyou_c() { 
			return getOperatorOptions('OrderItem__c', 'Item2_Product_naiyou__c');	
		}	
		public List<SelectOption> getOperatorOptions_OrderItem_c_Account_c() { 
			return getOperatorOptions('OrderItem__c', 'Account__c');	
		}	
		public List<SelectOption> getOperatorOptions_OrderItem_c_Item2_Keyword_strategy_keyword_c() { 
			return getOperatorOptions('OrderItem__c', 'Item2_Keyword_strategy_keyword__c');	
		}	
		public List<SelectOption> getOperatorOptions_OrderItem_c_Opportunity_Competition_c() { 
			return getOperatorOptions('OrderItem__c', 'Opportunity_Competition__c');	
		}	
		public List<SelectOption> getOperatorOptions_OrderItem_c_OrderItem_URL_c() { 
			return getOperatorOptions('OrderItem__c', 'OrderItem_URL__c');	
		}	
		public List<SelectOption> getOperatorOptions_OrderItem_c_sisakukigyou_c() { 
			return getOperatorOptions('OrderItem__c', 'sisakukigyou__c');	
		}	
		public List<SelectOption> getOperatorOptions_OrderItem_c_ServiceDate_c() { 
			return getOperatorOptions('OrderItem__c', 'ServiceDate__c');	
		}	
		public List<SelectOption> getOperatorOptions_OrderItem_c_Item2_ServiceDate_c() { 
			return getOperatorOptions('OrderItem__c', 'Item2_ServiceDate__c');	
		}	
		public List<SelectOption> getOperatorOptions_OrderItem_c_EndDate_c() { 
			return getOperatorOptions('OrderItem__c', 'EndDate__c');	
		}	
		public List<SelectOption> getOperatorOptions_OrderItem_c_Item2_EndData_c() { 
			return getOperatorOptions('OrderItem__c', 'Item2_EndData__c');	
		}	
		public List<SelectOption> getOperatorOptions_OrderItem_c_Bill_Main_c() { 
			return getOperatorOptions('OrderItem__c', 'Bill_Main__c');	
		}	
		public List<SelectOption> getOperatorOptions_OrderItem_c_Item2_CancellationDay_c() { 
			return getOperatorOptions('OrderItem__c', 'Item2_CancellationDay__c');	
		}	
			
			
	global with sharing class Component2Item extends SkyEditor2.ListItem {
		public OrderItem__c record{get; private set;}
		@TestVisible
		Component2Item(Component2 holder, OrderItem__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component2 extends SkyEditor2.ListItemHolder {
		public List<Component2Item> items{get; private set;}
		@TestVisible
			Component2(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component2Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component2Item(this, (OrderItem__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	public OrderItem__c Component2_table_Conversion { get { return new OrderItem__c();}}
	
	public String Component2_table_selectval { get; set; }
	
	
			
	}