@isTest
private with sharing class RequestSupportUnitTest{
		private static testMethod void testPageMethods() {	
			RequestSupportUnit page = new RequestSupportUnit(new ApexPages.StandardController(new CustomObject1__c()));	
			page.getOperatorOptions_CustomObject1_c_OrderNo_c();	
			page.getOperatorOptions_CustomObject1_c_OwnerId();	
			page.getOperatorOptions_CustomObject1_c_RecordTypeId();	
			page.getOperatorOptions_CustomObject1_c_AccountName_c();	
			page.getOperatorOptions_CustomObject1_c_StatusSupport_c_multi();	
			page.getOperatorOptions_CustomObject1_c_Status_c_multi();	
			page.getOperatorOptions_CustomObject1_c_SupportJudgePerson_c();	
			page.getOperatorOptions_CustomObject1_c_SupportMember_c_multi();	
			page.getOperatorOptions_CustomObject1_c_NextCheck_c_multi();	
			page.getOperatorOptions_CustomObject1_c_ProductGroup_c_multi();	
			page.getOperatorOptions_CustomObject1_c_Name();	
			System.assert(true);
		}	
			
	private static testMethod void testComponent3() {
		RequestSupportUnit.Component3 Component3 = new RequestSupportUnit.Component3(new List<CustomObject1__c>(), new List<RequestSupportUnit.Component3Item>(), new List<CustomObject1__c>(), new SkyEditor2.RecordTypeSelector(CustomObject1__c.SObjectType));
		Component3.setPageItems(new List<RequestSupportUnit.Component3Item>());
		Component3.create(new CustomObject1__c());
		Component3.doDeleteSelectedItems();
		Component3.setPagesize(10);		Component3.doFirst();
		Component3.doPrevious();
		Component3.doNext();
		Component3.doLast();
		Component3.doSort();
		System.assert(true);
	}
	
}