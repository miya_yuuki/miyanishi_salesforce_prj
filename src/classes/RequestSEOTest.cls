@isTest
private with sharing class RequestSEOTest{
	private static testMethod void testPageMethods() {		RequestSEO extension = new RequestSEO(new ApexPages.StandardController(new CustomObject1__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;

		defaultSize = extension.Component644.items.size();
		extension.Component644.add();
		System.assertEquals(defaultSize + 1, extension.Component644.items.size());
		extension.Component644.items[defaultSize].selected = true;
		extension.callRemove_Component644();
	}
	private static testMethod void testComponent644() {
		RequestSEO.Component644 Component644 = new RequestSEO.Component644(new List<SEOChange__c>(), new List<RequestSEO.Component644Item>(), new List<SEOChange__c>(), null);
		Component644.create(new SEOChange__c());
		System.assert(true);
	}
	
	public static testMethod void test_importByJSON_Component644() {
		RequestSEO.Component644 table = new RequestSEO.Component644(
			new List<SEOChange__c>(),
			new List<RequestSEO.Component644Item>(),
			new List<SEOChange__c>(), null);
		table.hiddenValue = '[{}]';
		table.importByJSON();
		System.assert(true);
	}
	@isTest
	private static void testLightDataTables(){

		System.assert(true);
	}

	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component413() {
		String testReferenceId = '';
		RequestSEO extension = new RequestSEO(new ApexPages.StandardController(new CustomObject1__c()));
		extension.loadReferenceValues_Component413();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(Order__c.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<Order__c> parents = [SELECT Id FROM Order__c LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		Order__c parent = [SELECT Id,Order_EndDate__c FROM Order__c WHERE Id = :testReferenceId];
		extension.record.OrderNo__c = parent.Id;
		extension.loadReferenceValues_Component413();
				
		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.endday__c)) {
			System.assertEquals(parent.Order_EndDate__c, extension.record.endday__c);
		}

		System.assert(true);
	}

	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component653() {
		String testReferenceId = '';
		RequestSEO.Component644 table = new RequestSEO.Component644(new List<SEOChange__c>(), new List<RequestSEO.Component644Item>(), new List<SEOChange__c>(), null);
		table.add();
		RequestSEO.Component644Item item = table.items[0];
		item.loadReferenceValues_Component653();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(OrderItem__c.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<OrderItem__c> parents = [SELECT Id FROM OrderItem__c LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		OrderItem__c parent = [SELECT Id,Item2_Product_naiyou__c,TotalPrice__c,CancelableAmount__c,Item2_ServiceDate__c,Item2_EndData__c,OrderItem_keywordType__c,Item2_Keyword_strategy_keyword__c,OrderItem_URL__c FROM OrderItem__c WHERE Id = :testReferenceId];
		item.record.OrderItem__c = parent.Id;
		item.loadReferenceValues_Component653();
				
		if (SkyEditor2.Util.isEditable(item.record, SEOChange__c.fields.ProductName2__c)) {
			System.assertEquals(parent.Item2_Product_naiyou__c, item.record.ProductName2__c);
		}


		if (SkyEditor2.Util.isEditable(item.record, SEOChange__c.fields.Amount2__c)) {
			System.assertEquals(parent.TotalPrice__c, item.record.Amount2__c);
		}


		if (SkyEditor2.Util.isEditable(item.record, SEOChange__c.fields.CancelableAmount__c)) {
			System.assertEquals(parent.CancelableAmount__c, item.record.CancelableAmount__c);
		}


		if (SkyEditor2.Util.isEditable(item.record, SEOChange__c.fields.Item2_ServiceDate__c)) {
			System.assertEquals(parent.Item2_ServiceDate__c, item.record.Item2_ServiceDate__c);
		}


		if (SkyEditor2.Util.isEditable(item.record, SEOChange__c.fields.Item2_EndData__c)) {
			System.assertEquals(parent.Item2_EndData__c, item.record.Item2_EndData__c);
		}


		if (SkyEditor2.Util.isEditable(item.record, SEOChange__c.fields.KeywordType1__c)) {
			System.assertEquals(parent.OrderItem_keywordType__c, item.record.KeywordType1__c);
		}


		if (SkyEditor2.Util.isEditable(item.record, SEOChange__c.fields.Keyword2__c)) {
			System.assertEquals(parent.Item2_Keyword_strategy_keyword__c, item.record.Keyword2__c);
		}


		if (SkyEditor2.Util.isEditable(item.record, SEOChange__c.fields.URL2__c)) {
			System.assertEquals(parent.OrderItem_URL__c, item.record.URL2__c);
		}

		System.assert(true);
	}
}