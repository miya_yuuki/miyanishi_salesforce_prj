global with sharing class LeadSearchMarketing extends SkyEditor2.SkyEditorPageBaseWithSharing {	
	    	
	    public Lead record{get;set;}	
	    	
    
	    public Component2 Component2 {get; private set;}	
	    	
	    public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
	    	
	    public SkyEditor2__SkyEditorDummy__c Component15_val {get;set;}	
        public SkyEditor2.TextHolder Component15_op{get;set;}	
	    	
	    public Lead Component71_val {get;set;}	
        public SkyEditor2.TextHolder Component71_op{get;set;}	
	    	
	    public SkyEditor2__SkyEditorDummy__c Component160_val {get;set;}	
        public SkyEditor2.TextHolder Component160_op{get;set;}	
	    	
	    public SkyEditor2__SkyEditorDummy__c Component171_val {get;set;}	
        public SkyEditor2.TextHolder Component171_op{get;set;}	
	    	
	    public SkyEditor2__SkyEditorDummy__c Component173_val {get;set;}	
        public SkyEditor2.TextHolder Component173_op{get;set;}	
	    	
	    public SkyEditor2__SkyEditorDummy__c Component168_val {get;set;}	
        public SkyEditor2.TextHolder Component168_op{get;set;}	
	    	
    public String recordTypeRecordsJSON_Lead {get; private set;}
    public String defaultRecordTypeId_Lead {get; private set;}
    public String metadataJSON_Lead {get; private set;}
    public String picklistValuesJSON_Lead_Salutation {get; private set;}
    public String picklistValuesJSON_Lead_LeadSource {get; private set;}
    public String picklistValuesJSON_Lead_Status {get; private set;}
    public String picklistValuesJSON_Lead_Industry {get; private set;}
    public String picklistValuesJSON_Lead_Rating {get; private set;}
    public String picklistValuesJSON_Lead_lead_ContentsCompetition_c {get; private set;}
    public String picklistValuesJSON_Lead_Contact_Role_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_Industry_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_Industry2_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_Industrycategory_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_Industrycategory2_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_closing_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_BusinessModel_c {get; private set;}
    public String picklistValuesJSON_Lead_Status1_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_lead1_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_lead2_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_ZOHOUser_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_Relation_c {get; private set;}
    public String picklistValuesJSON_Lead_Status2_c {get; private set;}
    public String picklistValuesJSON_Lead_Lead_WEBsite_Quality_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_WebPromotion_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_CompanySize_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_AttackProduct_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_Rival_c {get; private set;}
    public String picklistValuesJSON_Lead_FTP_ana_c {get; private set;}
	    public LeadSearchMarketing(ApexPages.StandardController controller) {	
	        super(controller);	

            SObjectField f;

            f = Lead.fields.Company;
            f = Lead.fields.OwnerId;
            f = Lead.fields.tag__c;
            f = Lead.fields.SmartViscal__NameCardListName__c;
            f = Lead.fields.Email;
            f = Lead.fields.Marketodouki__c;
            f = Lead.fields.Name;
            f = Lead.fields.Website;
            f = Lead.fields.Phone;
            f = Lead.fields.LastModifiedDate;

        List<RecordTypeInfo> recordTypes;
	        try {	
	            	
	            mainRecord = null;	
	            mainSObjectType = Lead.SObjectType;	
	            	
	            	
	            mode = SkyEditor2.LayoutMode.TempSearch_01; 
	            	
	            Lead lookupObjComponent47 = new Lead();	
	            Component15_val = new SkyEditor2__SkyEditorDummy__c();	
	            Component15_op = new SkyEditor2.TextHolder();	
	            	
	            Component71_val = lookupObjComponent47;	
	            Component71_op = new SkyEditor2.TextHolder();	
	            	
	            Component160_val = new SkyEditor2__SkyEditorDummy__c();	
	            Component160_op = new SkyEditor2.TextHolder();	
	            	
	            Component171_val = new SkyEditor2__SkyEditorDummy__c();	
	            Component171_op = new SkyEditor2.TextHolder();	
	            	
	            Component173_val = new SkyEditor2__SkyEditorDummy__c();	
	            Component173_op = new SkyEditor2.TextHolder();	
	            	
	            Component168_val = new SkyEditor2__SkyEditorDummy__c();	
	            Component168_op = new SkyEditor2.TextHolder();	
	            	
	            queryMap.put(	
	                'Component2',	
	                new SkyEditor2.Query('Lead')	
	                    .addField('Marketodouki__c')
	                    .addFieldAsOutput('OwnerId')
	                    .addFieldAsOutput('Company')
	                    .addFieldAsOutput('Name')
	                    .addFieldAsOutput('Website')
	                    .addFieldAsOutput('Phone')
	                    .addFieldAsOutput('LastModifiedDate')
                        .addFieldAsOutput('RecordTypeId')
	                    .limitRecords(500)	
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component15_val, 'SkyEditor2__Text__c', 'Company', Component15_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component71_val, 'OwnerId', 'OwnerId', Component71_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component160_val, 'SkyEditor2__Text__c', 'tag__c', Component160_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component171_val, 'SkyEditor2__Text__c', 'SmartViscal__NameCardListName__c', Component171_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component173_val, 'SkyEditor2__Text__c', 'Email', Component173_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component168_val, 'SkyEditor2__Checkbox__c', 'Marketodouki__c', Component168_op, true, 0, false ))
	            );	
	            	
	                Component2 = new Component2(new List<Lead>(), new List<Component2Item>(), new List<Lead>(), null);
                 Component2.setPageItems(new List<Component2Item>());
                 Component2.setPageSize(50);
	            listItemHolders.put('Component2', Component2);	
	            	
	            	
	            recordTypeSelector = new SkyEditor2.RecordTypeSelector(Lead.SObjectType, true);
	            	
	            	
            p_showHeader = true;
            p_sidebar = false;
            presetSystemParams();
            Component2.extender = this.extender;
	        } catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
	            SkyEditor2.Messages.addErrorMessage(e.getMessage());
	        } catch (SkyEditor2.Errors.FieldNotFoundException e) {	
	            SkyEditor2.Messages.addErrorMessage(e.getMessage());
            } catch (SkyEditor2.ExtenderException e) {                 e.setMessagesToPage();
	        } catch (Exception e) {	
	            System.Debug(LoggingLevel.Error, e);	
	            SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
	        }	
	    }	
	    	
        public List<SelectOption> getOperatorOptions_Lead_Company() { 
            return getOperatorOptions('Lead', 'Company');	
	    }	
        public List<SelectOption> getOperatorOptions_Lead_OwnerId() { 
            return getOperatorOptions('Lead', 'OwnerId');	
	    }	
        public List<SelectOption> getOperatorOptions_Lead_tag_c() { 
            return getOperatorOptions('Lead', 'tag__c');	
	    }	
        public List<SelectOption> getOperatorOptions_Lead_SmartViscal_NameCardListName_c() { 
            return getOperatorOptions('Lead', 'SmartViscal__NameCardListName__c');	
	    }	
        public List<SelectOption> getOperatorOptions_Lead_Email() { 
            return getOperatorOptions('Lead', 'Email');	
	    }	
        public List<SelectOption> getOperatorOptions_Lead_Marketodouki_c() { 
            return getOperatorOptions('Lead', 'Marketodouki__c');	
	    }	
	    	
	    private static testMethod void testPageMethods() {	
	        LeadSearchMarketing page = new LeadSearchMarketing(new ApexPages.StandardController(new Lead()));	
	        page.getOperatorOptions_Lead_Company();	
	        page.getOperatorOptions_Lead_OwnerId();	
	        page.getOperatorOptions_Lead_tag_c();	
	        page.getOperatorOptions_Lead_SmartViscal_NameCardListName_c();	
	        page.getOperatorOptions_Lead_Email();	
	        page.getOperatorOptions_Lead_Marketodouki_c();	

            Integer defaultSize;
            defaultSize = page.Component2.items.size();
            page.Component2.add();
            System.assertEquals(defaultSize + 1, page.Component2.items.size());
            page.Component2.items[defaultSize].selected = true;
            page.Component2.doDeleteSelectedItems();
            System.assertEquals(defaultSize, page.Component2.items.size());
            System.assert(true);
	    }	
	    	
	    	
    global with sharing class Component2Item extends SkyEditor2.ListItem {
        public Lead record{get; private set;}
        Component2Item(Component2 holder, Lead record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(holder);
            if (record.Id == null  && record.RecordTypeId == null){
                if (recordTypeSelector != null) {
                    recordTypeSelector.applyDefault(record);
                }
                
            }
            this.record = record;
        }
        global override SObject getRecord() {return record;}
        public void doDeleteItem(){deleteItem();}
    }
    global with sharing  class Component2 extends SkyEditor2.PagingList {
        public List<Component2Item> items{get; private set;}
        Component2(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(records, items, deleteRecords, recordTypeSelector);
            this.items = (List<Component2Item>)items;
        }
        global override SkyEditor2.ListItem create(SObject data) {
            return new Component2Item(this, (Lead)data, recordTypeSelector);
        }
        public void doDeleteSelectedItems(){deleteSelectedItems();}
        public void doFirst(){first();}
        public void doPrevious(){previous();}
        public void doNext(){next();}
        public void doLast(){last();}
        public void doSort(){sort();}

        public List<Component2Item> getViewItems() {            return (List<Component2Item>) getPageItems();        }
    }
    private static testMethod void testComponent2() {
        Component2 Component2 = new Component2(new List<Lead>(), new List<Component2Item>(), new List<Lead>(), null);
        Component2.setPageItems(new List<Component2Item>());
        Component2.create(new Lead());
        Component2.doDeleteSelectedItems();
        Component2.setPagesize(10);        Component2.doFirst();
        Component2.doPrevious();
        Component2.doNext();
        Component2.doLast();
        Component2.doSort();
        System.assert(true);
    }
    

    public Lead Component2_table_Conversion { get { return new Lead();}}
    
    public String Component2_table_selectval { get; set; }
    
    
	    	
	}