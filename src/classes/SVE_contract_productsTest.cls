@isTest
private with sharing class SVE_contract_productsTest{
		private static testMethod void testPageMethods() {	
			SVE_contract_products page = new SVE_contract_products(new ApexPages.StandardController(new contract_products__c()));	
			page.getOperatorOptions_contract_products_c_conpany_del_del_c();	
			page.getOperatorOptions_contract_products_c_torihikishikkou_no_c();	
			page.getOperatorOptions_contract_products_c_kikan_c();	
			page.getOperatorOptions_contract_products_c_kikan_end_c();	
			page.getOperatorOptions_contract_products_c_pay_rule_c();	
			page.getOperatorOptions_contract_products_c_bugyo_check_c();	
			page.getOperatorOptions_contract_products_c_cost_department_c();	
			System.assert(true);
		}	
			
	private static testMethod void testComponent3() {
		SVE_contract_products.Component3 Component3 = new SVE_contract_products.Component3(new List<contract_products__c>(), new List<SVE_contract_products.Component3Item>(), new List<contract_products__c>(), null);
		Component3.create(new contract_products__c());
		Component3.doDeleteSelectedItems();
		System.assert(true);
	}
	
}