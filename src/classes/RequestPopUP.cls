global with sharing class RequestPopUP extends SkyEditor2.SkyEditorPageBaseWithSharing{
	
	public OrderItem__c record{get;set;}
	public Component3 Component3 {get; private set;}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	public SkyEditor2__SkyEditorDummy__c Component48{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component5{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component8{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component11{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component14{get;set;}
	{
	setApiVersion(31.0);
	}
	public RequestPopUP(ApexPages.StandardController controller){
		super(controller);

		SObjectField f;

		f = OrderItem__c.fields.Item2_RelationView__c;
		f = OrderItem__c.fields.Name;
		f = OrderItem__c.fields.Account__c;
		f = OrderItem__c.fields.Item2_Product_naiyou__c;
		f = OrderItem__c.fields.Account_Item2_SalesPerson1__c;
		f = OrderItem__c.fields.SalesPerson1__c;
		f = OrderItem__c.fields.Item2_Relation__c;
		f = OrderItem__c.fields.Item2_Product__c;
		f = OrderItem__c.fields.Item2_Detail_Productlist__c;
		f = OrderItem__c.fields.OrderItem_keywordType__c;
		f = OrderItem__c.fields.Item2_Keyword_strategy_keyword__c;
		f = OrderItem__c.fields.Item2_Keyword_phase__c;
		f = OrderItem__c.fields.TotalPrice__c;
		f = OrderItem__c.fields.ServiceDate__c;
		f = OrderItem__c.fields.EndDate__c;

		try {
			mainRecord = null;
			mainSObjectType = OrderItem__c.SObjectType;
			mode = SkyEditor2.LayoutMode.TempProductLookup_01;
			
			Component48 = new SkyEditor2__SkyEditorDummy__c();
			Component5 = new SkyEditor2__SkyEditorDummy__c();
			Component8 = new SkyEditor2__SkyEditorDummy__c();
			Component11 = new SkyEditor2__SkyEditorDummy__c();
			Component14 = new SkyEditor2__SkyEditorDummy__c();
			
			queryMap.put(
				'Component3',
				new SkyEditor2.Query('OrderItem__c')
					.addFieldAsOutput('SalesPerson1__c')
					.addFieldAsOutput('Item2_Relation__c')
					.addFieldAsOutput('Name')
					.addFieldAsOutput('Account__c')
					.addFieldAsOutput('Item2_Product__c')
					.addFieldAsOutput('Item2_Detail_Productlist__c')
					.addFieldAsOutput('OrderItem_keywordType__c')
					.addFieldAsOutput('Item2_Keyword_strategy_keyword__c')
					.addFieldAsOutput('Item2_Keyword_phase__c')
					.addFieldAsOutput('TotalPrice__c')
					.addFieldAsOutput('ServiceDate__c')
					.addFieldAsOutput('EndDate__c')
					.addField('Name')
					.limitRecords(500)
					.addListener(new SkyEditor2.QueryWhereRegister(Component48, 'SkyEditor2__Text__c', 'Item2_RelationView__c', new SkyEditor2.TextHolder('co'), false, true, false))
					.addListener(new SkyEditor2.QueryWhereRegister(Component5, 'SkyEditor2__Text__c', 'Name', new SkyEditor2.TextHolder('co'), false, true, false))
					.addListener(new SkyEditor2.QueryWhereRegister(Component8, 'SkyEditor2__Text__c', 'Account__c', new SkyEditor2.TextHolder('co'), false, true, false))
					.addListener(new SkyEditor2.QueryWhereRegister(Component11, 'SkyEditor2__Text__c', 'Item2_Product_naiyou__c', new SkyEditor2.TextHolder('co'), false, true, false))
					.addListener(new SkyEditor2.QueryWhereRegister(Component14, 'SkyEditor2__Text__c', 'Account_Item2_SalesPerson1__c', new SkyEditor2.TextHolder('co'), false, true, false))
					 .addWhere(' ( Item2_Keyword_phase__c = \'契約中\' OR Item2_Keyword_phase__c = \'請求停止\')')
.addSort('Item2_Relation__c',False,True)
			);
			
			Component3 = new Component3(new List<OrderItem__c>(), new List<Component3Item>(), new List<OrderItem__c>(), null);
			listItemHolders.put('Component3', Component3);
			Component3.ignoredOnSave = true;
			
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(OrderItem__c.SObjectType);
			
			p_showHeader = false;
			p_sidebar = false;
			presetSystemParams();
			initSearch();
			
		} catch (SkyEditor2.Errors.SObjectNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.Errors.FieldNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.ExtenderException e){
			e.setMessagesToPage();
		} catch (SkyEditor2.Errors.PricebookNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
			hidePageBody = true;
		}
	}

	public List<SelectOption> getOperatorOptions_OrderItem_c_Item2_RelationView_c() {
		return getOperatorOptions('OrderItem__c', 'Item2_RelationView__c');
	}
	public List<SelectOption> getOperatorOptions_OrderItem_c_Name() {
		return getOperatorOptions('OrderItem__c', 'Name');
	}
	public List<SelectOption> getOperatorOptions_OrderItem_c_Account_c() {
		return getOperatorOptions('OrderItem__c', 'Account__c');
	}
	public List<SelectOption> getOperatorOptions_OrderItem_c_Item2_Product_naiyou_c() {
		return getOperatorOptions('OrderItem__c', 'Item2_Product_naiyou__c');
	}
	public List<SelectOption> getOperatorOptions_OrderItem_c_Account_Item2_SalesPerson1_c() {
		return getOperatorOptions('OrderItem__c', 'Account_Item2_SalesPerson1__c');
	}
	
	
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public OrderItem__c record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, OrderItem__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.ListItemHolder {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (OrderItem__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	
}