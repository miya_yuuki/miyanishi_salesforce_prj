public with sharing class CreateBillRecordFromOrderItem {

	public static boolean firstRun = true;

	public static void prcCreateBill(List<OrderItem__c> objOrderItem, Map<Id, OrderItem__c> oldMap, Boolean upd) {
		// 注文商品ID
		List<String> strOrderItem2Ids = new List<String>();
		// 請求オブジェクト：紐付け用
		Map<String, Bill__c> mapBill = new Map<String, Bill__c>();
		// 請求オブジェクト
		List<Bill__c> insBill = new List<Bill__c>();
		// 請求商品オブジェクト
		List<BillProduct__c> insBillProduct = new List<BillProduct__c>();

		// 注文商品オブジェクトのループ
		for (OrderItem__c obj : objOrderItem) {
            // 支払いサイトが前払いでなく、かつ、請求タイミングも受注でない場合は処理しない
			if (obj.Order_PaymentSiteMaster__c != '前払い' && obj.BillingTiming__c != '受注') continue;	
			// 更新対象の注文商品でなければ処理しない
			if (prcCheckPaymentQuantity(upd, obj, oldMap) == null) continue;
			// 特定の注文商品フェーズは処理しない
			if (obj.Item2_Keyword_phase__c == '請求停止') continue;
			if (obj.Item2_Keyword_phase__c == '契約変更') continue;
			if (obj.Item2_Keyword_phase__c == '途中解約') continue;
			if (obj.Item2_Keyword_phase__c == '取消') continue;
			if (obj.Item2_Keyword_phase__c == '売上取消') continue;
			// 既にこの注文商品IDを持つ請求商品が存在する場合には処理しない
			List<BillProduct__c> chkBillProduct = [SELECT Id, OrderItem__c FROM BillProduct__c WHERE OrderItem__c = :obj.Id];
			if (chkBillProduct.size() > 0) continue;

			// 配列に追加
			strOrderItem2Ids.add(obj.Id);
		}
		System.debug('############strOrderItem2Ids: ' + strOrderItem2Ids.size());

		// 請求レコードを作成
		insBill = prcInsertBill(strOrderItem2Ids);

		// 請求をMapに追加
		for (Bill__c obj : insBill) {
			mapBill.put(obj.Key__c, obj);
			System.debug('############insBill Key: ' + obj.Key__c);
		}

		// 注文商品オブジェクト
		List<OrderItem__c> objOrderItem2 = new List<OrderItem__c>();
		// 注文商品を取得
		objOrderItem2 = [
			SELECT
				OP.Id,
                OI.Name,
				OI.Item2_PaymentDueDate__c,
				OI.SalesPerson1__c,
				OD.BillTo_Contact__c,
				OD.BillTo_Contact__r.AccountId,
				OD.Opportunity_Support__c,
				OD.Order_PaymentMethod__c,
				OD.Order_PaymentSiteMaster__c,
				OD.Order_BillingMethod__c,
				OD.Order_Type__c,
				CO.Contract_EndClient1__c,
				CO.OwnerId,
				CO.Contract_FastOrdersSales__c,
				CO.SalesUnit1View__c,
				CO.SalesUnit2__c,
				CO.AccountId,
				OP.Opportunity__r.Opportunity_Title__r.AccountId,
				OI.ServiceDate__c,
				OI.EndDate__c,
				OI.Item2_ServiceDate__c,
				OI.Item2_EndData__c,
				OI.Item2_Commission_rate__c,
				OI.Item2_Entry_date__c,
				OI.Item2_Product__c,
				OI.Item2_Unit_selling_price__c,
				OI.Quantity__c,
				OI.Item2_Keyword_letter_count__c,
				OI.ProductSeparateUMU__c,
				OI.Billchek__c,
				OI.Item2_DeliveryExistence__c,
				OI.BillingTiming__c,
				OI.Order_PaymentSiteMaster__c,
				OI.OrderItem_keywordType__c,
				OI.Item2_Keyword_strategy_keyword__c,
				OI.OrderItem_URL__c,
				OI.Item2_Delivery_date__c, // 社外納品日→納品完了日に変更
				OI.TextCalculation__c,
				OI.Opportunity_Competition__c,
				OI.AmountCheck__c,
				OI.TextPlanning__c, // 要るのか？？（一応あったからつけておくけど未使用らしい）
				OI.TextType__c,
				OI.TextWarranty__c, // 送客保証って要るのか？？（一応あったからつけておく）
				OI.Bill_Main__c
			FROM OrderItem__c OI, OrderItem__c.Item2_Relation__r OD, OrderItem__c.Item2_Relation__r.Order_Relation__r CO, OrderItem__c.OppProduct__r OP
			WHERE
				OI.Id IN :strOrderItem2Ids
				AND OI.Item2_Keyword_phase__c NOT IN ('請求停止', '途中解約', '取消')

		];
		System.debug('########objOrderItem2: ' + objOrderItem2.size());

		// 納品管理オブジェクトのループ
		for (OrderItem__c obj : objOrderItem2) {
			System.debug('########Item2_PaymentDueDate__c: ' + obj.OppProduct__r.Id + ':' + obj.Item2_PaymentDueDate__c);
			// 請求年月日
			Date dt = obj.Item2_PaymentDueDate__c;
			String strBillDate = dt.year() + '-' + prcItoS(dt.month()) + '-' + prcItoS(dt.day());

			// 請求Keyを生成
			String strKey =
				'D_' +
				strBillDate + '_' + // ok
				obj.SalesPerson1__c + '_' + // ok
				obj.Item2_Relation__r.BillTo_Contact__c + '_' +
				obj.Item2_Relation__r.Order_PaymentMethod__c + '_' +
				obj.Item2_Relation__r.Order_PaymentSiteMaster__c + '_' +
				obj.Item2_Relation__r.Order_BillingMethod__c + '_' +
				obj.Item2_Relation__r.Order_Type__c + '_' +
				obj.Item2_Relation__r.Order_Relation__r.Contract_EndClient1__c;

			if (!mapBill.containsKey(strKey)) continue;

			// 請求商品Keyを生成
			String strItemKey = obj.Name + '_' + strBillDate;

			// Key
			System.debug('#########ItemKey:' + strItemKey);

			// 請求商品を作成
			BillProduct__c newBillProduct = new BillProduct__c(
				Key__c = strItemKey,														/* Key */
				Bill2__c = mapBill.get(strKey).Id,											/* 請求ID */
				OrderItem__c = obj.Id,									                    /* 注文商品ID */
				Contract_FastOrdersSales__c = obj.Item2_Relation__r.Order_Relation__r.Contract_FastOrdersSales__c,		/* 受注時営業担当 */
				BillAccountName__c = obj.Item2_Relation__r.Order_Relation__r.AccountId,			/* 契約取引先 */
				BillTo_Account__c = obj.Item2_Relation__r.BillTo_Contact__r.AccountId,			/* 請求取引先 */
				Bill2_SalesPerson__c = obj.Item2_Relation__r.Order_Relation__r.OwnerId,			/* 担当営業 */
				SalesUnit1__c = obj.Item2_Relation__r.Order_Relation__r.SalesUnit1View__c,			/* 担当営業ユニット */
				SalesUnit2__c = obj.Item2_Relation__r.Order_Relation__r.SalesUnit2__c,			/* 受注時営業担当ユニット */
				BillSupportPersonName__c = obj.Item2_Relation__r.Opportunity_Support__c,			/* サポート担当 */
				Bill2_PaymentDueDate__c = obj.Item2_PaymentDueDate__c,						/* 支払期日 */
				ServiceDate__c = obj.ServiceDate__c,											/* 契約開始日 */
				EndData__c = obj.EndDate__c,												/* 契約終了日 */
				Bill2_ServiceDate__c = obj.Item2_ServiceDate__c,							/* 請求対象開始日 */
				Bill2_EndDate__c = obj.Item2_EndData__c,									/* 請求対象終了日 */
				Bill2_Commission_rate__c = obj.Item2_Commission_rate__c,					/* 代理店手数料 */
				Bill2_Entry_date__c = obj.Item2_Entry_date__c,								/* 申込日 */
				Bill2_Product__c = obj.Item2_Product__c,				                    /* 商品 */
				Bill2_Price__c = obj.Item2_Unit_selling_price__c,								/* 単価 */
				Bill2_Count__c = prcCheckPaymentQuantity(upd, obj, oldMap),						/* 数量 */
				Bill2_TextCount__c = obj.Item2_Keyword_letter_count__c,							/* 文字数 */
				Bill2_keywordType__c = obj.OrderItem_keywordType__c,								/* キーワード種別 */
				Bill2_Keyword__c = obj.Item2_Keyword_strategy_keyword__c,							/* 対策キーワード */
				Bill2_URL__c = obj.OrderItem_URL__c,												/* URL */
				TextCalculation__c = obj.TextCalculation__c,										/* 文字数金額集計 */
				Opportunity_Competition__c = obj.Opportunity_Competition__c,						/* 区分 */
				Bill2_DeliveryCompletionDate__c = obj.Item2_Delivery_date__c,						/* 納品完了日 */
				AmountCheck__c = obj.AmountCheck__c,					                    /* 合計切り捨てなし */
				TextPlanning__c = obj.TextPlanning__c,										/* テキスト企画 */
				TextType__c = obj.TextType__c,												/* 記事タイプ */
				TextWarranty__c = obj.TextWarranty__c,										/* 送客保証 */
				Bill_TargetDay__c = obj.Bill_Main__c										/* 請求対象日 */ //昔のロジックだと分納の話があったけど、今はないので、メインだけ見ればOK
			);

			// 配列に追加
			insBillProduct.add(newBillProduct);
		}

		System.debug('########insBillProduct: ' + insBillProduct.size());
		// 請求商品を作成
		if (insBillProduct.size() > 0) {
			upsert insBillProduct;
			System.debug('insBillProduct upsert');
		}
		// 請求を更新
		if (insBill.size() > 0) update insBill;
		// 請求商品を更新
		if (insBillProduct.size() > 0) update insBillProduct;
	}

	// 請求レコード作成済みフラグから請求数量を抽出する
	private static Decimal prcCheckPaymentQuantity(Boolean upd, OrderItem__c objNew, Map<Id, OrderItem__c> oldMap) {
		// 請求レコード作成済みフラグにチェックが入ったタイミングのみ動作する
		if (upd) {
			// 古い値を取得
			OrderItem__c objOld = oldMap.get(objNew.Id);
            
			if (objOld.Billchek__c == false && objNew.Billchek__c == true) {
                return objNew.Quantity__c;
			}
		}
		// 新規作成時
		else {
            if (objNew.Billchek__c == true) {
                return objNew.Quantity__c;
            }
		}

		// 条件に一致しない
		return null;
	}

    // 請求レコードを作成する
	private static List<Bill__c> prcInsertBill (List<String> strOrderItem2Ids) {
		// 注文商品のグルーピング
		List<AggregateResult> agrOrderItem2 = new List<AggregateResult>();
		// 請求オブジェクト：作成用
		List<Bill__c> insBill = new List<Bill__c>();

		// 注文商品をKey項目でグルーピングして取得
		agrOrderItem2 = [
			SELECT
				OI.Item2_PaymentDueDate__c dt,								/* 支払期日 */
				OI.SalesPerson1__c sales,									/* 担当営業 */
				OD.Opportunity_Support__c support,							/* サポート担当 */
				OD.Order_Relation__r.AccountId account,						/* 取引先 */
				OD.BillTo_Contact__c contact,								/* 取引先責任者ID */
				OD.Order_PaymentMethod__c payment,							/* 支払い方法 */
				OD.Order_PaymentSiteMaster__c site,							/* 支払いサイト */
				OD.Order_BillingMethod__c bill,								/* 請求方法 */
				OD.Order_Type__c otype,										/* 案件種別 */
				OD.Order_Relation__r.Contract_EndClient1__c client,								/* 施策企業ID */
				OD.Order_Relation__r.Contract_Patnername__c partner,							/* セールスパートナーID */
				OD.Order_Relation__r.Contract_Patnername__r.partner_contract__c contract		/* パートナー契約 */
			FROM
				OrderItem__c OI, OrderItem__c.Item2_Relation__r OD
			WHERE
				Id IN :strOrderItem2Ids
				AND Item2_PaymentDueDate__c != null
			GROUP BY
				OI.Item2_PaymentDueDate__c,
				OI.SalesPerson1__c,
				OD.Order_Relation__r.AccountId,
				OD.Opportunity_Support__c,
				OD.BillTo_Contact__c,
				OD.Order_PaymentMethod__c,
				OD.Order_PaymentSiteMaster__c,
				OD.Order_BillingMethod__c,
				OD.Order_Type__c,
				OD.Order_Relation__r.Contract_EndClient1__c,
				OD.Order_Relation__r.Contract_Patnername__c,
				OD.Order_Relation__r.Contract_Patnername__r.partner_contract__c
		];

		// 請求オブジェクトの作成
		for (AggregateResult obj : agrOrderItem2) {
			// 支払期日
			Date dt = (Date)obj.get('dt');
			String strBillDate = dt.year() + '-' + prcItoS(dt.month()) + '-' + prcItoS(dt.day());

			// 請求Keyを生成
			String strKey =
				'D_' +
				strBillDate + '_' +
				(Id)obj.get('sales') + '_' +
				(Id)obj.get('contact') + '_' +
				(String)obj.get('payment') + '_' +
				(Id)obj.get('site') + '_' +
				(String)obj.get('bill') + '_' +
				(String)obj.get('otype') + '_' +
				(Id)obj.get('client');

			// 請求のインスタンスを生成
			Bill__c newBill = new Bill__c(
				RecordTypeId = '0125F0000006J32', // デフォルトのレコードタイプ（「請求書」）
				Key__c = strKey,
				InvoiceDate__c = (Date)obj.get('dt'),
				BillSupportPersonName__c = (Id)obj.get('support'),
				AccountID__c = (Id)obj.get('account'),
				Bill_Contact__c = (Id)obj.get('contact'),
				Bill_PaymentMethod__c = (String)obj.get('payment'),
				Bill_PaymentSiteMaster__c = (Id)obj.get('site'),
				Bill_BillingMethod__c = (String)obj.get('bill'),
				Bill_Type__c = (String)obj.get('otype'),
				Bill_EndClient__c = (Id)obj.get('client'),
				SalesPartner__c = (Id)obj.get('partner'),
				PartnerContract__c = (Id)obj.get('contract')
			);

			// 配列に追加
			insBill.add(newBill);
		}

		// 請求レコードをUpsertする
		if (insBill.size() > 0) {
			upsert insBill Key__c;
			System.debug('insBill upsert');
		}
		return insBill;
	}

	private static String prcItoS(Integer intVal) {
		if (intVal < 10) return '0' + String.valueOf(intVal);
		return String.valueOf(intVal);
	}
}