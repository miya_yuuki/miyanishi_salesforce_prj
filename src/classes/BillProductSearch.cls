global with sharing class BillProductSearch extends SkyEditor2.SkyEditorPageBaseWithSharing{
	public BillProduct__c record{get;set;}
	public Component3 Component3 {get; private set;}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	public BillProduct__c Component72_val {get;set;}
	public SkyEditor2.TextHolder Component72_op{get;set;}
	public BillProduct__c Component65_val {get;set;}
	public SkyEditor2.TextHolder Component65_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component8_val {get;set;}
	public SkyEditor2.TextHolder Component8_op{get;set;}
	public BillProduct__c Component74_val {get;set;}
	public SkyEditor2.TextHolder Component74_op{get;set;}
	public BillProduct__c Component67_val {get;set;}
	public SkyEditor2.TextHolder Component67_op{get;set;}
	public BillProduct__c Component76_val {get;set;}
	public SkyEditor2.TextHolder Component76_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component16_val {get;set;}
	public SkyEditor2.TextHolder Component16_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component18_val {get;set;}
	public SkyEditor2.TextHolder Component18_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component20_val {get;set;}
	public SkyEditor2.TextHolder Component20_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component78_val {get;set;}
	public SkyEditor2.TextHolder Component78_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component22_val {get;set;}
	public SkyEditor2.TextHolder Component22_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component24_val {get;set;}
	public SkyEditor2.TextHolder Component24_op{get;set;}
	public String recordTypeRecordsJSON_BillProduct_c {get; private set;}
	public String defaultRecordTypeId_BillProduct_c {get; private set;}
	public String metadataJSON_BillProduct_c {get; private set;}
	{
	setApiVersion(42.0);
	}
	public BillProductSearch(ApexPages.StandardController controller){
		super(controller);

		SObjectField f;

		f = BillProduct__c.fields.TextManagement2__c;
		f = BillProduct__c.fields.Bill2__c;
		f = BillProduct__c.fields.Name;
		f = BillProduct__c.fields.BillTo_Account__c;
		f = BillProduct__c.fields.Bill2_Product__c;
		f = BillProduct__c.fields.Bill2_SalesPerson__c;
		f = BillProduct__c.fields.BillingTiming__c;
		f = BillProduct__c.fields.Bill2_DeliveryanExistence__c;
		f = BillProduct__c.fields.BillProductCancel__c;
		f = BillProduct__c.fields.SalesCheck__c;
		f = BillProduct__c.fields.Bill_TargetDay__c;
		f = BillProduct__c.fields.Bill2_PaymentDueDate__c;
		f = BillProduct__c.fields.AccountName__c;
		f = BillProduct__c.fields.Bill2_ProductContent__c;
		f = BillProduct__c.fields.Bill2_Count__c;
		f = BillProduct__c.fields.Bill2_Price__c;
		f = BillProduct__c.fields.BillAmountPrint__c;
		f = BillProduct__c.fields.BillAmountTax__c;
		f = BillProduct__c.fields.Bill2_ServiceDate__c;
		f = BillProduct__c.fields.Bill2_EndDate__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainRecord = null;
			mainSObjectType = BillProduct__c.SObjectType;
			mode = SkyEditor2.LayoutMode.TempSearch_01; 
			BillProduct__c lookupObjComponent52 = new BillProduct__c();
			Component72_val = lookupObjComponent52;
			Component72_op = new SkyEditor2.TextHolder();
			Component65_val = lookupObjComponent52;
			Component65_op = new SkyEditor2.TextHolder();
			Component8_val = new SkyEditor2__SkyEditorDummy__c();
			Component8_op = new SkyEditor2.TextHolder();
			Component74_val = lookupObjComponent52;
			Component74_op = new SkyEditor2.TextHolder();
			Component67_val = lookupObjComponent52;
			Component67_op = new SkyEditor2.TextHolder();
			Component76_val = lookupObjComponent52;
			Component76_op = new SkyEditor2.TextHolder();
			Component16_val = new SkyEditor2__SkyEditorDummy__c();
			Component16_op = new SkyEditor2.TextHolder();
			Component18_val = new SkyEditor2__SkyEditorDummy__c();
			Component18_op = new SkyEditor2.TextHolder();
			Component20_val = new SkyEditor2__SkyEditorDummy__c();
			Component20_op = new SkyEditor2.TextHolder();
			Component78_val = new SkyEditor2__SkyEditorDummy__c();
			Component78_op = new SkyEditor2.TextHolder();
			Component22_val = new SkyEditor2__SkyEditorDummy__c();
			Component22_op = new SkyEditor2.TextHolder();
			Component24_val = new SkyEditor2__SkyEditorDummy__c();
			Component24_op = new SkyEditor2.TextHolder();
			queryMap.put(
				'Component3',
				new SkyEditor2.Query('BillProduct__c')
					.addField('BillProductCancel__c')
					.addFieldAsOutput('Bill2__c')
					.addFieldAsOutput('Name')
					.addFieldAsOutput('AccountName__c')
					.addField('Bill2_SalesPerson__c')
					.addField('Bill2_ProductContent__c')
					.addFieldAsOutput('Bill2_Count__c')
					.addField('Bill2_Price__c')
					.addFieldAsOutput('BillAmountPrint__c')
					.addField('BillAmountTax__c')
					.addFieldAsOutput('Bill2_PaymentDueDate__c')
					.addField('Bill_TargetDay__c')
					.addField('Bill2_ServiceDate__c')
					.addField('Bill2_EndDate__c')
					.limitRecords(500)
					.addListener(new SkyEditor2.QueryWhereRegister(Component72_val, 'TextManagement2__c', 'TextManagement2__c', Component72_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component65_val, 'Bill2__c', 'Bill2__c', Component65_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component8_val, 'SkyEditor2__Text__c', 'Name', Component8_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component74_val, 'BillTo_Account__c', 'BillTo_Account__c', Component74_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component67_val, 'Bill2_Product__c', 'Bill2_Product__c', Component67_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component76_val, 'Bill2_SalesPerson__c', 'Bill2_SalesPerson__c', Component76_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component16_val, 'SkyEditor2__Text__c', 'BillingTiming__c', Component16_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component18_val, 'SkyEditor2__Text__c', 'Bill2_DeliveryanExistence__c', Component18_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component20_val, 'SkyEditor2__Checkbox__c', 'BillProductCancel__c', Component20_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component78_val, 'SkyEditor2__Checkbox__c', 'SalesCheck__c', Component78_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component22_val, 'SkyEditor2__Date__c', 'Bill_TargetDay__c', Component22_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component24_val, 'SkyEditor2__Date__c', 'Bill2_PaymentDueDate__c', Component24_op, true, 0, false ))
				);
			Component3 = new Component3(new List<BillProduct__c>(), new List<Component3Item>(), new List<BillProduct__c>(), null);
			listItemHolders.put('Component3', Component3);
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(BillProduct__c.SObjectType, true);
			p_showHeader = true;
			p_sidebar = true;
			execInitialSearch = false;
			presetSystemParams();
			Component3.extender = this.extender;
			initSearch();
		} catch (SkyEditor2.Errors.SObjectNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.Errors.FieldNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.ExtenderException e) {
			 e.setMessagesToPage();
		} catch (Exception e) {
			System.Debug(LoggingLevel.Error, e);
			SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);
		}
	}
	public List<SelectOption> getOperatorOptions_BillProduct_c_TextManagement2_c() { 
		return getOperatorOptions('BillProduct__c', 'TextManagement2__c');
	}
	public List<SelectOption> getOperatorOptions_BillProduct_c_Bill2_c() { 
		return getOperatorOptions('BillProduct__c', 'Bill2__c');
	}
	public List<SelectOption> getOperatorOptions_BillProduct_c_Name() { 
		return getOperatorOptions('BillProduct__c', 'Name');
	}
	public List<SelectOption> getOperatorOptions_BillProduct_c_BillTo_Account_c() { 
		return getOperatorOptions('BillProduct__c', 'BillTo_Account__c');
	}
	public List<SelectOption> getOperatorOptions_BillProduct_c_Bill2_Product_c() { 
		return getOperatorOptions('BillProduct__c', 'Bill2_Product__c');
	}
	public List<SelectOption> getOperatorOptions_BillProduct_c_Bill2_SalesPerson_c() { 
		return getOperatorOptions('BillProduct__c', 'Bill2_SalesPerson__c');
	}
	public List<SelectOption> getOperatorOptions_BillProduct_c_BillingTiming_c() { 
		return getOperatorOptions('BillProduct__c', 'BillingTiming__c');
	}
	public List<SelectOption> getOperatorOptions_BillProduct_c_Bill2_DeliveryanExistence_c() { 
		return getOperatorOptions('BillProduct__c', 'Bill2_DeliveryanExistence__c');
	}
	public List<SelectOption> getOperatorOptions_BillProduct_c_BillProductCancel_c() { 
		return getOperatorOptions('BillProduct__c', 'BillProductCancel__c');
	}
	public List<SelectOption> getOperatorOptions_BillProduct_c_SalesCheck_c() { 
		return getOperatorOptions('BillProduct__c', 'SalesCheck__c');
	}
	public List<SelectOption> getOperatorOptions_BillProduct_c_Bill_TargetDay_c() { 
		return getOperatorOptions('BillProduct__c', 'Bill_TargetDay__c');
	}
	public List<SelectOption> getOperatorOptions_BillProduct_c_Bill2_PaymentDueDate_c() { 
		return getOperatorOptions('BillProduct__c', 'Bill2_PaymentDueDate__c');
	}
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public BillProduct__c record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, BillProduct__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.ListItemHolder {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (BillProduct__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	public BillProduct__c Component3_table_Conversion { get { return new BillProduct__c();}}
	
	public String Component3_table_selectval { get; set; }
	
	
}