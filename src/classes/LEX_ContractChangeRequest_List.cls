global with sharing class LEX_ContractChangeRequest_List extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public CustomObject1__c record {get{return (CustomObject1__c)mainRecord;}}
	public Component294 Component294 {get; private set;}
	{
	setApiVersion(42.0);
	}
	public LEX_ContractChangeRequest_List(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = SEOChange__c.fields.OrderItem__c;
		f = SEOChange__c.fields.Type__c;
		f = SEOChange__c.fields.KeywordType__c;
		f = SEOChange__c.fields.Keyword__c;
		f = SEOChange__c.fields.URL__c;
		f = SEOChange__c.fields.level__c;
		f = SEOChange__c.fields.Amount__c;
		f = SEOChange__c.fields.sinsei__c;
		f = SEOChange__c.fields.KeywordType2__c;
		f = SEOChange__c.fields.Keyword2__c;
		f = SEOChange__c.fields.URL2__c;
		f = SEOChange__c.fields.level2__c;
		f = SEOChange__c.fields.Amount2__c;
		f = OrderItem__c.fields.OrderItem_keywordType__c;
		f = OrderItem__c.fields.Item2_Keyword_strategy_keyword__c;
		f = OrderItem__c.fields.OrderItem_URL__c;
		f = OrderItem__c.fields.Item2_Estimate_level__c;
		f = OrderItem__c.fields.TotalPrice__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = CustomObject1__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'LEX_ContractChangeRequest_List';
			mainQuery = new SkyEditor2.Query('CustomObject1__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			Component294 = new Component294(new List<SEOChange__c>(), new List<Component294Item>(), new List<SEOChange__c>(), null);
			listItemHolders.put('Component294', Component294);
			query = new SkyEditor2.Query('SEOChange__c');
			query.addField('OrderItem__c');
			query.addField('Type__c');
			query.addField('KeywordType2__c');
			query.addField('Keyword2__c');
			query.addField('URL2__c');
			query.addField('level2__c');
			query.addField('Amount2__c');
			query.addFieldAsOutput('KeywordType__c');
			query.addFieldAsOutput('Keyword__c');
			query.addFieldAsOutput('URL__c');
			query.addFieldAsOutput('level__c');
			query.addFieldAsOutput('Amount__c');
			query.addFieldAsOutput('sinsei__c');
			query.addFieldAsOutput('RecordTypeId');
			query.addWhere('sinsei__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('Component294', 'sinsei__c');
			Component294.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('Component294', query);
			registRelatedList('sinsei__r', 'Component294');
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			Component294.extender = this.extender;
			if (record.Id == null) {
				saveOldValues();
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class Component294Item extends SkyEditor2.ListItem {
		public SEOChange__c record{get; private set;}
		@TestVisible
		Component294Item(Component294 holder, SEOChange__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}

	public void loadReferenceValues_Component351() {
		if (record.OrderItem__c == null) {
if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.KeywordType__c)) {
				record.KeywordType__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.Keyword__c)) {
				record.Keyword__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.URL__c)) {
				record.URL__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.level__c)) {
				record.level__c = null;
			}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.Amount__c)) {
				record.Amount__c = null;
			}
				return;
		}
		OrderItem__c[] referenceTo = [SELECT OrderItem_keywordType__c,Item2_Keyword_strategy_keyword__c,OrderItem_URL__c,Item2_Estimate_level__c,TotalPrice__c FROM OrderItem__c WHERE Id=:record.OrderItem__c];
		if (referenceTo.size() == 0) {
			record.OrderItem__c.addError(SkyEditor2.Messages.referenceDataNotFound(record.OrderItem__c));
			return;
		}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.KeywordType__c)) {
			record.KeywordType__c = referenceTo[0].OrderItem_keywordType__c;
		}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.Keyword__c)) {
			record.Keyword__c = referenceTo[0].Item2_Keyword_strategy_keyword__c;
		}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.URL__c)) {
			record.URL__c = referenceTo[0].OrderItem_URL__c;
		}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.level__c)) {
			record.level__c = referenceTo[0].Item2_Estimate_level__c;
		}
		if (SkyEditor2.Util.isEditable(record, SEOChange__c.fields.Amount__c)) {
			record.Amount__c = referenceTo[0].TotalPrice__c;
		}
		
	}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component294 extends SkyEditor2.ListItemHolder {
		public List<Component294Item> items{get; private set;}
		@TestVisible
			Component294(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component294Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component294Item(this, (SEOChange__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
		global override void importByJSON() {
			List<Object> data = (List<Object>) System.JSON.deserializeUntyped(hiddenValue);
			super.importByJSON(data);
			for (Integer n = items.size() - data.size(); n < items.size(); n++) {
				Component294Item i = items[n];
				if (i.record.OrderItem__c != null) {
					i.loadReferenceValues_Component351();
				}
			}
		}
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}