global with sharing class karute_view extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public karute__c record {get{return (karute__c)mainRecord;}}
	public String recordTypeRecordsJSON_karute_c {get; private set;}
	public String defaultRecordTypeId_karute_c {get; private set;}
	public String metadataJSON_karute_c {get; private set;}
	public String picklistValuesJSON_karute_c_innerSEO_c {get; private set;}
	public String picklistValuesJSON_karute_c_innerSEOSpeed_c {get; private set;}
	public String picklistValuesJSON_karute_c_Contentsplus_c {get; private set;}
	public String picklistValuesJSON_karute_c_ContentsSpeed_c {get; private set;}
	public Component4436 Component4436 {get; private set;}
	public Component2716 Component2716 {get; private set;}
	public Component3891 Component3891 {get; private set;}
	public Component3317 Component3317 {get; private set;}
	{
	setApiVersion(42.0);
	}
	public karute_view(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = karute__c.fields.Name;
		f = karute__c.fields.AccountName__c;
		f = karute__c.fields.AccountBillingMethod__c;
		f = karute__c.fields.Address__c;
		f = karute__c.fields.CreditLevel1__c;
		f = karute__c.fields.AccountPaymentSiteMaster__c;
		f = karute__c.fields.GoogleMAP__c;
		f = karute__c.fields.CreditDay1__c;
		f = karute__c.fields.AccountPaymentMethod__c;
		f = karute__c.fields.Opportunity__c;
		f = karute__c.fields.AccountWebSite__c;
		f = karute__c.fields.Penalty_experience__c;
		f = karute__c.fields.URL__c;
		f = karute__c.fields.Business__c;
		f = karute__c.fields.TargetNeeds1__c;
		f = karute__c.fields.MarketingBudget_All__c;
		f = karute__c.fields.BusinessModel__c;
		f = karute__c.fields.TargetAttributes1__c;
		f = karute__c.fields.SEO_contents_budget_per_month__c;
		f = karute__c.fields.ClientFocusService__c;
		f = karute__c.fields.Strength__c;
		f = karute__c.fields.web_policy_and_cost__c;
		f = karute__c.fields.CVPoint__c;
		f = karute__c.fields.ConflictSite__c;
		f = karute__c.fields.AcceptableCPA__c;
		f = karute__c.fields.CVR__c;
		f = karute__c.fields.SiteOperations__c;
		f = karute__c.fields.FocusKeywords__c;
		f = karute__c.fields.ProductionCompanyName__c;
		f = karute__c.fields.other_site_operations__c;
		f = karute__c.fields.WebStrategy__c;
		f = karute__c.fields.ClientGoal__c;
		f = karute__c.fields.Proposal_requirement__c;
		f = karute__c.fields.priority_policies__c;
		f = karute__c.fields.WGGoal__c;
		f = karute__c.fields.proposal_datum__c;
		f = karute__c.fields.MeasurePurpose__c;
		f = karute__c.fields.ProposalProducts__c;
		f = karute__c.fields.seo_measure__c;
		f = karute__c.fields.ToleranceOfExternalLinkRisk__c;
		f = karute__c.fields.CompetitorIsChecked__c;
		f = karute__c.fields.SEOBudget__c;
		f = karute__c.fields.SEOMeasureHistory__c;
		f = karute__c.fields.CompetitionPoint__c;
		f = karute__c.fields.Factor_budget_proposal__c;
		f = karute__c.fields.RivalCompany__c;
		f = karute__c.fields.CompetitionCompany__c;
		f = karute__c.fields.budget_type__c;
		f = karute__c.fields.RivalCompany2__c;
		f = karute__c.fields.CompetitionCompany2__c;
		f = karute__c.fields.budget_type_budget__c;
		f = karute__c.fields.PenaltyHistory__c;
		f = karute__c.fields.CompetitionCompany3__c;
		f = karute__c.fields.Products__c;
		f = karute__c.fields.SiteTrends__c;
		f = karute__c.fields.CompetitionCompany4__c;
		f = karute__c.fields.CotractPeriod__c;
		f = karute__c.fields.CompetitionCompany5__c;
		f = karute__c.fields.contents_scheduled_order_date__c;
		f = karute__c.fields.OrderReason__c;
		f = karute__c.fields.ProposalOutline__c;
		f = karute__c.fields.Irregulars__c;
		f = karute__c.fields.Expectation__c;
		f = karute__c.fields.NotesToConsultantsAndAnalysts__c;
		f = karute__c.fields.Memo__c;
		f = karute__c.fields.contents_measure_status__c;
		f = karute__c.fields.keyword_selection_resources__c;
		f = karute__c.fields.ContentsCompetitorIsChecked__c;
		f = karute__c.fields.contents_budget_per_month__c;
		f = karute__c.fields.keyword_selection_approach__c;
		f = karute__c.fields.ContentsCompetitionPoint__c;
		f = karute__c.fields.competitor_measures__c;
		f = karute__c.fields.planning_resources__c;
		f = karute__c.fields.ContentsCompetitionCompany1__c;
		f = karute__c.fields.planning_approach__c;
		f = karute__c.fields.ContentsCompetitionCompany2__c;
		f = karute__c.fields.expectation_for_sagooo__c;
		f = karute__c.fields.writing_resources__c;
		f = karute__c.fields.ContentsCompetitionCompany3__c;
		f = karute__c.fields.ContentsAdjustment__c;
		f = karute__c.fields.writing_approach__c;
		f = karute__c.fields.ContentsCompetitionCompany4__c;
		f = karute__c.fields.ContentsCompetitionCompany5__c;
		f = karute__c.fields.ContentsCompetitionCompany6__c;
		f = karute__c.fields.ContentsOrderReason__c;
		f = karute__c.fields.ContentsProposalOutline__c;
		f = karute__c.fields.ContentsIrregulars__c;
		f = karute__c.fields.cloud_estimates__c;
		f = karute__c.fields.selection_reason_for_sagooo__c;
		f = karute__c.fields.ContentsMemo__c;
		f = karute__c.fields.aegisON__c;
		f = karute__c.fields.cloverON__c;
		f = karute__c.fields.dragonON__c;
		f = karute__c.fields.TextON__c;
		f = karute__c.fields.formatON__c;
		f = karute__c.fields.aegisFrequency__c;
		f = karute__c.fields.cloverFrequency__c;
		f = karute__c.fields.dragonFrequency__c;
		f = karute__c.fields.TextFrequency__c;
		f = karute__c.fields.formatFrequency__c;
		f = karute__c.fields.aegis2__c;
		f = karute__c.fields.clover2__c;
		f = karute__c.fields.dragon2__c;
		f = karute__c.fields.Text2__c;
		f = karute__c.fields.format2__c;
		f = karute__c.fields.innerSEO__c;
		f = karute__c.fields.innerSEOSpeed__c;
		f = karute__c.fields.Contentsplus__c;
		f = karute__c.fields.ContentsSpeed__c;
		f = karute__c.fields.SEOTrouble__c;
		f = karute__c.fields.ContentsTrouble__c;
		f = karute__c.fields.WGNiesWant__c;
		f = karute__c.fields.EtcPromotion__c;
		f = karute__c.fields.Adjustment__c;
		f = karute__c.fields.ConsultingMemo__c;
		f = karute__c.fields.Caution__c;
		f = karute__c.fields.CustomerName__c;
		f = karute__c.fields.CustomerName2__c;
		f = karute__c.fields.CustomerName30__c;
		f = karute__c.fields.kana__c;
		f = karute__c.fields.kana2__c;
		f = karute__c.fields.kana3__c;
		f = karute__c.fields.Division__c;
		f = karute__c.fields.Division2__c;
		f = karute__c.fields.Division3__c;
		f = karute__c.fields.Title10__c;
		f = karute__c.fields.Title20__c;
		f = karute__c.fields.Title30__c;
		f = karute__c.fields.Phone1__c;
		f = karute__c.fields.Phone2__c;
		f = karute__c.fields.Phone3__c;
		f = karute__c.fields.Mobile__c;
		f = karute__c.fields.Mobile2__c;
		f = karute__c.fields.Mobile3__c;
		f = karute__c.fields.Mail1__c;
		f = karute__c.fields.Mail2__c;
		f = karute__c.fields.Mail3__c;
		f = karute__c.fields.WebLiteracy__c;
		f = karute__c.fields.WebLiteracy2__c;
		f = karute__c.fields.WebLiteracy3__c;
		f = karute__c.fields.HumanType__c;
		f = karute__c.fields.HumanType2__c;
		f = karute__c.fields.HumanType3__c;
		f = karute__c.fields.Type__c;
		f = karute__c.fields.Type2__c;
		f = karute__c.fields.Type3__c;
		f = karute__c.fields.BillRole__c;
		f = karute__c.fields.BillRole2__c;
		f = karute__c.fields.BillRole3__c;
		f = karute__c.fields.CustomerEdit__c;
		f = karute__c.fields.CustomerEdit2__c;
		f = karute__c.fields.CustomerEdit3__c;
		f = karute__c.fields.newPenalyManagementLink__c;
		f = PenaltyManagement__c.fields.date__c;
		f = PenaltyManagement__c.fields.regularStatus__c;
		f = PenaltyManagement__c.fields.penaltyStatus__c;
		f = PenaltyManagement__c.fields.detail__c;
		f = karute__c.fields.NewRireki__c;
		f = Minutes__c.fields.Name;
		f = Minutes__c.fields.Title__c;
		f = Minutes__c.fields.VisitDate__c;
		f = Minutes__c.fields.OwnerId;
		f = Minutes__c.fields.Customer11__c;
		f = Minutes__c.fields.LastModifiedDate;
		f = karute__c.fields.AEGISNew__c;
		f = AEGIS__c.fields.AegisLinkId__c;
		f = AEGIS__c.fields.session_modified__c;
		f = AEGIS__c.fields.TargetMonth__c;
		f = AEGIS__c.fields.TargetMonthPeriod__c;
		f = AEGIS__c.fields.Name;
		f = AEGIS__c.fields.Company__c;
		f = AEGIS__c.fields.URL__c;
		f = AEGIS__c.fields.ViewName__c;
		f = AEGIS__c.fields.EF_URL__c;
		f = AEGIS__c.fields.Keyword__c;
		f = AEGIS__c.fields.BrandKeyword__c;
		f = karute__c.fields.cloverNew__c;
		f = CLOVER__c.fields.Name;
		f = CLOVER__c.fields.AccountName__c;
		f = CLOVER__c.fields.URL__c;
		f = CLOVER__c.fields.saito__c;
		f = CLOVER__c.fields.karute__c;
		f = karute__c.fields.CreatedDate;
		f = karute__c.fields.CreatedById;
		f = karute__c.fields.LastModifiedDate;
		f = karute__c.fields.LastModifiedById;

		List<RecordTypeInfo> recordTypes;
		FilterMetadataResult filterResult;
		List<RecordType> recordTypeRecords_karute_c = [SELECT Id, DeveloperName, NamespacePrefix FROM RecordType WHERE SobjectType = 'karute__c'];
		Map<Id, RecordType> recordTypeMap_karute_c = new Map<Id, RecordType>(recordTypeRecords_karute_c);
		List<RecordType> availableRecordTypes_karute_c = new List<RecordType>();
		recordTypes = SObjectType.karute__c.getRecordTypeInfos();

		for (RecordTypeInfo t: recordTypes) {
			if (t.isDefaultRecordTypeMapping()) {
				defaultRecordTypeId_karute_c = t.getRecordTypeId();
			}
			if (t.isAvailable()) {
				RecordType rtype = recordTypeMap_karute_c.get(t.getRecordTypeId());
				if (rtype != null) {
					availableRecordTypes_karute_c.add(rtype);
				}
			}
		}
		recordTypeRecordsJSON_karute_c = System.JSON.serialize(availableRecordTypes_karute_c);
		filterResult = filterMetadataJSON(
			System.JSON.deserializeUntyped('{"CustomObject":{"recordTypes":[{"fullName":"Karute","picklistValues":[{"picklist":"ContentsSpeed__c","values":[{"fullName":"早い%EF%BC%881週間以内%EF%BC%89","default":false},{"fullName":"普通%EF%BC%881%EF%BD%9E2週間%EF%BC%89","default":false},{"fullName":"遅い%EF%BC%883週間以上%EF%BC%89","default":false}]},{"picklist":"Contentsplus__c","values":[{"fullName":"アウトソース","default":false},{"fullName":"インハウス","default":false}]},{"picklist":"innerSEOSpeed__c","values":[{"fullName":"早い%EF%BC%881週間以内%EF%BC%89","default":false},{"fullName":"普通%EF%BC%88１%EF%BD%9E3週間%EF%BC%89","default":false},{"fullName":"遅い%EF%BC%883週間以上%EF%BC%89","default":false}]},{"picklist":"innerSEO__c","values":[{"fullName":"アウトソース","default":false},{"fullName":"インハウス","default":false}]}]}]}}'),
			recordTypeFullNames(availableRecordTypes_karute_c),
			karute__c.SObjectType
		);
		metadataJSON_karute_c = System.JSON.serialize(filterResult.data);
		picklistValuesJSON_karute_c_innerSEO_c = System.JSON.serialize(filterPricklistEntries(karute__c.SObjectType.innerSEO__c.getDescribe(), filterResult));
		picklistValuesJSON_karute_c_innerSEOSpeed_c = System.JSON.serialize(filterPricklistEntries(karute__c.SObjectType.innerSEOSpeed__c.getDescribe(), filterResult));
		picklistValuesJSON_karute_c_Contentsplus_c = System.JSON.serialize(filterPricklistEntries(karute__c.SObjectType.Contentsplus__c.getDescribe(), filterResult));
		picklistValuesJSON_karute_c_ContentsSpeed_c = System.JSON.serialize(filterPricklistEntries(karute__c.SObjectType.ContentsSpeed__c.getDescribe(), filterResult));
		try {
			mainSObjectType = karute__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'karute_view';
			mainQuery = new SkyEditor2.Query('karute__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('AccountName__c');
			mainQuery.addFieldAsOutput('AccountBillingMethod__c');
			mainQuery.addFieldAsOutput('Address__c');
			mainQuery.addFieldAsOutput('CreditLevel1__c');
			mainQuery.addFieldAsOutput('AccountPaymentSiteMaster__c');
			mainQuery.addFieldAsOutput('GoogleMAP__c');
			mainQuery.addFieldAsOutput('CreditDay1__c');
			mainQuery.addFieldAsOutput('AccountPaymentMethod__c');
			mainQuery.addFieldAsOutput('Opportunity__c');
			mainQuery.addFieldAsOutput('AccountWebSite__c');
			mainQuery.addFieldAsOutput('Penalty_experience__c');
			mainQuery.addFieldAsOutput('URL__c');
			mainQuery.addFieldAsOutput('Business__c');
			mainQuery.addFieldAsOutput('TargetNeeds1__c');
			mainQuery.addFieldAsOutput('MarketingBudget_All__c');
			mainQuery.addFieldAsOutput('BusinessModel__c');
			mainQuery.addFieldAsOutput('TargetAttributes1__c');
			mainQuery.addFieldAsOutput('SEO_contents_budget_per_month__c');
			mainQuery.addFieldAsOutput('ClientFocusService__c');
			mainQuery.addFieldAsOutput('Strength__c');
			mainQuery.addFieldAsOutput('web_policy_and_cost__c');
			mainQuery.addFieldAsOutput('CVPoint__c');
			mainQuery.addFieldAsOutput('ConflictSite__c');
			mainQuery.addFieldAsOutput('AcceptableCPA__c');
			mainQuery.addFieldAsOutput('CVR__c');
			mainQuery.addFieldAsOutput('SiteOperations__c');
			mainQuery.addFieldAsOutput('FocusKeywords__c');
			mainQuery.addFieldAsOutput('ProductionCompanyName__c');
			mainQuery.addFieldAsOutput('other_site_operations__c');
			mainQuery.addFieldAsOutput('WebStrategy__c');
			mainQuery.addFieldAsOutput('ClientGoal__c');
			mainQuery.addFieldAsOutput('Proposal_requirement__c');
			mainQuery.addFieldAsOutput('priority_policies__c');
			mainQuery.addFieldAsOutput('WGGoal__c');
			mainQuery.addFieldAsOutput('proposal_datum__c');
			mainQuery.addFieldAsOutput('MeasurePurpose__c');
			mainQuery.addFieldAsOutput('ProposalProducts__c');
			mainQuery.addFieldAsOutput('seo_measure__c');
			mainQuery.addFieldAsOutput('ToleranceOfExternalLinkRisk__c');
			mainQuery.addFieldAsOutput('CompetitorIsChecked__c');
			mainQuery.addFieldAsOutput('SEOBudget__c');
			mainQuery.addFieldAsOutput('SEOMeasureHistory__c');
			mainQuery.addFieldAsOutput('CompetitionPoint__c');
			mainQuery.addFieldAsOutput('Factor_budget_proposal__c');
			mainQuery.addFieldAsOutput('RivalCompany__c');
			mainQuery.addFieldAsOutput('CompetitionCompany__c');
			mainQuery.addFieldAsOutput('budget_type__c');
			mainQuery.addFieldAsOutput('RivalCompany2__c');
			mainQuery.addFieldAsOutput('CompetitionCompany2__c');
			mainQuery.addFieldAsOutput('budget_type_budget__c');
			mainQuery.addFieldAsOutput('PenaltyHistory__c');
			mainQuery.addFieldAsOutput('CompetitionCompany3__c');
			mainQuery.addFieldAsOutput('Products__c');
			mainQuery.addFieldAsOutput('SiteTrends__c');
			mainQuery.addFieldAsOutput('CompetitionCompany4__c');
			mainQuery.addFieldAsOutput('CotractPeriod__c');
			mainQuery.addFieldAsOutput('CompetitionCompany5__c');
			mainQuery.addFieldAsOutput('contents_scheduled_order_date__c');
			mainQuery.addFieldAsOutput('OrderReason__c');
			mainQuery.addFieldAsOutput('ProposalOutline__c');
			mainQuery.addFieldAsOutput('Irregulars__c');
			mainQuery.addFieldAsOutput('Expectation__c');
			mainQuery.addFieldAsOutput('NotesToConsultantsAndAnalysts__c');
			mainQuery.addFieldAsOutput('Memo__c');
			mainQuery.addFieldAsOutput('contents_measure_status__c');
			mainQuery.addFieldAsOutput('keyword_selection_resources__c');
			mainQuery.addFieldAsOutput('ContentsCompetitorIsChecked__c');
			mainQuery.addFieldAsOutput('contents_budget_per_month__c');
			mainQuery.addFieldAsOutput('keyword_selection_approach__c');
			mainQuery.addFieldAsOutput('ContentsCompetitionPoint__c');
			mainQuery.addFieldAsOutput('competitor_measures__c');
			mainQuery.addFieldAsOutput('planning_resources__c');
			mainQuery.addFieldAsOutput('ContentsCompetitionCompany1__c');
			mainQuery.addFieldAsOutput('planning_approach__c');
			mainQuery.addFieldAsOutput('ContentsCompetitionCompany2__c');
			mainQuery.addFieldAsOutput('expectation_for_sagooo__c');
			mainQuery.addFieldAsOutput('writing_resources__c');
			mainQuery.addFieldAsOutput('ContentsCompetitionCompany3__c');
			mainQuery.addFieldAsOutput('ContentsAdjustment__c');
			mainQuery.addFieldAsOutput('writing_approach__c');
			mainQuery.addFieldAsOutput('ContentsCompetitionCompany4__c');
			mainQuery.addFieldAsOutput('ContentsCompetitionCompany5__c');
			mainQuery.addFieldAsOutput('ContentsCompetitionCompany6__c');
			mainQuery.addFieldAsOutput('ContentsOrderReason__c');
			mainQuery.addFieldAsOutput('ContentsProposalOutline__c');
			mainQuery.addFieldAsOutput('ContentsIrregulars__c');
			mainQuery.addFieldAsOutput('cloud_estimates__c');
			mainQuery.addFieldAsOutput('selection_reason_for_sagooo__c');
			mainQuery.addFieldAsOutput('ContentsMemo__c');
			mainQuery.addFieldAsOutput('aegisON__c');
			mainQuery.addFieldAsOutput('cloverON__c');
			mainQuery.addFieldAsOutput('dragonON__c');
			mainQuery.addFieldAsOutput('TextON__c');
			mainQuery.addFieldAsOutput('formatON__c');
			mainQuery.addFieldAsOutput('aegisFrequency__c');
			mainQuery.addFieldAsOutput('cloverFrequency__c');
			mainQuery.addFieldAsOutput('dragonFrequency__c');
			mainQuery.addFieldAsOutput('TextFrequency__c');
			mainQuery.addFieldAsOutput('formatFrequency__c');
			mainQuery.addFieldAsOutput('aegis2__c');
			mainQuery.addFieldAsOutput('clover2__c');
			mainQuery.addFieldAsOutput('dragon2__c');
			mainQuery.addFieldAsOutput('Text2__c');
			mainQuery.addFieldAsOutput('format2__c');
			mainQuery.addFieldAsOutput('innerSEO__c');
			mainQuery.addFieldAsOutput('innerSEOSpeed__c');
			mainQuery.addFieldAsOutput('Contentsplus__c');
			mainQuery.addFieldAsOutput('ContentsSpeed__c');
			mainQuery.addFieldAsOutput('SEOTrouble__c');
			mainQuery.addFieldAsOutput('ContentsTrouble__c');
			mainQuery.addFieldAsOutput('WGNiesWant__c');
			mainQuery.addFieldAsOutput('EtcPromotion__c');
			mainQuery.addFieldAsOutput('Adjustment__c');
			mainQuery.addFieldAsOutput('ConsultingMemo__c');
			mainQuery.addFieldAsOutput('Caution__c');
			mainQuery.addFieldAsOutput('CustomerName__c');
			mainQuery.addFieldAsOutput('CustomerName2__c');
			mainQuery.addFieldAsOutput('CustomerName30__c');
			mainQuery.addFieldAsOutput('kana__c');
			mainQuery.addFieldAsOutput('kana2__c');
			mainQuery.addFieldAsOutput('kana3__c');
			mainQuery.addFieldAsOutput('Division__c');
			mainQuery.addFieldAsOutput('Division2__c');
			mainQuery.addFieldAsOutput('Division3__c');
			mainQuery.addFieldAsOutput('Title10__c');
			mainQuery.addFieldAsOutput('Title20__c');
			mainQuery.addFieldAsOutput('Title30__c');
			mainQuery.addFieldAsOutput('Phone1__c');
			mainQuery.addFieldAsOutput('Phone2__c');
			mainQuery.addFieldAsOutput('Phone3__c');
			mainQuery.addFieldAsOutput('Mobile__c');
			mainQuery.addFieldAsOutput('Mobile2__c');
			mainQuery.addFieldAsOutput('Mobile3__c');
			mainQuery.addFieldAsOutput('Mail1__c');
			mainQuery.addFieldAsOutput('Mail2__c');
			mainQuery.addFieldAsOutput('Mail3__c');
			mainQuery.addFieldAsOutput('WebLiteracy__c');
			mainQuery.addFieldAsOutput('WebLiteracy2__c');
			mainQuery.addFieldAsOutput('WebLiteracy3__c');
			mainQuery.addFieldAsOutput('HumanType__c');
			mainQuery.addFieldAsOutput('HumanType2__c');
			mainQuery.addFieldAsOutput('HumanType3__c');
			mainQuery.addFieldAsOutput('Type__c');
			mainQuery.addFieldAsOutput('Type2__c');
			mainQuery.addFieldAsOutput('Type3__c');
			mainQuery.addFieldAsOutput('BillRole__c');
			mainQuery.addFieldAsOutput('BillRole2__c');
			mainQuery.addFieldAsOutput('BillRole3__c');
			mainQuery.addFieldAsOutput('CustomerEdit__c');
			mainQuery.addFieldAsOutput('CustomerEdit2__c');
			mainQuery.addFieldAsOutput('CustomerEdit3__c');
			mainQuery.addFieldAsOutput('newPenalyManagementLink__c');
			mainQuery.addFieldAsOutput('NewRireki__c');
			mainQuery.addFieldAsOutput('AEGISNew__c');
			mainQuery.addFieldAsOutput('cloverNew__c');
			mainQuery.addFieldAsOutput('CreatedDate');
			mainQuery.addFieldAsOutput('CreatedById');
			mainQuery.addFieldAsOutput('LastModifiedDate');
			mainQuery.addFieldAsOutput('LastModifiedById');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			Component4436 = new Component4436(new List<PenaltyManagement__c>(), new List<Component4436Item>(), new List<PenaltyManagement__c>(), null);
			listItemHolders.put('Component4436', Component4436);
			query = new SkyEditor2.Query('PenaltyManagement__c');
			query.addFieldAsOutput('date__c');
			query.addFieldAsOutput('regularStatus__c');
			query.addFieldAsOutput('penaltyStatus__c');
			query.addFieldAsOutput('detail__c');
			query.addWhere('karute__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('Component4436', 'karute__c');
			Component4436.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('Component4436', query);
			Component2716 = new Component2716(new List<Minutes__c>(), new List<Component2716Item>(), new List<Minutes__c>(), null);
			listItemHolders.put('Component2716', Component2716);
			query = new SkyEditor2.Query('Minutes__c');
			query.addFieldAsOutput('Name');
			query.addFieldAsOutput('Title__c');
			query.addFieldAsOutput('VisitDate__c');
			query.addFieldAsOutput('OwnerId');
			query.addFieldAsOutput('Customer11__c');
			query.addFieldAsOutput('LastModifiedDate');
			query.addFieldAsOutput('RecordTypeId');
			query.addWhere('karute__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('Component2716', 'karute__c');
			Component2716.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('Component2716', query);
			Component2716.ignoredOnSave = true;
			Component3891 = new Component3891(new List<AEGIS__c>(), new List<Component3891Item>(), new List<AEGIS__c>(), null);
			listItemHolders.put('Component3891', Component3891);
			query = new SkyEditor2.Query('AEGIS__c');
			query.addFieldAsOutput('AegisLinkId__c');
			query.addFieldAsOutput('session_modified__c');
			query.addFieldAsOutput('TargetMonth__c');
			query.addFieldAsOutput('TargetMonthPeriod__c');
			query.addFieldAsOutput('Name');
			query.addFieldAsOutput('Company__c');
			query.addFieldAsOutput('URL__c');
			query.addFieldAsOutput('ViewName__c');
			query.addFieldAsOutput('EF_URL__c');
			query.addFieldAsOutput('Keyword__c');
			query.addFieldAsOutput('BrandKeyword__c');
			query.addWhere('karute__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('Component3891', 'karute__c');
			Component3891.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('Component3891', query);
			Component3317 = new Component3317(new List<CLOVER__c>(), new List<Component3317Item>(), new List<CLOVER__c>(), null);
			listItemHolders.put('Component3317', Component3317);
			query = new SkyEditor2.Query('CLOVER__c');
			query.addFieldAsOutput('Name');
			query.addFieldAsOutput('AccountName__c');
			query.addFieldAsOutput('URL__c');
			query.addFieldAsOutput('saito__c');
			query.addFieldAsOutput('karute__c');
			query.addWhere('karute__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('Component3317', 'karute__c');
			Component3317.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('Component3317', query);
			registRelatedList('PenaltyManagements__r', 'Component4436');
			registRelatedList('MinutesKarute__r', 'Component2716');
			registRelatedList('AEGISkarute__r', 'Component3891');
			registRelatedList('CLOVERkarute__r', 'Component3317');
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('Opportunity__c', 'OpportunityId');
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			Component4436.extender = this.extender;
			Component2716.extender = this.extender;
			Component3891.extender = this.extender;
			Component3317.extender = this.extender;
			if (record.Id == null) {
				saveOldValues();
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class Component4436Item extends SkyEditor2.ListItem {
		public PenaltyManagement__c record{get; private set;}
		@TestVisible
		Component4436Item(Component4436 holder, PenaltyManagement__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component4436 extends SkyEditor2.ListItemHolder {
		public List<Component4436Item> items{get; private set;}
		@TestVisible
			Component4436(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component4436Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component4436Item(this, (PenaltyManagement__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	global with sharing class Component2716Item extends SkyEditor2.ListItem {
		public Minutes__c record{get; private set;}
		@TestVisible
		Component2716Item(Component2716 holder, Minutes__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component2716 extends SkyEditor2.ListItemHolder {
		public List<Component2716Item> items{get; private set;}
		@TestVisible
			Component2716(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component2716Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component2716Item(this, (Minutes__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	global with sharing class Component3891Item extends SkyEditor2.ListItem {
		public AEGIS__c record{get; private set;}
		@TestVisible
		Component3891Item(Component3891 holder, AEGIS__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3891 extends SkyEditor2.ListItemHolder {
		public List<Component3891Item> items{get; private set;}
		@TestVisible
			Component3891(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3891Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3891Item(this, (AEGIS__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	global with sharing class Component3317Item extends SkyEditor2.ListItem {
		public CLOVER__c record{get; private set;}
		@TestVisible
		Component3317Item(Component3317 holder, CLOVER__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3317 extends SkyEditor2.ListItemHolder {
		public List<Component3317Item> items{get; private set;}
		@TestVisible
			Component3317(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3317Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3317Item(this, (CLOVER__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	public String getComponent4127OptionsJS() {
		return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
		karute__c.getSObjectType(),
		SObjectType.karute__c.fields.innerSEO__c.getSObjectField()
		));
		}
	public String getComponent4128OptionsJS() {
		return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
		karute__c.getSObjectType(),
		SObjectType.karute__c.fields.innerSEOSpeed__c.getSObjectField()
		));
		}
	public String getComponent4129OptionsJS() {
		return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
		karute__c.getSObjectType(),
		SObjectType.karute__c.fields.Contentsplus__c.getSObjectField()
		));
		}
	public String getComponent4130OptionsJS() {
		return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
		karute__c.getSObjectType(),
		SObjectType.karute__c.fields.ContentsSpeed__c.getSObjectField()
		));
		}
	@TestVisible		static Set<String> recordTypeFullNames(RecordType[] records) {
		Set<String> result = new Set<String>();
		for (RecordType r : records) {
			result.add(r.DeveloperName);
			if (r.NamespacePrefix != null) {
				result.add(r.NamespacePrefix + '__' + r.DeveloperName);
			}
		}
		return result;
	}
	
	@TestVisible		static FilterMetadataResult filterMetadataJSON(Object metadata, Set<String> recordTypeFullNames, SObjectType soType) {
		Map<String, Object> metadataMap = (Map<String, Object>) metadata;
		Map<String, Object> customObject = (Map<String, Object>) metadataMap.get('CustomObject');
		List<Object> recordTypes = (List<Object>) customObject.get('recordTypes');
		Map<String, Set<String>> availableEntries = new Map<String, Set<String>>();
		for (Integer i = recordTypes.size() - 1; i >= 0; i--) {
			Map<String, Object> recordType = (Map<String, Object>)recordTypes[i];
			String fullName = (String)recordType.get('fullName');
			if (! recordTypeFullNames.contains(fullName)) {
				recordTypes.remove(i);
			} else {
				addAll(availableEntries, getOutEntries(recordType, soType));
			}
		}	
		return new FilterMetadataResult(metadataMap, availableEntries, recordTypes.size() == 0);
	}
	public class FilterMetadataResult {
		public Map<String, Object> data {get; private set;}
		public Map<String, Set<String>> availableEntries {get; private set;}
		public Boolean master {get; private set;}
		public FilterMetadataResult(Map<String, Object> data, Map<String, Set<String>> availableEntries, Boolean master) {
			this.data = data;
			this.availableEntries = availableEntries;
			this.master = master;
		}
	}
	
	static void addAll(Map<String, Set<String>> toMap, Map<String, Set<String>> fromMap) {
		for (String key : fromMap.keySet()) {
			Set<String> fromSet = fromMap.get(key);
			Set<String> toSet = toMap.get(key);
			if (toSet == null) {
				toSet = new Set<String>();
				toMap.put(key, toSet);
			}
			toSet.addAll(fromSet);
		}
	}

	static Map<String, Set<String>> getOutEntries(Map<String, Object> recordType, SObjectType soType) {
		Map<String, Set<String>> result = new Map<String, Set<String>>();
		List<Object> entries = (List<Object>)recordType.get('picklistValues');
		Map<String, SObjectField> fields = soType.getDescribe().fields.getMap();
		for (Object e : entries) {
			Map<String, Object> entry = (Map<String, Object>) e;
			String picklist = (String) entry.get('picklist');
			SObjectField f = fields.get(picklist);
			List<Object> values = (List<Object>)(entry.get('values'));
			if (f != null && f.getDescribe().isAccessible()) {
				Set<String> entrySet = new Set<String>();
				for (Object v : values) {
					Map<String, Object> value = (Map<String, Object>) v;
					entrySet.add(EncodingUtil.urlDecode((String)value.get('fullName'), 'utf-8'));
				}
				result.put(picklist, entrySet);
			} else { 
				values.clear(); 
			}
		}
		return result;
	}
	
	static List<PicklistEntry> filterPricklistEntries(DescribeFieldResult f, FilterMetadataResult parseResult) {
		List<PicklistEntry> all = f.getPicklistValues();
		if (parseResult.master) {
			return all;
		}
		Set<String> availables = parseResult.availableEntries.get(f.getName());
		List<PicklistEntry> result = new List<PicklistEntry>();
		if(availables == null) return result;
		for (PicklistEntry e : all) {
			if (e.isActive() && availables.contains(e.getValue())) {
				result.add(e);
			}
		}
		return result;
	}
	
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}