@isTest
private with sharing class TaskSearchTest{
		private static testMethod void testPageMethods() {	
			TaskSearch page = new TaskSearch(new ApexPages.StandardController(new Task()));	
			page.getOperatorOptions_Task_WhatId();	
			page.getOperatorOptions_Task_WhoId();	
			page.getOperatorOptions_Task_OwnerId();	
			page.getOperatorOptions_User_Name();	
			page.getOperatorOptions_Task_Status_multi();	
			page.getOperatorOptions_Task_Priority_multi();	
			page.getOperatorOptions_Task_Subject();	

		Integer defaultSize;

		defaultSize = page.Component2.items.size();
		page.Component2.add();
		System.assertEquals(defaultSize + 1, page.Component2.items.size());
		page.Component2.items[defaultSize].selected = true;
		page.Component2.doDeleteSelectedItems();
			System.assert(true);
		}	
			
	private static testMethod void testComponent2() {
		TaskSearch.Component2 Component2 = new TaskSearch.Component2(new List<Task>(), new List<TaskSearch.Component2Item>(), new List<Task>(), null);
		Component2.create(new Task());
		Component2.doDeleteSelectedItems();
		System.assert(true);
	}
	
	@isTest
	private static void testLightDataTables(){

		System.assert(true);
	}
}