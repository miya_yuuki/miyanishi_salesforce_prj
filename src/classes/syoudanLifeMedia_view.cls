global with sharing class syoudanLifeMedia_view extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public Opportunity record {get{return (Opportunity)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	
	
	{
	setApiVersion(31.0);
	}
	public syoudanLifeMedia_view(ApexPages.StandardController controller) {
		super(controller);


		SObjectField f;

		f = Opportunity.fields.Name;
		f = Opportunity.fields.Opportunity_PMMember_del__c;
		f = Opportunity.fields.Opportunity_Type__c;
		f = Opportunity.fields.Type;
		f = Opportunity.fields.AmountTotal__c;
		f = Opportunity.fields.Opportunity_endClient1__c;
		f = Opportunity.fields.Opportunity_lead1__c;
		f = Opportunity.fields.CloseDate;
		f = Opportunity.fields.Opportunity_Competition__c;
		f = Opportunity.fields.Opportunity_lead2__c;
		f = Opportunity.fields.StageName;
		f = Opportunity.fields.kurashiMail__c;
		f = Opportunity.fields.AddressCheck__c;
		f = Opportunity.fields.Opportunity_dead__c;
		f = Opportunity.fields.Case__c;
		f = Opportunity.fields.syouninn__c;
		f = Opportunity.fields.Opportunity_DeadMemo__c;
		f = Opportunity.fields.CasePrintCheck__c;
		f = Opportunity.fields.OwnerId;
		f = Opportunity.fields.Opportunity_deadDay__c;
		f = Opportunity.fields.AccountId;
		f = Opportunity.fields.Opportunity_BillPostalCode2__c;
		f = Opportunity.fields.account_Relation__c;
		f = Opportunity.fields.account_BusinessModel__c;
		f = Opportunity.fields.Opportunity_BillPrefecture2__c;
		f = Opportunity.fields.Phone__c;
		f = Opportunity.fields.account_Industry__c;
		f = Opportunity.fields.Opportunity_BillCity2__c;
		f = Opportunity.fields.Website__c;
		f = Opportunity.fields.account_Industrycategory__c;
		f = Opportunity.fields.Opportunity_BillBuilding2__c;
		f = Opportunity.fields.ServiceWEBSite2__c;
		f = Opportunity.fields.AccountEdit__c;
		f = Opportunity.fields.Commission_rate__c;
		f = Opportunity.fields.TransferAccountNumber1__c;
		f = Opportunity.fields.account_CreditLevel__c;
		f = Opportunity.fields.CompanyCheck__c;
		f = Opportunity.fields.CreditSpace__c;
		f = Opportunity.fields.hansyaMemo__c;
		f = Opportunity.fields.CreditBalance__c;
		f = Opportunity.fields.hansyaDay__c;
		f = Opportunity.fields.account_CreditDay__c;
		f = Opportunity.fields.Opportunity_Title__c;
		f = Opportunity.fields.Opportunity_BillPhone__c;
		f = Opportunity.fields.Opportunity_Billaccount__c;
		f = Opportunity.fields.Opportunity_BillKana__c;
		f = Opportunity.fields.Opportunity_BillFax__c;
		f = Opportunity.fields.Opportunity_BillPostalCode__c;
		f = Opportunity.fields.Opportunity_BillDepartment__c;
		f = Opportunity.fields.Opportunity_BillEmail__c;
		f = Opportunity.fields.Opportunity_BillPrefecture__c;
		f = Opportunity.fields.Opportunity_BillTitle__c;
		f = Opportunity.fields.TransferAccountNumber__c;
		f = Opportunity.fields.Opportunity_BillCity__c;
		f = Opportunity.fields.Opportunity_BillRole__c;
		f = Opportunity.fields.Opportunity_BillAddress__c;
		f = Opportunity.fields.ContacEdit__c;
		f = Opportunity.fields.Opportunity_BillBuilding__c;
		f = Opportunity.fields.Opportunity_BillingMethod__c;
		f = Opportunity.fields.Opportunity_PaymentSiteMaster__c;
		f = Opportunity.fields.Opportunity_EstimateMemo__c;
		f = Opportunity.fields.Opportunity_PaymentMethod__c;
		f = Opportunity.fields.QuoteExpirationDate__c;
		f = Opportunity.fields.Opportunity_Memo__c;
		f = Opportunity.fields.CreatedById;
		f = Opportunity.fields.CreatedDate;
		f = Opportunity.fields.LastModifiedById;
		f = Opportunity.fields.LastModifiedDate;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = Opportunity.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('Opportunity');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('Opportunity_PMMember_del__c');
			mainQuery.addFieldAsOutput('Opportunity_Type__c');
			mainQuery.addFieldAsOutput('Type');
			mainQuery.addFieldAsOutput('AmountTotal__c');
			mainQuery.addFieldAsOutput('Opportunity_endClient1__c');
			mainQuery.addFieldAsOutput('Opportunity_lead1__c');
			mainQuery.addFieldAsOutput('CloseDate');
			mainQuery.addFieldAsOutput('Opportunity_Competition__c');
			mainQuery.addFieldAsOutput('Opportunity_lead2__c');
			mainQuery.addFieldAsOutput('StageName');
			mainQuery.addFieldAsOutput('kurashiMail__c');
			mainQuery.addFieldAsOutput('AddressCheck__c');
			mainQuery.addFieldAsOutput('Opportunity_dead__c');
			mainQuery.addFieldAsOutput('Case__c');
			mainQuery.addFieldAsOutput('syouninn__c');
			mainQuery.addFieldAsOutput('Opportunity_DeadMemo__c');
			mainQuery.addFieldAsOutput('CasePrintCheck__c');
			mainQuery.addFieldAsOutput('OwnerId');
			mainQuery.addFieldAsOutput('Opportunity_deadDay__c');
			mainQuery.addFieldAsOutput('AccountId');
			mainQuery.addFieldAsOutput('Opportunity_BillPostalCode2__c');
			mainQuery.addFieldAsOutput('account_Relation__c');
			mainQuery.addFieldAsOutput('account_BusinessModel__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPrefecture2__c');
			mainQuery.addFieldAsOutput('Phone__c');
			mainQuery.addFieldAsOutput('account_Industry__c');
			mainQuery.addFieldAsOutput('Opportunity_BillCity2__c');
			mainQuery.addFieldAsOutput('Website__c');
			mainQuery.addFieldAsOutput('account_Industrycategory__c');
			mainQuery.addFieldAsOutput('Opportunity_BillBuilding2__c');
			mainQuery.addFieldAsOutput('ServiceWEBSite2__c');
			mainQuery.addFieldAsOutput('AccountEdit__c');
			mainQuery.addFieldAsOutput('Commission_rate__c');
			mainQuery.addFieldAsOutput('TransferAccountNumber1__c');
			mainQuery.addFieldAsOutput('account_CreditLevel__c');
			mainQuery.addFieldAsOutput('CompanyCheck__c');
			mainQuery.addFieldAsOutput('CreditSpace__c');
			mainQuery.addFieldAsOutput('hansyaMemo__c');
			mainQuery.addFieldAsOutput('CreditBalance__c');
			mainQuery.addFieldAsOutput('hansyaDay__c');
			mainQuery.addFieldAsOutput('account_CreditDay__c');
			mainQuery.addFieldAsOutput('Opportunity_Title__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPhone__c');
			mainQuery.addFieldAsOutput('Opportunity_Billaccount__c');
			mainQuery.addFieldAsOutput('Opportunity_BillKana__c');
			mainQuery.addFieldAsOutput('Opportunity_BillFax__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPostalCode__c');
			mainQuery.addFieldAsOutput('Opportunity_BillDepartment__c');
			mainQuery.addFieldAsOutput('Opportunity_BillEmail__c');
			mainQuery.addFieldAsOutput('Opportunity_BillPrefecture__c');
			mainQuery.addFieldAsOutput('Opportunity_BillTitle__c');
			mainQuery.addFieldAsOutput('TransferAccountNumber__c');
			mainQuery.addFieldAsOutput('Opportunity_BillCity__c');
			mainQuery.addFieldAsOutput('Opportunity_BillRole__c');
			mainQuery.addFieldAsOutput('Opportunity_BillAddress__c');
			mainQuery.addFieldAsOutput('ContacEdit__c');
			mainQuery.addFieldAsOutput('Opportunity_BillBuilding__c');
			mainQuery.addFieldAsOutput('Opportunity_BillingMethod__c');
			mainQuery.addFieldAsOutput('Opportunity_PaymentSiteMaster__c');
			mainQuery.addFieldAsOutput('Opportunity_EstimateMemo__c');
			mainQuery.addFieldAsOutput('Opportunity_PaymentMethod__c');
			mainQuery.addFieldAsOutput('QuoteExpirationDate__c');
			mainQuery.addFieldAsOutput('Opportunity_Memo__c');
			mainQuery.addFieldAsOutput('CreatedById');
			mainQuery.addFieldAsOutput('CreatedDate');
			mainQuery.addFieldAsOutput('LastModifiedById');
			mainQuery.addFieldAsOutput('LastModifiedDate');
			mainQuery.addFieldAsOutput('LeadSource');
			mainQuery.addFieldAsOutput('Id');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}