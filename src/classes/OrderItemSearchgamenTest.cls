@isTest
private with sharing class OrderItemSearchgamenTest{
		private static testMethod void testPageMethods() {	
			OrderItemSearchgamen page = new OrderItemSearchgamen(new ApexPages.StandardController(new OrderItem__c()));	
			page.getOperatorOptions_OrderItem_c_Item2_Relation_c();	
			page.getOperatorOptions_OrderItem_c_Account_c();	
			page.getOperatorOptions_OrderItem_c_Item2_Keyword_strategy_keyword_c();	
			page.getOperatorOptions_OrderItem_c_OrderItem_keywordType_c_multi();	
			page.getOperatorOptions_OrderItem_c_Item2_Keyword_phase_c_multi();	
			page.getOperatorOptions_OrderItem_c_OrderItem_URL_c();	

		Integer defaultSize;

		defaultSize = page.Component2.items.size();
		page.Component2.add();
		System.assertEquals(defaultSize + 1, page.Component2.items.size());
		page.Component2.items[defaultSize].selected = true;
		page.Component2.doDeleteSelectedItems();
			System.assert(true);
		}	
			
	private static testMethod void testComponent2() {
		OrderItemSearchgamen.Component2 Component2 = new OrderItemSearchgamen.Component2(new List<OrderItem__c>(), new List<OrderItemSearchgamen.Component2Item>(), new List<OrderItem__c>(), null);
		Component2.setPageItems(new List<OrderItemSearchgamen.Component2Item>());
		Component2.create(new OrderItem__c());
		Component2.doDeleteSelectedItems();
		Component2.setPagesize(10);		Component2.doFirst();
		Component2.doPrevious();
		Component2.doNext();
		Component2.doLast();
		Component2.doSort();
		System.assert(true);
	}
	
	@isTest
	private static void testLightDataTables(){

		System.assert(true);
	}
}