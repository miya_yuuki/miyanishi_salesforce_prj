global with sharing class SearchOpportunityProduct extends SkyEditor2.SkyEditorPageBaseWithSharing{
	public OpportunityProduct__c record{get;set;}
	public Component3 Component3 {get; private set;}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	public SkyEditor2__SkyEditorDummy__c Component19_from{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component19_to{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component19_isNull{get;set;}
	public SkyEditor2.TextHolder.OperatorHolder Component19_isNull_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component15_from{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component15_to{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component15_isNull{get;set;}
	public SkyEditor2.TextHolder.OperatorHolder Component15_isNull_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component6_val {get;set;}
	public SkyEditor2.TextHolder Component6_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component8_val {get;set;}
	public SkyEditor2.TextHolder Component8_op{get;set;}
	public Opportunity Component10_val {get;set;}
	public SkyEditor2.TextHolder Component10_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component12_val {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component12_val_dummy {get;set;}
	public SkyEditor2.TextHolder Component12_op{get;set;}
	public List<SelectOption> valueOptions_Opportunity_StageName_multi {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component14_val {get;set;}
	public SkyEditor2.TextHolder Component14_op{get;set;}
	public String recordTypeRecordsJSON_OpportunityProduct_c {get; private set;}
	public String defaultRecordTypeId_OpportunityProduct_c {get; private set;}
	public String metadataJSON_OpportunityProduct_c {get; private set;}
	{
	setApiVersion(42.0);
	}
	public SearchOpportunityProduct(ApexPages.StandardController controller){
		super(controller);

		SObjectField f;

		f = Opportunity.fields.Opportunity_SalesPerson_Unit__c;
		f = Opportunity.fields.Name;
		f = Opportunity.fields.Opportunity_endClient1__c;
		f = Opportunity.fields.StageName;
		f = Opportunity.fields.WEBTitle__c;
		f = OpportunityProduct__c.fields.Opportunity__c;
		f = Opportunity.fields.Opportunity_Competition__c;
		f = OpportunityProduct__c.fields.Product__c;
		f = OpportunityProduct__c.fields.Price__c;
		f = OpportunityProduct__c.fields.Quantity__c;
		f = OpportunityProduct__c.fields.ContractMonths__c;
		f = Opportunity.fields.CloseDate;
		f = Opportunity.fields.Opportunity_deadDay__c;
		f = Opportunity.fields.NA_memo__c;
		f = Opportunity.fields.karute__c;
		f = Opportunity.fields.AmountTotal__c;
 f = Opportunity.fields.CloseDate;
 f = Opportunity.fields.Opportunity_deadDay__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainRecord = null;
			mainSObjectType = OpportunityProduct__c.SObjectType;
			mode = SkyEditor2.LayoutMode.TempSearch_01; 
			Component19_from = new SkyEditor2__SkyEditorDummy__c();
			Component19_to = new SkyEditor2__SkyEditorDummy__c();
			Component19_isNull = new SkyEditor2__SkyEditorDummy__c();
			Component19_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq');
			Component15_from = new SkyEditor2__SkyEditorDummy__c();
			Component15_to = new SkyEditor2__SkyEditorDummy__c();
			Component15_isNull = new SkyEditor2__SkyEditorDummy__c();
			Component15_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq');
			Opportunity lookupObjComponent52Opportunityc = new Opportunity();
			Component6_val = new SkyEditor2__SkyEditorDummy__c();
			Component6_op = new SkyEditor2.TextHolder();
			Component8_val = new SkyEditor2__SkyEditorDummy__c();
			Component8_op = new SkyEditor2.TextHolder();
			Component10_val = lookupObjComponent52Opportunityc;
			Component10_op = new SkyEditor2.TextHolder();
			Component12_val = new SkyEditor2__SkyEditorDummy__c();
			Component12_val_dummy = new SkyEditor2__SkyEditorDummy__c();
			Component12_op = new SkyEditor2.TextHolder();
			valueOptions_Opportunity_StageName_multi = new List<SelectOption>{new SelectOption('', Label.none)};
			for (PicklistEntry e : Opportunity.StageName.getDescribe().getPicklistValues()) {
				if (e.isActive()) {valueOptions_Opportunity_StageName_multi.add(new SelectOption(e.getValue(), e.getLabel()));}
			}
			Component14_val = new SkyEditor2__SkyEditorDummy__c();
			Component14_op = new SkyEditor2.TextHolder();
			queryMap.put(
				'Component3',
				new SkyEditor2.Query('OpportunityProduct__c')
					.addFieldAsOutput('Opportunity__r.Opportunity_SalesPerson_Unit__c')
					.addFieldAsOutput('Opportunity__c')
					.addFieldAsOutput('Opportunity__r.AmountTotal__c')
					.addFieldAsOutput('Opportunity__r.Opportunity_endClient1__c')
					.addFieldAsOutput('Opportunity__r.WEBTitle__c')
					.addFieldAsOutput('Opportunity__r.Opportunity_Competition__c')
					.addField('Product__c')
					.addField('Price__c')
					.addField('Quantity__c')
					.addField('ContractMonths__c')
					.addFieldAsOutput('Opportunity__r.CloseDate')
					.addFieldAsOutput('Opportunity__r.StageName')
					.addFieldAsOutput('Opportunity__r.Opportunity_deadDay__c')
					.addFieldAsOutput('Opportunity__r.NA_memo__c')
					.addFieldAsOutput('Opportunity__r.karute__c')
					.addFieldAsOutput('RecordTypeId')
					.limitRecords(500)
					.addListener(new SkyEditor2.QueryWhereRegister(Component19_from, 'SkyEditor2__Date__c', 'Opportunity__r.CloseDate',Opportunity.fields.CloseDate, new SkyEditor2.TextHolder('ge'), false, false,true,0,false,'Opportunity__c',OpportunityProduct__c.fields.Opportunity__c ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component19_to, 'SkyEditor2__Date__c', 'Opportunity__r.CloseDate',Opportunity.fields.CloseDate, new SkyEditor2.TextHolder('le'), false, false,true,0,false,'Opportunity__c',OpportunityProduct__c.fields.Opportunity__c ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component19_isNull, 'SkyEditor2__Date__c', 'Opportunity__r.CloseDate',Opportunity.fields.CloseDate, Component19_isNull_op, true, false,true,0,false,'Opportunity__c',OpportunityProduct__c.fields.Opportunity__c )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component15_from, 'SkyEditor2__Date__c', 'Opportunity__r.Opportunity_deadDay__c',Opportunity.fields.Opportunity_deadDay__c, new SkyEditor2.TextHolder('ge'), false, false,true,0,false,'Opportunity__c',OpportunityProduct__c.fields.Opportunity__c ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component15_to, 'SkyEditor2__Date__c', 'Opportunity__r.Opportunity_deadDay__c',Opportunity.fields.Opportunity_deadDay__c, new SkyEditor2.TextHolder('le'), false, false,true,0,false,'Opportunity__c',OpportunityProduct__c.fields.Opportunity__c ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component15_isNull, 'SkyEditor2__Date__c', 'Opportunity__r.Opportunity_deadDay__c',Opportunity.fields.Opportunity_deadDay__c, Component15_isNull_op, true, false,true,0,false,'Opportunity__c',OpportunityProduct__c.fields.Opportunity__c )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component6_val, 'SkyEditor2__Text__c', 'Opportunity__r.Opportunity_SalesPerson_Unit__c',Opportunity.fields.Opportunity_SalesPerson_Unit__c, Component6_op, true, false,true,0,false,'Opportunity__c',OpportunityProduct__c.fields.Opportunity__c )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component8_val, 'SkyEditor2__Text__c', 'Opportunity__r.Name',Opportunity.fields.Name, Component8_op, true, false,true,0,false,'Opportunity__c',OpportunityProduct__c.fields.Opportunity__c )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component10_val, 'Opportunity_endClient1__c', 'Opportunity__r.Opportunity_endClient1__c',Opportunity.fields.Opportunity_endClient1__c, Component10_op, true, false,true,0,false,'Opportunity__c',OpportunityProduct__c.fields.Opportunity__c )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component12_val_dummy, 'SkyEditor2__Text__c','Opportunity__r.StageName',Opportunity.fields.StageName, Component12_op, true, false,true,0,false,'Opportunity__c',OpportunityProduct__c.fields.Opportunity__c ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component14_val, 'SkyEditor2__Text__c', 'Opportunity__r.WEBTitle__c',Opportunity.fields.WEBTitle__c, Component14_op, true, false,true,0,false,'Opportunity__c',OpportunityProduct__c.fields.Opportunity__c )) 
				);
			Component3 = new Component3(new List<OpportunityProduct__c>(), new List<Component3Item>(), new List<OpportunityProduct__c>(), null);
			listItemHolders.put('Component3', Component3);
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(OpportunityProduct__c.SObjectType, true);
			p_showHeader = true;
			p_sidebar = true;
			execInitialSearch = false;
			presetSystemParams();
			Component3.extender = this.extender;
			initSearch();
		} catch (SkyEditor2.Errors.SObjectNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.Errors.FieldNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.ExtenderException e) {
			 e.setMessagesToPage();
		} catch (Exception e) {
			System.Debug(LoggingLevel.Error, e);
			SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);
		}
	}
	public List<SelectOption> getOperatorOptions_Opportunity_Opportunity_SalesPerson_Unit_c() { 
		return getOperatorOptions('Opportunity', 'Opportunity_SalesPerson_Unit__c');
	}
	public List<SelectOption> getOperatorOptions_Opportunity_Name() { 
		return getOperatorOptions('Opportunity', 'Name');
	}
	public List<SelectOption> getOperatorOptions_Opportunity_Opportunity_endClient1_c() { 
		return getOperatorOptions('Opportunity', 'Opportunity_endClient1__c');
	}
	public List<SelectOption> getOperatorOptions_Opportunity_StageName_multi() { 
		return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	}
	public List<SelectOption> getOperatorOptions_Opportunity_WEBTitle_c() { 
		return getOperatorOptions('Opportunity', 'WEBTitle__c');
	}
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public OpportunityProduct__c record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, OpportunityProduct__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.ListItemHolder {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (OpportunityProduct__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	public OpportunityProduct__c Component3_table_Conversion { get { return new OpportunityProduct__c();}}
	
	public String Component3_table_selectval { get; set; }
	
	
}