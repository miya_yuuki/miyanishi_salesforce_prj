public with sharing class DeliveryGroupClass {
	
	public static boolean firstRun = true;
	
    public static void prcDeliveryGroup(List<TextManagement2__c> objDelivery)
    {
        // 注文商品のID
        List<String> strOrderItemIds = new List<String>();
        // 納品履歴のグルーピング
        List<AggregateResult> agrDelivery = new List<AggregateResult>();
        // 注文商品オブジェクト
        List<OrderItem__c> updOrderItem = new List<OrderItem__c>();
        // 納品履歴のIDを配列にセット
        for (TextManagement2__c obj : objDelivery) {
            // nullチェック
            if (obj.Bill_Main__c == null || obj.Item2_PaymentDueDate__c == null || obj.DeliveryCount__c == null) continue;
            // 注文商品IDを配列にセット
            strOrderItemIds.add(obj.Item2_OrderProduct__c);
        }
        // 納品履歴を支払期日でグルーピング
        agrDelivery = [
            SELECT 
                Item2_OrderProduct__c itmId,
                Bill_Main__c billDt,
                Item2_PaymentDueDate__c payDt,
                Sum(DeliveryCount__c) total
            FROM 
                TextManagement2__c 
            WHERE 
                Item2_OrderProduct__c = :strOrderItemIds AND 
                Bill_Main__c != null AND
                Item2_PaymentDueDate__c != null AND 
                DeliveryCount__c != null
            GROUP BY
                Bill_Main__c,
                Item2_OrderProduct__c,
                Item2_PaymentDueDate__c
            ORDER BY
                Item2_OrderProduct__c ASC,
                Bill_Main__c ASC,
                Item2_PaymentDueDate__c ASC
        ];
        
        // 注文商品オブジェクト
        OrderItem__c itm = new OrderItem__c();
        // Before Id
        Id beforeId = null;
        
        // グルーピングのループ
        for (AggregateResult obj : agrDelivery) {
            // Before IDの相違
            if (beforeId != (Id)obj.get('itmId')) {
                // 更新用配列に追加
                if (beforeId != null) {
                    updOrderItem.add(itm);
                }
                // インスタンスを生成
                itm = new OrderItem__c(
                    Id = (Id)obj.get('itmId'),
                    Bill_Day1__c = null,
                    Bill_Day2__c = null,
                    Bill_Day3__c = null,
                    Bill_Day4__c = null,
                    Item2_PaymentDueDate1__c = null,
                    Item2_PaymentDueDate2__c = null,
                    Item2_PaymentDueDate3__c = null,
                    Item2_PaymentDueDate4__c = null,
                    PaymentQuantity1__c = null,
                    PaymentQuantity2__c = null,
                    PaymentQuantity3__c = null,
                    PaymentQuantity4__c = null
                );
            }
            // 分割納品（1）
            if (itm.Bill_Day1__c == null) {
                itm.Bill_Day1__c = (Date)obj.get('billDt');
                itm.Item2_PaymentDueDate1__c = (Date)obj.get('payDt');
                itm.PaymentQuantity1__c = (Decimal)obj.get('total');
            }
            // 分割納品（2）
            else if (itm.Bill_Day2__c == null) {
                itm.Bill_Day2__c = (Date)obj.get('billDt');
                itm.Item2_PaymentDueDate2__c = (Date)obj.get('payDt');
                itm.PaymentQuantity2__c = (Decimal)obj.get('total');
            }
            // 分割納品（3）
            else if (itm.Bill_Day3__c == null) {
                itm.Bill_Day3__c = (Date)obj.get('billDt');
                itm.Item2_PaymentDueDate3__c = (Date)obj.get('payDt');
                itm.PaymentQuantity3__c = (Decimal)obj.get('total');
            }
            // 分割納品（4）
            else if (itm.Bill_Day4__c == null) {
                itm.Bill_Day4__c = (Date)obj.get('billDt');
                itm.Item2_PaymentDueDate4__c = (Date)obj.get('payDt');
                itm.PaymentQuantity4__c = (Decimal)obj.get('total');
            }

            // Before IDをセット
            beforeId = (Id)obj.get('itmId');
        }
        // 更新用配列に追加
        if (agrDelivery.size() > 0) updOrderItem.add(itm);
        System.debug('***************************:' + updOrderItem);
        // 注文商品を更新
        if (updOrderItem.size() > 0) update updOrderItem;
    }
}