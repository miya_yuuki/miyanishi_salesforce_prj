global with sharing class RequestKeywordURL_view extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public CustomObject1__c record {get{return (CustomObject1__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	
	
	public Component256 Component256 {get; private set;}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	
	{
	setApiVersion(31.0);
	}
	public RequestKeywordURL_view(ApexPages.StandardController controller) {
		super(controller);


		SObjectField f;

		f = CustomObject1__c.fields.Name;
		f = CustomObject1__c.fields.Status__c;
		f = CustomObject1__c.fields.unit__c;
		f = CustomObject1__c.fields.RequestDay__c;
		f = CustomObject1__c.fields.OwnerId;
		f = CustomObject1__c.fields.RequestEndDay__c;
		f = CustomObject1__c.fields.SalesSupportPerson__c;
		f = CustomObject1__c.fields.SupportJudgePerson__c;
		f = CustomObject1__c.fields.OrderNo__c;
		f = CustomObject1__c.fields.SupportDay__c;
		f = CustomObject1__c.fields.Account__c;
		f = CustomObject1__c.fields.SupportMember__c;
		f = CustomObject1__c.fields.Sign__c;
		f = CustomObject1__c.fields.RDDay__c;
		f = CustomObject1__c.fields.OrderProvisional1__c;
		f = CustomObject1__c.fields.RecordTypeId;
		f = CustomObject1__c.fields.OrderProvisionalMemo1__c;
		f = CustomObject1__c.fields.SalvageScheduledDate__c;
		f = CustomObject1__c.fields.ContractChangeDay__c;
		f = CustomObject1__c.fields.comment__c;
		f = CustomObject1__c.fields.RequestMemo__c;
		f = CustomObject1__c.fields.sentaku__c;
		f = CustomObject1__c.fields.henkougo__c;
		f = SEOChange__c.fields.OrderItem__c;
		f = SEOChange__c.fields.Type__c;
		f = SEOChange__c.fields.KeywordType__c;
		f = SEOChange__c.fields.Keyword__c;
		f = SEOChange__c.fields.URL__c;
		f = SEOChange__c.fields.level__c;
		f = SEOChange__c.fields.Amount__c;
		f = SEOChange__c.fields.sinsei__c;
		f = SEOChange__c.fields.KeywordType2__c;
		f = SEOChange__c.fields.Keyword2__c;
		f = SEOChange__c.fields.URL2__c;
		f = SEOChange__c.fields.level2__c;
		f = SEOChange__c.fields.Amount2__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = CustomObject1__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(CustomObject1__c.SObjectType);
			
			mainQuery = new SkyEditor2.Query('CustomObject1__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('Status__c');
			mainQuery.addFieldAsOutput('unit__c');
			mainQuery.addFieldAsOutput('RequestDay__c');
			mainQuery.addFieldAsOutput('OwnerId');
			mainQuery.addFieldAsOutput('RequestEndDay__c');
			mainQuery.addFieldAsOutput('SalesSupportPerson__c');
			mainQuery.addFieldAsOutput('SupportJudgePerson__c');
			mainQuery.addFieldAsOutput('OrderNo__c');
			mainQuery.addFieldAsOutput('SupportDay__c');
			mainQuery.addFieldAsOutput('Account__c');
			mainQuery.addFieldAsOutput('SupportMember__c');
			mainQuery.addFieldAsOutput('Sign__c');
			mainQuery.addFieldAsOutput('RDDay__c');
			mainQuery.addFieldAsOutput('OrderProvisional1__c');
			mainQuery.addFieldAsOutput('RecordType.Name');
			mainQuery.addFieldAsOutput('OrderProvisionalMemo1__c');
			mainQuery.addFieldAsOutput('SalvageScheduledDate__c');
			mainQuery.addFieldAsOutput('ContractChangeDay__c');
			mainQuery.addFieldAsOutput('comment__c');
			mainQuery.addFieldAsOutput('RequestMemo__c');
			mainQuery.addFieldAsOutput('sentaku__c');
			mainQuery.addFieldAsOutput('henkougo__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			Component256 = new Component256(new List<SEOChange__c>(), new List<Component256Item>(), new List<SEOChange__c>(), null);
			listItemHolders.put('Component256', Component256);
			query = new SkyEditor2.Query('SEOChange__c');
			query.addFieldAsOutput('OrderItem__c');
			query.addFieldAsOutput('Type__c');
			query.addFieldAsOutput('KeywordType__c');
			query.addFieldAsOutput('Keyword__c');
			query.addFieldAsOutput('URL__c');
			query.addFieldAsOutput('level__c');
			query.addFieldAsOutput('Amount__c');
			query.addFieldAsOutput('sinsei__c');
			query.addFieldAsOutput('KeywordType2__c');
			query.addFieldAsOutput('Keyword2__c');
			query.addFieldAsOutput('URL2__c');
			query.addFieldAsOutput('level2__c');
			query.addFieldAsOutput('Amount2__c');
			query.addFieldAsOutput('RecordTypeId');
			query.addWhere('sinsei__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('Component256', 'sinsei__c');
			Component256.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('Component256', query);
			
			registRelatedList('sinsei__r', 'Component256');
			
			p_showHeader = true;
			p_sidebar = false;
			sve_ClassName = 'RequestKeywordURL_view';
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			
			Component256.extender = this.extender;
			if (record.Id == null) {
				
				saveOldValues();
				
				if(record.RecordTypeId == null) recordTypeSelector.applyDefault(record);
				
			}

			
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class Component256Item extends SkyEditor2.ListItem {
		public SEOChange__c record{get; private set;}
		@TestVisible
		Component256Item(Component256 holder, SEOChange__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component256 extends SkyEditor2.ListItemHolder {
		public List<Component256Item> items{get; private set;}
		@TestVisible
			Component256(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component256Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component256Item(this, (SEOChange__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}