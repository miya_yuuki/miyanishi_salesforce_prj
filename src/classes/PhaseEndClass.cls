public with sharing class PhaseEndClass {
	
	public static boolean firstRun = true;
	
	/**
		注文商品のフェーズがすべて"契約終了"になったら、注文、契約のフェーズをすべて"契約終了"に変更する
	**/
	public static void prcSetPhaseEnd(List<OrderItem__c> objOrderItem)
	{
		//----------------------------------------------------------------------
		// 契約IDの配列を定義
		List<String> strContractId = new List<String>();
		// 注文オブジェクトのインスタンスを生成
		List<Order__c> resOrder = new List<Order__c>();		
		// フェーズの配列
		List<String> strPhaseList = new List<String>();
		// 契約オブジェクトのインスタンスを生成
		List<Contract> resContract = new List<Contract>();
		//----------------------------------------------------------------------
		
		// 契約IDを配列にセット
		for (OrderItem__c obj : objOrderItem) strContractId.add(obj.ContractId__c);
		
		// 注文商品オブジェクトを取得
		resOrder = [SELECT Id, Order_Phase__c, (select Item2_Keyword_phase__c from Item2_Relation__r) FROM Order__c WHERE Order_Relation__c IN :strContractId];
		
		// 注文のループ
		for (Order__c obj : resOrder) {
			// インスタンスを生成
			strPhaseList = new List<String>();
			// フェーズのみ配列にセット
			for (OrderItem__c itm : obj.Item2_Relation__r) strPhaseList.add(itm.Item2_Keyword_phase__c);
			// フェーズをセット
			obj.Order_Phase__c = prcJudgePhase(strPhaseList);
		}
		
		// 注文を更新
		if (resOrder.size() > 0) update resOrder;
		
		// 契約オブジェクトを取得
		resContract = [SELECT Id, Phase__c, (select Order_Phase__c from Contract__r) FROM Contract WHERE Id IN :strContractId];
		
		// 契約のループ
		for (Contract obj : resContract) {
			// インスタンスを生成
			strPhaseList = new List<String>();
			// フェーズのみ配列にセット
			for (Order__c odr : obj.Contract__r) strPhaseList.add(odr.Order_Phase__c);
			// フェーズをセット
			obj.Phase__c = prcJudgePhase(strPhaseList);
		}

		// 契約を更新
		if (resContract.size() > 0) update resContract;
	}
	
	// フェーズをチェックする
	private static String prcJudgePhase(List<String> strList)
	{
		Integer intStatus1 = 0;		// 契約中
		Integer intStatus2 = 0;		// 請求停止
		Integer intStatus3 = 0;		// 契約終了
		Integer intStatus4 = 0;		// 契約期間なし
		Integer intStatus5 = 0;		// 途中解約
		Integer intStatus6 = 0;		// 更新済み
		Integer intStatus7 = 0;		// 取消
		
		// フェーズのカウント
		for (Integer i = 0; i < strList.size(); i++) {
			if (strList[i] == '契約中') intStatus1++;
			if (strList[i] == '請求停止') intStatus2++;
			if (strList[i] == '契約終了') intStatus3++;
			if (strList[i] == '契約期間なし') intStatus4++;
			if (strList[i] == '途中解約') intStatus5++;
			if (strList[i] == '更新済み') intStatus6++;
			if (strList[i] == '取消') intStatus7++;
		}
		
		// 契約中=1件以上
		if (intStatus1 >= 1) {
			return '契約中';
		// 請求停止=1件以上
		} else if (intStatus2 >= 1) {
			return '請求停止';
		// 更新済み=全件
		} else if (intStatus1 == 0 && intStatus2 == 0 && intStatus3 == 0 && intStatus4 == 0 && intStatus5 == 0 && intStatus6 >= 1 && intStatus7 == 0) {
			return '更新済み';
		// 取消=全件
		} else if (intStatus1 == 0 && intStatus2 == 0 && intStatus3 == 0 && intStatus4 == 0 && intStatus5 == 0 && intStatus6 == 0 && intStatus7 >= 1) {
			return '取消';
		// 契約終了=契約中0件、契約終了1件以上
		} else if (intStatus1 == 0 && intStatus3 >= 1) {
			return '契約終了';
		// 契約期間なし=契約中0件、契約期間なし1件以上
		} else if (intStatus1 == 0 && intStatus4 >= 1) {
			return '契約期間なし';
		// 途中解約=契約中0件、途中解約1件以上
		} else if (intStatus1 == 0 && intStatus5 >= 1) {
			return '途中解約';
		}
		// それ以外
		return '契約終了';
	}
	
}