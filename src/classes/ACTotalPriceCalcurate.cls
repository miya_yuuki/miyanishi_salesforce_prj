/*
* 取引先別の契約額合計を集計するバッチ処理
* 契約オブジェクトの「フェーズ」が「契約中」のレコードの「月額/合計」を集計する
*/

global class ACTotalPriceCalcurate {

	//  契約のフェーズコードと項目値変換テーブル
	static final Map<Integer, String> contractPhaseCodeMap = new Map<Integer, String>{
		1=>'契約中',
		2=>'請求停止',
		3=>'更新済み',
		4=>'契約期間なし',
		5=>'契約終了',
		6=>'途中解約',
		9=>'その他'
	};

	//  商談のフェーズコードと項目値変換テーブル
	static final Map<Integer, String> opportunityPhaseCodeMap = new Map<Integer, String>{
		1=>'商談中',
		3=>'見込客',
		4=>'無効'
	};

	global ACTotalPriceCalcurate()
	{
	}

    //バッチ処理の開始
    global static PageReference doStartBatch()
    {
    	// 更新対象の取引先レコード登録用のリスト定義
    	List<Account> updAccountList = new List<Account>();

    	String resultText = '■取引先別契約額合計集計<BR><BR>';

		Integer resultCnt = 0;

		// 契約オブジェクトから取引先でグループ化して月額合計、保証金合計を抽出してくる
 		for(AggregateResult ar : [SELECT C.Account.ID, C.Account.Name, sum(C.TotalPrice__c) TotalPrice, sum(C.AmountTotalNoCharge__c) TotalNoCharge, sum(ContractHosyoukin__c) TotalHosyokin
			FROM Contract C
			WHERE C.Phase__c in ('契約中','契約期間なし','請求停止','更新済み')
			GROUP BY C.Account.ID, C.Account.Name
			ORDER BY C.Account.Name asc
			LIMIT 10000]) {

			// SOQLで取得してきた内容をローカル変数に格納
    		String accountId = String.valueOf(ar.get('ID'));
    		String accountName = String.valueOf(ar.get('Name'));
    		Integer totalPrice = Integer.valueOf(ar.get('TotalPrice'));
    		Integer totalNoCharge = Integer.valueOf(ar.get('TotalNoCharge'));
			Integer totalHosyokin = Integer.valueOf(ar.get('TotalHosyokin'));

			// 更新対象の取引先のインスタンスを作成
			Account updItem = new Account(
				Id = accountId,
				AmountTotal__c = totalPrice,
				NoBillAmountTotal__c = totalNoCharge,
				hosyoukin__c = totalHosyokin
			);

			updAccountList.add(updItem);

	    	resultCnt++;
		}

		// 取引先を一括で更新する
		update updAccountList;

    	resultText += '　　' + resultCnt + ' 件<BR><BR>';


    	// 更新対象の取引先レコード登録用のリストを初期化
    	updAccountList = new List<Account>();

    	resultText += '■取引先別契約フェーズ集計<BR><BR>';

		resultCnt = 0;

   		String accountId = '';
   		Integer phaseCode = 0;
   		Date StartDate;
   		Date EndDate;

		// 契約オブジェクトを取引先ごとに集計する、PhaseCodeの一番小さいものを取引先としてのフェーズとして扱う
		List<AggregateResult> contractItems = [SELECT C.Account.ID, min(C.PhaseCode__c) MinPhaseCode, min(C.Contract_StartDate__c) MinStartDate, max(C.Order_EndDate__c) MaxEndDate
			FROM Contract C
			GROUP BY C.Account.ID
			ORDER BY C.Account.ID asc
			LIMIT 20000];

 		for(AggregateResult contractItem : contractItems) {

			// 更新対象の取引先情報を更新
			accountId = String.valueOf(contractItem.get('ID'));
			phaseCode = Integer.valueOf(contractItem.get('MinPhaseCode'));
			startDate = Date.valueOf(contractItem.get('MinStartDate'));
			endDate = Date.valueOf(contractItem.get('MaxEndDate'));

			// 更新対象の取引先のインスタンスを作成
			Account updItem = new Account(
				Id = accountId,
				Phase__c = contractPhaseCodeMap.get(phaseCode),
				StartDay__c = startDate,
				EndDay__c = endDate
			);

			updAccountList.add(updItem);
	    	resultCnt++;
		}

		// 取引先を一括で更新する
		update updAccountList;

    	resultText += '　　' + resultCnt + ' 件<BR><BR>';


    	// 更新対象の取引先レコード登録用のリストを初期化
    	updAccountList = new List<Account>();

    	resultText += '■取引先別商談フェーズ集計<BR><BR>';

		resultCnt = 0;
   		accountId = '';
   		phaseCode = 0;

		// 商談オブジェクトを取引先ごとに集計する、PhaseCodeの一番小さいものを取引先としてのフェーズとして扱う
		List<AggregateResult> opportunityItems = [SELECT O.Account.ID, min(O.PhaseCode__c) MinPhaseCode
			FROM Opportunity O
			WHERE Account.ID != '000000000000000AAA'
			GROUP BY O.Account.ID
			ORDER BY O.Account.ID asc
			LIMIT 20000];

 		for(AggregateResult opportunityItem : opportunityItems) {

			// 更新対象の取引先情報を更新
			accountId = String.valueOf(opportunityItem.get('ID'));
			phaseCode = Integer.valueOf(opportunityItem.get('MinPhaseCode'));

			String opportunityPhaseString = (phaseCode != null) ? opportunityPhaseCodeMap.get(phaseCode) : null;

			// 更新対象の取引先のインスタンスを作成
			Account updItem = new Account(
				Id = accountId,
				OpportunityPhase__c = opportunityPhaseString
			);

			updAccountList.add(updItem);

    		resultCnt++;
		}

		// 取引先を一括で更新する
		update updAccountList;

    	resultText += '　　' + resultCnt + ' 件<BR><BR>';


        // 完了メッセージ
        try {
	        if(ApexPages.currentPage() != null) {
		        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '取引先別契約額合計/フェーズ集計バッチを実行しました。<BR>' + resultText));
	        } else {
	        	System.debug('取引先別契約額合計/フェーズ集計バッチを実行しました。' + resultText);
	        }
        } catch (Exception ex) {
        	System.debug('取引先別契約額合計/フェーズ集計バッチを実行しました。' + resultText);
        }

        return null;
    }
}