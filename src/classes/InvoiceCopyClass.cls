public class InvoiceCopyClass {

	public static boolean firstRun = true;

	public static void prcInvoiceCopy(List<OrderItem__c> objOrderItem, Map<Id, OrderItem__c> oldMap, Boolean upd) {
		// 注文商品のID
		List<String> strOrderItemIds = new List<String>();
		// 請求オブジェクト：紐付け用
		Map<String, Bill__c> mapInvoice = new Map<String, Bill__c>();
		// 請求オブジェクト
		List<Bill__c> insInvoice = new List<Bill__c>();
		// 注文商品オブジェクト
		List<OrderItem__c> objOrderItem2 = new List<OrderItem__c>();
		// 請求商品オブジェクト
		List<BillProduct__c> insInvoItem = new List<BillProduct__c>();

		// 注文商品オブジェクトのループ
		for (OrderItem__c obj : objOrderItem) {
			// 更新する注文商品でなければ処理しない
			if (prcCheckPaymentQuantity(upd, obj, oldMap) == null) continue;
			// フェーズが「請求停止」は処理しない
			if (obj.Item2_Keyword_phase__c == '請求停止') continue;
			if (obj.Item2_Keyword_phase__c == '途中解約') continue;
			if (obj.Item2_Keyword_phase__c == '取消') continue;

			// 配列に追加
			strOrderItemIds.add(obj.Id);
		}

		// 請求レコードを作成
		insInvoice = prcInsertInvoice(strOrderItemIds);

		// 請求をMapに追加
		for (Bill__c obj : insInvoice) mapInvoice.put(obj.Key__c, obj);

		// 注文商品を取得
		objOrderItem2 = [
			SELECT
				Id,
				Name,
				Item2_PaymentDueDateNew__c,
				SalesPerson1__c,
				Item2_Relation__r.BillTo_Contact__c,
				Item2_Relation__r.BillTo_Contact__r.AccountId,
				Item2_Relation__r.Opportunity_Support__c,
				Item2_Relation__r.Order_PaymentMethod__c,
				Item2_Relation__r.Order_PaymentSiteMaster__c,
				Item2_Relation__r.Order_BillingMethod__c,
				Item2_Relation__r.Order_Type__c,
				Item2_Relation__r.Order_Relation__r.Contract_EndClient1__c,
				Item2_Relation__r.Order_Relation__r.OwnerId,
				Item2_Relation__r.Order_Relation__r.Contract_FastOrdersSales__c,
				Item2_Relation__r.Order_Relation__r.SalesUnit1__c,
				Item2_Relation__r.Order_Relation__r.SalesUnit2__c,
				Item2_Relation__r.Order_Relation__r.AccountId,
				OppProduct__r.Opportunity__r.Opportunity_Title__r.AccountId,
				ServiceDate__c,
				EndDate__c,
				Item2_ServiceDate__c,
				Item2_EndData__c,
				Item2_Commission_rate__c,
				Item2_Entry_date__c,
				Item2_Product__c,
				Item2_Unit_selling_price__c,
				Quantity__c,
				PaymentQuantity1__c,
				PaymentQuantity2__c,
				PaymentQuantity3__c,
				PaymentQuantity4__c,
				Item2_Keyword_letter_count__c,
				ProductSeparateUMU__c,
				Order_PaymentSiteMaster__c,
				Billchek__c,
				Billcheck1__c,
				Billcheck2__c,
				Billcheck3__c,
				Billcheck4__c,
				Item2_DeliveryStandards__c,
				BillingTiming__c,
				Item2_Keyword_multiple_strategy_keyword__c,
				OrderItem_keywordType__c,
				Item2_Keyword_strategy_keyword__c,
				OrderItem_URL__c,
				Item2_Delivery_date__c,
				TextCalculation__c,
				Opportunity_Competition__c,
				AmountCheck__c,
				TextPlanning__c,
				TextType__c,
				TextWarranty__c,
				Bill_Main__c,
				Bill_Day1__c,
				Bill_Day2__c,
				Bill_Day3__c,
				Bill_Day4__c
			FROM OrderItem__c
			WHERE Id IN :strOrderItemIds
			AND Item2_Keyword_phase__c != '請求停止'
			AND Item2_Keyword_phase__c != '途中解約'
			AND Item2_Keyword_phase__c != '取消'
		];

		// 注文商品オブジェクトのループ
		for (OrderItem__c obj : objOrderItem2) {
			// 請求年月日
			Date dt = obj.Item2_PaymentDueDateNew__c;

			// 請求年月日
			String strInvoiceDate = dt.year() + '-' + prcItoS(dt.month()) + '-' + prcItoS(dt.day());

			// 請求Keyを生成
			String strKey =
				strInvoiceDate + '_' +
				obj.SalesPerson1__c + '_' +
				obj.Item2_Relation__r.BillTo_Contact__c + '_' +
				obj.Item2_Relation__r.Order_PaymentMethod__c + '_' +
				obj.Item2_Relation__r.Order_PaymentSiteMaster__c + '_' +
				obj.Item2_Relation__r.Order_BillingMethod__c + '_' +
				obj.Item2_Relation__r.Order_Type__c + '_' +
				obj.Item2_Relation__r.Order_Relation__r.Contract_EndClient1__c;

			if (!mapInvoice.containsKey(strKey)) continue;

			// 請求商品Keyを生成
			String strItemKey = obj.Name + '_' + strInvoiceDate;

			// Key
			System.debug('#########Key:' + strItemKey);

			// 請求商品を作成
			BillProduct__c newInvoItem = new BillProduct__c(
				Key__c = strItemKey,                                            /* Key */
				Bill2__c = mapInvoice.get(strKey).Id,                           /* 請求ID */
				OrderItem__c = obj.Id,                                          /* 注文商品ID */
				Contract_FastOrdersSales__c = obj.Item2_Relation__r.Order_Relation__r.Contract_FastOrdersSales__c,  /* 受注時営業担当 */
				BillAccountName__c = obj.Item2_Relation__r.Order_Relation__r.AccountId, /* 契約取引先 */
				BillTo_Account__c = obj.Item2_Relation__r.BillTo_Contact__r.AccountId,  /* 請求取引先 */
				Bill2_SalesPerson__c = obj.Item2_Relation__r.Order_Relation__r.OwnerId, /* 担当営業 */
				SalesUnit1__c = obj.Item2_Relation__r.Order_Relation__r.SalesUnit1__c,  /* 担当営業ユニット */
				SalesUnit2__c = obj.Item2_Relation__r.Order_Relation__r.SalesUnit2__c,  /* 受注時営業担当ユニット */
				BillSupportPersonName__c = obj.Item2_Relation__r.Opportunity_Support__c,   /* サポート担当 */
				Bill2_PaymentDueDate__c = obj.Item2_PaymentDueDateNew__c,       /* 支払期日 */
				ServiceDate__c = obj.ServiceDate__c,                            /* 契約開始日 */
				EndData__c = obj.EndDate__c,                                    /* 契約終了日 */
				Bill2_ServiceDate__c = obj.Item2_ServiceDate__c,                /* 請求対象開始日 */
				Bill2_EndDate__c = obj.Item2_EndData__c,                        /* 請求対象終了日 */
				Bill2_Commission_rate__c = obj.Item2_Commission_rate__c,        /* 代理店手数料 */
				Bill2_Entry_date__c = obj.Item2_Entry_date__c,                  /* 申込日 */
				Bill2_Product__c = obj.Item2_Product__c,                        /* 商品 */
				Bill2_Price__c = obj.Item2_Unit_selling_price__c,               /* 単価 */
				Bill2_Count__c = prcCheckPaymentQuantity(upd, obj, oldMap),     /* 数量 */
				Bill2_TextCount__c = obj.Item2_Keyword_letter_count__c,         /* 文字数 */
				Bill2_KeywordURL__c = obj.Item2_Keyword_multiple_strategy_keyword__c,   /* 対策キーワード（複数） or 対策URL */
				Bill2_keywordType__c = obj.OrderItem_keywordType__c,            /* キーワード種別 */
				Bill2_Keyword__c = obj.Item2_Keyword_strategy_keyword__c,       /* 対策キーワード */
				Bill2_URL__c = obj.OrderItem_URL__c,                            /* URL */
				TextCalculation__c = obj.TextCalculation__c,                    /* 文字数金額集計 */
				Opportunity_Competition__c = obj.Opportunity_Competition__c,    /* 区分 */
				Bill2_DeliveryCompletionDate__c = obj.Item2_Delivery_date__c,   /* 納品完了日 */
				AmountCheck__c = obj.AmountCheck__c,                            /* 合計切り捨てなし */
				TextPlanning__c = obj.TextPlanning__c,                          /* テキスト企画 */
				TextType__c = obj.TextType__c,                                  /* 記事タイプ */
				TextWarranty__c = obj.TextWarranty__c,                          /* 送客保証 */
				Bill_TargetDay__c = prcCheckBillDate(upd, obj, oldMap)          /* 請求対象日 */
			);

			// 配列に追加
			insInvoItem.add(newInvoItem);
		}

		// 請求商品を作成
		if (insInvoItem.size() > 0) upsert insInvoItem;
		// 請求を更新
		if (insInvoice.size() > 0) update insInvoice;
		// 請求商品を更新
		if (insInvoItem.size() > 0) update insInvoItem;
	}

	// 請求レコード作成済みフラグから請求書発行日を抽出する
	private static Date prcCheckBillDate(Boolean upd, OrderItem__c objNew, Map<Id, OrderItem__c> oldMap) {
		// 請求レコード作成済みフラグチェック
		if (upd) {
			// 古い値を取得
			OrderItem__c objOld = oldMap.get(objNew.Id);
			// 分割納品無し
			if (objNew.ProductSeparateUMU__c == '無' || (objNew.ProductSeparateUMU__c == '有' && objNew.BillingTiming__c == '受注') || (objNew.ProductSeparateUMU__c == '有' && objNew.Order_PaymentSiteMaster__c == '前払い')) {
				if (objOld.Billchek__c == false && objNew.Billchek__c == true) return objNew.Bill_Main__c;
			}
			// 分割納品有り
			else if (objNew.ProductSeparateUMU__c == '有') {
				if (objOld.Billcheck1__c == false && objNew.Billcheck1__c == true) return objNew.Bill_Day1__c;
				if (objOld.Billcheck2__c == false && objNew.Billcheck2__c == true) return objNew.Bill_Day2__c;
				if (objOld.Billcheck3__c == false && objNew.Billcheck3__c == true) return objNew.Bill_Day3__c;
				if (objOld.Billcheck4__c == false && objNew.Billcheck4__c == true) return objNew.Bill_Day4__c;
			}
		}
		// 新規作成
		else {
			// 分割納品無し
			if (objNew.ProductSeparateUMU__c == '無' || (objNew.ProductSeparateUMU__c == '有' && objNew.BillingTiming__c == '受注') || (objNew.ProductSeparateUMU__c == '有' && objNew.Order_PaymentSiteMaster__c == '前払い')) {
				if (objNew.Billchek__c == true) return objNew.Bill_Main__c;
			}
			// 分割納品有り
			else if (objNew.ProductSeparateUMU__c == '有') {
				if (objNew.Billcheck1__c == true) return objNew.Bill_Day1__c;
				if (objNew.Billcheck2__c == true) return objNew.Bill_Day2__c;
				if (objNew.Billcheck3__c == true) return objNew.Bill_Day3__c;
				if (objNew.Billcheck4__c == true) return objNew.Bill_Day4__c;
			}
		}
		// 条件に一致しない
		return null;
	}

	// 請求レコード作成済みフラグから請求数量を抽出する
	private static Decimal prcCheckPaymentQuantity(Boolean upd, OrderItem__c objNew, Map<Id, OrderItem__c> oldMap) {
		// 請求レコード作成済みフラグチェック
		if (upd) {
			// 古い値を取得
			OrderItem__c objOld = oldMap.get(objNew.Id);
			// 分割納品無し
			if (objNew.ProductSeparateUMU__c == '無' || (objNew.ProductSeparateUMU__c == '有' && objNew.BillingTiming__c == '受注') || (objNew.ProductSeparateUMU__c == '有' && objNew.Order_PaymentSiteMaster__c == '前払い')) {
				if (objOld.Billchek__c == false && objNew.Billchek__c == true) return objNew.Quantity__c;
			// 分割納品有り
			}
			else if (objNew.ProductSeparateUMU__c == '有') {
				if (objOld.Billcheck1__c == false && objNew.Billcheck1__c == true) return objNew.PaymentQuantity1__c;
				if (objOld.Billcheck2__c == false && objNew.Billcheck2__c == true) return objNew.PaymentQuantity2__c;
				if (objOld.Billcheck3__c == false && objNew.Billcheck3__c == true) return objNew.PaymentQuantity3__c;
				if (objOld.Billcheck4__c == false && objNew.Billcheck4__c == true) return objNew.PaymentQuantity4__c;
			}
		}
		// 新規作成
		else {
			// 分割納品無し
			if (objNew.ProductSeparateUMU__c == '無' || (objNew.ProductSeparateUMU__c == '有' && objNew.BillingTiming__c == '受注') || (objNew.ProductSeparateUMU__c == '有' && objNew.Order_PaymentSiteMaster__c == '前払い')) {
				if (objNew.Billchek__c == true) return objNew.Quantity__c;
			}
			// 分割納品有り
			else if (objNew.ProductSeparateUMU__c == '有') {
				if (objNew.Billcheck1__c == true) return objNew.PaymentQuantity1__c;
				if (objNew.Billcheck2__c == true) return objNew.PaymentQuantity2__c;
				if (objNew.Billcheck3__c == true) return objNew.PaymentQuantity3__c;
				if (objNew.Billcheck4__c == true) return objNew.PaymentQuantity4__c;
			}
		}
		// 条件に一致しない
		return null;
	}

	private static List<Bill__c> prcInsertInvoice(List<String> strOrderItemIds) {
		// 注文商品のグルーピング
		List<AggregateResult> agrOrderItem = new List<AggregateResult>();
		// 請求オブジェクト：作成用
		List<Bill__c> insInvoice = new List<Bill__c>();

		// 注文商品・注文オブジェクトをKey項目でグルーピングして取得
		agrOrderItem = [
			SELECT
				Item2_PaymentDueDateNew__c dt,                                              /* 支払期日 */
				SalesPerson1__c sales,                                                      /* 担当営業 */
				Item2_Relation__r.Opportunity_Support__c support,                           /* サポート担当 */
				Item2_Relation__r.Order_Relation__r.AccountId account,						/* 取引先 */
				Item2_Relation__r.BillTo_Contact__c contact,                                /* 取引先責任者ID */
				Item2_Relation__r.Order_PaymentMethod__c payment,                           /* 支払い方法 */
				Item2_Relation__r.Order_PaymentSiteMaster__c site,                          /* 支払いサイト */
				Item2_Relation__r.Order_BillingMethod__c bill,                              /* 請求方法 */
				Item2_Relation__r.Order_Type__c otype,                                      /* 案件種別 */
				Item2_Relation__r.Order_Relation__r.Contract_EndClient1__c client,          /* 施策企業ID */
				Item2_Relation__r.Order_Relation__r.Contract_Patnername__c partner,         /* セールスパートナーID */
				Item2_Relation__r.Order_Relation__r.Contract_Patnername__r.partner_contract__c contract		/* パートナー契約 */
			FROM
				OrderItem__c
			WHERE
				Id IN :strOrderItemIds
				AND Item2_PaymentDueDateNew__c != null
			GROUP BY
				Item2_PaymentDueDateNew__c,
				SalesPerson1__c,
				Item2_Relation__r.Order_Relation__r.AccountId,
				Item2_Relation__r.Opportunity_Support__c,
				Item2_Relation__r.BillTo_Contact__c,
				Item2_Relation__r.Order_PaymentMethod__c,
				Item2_Relation__r.Order_PaymentSiteMaster__c,
				Item2_Relation__r.Order_PaymentSiteMaster__r.Name,
				Item2_Relation__r.Order_BillingMethod__c,
				Item2_Relation__r.Order_Type__c,
				Item2_Relation__r.Order_Relation__r.Contract_EndClient1__c,
				Item2_Relation__r.Order_Relation__r.Contract_Patnername__c,
				Item2_Relation__r.Order_Relation__r.Contract_Patnername__r.partner_contract__c
		];

		// 請求オブジェクトの作成
		for (AggregateResult obj : agrOrderItem) {
			// 請求年月日
			Date dt = (Date)obj.get('dt');
			// 支払いサイト
			//String sitemaster = (String)obj.get('sitemaster');

			// 請求年月日（文字列）
			//String strInvoiceDate = null;
			//if (sitemaster != '前払い') {
				String strInvoiceDate = dt.year() + '-' + prcItoS(dt.month()) + '-' + prcItoS(dt.day());
			//}
			//else {
				//strInvoiceDate = dt.year() + '-' + prcItoS(dt.month()) + '-01';
			//}

			// 請求Keyを生成
			String strKey =
				strInvoiceDate + '_' +
				(Id)obj.get('sales') + '_' +
				(Id)obj.get('contact') + '_' +
				(String)obj.get('payment') + '_' +
				(Id)obj.get('site') + '_' +
				(String)obj.get('bill') + '_' +
				(String)obj.get('otype') + '_' +
				(Id)obj.get('client');

			// 請求のインスタンスを生成
			Bill__c newInvoice = new Bill__c(
				Key__c = strKey,
				InvoiceDate__c = (Date)obj.get('dt'),
				BillSupportPersonName__c = (Id)obj.get('support'),
				AccountID__c = (Id)obj.get('account'),
				Bill_Contact__c = (Id)obj.get('contact'),
				Bill_PaymentMethod__c = (String)obj.get('payment'),
				Bill_PaymentSiteMaster__c = (Id)obj.get('site'),
				Bill_BillingMethod__c = (String)obj.get('bill'),
				Bill_Type__c = (String)obj.get('otype'),
				Bill_EndClient__c = (Id)obj.get('client'),
				SalesPartner__c = (Id)obj.get('partner'),
				PartnerContract__c = (Id)obj.get('contract')
			);

			// 配列に追加
			insInvoice.add(newInvoice);
		}

		// 請求レコードをUpsertする
		if (insInvoice.size() > 0) upsert insInvoice Key__c;

		return insInvoice;
	}

	private static String prcItoS(Integer intVal) {
		if (intVal < 10) return '0' + String.valueOf(intVal);
		return String.valueOf(intVal);
	}
}