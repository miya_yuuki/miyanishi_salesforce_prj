global with sharing class Opportunity_IntroductionFee extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public OpportunityProduct__c record {get{return (OpportunityProduct__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	
	
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	
	{
	setApiVersion(31.0);
	}
	public Opportunity_IntroductionFee(ApexPages.StandardController controller) {
		super(controller);


		SObjectField f;

		f = OpportunityProduct__c.fields.AccountName__c;
		f = OpportunityProduct__c.fields.sisakuompany__c;
		f = OpportunityProduct__c.fields.Opportunity__c;
		f = OpportunityProduct__c.fields.Price__c;
		f = OpportunityProduct__c.fields.Product__c;
		f = OpportunityProduct__c.fields.Quantity__c;
		f = OpportunityProduct__c.fields.Item_Detail_ProductView__c;
		f = OpportunityProduct__c.fields.TotalPrice__c;
		f = OpportunityProduct__c.fields.ProductTypeView__c;
		f = OpportunityProduct__c.fields.SalesPerson__c;
		f = OpportunityProduct__c.fields.Opportunity_DeliveryExistenceView__c;
		f = OpportunityProduct__c.fields.RecordTypeId;
		f = OpportunityProduct__c.fields.DeliveryStandardsView__c;
		f = OpportunityProduct__c.fields.Name;
		f = OpportunityProduct__c.fields.Item_Special_instruction__c;
		f = OpportunityProduct__c.fields.SalesPersonMainUnit__c;
		f = OpportunityProduct__c.fields.SalesPersonSubUnit__c;
		f = OpportunityProduct__c.fields.SalesPersonMain__c;
		f = OpportunityProduct__c.fields.SalesPersonSub__c;
		f = OpportunityProduct__c.fields.SalesPercentage1__c;
		f = OpportunityProduct__c.fields.SalesPercentage2__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = OpportunityProduct__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(OpportunityProduct__c.SObjectType);
			
			mainQuery = new SkyEditor2.Query('OpportunityProduct__c');
			mainQuery.addField('Opportunity__c');
			mainQuery.addField('Price__c');
			mainQuery.addField('Product__c');
			mainQuery.addField('Quantity__c');
			mainQuery.addField('Item_Special_instruction__c');
			mainQuery.addField('SalesPersonMainUnit__c');
			mainQuery.addField('SalesPersonMain__c');
			mainQuery.addField('SalesPersonSub__c');
			mainQuery.addField('SalesPercentage1__c');
			mainQuery.addField('SalesPercentage2__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('AccountName__c');
			mainQuery.addFieldAsOutput('sisakuompany__c');
			mainQuery.addFieldAsOutput('Item_Detail_ProductView__c');
			mainQuery.addFieldAsOutput('TotalPrice__c');
			mainQuery.addFieldAsOutput('ProductTypeView__c');
			mainQuery.addFieldAsOutput('SalesPerson__c');
			mainQuery.addFieldAsOutput('Opportunity_DeliveryExistenceView__c');
			mainQuery.addFieldAsOutput('RecordType.Name');
			mainQuery.addFieldAsOutput('DeliveryStandardsView__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('SalesPersonSubUnit__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('Opportunity__c', 'CF00N10000005jJYH_lkid');
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			
			if (record.Id == null) {
				
				saveOldValues();
				
				if(record.RecordTypeId == null) recordTypeSelector.applyDefault(record);
				
			}

			
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}