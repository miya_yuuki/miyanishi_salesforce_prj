global class ContractSummaryRecordUpdate {
    global static PageReference ContractSummaryUpdate() {
        List<ContractName__c> updContractName = new List<ContractName__c>();

        List<ContractName__c> chkContractName = [
            SELECT
                Id,                             // 契約集計用ID
                LastModifiedDate,               // 契約集計用最終更新日
                ContractName__c,                // 契約ID
                ContractLastModifiedDate__c,    // 契約最終更新日
                ContractName__r.Phase__c,       // 契約フェーズ
                AmountTotal1__c,                // 契約金額合計
                StartDate1__c,                  // 契約開始日
                EndDate__c,                     // 契約終了日
                Delivery_dateEnd1__c,           // 納品完了日
                NoDeliveryCount1__c,            // 未納品数
                AmountTotalNoCharge1__c,        // 売上高
                billAmountTax1__c,              // 売上高消費税
                hosyoukin1__c,                  // 保証金
                Last_Billing_Cutoff_Date_1__c,  // 一番遅い請求対象終了日
                Last_Delivery_Date_1__c,        // 一番遅い納品完了日
                Undelivered_Records_1__c        // 未納品レコード数
            FROM ContractName__c
            WHERE
                ContractLastModifiedDate__c = LAST_N_DAYS:7
                AND LastModifiedDate <> TODAY
            order by ContractLastModifiedDate__c desc
            limit 2000
        ];
        for(ContractName__c chk : chkContractName) {
            ContractName__c newContractName = new ContractName__c(
                Id = chk.Id,                                                        // 契約集計用ID
                ContractName__c = chk.ContractName__c,                              // 契約ID
                ContractPhase__c = chk.ContractName__r.Phase__c,                    // 契約フェーズ
                AmountTotal__c = chk.AmountTotal1__c,                               // 契約金額合計
                StartDate__c = chk.StartDate1__c,                                   // 契約開始日
                EndDay__c = chk.EndDate__c,                                         // 契約終了日
                Delivery_dateEnd__c = chk.Delivery_dateEnd1__c,                     // 納品完了日
                NoDeliveryCount__c = chk.NoDeliveryCount1__c,                       // 未納品数
                NoBillAmountTotal__c = chk.AmountTotalNoCharge1__c,                 // 売上高
                billAmountTax__c = chk.billAmountTax1__c,                           // 売上高消費税
                hosyoukin__c = chk.hosyoukin1__c,                                   // 保証金合計
                Last_Billing_Cutoff_Date__c = chk.Last_Billing_Cutoff_Date_1__c,    // 一番遅い請求対象終了日
                Last_Delivery_Date__c = chk.Last_Delivery_Date_1__c,                // 一番遅い納品完了日
                Undelivered_Records__c = chk.Undelivered_Records_1__c               // 未納品レコード数
            );
            updContractName.add(newContractName);
            System.debug('契約ID: ' + chk.ContractName__c + ', 契約最終更新日: ' + chk.ContractLastModifiedDate__c + ', 集計用ID: ' + chk.Id + ', 集計用最終更新日: ' + chk.LastModifiedDate);
            System.debug('集計用ID: ' + chk.Id + ', 契約フェーズ: ' + chk.ContractName__r.Phase__c + ', StartDate: ' + chk.StartDate1__c + ', EndDate: ' + chk.EndDate__c + ', Delivery_dateEnd: ' + chk.Delivery_dateEnd1__c);
        }

        if(updContractName.size() > 0) {
            System.debug('更新対象: ' + updContractName.size() + ' 件');
            update updContractName;
        }
        else {
            System.debug('更新対象: なし');
        }

        return null;
    }
}