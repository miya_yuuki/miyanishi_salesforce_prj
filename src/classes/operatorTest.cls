@isTest
public class operatorTest {
    static testMethod void testInsert() {
    	// 稟議＆申請
		teamspirit__AtkApply__c testAtkApply = new teamspirit__AtkApply__c(
			RecordTypeId = '01210000000ATOe',
			contract_months__c = 3
		);
		insert testAtkApply;
        
        // 取引執行管理
		operator__c testOperator = new operator__c(
			RecordTypeId = '01210000000ATj5',
			orijginal_order__c = testAtkApply.Id,
			details__c = 'テスト取引執行管理詳細',
			keiyakugessu__c = 3
		);
		insert testOperator;
    }
}