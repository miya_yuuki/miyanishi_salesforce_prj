global with sharing class SVE_contract_products extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public contract_products__c record{get;set;}	
			
	
		public Component3 Component3 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component13_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component13_to{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component17_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component17_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component17_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component17_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component19_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component19_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component19_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component19_isNull_op{get;set;}	
			
		public contract_products__c Component6_val {get;set;}	
		public SkyEditor2.TextHolder Component6_op{get;set;}	
			
		public contract_products__c Component8_val {get;set;}	
		public SkyEditor2.TextHolder Component8_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component10_val {get;set;}	
		public SkyEditor2.TextHolder Component10_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component12_val {get;set;}	
		public SkyEditor2.TextHolder Component12_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component16_val {get;set;}	
		public SkyEditor2.TextHolder Component16_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component82_val {get;set;}	
		public SkyEditor2.TextHolder Component82_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component84_val {get;set;}	
		public SkyEditor2.TextHolder Component84_op{get;set;}	
			
	public String recordTypeRecordsJSON_contract_products_c {get; private set;}
	public String defaultRecordTypeId_contract_products_c {get; private set;}
	public String metadataJSON_contract_products_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public SVE_contract_products(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = contract_products__c.fields.conpany_del_del__c;
		f = contract_products__c.fields.torihikishikkou_no__c;
		f = contract_products__c.fields.kikan__c;
		f = contract_products__c.fields.kikan_end__c;
		f = contract_products__c.fields.pay_rule__c;
		f = contract_products__c.fields.bugyo_check__c;
		f = contract_products__c.fields.cost_department__c;
		f = contract_products__c.fields.Name;
		f = contract_products__c.fields.denpyo__c;
		f = contract_products__c.fields.payday__c;
		f = contract_products__c.fields.pay_day__c;
		f = contract_products__c.fields.include_tax1__c;
		f = contract_products__c.fields.order_products__c;
		f = contract_products__c.fields.left_bugyo_maintitle__c;
		f = contract_products__c.fields.left_bugyo_subtitle__c;
		f = contract_products__c.fields.right_bugyo_maintitle__c;
		f = contract_products__c.fields.right_bugyo_subtitle__c;
 f = contract_products__c.fields.include_tax1__c;
 f = contract_products__c.fields.pay_day__c;
 f = contract_products__c.fields.payday__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = contract_products__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component13_from = new SkyEditor2__SkyEditorDummy__c();	
				Component13_to = new SkyEditor2__SkyEditorDummy__c();	
					
				Component17_from = new SkyEditor2__SkyEditorDummy__c();	
				Component17_to = new SkyEditor2__SkyEditorDummy__c();	
				Component17_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component17_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component19_from = new SkyEditor2__SkyEditorDummy__c();	
				Component19_to = new SkyEditor2__SkyEditorDummy__c();	
				Component19_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component19_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				contract_products__c lookupObjComponent44 = new contract_products__c();	
				Component6_val = lookupObjComponent44;	
				Component6_op = new SkyEditor2.TextHolder();	
					
				Component8_val = lookupObjComponent44;	
				Component8_op = new SkyEditor2.TextHolder();	
					
				Component10_val = new SkyEditor2__SkyEditorDummy__c();	
				Component10_op = new SkyEditor2.TextHolder();	
					
				Component12_val = new SkyEditor2__SkyEditorDummy__c();	
				Component12_op = new SkyEditor2.TextHolder();	
					
				Component16_val = new SkyEditor2__SkyEditorDummy__c();	
				Component16_op = new SkyEditor2.TextHolder();	
					
				Component82_val = new SkyEditor2__SkyEditorDummy__c();	
				Component82_op = new SkyEditor2.TextHolder();	
					
				Component84_val = new SkyEditor2__SkyEditorDummy__c();	
				Component84_op = new SkyEditor2.TextHolder();	
					
				queryMap.put(	
					'Component3',	
					new SkyEditor2.Query('contract_products__c')
						.addFieldAsOutput('torihikishikkou_no__c')
						.addFieldAsOutput('Name')
						.addFieldAsOutput('conpany_del_del__c')
						.addField('denpyo__c')
						.addField('kikan__c')
						.addField('payday__c')
						.addFieldAsOutput('pay_day__c')
						.addFieldAsOutput('include_tax1__c')
						.addFieldAsOutput('pay_rule__c')
						.addFieldAsOutput('cost_department__c')
						.addField('bugyo_check__c')
						.addField('left_bugyo_maintitle__c')
						.addField('left_bugyo_subtitle__c')
						.addField('right_bugyo_maintitle__c')
						.addField('right_bugyo_subtitle__c')
						.addFieldAsOutput('order_products__c')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component13_from, 'SkyEditor2__Text__c', 'include_tax1__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component13_to, 'SkyEditor2__Text__c', 'include_tax1__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component17_from, 'SkyEditor2__Date__c', 'pay_day__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component17_to, 'SkyEditor2__Date__c', 'pay_day__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component17_isNull, 'SkyEditor2__Date__c', 'pay_day__c', Component17_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component19_from, 'SkyEditor2__Date__c', 'payday__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component19_to, 'SkyEditor2__Date__c', 'payday__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component19_isNull, 'SkyEditor2__Date__c', 'payday__c', Component19_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component6_val, 'conpany_del_del__c', 'conpany_del_del__c', Component6_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component8_val, 'torihikishikkou_no__c', 'torihikishikkou_no__c', Component8_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component10_val, 'SkyEditor2__Date__c', 'kikan__c', Component10_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component12_val, 'SkyEditor2__Date__c', 'kikan_end__c', Component12_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component16_val, 'SkyEditor2__Text__c', 'pay_rule__c', Component16_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component82_val, 'SkyEditor2__Checkbox__c', 'bugyo_check__c', Component82_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component84_val, 'SkyEditor2__Text__c', 'cost_department__c', Component84_op, true, 0, false ))
				);	
					
					Component3 = new Component3(new List<contract_products__c>(), new List<Component3Item>(), new List<contract_products__c>(), null);
				listItemHolders.put('Component3', Component3);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(contract_products__c.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = true;
			execInitialSearch = false;
			presetSystemParams();
			Component3.extender = this.extender;
			initSearch();
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_contract_products_c_conpany_del_del_c() { 
			return getOperatorOptions('contract_products__c', 'conpany_del_del__c');	
		}	
		public List<SelectOption> getOperatorOptions_contract_products_c_torihikishikkou_no_c() { 
			return getOperatorOptions('contract_products__c', 'torihikishikkou_no__c');	
		}	
		public List<SelectOption> getOperatorOptions_contract_products_c_kikan_c() { 
			return getOperatorOptions('contract_products__c', 'kikan__c');	
		}	
		public List<SelectOption> getOperatorOptions_contract_products_c_kikan_end_c() { 
			return getOperatorOptions('contract_products__c', 'kikan_end__c');	
		}	
		public List<SelectOption> getOperatorOptions_contract_products_c_pay_rule_c() { 
			return getOperatorOptions('contract_products__c', 'pay_rule__c');	
		}	
		public List<SelectOption> getOperatorOptions_contract_products_c_bugyo_check_c() { 
			return getOperatorOptions('contract_products__c', 'bugyo_check__c');	
		}	
		public List<SelectOption> getOperatorOptions_contract_products_c_cost_department_c() { 
			return getOperatorOptions('contract_products__c', 'cost_department__c');	
		}	
			
			
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public contract_products__c record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, contract_products__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.ListItemHolder {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (contract_products__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	public contract_products__c Component3_table_Conversion { get { return new contract_products__c();}}
	
	public String Component3_table_selectval { get; set; }
	
	
			
	}