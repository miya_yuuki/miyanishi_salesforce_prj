@isTest
private class TextManagementCompletedCheckTest {
	static testMethod void testTextManagementCompletedCheck() {
		Test.startTest();
		String jobId = System.schedule('test TextManagementCompletedCheck', '0 0 * * * ?', new ScheduledContractSummaryUpdate());
		System.debug('**** job id : ' + jobId + ' ***** ***** *****');
		Test.stopTest();
	}
}