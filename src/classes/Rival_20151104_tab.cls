global with sharing class Rival_20151104_tab extends SkyEditor2.SkyEditorPageBaseWithSharing{
	
	public RivalCompany__c record{get;set;}
	public Component3 Component3 {get; private set;}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	public SkyEditor2__SkyEditorDummy__c Component19{get;set;}
	{
	setApiVersion(31.0);
	}
	public Rival_20151104_tab(ApexPages.StandardController controller){
		super(controller);

		SObjectField f;

		f = RivalCompany__c.fields.Name;
		f = RivalCompany__c.fields.ManagementNo__c;
		f = RivalCompany__c.fields.service__c;
		f = RivalCompany__c.fields.LastModifiedDate;
		f = RivalCompany__c.fields.CreatedDate;

		try {
			mainRecord = null;
			mainSObjectType = RivalCompany__c.SObjectType;
			mode = SkyEditor2.LayoutMode.TempProductLookup_01;
			
			Component19 = new SkyEditor2__SkyEditorDummy__c();
			
			queryMap.put(
				'Component3',
				new SkyEditor2.Query('RivalCompany__c')
					.addFieldAsOutput('ManagementNo__c')
					.addFieldAsOutput('Name')
					.addFieldAsOutput('service__c')
					.addFieldAsOutput('LastModifiedDate')
					.addFieldAsOutput('CreatedDate')
					.addField('Name')
					.limitRecords(500)
					.addListener(new SkyEditor2.QueryWhereRegister(Component19, 'SkyEditor2__Text__c', 'Name', new SkyEditor2.TextHolder('co'), false, true, false))
			);
			
			Component3 = new Component3(new List<RivalCompany__c>(), new List<Component3Item>(), new List<RivalCompany__c>(), null);
			listItemHolders.put('Component3', Component3);
			Component3.ignoredOnSave = true;
			
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(RivalCompany__c.SObjectType);
			
			p_showHeader = false;
			p_sidebar = false;
			presetSystemParams();
			initSearch();
			
		} catch (SkyEditor2.Errors.SObjectNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.Errors.FieldNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.ExtenderException e){
			e.setMessagesToPage();
		} catch (SkyEditor2.Errors.PricebookNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
			hidePageBody = true;
		}
	}

	public List<SelectOption> getOperatorOptions_RivalCompany_c_Name() {
		return getOperatorOptions('RivalCompany__c', 'Name');
	}
	
	
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public RivalCompany__c record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, RivalCompany__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.ListItemHolder {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (RivalCompany__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	
}