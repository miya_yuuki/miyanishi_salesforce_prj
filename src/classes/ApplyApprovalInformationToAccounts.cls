public with sharing class ApplyApprovalInformationToAccounts {
    public static boolean firstRun = true;

    public static void applyApprovalInformation(List<teamspirit__AtkApply__c> newApprovalInformationList, Map<Id, teamspirit__AtkApply__c> oldApprovalInformationMap) {
        // 配列・Mapの初期化
        teamspirit__AtkApply__c oldObj = new teamspirit__AtkApply__c();
        List<String> updAccountIds = new List<String>();
        List<Account> updAccountObjs = new List<Account>();
        Map<Id,teamspirit__AtkApply__c> updAtkApplyMap = new Map<Id,teamspirit__AtkApply__c>();
        teamspirit__AtkApply__c updAtkApply = new teamspirit__AtkApply__c();
        
        // 更新対象の洗い出し
		for (teamspirit__AtkApply__c newObj : newApprovalInformationList) {
            oldObj = oldApprovalInformationMap.get(newObj.Id);
            // 与信・反社申請の場合のみ
            if (newObj.RecordTypeId == '01210000000ATZSAA4') {
                // 承認ステータスが「承認済み」に変化したタイミング
                if (oldObj.teamspirit__Status__c != '承認済み' && newObj.teamspirit__Status__c == '承認済み') {
                    updAtkApplyMap.put(newObj.account_name__c, newObj);
                    updAccountIds.add(newObj.account_name__c);
                }
            }
		}

        if (updAccountIds.size() <= 0) {
            return;
        }

        System.debug('-------以下の与信・反社申請の内容を取引先に引継ぎます。----------');
        for (Id accountId : updAccountIds) {
            System.debug(accountId);
        }

        updAccountObjs = [
			SELECT 
                Id, // 取引先ID
                CompanyCheck__c, // 反社チェック結果
                hansyaDay__c, // 反社チェック
                hansyaMemo__c, // 反社チェック備考
                account_CreditLevel__c, // 格付
                account_CreditDay__c, // 最終格付日
                PaymentSiteMaster__c // 支払いサイト
			FROM Account
			WHERE Id IN :updAccountIds
		];
        
        for (Account updAccountObj : updAccountObjs) {
            updAtkApply = updAtkApplyMap.get(updAccountObj.Id);
            
            // 与信チェックが実行された場合
            if (updAtkApply.first_yoshin__c != null) {
                updAccountObj.account_CreditDay__c = updAtkApply.first_yoshin__c;
                updAccountObj.account_CreditLevel__c = updAtkApply.yoshin__c;
            }
            
            // 反社チェックが実行された場合
            if (updAtkApply.resarch_day__c != null) {
                updAccountObj.CompanyCheck__c = updAtkApply.CompanyCheck__c;
                updAccountObj.hansyaDay__c = updAtkApply.resarch_day__c;
                // 備考に記載がある場合は上書きする
                if (updAtkApply.hansyaMemo__c != null) {
                    updAccountObj.hansyaMemo__c = updAtkApply.hansyaMemo__c;
                }
            }

            // 支払いサイト変更がある場合
            if (updAtkApply.change_shiharai__c == true) {
                updAccountObj.PaymentSiteMaster__c = updAtkApply.changed_shiharai__c;
            }
		}
        if (updAccountObjs.size() > 0) {
			update updAccountObjs;
		}
    }
}