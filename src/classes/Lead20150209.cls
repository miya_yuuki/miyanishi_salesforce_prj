global with sharing class Lead20150209 extends SkyEditor2.SkyEditorPageBaseWithSharing {
    
    public Lead record {get{return (Lead)mainRecord;}}
    public with sharing class CanvasException extends Exception {}

    
    
    public Lead20150209(ApexPages.StandardController controller) {
        super(controller);


        SObjectField f;

        f = Lead.fields.Company;
        f = Lead.fields.LastName;
        f = Lead.fields.FirstName;
        f = Lead.fields.lead_kana__c;
        f = Lead.fields.lead_Division__c;
        f = Lead.fields.Title;
        f = Lead.fields.Email;
        f = Lead.fields.Status1__c;
        f = Lead.fields.Status2__c;
        f = Lead.fields.lead_NGmemo__c;
        f = Lead.fields.Website;
        f = Lead.fields.Phone;
        f = Lead.fields.lead_Relation__c;
        f = Lead.fields.lead_lead1__c;
        f = Lead.fields.lead_lead2__c;
        f = Lead.fields.PostalCode;
        f = Lead.fields.State;
        f = Lead.fields.City;
        f = Lead.fields.Street;
        f = Lead.fields.Description;
        f = Lead.fields.lead_BusinessModel__c;
        f = Lead.fields.lead_BusinessContent__c;

        List<RecordTypeInfo> recordTypes;
        try {
            mainSObjectType = Lead.SObjectType;
            setPageReferenceFactory(new PageReferenceFactory());
            
            mainQuery = new SkyEditor2.Query('Lead');
            mainQuery.addField('Company');
            mainQuery.addField('LastName');
            mainQuery.addField('FirstName');
            mainQuery.addField('lead_kana__c');
            mainQuery.addField('lead_Division__c');
            mainQuery.addField('Title');
            mainQuery.addField('Email');
            mainQuery.addField('Status1__c');
            mainQuery.addField('Status2__c');
            mainQuery.addField('lead_NGmemo__c');
            mainQuery.addField('Website');
            mainQuery.addField('Phone');
            mainQuery.addField('lead_Relation__c');
            mainQuery.addField('lead_lead1__c');
            mainQuery.addField('lead_lead2__c');
            mainQuery.addField('PostalCode');
            mainQuery.addField('State');
            mainQuery.addField('City');
            mainQuery.addField('Street');
            mainQuery.addField('Description');
            mainQuery.addField('lead_BusinessModel__c');
            mainQuery.addField('lead_BusinessContent__c');
            mainQuery.addFieldAsOutput('Name');
            mainQuery.addFieldAsOutput('RecordTypeId');
            mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
                .limitRecords(1);
            
            
            
            mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
            
            queryMap = new Map<String, SkyEditor2.Query>();
            SkyEditor2.Query query;
            
            
            p_showHeader = true;
            p_sidebar = true;
            addInheritParameter('RecordTypeId', 'RecordType');
            init();
            
            if (record.Id == null) {
                
                saveOldValues();
                
            }

            
            
        }  catch (SkyEditor2.Errors.FieldNotFoundException e) {
            fieldNotFound(e);
        } catch (SkyEditor2.Errors.RecordNotFoundException e) {
            recordNotFound(e);
        } catch (SkyEditor2.ExtenderException e) {
            e.setMessagesToPage();
        }
    }
    

    private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    
    private static testMethod void testPageMethods() {        Lead20150209 extension = new Lead20150209(new ApexPages.StandardController(new Lead()));
        SkyEditor2.Messages.clear();
        extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
        SkyEditor2.Messages.clear();
        extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
        SkyEditor2.Messages.clear();
        extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

        Integer defaultSize;
    }
    with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
        public PageReference newPageReference(String url) {
            return new PageReference(url);
        }
    }
}