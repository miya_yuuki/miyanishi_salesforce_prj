global with sharing class LEX_TextManagement2_view extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public TextManagement2__c record {get{return (TextManagement2__c)mainRecord;}}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	{
	setApiVersion(42.0);
	}
	public LEX_TextManagement2_view(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = TextManagement2__c.fields.LinkTextManagement__c;
		f = TextManagement2__c.fields.Name;
		f = TextManagement2__c.fields.OrderItemStatus__c;
		f = TextManagement2__c.fields.Item2_OrderProduct__c;
		f = TextManagement2__c.fields.Item2_Entry_date__c;
		f = TextManagement2__c.fields.Account__c;
		f = TextManagement2__c.fields.StartDate_EndDatec2__c;
		f = TextManagement2__c.fields.sisakukigyou__c;
		f = TextManagement2__c.fields.Item2_ServiceDate__c;
		f = TextManagement2__c.fields.PSkubun__c;
		f = TextManagement2__c.fields.Status__c;
		f = TextManagement2__c.fields.ItemOrderNO__c;
		f = TextManagement2__c.fields.DeliveryTotalCount__c;
		f = TextManagement2__c.fields.NoBill__c;
		f = TextManagement2__c.fields.nouhinn__c;
		f = TextManagement2__c.fields.SalesforceID__c;
		f = TextManagement2__c.fields.Bill_Main__c;
		f = TextManagement2__c.fields.RecordTypeId;
		f = TextManagement2__c.fields.Item2_PaymentDueDate__c;
		f = TextManagement2__c.fields.BillSet__c;
		f = TextManagement2__c.fields.Billchek__c;
		f = TextManagement2__c.fields.recorded_date__c;
		f = TextManagement2__c.fields.ChangeDay__c;
		f = TextManagement2__c.fields.CancelDay__c;
		f = TextManagement2__c.fields.Item2_DeliveryCount__c;
		f = TextManagement2__c.fields.SalesDeliveryCountTotal__c;
		f = TextManagement2__c.fields.DeliveryTotalAmount__c;
		f = TextManagement2__c.fields.DeliveryCountOver1__c;
		f = TextManagement2__c.fields.Product__c;
		f = TextManagement2__c.fields.ChangeDay1__c;
		f = TextManagement2__c.fields.SalesTotalSummary__c;
		f = TextManagement2__c.fields.BillingTiming__c;
		f = TextManagement2__c.fields.PartnerBillAccount__c;
		f = TextManagement2__c.fields.AutomaticUpdate__c;
		f = TextManagement2__c.fields.BillAmountPrint__c;
		f = TextManagement2__c.fields.TextType__c;
		f = TextManagement2__c.fields.Selling_price__c;
		f = TextManagement2__c.fields.Item2_Keyword_letter_count__c;
		f = TextManagement2__c.fields.DeliverySummary__c;
		f = TextManagement2__c.fields.Memo__c;
		f = TextManagement2__c.fields.CloudStatus__c;
		f = TextManagement2__c.fields.DeliveryScheduleDay__c;
		f = TextManagement2__c.fields.CloudBox__c;
		f = TextManagement2__c.fields.SalesDeliveryDay__c;
		f = TextManagement2__c.fields.GroupID__c;
		f = TextManagement2__c.fields.SalesDeliveryCount__c;
		f = TextManagement2__c.fields.DeliveryCount__c;
		f = TextManagement2__c.fields.DeliveryCompleteHopeDate__c;
		f = TextManagement2__c.fields.DeliveryDaySales1__c;
		f = TextManagement2__c.fields.DeliverOnlyCompleted__c;
		f = TextManagement2__c.fields.DeliveryDay__c;
		f = TextManagement2__c.fields.SplitDeliveryHopeDate__c;
		f = TextManagement2__c.fields.ContentsS_TextCount__c;
		f = TextManagement2__c.fields.SalesMemo1__c;
		f = TextManagement2__c.fields.SalesPerson10team__c;
		f = TextManagement2__c.fields.DeliveryPersonUnit10__c;
		f = TextManagement2__c.fields.SalesPerson10__c;
		f = TextManagement2__c.fields.DeliveryPerson__c;
		f = TextManagement2__c.fields.Boss1__c;
		f = TextManagement2__c.fields.SalesPersonUnit__c;
		f = TextManagement2__c.fields.SalesPersonSubUnit__c;
		f = TextManagement2__c.fields.SalesPerson20__c;
		f = TextManagement2__c.fields.SalesPersonSub__c;
		f = TextManagement2__c.fields.SalesPercentage1__c;
		f = TextManagement2__c.fields.SalesPercentage2__c;
		f = TextManagement2__c.fields.CreatedById;
		f = TextManagement2__c.fields.CreatedDate;
		f = TextManagement2__c.fields.LastModifiedById;
		f = TextManagement2__c.fields.LastModifiedDate;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = TextManagement2__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'LEX_TextManagement2_view';
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(TextManagement2__c.SObjectType);
			mainQuery = new SkyEditor2.Query('TextManagement2__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('LinkTextManagement__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('OrderItemStatus__c');
			mainQuery.addFieldAsOutput('Item2_OrderProduct__c');
			mainQuery.addFieldAsOutput('Item2_Entry_date__c');
			mainQuery.addFieldAsOutput('Account__c');
			mainQuery.addFieldAsOutput('StartDate_EndDatec2__c');
			mainQuery.addFieldAsOutput('sisakukigyou__c');
			mainQuery.addFieldAsOutput('Item2_ServiceDate__c');
			mainQuery.addFieldAsOutput('PSkubun__c');
			mainQuery.addFieldAsOutput('Status__c');
			mainQuery.addFieldAsOutput('ItemOrderNO__c');
			mainQuery.addFieldAsOutput('DeliveryTotalCount__c');
			mainQuery.addFieldAsOutput('NoBill__c');
			mainQuery.addFieldAsOutput('nouhinn__c');
			mainQuery.addFieldAsOutput('SalesforceID__c');
			mainQuery.addFieldAsOutput('Bill_Main__c');
			mainQuery.addFieldAsOutput('RecordType.Name');
			mainQuery.addFieldAsOutput('Item2_PaymentDueDate__c');
			mainQuery.addFieldAsOutput('BillSet__c');
			mainQuery.addFieldAsOutput('Billchek__c');
			mainQuery.addFieldAsOutput('recorded_date__c');
			mainQuery.addFieldAsOutput('ChangeDay__c');
			mainQuery.addFieldAsOutput('CancelDay__c');
			mainQuery.addFieldAsOutput('Item2_DeliveryCount__c');
			mainQuery.addFieldAsOutput('SalesDeliveryCountTotal__c');
			mainQuery.addFieldAsOutput('DeliveryTotalAmount__c');
			mainQuery.addFieldAsOutput('DeliveryCountOver1__c');
			mainQuery.addFieldAsOutput('Product__c');
			mainQuery.addFieldAsOutput('ChangeDay1__c');
			mainQuery.addFieldAsOutput('SalesTotalSummary__c');
			mainQuery.addFieldAsOutput('BillingTiming__c');
			mainQuery.addFieldAsOutput('PartnerBillAccount__c');
			mainQuery.addFieldAsOutput('AutomaticUpdate__c');
			mainQuery.addFieldAsOutput('BillAmountPrint__c');
			mainQuery.addFieldAsOutput('TextType__c');
			mainQuery.addFieldAsOutput('Selling_price__c');
			mainQuery.addFieldAsOutput('Item2_Keyword_letter_count__c');
			mainQuery.addFieldAsOutput('DeliverySummary__c');
			mainQuery.addFieldAsOutput('Memo__c');
			mainQuery.addFieldAsOutput('CloudStatus__c');
			mainQuery.addFieldAsOutput('DeliveryScheduleDay__c');
			mainQuery.addFieldAsOutput('CloudBox__c');
			mainQuery.addFieldAsOutput('SalesDeliveryDay__c');
			mainQuery.addFieldAsOutput('GroupID__c');
			mainQuery.addFieldAsOutput('SalesDeliveryCount__c');
			mainQuery.addFieldAsOutput('DeliveryCount__c');
			mainQuery.addFieldAsOutput('DeliveryCompleteHopeDate__c');
			mainQuery.addFieldAsOutput('DeliveryDaySales1__c');
			mainQuery.addFieldAsOutput('DeliverOnlyCompleted__c');
			mainQuery.addFieldAsOutput('DeliveryDay__c');
			mainQuery.addFieldAsOutput('SplitDeliveryHopeDate__c');
			mainQuery.addFieldAsOutput('ContentsS_TextCount__c');
			mainQuery.addFieldAsOutput('SalesMemo1__c');
			mainQuery.addFieldAsOutput('SalesPerson10team__c');
			mainQuery.addFieldAsOutput('DeliveryPersonUnit10__c');
			mainQuery.addFieldAsOutput('SalesPerson10__c');
			mainQuery.addFieldAsOutput('DeliveryPerson__c');
			mainQuery.addFieldAsOutput('Boss1__c');
			mainQuery.addFieldAsOutput('SalesPersonUnit__c');
			mainQuery.addFieldAsOutput('SalesPersonSubUnit__c');
			mainQuery.addFieldAsOutput('SalesPerson20__c');
			mainQuery.addFieldAsOutput('SalesPersonSub__c');
			mainQuery.addFieldAsOutput('SalesPercentage1__c');
			mainQuery.addFieldAsOutput('SalesPercentage2__c');
			mainQuery.addFieldAsOutput('CreatedById');
			mainQuery.addFieldAsOutput('CreatedDate');
			mainQuery.addFieldAsOutput('LastModifiedById');
			mainQuery.addFieldAsOutput('LastModifiedDate');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			if (record.Id == null) {
				saveOldValues();
				if(record.RecordTypeId == null) recordTypeSelector.applyDefault(record);
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}