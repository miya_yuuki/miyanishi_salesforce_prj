public with sharing class UpdateDeliveryRecordClass {
    public static void prcUpdateDeliveryRecord(List<OrderItem__c> objOrderItem, Map<Id, OrderItem__c> oldMap) {
    	// 更新対象の注文商品ID
		List<String> strOrderItemIds = new List<String>();
		// 納品管理のレコード
		List<TextManagement2__c> updDelivaryRecords = new List<TextManagement2__c>();
		// 更新対象項目
		List<String> RTString = new List<String>();

		// 更新対象のチェック
    	for (OrderItem__c obj : objOrderItem) {
			RTString = prcIsUpdatedTarget(obj, oldMap);
    		if (RTString.size() == 0) {
				continue;
			}
			else {
    			strOrderItemIds.add(obj.Id);
    		}
			// 納品管理取得
			updDelivaryRecords = [
				Select
					Id,
					Item2_OrderProduct__r.Id, // 注文商品のID
					CloudStatus__c, // 納品管理ステータス
					ChangeDay__c, // サポート処理日（オープン）
					CancelDay__c // サポート処理日（クローズ）
				FROM TextManagement2__c
				WHERE
					Item2_OrderProduct__r.Id IN :strOrderItemIds
			];
			// 納品管理項目への代入
			for (TextManagement2__c upd: updDelivaryRecords) {
				for (String RTS: RTString) {
					if (RTS == 'ChangeDay') {
						upd.ChangeDay__c = obj.ChangeDay__c;
					}
					if (RTS == 'CancelDay') {
						upd.CancelDay__c = obj.CancelDay__c;
					}
					if (RTS == '契約変更') {
						upd.CloudStatus__c = '契約変更';
					}
					if (RTS == '取消') {
						upd.CloudStatus__c = '取消';
					}
					if (RTS == '売上取消') {
						upd.CloudStatus__c = '売上取消';
					}
				}
			}

			if (updDelivaryRecords.size() > 0) {
				update updDelivaryRecords;
			}
    	}
    }
    
    private static List<String> prcIsUpdatedTarget(OrderItem__c objNew, Map<Id, OrderItem__c> oldMap) {
    	List<String> Target = new List<String>();
		// 古い値を取得
		OrderItem__c objOld = oldMap.get(objNew.Id);

		// サポート処理日（オープン）の値がnullから変化している場合
		if (objNew.ChangeDay__c != null && objOld.ChangeDay__c != objNew.ChangeDay__c) {
			Target.add('ChangeDay');
		}	
		// サポート処理日（クローズ）の値がnullから変化している場合
		if (objNew.CancelDay__c != null && objOld.CancelDay__c != objNew.CancelDay__c) {
   			Target.add('CancelDay');
   		}
		// 注文商品フェーズの値が「契約変更」「取消」「売上取消」に変化された場合
		if (objNew.Item2_Keyword_phase__c != null && objOld.Item2_Keyword_phase__c != objNew.Item2_Keyword_phase__c) {
			if (objNew.Item2_Keyword_phase__c == '契約変更' || objNew.Item2_Keyword_phase__c == '取消' || objNew.Item2_Keyword_phase__c == '売上取消') {
				Target.add(objNew.Item2_Keyword_phase__c);
			}
		}

    	return Target;
    }
}