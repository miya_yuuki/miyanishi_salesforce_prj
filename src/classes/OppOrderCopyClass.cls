/*
*** T.Kumagai
*** 商談から契約管理への自動コピー
*/
public class OppOrderCopyClass {

    public String ITEM_TYPE1 = 'SEO';
    public String ITEM_TYPE2 = 'SearchCleaner';
    public String ITEM_TYPE3 = 'WEBService';
    public String ITEM_TYPE4 = 'SiteProduct';
    public String ITEM_TYPE5 = 'KIJITASU';
    public String ITEM_TYPE6 = 'IntroductionFee';
    public String ITEM_TYPE7 = 'WEBServiceNo';

    public static void prcOppOrderCopy(List<Opportunity> objOpp)
    {
        //-------------------------------------------------------------
        // 商談IDの配列
        List<String> strOppIds = new List<String>();
        // 商談オブジェクト
        List<Opportunity> resOpp = new List<Opportunity>();
        // 契約オブジェクト
        List<Contract> insContract = new List<Contract>();
        // 注文オブジェクト
        List<Order__c> insOrder = new List<Order__c>();
        // 契約のMap＜商談ID, 契約オブジェクト＞
        Map<Id, Contract> mapCon = new Map<Id, Contract>();
        // レコードタイプオブジェクト
        List<RecordType> resRecordType = new List<RecordType>();
        // レコードタイプのMap＜レコードタイプ名, レコードタイプオブジェクト＞
        Map<String, RecordType> mapRec = new Map<String, RecordType>();
        // 注文フェーズ
        String strOrderPhase = '';
        // クラスの呼び出し
        OppOrderCopyClass G = new OppOrderCopyClass();
        // 注文商品オブジェクト
        List<OrderItem__c> insOrderItem = new List<OrderItem__c>();
        //-------------------------------------------------------------

        // 受注承認日がセットされている＆注文作成済みが未チェックのレコードのみ抽出
        for (Opportunity obj : objOpp)
            if (obj.syouninn__c != null && obj.OrderCreate__c == false) strOppIds.add(obj.Id);

        // レコードタイプを取得
        resRecordType = [SELECT Id, DeveloperName, Name FROM RecordType WHERE SobjectType = 'OrderItem__c'];

        // レコードタイプをMapに追加
        for (RecordType rec : resRecordType) {
            mapRec.put(rec.DeveloperName, rec);
        }

        // 商談からレコードを取得
        resOpp = prcGetOpportunity(strOppIds);

        // 商談のループ
        for (Opportunity opp : resOpp) {
            // 注文フェーズをセット
            strOrderPhase = prcChkPhaseAll(opp.Opportunityname__r);

            // 契約を新規作成
            Contract newCon = new Contract(
                /* ID （サポート契約変更のみ発生） */
                Id = opp.OriginOrder__r.Order_Relation__r.Id,
                /* 担当営業 */
                OwnerId = opp.OwnerId,
                /* 受注時担当営業 */
                Contract_FastOrdersSales__c = opp.Opportunity_FastOrdersSales__c,
                /* 担当者（請求先） */
                Contract_Title__c = opp.Opportunity_Title__c,
                /* 取引先 */
                AccountId = opp.AccountId,
                /* 契約形態 */
                ContractType__c = opp.Opportunity_Type__c,
                /* パートナー名 */
                Contract_Patnername__c = opp.Opportunity_Partner__c,
                /* 施策企業名 */
                Contract_EndClient1__c = opp.Opportunity_endClient1__c,
                /* 商談 */
                Opportunity__c = opp.Id,
                /* フェーズ */
                Phase__c = strOrderPhase,
                /* 状況 */
                Status = 'ドラフト',
                /* 顧客カルテ */
                karute__c = opp.karute__c,
                /* 種別 */
                Type__c = opp.Type,
                /* リード（大） */
                Opportunity_lead1__c = opp.Opportunity_lead1__c,
                /* リード（小） */
                Opportunity_lead2__c = opp.Opportunity_lead2__c,
                /* 取引先Webサイト */
                account_web_site__c = opp.account_web_site__c
            );

            // 配列に追加
            insContract.add(newCon);
        }
        // 契約を作成
        if (insContract.size() > 0) upsert insContract;

        // 契約のMapを生成
        for (Contract con : insContract) mapCon.put(con.Opportunity__c, con);

        // 商談のループ
        for (Opportunity opp : resOpp) {
            // Type1を判定
            if (prcChkRecordType(opp.Opportunityname__r, G.ITEM_TYPE1)) {
                String strPhase = prcChkPhaseType(opp.Opportunityname__r, G.ITEM_TYPE1);            // フェーズを取得
                Id orderId = prcInsertOrder(opp, mapCon, strPhase, mapRec.get(G.ITEM_TYPE1).Name);  // 注文を作成
                List<OrderItem__c> result1 = prcInsertOrderItem(opp.Opportunityname__r, G.ITEM_TYPE1, mapRec, orderId); // 注文商品を作成
                for (OrderItem__c obj : result1) insOrderItem.add(obj);
            }
            // Type2を判定
            if (prcChkRecordType(opp.Opportunityname__r, G.ITEM_TYPE2)) {
                String strPhase = prcChkPhaseType(opp.Opportunityname__r, G.ITEM_TYPE2);            // フェーズを取得
                Id orderId = prcInsertOrder(opp, mapCon, strPhase, mapRec.get(G.ITEM_TYPE2).Name);  // 注文を作成
                List<OrderItem__c> result2 = prcInsertOrderItem(opp.Opportunityname__r, G.ITEM_TYPE2, mapRec, orderId); // 注文商品を作成
                for (OrderItem__c obj : result2) insOrderItem.add(obj);
            }
            // Type3を判定
            if (prcChkRecordType(opp.Opportunityname__r, G.ITEM_TYPE3)) {
                String strPhase = prcChkPhaseType(opp.Opportunityname__r, G.ITEM_TYPE3);            // フェーズを取得
                Id orderId = prcInsertOrder(opp, mapCon, strPhase, mapRec.get(G.ITEM_TYPE3).Name);  // 注文を作成
                List<OrderItem__c> result3 = prcInsertOrderItem(opp.Opportunityname__r, G.ITEM_TYPE3, mapRec, orderId); // 注文商品を作成
                for (OrderItem__c obj : result3) insOrderItem.add(obj);
            }
            // Type4を判定
            if (prcChkRecordType(opp.Opportunityname__r, G.ITEM_TYPE4)) {
                String strPhase = prcChkPhaseType(opp.Opportunityname__r, G.ITEM_TYPE4);            // フェーズを取得
                Id orderId = prcInsertOrder(opp, mapCon, strPhase, mapRec.get(G.ITEM_TYPE4).Name);  // 注文を作成
                List<OrderItem__c> result4 = prcInsertOrderItem(opp.Opportunityname__r, G.ITEM_TYPE4, mapRec, orderId); // 注文商品を作成
                for (OrderItem__c obj : result4) insOrderItem.add(obj);
            }
            // Type5を判定
            if (prcChkRecordType(opp.Opportunityname__r, G.ITEM_TYPE5)) {
                // メロンホイップPh.2対応（商談商品1レコードに対し、注文1レコード作成する）
                // テキスト商材の場合のみ、商談商品単位でループを回す
                for (OpportunityProduct__c objItm : opp.Opportunityname__r) {
                    // 商談商品1レコードだけ配列に入れ直す
                    List<OpportunityProduct__c> newItms = new List<OpportunityProduct__c>();
                    if (objItm != null) newItms.add(objItm);

                    // 商談商品ごとに 注文・注文商品作成を実施する
                    String strPhase = prcChkPhaseType(newItms, G.ITEM_TYPE5);            // フェーズを取得
                    Id orderId = prcInsertOrder(opp, mapCon, strPhase, mapRec.get(G.ITEM_TYPE5).Name);  // 注文を作成
                    List<OrderItem__c> result5 = prcInsertOrderItem(newItms, G.ITEM_TYPE5, mapRec, orderId); // 注文商品を作成
                    for (OrderItem__c obj : result5) insOrderItem.add(obj);
                }
            }
            // Type6を判定
            if (prcChkRecordType(opp.Opportunityname__r, G.ITEM_TYPE6)) {
                String strPhase = prcChkPhaseType(opp.Opportunityname__r, G.ITEM_TYPE6);            // フェーズを取得
                Id orderId = prcInsertOrder(opp, mapCon, strPhase, mapRec.get(G.ITEM_TYPE6).Name);  // 注文を作成
                List<OrderItem__c> result6 = prcInsertOrderItem(opp.Opportunityname__r, G.ITEM_TYPE6, mapRec, orderId);         // 注文商品を作成
                for (OrderItem__c obj : result6) insOrderItem.add(obj);
            }
            // Type7を判定
            if (prcChkRecordType(opp.Opportunityname__r, G.ITEM_TYPE7)) {
                String strPhase = prcChkPhaseType(opp.Opportunityname__r, G.ITEM_TYPE7);            // フェーズを取得
                Id orderId = prcInsertOrder(opp, mapCon, strPhase, mapRec.get(G.ITEM_TYPE7).Name);  // 注文を作成
                List<OrderItem__c> result7 = prcInsertOrderItem(opp.Opportunityname__r, G.ITEM_TYPE7, mapRec, orderId); // 注文商品を作成
                for (OrderItem__c obj : result7) insOrderItem.add(obj);
            }

            // 注文商品を作成
            if (insOrderItem.size() > 0) insert insOrderItem;
        }

        System.debug('###### 実行対象商談: ' + resOpp.size() + '件');
        if (resOpp.size() > 0) {
            for (Opportunity oppNew : resOpp) {
                oppNew.OrderCreate__c = true;
            }
            update resOpp;
        }
    }

    // フェーズをチェックする
    private static String prcJudgePhase(Decimal cnt)
    {
        if (cnt == null || cnt == 0) return '契約期間なし';
        return '契約中';
    }

    private static OrderItem__c prcSetOrderItem(OpportunityProduct__c itm, Id recordTypeId, Id orderId, Integer i)
    {
		// 注文商品のインスタンスを生成
        OrderItem__c od = new OrderItem__c(
            /* レコードタイプID */
            RecordTypeId = recordTypeId,
            /* ②注文番号 */
            Item2_Relation__c = orderId,
            /* 商談商品 */
            OppProduct__c = itm.Id,
            /* 契約開始日 */
            ServiceDate__c = itm.Item_Contract_start_date__c,
            /* No. */
            Item2_ProductCount__c = (i + 1),
            /* 請求対象開始日 */
            Item2_ServiceDate__c = UtilityClass.prcAddMonths(itm.Item_Contract_start_date__c, i),
            /* 請求対象終了日 */
            // Item2_EndData__c = UtilityClass.prcAddDays(UtilityClass.prcAddMonths(UtilityClass.prcAddMonths(itm.Item_Contract_start_date__c, i), 1), -1),
            Item2_EndData__c = UtilityClass.prcAddDays(UtilityClass.prcAddMonths(itm.Item_Contract_start_date__c, (i+1)), -1),
            /* フェーズ */
            Item2_Keyword_phase__c = prcJudgePhase(itm.ContractMonths__c),
            /* 代理店手数料 */
            Item2_Commission_rate__c = UtilityClass.StoD(UtilityClass.prcReplace(itm.Item_Commission_rate__c, '%', '')),
            /* 特記事項 */
            Item2_Special_instruction__c = itm.Item_Special_instruction__c,
            /* 強化施策 */
            Item2_Strengthen_measures__c = itm.Item_Strengthen_measures__c,
            /* 施策金額（見積レベル） */
            Item2_Estimate_level__c = itm.Item_Estimate_level__c,
            /* 仕入金額 */
            BuyingupPrice__c = itm.BuyingupPrice__c,
            /* 契約月数 */
            ContractMonths__c = itm.ContractMonths__c,
            /* 請求タイミング */
            BillingTiming__c = itm.BillingTiming__c,
            /* 自動更新有無 */
            AutomaticUpdate__c = itm.AutomaticUpdate__c,
            /* 商品 */
            Item2_Product__c = itm.Product__c,
            /* 商品内容 */
            Item2_Detail_Productlist__c = itm.Item_Detail_Product__c,
            /* 数量 */
            Quantity__c = itm.Quantity__c,
            /* 申込日 */
            Item2_Entry_date__c = itm.Item_Entry_date__c,
            /* キーワード種別 */
            OrderItem_keywordType__c = itm.Item_Classification__c,
            /* キーワード */
            Item2_Keyword_strategy_keyword__c = itm.Item_Keyword_strategy_keyword__c,
            /* 検索キーワード */
            Item2_Search_keyword__c = itm.Item_Search_keyword__c,
            /* 対策キーワード（複数）　or　対策URL */
            Item2_Keyword_multiple_strategy_keyword__c = itm.Item_Keyword_multiple_strategy_keyword__c,
            /* 対策URL */
            OrderItem_URL__c = itm.Item_Strategy_url_del__c,
            /* 単価 */
            Item2_Unit_selling_price__c = itm.Price__c,
            /* 文字数 */
            Item2_Keyword_letter_count__c = itm.Item_keyword_letter_count__c,
            /* 納品予定日 */
            Item2_DeliveryDate__c = UtilityClass.prcAddMonths(itm.Item_DeliveryDate__c, i),
            /* 文字数金額集計 */
            TextCalculation__c = itm.TextCalculation__c,
            /* 区分 */
            Opportunity_Competition__c = itm.Opportunity_Competition__c,
            /* 契約更新回数 */
            Item_Count__c = 0,
            /* 受注承認日 */
            syouninn__c = itm.OrderDay__c,
            /* 担当営業（注文商品） */
            SalesPerson1__c = itm.OwnerId__c,
            /* 案分担当② */
            SalesPersonSub__c = itm.SalesPersonSub__c,
            /* 担当① 案分比率 */
            SalesPercentage1__c = itm.SalesPercentage1__c,
            /* 担当② 案分比率 */
            SalesPercentage2__c = itm.SalesPercentage2__c,
            /* 営業担当②ユニット */
            SalesPersonSubUnit__c = itm.SalesPersonSubUnit__c,
            /* 合計切り捨てなし */
            AmountCheck__c = itm.AmountCheck__c,
            /* Text_standard */
            Text_standard__c = itm.Text_standard__c,
            /* 記事タイプ */
            TextType__c = itm.TextType__c,
            /* 作成種類 */
            RecordCreateType__c = '(1) 新規',
            /* 契約獲得担当① ユニット */
            ContractGetPersonUnit__c = String.valueOf(itm.SalesPersonUnit__c),
            /* 契約獲得担当者① */
            ContractGetPerson1__c = String.valueOf(itm.OpportunityOwner__c),
            /* 獲得担当① 案分 */
            ContractGetPersonPercentage1__c = itm.SalesPercentage1__c,
            /* 契約獲得担当② ユニット */
            ContractGetPersonUnit2__c = String.valueOf(itm.SalesPersonSubUnit__c),
            /* 契約獲得担当者② */
            ContractGetPerson2__c = String.valueOf(itm.SalesPersonSub__r.Name),
            /* 獲得担当② 案分 */
            ContractGetPersonPercentage2__c = itm.SalesPercentage2__c,
            /* 発リンク開始月（ヶ月目） */
            makeLinkStartMonth__c = itm.makeLinkStartMonth__c,
            /* コンサルティング売上相当額 */
            consultingSalesEquivalent__c = itm.consultingSalesEquivalent__c,
            /* TACTアカウントID */
            TACT_Account_ID__c = itm.TACT_Account_ID__c
        );

		// 案分比率の計算（異常値混入の防止）
		if (itm.SalesPercentage1__c == null && itm.SalesPercentage2__c == null){
			od.SalesPercentage1__c = 100;
			od.SalesPercentage1__c = 0;
		}
		else if (itm.SalesPercentage1__c == null && itm.SalesPercentage2__c != null){
			od.SalesPercentage1__c = 100 - od.SalesPercentage2__c;
		}
		else if (itm.SalesPercentage1__c != null && itm.SalesPercentage2__c == null){
			od.SalesPercentage2__c = 100 - od.SalesPercentage1__c;
		}

        // 発リンク開始月より前のレコードは外部リンク施策相当額を0円にする
        if (itm.makeLinkStartMonth__c != null && itm.makeLinkStartMonth__c > (i + 1)) {
        	od.linkPerformingPrice__c = 0;
        }
        else {
        	od.linkPerformingPrice__c = itm.linkPerformingPrice__c;
        }

        return od;
    }

    private static List<OrderItem__c> prcInsertOrderItem(List<OpportunityProduct__c> op, String itemType, Map<String, RecordType> mapRec, Id orderId)
    {
        // 注文商品オブジェクト
        List<OrderItem__c> insOrderItem = new List<OrderItem__c>();

        // 商談商品のループ
        for (OpportunityProduct__c itm : op) {
            // レコードタイプ判定
            if (itm.RecordType.DeveloperName != itemType) continue;

            // 契約期間（月） = 0
            if (itm.ContractMonths__c == 0 || itm.ContractMonths__c == null) {
                // 注文商品オブジェクトに値をセット
                OrderItem__c newOrderItem = prcSetOrderItem(itm, mapRec.get(itemType).Id, orderId, 0);
                // 配列に追加
                insOrderItem.add(newOrderItem);
            }
            // 契約期間分のループ
            for (Integer i = 0; i < itm.ContractMonths__c; i++) {
                // 注文商品オブジェクトに値をセット
                OrderItem__c newOrderItem = prcSetOrderItem(itm, mapRec.get(itemType).Id, orderId, i);
                // 配列に追加
                insOrderItem.add(newOrderItem);
            }
        }
        return insOrderItem;
    }

    // 注文を作成
    private static Id prcInsertOrder(Opportunity opp, Map<Id, Contract> mapCon, String strPhase, String recName)
    {
        // 注文のインスタンスを生成
        Order__c newOrder = new Order__c(
            /* ①契約番号 */
            Order_Relation__c = mapCon.get(opp.Id).Id,
            /* 注文フェーズ */
            Order_Phase__c = strPhase,
            /* 商品区分 */
            ProductType__c = recName,
            /* 請求担当者名（請求先） */
            BillTo_Contact__c = opp.Opportunity_Title__c,
            /* 請求担当者名（請求先） */
            Order_BillingMethod__c = opp.Opportunity_BillingMethod__c,
            /* 口座振替希望 */
            Order_Transfer__c = opp.Opportunity_Transfer__c,
            /* 支払い方法 */
            Order_PaymentMethod__c = opp.Opportunity_PaymentMethod__c,
            /* 支払サイト */
            Order_PaymentSiteMaster__c = opp.Opportunity_PaymentSiteMaster__c,
            /* 手数料負担 */
            Order_Commission__c = opp.Opportunity_Commission__c,
            /* 請求書備考 */
            Order_BillMemo__c = opp.Opportunity_Memo__c,
            /* 担当営業（参照） */
            SalesPerson10__c = mapCon.get(opp.Id).OwnerId,
            /* サポート担当 */
            Opportunity_Support__c = opp.Opportunity_Support__c,
            /* 顧客カルテ */
            karute__c = opp.karute__c,
            /* 商談名 */
            Opportunity_Item__c = opp.Id
        );
        // 注文を作成
        insert newOrder;
        // 戻り値
        return newOrder.Id;
    }

    // レコードタイプをチェックする
    private static Boolean prcChkRecordType(List<OpportunityProduct__c> objItm, String strDevName)
    {
        // レコードタイプ名を判定する
        for (OpportunityProduct__c itm : objItm) {
            if (itm.RecordType.DeveloperName == strDevName) return true;
        }
        return false;
    }

    // フェーズをチェックする（商品タイプ別）
    private static String prcChkPhaseType(List<OpportunityProduct__c> objItm, String itemType)
    {
        // 注文期間がセットされている場合、注文フェーズを「契約中」にする
        for (OpportunityProduct__c itm : objItm) {
            if (itm.RecordType.DeveloperName == itemType && itm.ContractMonths__c > 0) return '契約中';
        }
        return '契約期間なし';
    }

    // フェーズをチェックする（全件）
    private static String prcChkPhaseAll(List<OpportunityProduct__c> objItm)
    {
        // 注文期間がセットされている場合、注文フェーズを「契約中」にする
        for (OpportunityProduct__c itm : objItm) {
            if (itm.ContractMonths__c > 0) return '契約中';
        }
        return '契約期間なし';
    }

    // 商談を取得する
    private static List<Opportunity> prcGetOpportunity(List<String> strOppIds)
    {
        // 商談オブジェクト
        List<Opportunity> resOpp = new List<Opportunity>();

        // 商談からレコードを取得
        resOpp = [
            SELECT
                Id,
                OwnerId,
                AccountId,
                Opportunity_FastOrdersSales__c,
                Opportunity_Title__c,
                Opportunity_Partner__c,
                Opportunity_endClient1__c,
                Opportunity_BillingMethod__c,
                Opportunity_Transfer__c,
                Opportunity_PaymentMethod__c,
                Opportunity_Commission__c,
                Opportunity_PaymentSiteMaster__c,
                Opportunity_Type__c,
                Opportunity_Entry_date__c,
                Opportunity_Memo__c,
                Opportunity_Support__c,
                OriginOrder__c,
                OriginOrder__r.Order_Relation__r.Id,
                karute__c,
                Type,
                Opportunity_lead1__c,
                Opportunity_lead2__c,
                account_web_site__c,
                (
                    select
                        Item_Contract_start_date__c,
                        Item_Contract_end_date__c,
                        Item_Commission_rate__c,
                        Item_Special_instruction__c,
                        Item_Strengthen_measures__c,
                        Item_Estimate_level__c,
                        Item_Entry_date__c,
                        BuyingupPrice__c,
                        ContractMonths__c,
                        BillingTiming__c,
                        AutomaticUpdate__c,
                        Product__c,
                        Item_Detail_Product__c,
                        Item_Production__c,
                        Quantity__c,
                        Item_Classification__c,
                        Item_Keyword_strategy_keyword__c,
                        Item_Search_keyword__c,
                        Item_Keyword_multiple_strategy_keyword__c,
                        Item_Strategy_url_del__c,
                        ProductType__c,
                        Price__c,
                        Item_keyword_letter_count__c,
                        Item_DeliveryDate__c,
                        Item_Contract_update_month__c,
                        TextCalculation__c,
                        Opportunity_Competition__c,
                        OrderDay__c,
                        CloseDate__c,
                        OwnerId__c,
                        SalesPersonSub__c,
                        SalesPercentage1__c,
                        SalesPercentage2__c,
                        Update__c,
                        SalesPersonSubUnit__c,
                        SalesPersonUnit__c,
                        OpportunityOwner__c,
                        SalesPersonSub__r.Name,
                        Item_Detail_ProductView__c,
                        AmountCheck__c,
                        Text_standard__c,
                        TextType__c,
                        RecordType.DeveloperName,
                        linkPerformingPrice__c,
                        makeLinkStartMonth__c,
                        consultingSalesEquivalent__c,
                        TACT_Account_ID__c
                    from Opportunityname__r
                    order by RecordType.DeveloperName asc
                )
            FROM Opportunity
            WHERE Id IN : strOppIds
        ];

        return resOpp;
    }

}