global class ScheduledUpdateKaruteMainContract implements Schedulable {
	global void execute(SchedulableContext sc) {

		System.debug('**** 顧客カルテの主契約更新バッチ実行開始');
		// 昨日に作成された契約を取得
		List<Contract> newContracts = [
			SELECT
				Id,
				Opportunity__r.karute__c
			FROM
				Contract
			WHERE
				CreatedDate = YESTERDAY AND
				Opportunity__r.karute__c != null
        ];

		// key:顧客カルテID, value:契約ID の Map を作成
		Map<Id, Id> karute2Contract = new Map<Id, Id>();
		for (Contract contract : newContracts) {
			karute2Contract.put(contract.Opportunity__r.karute__c, contract.Id);
		}

		List<karute__c> targetKarutes = [
			SELECT
				Id,
				MainContract__c
			FROM
				karute__c
			WHERE
				Id IN : karute2Contract.keySet()
		];

		// 「主契約」を更新
		for (karute__c karute : targetKarutes) {
			karute.MainContract__c = karute2Contract.get(karute.Id);
		}

		if (targetKarutes.size() > 0) update targetKarutes;
		System.debug('**** 顧客カルテの主契約更新バッチ実行完了');
	}
}