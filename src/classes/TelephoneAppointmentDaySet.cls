public with sharing class TelephoneAppointmentDaySet {
	public static boolean firstRun = true;
	
	public static void prcTelAppoLastDayUpdate(List<task__c> objTelAppo) {
		// リードID
		List<String> strLeadIds = new List<String>();
		// リードオブジェクト
		List<Lead> updLead = new List<Lead>();
		// 商談ID
		List<String> strOpportunityIds = new List<String>();
		// 商談オブジェクト
		List<Opportunity> updOpportunity = new List<Opportunity>();
		
		// テレアポのループ
		for (task__c obj: objTelAppo) {
			// テレアポにリードの参照関係があれば処理する
			if (obj.lead__c != null) {
				strLeadIds.add(obj.lead__c);
				updLead = [SELECT Id, lastTereapoDay__c FROM Lead WHERE Id = :strLeadIds];
				for (Lead objNew: updLead) {
					// テレアポの架電日時を代入
					if (obj.PhoneA__c != null) {
						if (obj.PhaseBig__c == '接続済' || obj.PhaseBig__c == '興味あり' || obj.PhaseBig__c == 'アポ完了') {
							objNew.lastConnectionDay__c = obj.PhoneA__c;
						}
						objNew.lastTereapoDay__c = obj.PhoneA__c;
					}
					else {
						if (obj.PhaseBig__c == '接続済' || obj.PhaseBig__c == '興味あり' || obj.PhaseBig__c == 'アポ完了') {
							objNew.lastConnectionDay__c = obj.CreatedDate;
						}
						objNew.lastTereapoDay__c = obj.CreatedDate;
					}
					// テレアポの通話メモを代入
					if (obj.Memo__c != null) {
						if (obj.PhaseBig__c == '接続済' || obj.PhaseBig__c == '興味あり' || obj.PhaseBig__c == 'アポ完了') {
							objNew.lastConnectionMemo__c = obj.Memo__c;
						}
						objNew.lastTereapoMemo__c = obj.Memo__c;
					}					
				}
				if (updLead.size() > 0) {
					update updLead;
				}
			}
			// テレアポに商談の参照関係があれば処理する
			if (obj.opportunity__c != null) {
				strOpportunityIds.add(obj.opportunity__c);
				updOpportunity = [SELECT Id, lastTereapoDay__c FROM Opportunity WHERE Id = :strOpportunityIds];
				for (Opportunity objNew: updOpportunity) {
					// テレアポの架電日時を代入
					if (obj.PhoneA__c != null) {
						if (obj.PhaseBig__c == '接続済' || obj.PhaseBig__c == '興味あり' || obj.PhaseBig__c == 'アポ完了') {
							objNew.lastConnectionDay__c = obj.PhoneA__c;
						}
						objNew.lastTereapoDay__c = obj.PhoneA__c;
					}
					else {
						if (obj.PhaseBig__c == '接続済' || obj.PhaseBig__c == '興味あり' || obj.PhaseBig__c == 'アポ完了') {
							objNew.lastConnectionDay__c = obj.CreatedDate;
						}
						objNew.lastTereapoDay__c = obj.CreatedDate;
					}
					// テレアポの通話メモを代入
					if (obj.Memo__c != null) {
						if (obj.PhaseBig__c == '接続済' || obj.PhaseBig__c == '興味あり' || obj.PhaseBig__c == 'アポ完了') {
							objNew.lastConnectionMemo__c = obj.Memo__c;
						}
						objNew.lastTereapoMemo__c = obj.Memo__c;
					}
				}
				if (updOpportunity.size() > 0) {
					update updOpportunity;
				}
			}
		}
	}
}