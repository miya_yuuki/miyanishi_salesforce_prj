@isTest
private with sharing class OrderItemSearchPSTest{
		private static testMethod void testPageMethods() {	
			OrderItemSearchPS page = new OrderItemSearchPS(new ApexPages.StandardController(new OrderItem__c()));	
			page.getOperatorOptions_OrderItem_c_SalesPerson1_c();	
			page.getOperatorOptions_OrderItem_c_DeliveryPerson_c();	
			page.getOperatorOptions_OrderItem_c_Item2_Keyword_phase_c_multi();	
			page.getOperatorOptions_OrderItem_c_Item2_Relation_c();	
			page.getOperatorOptions_OrderItem_c_Item2_Product_naiyou_c();	
			page.getOperatorOptions_OrderItem_c_Account_c();	
			page.getOperatorOptions_OrderItem_c_Item2_Keyword_strategy_keyword_c();	
			page.getOperatorOptions_OrderItem_c_Opportunity_Competition_c();	
			page.getOperatorOptions_OrderItem_c_OrderItem_URL_c();	
			page.getOperatorOptions_OrderItem_c_sisakukigyou_c();	
			page.getOperatorOptions_OrderItem_c_ServiceDate_c();	
			page.getOperatorOptions_OrderItem_c_Item2_ServiceDate_c();	
			page.getOperatorOptions_OrderItem_c_EndDate_c();	
			page.getOperatorOptions_OrderItem_c_Item2_EndData_c();	
			page.getOperatorOptions_OrderItem_c_Bill_Main_c();	
			page.getOperatorOptions_OrderItem_c_Item2_CancellationDay_c();	
			System.assert(true);
		}	
			
	private static testMethod void testComponent2() {
		OrderItemSearchPS.Component2 Component2 = new OrderItemSearchPS.Component2(new List<OrderItem__c>(), new List<OrderItemSearchPS.Component2Item>(), new List<OrderItem__c>(), null);
		Component2.create(new OrderItem__c());
		Component2.doDeleteSelectedItems();
		System.assert(true);
	}
	
}