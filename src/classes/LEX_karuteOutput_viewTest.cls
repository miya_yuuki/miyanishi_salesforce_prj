@isTest
private with sharing class LEX_karuteOutput_viewTest{
	private static testMethod void testPageMethods() {		LEX_karuteOutput_view extension = new LEX_karuteOutput_view(new ApexPages.StandardController(new karute__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testComponent3891() {
		LEX_karuteOutput_view.Component3891 Component3891 = new LEX_karuteOutput_view.Component3891(new List<AEGIS__c>(), new List<LEX_karuteOutput_view.Component3891Item>(), new List<AEGIS__c>(), null);
		Component3891.create(new AEGIS__c());
		System.assert(true);
	}
	
	private static testMethod void testComponent3317() {
		LEX_karuteOutput_view.Component3317 Component3317 = new LEX_karuteOutput_view.Component3317(new List<CLOVER__c>(), new List<LEX_karuteOutput_view.Component3317Item>(), new List<CLOVER__c>(), null);
		Component3317.create(new CLOVER__c());
		System.assert(true);
	}
	
	@isTest
	private static void testLightDataTables(){

		System.assert(true);
	}
}