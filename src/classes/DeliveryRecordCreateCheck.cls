global class DeliveryRecordCreateCheck {
	global static PageReference TargetOrderItemUpdate() {
		Datetime dt = Datetime.newInstance(2015, 4, 1, 00, 00, 00);
		
		// 納品管理を持たない契約中注文商品を抽出する
		List<OrderItem__c> updOrderItem = [
			SELECT Id
				FROM OrderItem__c
				WHERE DeliveryRecordCount__c = 0
				AND CreatedDate >= :dt
				AND (Item2_Keyword_phase__c = '契約中' OR Item2_Keyword_phase__c = '契約期間なし')
				limit 500
		];
		
		if (updOrderItem.size() > 0) {
			update updOrderItem;
			System.debug('更新対象' + updOrderItem.size() + '件');
		}
		else {
			System.debug('更新対象なし');
		}
		
		return null;
	}
}