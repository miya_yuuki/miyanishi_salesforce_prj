global with sharing class RDsearch extends SkyEditor2.SkyEditorPageBaseWithSharing {	
	    	
	    public OrderItem__c record{get;set;}	
	    	
    
	    public Component2 Component2 {get; private set;}	
	    	
	    public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
	    	
	    public SkyEditor2__SkyEditorDummy__c Component193_from{get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component193_to{get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component193_isNull{get;set;}	
	    public SkyEditor2.TextHolder.OperatorHolder Component193_isNull_op{get;set;}	
	    	
	    public SkyEditor2__SkyEditorDummy__c Component4_from{get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component4_to{get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component4_isNull{get;set;}	
	    public SkyEditor2.TextHolder.OperatorHolder Component4_isNull_op{get;set;}	
	    	
	    public SkyEditor2__SkyEditorDummy__c Component6_from{get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component6_to{get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component6_isNull{get;set;}	
	    public SkyEditor2.TextHolder.OperatorHolder Component6_isNull_op{get;set;}	
	    	
	    public SkyEditor2__SkyEditorDummy__c Component14_from{get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component14_to{get;set;}	
	    	
	    public OrderItem__c Component19_val {get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component19_val_dummy {get;set;}	
        public SkyEditor2.TextHolder Component19_op{get;set;}	
        public List<SelectOption> valueOptions_OrderItem_c_Item2_Keyword_phase_c_multi {get;set;}
	    	
	    public OrderItem__c Component207_val {get;set;}	
        public SkyEditor2.TextHolder Component207_op{get;set;}	
        public List<SelectOption> valueOptions_OrderItem_c_R_D_c {get;set;}
	    	
	    public OrderItem__c Component69_val {get;set;}	
        public SkyEditor2.TextHolder Component69_op{get;set;}	
	    	
	    public SkyEditor2__SkyEditorDummy__c Component73_val {get;set;}	
        public SkyEditor2.TextHolder Component73_op{get;set;}	
	    	
	    public SkyEditor2__SkyEditorDummy__c Component116_val {get;set;}	
        public SkyEditor2.TextHolder Component116_op{get;set;}	
	    	
	    public OrderItem__c Component118_val {get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component118_val_dummy {get;set;}	
        public SkyEditor2.TextHolder Component118_op{get;set;}	
        public List<SelectOption> valueOptions_OrderItem_c_OrderItem_keywordType_c_multi {get;set;}
	    	
	    public SkyEditor2__SkyEditorDummy__c Component147_val {get;set;}	
        public SkyEditor2.TextHolder Component147_op{get;set;}	
	    	
    public String recordTypeRecordsJSON_OrderItem_c {get; private set;}
    public String defaultRecordTypeId_OrderItem_c {get; private set;}
    public String metadataJSON_OrderItem_c {get; private set;}
    public String picklistValuesJSON_OrderItem_c_Item2_Consulting_method_c {get; private set;}
    public String picklistValuesJSON_OrderItem_c_Item2_Automatic_updating_c {get; private set;}
    public String picklistValuesJSON_OrderItem_c_OrderItem_keywordType_c {get; private set;}
    public String picklistValuesJSON_OrderItem_c_Item2_Keyword_phase_c {get; private set;}
    public String picklistValuesJSON_OrderItem_c_Item2_Estimate_level_c {get; private set;}
    public String picklistValuesJSON_OrderItem_c_ProductionStatus_c {get; private set;}
    public String picklistValuesJSON_OrderItem_c_ProductionPM_c {get; private set;}
    public String picklistValuesJSON_OrderItem_c_ProductionGenre_c {get; private set;}
    public String picklistValuesJSON_OrderItem_c_Item_kijyun2_c {get; private set;}
    public String picklistValuesJSON_OrderItem_c_CMS_c {get; private set;}
    public String picklistValuesJSON_OrderItem_c_Type_c {get; private set;}
    public String picklistValuesJSON_OrderItem_c_Text_Type_c {get; private set;}
    public String picklistValuesJSON_OrderItem_c_Text_Member_c {get; private set;}
    public String picklistValuesJSON_OrderItem_c_Text_Accep_TedNorm_c {get; private set;}
    public String picklistValuesJSON_OrderItem_c_R_D_c {get; private set;}
    public String picklistValuesJSON_OrderItem_c_SEO_sisaku_c_c {get; private set;}
	    public RDsearch(ApexPages.StandardController controller) {	
	        super(controller);	

            SObjectField f;

            f = OrderItem__c.fields.Item2_Keyword_phase__c;
            f = OrderItem__c.fields.R_D__c;
            f = OrderItem__c.fields.Item2_Relation__c;
            f = OrderItem__c.fields.Account__c;
            f = OrderItem__c.fields.Item2_Keyword_strategy_keyword__c;
            f = OrderItem__c.fields.OrderItem_keywordType__c;
            f = OrderItem__c.fields.RecordTypeId;
            f = OrderItem__c.fields.Name;
            f = OrderItem__c.fields.sisakukigyou__c;
            f = OrderItem__c.fields.Item2_Product__c;
            f = OrderItem__c.fields.Item2_Keyword_Type__c;
            f = OrderItem__c.fields.Item2_Estimate_level__c;
            f = OrderItem__c.fields.Item2_Strengthen_measures__c;
            f = OrderItem__c.fields.TotalPrice__c;
            f = OrderItem__c.fields.Item2_Entry_date__c;
            f = OrderItem__c.fields.StartDate_EndDatec__c;
            f = OrderItem__c.fields.Item2_Contract_update_month1__c;
            f = OrderItem__c.fields.ServiceDate__c;
            f = OrderItem__c.fields.EndDate__c;
            f = OrderItem__c.fields.Item2_ProductCount__c;

        List<RecordTypeInfo> recordTypes;
	        try {	
	            	
	            mainRecord = null;	
	            mainSObjectType = OrderItem__c.SObjectType;	
	            	
	            	
	            mode = SkyEditor2.LayoutMode.TempSearch_01; 
	            	
	            Component193_from = new SkyEditor2__SkyEditorDummy__c();	
	            Component193_to = new SkyEditor2__SkyEditorDummy__c();	
	            Component193_isNull = new SkyEditor2__SkyEditorDummy__c();	
	            Component193_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
	            	
	            Component4_from = new SkyEditor2__SkyEditorDummy__c();	
	            Component4_to = new SkyEditor2__SkyEditorDummy__c();	
	            Component4_isNull = new SkyEditor2__SkyEditorDummy__c();	
	            Component4_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
	            	
	            Component6_from = new SkyEditor2__SkyEditorDummy__c();	
	            Component6_to = new SkyEditor2__SkyEditorDummy__c();	
	            Component6_isNull = new SkyEditor2__SkyEditorDummy__c();	
	            Component6_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
	            	
	            Component14_from = new SkyEditor2__SkyEditorDummy__c();	
	            Component14_to = new SkyEditor2__SkyEditorDummy__c();	
	            	
	            OrderItem__c lookupObjComponent27 = new OrderItem__c();	
	            Component19_val = new OrderItem__c();	
	            Component19_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
	            Component19_op = new SkyEditor2.TextHolder();	
	            valueOptions_OrderItem_c_Item2_Keyword_phase_c_multi = new List<SelectOption>{
	                new SelectOption('', Label.none)
	            };
	            for (PicklistEntry e : OrderItem__c.Item2_Keyword_phase__c.getDescribe().getPicklistValues()) {
	                if (e.isActive()) {
	                    valueOptions_OrderItem_c_Item2_Keyword_phase_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
	                }
	            }
	            	
	            Component207_val = new OrderItem__c();	
	            Component207_op = new SkyEditor2.TextHolder();	
	            valueOptions_OrderItem_c_R_D_c = new List<SelectOption>{
	                new SelectOption('', Label.none)
	            };
	            for (PicklistEntry e : OrderItem__c.R_D__c.getDescribe().getPicklistValues()) {
	                if (e.isActive()) {
	                    valueOptions_OrderItem_c_R_D_c.add(new SelectOption(e.getValue(), e.getLabel()));
	                }
	            }
	            	
	            Component69_val = lookupObjComponent27;	
	            Component69_op = new SkyEditor2.TextHolder();	
	            	
	            Component73_val = new SkyEditor2__SkyEditorDummy__c();	
	            Component73_op = new SkyEditor2.TextHolder();	
	            	
	            Component116_val = new SkyEditor2__SkyEditorDummy__c();	
	            Component116_op = new SkyEditor2.TextHolder();	
	            	
	            Component118_val = new OrderItem__c();	
	            Component118_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
	            Component118_op = new SkyEditor2.TextHolder();	
	            valueOptions_OrderItem_c_OrderItem_keywordType_c_multi = new List<SelectOption>{
	                new SelectOption('', Label.none)
	            };
	            for (PicklistEntry e : OrderItem__c.OrderItem_keywordType__c.getDescribe().getPicklistValues()) {
	                if (e.isActive()) {
	                    valueOptions_OrderItem_c_OrderItem_keywordType_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
	                }
	            }
	            	
	            Component147_val = new SkyEditor2__SkyEditorDummy__c();	
	            Component147_op = new SkyEditor2.TextHolder();	
	            	
	            queryMap.put(	
	                'Component2',	
	                new SkyEditor2.Query('OrderItem__c')	
	                    .addField('R_D__c')
	                    .addFieldAsOutput('Item2_Relation__c')
	                    .addFieldAsOutput('Name')
	                    .addFieldAsOutput('Account__c')
	                    .addFieldAsOutput('sisakukigyou__c')
	                    .addFieldAsOutput('Item2_Product__c')
	                    .addFieldAsOutput('Item2_Keyword_Type__c')
	                    .addFieldAsOutput('Item2_Estimate_level__c')
	                    .addFieldAsOutput('Item2_Strengthen_measures__c')
	                    .addFieldAsOutput('TotalPrice__c')
	                    .addFieldAsOutput('Item2_Entry_date__c')
	                    .addFieldAsOutput('StartDate_EndDatec__c')
	                    .addFieldAsOutput('Item2_Contract_update_month1__c')
	                    .addFieldAsOutput('Item2_Keyword_phase__c')
	                    .addFieldAsOutput('RecordType.Name')
                        .addFieldAsOutput('RecordTypeId')
	                    .limitRecords(500)	
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component193_from, 'SkyEditor2__Date__c', 'Item2_Entry_date__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component193_to, 'SkyEditor2__Date__c', 'Item2_Entry_date__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component193_isNull, 'SkyEditor2__Date__c', 'Item2_Entry_date__c', Component193_isNull_op, true,0,false )) 
	                    
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component4_from, 'SkyEditor2__Date__c', 'ServiceDate__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component4_to, 'SkyEditor2__Date__c', 'ServiceDate__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component4_isNull, 'SkyEditor2__Date__c', 'ServiceDate__c', Component4_isNull_op, true,0,false )) 
	                    
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component6_from, 'SkyEditor2__Date__c', 'EndDate__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component6_to, 'SkyEditor2__Date__c', 'EndDate__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component6_isNull, 'SkyEditor2__Date__c', 'EndDate__c', Component6_isNull_op, true,0,false )) 
	                    
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component14_from, 'SkyEditor2__Text__c', 'Item2_ProductCount__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component14_to, 'SkyEditor2__Text__c', 'Item2_ProductCount__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component19_val_dummy, 'SkyEditor2__Text__c','Item2_Keyword_phase__c', Component19_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component207_val, 'R_D__c', 'R_D__c', Component207_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component69_val, 'Item2_Relation__c', 'Item2_Relation__c', Component69_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component73_val, 'SkyEditor2__Text__c', 'Account__c', Component73_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component116_val, 'SkyEditor2__Text__c', 'Item2_Keyword_strategy_keyword__c', Component116_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component118_val_dummy, 'SkyEditor2__Text__c','OrderItem_keywordType__c', Component118_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component147_val, 'SkyEditor2__Text__c', 'RecordTypeId', Component147_op, true, 0, false ))
	            );	
	            	
	                Component2 = new Component2(new List<OrderItem__c>(), new List<Component2Item>(), new List<OrderItem__c>(), null);
                 Component2.setPageItems(new List<Component2Item>());
                 Component2.setPageSize(100);
	            listItemHolders.put('Component2', Component2);	
	            	
	            	
	            recordTypeSelector = new SkyEditor2.RecordTypeSelector(OrderItem__c.SObjectType, true);
	            	
	            	
            p_showHeader = true;
            p_sidebar = true;
            presetSystemParams();
            Component2.extender = this.extender;
	        } catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
	            SkyEditor2.Messages.addErrorMessage(e.getMessage());
	        } catch (SkyEditor2.Errors.FieldNotFoundException e) {	
	            SkyEditor2.Messages.addErrorMessage(e.getMessage());
            } catch (SkyEditor2.ExtenderException e) {                 e.setMessagesToPage();
	        } catch (Exception e) {	
	            System.Debug(LoggingLevel.Error, e);	
	            SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
	        }	
	    }	
	    	
        public List<SelectOption> getOperatorOptions_OrderItem_c_Item2_Keyword_phase_c_multi() { 
            return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	    }	
        public List<SelectOption> getOperatorOptions_OrderItem_c_R_D_c() { 
            return getOperatorOptions('OrderItem__c', 'R_D__c');	
	    }	
        public List<SelectOption> getOperatorOptions_OrderItem_c_Item2_Relation_c() { 
            return getOperatorOptions('OrderItem__c', 'Item2_Relation__c');	
	    }	
        public List<SelectOption> getOperatorOptions_OrderItem_c_Account_c() { 
            return getOperatorOptions('OrderItem__c', 'Account__c');	
	    }	
        public List<SelectOption> getOperatorOptions_OrderItem_c_Item2_Keyword_strategy_keyword_c() { 
            return getOperatorOptions('OrderItem__c', 'Item2_Keyword_strategy_keyword__c');	
	    }	
        public List<SelectOption> getOperatorOptions_OrderItem_c_OrderItem_keywordType_c_multi() { 
            return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	    }	
        public List<SelectOption> getOperatorOptions_OrderItem_c_RecordTypeId() { 
            return getOperatorOptions('OrderItem__c', 'RecordTypeId');	
	    }	
	    	
	    private static testMethod void testPageMethods() {	
	        RDsearch page = new RDsearch(new ApexPages.StandardController(new OrderItem__c()));	
	        page.getOperatorOptions_OrderItem_c_Item2_Keyword_phase_c_multi();	
	        page.getOperatorOptions_OrderItem_c_R_D_c();	
	        page.getOperatorOptions_OrderItem_c_Item2_Relation_c();	
	        page.getOperatorOptions_OrderItem_c_Account_c();	
	        page.getOperatorOptions_OrderItem_c_Item2_Keyword_strategy_keyword_c();	
	        page.getOperatorOptions_OrderItem_c_OrderItem_keywordType_c_multi();	
	        page.getOperatorOptions_OrderItem_c_RecordTypeId();	
            System.assert(true);
	    }	
	    	
	    	
    global with sharing class Component2Item extends SkyEditor2.ListItem {
        public OrderItem__c record{get; private set;}
        Component2Item(Component2 holder, OrderItem__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(holder);
            if (record.Id == null  && record.RecordTypeId == null){
                if (recordTypeSelector != null) {
                    recordTypeSelector.applyDefault(record);
                }
                
            }
            this.record = record;
        }
        global override SObject getRecord() {return record;}
        public void doDeleteItem(){deleteItem();}
    }
    global with sharing  class Component2 extends SkyEditor2.PagingList {
        public List<Component2Item> items{get; private set;}
        Component2(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(records, items, deleteRecords, recordTypeSelector);
            this.items = (List<Component2Item>)items;
        }
        global override SkyEditor2.ListItem create(SObject data) {
            return new Component2Item(this, (OrderItem__c)data, recordTypeSelector);
        }
        public void doDeleteSelectedItems(){deleteSelectedItems();}
        public void doFirst(){first();}
        public void doPrevious(){previous();}
        public void doNext(){next();}
        public void doLast(){last();}
        public void doSort(){sort();}

        public List<Component2Item> getViewItems() {            return (List<Component2Item>) getPageItems();        }
    }
    private static testMethod void testComponent2() {
        Component2 Component2 = new Component2(new List<OrderItem__c>(), new List<Component2Item>(), new List<OrderItem__c>(), new SkyEditor2.RecordTypeSelector(OrderItem__c.SObjectType));
        Component2.setPageItems(new List<Component2Item>());
        Component2.create(new OrderItem__c());
        Component2.doDeleteSelectedItems();
        Component2.setPagesize(10);        Component2.doFirst();
        Component2.doPrevious();
        Component2.doNext();
        Component2.doLast();
        Component2.doSort();
        System.assert(true);
    }
    

    public OrderItem__c Component2_table_Conversion { get { return new OrderItem__c();}}
    
    public String Component2_table_selectval { get; set; }
    
    
	    	
	}