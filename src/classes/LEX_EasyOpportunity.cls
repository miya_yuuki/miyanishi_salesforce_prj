global with sharing class LEX_EasyOpportunity extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public Opportunity record {get{return (Opportunity)mainRecord;}}
	{
	setApiVersion(42.0);
	}
	public LEX_EasyOpportunity(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = Opportunity.fields.Name;
		f = Opportunity.fields.OpportunityCopy__c;
		f = Opportunity.fields.AccountId;
		f = Opportunity.fields.Opportunity_Type__c;
		f = Opportunity.fields.BoardDiscussed__c;
		f = Opportunity.fields.Opportunity_Partner__c;
		f = Opportunity.fields.Opportunity_endClient1__c;
		f = Opportunity.fields.Opportunity_Competition__c;
		f = Opportunity.fields.OwnerId;
		f = Opportunity.fields.Opportunity_Support__c;
		f = Opportunity.fields.Opportunity_FastOrdersSales__c;
		f = Opportunity.fields.account_web_site__c;
		f = Opportunity.fields.account_web_site_unnecessary__c;
		f = Opportunity.fields.ServiceWEBSite__c;
		f = Opportunity.fields.ServiceWEBSite2__c;
		f = Opportunity.fields.AmountTotal__c;
		f = Opportunity.fields.CreditDecisionPrice__c;
		f = Opportunity.fields.CloseDate;
		f = Opportunity.fields.StageName;
		f = Opportunity.fields.karute__c;
		f = Opportunity.fields.karuteCheck__c;
		f = Opportunity.fields.karutenew__c;
		f = Opportunity.fields.AppointmentDay__c;
		f = Opportunity.fields.Opportunity_lead1__c;
		f = Opportunity.fields.Opportunity_lead2__c;
		f = Opportunity.fields.CampaignId;
		f = Opportunity.fields.not_campaign_flg__c;
		f = Opportunity.fields.Opportunity_BillingMethod__c;
		f = Opportunity.fields.CaseApproval__c;
		f = Opportunity.fields.Opportunity_Title__c;
		f = Opportunity.fields.Opportunity_PaymentSiteMaster__c;
		f = Opportunity.fields.Opportunity_BillKana__c;
		f = Opportunity.fields.Opportunity_PaymentMethod__c;
		f = Opportunity.fields.AddressCheck__c;
		f = Opportunity.fields.Opportunity_Memo__c;
		f = Opportunity.fields.CustomerNamePRINT__c;
		f = Opportunity.fields.QuoteExpirationDate__c;
		f = Opportunity.fields.Opportunity_EstimateMemo__c;
		f = Opportunity.fields.OrderProvisional__c;
		f = Opportunity.fields.OrderProvisionalMemo__c;
		f = Opportunity.fields.SalvageScheduledDate__c;
		f = Opportunity.fields.opportunityBillMemo__c;
		f = Opportunity.fields.notices__c;
		f = Opportunity.fields.Agreement1__c;
		f = Opportunity.fields.Agreement2__c;
		f = Opportunity.fields.Agreement3__c;
		f = Opportunity.fields.Agreement4__c;
		f = Opportunity.fields.Agreement5__c;
		f = Opportunity.fields.Agreement6__c;
		f = Opportunity.fields.Agreement7__c;
		f = Opportunity.fields.Agreement8__c;
		f = Opportunity.fields.Agreement10__c;
		f = Opportunity.fields.Agreement11__c;
		f = Opportunity.fields.Agreement12__c;
		f = Opportunity.fields.Agreement17__c;
		f = Opportunity.fields.Agreement18__c;
		f = Opportunity.fields.Agreement19__c;
		f = Opportunity.fields.Agreement20__c;
		f = Opportunity.fields.Agreement13__c;
		f = Opportunity.fields.Agreement14__c;
		f = Opportunity.fields.Agreement15__c;
		f = Opportunity.fields.Agreement9__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = Opportunity.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'LEX_EasyOpportunity';
			mainQuery = new SkyEditor2.Query('Opportunity');
			mainQuery.addField('Name');
			mainQuery.addField('OpportunityCopy__c');
			mainQuery.addField('AccountId');
			mainQuery.addField('Opportunity_Type__c');
			mainQuery.addField('BoardDiscussed__c');
			mainQuery.addField('Opportunity_Partner__c');
			mainQuery.addField('Opportunity_endClient1__c');
			mainQuery.addField('Opportunity_Competition__c');
			mainQuery.addField('OwnerId');
			mainQuery.addField('Opportunity_Support__c');
			mainQuery.addField('account_web_site__c');
			mainQuery.addField('account_web_site_unnecessary__c');
			mainQuery.addField('ServiceWEBSite__c');
			mainQuery.addField('ServiceWEBSite2__c');
			mainQuery.addField('CloseDate');
			mainQuery.addField('StageName');
			mainQuery.addField('karute__c');
			mainQuery.addField('karuteCheck__c');
			mainQuery.addField('AppointmentDay__c');
			mainQuery.addField('Opportunity_lead1__c');
			mainQuery.addField('Opportunity_lead2__c');
			mainQuery.addField('CampaignId');
			mainQuery.addField('not_campaign_flg__c');
			mainQuery.addField('Opportunity_BillingMethod__c');
			mainQuery.addField('CaseApproval__c');
			mainQuery.addField('Opportunity_Title__c');
			mainQuery.addField('Opportunity_PaymentSiteMaster__c');
			mainQuery.addField('Opportunity_BillKana__c');
			mainQuery.addField('Opportunity_PaymentMethod__c');
			mainQuery.addField('AddressCheck__c');
			mainQuery.addField('Opportunity_Memo__c');
			mainQuery.addField('CustomerNamePRINT__c');
			mainQuery.addField('QuoteExpirationDate__c');
			mainQuery.addField('Opportunity_EstimateMemo__c');
			mainQuery.addField('OrderProvisional__c');
			mainQuery.addField('OrderProvisionalMemo__c');
			mainQuery.addField('SalvageScheduledDate__c');
			mainQuery.addField('opportunityBillMemo__c');
			mainQuery.addField('notices__c');
			mainQuery.addField('Agreement1__c');
			mainQuery.addField('Agreement2__c');
			mainQuery.addField('Agreement3__c');
			mainQuery.addField('Agreement4__c');
			mainQuery.addField('Agreement5__c');
			mainQuery.addField('Agreement6__c');
			mainQuery.addField('Agreement7__c');
			mainQuery.addField('Agreement8__c');
			mainQuery.addField('Agreement10__c');
			mainQuery.addField('Agreement11__c');
			mainQuery.addField('Agreement12__c');
			mainQuery.addField('Agreement17__c');
			mainQuery.addField('Agreement18__c');
			mainQuery.addField('Agreement19__c');
			mainQuery.addField('Agreement20__c');
			mainQuery.addField('Agreement13__c');
			mainQuery.addField('Agreement14__c');
			mainQuery.addField('Agreement15__c');
			mainQuery.addField('Agreement9__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Opportunity_FastOrdersSales__c');
			mainQuery.addFieldAsOutput('AmountTotal__c');
			mainQuery.addFieldAsOutput('CreditDecisionPrice__c');
			mainQuery.addFieldAsOutput('karutenew__c');
			mainQuery.addFieldAsOutput('LeadSource');
			mainQuery.addFieldAsOutput('Id');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('OpportunityCopy__c', 'OpportunityCopy__c');
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			if (record.Id == null) {
				saveOldValues();
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}