@isTest
private with sharing class OpportunityforPSTest{
		private static testMethod void testPageMethods() {	
			OpportunityforPS page = new OpportunityforPS(new ApexPages.StandardController(new OpportunityProduct__c()));	
			page.getOperatorOptions_OpportunityProduct_c_Phase_c_multi();	
			page.getOperatorOptions_OpportunityProduct_c_Opportunity_Phase_c();	
			page.getOperatorOptions_OpportunityProduct_c_SalesPerson_c();	
			page.getOperatorOptions_OpportunityProduct_c_Opportunity_c();	
			page.getOperatorOptions_OpportunityProduct_c_SalesPersonMainUnit_c();	
			page.getOperatorOptions_OpportunityProduct_c_Name();	
			page.getOperatorOptions_OpportunityProduct_c_AccountName_c();	
			page.getOperatorOptions_OpportunityProduct_c_sisakuompany_c();	
			page.getOperatorOptions_OpportunityProduct_c_Opportunity_Competition_c();	
			page.getOperatorOptions_OpportunityProduct_c_ProductName_Detail_c();	
			page.getOperatorOptions_OpportunityProduct_c_AmountCountMonth_c();	
			page.getOperatorOptions_OpportunityProduct_c_CloseMonth_c();	
			page.getOperatorOptions_OpportunityProduct_c_AmountCountMonth2_c();	
			page.getOperatorOptions_OpportunityProduct_c_StartMonth_c();	
			System.assert(true);
		}	
			
	private static testMethod void testComponent3() {
		OpportunityforPS.Component3 Component3 = new OpportunityforPS.Component3(new List<OpportunityProduct__c>(), new List<OpportunityforPS.Component3Item>(), new List<OpportunityProduct__c>(), null);
		Component3.create(new OpportunityProduct__c());
		Component3.doDeleteSelectedItems();
		System.assert(true);
	}
	
}