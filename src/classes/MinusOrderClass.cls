/*
*** T.Kumagai
*** 注文商品のマイナス注文機能
*/
public with sharing class MinusOrderClass {

    // スタンダードコントローラ
    private ApexPages.StandardSetController controller;
    
    public List<OrderItem__c> ODER_ITEM { get; set; }   // 注文商品オブジェクト
    
    public MinusOrderClass(ApexPages.StandardSetController controller)
    {
        this.controller = controller;
        this.ODER_ITEM = (List<OrderItem__c>)controller.getSelected();
        
        // 注文商品ID
        List<String> strOderItemIds = new List<String>();
        
        if (ODER_ITEM.size() > 0){
            for (OrderItem__c obj : ODER_ITEM) strOderItemIds.add(obj.Id);
            System.debug('ODER_ITEMからIdを抽出します ' + strOderItemIds);
            // 注文商品を取得
            ODER_ITEM = prcGetOrderItem(strOderItemIds);
        }
        else{
            // OrderSearchGamen からの画面遷移対応 -> 一旦文字列として受け取り、「,」区切りで分割し、prcGetOrderItemで取り直す
            String conId = ApexPages.currentPage().getParameters().get('conid');
            if (conId.length() > 0){
                for(String str : conId.split(',')) strOderItemIds.add(str);
                System.debug('conidを分割します ' + strOderItemIds);
                // 注文商品を取得
                this.ODER_ITEM = prcGetOrderItem(strOderItemIds);
            }
            else{
                System.debug('conidが見つかりません ' + conId);
            }
        }
        
        
        // 注文商品が1の場合、キャンセル商品も1を自動セット
        for (OrderItem__c obj : ODER_ITEM) {
            if (obj.Quantity__c == 1) obj.Item2_CancelCount__c = 1;
        }
    }

    // 注文商品のレコードを取得
    private List<OrderItem__c> prcGetOrderItem(List<String> strOderItemIds)
    {
        // 注文商品のインスタンスを生成
        List<OrderItem__c> objList = new List<OrderItem__c>();
        
        String wfnms = 'Id,Name,Item2_Product__r.Name,' + UtilityClass.getCustomFieldsNames(OrderItem__c.SObjecttype);
        
        String query = 'SELECT ' + wfnms + ' FROM OrderItem__c WHERE Id IN :strOderItemIds AND Item2_Keyword_phase__c IN (\'契約中\', \'契約期間なし\', \'更新済み\') ';
        
        objList = Database.query(query);
        
        return objList;
    }
    
    public PageReference prcSave()
    {
        // キャンセル商品の作成
        List<OrderItem__c> upsOrderItem = new List<OrderItem__c>();
        
        // 入力チェック
        if (ODER_ITEM.size() == 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'キャンセル対象の商品が存在しません'));
            return null;
        }
        for (OrderItem__c obj : ODER_ITEM) {
            if (obj.Quantity__c < obj.Item2_CancelCount__c) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'キャンセル数量が注文数量を超えています'));
                return null;
            }
            if (obj.Item2_CancelCount__c == null) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'キャンセル数量が入力されていません'));
                return null;
            }
            if (obj.Item2_CancelCount__c < 0) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'キャンセル数量はマイナスにできません'));
                return null;
            }
            if (obj.Item2_CancellationDay__c == null && obj.Item2_Billing_Stop_Start_Date__c == null && obj.Item2_Billing_Stop_End_Date__c == null) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '「途中解約日」または、「請求停止開始日～請求停止終了日」を入力して下さい'));
                return null;
            }
            if (obj.Item2_CancellationDay__c != null && obj.Item2_Billing_Stop_Start_Date__c != null && obj.Item2_Billing_Stop_End_Date__c != null) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '「途中解約日」または、「請求停止開始日～請求停止終了日」のどちらか一方を入力して下さい'));
                return null;
            }
            if (obj.Item2_CancellationDay__c == null && (obj.Item2_Billing_Stop_Start_Date__c != null && obj.Item2_Billing_Stop_End_Date__c == null)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '請求停止終了日を入力して下さい'));
                return null;
            }
            if (obj.Item2_CancellationDay__c == null && (obj.Item2_Billing_Stop_Start_Date__c == null && obj.Item2_Billing_Stop_End_Date__c != null)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '請求停止開始日を入力して下さい'));
                return null;
            }
        }
        
        for (OrderItem__c obj : ODER_ITEM) {
            /* マイナス商品 */
            OrderItem__c insObj1 = new OrderItem__c();
            // 元データをコピー
            insObj1 = obj.clone(false, true);
            // 数量
            insObj1.Quantity__c = obj.Quantity__c * -1;
            // フェーズ
            if (obj.Item2_CancellationDay__c != null && obj.CancellationRequestCheck__c) {
                insObj1.Item2_Keyword_phase__c = '途中解約（請求）';
            } else {
                insObj1.Item2_Keyword_phase__c = (obj.Item2_CancellationDay__c != null) ? '途中解約' : '請求停止';
            }
            // キャンセル数量
            insObj1.Item2_CancelCount__c = null;
            // マイナス注文番号
            insObj1.MinusOrderNo__c = obj.Name;
            // 契約開始日
            insObj1.ServiceDate__c = obj.ServiceDate__c;
            // 契約終了日
            //insObj1.EndDate__c = obj.Item2_Billing_Stop_End_Date__c;
            // 作成種類
            insObj1.RecordCreateType__c = '';
            
            // 配列に追加
            upsOrderItem.add(insObj1);
            
            if (obj.Quantity__c > obj.Item2_CancelCount__c) {
                /* 差分商品 */
                OrderItem__c insObj2 = new OrderItem__c();
                // 元データをコピー
                insObj2 = obj.clone(false, true);
                // 数量（注文数量 ― キャンセル数量）
                insObj2.Quantity__c = obj.Quantity__c - obj.Item2_CancelCount__c;
                // キャンセル数量
                insObj2.Item2_CancelCount__c = null;
                // 途中解約日
                insObj2.Item2_CancellationDay__c = null;
                // 配列に追加
                upsOrderItem.add(insObj2);
            }
            
            /* 元データの変更 */
            // フェーズ
            if (obj.Item2_CancellationDay__c != null && obj.CancellationRequestCheck__c) {
                obj.Item2_Keyword_phase__c = '途中解約（請求）';
            } else {
                obj.Item2_Keyword_phase__c = (obj.Item2_CancellationDay__c != null) ? '途中解約' : '請求停止';
            }
            // キャンセル数量
            obj.Item2_CancelCount__c = null;
            
            upsOrderItem.add(obj);
        }
        
        // 注文商品をupsert
        if (upsOrderItem.size() > 0) upsert upsOrderItem;
        
        String prevURL = System.currentPageReference().getParameters().get('retURL');
        PageReference prevPage = new PageReference(prevURL); 
        return prevPage;
    }

}