@isTest
private with sharing class LeadSearch2Test{
		private static testMethod void testPageMethods() {	
			LeadSearch2 page = new LeadSearch2(new ApexPages.StandardController(new Lead()));	
			page.getOperatorOptions_Lead_OwnerId();	
			page.getOperatorOptions_Lead_Lead_WEBsite_Quality_c_multi();	
			page.getOperatorOptions_Lead_SalesUnit_c();	
			page.getOperatorOptions_Lead_WebSiteType_c_multi();	
			page.getOperatorOptions_Lead_Company();	
			page.getOperatorOptions_Lead_Email();	
			page.getOperatorOptions_Lead_tag_c();	
			page.getOperatorOptions_Lead_IsConverted();	
			page.getOperatorOptions_Lead_Status1_c();	
			page.getOperatorOptions_Lead_Status2_c();	
			page.getOperatorOptions_Lead_MailNG_c();	

		Integer defaultSize;

		defaultSize = page.Component3.items.size();
		page.Component3.add();
		System.assertEquals(defaultSize + 1, page.Component3.items.size());
		page.Component3.items[defaultSize].selected = true;
		page.Component3.doDeleteSelectedItems();
			System.assert(true);
		}	
			
	private static testMethod void testComponent3() {
		LeadSearch2.Component3 Component3 = new LeadSearch2.Component3(new List<Lead>(), new List<LeadSearch2.Component3Item>(), new List<Lead>(), null);
		Component3.setPageItems(new List<LeadSearch2.Component3Item>());
		Component3.create(new Lead());
		Component3.doDeleteSelectedItems();
		Component3.setPagesize(10);		Component3.doFirst();
		Component3.doPrevious();
		Component3.doNext();
		Component3.doLast();
		Component3.doSort();
		System.assert(true);
	}
	
}