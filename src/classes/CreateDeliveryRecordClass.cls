public class CreateDeliveryRecordClass {

    public static boolean firstRun = true;

    public static void prcCreateDelivery(List<OrderItem__c> trgOrderItem)
    {
        // 注文商品のID
        List<String> strOrderItemId = new List<String>();
        // 注文商品オブジェクト
        List<OrderItem__c> objOrderItem = new List<OrderItem__c>();
        // 納品管理オブジェクト
        List<TextManagement2__c> insDeli = new List<TextManagement2__c>();
        // レコードタイプを取得
        List<RecordType> objRecordType = new List<RecordType>();
        // レコードタイプのMap
        Map<String, RecordType> mapRecordType = new Map<String, RecordType>();

        // 注文商品のIDをセット
        for (OrderItem__c obj : trgOrderItem) strOrderItemId.add(obj.Id);

        // 注文商品を取得
        objOrderItem = [
            SELECT
                Id,
                RecordType__c,
                SalesPerson1__c,
                OppProduct__r.Opportunity__r.Opportunity_Support__c,
                Item2_Relation__r.Order_Relation__r.Contract_FastOrdersSales__c,
                SalesPersonSub__c,
                Quantity__c,
                Item2_DeliveryExistence__c,
                Item2_ServiceDate__c,
                Item2_Entry_date__c,
                Item2_ProductType1__c,
                sisakukigyou__c,
                SalesPercentage1__c,
                SalesPercentage2__c,
                SalesPersonUnit__c,
                Bill_Main__c,
                Order_PaymentSiteMaster__c,
                RecordCreateType__c,
                OppProduct__r.DeliveryFormat__c
            FROM OrderItem__c
            WHERE Id IN :strOrderItemId
            AND DeliveryRecordCount__c = 0
        ];

        // レコードタイプを取得
        objRecordType = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'TextManagement2__c'];

        // レコードタイプをMapに追加
        for (RecordType rec : objRecordType) mapRecordType.put(rec.DeveloperName, rec);

        // 注文商品のループ
        for (OrderItem__c obj : objOrderItem) {
            // レコードタイプ判定
            String strRecordTypeId = '';
            // 社外納品日
            Date dtShagaiNouhinbi = null;
            // 社外納品数
            Decimal intShagaiNouhin = null;
            // 支払サイト判定
            String CloudStatus = '';

            // 注文商品のレコードタイプが「サグーワークス/KIJITASU用」
            if (obj.RecordType__c == 'KIJITASU') continue;
            // 注文商品のレコードタイプが「サグーワークス/KIJITASU用」以外で かつ 納品有無（コード）が"有"
            if (obj.RecordType__c != 'KIJITASU' && obj.Item2_DeliveryExistence__c == '有') strRecordTypeId = mapRecordType.get('Report').Id;
            // 注文商品のレコードタイプが「サグーワークス/KIJITASU用」以外で かつ 納品有無（コード）が"無"
            if (obj.RecordType__c != 'KIJITASU' && obj.Item2_DeliveryExistence__c == '無') {
                strRecordTypeId = mapRecordType.get('NoDelivery').Id;
                // 社外納品日に請求対象日（メイン）をセットする
                dtShagaiNouhinbi = obj.Bill_Main__c;
            }
            // レコードタイプがnullの場合
            if (strRecordTypeId == '') strRecordTypeId = mapRecordType.get('NoDelivery').Id;

            // 注文商品の「納品有無（コード）」が"有"以外の場合
            if (obj.Item2_DeliveryExistence__c != '有') {
                intShagaiNouhin = obj.Quantity__c;
            }

            // 支払サイトが"前払い"の場合、納品管理ステータスは"前払い入金待ち"で作成する
            if (obj.Order_PaymentSiteMaster__c == '前払い') {
                CloudStatus = '前払い入金待ち';
            }

            // 納品管理のインスタンスを生成
            TextManagement2__c ins = new TextManagement2__c(
                Item2_OrderProduct__c = obj.Id,
                RecordTypeId = strRecordTypeId,
                SalesDeliveryCount__c = obj.Quantity__c,
                DeliveryCount__c = intShagaiNouhin,
                Bill_Main__c = dtShagaiNouhinbi,
                sisakukigyou__c = obj.sisakukigyou__c,
                CloudStatus__c = CloudStatus
            );

            // 納品管理の配列に追加
            insDeli.add(ins);
        }

        // 納品管理を作成
        if (insDeli.size() > 0) insert insDeli;
    }

}