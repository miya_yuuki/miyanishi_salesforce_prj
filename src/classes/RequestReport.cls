global with sharing class RequestReport extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public CustomObject1__c record {get{return (CustomObject1__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	
	
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	
	{
	setApiVersion(31.0);
	}
	public RequestReport(ApexPages.StandardController controller) {
		super(controller);


		SObjectField f;

		f = CustomObject1__c.fields.Name;
		f = CustomObject1__c.fields.Status__c;
		f = CustomObject1__c.fields.Opportunity__c;
		f = CustomObject1__c.fields.RequestDay__c;
		f = CustomObject1__c.fields.OrderNo2__c;
		f = CustomObject1__c.fields.RequestEndDay__c;
		f = CustomObject1__c.fields.OrderNo__c;
		f = CustomObject1__c.fields.RDDay__c;
		f = CustomObject1__c.fields.ProductGroupgazou__c;
		f = CustomObject1__c.fields.RecordTypeId;
		f = CustomObject1__c.fields.unit__c;
		f = CustomObject1__c.fields.OwnerId;
		f = CustomObject1__c.fields.SalesSupportPerson__c;
		f = CustomObject1__c.fields.Account__c;
		f = CustomObject1__c.fields.ReportType__c;
		f = CustomObject1__c.fields.MailAddress__c;
		f = CustomObject1__c.fields.MailPerson__c;
		f = CustomObject1__c.fields.Report11__c;
		f = CustomObject1__c.fields.BillTitle__c;
		f = CustomObject1__c.fields.TOorCC11__c;
		f = CustomObject1__c.fields.ReportType2__c;
		f = CustomObject1__c.fields.MailAddress2__c;
		f = CustomObject1__c.fields.MailPerson2__c;
		f = CustomObject1__c.fields.Report22__c;
		f = CustomObject1__c.fields.BillTitle12__c;
		f = CustomObject1__c.fields.TOorCC22__c;
		f = CustomObject1__c.fields.ReportType3__c;
		f = CustomObject1__c.fields.MailAddress3__c;
		f = CustomObject1__c.fields.MailPerson3__c;
		f = CustomObject1__c.fields.Report33__c;
		f = CustomObject1__c.fields.BillTitle13__c;
		f = CustomObject1__c.fields.TOorCC33__c;
		f = Opportunity.fields.AccountNameView__c;
		f = Contact.fields.Contact_BillCompany__c;
		f = Contact.fields.Title;
		f = Contact.fields.Email;
		f = Contact.fields.TOorCC__c;
		f = Contact.fields.report__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = CustomObject1__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(CustomObject1__c.SObjectType);
			
			mainQuery = new SkyEditor2.Query('CustomObject1__c');
			mainQuery.addField('Status__c');
			mainQuery.addField('Opportunity__c');
			mainQuery.addField('OrderNo2__c');
			mainQuery.addField('OwnerId');
			mainQuery.addField('SalesSupportPerson__c');
			mainQuery.addField('Account__c');
			mainQuery.addField('ReportType__c');
			mainQuery.addField('MailPerson__c');
			mainQuery.addField('Report11__c');
			mainQuery.addField('TOorCC11__c');
			mainQuery.addField('ReportType2__c');
			mainQuery.addField('MailPerson2__c');
			mainQuery.addField('Report22__c');
			mainQuery.addField('TOorCC22__c');
			mainQuery.addField('ReportType3__c');
			mainQuery.addField('MailPerson3__c');
			mainQuery.addField('Report33__c');
			mainQuery.addField('TOorCC33__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('RequestDay__c');
			mainQuery.addFieldAsOutput('RequestEndDay__c');
			mainQuery.addFieldAsOutput('OrderNo__c');
			mainQuery.addFieldAsOutput('RDDay__c');
			mainQuery.addFieldAsOutput('ProductGroupgazou__c');
			mainQuery.addFieldAsOutput('RecordType.Name');
			mainQuery.addFieldAsOutput('unit__c');
			mainQuery.addFieldAsOutput('MailAddress__c');
			mainQuery.addFieldAsOutput('BillTitle__c');
			mainQuery.addFieldAsOutput('MailAddress2__c');
			mainQuery.addFieldAsOutput('BillTitle12__c');
			mainQuery.addFieldAsOutput('MailAddress3__c');
			mainQuery.addFieldAsOutput('BillTitle13__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			
			if (record.Id == null) {
				
				saveOldValues();
				
				if(record.RecordTypeId == null) recordTypeSelector.applyDefault(record);
				
			}

			
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}


	public void loadReferenceValues_Component305() {
		if (record.Opportunity__c == null) {

			if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.Account__c)) {
				record.Account__c = null;
			}
				return;
		}
		Opportunity[] referenceTo = [SELECT AccountNameView__c FROM Opportunity WHERE Id=:record.Opportunity__c];
		if (referenceTo.size() == 0) {
			record.Opportunity__c.addError(SkyEditor2.Messages.referenceDataNotFound(record.Opportunity__c));
			return;
		}
		
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.Account__c)) {
			record.Account__c = referenceTo[0].AccountNameView__c;
		}
		
	}

	public void loadReferenceValues_Component111() {
		if (record.MailPerson__c == null) {

			if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.Account__c)) {
				record.Account__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.BillTitle__c)) {
				record.BillTitle__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.MailAddress__c)) {
				record.MailAddress__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.Report11__c)) {
				record.Report11__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.TOorCC11__c)) {
				record.TOorCC11__c = null;
			}
				return;
		}
		Contact[] referenceTo = [SELECT Contact_BillCompany__c,Title,Email,TOorCC__c,report__c FROM Contact WHERE Id=:record.MailPerson__c];
		if (referenceTo.size() == 0) {
			record.MailPerson__c.addError(SkyEditor2.Messages.referenceDataNotFound(record.MailPerson__c));
			return;
		}
		
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.Account__c)) {
			record.Account__c = referenceTo[0].Contact_BillCompany__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.BillTitle__c)) {
			record.BillTitle__c = referenceTo[0].Title;
		}
		
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.MailAddress__c)) {
			record.MailAddress__c = referenceTo[0].Email;
		}
		
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.Report11__c)) {
			record.Report11__c = referenceTo[0].TOorCC__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.TOorCC11__c)) {
			record.TOorCC11__c = referenceTo[0].report__c;
		}
		
	}

	public void loadReferenceValues_Component132() {
		if (record.MailPerson2__c == null) {

			if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.TOorCC11__c)) {
				record.TOorCC11__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.BillTitle12__c)) {
				record.BillTitle12__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.MailAddress2__c)) {
				record.MailAddress2__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.Report22__c)) {
				record.Report22__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.TOorCC22__c)) {
				record.TOorCC22__c = null;
			}
				return;
		}
		Contact[] referenceTo = [SELECT report__c,Title,Email,TOorCC__c FROM Contact WHERE Id=:record.MailPerson2__c];
		if (referenceTo.size() == 0) {
			record.MailPerson2__c.addError(SkyEditor2.Messages.referenceDataNotFound(record.MailPerson2__c));
			return;
		}
		
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.TOorCC11__c)) {
			record.TOorCC11__c = referenceTo[0].report__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.BillTitle12__c)) {
			record.BillTitle12__c = referenceTo[0].Title;
		}
		
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.MailAddress2__c)) {
			record.MailAddress2__c = referenceTo[0].Email;
		}
		
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.Report22__c)) {
			record.Report22__c = referenceTo[0].TOorCC__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.TOorCC22__c)) {
			record.TOorCC22__c = referenceTo[0].report__c;
		}
		
	}

	public void loadReferenceValues_Component137() {
		if (record.MailPerson3__c == null) {

			if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.BillTitle13__c)) {
				record.BillTitle13__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.MailAddress3__c)) {
				record.MailAddress3__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.Report33__c)) {
				record.Report33__c = null;
			}
		
			if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.TOorCC33__c)) {
				record.TOorCC33__c = null;
			}
				return;
		}
		Contact[] referenceTo = [SELECT Title,Email,TOorCC__c,report__c FROM Contact WHERE Id=:record.MailPerson3__c];
		if (referenceTo.size() == 0) {
			record.MailPerson3__c.addError(SkyEditor2.Messages.referenceDataNotFound(record.MailPerson3__c));
			return;
		}
		
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.BillTitle13__c)) {
			record.BillTitle13__c = referenceTo[0].Title;
		}
		
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.MailAddress3__c)) {
			record.MailAddress3__c = referenceTo[0].Email;
		}
		
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.Report33__c)) {
			record.Report33__c = referenceTo[0].TOorCC__c;
		}
		
		if (SkyEditor2.Util.isEditable(record, CustomObject1__c.fields.TOorCC33__c)) {
			record.TOorCC33__c = referenceTo[0].report__c;
		}
		
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}