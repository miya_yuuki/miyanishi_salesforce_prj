global with sharing class Opportunity_Search extends SkyEditor2.SkyEditorPageBaseWithSharing{
	public Opportunity record{get;set;}
	public Component2 Component2 {get; private set;}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	public string Component243_pagename{get;set;}
	public string Component243_recordid{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component123_from{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component123_to{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component123_isNull{get;set;}
	public SkyEditor2.TextHolder.OperatorHolder Component123_isNull_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component317_from{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component317_to{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component317_isNull{get;set;}
	public SkyEditor2.TextHolder.OperatorHolder Component317_isNull_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component323_from{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component323_to{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component323_isNull{get;set;}
	public SkyEditor2.TextHolder.OperatorHolder Component323_isNull_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component325_from{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component325_to{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component325_isNull{get;set;}
	public SkyEditor2.TextHolder.OperatorHolder Component325_isNull_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component262_from{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component262_to{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component262_isNull{get;set;}
	public SkyEditor2.TextHolder.OperatorHolder Component262_isNull_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component329_from{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component329_to{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component329_isNull{get;set;}
	public SkyEditor2.TextHolder.OperatorHolder Component329_isNull_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component222_from{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component222_to{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component222_isNull{get;set;}
	public SkyEditor2.TextHolder.OperatorHolder Component222_isNull_op{get;set;}
	public Opportunity Component180_val {get;set;}
	public SkyEditor2.TextHolder Component180_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component320_val {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component320_val_dummy {get;set;}
	public SkyEditor2.TextHolder Component320_op{get;set;}
	public List<SelectOption> valueOptions_Opportunity_LeadEvaluation_c_multi {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component322_val {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component322_val_dummy {get;set;}
	public SkyEditor2.TextHolder Component322_op{get;set;}
	public List<SelectOption> valueOptions_Opportunity_suggestion_importance_c_multi {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component191_val {get;set;}
	public SkyEditor2.TextHolder Component191_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component265_val {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component265_val_dummy {get;set;}
	public SkyEditor2.TextHolder Component265_op{get;set;}
	public List<SelectOption> valueOptions_Opportunity_Opportunity_lead1_c_multi {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component328_val {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component328_val_dummy {get;set;}
	public SkyEditor2.TextHolder Component328_op{get;set;}
	public List<SelectOption> valueOptions_Opportunity_document_c_multi {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component11_val {get;set;}
	public SkyEditor2.TextHolder Component11_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component261_val {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component261_val_dummy {get;set;}
	public SkyEditor2.TextHolder Component261_op{get;set;}
	public List<SelectOption> valueOptions_Opportunity_this_month_approach_c_multi {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component269_val {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component269_val_dummy {get;set;}
	public SkyEditor2.TextHolder Component269_op{get;set;}
	public List<SelectOption> valueOptions_Opportunity_Opportunity_lead2_c_multi {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component9_val {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component9_val_dummy {get;set;}
	public SkyEditor2.TextHolder Component9_op{get;set;}
	public List<SelectOption> valueOptions_Opportunity_StageName_multi {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component332_val {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component332_val_dummy {get;set;}
	public SkyEditor2.TextHolder Component332_op{get;set;}
	public List<SelectOption> valueOptions_Opportunity_agreement_probability_c_multi {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component334_val {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component334_val_dummy {get;set;}
	public SkyEditor2.TextHolder Component334_op{get;set;}
	public List<SelectOption> valueOptions_Opportunity_AppointmentType_c_multi {get;set;}
	public String recordTypeRecordsJSON_Opportunity {get; private set;}
	public String defaultRecordTypeId_Opportunity {get; private set;}
	public String metadataJSON_Opportunity {get; private set;}
	{
	setApiVersion(42.0);
	}
	public Opportunity_Search(ApexPages.StandardController controller){
		super(controller);

		SObjectField f;

		f = Opportunity.fields.OwnerId;
		f = Opportunity.fields.LeadEvaluation__c;
		f = Opportunity.fields.suggestion_importance__c;
		f = Opportunity.fields.Opportunity_SalesPerson_Unit__c;
		f = Opportunity.fields.Opportunity_lead1__c;
		f = Opportunity.fields.document__c;
		f = Opportunity.fields.Name;
		f = Opportunity.fields.this_month_approach__c;
		f = Opportunity.fields.Opportunity_lead2__c;
		f = Opportunity.fields.StageName;
		f = Opportunity.fields.agreement_probability__c;
		f = Opportunity.fields.AppointmentType__c;
		f = Opportunity.fields.CloseDate;
		f = Opportunity.fields.Opportunity_Type__c;
		f = Opportunity.fields.Opportunity_Partner__c;
		f = Opportunity.fields.Opportunity_endClient1__c;
		f = Opportunity.fields.ServiceWEBSite__c;
		f = Opportunity.fields.WEBTitle__c;
		f = Opportunity.fields.gross_profit_all__c;
		f = Opportunity.fields.Opportunity_deadDay__c;
		f = Opportunity.fields.NA_memo__c;
		f = Opportunity.fields.karute__c;
		f = Opportunity.fields.MQLDate__c;
		f = Opportunity.fields.MQL_first_visit_date__c;
		f = Opportunity.fields.AppointmentDay__c;
		f = Opportunity.fields.first_visit_date__c;
		f = Opportunity.fields.recorded_date__c;
		f = Opportunity.fields.non_recorded_flg__c;
		f = Opportunity.fields.suggest_date__c;
		f = Opportunity.fields.suggestion_importance_summary__c;
		f = Opportunity.fields.review_status__c;
		f = Opportunity.fields.review_date__c;
		f = Opportunity.fields.review_practitioner__c;
		f = Opportunity.fields.roleplaying__c;
		f = Opportunity.fields.sub_sales_person__c;
		f = Opportunity.fields.MQLSalesPerson__c;
		f = Opportunity.fields.contents_order_status__c;
		f = Opportunity.fields.Opportunity_ActionDay__c;
		f = Opportunity.fields.Opportunity_NoRecord__c;
		f = Opportunity.fields.Opportunity_DeadMemo__c;
		f = Opportunity.fields.ProposalProducts__c;
		f = Opportunity.fields.SQLType__c;
 f = Opportunity.fields.CloseDate;
 f = Opportunity.fields.Opportunity_deadDay__c;
 f = Opportunity.fields.syouninn__c;
 f = Opportunity.fields.MQLDate__c;
 f = Opportunity.fields.AppointmentDay__c;
 f = Opportunity.fields.documents_delivery_date__c;
 f = Opportunity.fields.Opportunity_ActionDay__c;
		f = Opportunity.fields.Opportunity_dead__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainRecord = null;
			mainSObjectType = Opportunity.SObjectType;
			mode = SkyEditor2.LayoutMode.TempSearch_01; 
			Component123_from = new SkyEditor2__SkyEditorDummy__c();
			Component123_to = new SkyEditor2__SkyEditorDummy__c();
			Component123_isNull = new SkyEditor2__SkyEditorDummy__c();
			Component123_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq');
			Component317_from = new SkyEditor2__SkyEditorDummy__c();
			Component317_to = new SkyEditor2__SkyEditorDummy__c();
			Component317_isNull = new SkyEditor2__SkyEditorDummy__c();
			Component317_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq');
			Component323_from = new SkyEditor2__SkyEditorDummy__c();
			Component323_to = new SkyEditor2__SkyEditorDummy__c();
			Component323_isNull = new SkyEditor2__SkyEditorDummy__c();
			Component323_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq');
			Component325_from = new SkyEditor2__SkyEditorDummy__c();
			Component325_to = new SkyEditor2__SkyEditorDummy__c();
			Component325_isNull = new SkyEditor2__SkyEditorDummy__c();
			Component325_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq');
			Component262_from = new SkyEditor2__SkyEditorDummy__c();
			Component262_to = new SkyEditor2__SkyEditorDummy__c();
			Component262_isNull = new SkyEditor2__SkyEditorDummy__c();
			Component262_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq');
			Component329_from = new SkyEditor2__SkyEditorDummy__c();
			Component329_to = new SkyEditor2__SkyEditorDummy__c();
			Component329_isNull = new SkyEditor2__SkyEditorDummy__c();
			Component329_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq');
			Component222_from = new SkyEditor2__SkyEditorDummy__c();
			Component222_to = new SkyEditor2__SkyEditorDummy__c();
			Component222_isNull = new SkyEditor2__SkyEditorDummy__c();
			Component222_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq');
			Opportunity lookupObjComponent39 = new Opportunity();
			Component180_val = lookupObjComponent39;
			Component180_op = new SkyEditor2.TextHolder();
			Component320_val = new SkyEditor2__SkyEditorDummy__c();
			Component320_val_dummy = new SkyEditor2__SkyEditorDummy__c();
			Component320_op = new SkyEditor2.TextHolder();
			valueOptions_Opportunity_LeadEvaluation_c_multi = new List<SelectOption>{new SelectOption('', Label.none)};
			for (PicklistEntry e : Opportunity.LeadEvaluation__c.getDescribe().getPicklistValues()) {
				if (e.isActive()) {valueOptions_Opportunity_LeadEvaluation_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));}
			}
			Component322_val = new SkyEditor2__SkyEditorDummy__c();
			Component322_val_dummy = new SkyEditor2__SkyEditorDummy__c();
			Component322_op = new SkyEditor2.TextHolder();
			valueOptions_Opportunity_suggestion_importance_c_multi = new List<SelectOption>{new SelectOption('', Label.none)};
			for (PicklistEntry e : Opportunity.suggestion_importance__c.getDescribe().getPicklistValues()) {
				if (e.isActive()) {valueOptions_Opportunity_suggestion_importance_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));}
			}
			Component191_val = new SkyEditor2__SkyEditorDummy__c();
			Component191_op = new SkyEditor2.TextHolder();
			Component265_val = new SkyEditor2__SkyEditorDummy__c();
			Component265_val_dummy = new SkyEditor2__SkyEditorDummy__c();
			Component265_op = new SkyEditor2.TextHolder();
			valueOptions_Opportunity_Opportunity_lead1_c_multi = new List<SelectOption>{new SelectOption('', Label.none)};
			for (PicklistEntry e : Opportunity.Opportunity_lead1__c.getDescribe().getPicklistValues()) {
				if (e.isActive()) {valueOptions_Opportunity_Opportunity_lead1_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));}
			}
			Component328_val = new SkyEditor2__SkyEditorDummy__c();
			Component328_val_dummy = new SkyEditor2__SkyEditorDummy__c();
			Component328_op = new SkyEditor2.TextHolder();
			valueOptions_Opportunity_document_c_multi = new List<SelectOption>{new SelectOption('', Label.none)};
			for (PicklistEntry e : Opportunity.document__c.getDescribe().getPicklistValues()) {
				if (e.isActive()) {valueOptions_Opportunity_document_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));}
			}
			Component11_val = new SkyEditor2__SkyEditorDummy__c();
			Component11_op = new SkyEditor2.TextHolder();
			Component261_val = new SkyEditor2__SkyEditorDummy__c();
			Component261_val_dummy = new SkyEditor2__SkyEditorDummy__c();
			Component261_op = new SkyEditor2.TextHolder();
			valueOptions_Opportunity_this_month_approach_c_multi = new List<SelectOption>{new SelectOption('', Label.none)};
			for (PicklistEntry e : Opportunity.this_month_approach__c.getDescribe().getPicklistValues()) {
				if (e.isActive()) {valueOptions_Opportunity_this_month_approach_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));}
			}
			Component269_val = new SkyEditor2__SkyEditorDummy__c();
			Component269_val_dummy = new SkyEditor2__SkyEditorDummy__c();
			Component269_op = new SkyEditor2.TextHolder();
			valueOptions_Opportunity_Opportunity_lead2_c_multi = new List<SelectOption>{new SelectOption('', Label.none)};
			for (PicklistEntry e : Opportunity.Opportunity_lead2__c.getDescribe().getPicklistValues()) {
				if (e.isActive()) {valueOptions_Opportunity_Opportunity_lead2_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));}
			}
			Component9_val = new SkyEditor2__SkyEditorDummy__c();
			Component9_val_dummy = new SkyEditor2__SkyEditorDummy__c();
			Component9_op = new SkyEditor2.TextHolder();
			valueOptions_Opportunity_StageName_multi = new List<SelectOption>{new SelectOption('', Label.none)};
			for (PicklistEntry e : Opportunity.StageName.getDescribe().getPicklistValues()) {
				if (e.isActive()) {valueOptions_Opportunity_StageName_multi.add(new SelectOption(e.getValue(), e.getLabel()));}
			}
			Component332_val = new SkyEditor2__SkyEditorDummy__c();
			Component332_val_dummy = new SkyEditor2__SkyEditorDummy__c();
			Component332_op = new SkyEditor2.TextHolder();
			valueOptions_Opportunity_agreement_probability_c_multi = new List<SelectOption>{new SelectOption('', Label.none)};
			for (PicklistEntry e : Opportunity.agreement_probability__c.getDescribe().getPicklistValues()) {
				if (e.isActive()) {valueOptions_Opportunity_agreement_probability_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));}
			}
			Component334_val = new SkyEditor2__SkyEditorDummy__c();
			Component334_val_dummy = new SkyEditor2__SkyEditorDummy__c();
			Component334_op = new SkyEditor2.TextHolder();
			valueOptions_Opportunity_AppointmentType_c_multi = new List<SelectOption>{new SelectOption('', Label.none)};
			for (PicklistEntry e : Opportunity.AppointmentType__c.getDescribe().getPicklistValues()) {
				if (e.isActive()) {valueOptions_Opportunity_AppointmentType_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));}
			}
			queryMap.put(
				'Component2',
				new SkyEditor2.Query('Opportunity')
					.addFieldAsOutput('Opportunity_SalesPerson_Unit__c')
					.addField('CloseDate')
					.addField('StageName')
					.addFieldAsOutput('Name')
					.addField('Opportunity_Type__c')
					.addField('Opportunity_Partner__c')
					.addField('Opportunity_endClient1__c')
					.addField('ServiceWEBSite__c')
					.addField('WEBTitle__c')
					.addField('this_month_approach__c')
					.addField('agreement_probability__c')
					.addFieldAsOutput('gross_profit_all__c')
					.addField('Opportunity_deadDay__c')
					.addField('NA_memo__c')
					.addFieldAsOutput('karute__c')
					.addField('MQLDate__c')
					.addField('MQL_first_visit_date__c')
					.addField('AppointmentDay__c')
					.addField('first_visit_date__c')
					.addField('recorded_date__c')
					.addField('non_recorded_flg__c')
					.addField('suggest_date__c')
					.addField('suggestion_importance_summary__c')
					.addField('suggestion_importance__c')
					.addField('review_status__c')
					.addField('review_date__c')
					.addField('review_practitioner__c')
					.addField('document__c')
					.addField('roleplaying__c')
					.addField('sub_sales_person__c')
					.addField('MQLSalesPerson__c')
					.addField('LeadEvaluation__c')
					.addField('Opportunity_lead1__c')
					.addField('Opportunity_lead2__c')
					.addField('AppointmentType__c')
					.addField('SQLType__c')
					.addField('contents_order_status__c')
					.addField('Opportunity_ActionDay__c')
					.addField('Opportunity_NoRecord__c')
					.addField('Opportunity_DeadMemo__c')
					.addField('ProposalProducts__c')
					.addFieldAsOutput('RecordTypeId')
					.limitRecords(500)
					.addListener(new SkyEditor2.QueryWhereRegister(Component123_from, 'SkyEditor2__Date__c', 'CloseDate', new SkyEditor2.TextHolder('ge'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component123_to, 'SkyEditor2__Date__c', 'CloseDate', new SkyEditor2.TextHolder('le'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component123_isNull, 'SkyEditor2__Date__c', 'CloseDate', Component123_isNull_op, true,0,false )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component317_from, 'SkyEditor2__Date__c', 'Opportunity_deadDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component317_to, 'SkyEditor2__Date__c', 'Opportunity_deadDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component317_isNull, 'SkyEditor2__Date__c', 'Opportunity_deadDay__c', Component317_isNull_op, true,0,false )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component323_from, 'SkyEditor2__Date__c', 'syouninn__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component323_to, 'SkyEditor2__Date__c', 'syouninn__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component323_isNull, 'SkyEditor2__Date__c', 'syouninn__c', Component323_isNull_op, true,0,false )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component325_from, 'SkyEditor2__Date__c', 'MQLDate__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component325_to, 'SkyEditor2__Date__c', 'MQLDate__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component325_isNull, 'SkyEditor2__Date__c', 'MQLDate__c', Component325_isNull_op, true,0,false )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component262_from, 'SkyEditor2__Date__c', 'AppointmentDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component262_to, 'SkyEditor2__Date__c', 'AppointmentDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component262_isNull, 'SkyEditor2__Date__c', 'AppointmentDay__c', Component262_isNull_op, true,0,false )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component329_from, 'SkyEditor2__Date__c', 'documents_delivery_date__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component329_to, 'SkyEditor2__Date__c', 'documents_delivery_date__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component329_isNull, 'SkyEditor2__Date__c', 'documents_delivery_date__c', Component329_isNull_op, true,0,false )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component222_from, 'SkyEditor2__Date__c', 'Opportunity_ActionDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component222_to, 'SkyEditor2__Date__c', 'Opportunity_ActionDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component222_isNull, 'SkyEditor2__Date__c', 'Opportunity_ActionDay__c', Component222_isNull_op, true,0,false )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component180_val, 'OwnerId', 'OwnerId', Component180_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component320_val_dummy, 'SkyEditor2__Text__c','LeadEvaluation__c', Component320_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component322_val_dummy, 'SkyEditor2__Text__c','suggestion_importance__c', Component322_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component191_val, 'SkyEditor2__Text__c', 'Opportunity_SalesPerson_Unit__c', Component191_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component265_val_dummy, 'SkyEditor2__Text__c','Opportunity_lead1__c', Component265_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component328_val_dummy, 'SkyEditor2__Text__c','document__c', Component328_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component11_val, 'SkyEditor2__Text__c', 'Name', Component11_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component261_val_dummy, 'SkyEditor2__Text__c','this_month_approach__c', Component261_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component269_val_dummy, 'SkyEditor2__Text__c','Opportunity_lead2__c', Component269_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component9_val_dummy, 'SkyEditor2__Text__c','StageName', Component9_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component332_val_dummy, 'SkyEditor2__Text__c','agreement_probability__c', Component332_op, true, 0, false ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component334_val_dummy, 'SkyEditor2__Text__c','AppointmentType__c', Component334_op, true, 0, false ))
					.addWhere(' ( Opportunity_dead__c not in (\'5. 審査NG\',\'6. 2重登録\' ) )')
.addSort('StageName',True,True).addSort('CloseDate',True,True)
				);
			Component2 = new Component2(new List<Opportunity>(), new List<Component2Item>(), new List<Opportunity>(), null);
			 Component2.setPageItems(new List<Component2Item>());
			 Component2.setPageSize(10);
			listItemHolders.put('Component2', Component2);
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(Opportunity.SObjectType, true);
			p_showHeader = true;
			p_sidebar = false;
			execInitialSearch = false;
			presetSystemParams();
			Component2.extender = this.extender;
			initSearch();
		} catch (SkyEditor2.Errors.SObjectNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.Errors.FieldNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.ExtenderException e) {
			 e.setMessagesToPage();
		} catch (Exception e) {
			System.Debug(LoggingLevel.Error, e);
			SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);
		}
	}
	public List<SelectOption> getOperatorOptions_Opportunity_OwnerId() { 
		return getOperatorOptions('Opportunity', 'OwnerId');
	}
	public List<SelectOption> getOperatorOptions_Opportunity_LeadEvaluation_c_multi() { 
		return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	}
	public List<SelectOption> getOperatorOptions_Opportunity_suggestion_importance_c_multi() { 
		return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	}
	public List<SelectOption> getOperatorOptions_Opportunity_Opportunity_SalesPerson_Unit_c() { 
		return getOperatorOptions('Opportunity', 'Opportunity_SalesPerson_Unit__c');
	}
	public List<SelectOption> getOperatorOptions_Opportunity_Opportunity_lead1_c_multi() { 
		return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	}
	public List<SelectOption> getOperatorOptions_Opportunity_document_c_multi() { 
		return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	}
	public List<SelectOption> getOperatorOptions_Opportunity_Name() { 
		return getOperatorOptions('Opportunity', 'Name');
	}
	public List<SelectOption> getOperatorOptions_Opportunity_this_month_approach_c_multi() { 
		return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	}
	public List<SelectOption> getOperatorOptions_Opportunity_Opportunity_lead2_c_multi() { 
		return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	}
	public List<SelectOption> getOperatorOptions_Opportunity_StageName_multi() { 
		return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	}
	public List<SelectOption> getOperatorOptions_Opportunity_agreement_probability_c_multi() { 
		return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	}
	public List<SelectOption> getOperatorOptions_Opportunity_AppointmentType_c_multi() { 
		return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	}
	global with sharing class Component2Item extends SkyEditor2.ListItem {
		public Opportunity record{get; private set;}
		@TestVisible
		Component2Item(Component2 holder, Opportunity record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component2 extends SkyEditor2.PagingList {
		public List<Component2Item> items{get; private set;}
		@TestVisible
			Component2(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component2Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component2Item(this, (Opportunity)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
		public void doFirst(){first();}
		public void doPrevious(){previous();}
		public void doNext(){next();}
		public void doLast(){last();}
		public void doSort(){sort();}

        public List<Component2Item> getViewItems() {            return (List<Component2Item>) getPageItems();        }
	}

	public Opportunity Component2_table_Conversion { get { return new Opportunity();}}
	
	public String Component2_table_selectval { get; set; }
	
	
}