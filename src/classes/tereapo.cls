global with sharing class tereapo extends SkyEditor2.SkyEditorPageBaseWithSharing {	
	    	
	    public task__c record{get;set;}	
	    	
    
	    public Component2 Component2 {get; private set;}	
	    	
	    public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
	    	
	    public SkyEditor2__SkyEditorDummy__c Component4_from{get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component4_to{get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component4_isNull{get;set;}	
	    public SkyEditor2.TextHolder.OperatorHolder Component4_isNull_op{get;set;}	
	    	
	    public SkyEditor2__SkyEditorDummy__c Component6_from{get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component6_to{get;set;}	
	    public SkyEditor2__SkyEditorDummy__c Component6_isNull{get;set;}	
	    public SkyEditor2.TextHolder.OperatorHolder Component6_isNull_op{get;set;}	
	    	
	    public task__c Component9_val {get;set;}	
        public SkyEditor2.TextHolder Component9_op{get;set;}	
	    	
	    public task__c Component11_val {get;set;}	
        public SkyEditor2.TextHolder Component11_op{get;set;}	
	    	
	    public task__c Component13_val {get;set;}	
        public SkyEditor2.TextHolder Component13_op{get;set;}	
	    	
    public String recordTypeRecordsJSON_task_c {get; private set;}
    public String defaultRecordTypeId_task_c {get; private set;}
    public String metadataJSON_task_c {get; private set;}
    public String picklistValuesJSON_task_c_Type_c {get; private set;}
    public String picklistValuesJSON_task_c_Time_c {get; private set;}
    public String picklistValuesJSON_task_c_PhaseBig_c {get; private set;}
    public String picklistValuesJSON_task_c_PhaseSmall_c {get; private set;}
	    public tereapo(ApexPages.StandardController controller) {	
	        super(controller);	

            SObjectField f;

            f = task__c.fields.OwnerId;
            f = task__c.fields.Opportunity__c;
            f = task__c.fields.lead__c;
            f = task__c.fields.Type__c;
            f = task__c.fields.Time__c;
            f = task__c.fields.PhaseBig__c;
            f = task__c.fields.PhaseSmall__c;
            f = task__c.fields.PhoneA__c;
            f = task__c.fields.Memo__c;
            f = task__c.fields.CreatedDate;

        List<RecordTypeInfo> recordTypes;
	        try {	
	            	
	            mainRecord = null;	
	            mainSObjectType = task__c.SObjectType;	
	            	
	            	
	            mode = SkyEditor2.LayoutMode.TempSearch_01; 
	            	
	            Component4_from = new SkyEditor2__SkyEditorDummy__c();	
	            Component4_to = new SkyEditor2__SkyEditorDummy__c();	
	            Component4_isNull = new SkyEditor2__SkyEditorDummy__c();	
	            Component4_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
	            	
	            Component6_from = new SkyEditor2__SkyEditorDummy__c();	
	            Component6_to = new SkyEditor2__SkyEditorDummy__c();	
	            Component6_isNull = new SkyEditor2__SkyEditorDummy__c();	
	            Component6_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
	            	
	            task__c lookupObjComponent37 = new task__c();	
	            Component9_val = lookupObjComponent37;	
	            Component9_op = new SkyEditor2.TextHolder();	
	            	
	            Component11_val = lookupObjComponent37;	
	            Component11_op = new SkyEditor2.TextHolder();	
	            	
	            Component13_val = lookupObjComponent37;	
	            Component13_op = new SkyEditor2.TextHolder();	
	            	
	            queryMap.put(	
	                'Component2',	
	                new SkyEditor2.Query('task__c')	
	                    .addFieldAsOutput('OwnerId')
	                    .addFieldAsOutput('lead__c')
	                    .addFieldAsOutput('Opportunity__c')
	                    .addFieldAsOutput('Type__c')
	                    .addFieldAsOutput('Time__c')
	                    .addFieldAsOutput('PhaseBig__c')
	                    .addFieldAsOutput('PhaseSmall__c')
	                    .addFieldAsOutput('PhoneA__c')
	                    .addFieldAsOutput('Memo__c')
	                    .addFieldAsOutput('CreatedDate')
	                    .limitRecords(500)	
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component4_from, 'SkyEditor2__Date__c', 'CreatedDate', new SkyEditor2.TextHolder('ge'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component4_to, 'SkyEditor2__Date__c', 'CreatedDate', new SkyEditor2.TextHolder('le'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component4_isNull, 'SkyEditor2__Date__c', 'CreatedDate', Component4_isNull_op, true,0,false )) 
	                    
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component6_from, 'SkyEditor2__Date__c', 'PhoneA__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component6_to, 'SkyEditor2__Date__c', 'PhoneA__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component6_isNull, 'SkyEditor2__Date__c', 'PhoneA__c', Component6_isNull_op, true,0,false )) 
	                    
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component9_val, 'OwnerId', 'OwnerId', Component9_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component11_val, 'Opportunity__c', 'Opportunity__c', Component11_op, true, 0, false ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(Component13_val, 'lead__c', 'lead__c', Component13_op, true, 0, false ))
	            );	
	            	
	                Component2 = new Component2(new List<task__c>(), new List<Component2Item>(), new List<task__c>(), null);
                 Component2.setPageItems(new List<Component2Item>());
                 Component2.setPageSize(50);
	            listItemHolders.put('Component2', Component2);	
	            	
	            	
	            recordTypeSelector = new SkyEditor2.RecordTypeSelector(task__c.SObjectType, true);
	            	
	            	
            p_showHeader = true;
            p_sidebar = true;
            presetSystemParams();
            Component2.extender = this.extender;
	        } catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
	            SkyEditor2.Messages.addErrorMessage(e.getMessage());
	        } catch (SkyEditor2.Errors.FieldNotFoundException e) {	
	            SkyEditor2.Messages.addErrorMessage(e.getMessage());
            } catch (SkyEditor2.ExtenderException e) {                 e.setMessagesToPage();
	        } catch (Exception e) {	
	            System.Debug(LoggingLevel.Error, e);	
	            SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
	        }	
	    }	
	    	
        public List<SelectOption> getOperatorOptions_task_c_OwnerId() { 
            return getOperatorOptions('task__c', 'OwnerId');	
	    }	
        public List<SelectOption> getOperatorOptions_task_c_Opportunity_c() { 
            return getOperatorOptions('task__c', 'Opportunity__c');	
	    }	
        public List<SelectOption> getOperatorOptions_task_c_lead_c() { 
            return getOperatorOptions('task__c', 'lead__c');	
	    }	
	    	
	    private static testMethod void testPageMethods() {	
	        tereapo page = new tereapo(new ApexPages.StandardController(new task__c()));	
	        page.getOperatorOptions_task_c_OwnerId();	
	        page.getOperatorOptions_task_c_Opportunity_c();	
	        page.getOperatorOptions_task_c_lead_c();	
            System.assert(true);
	    }	
	    	
	    	
    global with sharing class Component2Item extends SkyEditor2.ListItem {
        public task__c record{get; private set;}
        Component2Item(Component2 holder, task__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(holder);
            if (record.Id == null ){
                if (recordTypeSelector != null) {
                    recordTypeSelector.applyDefault(record);
                }
                
            }
            this.record = record;
        }
        global override SObject getRecord() {return record;}
        public void doDeleteItem(){deleteItem();}
    }
    global with sharing  class Component2 extends SkyEditor2.PagingList {
        public List<Component2Item> items{get; private set;}
        Component2(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(records, items, deleteRecords, recordTypeSelector);
            this.items = (List<Component2Item>)items;
        }
        global override SkyEditor2.ListItem create(SObject data) {
            return new Component2Item(this, (task__c)data, recordTypeSelector);
        }
        public void doDeleteSelectedItems(){deleteSelectedItems();}
        public void doFirst(){first();}
        public void doPrevious(){previous();}
        public void doNext(){next();}
        public void doLast(){last();}
        public void doSort(){sort();}

        public List<Component2Item> getViewItems() {            return (List<Component2Item>) getPageItems();        }
    }
    private static testMethod void testComponent2() {
        Component2 Component2 = new Component2(new List<task__c>(), new List<Component2Item>(), new List<task__c>(), null);
        Component2.setPageItems(new List<Component2Item>());
        Component2.create(new task__c());
        Component2.doDeleteSelectedItems();
        Component2.setPagesize(10);        Component2.doFirst();
        Component2.doPrevious();
        Component2.doNext();
        Component2.doLast();
        Component2.doSort();
        System.assert(true);
    }
    

	    	
	}