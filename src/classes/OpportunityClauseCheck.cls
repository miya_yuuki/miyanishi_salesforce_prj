public with sharing class OpportunityClauseCheck {
    public static boolean firstRun = true;
    
    public static void OpportunityClauseCheck(List<OpportunityProduct__c> objOpportunityProduct) {
        // 商談商品オブジェクト
        List<OpportunityProduct__c> objOppProduct = new List<OpportunityProduct__c>();
        // 商談ID1
        List<String> strOpportunityIds1 = new List<String>();
        // 商談ID2
        List<String> strOpportunityIds2 = new List<String>();
        // 商談ID3
        List<String> strOpportunityIds3 = new List<String>();
        // 商談ID4
        List<String> strOpportunityIds4 = new List<String>();
        // 商談ID5
        List<String> strOpportunityIds5 = new List<String>();
        // 商談ID6
        List<String> strOpportunityIds6 = new List<String>();
        // 商談ID7
        List<String> strOpportunityIds7 = new List<String>();
        // 商談ID8
        List<String> strOpportunityIds8 = new List<String>();
        // 商談ID9
        List<String> strOpportunityIds9 = new List<String>();
        // 商談ID10
        List<String> strOpportunityIds10 = new List<String>();
        // 商談ID11
        List<String> strOpportunityIds11 = new List<String>();
        // 商談ID12
        List<String> strOpportunityIds12 = new List<String>();
        // 商談ID13
        List<String> strOpportunityIds13 = new List<String>();
        // 商談ID14
        List<String> strOpportunityIds14 = new List<String>();
        // 商談ID15
        List<String> strOpportunityIds15 = new List<String>();
        // 商談ID16
        List<String> strOpportunityIds16 = new List<String>();
        // 商談ID17
        List<String> strOpportunityIds17 = new List<String>();
        // 商談ID18
        List<String> strOpportunityIds18 = new List<String>();
        // 商談ID19
        List<String> strOpportunityIds19 = new List<String>();
        // 商談ID94
        List<String> strOpportunityIds94 = new List<String>();
        // 商談ID95
        List<String> strOpportunityIds95 = new List<String>();
        // 商談ID96
        List<String> strOpportunityIds96 = new List<String>();
        // 商談ID97
        List<String> strOpportunityIds97 = new List<String>();
        // 商談ID98
        List<String> strOpportunityIds98 = new List<String>();
        // 商談ID99
        List<String> strOpportunityIds99 = new List<String>();
        // 商談オブジェクト1
        List<Opportunity> updOpportunity1 = new List<Opportunity>();
        // 商談オブジェクト2
        List<Opportunity> updOpportunity2 = new List<Opportunity>();
        // 商談オブジェクト3
        List<Opportunity> updOpportunity3 = new List<Opportunity>();
        // 商談オブジェクト4
        List<Opportunity> updOpportunity4 = new List<Opportunity>();
        // 商談オブジェクト5
        List<Opportunity> updOpportunity5 = new List<Opportunity>();
        // 商談オブジェクト6
        List<Opportunity> updOpportunity6 = new List<Opportunity>();
        // 商談オブジェクト7
        List<Opportunity> updOpportunity7 = new List<Opportunity>();
        // 商談オブジェクト8
        List<Opportunity> updOpportunity8 = new List<Opportunity>();
        // 商談オブジェクト9
        List<Opportunity> updOpportunity9 = new List<Opportunity>();
        // 商談オブジェクト10
        List<Opportunity> updOpportunity10 = new List<Opportunity>();
        // 商談オブジェクト11
        List<Opportunity> updOpportunity11 = new List<Opportunity>();
        // 商談オブジェクト12
        List<Opportunity> updOpportunity12 = new List<Opportunity>();
        // 商談オブジェクト13
        List<Opportunity> updOpportunity13 = new List<Opportunity>();
        // 商談オブジェクト14
        List<Opportunity> updOpportunity14 = new List<Opportunity>();
        // 商談オブジェクト15
        List<Opportunity> updOpportunity15 = new List<Opportunity>();
        // 商談オブジェクト16
        List<Opportunity> updOpportunity16 = new List<Opportunity>();
        // 商談オブジェクト17
        List<Opportunity> updOpportunity17 = new List<Opportunity>();
        // 商談オブジェクト18
        List<Opportunity> updOpportunity18 = new List<Opportunity>();
        // 商談オブジェクト19
        List<Opportunity> updOpportunity19 = new List<Opportunity>();
        // 商談オブジェクト94
        List<Opportunity> updOpportunity94 = new List<Opportunity>();
        // 商談オブジェクト95
        List<Opportunity> updOpportunity95 = new List<Opportunity>();
        // 商談オブジェクト96
        List<Opportunity> updOpportunity96 = new List<Opportunity>();
        // 商談オブジェクト97
        List<Opportunity> updOpportunity97 = new List<Opportunity>();
        // 商談オブジェクト98
        List<Opportunity> updOpportunity98 = new List<Opportunity>();
        // 商談オブジェクト99
        List<Opportunity> updOpportunity99 = new List<Opportunity>();
        
        
        // 商談商品オブジェクトのループ
        for (OpportunityProduct__c obj : objOpportunityProduct) {
            System.debug('Agreement__c: ' + obj.AgreementView__c + ', Opportunity.Id: ' + obj.OpportunityId__c);
            if (obj.AgreementView__c == null || obj.OpportunityId__c == null) {
                continue;
            }
            if (obj.AgreementView__c == 'コンサルティングサービス約款') {
                strOpportunityIds1.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == 'コンテンツ拡散サービス利用規約') {
                strOpportunityIds2.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == 'レポーティングサービス約款') {
                strOpportunityIds3.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == 'CMS構築サービス約款') {
                strOpportunityIds4.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == 'Webサイト運用代行サービス約款') {
                strOpportunityIds5.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == 'SNSアカウント運用代行サービス約款') {
                strOpportunityIds6.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == 'SNS広告運用代行サービス約款') {
                strOpportunityIds7.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == '外部リンクサービス利用約款') {
                strOpportunityIds8.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == 'アドネット広告運用代行サービス約款') {
                strOpportunityIds9.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == '暮らしニスタ広告約款') {
                strOpportunityIds10.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == 'コンテンツ制作サービス約款') {
                strOpportunityIds11.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == 'サービス基本約款 + EASY ENTRY　利用約款') {
                strOpportunityIds12.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == 'コンテンツ制作サービス約款（その他申込書印刷なし）') {
                strOpportunityIds13.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == 'コンテンツ監修サービス約款') {
                strOpportunityIds14.add(obj.OpportunityId__c);
            }
            /*if (obj.AgreementView__c == 'コンテンツ翻訳サービス約款') {
                strOpportunityIds15.add(obj.OpportunityId__c);
            }*/
            if (obj.AgreementView__c == 'Milly広告約款') {
                strOpportunityIds16.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == 'マーケティングツール「TACT SEO」サービス約款') {
                strOpportunityIds17.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == 'レポーティングサービス約款（その他申込書印刷なし）') {
                strOpportunityIds18.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == 'エディトルサービス約款') {
                strOpportunityIds19.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == '集客改善プラン + TACT SEO') {
                strOpportunityIds94.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == 'コンサルティングプラン + TACT SEO') {
                strOpportunityIds95.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == 'コンサルティングサービス約款 + 「Front Desk」サービス約款') {
                strOpportunityIds96.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == '専門家監修コンテンツ') {
                strOpportunityIds97.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == 'コンテンツ制作サービス約款 + レポーティングサービス約款') {
                strOpportunityIds98.add(obj.OpportunityId__c);
            }
            if (obj.AgreementView__c == '集客改善プラン' || obj.AgreementView__c == 'キーワード最適化プラン') {
                strOpportunityIds99.add(obj.OpportunityId__c);
            }
        }
        
        if (strOpportunityIds1.size() > 0) {
            updOpportunity1 = [
                SELECT Id, Agreement1__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds1
            ];
            for (Opportunity obj: updOpportunity1) {
                obj.Agreement1__c = true;
            }
            update updOpportunity1;
        }
        if (strOpportunityIds2.size() > 0) {
            updOpportunity2 = [
                SELECT Id, Agreement2__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds2
            ];
            for (Opportunity obj: updOpportunity2) {
                obj.Agreement2__c = true;
            }
            update updOpportunity2;
        }
        if (strOpportunityIds3.size() > 0) {
            updOpportunity3 = [
                SELECT Id, Agreement3__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds3
            ];
            for (Opportunity obj: updOpportunity3) {
                obj.Agreement3__c = true;
            }
            update updOpportunity3;
        }
        if (strOpportunityIds4.size() > 0) {
            updOpportunity4 = [
                SELECT Id, Agreement4__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds4
            ];
            for (Opportunity obj: updOpportunity4) {
                obj.Agreement4__c = true;
            }
            update updOpportunity4;
        }
        if (strOpportunityIds5.size() > 0) {
            updOpportunity5 = [
                SELECT Id, Agreement5__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds5
            ];
            for (Opportunity obj: updOpportunity5) {
                obj.Agreement5__c = true;
            }
            update updOpportunity5;
        }
        if (strOpportunityIds6.size() > 0) {
            updOpportunity6 = [
                SELECT Id, Agreement6__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds6
            ];
            for (Opportunity obj: updOpportunity6) {
                obj.Agreement6__c = true;
            }
            update updOpportunity6;
        }
        if (strOpportunityIds7.size() > 0) {
            updOpportunity7 = [
                SELECT Id, Agreement7__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds7
            ];
            for (Opportunity obj: updOpportunity7) {
                obj.Agreement7__c = true;
            }
            update updOpportunity7;
        }
        if (strOpportunityIds8.size() > 0) {
            updOpportunity8 = [
                SELECT Id, Agreement8__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds8
            ];
            for (Opportunity obj: updOpportunity8) {
                obj.Agreement8__c = true;
            }
            update updOpportunity8;
        }
        // アドネット広告運用代行サービス約款
        if (strOpportunityIds9.size() > 0) {
            updOpportunity9 = [
                SELECT Id, Agreement9__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds9
            ];
            for (Opportunity obj: updOpportunity9) {
                obj.Agreement9__c = true;
            }
            update updOpportunity9;
        }
        if (strOpportunityIds10.size() > 0) {
            updOpportunity10 = [
                SELECT Id, Agreement10__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds10
            ];
            for (Opportunity obj: updOpportunity10) {
                obj.Agreement10__c = true;
            }
            update updOpportunity10;
        }
        if (strOpportunityIds11.size() > 0) {
            updOpportunity11 = [
                SELECT Id, Agreement11__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds11
            ];
            for (Opportunity obj: updOpportunity11) {
                obj.Agreement11__c = true;
            }
            update updOpportunity11;
        }
        if (strOpportunityIds12.size() > 0) {
            updOpportunity12 = [
                SELECT Id, Agreement12__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds12
            ];
            for (Opportunity obj: updOpportunity12) {
                obj.Agreement12__c = true;
            }
            update updOpportunity12;
        }
        // コンテンツ制作サービス約款（その他申込書印刷なし）
        if (strOpportunityIds13.size() > 0) {
            updOpportunity13 = [
                SELECT Id, Agreement14__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds13
            ];
            for (Opportunity obj: updOpportunity13) {
                obj.Agreement14__c = true;
            }
            update updOpportunity13;
        }
        // 専門家監修
        if (strOpportunityIds14.size() > 0) {
            updOpportunity14 = [
                SELECT Id, Agreement15__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds14
            ];
            for (Opportunity obj: updOpportunity14) {
                obj.Agreement15__c = true;
            }
            update updOpportunity14;
        }
        /*if (strOpportunityIds15.size() > 0) {
            updOpportunity15 = [
                SELECT Id, Agreement16__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds15
            ];
            for (Opportunity obj: updOpportunity15) {
                obj.Agreement16__c = true;
            }
            update updOpportunity15;
        }*/
        // Milly広告約款
        if (strOpportunityIds16.size() > 0) {
            updOpportunity16 = [
                SELECT Id, Agreement17__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds16
            ];
            for (Opportunity obj: updOpportunity16) {
                obj.Agreement17__c = true;
            }
            update updOpportunity16;
        }
        // マーケティングツール「TACT SEO」サービス約款
        if (strOpportunityIds17.size() > 0) {
            updOpportunity17 = [
                SELECT Id, Agreement18__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds17
            ];
            for (Opportunity obj: updOpportunity17) {
                obj.Agreement18__c = true;
            }
            update updOpportunity17;
        }
        // レポーティングサービス約款（その他申込書印刷なし）
        if (strOpportunityIds18.size() > 0) {
            updOpportunity18 = [
                SELECT Id, Agreement13__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds18
            ];
            for (Opportunity obj: updOpportunity18) {
                obj.Agreement13__c = true;
            }
            update updOpportunity18;
        }
        // エディトルサービス約款
        if (strOpportunityIds19.size() > 0) {
            updOpportunity19 = [
                SELECT Id, Agreement20__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds19
            ];
            for (Opportunity obj: updOpportunity19) {
                obj.Agreement20__c = true;
            }
            update updOpportunity19;
        }
        // 集客改善プラン + TACT SEO
        if (strOpportunityIds94.size() > 0) {
            updOpportunity94 = [
                SELECT Id, Agreement1__c, Agreement8__c, Agreement18__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds94
            ];
            for (Opportunity obj: updOpportunity94) {
                obj.Agreement1__c = true;
                obj.Agreement8__c = true;
                obj.Agreement18__c = true;
            }
            update updOpportunity94;
        }
        // コンサルティングプラン + TACT SEO
        if (strOpportunityIds95.size() > 0) {
            updOpportunity95 = [
                SELECT Id, Agreement1__c, Agreement18__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds95
            ];
            for (Opportunity obj: updOpportunity95) {
                obj.Agreement1__c = true;
                obj.Agreement18__c = true;
            }
            update updOpportunity95;
        }
        // コンサルティングサービス約款 + 「Front Desk」サービス約款
        if (strOpportunityIds96.size() > 0) {
            updOpportunity96 = [
                SELECT Id, Agreement1__c, Agreement19__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds96
            ];
            for (Opportunity obj: updOpportunity96) {
                obj.Agreement1__c = true;
                obj.Agreement19__c = true;
            }
            update updOpportunity96;
        }
        // 専門家監修コンテンツ
        if (strOpportunityIds97.size() > 0) {
            updOpportunity97 = [
                SELECT Id, Agreement14__c, Agreement15__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds97
            ];
            for (Opportunity obj: updOpportunity97) {
                obj.Agreement14__c = true;
                obj.Agreement15__c = true;
            }
            update updOpportunity97;
        }
        // コンテンツ制作サービス約款 + レポーティングサービス約款
        if (strOpportunityIds98.size() > 0) {
            updOpportunity98 = [
                SELECT Id, Agreement13__c, Agreement14__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds98
            ];
            for (Opportunity obj: updOpportunity98) {
                obj.Agreement13__c = true;
                obj.Agreement14__c = true;
            }
            update updOpportunity98;
        }
        // 集客改善プラン・キーワード最適化プラン
        if (strOpportunityIds99.size() > 0) {
            updOpportunity99 = [
                SELECT Id, Agreement1__c, Agreement8__c
                FROM Opportunity
                WHERE Id IN :strOpportunityIds99
            ];
            for (Opportunity obj: updOpportunity99) {
                obj.Agreement1__c = true;
                obj.Agreement8__c = true;
            }
            update updOpportunity99;
        }
    }
}