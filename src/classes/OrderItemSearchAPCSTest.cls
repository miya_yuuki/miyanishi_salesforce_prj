@isTest
private with sharing class OrderItemSearchAPCSTest{
		private static testMethod void testPageMethods() {	
			OrderItemSearchAPCS page = new OrderItemSearchAPCS(new ApexPages.StandardController(new OrderItem__c()));	
			page.getOperatorOptions_OrderItem_c_Account_c();	
			page.getOperatorOptions_OrderItem_c_Item2_Product_naiyou_c();	
			page.getOperatorOptions_OrderItem_c_SalesPerson1_c();	
			page.getOperatorOptions_OrderItem_c_Item2_Keyword_strategy_keyword_c();	
			page.getOperatorOptions_OrderItem_c_DeliveryPerson_c();	
			page.getOperatorOptions_OrderItem_c_OrderItem_keywordType_c_multi();	
			page.getOperatorOptions_OrderItem_c_Item2_DeliveryStandards_c();	
			page.getOperatorOptions_OrderItem_c_Item2_Keyword_phase_c_multi();	
			page.getOperatorOptions_OrderItem_c_Item2_Product_r_ClassificationSales_c();	
			page.getOperatorOptions_OrderItem_c_ProductionStatus_c();	
			page.getOperatorOptions_OrderItem_c_Item2_Automatic_updating1_c();	
			page.getOperatorOptions_OrderItem_c_ServiceDate_c();	
			page.getOperatorOptions_OrderItem_c_Item2_ServiceDate_c();	
			page.getOperatorOptions_OrderItem_c_EndDate_c();	
			page.getOperatorOptions_OrderItem_c_Item2_EndData_c();	
			page.getOperatorOptions_OrderItem_c_Bill_Main_c();	
			page.getOperatorOptions_OrderItem_c_Item2_CancellationDay_c();	
			page.getOperatorOptions_OrderItem_c_Item2_Relation_c();	
			page.getOperatorOptions_OrderItem_c_RecordTypeId();	
			System.assert(true);
		}	
			
	private static testMethod void testComponent2() {
		OrderItemSearchAPCS.Component2 Component2 = new OrderItemSearchAPCS.Component2(new List<OrderItem__c>(), new List<OrderItemSearchAPCS.Component2Item>(), new List<OrderItem__c>(), null, null);
		Component2.create(new OrderItem__c());
		Component2.doDeleteSelectedItems();
		Component2.setPagesize(10);		Component2.doFirst();
		Component2.doPrevious();
		Component2.doNext();
		Component2.doLast();
		Component2.doSort();
		System.assert(true);
	}
	
}