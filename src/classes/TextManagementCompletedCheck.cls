global class TextManagementCompletedCheck {
	global static PageReference prcCompletedCheck() {
		// 納品管理オブジェクト
		List<TextManagement2__c> updTextManagement = new List<TextManagement2__c>();
		
		updTextManagement = [
			SELECT Id
			FROM TextManagement2__c
			WHERE CloudStatus__c = '社外納品済み'
			AND Item2_DeliveryExistence__c = '有'
			AND OrderItem_PaymentDueDate__c = null
			AND OrderItem_DeliveryCountTotal__c >= 0
			AND ProductSeparateUMU__c = '無'
			AND OrderItemStatus__c NOT IN ('契約変更', '途中解約', '請求停止')
			AND (NOT Product__c like '%社内発注%')
			AND (NOT Account1__c like '%テスト用%')
			AND CreatedDate > 2015-08-31T10:00:00+09:00
			limit 300
		];
		
		if (updTextManagement.size() > 0) {
			update updTextManagement;
			System.debug('納品管理：更新対象' + updTextManagement.size() + '件');
		}
		else {
			System.debug('納品管理：更新対象なし');
		}
		
		// 完了メッセージ
		String resultText = '納品管理：更新対象 ' + updTextManagement.size() + '件';
		/*try {
			if(ApexPages.currentPage() != null) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, resultText));
			}
			else {
				System.debug(resultText);
			}
		}
		catch (Exception ex) {*/
			System.debug(resultText);
		//}
		
		return null;
	}
}