@isTest
private with sharing class syoudan_ContactPerson_viewTest{
	private static testMethod void testPageMethods() {		syoudan_ContactPerson_view extension = new syoudan_ContactPerson_view(new ApexPages.StandardController(new Opportunity()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testComponent88() {
		syoudan_ContactPerson_view.Component88 Component88 = new syoudan_ContactPerson_view.Component88(new List<AccountCustomer__c>(), new List<syoudan_ContactPerson_view.Component88Item>(), new List<AccountCustomer__c>(), null);
		Component88.create(new AccountCustomer__c());
		System.assert(true);
	}
	
	@isTest
	private static void testLightDataTables(){

		System.assert(true);
	}

	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component117() {
		String testReferenceId = '';
		syoudan_ContactPerson_view.Component88 table = new syoudan_ContactPerson_view.Component88(new List<AccountCustomer__c>(), new List<syoudan_ContactPerson_view.Component88Item>(), new List<AccountCustomer__c>(), null);
		table.add();
		syoudan_ContactPerson_view.Component88Item item = table.items[0];
		item.loadReferenceValues_Component117();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(Contact.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<Contact> parents = [SELECT Id FROM Contact LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		Contact parent = [SELECT Id,Department,Title,Contact_Role__c,Email FROM Contact WHERE Id = :testReferenceId];
		item.record.CustomerName__c = parent.Id;
		item.loadReferenceValues_Component117();
				
		if (SkyEditor2.Util.isEditable(item.record, AccountCustomer__c.fields.department__c)) {
			System.assertEquals(parent.Department, item.record.department__c);
		}


		if (SkyEditor2.Util.isEditable(item.record, AccountCustomer__c.fields.Title__c)) {
			System.assertEquals(parent.Title, item.record.Title__c);
		}


		if (SkyEditor2.Util.isEditable(item.record, AccountCustomer__c.fields.yakuwari__c)) {
			System.assertEquals(parent.Contact_Role__c, item.record.yakuwari__c);
		}


		if (SkyEditor2.Util.isEditable(item.record, AccountCustomer__c.fields.Mail__c)) {
			System.assertEquals(parent.Email, item.record.Mail__c);
		}

		System.assert(true);
	}
}