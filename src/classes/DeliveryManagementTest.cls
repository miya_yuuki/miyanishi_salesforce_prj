@isTest
private with sharing class DeliveryManagementTest{
		private static testMethod void testPageMethods() {	
			DeliveryManagement page = new DeliveryManagement(new ApexPages.StandardController(new TextManagement2__c()));	
			page.getOperatorOptions_TextManagement2_c_flagPlatinum_c();	
			page.getOperatorOptions_TextManagement2_c_flagQuestionnaire_c();	
			page.getOperatorOptions_TextManagement2_c_flagLighting_c();	
			page.getOperatorOptions_TextManagement2_c_flagExpert_c();	
			page.getOperatorOptions_TextManagement2_c_Account1_c();	
			page.getOperatorOptions_TextManagement2_c_Product_c();	
			page.getOperatorOptions_TextManagement2_c_Status_c();	
			page.getOperatorOptions_TextManagement2_c_CloudStatus_c_multi();	
			page.getOperatorOptions_TextManagement2_c_SalesPerson10_c();	
			page.getOperatorOptions_TextManagement2_c_Text_Accep_TedNorm_c();	
			page.getOperatorOptions_TextManagement2_c_GroupID_c();	
			page.getOperatorOptions_TextManagement2_c_Name();	
			System.assert(true);
		}	
			
	private static testMethod void testComponent2() {
		DeliveryManagement.Component2 Component2 = new DeliveryManagement.Component2(new List<TextManagement2__c>(), new List<DeliveryManagement.Component2Item>(), new List<TextManagement2__c>(), null, null);
		Component2.create(new TextManagement2__c());
		Component2.doDeleteSelectedItems();
		Component2.setPagesize(10);		Component2.doFirst();
		Component2.doPrevious();
		Component2.doNext();
		Component2.doLast();
		Component2.doSort();
		System.assert(true);
	}
	
}