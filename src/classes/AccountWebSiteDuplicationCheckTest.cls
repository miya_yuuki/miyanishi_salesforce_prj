@isTest
public with sharing class AccountWebSiteDuplicationCheckTest {
	private static testMethod void testMethods() {
        Account account1 = new Account(
            Name = '小島商事_テスト用'
        );
        insert account1;
        AccountWebSite__c newAccountWebSite1 = new AccountWebSite__c(
            Name = '小島商事_テスト用（hogehoge.com）',
            Account__c = account1.Id,
            WEBsite_URL__c = 'https://hogehoge.com'
        );
        insert newAccountWebSite1;

        AccountWebSite__c newAccountWebSite2 = new AccountWebSite__c(
            Name = '小島商事_テスト用（fugafuga.com）',
            Account__c = account1.Id,
            WEBsite_URL__c = 'https://fugafuga.com'
        );
        insert newAccountWebSite2;

        newAccountWebSite2.WEBsite_URL__c = 'https://hoge.com';
        update newAccountWebSite2;
    }
}