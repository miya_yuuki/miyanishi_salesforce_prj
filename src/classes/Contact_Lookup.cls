global with sharing class Contact_Lookup extends SkyEditor2.SkyEditorPageBaseWithSharing{
    
    public Contact record{get;set;}
    public Component2 Component2 {get; private set;}
    public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
    public SkyEditor2__SkyEditorDummy__c Component3{get;set;}
    public List<SelectOption> Component3_options {get;set;}
    public SkyEditor2__SkyEditorDummy__c Component6{get;set;}
    public SkyEditor2__SkyEditorDummy__c Component9{get;set;}
    public SkyEditor2__SkyEditorDummy__c Component12{get;set;}
    public SkyEditor2__SkyEditorDummy__c Component15{get;set;}
    public Contact_Lookup(ApexPages.StandardController controller){
        super(controller);

        SObjectField f;
        f = Contact.fields.Name;
        f = Contact.fields.Contact_kana__c;
        f = Contact.fields.Department;
        f = Contact.fields.Title;
        f = Contact.fields.Contact_Role__c;
        f = Contact.fields.AccountId;
        f = Contact.fields.Email;
        f = Contact.fields.Phone;
        f = Contact.fields.Contact_Phase__c;

        try {
            mainRecord = null;
            mainSObjectType = Contact.SObjectType;
            mode = SkyEditor2.LayoutMode.TempProductLookup_01;
            
            Component3 = new SkyEditor2__SkyEditorDummy__c();
            Component6 = new SkyEditor2__SkyEditorDummy__c();
            Component9 = new SkyEditor2__SkyEditorDummy__c();
            Component12 = new SkyEditor2__SkyEditorDummy__c();
            Component15 = new SkyEditor2__SkyEditorDummy__c();
            
            queryMap.put(
                'Component2',
                new SkyEditor2.Query('Contact')
                    .addFieldAsOutput('Name')
                    .addFieldAsOutput('Contact_kana__c')
                    .addFieldAsOutput('Contact_Role__c')
                    .addFieldAsOutput('AccountId')
                    .addFieldAsOutput('Department')
                    .addFieldAsOutput('Title')
                    .addFieldAsOutput('Email')
                    .addFieldAsOutput('Phone')
                    .addField('Name')
                    .limitRecords(500)
                    .addListener(new SkyEditor2.QueryWhereRegister(Component3, 'SkyEditor2__Text__c', 'Contact_Phase__c', new SkyEditor2.TextHolder('eq'), false, true, false))
                    .addListener(new SkyEditor2.QueryWhereRegister(Component6, 'SkyEditor2__Text__c', 'Name', new SkyEditor2.TextHolder('co'), false, true, false))
                    .addListener(new SkyEditor2.QueryWhereRegister(Component9, 'SkyEditor2__Text__c', 'Contact_kana__c', new SkyEditor2.TextHolder('co'), false, true, false))
                    .addListener(new SkyEditor2.QueryWhereRegister(Component12, 'SkyEditor2__Text__c', 'Department', new SkyEditor2.TextHolder('co'), false, true, false))
                    .addListener(new SkyEditor2.QueryWhereRegister(Component15, 'SkyEditor2__Text__c', 'Title', new SkyEditor2.TextHolder('co'), false, true, false))
            );
            
            Component2 = new Component2(new List<Contact>(), new List<Component2Item>(), new List<Contact>(), null);
            listItemHolders.put('Component2', Component2);
            
            recordTypeSelector = new SkyEditor2.RecordTypeSelector(Contact.SObjectType);
            
            p_showHeader = false;
            p_sidebar = false;
            presetSystemParams();
            update_Component3_options();
            initSearch();
            
        } catch (SkyEditor2.Errors.SObjectNotFoundException e) {
            SkyEditor2.Messages.addErrorMessage(e.getMessage());
        } catch (SkyEditor2.Errors.FieldNotFoundException e) {
            SkyEditor2.Messages.addErrorMessage(e.getMessage());
        } catch (SkyEditor2.ExtenderException e){
            e.setMessagesToPage();
        } catch (SkyEditor2.Errors.PricebookNotFoundException e) {
            SkyEditor2.Messages.addErrorMessage(e.getMessage());
            hidePageBody = true;
        }
    }

    public List<SelectOption> getOperatorOptions_Contact_Name() {
        return getOperatorOptions('Contact', 'Name');
    }
    public List<SelectOption> getOperatorOptions_Contact_Contact_kana_c() {
        return getOperatorOptions('Contact', 'Contact_kana__c');
    }
    public List<SelectOption> getOperatorOptions_Contact_Department() {
        return getOperatorOptions('Contact', 'Department');
    }
    public List<SelectOption> getOperatorOptions_Contact_Title() {
        return getOperatorOptions('Contact', 'Title');
    }
    

    @isTest(SeeAllData=true)
    private static void testPageMethods() {
        Contact_Lookup page;
        page = new Contact_Lookup(new ApexPages.StandardController(new Contact()));
        page.getOperatorOptions_Contact_Name();
        page.getOperatorOptions_Contact_Contact_kana_c();
        page.getOperatorOptions_Contact_Department();
        page.getOperatorOptions_Contact_Title();
		System.assert(true);
    }
    
    global with sharing class Component2Item extends SkyEditor2.ListItem {
        public Contact record{get; private set;}
        Component2Item(Component2 holder, Contact record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(holder);
            if (record.Id == null ){
                if (recordTypeSelector != null) {
                    recordTypeSelector.applyDefault(record);
                }
                
            }
            this.record = record;
        }
        global override SObject getRecord() {return record;}
        public void doDeleteItem(){deleteItem();}
    }
    global with sharing  class Component2 extends SkyEditor2.ListItemHolder {
        public List<Component2Item> items{get; private set;}
        Component2(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(records, items, deleteRecords, recordTypeSelector);
            this.items = (List<Component2Item>)items;
        }
        global override SkyEditor2.ListItem create(SObject data) {
            return new Component2Item(this, (Contact)data, recordTypeSelector);
        }
        public void doDeleteSelectedItems(){deleteSelectedItems();}
    }
    private static testMethod void testComponent2() {
        Component2 Component2 = new Component2(new List<Contact>(), new List<Component2Item>(), new List<Contact>(), null);
        Component2.create(new Contact());
        Component2.doDeleteSelectedItems();
        System.assert(true);
    }
    

    
    public void update_Component3_options() {
        Component3_options = new SelectOption[]{ new SelectOption('', label.none) };
            SObject[] results;
            String soql = 'SELECT Contact_Phase__c FROM Contact WHERE Contact_Phase__c != null ';
            soql += 'GROUP BY Contact_Phase__c ORDER BY Contact_Phase__c LIMIT 999';
            results = Database.query(soql);
            for (SObject r : results) {
                String value = (String)(r.get('Contact_Phase__c'));
                Component3_options.add(new SelectOption(value, value));
            }
    }
    
}