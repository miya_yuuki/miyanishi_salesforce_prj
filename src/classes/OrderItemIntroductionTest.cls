@isTest
private with sharing class OrderItemIntroductionTest{
	private static testMethod void testPageMethods() {		OrderItemIntroduction extension = new OrderItemIntroduction(new ApexPages.StandardController(new OrderItem__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}

	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component182() {
		String testReferenceId = '';
		OrderItemIntroduction extension = new OrderItemIntroduction(new ApexPages.StandardController(new OrderItem__c()));
		extension.loadReferenceValues_Component182();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(Product__c.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<Product__c> parents = [SELECT Id FROM Product__c LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		Product__c parent = [SELECT Id,ProductDetails__c FROM Product__c WHERE Id = :testReferenceId];
		extension.record.Item2_Product__c = parent.Id;
		extension.loadReferenceValues_Component182();
				
		if (SkyEditor2.Util.isEditable(extension.record, OrderItem__c.fields.Item2_Detail_Productlist__c)) {
			System.assertEquals(parent.ProductDetails__c, extension.record.Item2_Detail_Productlist__c);
		}

		System.assert(true);
	}
}