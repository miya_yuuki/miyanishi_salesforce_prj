@isTest
private with sharing class RequestKeywordURL_viewTest{
	private static testMethod void testPageMethods() {		RequestKeywordURL_view extension = new RequestKeywordURL_view(new ApexPages.StandardController(new CustomObject1__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testComponent256() {
		RequestKeywordURL_view.Component256 Component256 = new RequestKeywordURL_view.Component256(new List<SEOChange__c>(), new List<RequestKeywordURL_view.Component256Item>(), new List<SEOChange__c>(), null);
		Component256.create(new SEOChange__c());
		System.assert(true);
	}
	
}