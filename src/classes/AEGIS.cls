global with sharing class AEGIS extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public AEGIS__c record {get{return (AEGIS__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	
	
	public Component76 Component76 {get; private set;}
	public Component50 Component50 {get; private set;}
	{
	setApiVersion(31.0);
	}
	public AEGIS(ApexPages.StandardController controller) {
		super(controller);


		SObjectField f;

		f = AEGIS__c.fields.karute__c;
		f = AEGIS__c.fields.Company__c;
		f = AEGIS__c.fields.URL__c;
		f = AEGIS__c.fields.AccountID__c;
		f = AEGIS__c.fields.EF_URL__c;
		f = AEGIS__c.fields.Keyword__c;
		f = AEGIS__c.fields.BrandKeyword__c;
		f = AEGIS__c.fields.ViewName__c;
		f = AEGIS__c.fields.CVReport1__c;
		f = AEGIS__c.fields.CVFoam__c;
		f = AEGIS__c.fields.DirectoryURL__c;
		f = Landing__c.fields.URLText__c;
		f = Landing__c.fields.Keyword__c;
		f = AEGIS2__c.fields.Seat__c;
		f = AEGIS2__c.fields.Type01__c;
		f = AEGIS2__c.fields.Input__c;
		f = AEGIS2__c.fields.MuchType__c;
		f = AEGIS2__c.fields.Select__c;
		f = AEGIS2__c.fields.AEGIS__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = AEGIS__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('AEGIS__c');
			mainQuery.addField('karute__c');
			mainQuery.addField('Company__c');
			mainQuery.addField('URL__c');
			mainQuery.addField('AccountID__c');
			mainQuery.addField('EF_URL__c');
			mainQuery.addField('Keyword__c');
			mainQuery.addField('BrandKeyword__c');
			mainQuery.addField('ViewName__c');
			mainQuery.addField('CVReport1__c');
			mainQuery.addField('CVFoam__c');
			mainQuery.addField('DirectoryURL__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			Component76 = new Component76(new List<Landing__c>(), new List<Component76Item>(), new List<Landing__c>(), null);
			listItemHolders.put('Component76', Component76);
			query = new SkyEditor2.Query('Landing__c');
			query.addField('URLText__c');
			query.addField('Keyword__c');
			query.addWhere('AEGIS__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('Component76', 'AEGIS__c');
			Component76.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('Component76', query);
			
			Component50 = new Component50(new List<AEGIS2__c>(), new List<Component50Item>(), new List<AEGIS2__c>(), null);
			listItemHolders.put('Component50', Component50);
			query = new SkyEditor2.Query('AEGIS2__c');
			query.addField('Seat__c');
			query.addField('Type01__c');
			query.addField('Input__c');
			query.addField('MuchType__c');
			query.addField('Select__c');
			query.addFieldAsOutput('AEGIS__c');
			query.addWhere('AEGIS__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('Component50', 'AEGIS__c');
			Component50.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('Component50', query);
			
			registRelatedList('AEGISLanding__r', 'Component76');
			registRelatedList('AEGIS__r', 'Component50');
			
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('karute__c', 'KarteId');
			init();
			
			Component76.extender = this.extender;
			Component50.extender = this.extender;
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class Component76Item extends SkyEditor2.ListItem {
		public Landing__c record{get; private set;}
		@TestVisible
		Component76Item(Component76 holder, Landing__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component76 extends SkyEditor2.ListItemHolder {
		public List<Component76Item> items{get; private set;}
		@TestVisible
			Component76(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component76Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component76Item(this, (Landing__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	global with sharing class Component50Item extends SkyEditor2.ListItem {
		public AEGIS2__c record{get; private set;}
		@TestVisible
		Component50Item(Component50 holder, AEGIS2__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component50 extends SkyEditor2.ListItemHolder {
		public List<Component50Item> items{get; private set;}
		@TestVisible
			Component50(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component50Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component50Item(this, (AEGIS2__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	public Landing__c Component76_table_Conversion { get { return new Landing__c();}}
	
	public String Component76_table_selectval { get; set; }
	
	public AEGIS2__c Component50_table_Conversion { get { return new AEGIS2__c();}}
	
	public String Component50_table_selectval { get; set; }
	
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}