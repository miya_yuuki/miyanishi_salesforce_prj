@IsTest
public with sharing class CancellationRecordsCheckTest {
    private static testMethod void testMethods()  {
		// プロセスビルダー、入力チェックを停止
		User stopCheck = [SELECT Id, StopPB__c, StopInputCheck__c FROM User WHERE Id = :Userinfo.getUserId()];
		// 値を更新
		if (stopCheck.StopPB__c == false || stopCheck.StopInputCheck__c == false) {
			stopCheck.StopPB__c = true;
			stopCheck.StopInputCheck__c = true;
			update stopCheck;
		}
		
		List<Account> testAccounts = new List<Account>();
		
		// 取引先
		Account testAccount = new Account(
			Name = 'テスト株式会社',
			account_localName__c = 'テストフリガナ',
			account_Lead__c = '架電',
			account_Lead2__c = '架電',
			Type = '顧客'
		);
		insert testAccount;
		
		// 取引先責任者
		Contact testContact = new Contact(
			FirstName = 'テスト姓',
			LastName = 'テスト名',
			AccountId = testAccount.Id,
			report__c = '送信不要'
		);
		insert testContact;
		
		// 商談
		Opportunity testOpp = new Opportunity(
			Name = 'テスト商談',
			OwnerId = Userinfo.getUserId(),
			AccountId = testAccount.Id,
			Opportunity_Title__c = testContact.Id,
			Opportunity_lead1__c = '架電',
			Opportunity_lead2__c = '架電',
			CloseDate = System.today().addDays(5),
			StageName = 'D（検討段階）',
			Opportunity_BillEmail__c = 'test@test.com',
			Opportunity_BillRole__c = '役割',
			Opportunity_BillTitle__c = '役職',
			Opportunity_BillPostalCode__c = '999-9999',
			Opportunity_BillPrefecture__c = '都道府県',
			Opportunity_BillPrefecture2__c = '都道府県',
			Opportunity_BillPhone__c = '03-999-9999',
			Opportunity_BillAddress__c = '住所',
			Opportunity_BillCity__c = '市区群'
		);
		insert testOpp;
		
		// 契約
		Contract testContract = new Contract(
			AccountId = testAccount.Id,
			Opportunity__c = testOpp.Id,
			Contract_SalesPerson__c = [SELECT Id FROM User WHERE Id = :Userinfo.getUserId()].Id,
			Contract_Support__c = [SELECT Id FROM User WHERE Id = :Userinfo.getUserId()].Id,
			Phase__c = '契約中',
			Status = 'ドラフト',
			StartDate = System.today().addDays(5),
			ContractTerm = 6
		);
		insert testContract;
		
		// 支払いサイト
		PaymentSite__c testPayment = new PaymentSite__c(
			Name = '月末締め翌々月末払い',
			fast__c = '月末締め',
			second__c = '翌々月',
			Third__c = '末払い'
		);
		insert testPayment;
		
		// 注文
		Order__c testOrder = new Order__c(
			Order_Relation__c = testContract.Id,
			Order_Phase__c = '契約中',
			Order_PaymentMethod__c = '振り込み【通常】',
			Order_PaymentSiteMaster__c = testPayment.Id
		);
        insert testOrder;

		// 注文商品
		OrderItem__c testOrderItem = new OrderItem__c(
            Item2_Relation__c = testOrder.Id,
            BillingTiming__c = '受注',
            ContractMonths__c = 6,
            ChangeDay__c = Date.Today()
        );
		insert testOrderItem;
		
        // 申請（案件）
        CustomObject1__c testRequest = new CustomObject1__c(
            RecordTypeId = '01210000000AQRB',
            OrderNo__c = testOrder.Id,
            CancelType2__c = '途中解約',
            CancelMonthBilling__c = '無'
        );
        insert testRequest;
    }
}