global with sharing class LEX_EasyOpportunity_view extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public Opportunity record {get{return (Opportunity)mainRecord;}}
	{
	setApiVersion(42.0);
	}
	public LEX_EasyOpportunity_view(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = Opportunity.fields.Name;
		f = Opportunity.fields.OpportunityCopy__c;
		f = Opportunity.fields.AccountId;
		f = Opportunity.fields.Opportunity_Type__c;
		f = Opportunity.fields.BoardDiscussed__c;
		f = Opportunity.fields.Opportunity_Partner__c;
		f = Opportunity.fields.Opportunity_endClient1__c;
		f = Opportunity.fields.Opportunity_Competition__c;
		f = Opportunity.fields.OwnerId;
		f = Opportunity.fields.Opportunity_Support__c;
		f = Opportunity.fields.Opportunity_FastOrdersSales__c;
		f = Opportunity.fields.account_web_site__c;
		f = Opportunity.fields.account_web_site_unnecessary__c;
		f = Opportunity.fields.ServiceWEBSite__c;
		f = Opportunity.fields.ServiceWEBSite2__c;
		f = Opportunity.fields.AmountTotal__c;
		f = Opportunity.fields.CreditDecisionPrice__c;
		f = Opportunity.fields.CloseDate;
		f = Opportunity.fields.StageName;
		f = Opportunity.fields.karute__c;
		f = Opportunity.fields.karuteCheck__c;
		f = Opportunity.fields.karutenew__c;
		f = Opportunity.fields.AppointmentDay__c;
		f = Opportunity.fields.Opportunity_lead1__c;
		f = Opportunity.fields.Opportunity_lead2__c;
		f = Opportunity.fields.CampaignId;
		f = Opportunity.fields.not_campaign_flg__c;
		f = Opportunity.fields.Opportunity_BillingMethod__c;
		f = Opportunity.fields.CaseApproval__c;
		f = Opportunity.fields.Opportunity_Title__c;
		f = Opportunity.fields.Opportunity_PaymentSiteMaster__c;
		f = Opportunity.fields.Opportunity_BillKana__c;
		f = Opportunity.fields.Opportunity_PaymentMethod__c;
		f = Opportunity.fields.AddressCheck__c;
		f = Opportunity.fields.Opportunity_Memo__c;
		f = Opportunity.fields.CustomerNamePRINT__c;
		f = Opportunity.fields.QuoteExpirationDate__c;
		f = Opportunity.fields.Opportunity_EstimateMemo__c;
		f = Opportunity.fields.OrderProvisional__c;
		f = Opportunity.fields.OrderProvisionalMemo__c;
		f = Opportunity.fields.SalvageScheduledDate__c;
		f = Opportunity.fields.opportunityBillMemo__c;
		f = Opportunity.fields.notices__c;
		f = Opportunity.fields.Agreement1__c;
		f = Opportunity.fields.Agreement2__c;
		f = Opportunity.fields.Agreement3__c;
		f = Opportunity.fields.Agreement4__c;
		f = Opportunity.fields.Agreement5__c;
		f = Opportunity.fields.Agreement6__c;
		f = Opportunity.fields.Agreement7__c;
		f = Opportunity.fields.Agreement8__c;
		f = Opportunity.fields.Agreement10__c;
		f = Opportunity.fields.Agreement11__c;
		f = Opportunity.fields.Agreement12__c;
		f = Opportunity.fields.Agreement17__c;
		f = Opportunity.fields.Agreement18__c;
		f = Opportunity.fields.Agreement19__c;
		f = Opportunity.fields.Agreement20__c;
		f = Opportunity.fields.Agreement13__c;
		f = Opportunity.fields.Agreement14__c;
		f = Opportunity.fields.Agreement15__c;
		f = Opportunity.fields.CreatedById;
		f = Opportunity.fields.LastModifiedById;
		f = Opportunity.fields.syouninn__c;
		f = Opportunity.fields.CreatedDate;
		f = Opportunity.fields.LastModifiedDate;
		f = Opportunity.fields.Salesforce_ID__c;
		f = Opportunity.fields.Agreement9__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = Opportunity.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'LEX_EasyOpportunity_view';
			mainQuery = new SkyEditor2.Query('Opportunity');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('OpportunityCopy__c');
			mainQuery.addFieldAsOutput('AccountId');
			mainQuery.addFieldAsOutput('Opportunity_Type__c');
			mainQuery.addFieldAsOutput('BoardDiscussed__c');
			mainQuery.addFieldAsOutput('Opportunity_Partner__c');
			mainQuery.addFieldAsOutput('Opportunity_endClient1__c');
			mainQuery.addFieldAsOutput('Opportunity_Competition__c');
			mainQuery.addFieldAsOutput('OwnerId');
			mainQuery.addFieldAsOutput('Opportunity_Support__c');
			mainQuery.addFieldAsOutput('Opportunity_FastOrdersSales__c');
			mainQuery.addFieldAsOutput('account_web_site__c');
			mainQuery.addFieldAsOutput('account_web_site_unnecessary__c');
			mainQuery.addFieldAsOutput('ServiceWEBSite__c');
			mainQuery.addFieldAsOutput('ServiceWEBSite2__c');
			mainQuery.addFieldAsOutput('AmountTotal__c');
			mainQuery.addFieldAsOutput('CreditDecisionPrice__c');
			mainQuery.addFieldAsOutput('CloseDate');
			mainQuery.addFieldAsOutput('StageName');
			mainQuery.addFieldAsOutput('karute__c');
			mainQuery.addFieldAsOutput('karuteCheck__c');
			mainQuery.addFieldAsOutput('karutenew__c');
			mainQuery.addFieldAsOutput('AppointmentDay__c');
			mainQuery.addFieldAsOutput('Opportunity_lead1__c');
			mainQuery.addFieldAsOutput('Opportunity_lead2__c');
			mainQuery.addFieldAsOutput('CampaignId');
			mainQuery.addFieldAsOutput('not_campaign_flg__c');
			mainQuery.addFieldAsOutput('Opportunity_BillingMethod__c');
			mainQuery.addFieldAsOutput('CaseApproval__c');
			mainQuery.addFieldAsOutput('Opportunity_Title__c');
			mainQuery.addFieldAsOutput('Opportunity_PaymentSiteMaster__c');
			mainQuery.addFieldAsOutput('Opportunity_BillKana__c');
			mainQuery.addFieldAsOutput('Opportunity_PaymentMethod__c');
			mainQuery.addFieldAsOutput('AddressCheck__c');
			mainQuery.addFieldAsOutput('Opportunity_Memo__c');
			mainQuery.addFieldAsOutput('CustomerNamePRINT__c');
			mainQuery.addFieldAsOutput('QuoteExpirationDate__c');
			mainQuery.addFieldAsOutput('Opportunity_EstimateMemo__c');
			mainQuery.addFieldAsOutput('OrderProvisional__c');
			mainQuery.addFieldAsOutput('OrderProvisionalMemo__c');
			mainQuery.addFieldAsOutput('SalvageScheduledDate__c');
			mainQuery.addFieldAsOutput('opportunityBillMemo__c');
			mainQuery.addFieldAsOutput('notices__c');
			mainQuery.addFieldAsOutput('Agreement1__c');
			mainQuery.addFieldAsOutput('Agreement2__c');
			mainQuery.addFieldAsOutput('Agreement3__c');
			mainQuery.addFieldAsOutput('Agreement4__c');
			mainQuery.addFieldAsOutput('Agreement5__c');
			mainQuery.addFieldAsOutput('Agreement6__c');
			mainQuery.addFieldAsOutput('Agreement7__c');
			mainQuery.addFieldAsOutput('Agreement8__c');
			mainQuery.addFieldAsOutput('Agreement10__c');
			mainQuery.addFieldAsOutput('Agreement11__c');
			mainQuery.addFieldAsOutput('Agreement12__c');
			mainQuery.addFieldAsOutput('Agreement17__c');
			mainQuery.addFieldAsOutput('Agreement18__c');
			mainQuery.addFieldAsOutput('Agreement19__c');
			mainQuery.addFieldAsOutput('Agreement20__c');
			mainQuery.addFieldAsOutput('Agreement13__c');
			mainQuery.addFieldAsOutput('Agreement14__c');
			mainQuery.addFieldAsOutput('Agreement15__c');
			mainQuery.addFieldAsOutput('CreatedById');
			mainQuery.addFieldAsOutput('LastModifiedById');
			mainQuery.addFieldAsOutput('syouninn__c');
			mainQuery.addFieldAsOutput('CreatedDate');
			mainQuery.addFieldAsOutput('LastModifiedDate');
			mainQuery.addFieldAsOutput('Salesforce_ID__c');
			mainQuery.addFieldAsOutput('Agreement9__c');
			mainQuery.addFieldAsOutput('LeadSource');
			mainQuery.addFieldAsOutput('Id');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('OpportunityCopy__c', 'OpportunityCopy__c');
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			if (record.Id == null) {
				saveOldValues();
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}