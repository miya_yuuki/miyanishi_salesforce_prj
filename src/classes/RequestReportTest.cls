@isTest
private with sharing class RequestReportTest{
	private static testMethod void testPageMethods() {		RequestReport extension = new RequestReport(new ApexPages.StandardController(new CustomObject1__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}

	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component305() {
		String testReferenceId = '';
		RequestReport extension = new RequestReport(new ApexPages.StandardController(new CustomObject1__c()));
		extension.loadReferenceValues_Component305();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(Opportunity.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<Opportunity> parents = [SELECT Id FROM Opportunity LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		Opportunity parent = [SELECT Id,AccountNameView__c FROM Opportunity WHERE Id = :testReferenceId];
		extension.record.Opportunity__c = parent.Id;
		extension.loadReferenceValues_Component305();
				
		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.Account__c)) {
			System.assertEquals(parent.AccountNameView__c, extension.record.Account__c);
		}

		System.assert(true);
	}

	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component111() {
		String testReferenceId = '';
		RequestReport extension = new RequestReport(new ApexPages.StandardController(new CustomObject1__c()));
		extension.loadReferenceValues_Component111();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(Contact.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<Contact> parents = [SELECT Id FROM Contact LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		Contact parent = [SELECT Id,Contact_BillCompany__c,Title,Email,TOorCC__c,report__c FROM Contact WHERE Id = :testReferenceId];
		extension.record.MailPerson__c = parent.Id;
		extension.loadReferenceValues_Component111();
				
		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.Account__c)) {
			System.assertEquals(parent.Contact_BillCompany__c, extension.record.Account__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.BillTitle__c)) {
			System.assertEquals(parent.Title, extension.record.BillTitle__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.MailAddress__c)) {
			System.assertEquals(parent.Email, extension.record.MailAddress__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.Report11__c)) {
			System.assertEquals(parent.TOorCC__c, extension.record.Report11__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.TOorCC11__c)) {
			System.assertEquals(parent.report__c, extension.record.TOorCC11__c);
		}

		System.assert(true);
	}

	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component132() {
		String testReferenceId = '';
		RequestReport extension = new RequestReport(new ApexPages.StandardController(new CustomObject1__c()));
		extension.loadReferenceValues_Component132();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(Contact.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<Contact> parents = [SELECT Id FROM Contact LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		Contact parent = [SELECT Id,report__c,Title,Email,TOorCC__c FROM Contact WHERE Id = :testReferenceId];
		extension.record.MailPerson2__c = parent.Id;
		extension.loadReferenceValues_Component132();
				
		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.TOorCC11__c)) {
			System.assertEquals(parent.report__c, extension.record.TOorCC11__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.BillTitle12__c)) {
			System.assertEquals(parent.Title, extension.record.BillTitle12__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.MailAddress2__c)) {
			System.assertEquals(parent.Email, extension.record.MailAddress2__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.Report22__c)) {
			System.assertEquals(parent.TOorCC__c, extension.record.Report22__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.TOorCC22__c)) {
			System.assertEquals(parent.report__c, extension.record.TOorCC22__c);
		}

		System.assert(true);
	}

	@isTest(SeeAllData=true)
	public static void test_loadReferenceValues_Component137() {
		String testReferenceId = '';
		RequestReport extension = new RequestReport(new ApexPages.StandardController(new CustomObject1__c()));
		extension.loadReferenceValues_Component137();

		if (testReferenceId == '') {
			try {
				SkyEditor2.TestData testdata = new SkyEditor2.TestData(Contact.getSObjectType());
				SObject parent = testdata.newSObject();
				insert parent;
				testReferenceId = parent.Id;
			} catch (Exception e) {
				List<Contact> parents = [SELECT Id FROM Contact LIMIT 1];
				if (parents.size() == 0) {
					throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
				} else {
					testReferenceId = parents[0].Id;
				}
			}
		}
		Contact parent = [SELECT Id,Title,Email,TOorCC__c,report__c FROM Contact WHERE Id = :testReferenceId];
		extension.record.MailPerson3__c = parent.Id;
		extension.loadReferenceValues_Component137();
				
		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.BillTitle13__c)) {
			System.assertEquals(parent.Title, extension.record.BillTitle13__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.MailAddress3__c)) {
			System.assertEquals(parent.Email, extension.record.MailAddress3__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.Report33__c)) {
			System.assertEquals(parent.TOorCC__c, extension.record.Report33__c);
		}


		if (SkyEditor2.Util.isEditable(extension.record, CustomObject1__c.fields.TOorCC33__c)) {
			System.assertEquals(parent.report__c, extension.record.TOorCC33__c);
		}

		System.assert(true);
	}
}