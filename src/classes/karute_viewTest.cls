@isTest
private with sharing class karute_viewTest{
	private static testMethod void testPageMethods() {		karute_view extension = new karute_view(new ApexPages.StandardController(new karute__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
		extension.getComponent4127OptionsJS();
		extension.getComponent4128OptionsJS();
		extension.getComponent4129OptionsJS();
		extension.getComponent4130OptionsJS();
	}
	private static testMethod void testComponent4436() {
		karute_view.Component4436 Component4436 = new karute_view.Component4436(new List<PenaltyManagement__c>(), new List<karute_view.Component4436Item>(), new List<PenaltyManagement__c>(), null);
		Component4436.create(new PenaltyManagement__c());
		System.assert(true);
	}
	
	private static testMethod void testComponent2716() {
		karute_view.Component2716 Component2716 = new karute_view.Component2716(new List<Minutes__c>(), new List<karute_view.Component2716Item>(), new List<Minutes__c>(), null);
		Component2716.create(new Minutes__c());
		System.assert(true);
	}
	
	private static testMethod void testComponent3891() {
		karute_view.Component3891 Component3891 = new karute_view.Component3891(new List<AEGIS__c>(), new List<karute_view.Component3891Item>(), new List<AEGIS__c>(), null);
		Component3891.create(new AEGIS__c());
		System.assert(true);
	}
	
	private static testMethod void testComponent3317() {
		karute_view.Component3317 Component3317 = new karute_view.Component3317(new List<CLOVER__c>(), new List<karute_view.Component3317Item>(), new List<CLOVER__c>(), null);
		Component3317.create(new CLOVER__c());
		System.assert(true);
	}
	
	@isTest
	private static void testLightDataTables(){

		System.assert(true);
	}
	private static testMethod void testRecordTypeFullNames() {
		Set<String> result = karute_view.recordTypeFullNames(new RecordType[] {
			new RecordType(DeveloperName = 'TestRecordType')
		});
		System.assertEquals(result.size(), 1);
		System.assert(result.contains('TestRecordType'));
	}
	
	private static testMethod void testFilterMetadataJSON() {
		String json = '{"CustomObject":{"recordTypes":[{"fullName":"RecordType1","picklistValues":[]},{"fullName":"RecordType2","picklistValues":[]}]}}';		Set<String> recordTypeSet = new Set<String>();
		recordTypeSet.add('RecordType2');
		Object metadata = System.JSON.deserializeUntyped(json);
		Map<String, Object> data = (Map<String, Object>) karute_view.filterMetadataJSON(metadata, recordTypeSet, SkyEditor2__SkyEditorDummy__c.SObjectType).data;
		Map<String, Object> customObject = (Map<String, Object>) data.get('CustomObject');
		List<Object> recordTypes = (List<Object>) customObject.get('recordTypes');
		System.assertEquals(recordTypes.size(), 1);
		Map<String, Object> recordType = (Map<String, Object>) recordTypes[0];
		System.assertEquals('RecordType2', recordType.get('fullName'));
	}

}