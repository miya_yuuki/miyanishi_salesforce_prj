global with sharing class RDunit extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public OpportunityProduct__c record{get;set;}	
			
	
		public Component2 Component2 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component125_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component125_to{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component4_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component4_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component4_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component4_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component58_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component58_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component58_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component58_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component63_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component63_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component63_op{get;set;}	
		public List<SelectOption> valueOptions_OpportunityProduct_c_R_D_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component108_val {get;set;}	
		public SkyEditor2.TextHolder Component108_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component13_val {get;set;}	
		public SkyEditor2.TextHolder Component13_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component67_val {get;set;}	
		public SkyEditor2.TextHolder Component67_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component110_val {get;set;}	
		public SkyEditor2.TextHolder Component110_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component112_val {get;set;}	
		public SkyEditor2.TextHolder Component112_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component114_val {get;set;}	
		public SkyEditor2.TextHolder Component114_op{get;set;}	
		public List<SelectOption> valueOptions_OpportunityProduct_c_Item_Classification_c {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component116_val {get;set;}	
		public SkyEditor2.TextHolder Component116_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component118_val {get;set;}	
		public SkyEditor2.TextHolder Component118_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component120_val {get;set;}	
		public SkyEditor2.TextHolder Component120_op{get;set;}	
		public List<SelectOption> valueOptions_OpportunityProduct_c_SEO_sisaku_c {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component122_val {get;set;}	
		public SkyEditor2.TextHolder Component122_op{get;set;}	
		public List<SelectOption> valueOptions_OpportunityProduct_c_Item_Automatic_updating_c {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component124_val {get;set;}	
		public SkyEditor2.TextHolder Component124_op{get;set;}	
		public List<SelectOption> valueOptions_OpportunityProduct_c_Item_Estimate_level_c {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component128_val {get;set;}	
		public SkyEditor2.TextHolder Component128_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component92_val {get;set;}	
		public SkyEditor2.TextHolder Component92_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component94_val {get;set;}	
		public SkyEditor2.TextHolder Component94_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component17_val {get;set;}	
		public SkyEditor2.TextHolder Component17_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component15_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component15_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component15_op{get;set;}	
		public List<SelectOption> valueOptions_Opportunity_StageName_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component105_val {get;set;}	
		public SkyEditor2.TextHolder Component105_op{get;set;}	
			
	public String recordTypeRecordsJSON_OpportunityProduct_c {get; private set;}
	public String defaultRecordTypeId_OpportunityProduct_c {get; private set;}
	public String metadataJSON_OpportunityProduct_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public RDunit(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = OpportunityProduct__c.fields.R_D__c;
		f = Opportunity.fields.Name;
		f = OpportunityProduct__c.fields.Name;
		f = OpportunityProduct__c.fields.Account__c;
		f = OpportunityProduct__c.fields.sisakuompany__c;
		f = OpportunityProduct__c.fields.ProductName_Detail__c;
		f = OpportunityProduct__c.fields.Item_Classification__c;
		f = OpportunityProduct__c.fields.Item_Keyword_strategy_keyword__c;
		f = OpportunityProduct__c.fields.URL_RandDType__c;
		f = OpportunityProduct__c.fields.SEO_sisaku__c;
		f = OpportunityProduct__c.fields.Item_Automatic_updating__c;
		f = OpportunityProduct__c.fields.Item_Estimate_level__c;
		f = OpportunityProduct__c.fields.SalesPerson__c;
		f = OpportunityProduct__c.fields.Item_Contract_start_date__c;
		f = OpportunityProduct__c.fields.Item_Contract_end_date__c;
		f = OpportunityProduct__c.fields.RecordTypeId;
		f = Opportunity.fields.StageName;
		f = OpportunityProduct__c.fields.Item_Detail_Product__c;
		f = OpportunityProduct__c.fields.Opportunity__c;
		f = OpportunityProduct__c.fields.TotalPrice__c;
		f = OpportunityProduct__c.fields.keiyakukikan__c;
		f = OpportunityProduct__c.fields.Opportunity_Phase__c;
		f = Opportunity.fields.syouninn__c;
 f = OpportunityProduct__c.fields.TotalPrice__c;
 f = OpportunityProduct__c.fields.Item_Entry_date__c;
 f = OpportunityProduct__c.fields.OrderDay__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = OpportunityProduct__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component125_from = new SkyEditor2__SkyEditorDummy__c();	
				Component125_to = new SkyEditor2__SkyEditorDummy__c();	
					
				Component4_from = new SkyEditor2__SkyEditorDummy__c();	
				Component4_to = new SkyEditor2__SkyEditorDummy__c();	
				Component4_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component4_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component58_from = new SkyEditor2__SkyEditorDummy__c();	
				Component58_to = new SkyEditor2__SkyEditorDummy__c();	
				Component58_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component58_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component63_val = new SkyEditor2__SkyEditorDummy__c();	
				Component63_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component63_op = new SkyEditor2.TextHolder();	
				valueOptions_OpportunityProduct_c_R_D_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : OpportunityProduct__c.R_D__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_OpportunityProduct_c_R_D_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component13_val = new SkyEditor2__SkyEditorDummy__c();	
				Component13_op = new SkyEditor2.TextHolder();	
					
				Component67_val = new SkyEditor2__SkyEditorDummy__c();	
				Component67_op = new SkyEditor2.TextHolder();	
					
				Component110_val = new SkyEditor2__SkyEditorDummy__c();	
				Component110_op = new SkyEditor2.TextHolder();	
					
				Component112_val = new SkyEditor2__SkyEditorDummy__c();	
				Component112_op = new SkyEditor2.TextHolder();	
					
				Component114_val = new SkyEditor2__SkyEditorDummy__c();	
				Component114_op = new SkyEditor2.TextHolder();	
				valueOptions_OpportunityProduct_c_Item_Classification_c = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : OpportunityProduct__c.Item_Classification__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_OpportunityProduct_c_Item_Classification_c.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component116_val = new SkyEditor2__SkyEditorDummy__c();	
				Component116_op = new SkyEditor2.TextHolder();	
					
				Component118_val = new SkyEditor2__SkyEditorDummy__c();	
				Component118_op = new SkyEditor2.TextHolder();	
					
				Component120_val = new SkyEditor2__SkyEditorDummy__c();	
				Component120_op = new SkyEditor2.TextHolder();	
				valueOptions_OpportunityProduct_c_SEO_sisaku_c = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : OpportunityProduct__c.SEO_sisaku__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_OpportunityProduct_c_SEO_sisaku_c.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component122_val = new SkyEditor2__SkyEditorDummy__c();	
				Component122_op = new SkyEditor2.TextHolder();	
				valueOptions_OpportunityProduct_c_Item_Automatic_updating_c = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : OpportunityProduct__c.Item_Automatic_updating__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_OpportunityProduct_c_Item_Automatic_updating_c.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component124_val = new SkyEditor2__SkyEditorDummy__c();	
				Component124_op = new SkyEditor2.TextHolder();	
				valueOptions_OpportunityProduct_c_Item_Estimate_level_c = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : OpportunityProduct__c.Item_Estimate_level__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_OpportunityProduct_c_Item_Estimate_level_c.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component128_val = new SkyEditor2__SkyEditorDummy__c();	
				Component128_op = new SkyEditor2.TextHolder();	
					
				Component92_val = new SkyEditor2__SkyEditorDummy__c();	
				Component92_op = new SkyEditor2.TextHolder();	
					
				Component94_val = new SkyEditor2__SkyEditorDummy__c();	
				Component94_op = new SkyEditor2.TextHolder();	
					
				Component17_val = new SkyEditor2__SkyEditorDummy__c();	
				Component17_op = new SkyEditor2.TextHolder();	
					
				Component105_val = new SkyEditor2__SkyEditorDummy__c();	
				Component105_op = new SkyEditor2.TextHolder();	
					
				Component108_val = new SkyEditor2__SkyEditorDummy__c();	
				Component108_op = new SkyEditor2.TextHolder();	
					
				Component15_val = new SkyEditor2__SkyEditorDummy__c();	
				Component15_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component15_op = new SkyEditor2.TextHolder();	
				valueOptions_Opportunity_StageName_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : Opportunity.StageName.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_Opportunity_StageName_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				queryMap.put(	
					'Component2',	
					new SkyEditor2.Query('OpportunityProduct__c')
						.addField('R_D__c')
						.addFieldAsOutput('Opportunity__c')
						.addFieldAsOutput('Name')
						.addFieldAsOutput('Account__c')
						.addFieldAsOutput('sisakuompany__c')
						.addFieldAsOutput('ProductName_Detail__c')
						.addFieldAsOutput('Item_Classification__c')
						.addFieldAsOutput('Item_Keyword_strategy_keyword__c')
						.addFieldAsOutput('URL_RandDType__c')
						.addFieldAsOutput('Item_Estimate_level__c')
						.addFieldAsOutput('TotalPrice__c')
						.addFieldAsOutput('SalesPerson__c')
						.addFieldAsOutput('keiyakukikan__c')
						.addFieldAsOutput('Opportunity_Phase__c')
						.addFieldAsOutput('Opportunity__r.syouninn__c')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component125_from, 'SkyEditor2__Text__c', 'TotalPrice__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component125_to, 'SkyEditor2__Text__c', 'TotalPrice__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component4_from, 'SkyEditor2__Date__c', 'Item_Entry_date__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component4_to, 'SkyEditor2__Date__c', 'Item_Entry_date__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component4_isNull, 'SkyEditor2__Date__c', 'Item_Entry_date__c', Component4_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component58_from, 'SkyEditor2__Date__c', 'OrderDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component58_to, 'SkyEditor2__Date__c', 'OrderDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component58_isNull, 'SkyEditor2__Date__c', 'OrderDay__c', Component58_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component63_val_dummy, 'SkyEditor2__Text__c','R_D__c', Component63_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component108_val, 'SkyEditor2__Text__c', 'Opportunity__r.Name',Opportunity.fields.Name, Component108_op, true, false,true,0,false,'Opportunity__c',OpportunityProduct__c.fields.Opportunity__c )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component13_val, 'SkyEditor2__Text__c', 'Name', Component13_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component67_val, 'SkyEditor2__Text__c', 'Account__c', Component67_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component110_val, 'SkyEditor2__Text__c', 'sisakuompany__c', Component110_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component112_val, 'SkyEditor2__Text__c', 'ProductName_Detail__c', Component112_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component114_val, 'SkyEditor2__Text__c', 'Item_Classification__c', Component114_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component116_val, 'SkyEditor2__Text__c', 'Item_Keyword_strategy_keyword__c', Component116_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component118_val, 'SkyEditor2__Text__c', 'URL_RandDType__c', Component118_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component120_val, 'SkyEditor2__Text__c', 'SEO_sisaku__c', Component120_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component122_val, 'SkyEditor2__Text__c', 'Item_Automatic_updating__c', Component122_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component124_val, 'SkyEditor2__Text__c', 'Item_Estimate_level__c', Component124_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component128_val, 'SkyEditor2__Text__c', 'SalesPerson__c', Component128_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component92_val, 'SkyEditor2__Date__c', 'Item_Contract_start_date__c', Component92_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component94_val, 'SkyEditor2__Date__c', 'Item_Contract_end_date__c', Component94_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component17_val, 'SkyEditor2__Text__c', 'RecordTypeId', Component17_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component15_val_dummy, 'SkyEditor2__Text__c','Opportunity__r.StageName',Opportunity.fields.StageName, Component15_op, true, false,true,0,false,'Opportunity__c',OpportunityProduct__c.fields.Opportunity__c )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component105_val, 'SkyEditor2__Text__c', 'Item_Detail_Product__c', Component105_op, true, 0, false ))
				);	
					
					Component2 = new Component2(new List<OpportunityProduct__c>(), new List<Component2Item>(), new List<OpportunityProduct__c>(), null);
				listItemHolders.put('Component2', Component2);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(OpportunityProduct__c.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = true;
			execInitialSearch = false;
			presetSystemParams();
			Component2.extender = this.extender;
			initSearch();
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_R_D_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_Opportunity_Name() { 
			return getOperatorOptions('Opportunity', 'Name');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_Name() { 
			return getOperatorOptions('OpportunityProduct__c', 'Name');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_Account_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'Account__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_sisakuompany_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'sisakuompany__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_ProductName_Detail_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'ProductName_Detail__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_Item_Classification_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'Item_Classification__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_Item_Keyword_strategy_keyword_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'Item_Keyword_strategy_keyword__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_URL_RandDType_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'URL_RandDType__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_SEO_sisaku_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'SEO_sisaku__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_Item_Automatic_updating_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'Item_Automatic_updating__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_Item_Estimate_level_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'Item_Estimate_level__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_SalesPerson_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'SalesPerson__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_Item_Contract_start_date_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'Item_Contract_start_date__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_Item_Contract_end_date_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'Item_Contract_end_date__c');	
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_RecordTypeId() { 
			return getOperatorOptions('OpportunityProduct__c', 'RecordTypeId');	
		}	
		public List<SelectOption> getOperatorOptions_Opportunity_StageName_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_OpportunityProduct_c_Item_Detail_Product_c() { 
			return getOperatorOptions('OpportunityProduct__c', 'Item_Detail_Product__c');	
		}	
			
			
	global with sharing class Component2Item extends SkyEditor2.ListItem {
		public OpportunityProduct__c record{get; private set;}
		@TestVisible
		Component2Item(Component2 holder, OpportunityProduct__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component2 extends SkyEditor2.ListItemHolder {
		public List<Component2Item> items{get; private set;}
		@TestVisible
			Component2(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component2Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component2Item(this, (OpportunityProduct__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	public OpportunityProduct__c Component2_table_Conversion { get { return new OpportunityProduct__c();}}
	
	public String Component2_table_selectval { get; set; }
	
	
			
	}