public with sharing class OpportunityClauseSet {
    public static List<Opportunity> prcOpportunityClauseSet(List<Opportunity> objOpp) {
        // 約款ページID
        String ClauseID = null;
        // その他申込書出力約款ページID
        List<String> strOthersIds = new List<String>();
        // コンテンツ申込書出力約款ページID
        List<String> strContentsIds = new List<String>();
        // その他申込書出力約款ファイル
        List<ContentDocument> OthersDocuments = new List<ContentDocument>();
        // コンテンツ申込書出力約款ファイル
        List<ContentDocument> ContentsDocuments = new List<ContentDocument>();
        // その他申込書出力約款名
        List<String> strOthersTitles = new List<String>();
        // コンテンツ申込書出力約款名
        List<String> strContentsTitles = new List<String>();
        
        
        for (Opportunity obj: objOpp) {
            obj.AgreementTEXTView__c = '';
            obj.AgreementTEXTContents__c = '';

            //////// その他申込書 ////////
            // 1. コンサルティングサービス約款
            if (obj.Agreement1__c == true) {
                ClauseID = '06910000004j6m7AAA';
                strOthersIds.add(ClauseID);
            }
            // 2. コンテンツ拡散サービス約款
            if (obj.Agreement2__c == true) {
                ClauseID = '069100000069thAAAQ';
                strOthersIds.add(ClauseID);
            }
            // 3. レポーティングサービス約款
            if (obj.Agreement3__c == true) {
                ClauseID = '069100000069uqwAAA';
                strOthersIds.add(ClauseID);
            }
            // 4. CMS構築サービス約款
            if (obj.Agreement4__c == true) {
                ClauseID = '0695F0000073QLGQA2';
                strOthersIds.add(ClauseID);
            }
            // 5. Webサイト運用代行サービス約款
            if (obj.Agreement5__c == true) {
                ClauseID = '06910000004jEj6AAE';
                strOthersIds.add(ClauseID);
            }
            // 6. SNSアカウント運用代行サービス約款
            if (obj.Agreement6__c == true) {
                ClauseID = '0695F0000073NmYQAU';
                strOthersIds.add(ClauseID);
            }
            // 7. SNS広告運用代行サービス約款
            if (obj.Agreement7__c == true) {
                ClauseID = '0695F00000731SMQAY';
                strOthersIds.add(ClauseID);
            }
            // 8. 外部リンクサービス約款
            if (obj.Agreement8__c == true) {
                ClauseID = '06910000004hWleAAE';
                strOthersIds.add(ClauseID);
            }
            // 9. アドネット広告運用代行サービス約款
            if (obj.Agreement9__c == true) {
                ClauseID = '0695F00000BKjTxQAL';
                strOthersIds.add(ClauseID);
            }
            // 10. 暮らしニスタ広告約款
            if (obj.Agreement10__c == true) {
                ClauseID = '0695F0000075EcdQAE';
                strOthersIds.add(ClauseID);
            }
            // 11. コンテンツ制作サービス約款
            if (obj.Agreement11__c == true) {
                ClauseID = '06910000004jEj1AAE';
                strOthersIds.add(ClauseID);
            }
            // 12. EASY ENTRYサービス約款
            if (obj.Agreement12__c == true) {
                ClauseID = '06910000004gxkyAAA';
                strOthersIds.add(ClauseID);
            }
            // 17. Milly広告約款
            if (obj.Agreement17__c == true) {
                ClauseID = '0695F00000AR8aXQAT';
                strOthersIds.add(ClauseID);
            }
            // 18. マーケティングツール「TACT SEO」サービス約款
            if (obj.Agreement18__c == true) {
                ClauseID = '0695F00000CLYnEQAX';
                strOthersIds.add(ClauseID);
            }
            // 19. 「Front Desk」サービス約款
            if (obj.Agreement19__c == true) {
                ClauseID = '0695F00000C4voaQAB';
                strOthersIds.add(ClauseID);
            }
            // 20. エディトルサービス約款
            if (obj.Agreement20__c == true) {
                ClauseID = '0695F00000EG33XQAT';
                strOthersIds.add(ClauseID);
            }
            // その他申込書出力約款名を取得
            if (strOthersIds.size() > 0) {
                OthersDocuments = [SELECT Id, Title FROM ContentDocument WHERE Id in :strOthersIds];
                for (ContentDocument Odoc: OthersDocuments) strOthersTitles.add(Odoc.Title);
                // 申込書出力項目に代入
                obj.AgreementTEXTView__c = String.join(strOthersTitles,'\n');
            }

            //////// コンテンツ申込書 ////////
            // 13. レポーティングサービス約款
            if (obj.Agreement13__c == true) {
                ClauseID = '069100000069uqwAAA';
                strContentsIds.add(ClauseID);
            }
            // 14. レポーティングサービス約款
            if (obj.Agreement14__c == true) {
                ClauseID = '06910000004jEj1AAE';
                strContentsIds.add(ClauseID);
            }
            // 15. レポーティングサービス約款
            if (obj.Agreement15__c == true) {
                ClauseID = '0695F000008opunQAA';
                strContentsIds.add(ClauseID);
            }
            // コンテンツ申込書出力約款名を取得
            if (strContentsIds.size() > 0) {
                ContentsDocuments = [SELECT Id, Title FROM ContentDocument WHERE Id in :strContentsIds];
                for (ContentDocument Cdoc: ContentsDocuments) strContentsTitles.add(Cdoc.Title);
                // 申込書出力項目に代入
                obj.AgreementTEXTContents__c = String.join(strContentsTitles,'\n');
            }
        }
        return objOpp;
    }
}