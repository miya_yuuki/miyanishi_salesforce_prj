public with sharing class AccountWebSiteUpdate {
    public static boolean firstRun = true;

    public static void prcAccountWebSiteUpdate(List<Opportunity> objOpportunity, Map<Id, Opportunity> oldMap) {
        // 商談IDの配列
        List<String> strOppIds = new List<String>();
        // 取引先WebサイトIDの配列
        List<String> strWebSiteIds = new List<String>();
        // 商談オブジェクトの配列
        List<Opportunity> resOpp = new List<Opportunity>();
        // 取引先Webサイトオブジェクトの配列
        List<AccountWebSite__c> accountWebSites = new List<AccountWebSite__c>();
        // 取引先WebサイトオブジェクトのMap
        Map<Id, AccountWebSite__c> mapWebSites = new Map<Id, AccountWebSite__c>();
        // 更新対象の取引先Webサイトオブジェクトの配列
        List<AccountWebSite__c> updWebSites = new List<AccountWebSite__c>();
        // 更新対象か否かのフラグ
        Boolean updFlg = false;
        
        // 対象取引先Webサイトを取得
        for (Opportunity obj : objOpportunity) strWebSiteIds.add(obj.account_web_site__c);
        // レコードを取得
        accountWebSites = [
            SELECT
            Id,
            writing_resource_summary__c,
            writing_resource_detail__c,
            SEO_contents_budget_per_month__c,
            seo_measure__c,
            Record__c
            FROM AccountWebSite__c
            WHERE Id IN :strWebSiteIds];
        // 取引先Webサイトの情報をMapに保存
        for (AccountWebSite__c webSite : accountWebSites) mapWebSites.put(webSite.Id, webSite);

        for (Opportunity newOpp : objOpportunity) {
            updFlg = false;
            if (newOpp.account_web_site__c != null) {
                // 更新対象の取引先Webサイトの情報を取得
                AccountWebSite__c targetWebSite = mapWebSites.get(newOpp.account_web_site__c);
                Opportunity oldOpp = oldMap.get(newOpp.Id);
                // 執筆リソース（大）の内容を書き込む
                if (newOpp.writing_resource_summary__c != null && newOpp.writing_resource_summary__c != oldOpp.writing_resource_summary__c && targetWebSite.writing_resource_summary__c != newOpp.writing_resource_summary__c) {
                    targetWebSite.writing_resource_summary__c = newOpp.writing_resource_summary__c;
                    updFlg = true;
                }
                // 執筆リソース（小）の内容を書き込む
                if (newOpp.contents_measure_status__c != null && newOpp.contents_measure_status__c != oldOpp.contents_measure_status__c && targetWebSite.writing_resource_detail__c != newOpp.contents_measure_status__c) {
                    targetWebSite.writing_resource_detail__c = newOpp.contents_measure_status__c;
                    updFlg = true;
                }
                // SEO・コンテンツ予算/月の内容を書き込む
                if (newOpp.SEO_contents_budget_per_month__c != null && newOpp.SEO_contents_budget_per_month__c != oldOpp.SEO_contents_budget_per_month__c && targetWebSite.SEO_contents_budget_per_month__c != newOpp.SEO_contents_budget_per_month__c) {
                    targetWebSite.SEO_contents_budget_per_month__c = newOpp.SEO_contents_budget_per_month__c;
                    updFlg = true;
                }
                // SEO施策状況の内容を書き込む
                if (newOpp.SEOMeasureStatus__c != null && newOpp.SEOMeasureStatus__c != oldOpp.SEOMeasureStatus__c && targetWebSite.seo_measure__c != newOpp.SEOMeasureStatus__c) {
                    targetWebSite.seo_measure__c = newOpp.SEOMeasureStatus__c;
                    updFlg = true;
                }
                // レコードの内容を書き込む
                if (newOpp.Opportunity_ActionDay__c != null && newOpp.Opportunity_ActionDay__c != oldOpp.Opportunity_ActionDay__c && targetWebSite.Record__c != newOpp.Opportunity_ActionDay__c) {
                    targetWebSite.Record__c = newOpp.Opportunity_ActionDay__c;
                    updFlg = true;
                }

                // 更新内容があれば、保存対象に追加
                if (updFlg) {
                    updWebSites.add(targetWebSite);
                }
            }
        }
        if (updWebSites.size() > 0) {
            update updWebSites;
        }
    }
}