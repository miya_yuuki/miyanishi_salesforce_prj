@isTest
private with sharing class textmanager_for_accountantTest{
		private static testMethod void testPageMethods() {	
			textmanager_for_accountant page = new textmanager_for_accountant(new ApexPages.StandardController(new TextManagement2__c()));	
			page.getOperatorOptions_TextManagement2_c_Account1_c();	
			page.getOperatorOptions_TextManagement2_c_Name();	
			page.getOperatorOptions_TextManagement2_c_OrderItemStatus_c();	
			page.getOperatorOptions_TextManagement2_c_Item2_OrderProduct_c();	
			page.getOperatorOptions_TextManagement2_c_CloudStatus_c_multi();	
			page.getOperatorOptions_TextManagement2_c_Product11_c();	
			page.getOperatorOptions_TextManagement2_c_ProductRecordType_c();	
			page.getOperatorOptions_TextManagement2_c_BillAmountPrint_c();	
			System.assert(true);
		}	
			
	private static testMethod void testComponent3() {
		textmanager_for_accountant.Component3 Component3 = new textmanager_for_accountant.Component3(new List<TextManagement2__c>(), new List<textmanager_for_accountant.Component3Item>(), new List<TextManagement2__c>(), null);
		Component3.create(new TextManagement2__c());
		Component3.doDeleteSelectedItems();
		System.assert(true);
	}
	
}