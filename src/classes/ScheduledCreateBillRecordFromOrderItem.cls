global class ScheduledCreateBillRecordFromOrderItem implements Schedulable {

	// 1回のexecuteメソッドで処理される件数
	private final Integer BATCH_SIZE = 1;

	global void execute(SchedulableContext sc) {

		// 月初の日付を取得
		Date startOfMonth = Date.today().toStartOfMonth();

		// 月初の場合は前月が対象
		if (Date.today().day() == 1) {
			startOfMonth = startOfMonth.addMonths(-1);
		}

		// 月末の日付を算出 （月初 + 1ヵ月 - 1日）
		Date endOfMonth = startOfMonth.addMonths(1).addDays(-1);

		System.debug('**** 請求レコード作成バッチ実行開始 請求対象日: ' + startOfMonth.format() + '～' + endOfMonth.format());
		BatchScheduledCreateBillRecordFromOI batch = new BatchScheduledCreateBillRecordFromOI(startOfMonth, endOfMonth);
        Database.executeBatch(batch, BATCH_SIZE);
		System.debug('**** 請求レコード作成バッチ実行完了 請求対象日: ' + startOfMonth.format() + '～' + endOfMonth.format());
	}
}