global with sharing class SVE_20150408194724 extends SkyEditor2.SkyEditorPageBaseWithSharing {
    
    public Lead record {get{return (Lead)mainRecord;}}
    public with sharing class CanvasException extends Exception {}

    public String recordTypeRecordsJSON_Lead {get; private set;}
    public String defaultRecordTypeId_Lead {get; private set;}
    public String metadataJSON_Lead {get; private set;}
    public String picklistValuesJSON_Lead_lead_AttackProduct_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_WebPromotion_c {get; private set;}
    public String picklistValuesJSON_Lead_lead_Rival_c {get; private set;}
    public String Component200_hidden { get; set; }
    public String Component182_hidden { get; set; }
    public String Component193_hidden { get; set; }
    public Map<String,Map<String,Object>> appComponentProperty {get; set;}
    
    
    public SVE_20150408194724(ApexPages.StandardController controller) {
        super(controller);

        appComponentProperty = new Map<String, Map<String, Object>>();
        Map<String, Object> tmpPropMap = null;

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('noneTextOn','true');
        tmpPropMap.put('noneText','--選択--');
        tmpPropMap.put('editableOn','true');
        tmpPropMap.put('targetField1','');
        tmpPropMap.put('makeSelOp1','1\n2\n3');
        tmpPropMap.put('targetField2','');
        tmpPropMap.put('makeSelOp2','');
        tmpPropMap.put('targetField3','');
        tmpPropMap.put('makeSelOp3','');
        tmpPropMap.put('targetField4','');
        tmpPropMap.put('makeSelOp4','');
        tmpPropMap.put('targetField5','');
        tmpPropMap.put('makeSelOp5','');
        tmpPropMap.put('targetField6','');
        tmpPropMap.put('makeSelOp6','');
        tmpPropMap.put('targetField7','');
        tmpPropMap.put('makeSelOp7','');
        tmpPropMap.put('targetField8','');
        tmpPropMap.put('makeSelOp8','');
        tmpPropMap.put('targetField9','');
        tmpPropMap.put('makeSelOp9','');
        tmpPropMap.put('targetField10','');
        tmpPropMap.put('makeSelOp10','');
        tmpPropMap.put('Component__Width','188');
        tmpPropMap.put('Component__Height','50');
        tmpPropMap.put('Component__id','Component233');
        tmpPropMap.put('Component__Name','ChangeSelectList');
        tmpPropMap.put('Component__NameSpace','appcom');
        tmpPropMap.put('Component__Top','0');
        tmpPropMap.put('Component__Left','0');
        tmpPropMap.put('settings','{"targetField1":null,"makeSelOp1":"1\\\\n2\\\\n3","targetField2":null,"makeSelOp2":"","targetField3":null,"makeSelOp3":"","targetField4":null,"makeSelOp4":"","targetField5":null,"makeSelOp5":"","targetField6":null,"makeSelOp6":"","targetField7":null,"makeSelOp7":"","targetField8":null,"makeSelOp8":"","targetField9":null,"makeSelOp9":"","targetField10":null,"makeSelOp10":""}');
        appComponentProperty.put('Component233',tmpPropMap);


        SObjectField f;

        f = Lead.fields.OwnerId;
        f = Lead.fields.Company;
        f = Lead.fields.lead_AccountKana__c;
        f = Lead.fields.Phone;
        f = Lead.fields.LastName;
        f = Lead.fields.FirstName;
        f = Lead.fields.lead_kana__c;
        f = Lead.fields.lead_Division__c;
        f = Lead.fields.Title;
        f = Lead.fields.Email;
        f = Lead.fields.Status1__c;
        f = Lead.fields.Status2__c;
        f = Lead.fields.lead_NGmemo__c;
        f = Lead.fields.Website;
        f = Lead.fields.lead_Relation__c;
        f = Lead.fields.lead_lead1__c;
        f = Lead.fields.lead_lead2__c;
        f = Lead.fields.Lead_WEBsite_Quality__c;
        f = Lead.fields.lead_CompanySize__c;
        f = Lead.fields.lead_BudgetMonth1__c;
        f = Lead.fields.lead_AttackProduct__c;
        f = Lead.fields.lead_hope__c;
        f = Lead.fields.lead_Timing1__c;
        f = Lead.fields.lead_NextContactDay__c;
        f = Lead.fields.lead_WebPromotion__c;
        f = Lead.fields.lead_Rival__c;
        f = Lead.fields.lead_PromotionContents__c;
        f = Lead.fields.PostalCode;
        f = Lead.fields.State;
        f = Lead.fields.City;
        f = Lead.fields.Street;
        f = Lead.fields.lead_BusinessModel__c;
        f = Lead.fields.lead_BusinessContent__c;
        f = Lead.fields.Description;

        List<RecordTypeInfo> recordTypes;
        List<RecordType> recordTypeRecords_Lead = [SELECT Id, DeveloperName, NamespacePrefix FROM RecordType WHERE SobjectType = 'Lead'];
        Map<Id, RecordType> recordTypeMap_Lead = new Map<Id, RecordType>(recordTypeRecords_Lead);
        List<RecordType> availableRecordTypes_Lead = new List<RecordType>();
        recordTypeRecordsJSON_Lead = System.JSON.serialize(recordTypeRecords_Lead);
        recordTypes = SObjectType.Lead.getRecordTypeInfos();

        for (RecordTypeInfo t: recordTypes) {
            if (t.isDefaultRecordTypeMapping()) {
                defaultRecordTypeId_Lead = t.getRecordTypeId();
            }
            if (t.isAvailable()) {
                RecordType rtype = recordTypeMap_Lead.get(t.getRecordTypeId());
                if (rtype != null) {
                    availableRecordTypes_Lead.add(rtype);
                }
            }
        }
        metadataJSON_Lead = System.JSON.serialize(filterMetadataJSON(
            System.JSON.deserializeUntyped('{"CustomObject":{"recordTypes":[{"fullName":"Lead","picklistValues":[{"picklist":"lead_AttackProduct__c","values":[{"fullName":"EFO","default":false},{"fullName":"KIJITASU","default":false},{"fullName":"SEO%EF%BC%88外部リンク%EF%BC%89","default":false},{"fullName":"イージス","default":false},{"fullName":"クローバー","default":false},{"fullName":"コンサル","default":false},{"fullName":"サイト制作","default":false},{"fullName":"テキスト制作","default":false},{"fullName":"ペナルティプロテクト","default":false},{"fullName":"風評被害","default":false}]},{"picklist":"lead_Rival__c","values":[{"fullName":"CA","default":false},{"fullName":"GMO","default":false},{"fullName":"PLAN-B","default":false},{"fullName":"アイオイクス","default":false},{"fullName":"アウンコンサル","default":false},{"fullName":"フルスピード","default":false}]},{"picklist":"lead_WebPromotion__c","values":[{"fullName":"SEO対策","default":true},{"fullName":"アドネットワーク","default":false},{"fullName":"コンテンツマーケ","default":false},{"fullName":"リスティング","default":false}]}]},{"fullName":"Recruit","picklistValues":[{"picklist":"lead_AttackProduct__c","values":[{"fullName":"EFO","default":false},{"fullName":"KIJITASU","default":false},{"fullName":"SEO%EF%BC%88外部リンク%EF%BC%89","default":false},{"fullName":"イージス","default":false},{"fullName":"クローバー","default":false},{"fullName":"コンサル","default":false},{"fullName":"サイト制作","default":false},{"fullName":"テキスト制作","default":false},{"fullName":"ペナルティプロテクト","default":false},{"fullName":"風評被害","default":false}]},{"picklist":"lead_Rival__c","values":[{"fullName":"CA","default":false},{"fullName":"GMO","default":false},{"fullName":"PLAN-B","default":false},{"fullName":"アイオイクス","default":false},{"fullName":"アウンコンサル","default":false},{"fullName":"フルスピード","default":false}]},{"picklist":"lead_WebPromotion__c","values":[{"fullName":"SEO対策","default":true},{"fullName":"アドネットワーク","default":false},{"fullName":"コンテンツマーケ","default":false},{"fullName":"リスティング","default":false}]}]},{"fullName":"analytics","picklistValues":[{"picklist":"lead_AttackProduct__c","values":[{"fullName":"EFO","default":false},{"fullName":"KIJITASU","default":false},{"fullName":"SEO%EF%BC%88外部リンク%EF%BC%89","default":false},{"fullName":"イージス","default":false},{"fullName":"クローバー","default":false},{"fullName":"コンサル","default":false},{"fullName":"サイト制作","default":false},{"fullName":"テキスト制作","default":false},{"fullName":"ペナルティプロテクト","default":false},{"fullName":"風評被害","default":false}]},{"picklist":"lead_Rival__c","values":[{"fullName":"CA","default":false},{"fullName":"GMO","default":false},{"fullName":"PLAN-B","default":false},{"fullName":"アイオイクス","default":false},{"fullName":"アウンコンサル","default":false},{"fullName":"フルスピード","default":false}]},{"picklist":"lead_WebPromotion__c","values":[{"fullName":"SEO対策","default":true},{"fullName":"アドネットワーク","default":false},{"fullName":"コンテンツマーケ","default":false},{"fullName":"リスティング","default":false}]}]},{"fullName":"taisaku","picklistValues":[{"picklist":"lead_AttackProduct__c","values":[{"fullName":"EFO","default":false},{"fullName":"KIJITASU","default":false},{"fullName":"SEO%EF%BC%88外部リンク%EF%BC%89","default":false},{"fullName":"イージス","default":false},{"fullName":"クローバー","default":false},{"fullName":"コンサル","default":false},{"fullName":"サイト制作","default":false},{"fullName":"テキスト制作","default":false},{"fullName":"ペナルティプロテクト","default":false},{"fullName":"風評被害","default":false}]},{"picklist":"lead_Rival__c","values":[{"fullName":"CA","default":false},{"fullName":"GMO","default":false},{"fullName":"PLAN-B","default":false},{"fullName":"アイオイクス","default":false},{"fullName":"アウンコンサル","default":false},{"fullName":"フルスピード","default":false}]},{"picklist":"lead_WebPromotion__c","values":[{"fullName":"SEO対策","default":true},{"fullName":"アドネットワーク","default":false},{"fullName":"コンテンツマーケ","default":false},{"fullName":"リスティング","default":false}]}]}]}}'),
            recordTypeFullNames(availableRecordTypes_Lead)
        ));
        picklistValuesJSON_Lead_lead_AttackProduct_c = System.JSON.serialize(Lead.SObjectType.lead_AttackProduct__c.getDescribe().getPicklistValues());
        picklistValuesJSON_Lead_lead_WebPromotion_c = System.JSON.serialize(Lead.SObjectType.lead_WebPromotion__c.getDescribe().getPicklistValues());
        picklistValuesJSON_Lead_lead_Rival_c = System.JSON.serialize(Lead.SObjectType.lead_Rival__c.getDescribe().getPicklistValues());
        try {
            mainSObjectType = Lead.SObjectType;
            setPageReferenceFactory(new PageReferenceFactory());
            
            mainQuery = new SkyEditor2.Query('Lead');
            mainQuery.addField('OwnerId');
            mainQuery.addField('Company');
            mainQuery.addField('lead_AccountKana__c');
            mainQuery.addField('Phone');
            mainQuery.addField('LastName');
            mainQuery.addField('FirstName');
            mainQuery.addField('lead_kana__c');
            mainQuery.addField('lead_Division__c');
            mainQuery.addField('Title');
            mainQuery.addField('Email');
            mainQuery.addField('Status1__c');
            mainQuery.addField('Status2__c');
            mainQuery.addField('lead_NGmemo__c');
            mainQuery.addField('Website');
            mainQuery.addField('lead_Relation__c');
            mainQuery.addField('lead_lead1__c');
            mainQuery.addField('lead_lead2__c');
            mainQuery.addField('Lead_WEBsite_Quality__c');
            mainQuery.addField('lead_CompanySize__c');
            mainQuery.addField('lead_BudgetMonth1__c');
            mainQuery.addField('lead_AttackProduct__c');
            mainQuery.addField('lead_hope__c');
            mainQuery.addField('lead_Timing1__c');
            mainQuery.addField('lead_NextContactDay__c');
            mainQuery.addField('lead_WebPromotion__c');
            mainQuery.addField('lead_Rival__c');
            mainQuery.addField('lead_PromotionContents__c');
            mainQuery.addField('PostalCode');
            mainQuery.addField('State');
            mainQuery.addField('City');
            mainQuery.addField('Street');
            mainQuery.addField('lead_BusinessModel__c');
            mainQuery.addField('lead_BusinessContent__c');
            mainQuery.addField('Description');
            mainQuery.addFieldAsOutput('Name');
            mainQuery.addFieldAsOutput('RecordTypeId');
            mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
                .limitRecords(1);
            
            
            
            mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
            
            queryMap = new Map<String, SkyEditor2.Query>();
            SkyEditor2.Query query;
            
            
            p_showHeader = true;
            p_sidebar = true;
            addInheritParameter('RecordTypeId', 'RecordType');
            init();
            
            if (record.Id == null) {
                
                saveOldValues();
                
            }

            
            
        }  catch (SkyEditor2.Errors.FieldNotFoundException e) {
            fieldNotFound(e);
        } catch (SkyEditor2.Errors.RecordNotFoundException e) {
            recordNotFound(e);
        } catch (SkyEditor2.ExtenderException e) {
            e.setMessagesToPage();
        }
    }
    

    private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    
    private static testMethod void testPageMethods() {        SVE_20150408194724 extension = new SVE_20150408194724(new ApexPages.StandardController(new Lead()));
        SkyEditor2.Messages.clear();
        extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
        SkyEditor2.Messages.clear();
        extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
        SkyEditor2.Messages.clear();
        extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

        Integer defaultSize;
        extension.getComponent200OptionsJS();
        extension.getComponent182OptionsJS();
        extension.getComponent193OptionsJS();
    }
    public String getComponent200OptionsJS() {
        return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
        Lead.getSObjectType(),
        SObjectType.Lead.fields.lead_AttackProduct__c.getSObjectField()
        ));
        }
    public String getComponent182OptionsJS() {
        return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
        Lead.getSObjectType(),
        SObjectType.Lead.fields.lead_WebPromotion__c.getSObjectField()
        ));
        }
    public String getComponent193OptionsJS() {
        return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
        Lead.getSObjectType(),
        SObjectType.Lead.fields.lead_Rival__c.getSObjectField()
        ));
        }
    static Set<String> recordTypeFullNames(RecordType[] records) {
        Set<String> result = new Set<String>();
        for (RecordType r : records) {
            result.add(r.DeveloperName);
            if (r.NamespacePrefix != null) {
                result.add(r.NamespacePrefix + '__' + r.DeveloperName);
            }
        }
        return result;
    }
    
    static Object filterMetadataJSON(Object metadata, Set<String> recordTypeFullNames) {
        Map<String, Object> metadataMap = (Map<String, Object>) metadata;
        Map<String, Object> customObject = (Map<String, Object>) metadataMap.get('CustomObject');
        List<Object> recordTypes = (List<Object>) customObject.get('recordTypes');
        for (Integer i = recordTypes.size() - 1; i >= 0; i--) {
            Map<String, Object> recordType = (Map<String, Object>)recordTypes[i];
            String fullName = (String)recordType.get('fullName');
            if (! recordTypeFullNames.contains(fullName)) {
                recordTypes.remove(i);
                continue;
            }
        }
        return metadata;
    }
    
    private static testMethod void testRecordTypeFullNames() {
        Set<String> result = recordTypeFullNames(new RecordType[] {
            new RecordType(DeveloperName = 'TestRecordType')
        });
        System.assertEquals(result.size(), 1);
        System.assert(result.contains('TestRecordType'));
    }
    
    private static testMethod void testFilterMetadataJSON() {
        String json = '{"CustomObject":{"recordTypes":[{"fullName":"RecordType1"},{"fullName":"RecordType2"},{"fullName":"RecordType3"}]}}';        Set<String> recordTypeSet = new Set<String>();
        recordTypeSet.add('RecordType2');
        Object metadata = System.JSON.deserializeUntyped(json);
        Map<String, Object> result = (Map<String, Object>) filterMetadataJSON(metadata, recordTypeSet);
        Map<String, Object> customObject = (Map<String, Object>) result.get('CustomObject');
        List<Object> recordTypes = (List<Object>) customObject.get('recordTypes');
        System.assertEquals(recordTypes.size(), 1);
        Map<String, Object> recordType = (Map<String, Object>) recordTypes[0];
        System.assertEquals('RecordType2', recordType.get('fullName'));
    }
    with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
        public PageReference newPageReference(String url) {
            return new PageReference(url);
        }
    }
}