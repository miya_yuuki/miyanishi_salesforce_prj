@isTest
private with sharing class AEGISTest{
	private static testMethod void testPageMethods() {		AEGIS extension = new AEGIS(new ApexPages.StandardController(new AEGIS__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testComponent76() {
		AEGIS.Component76 Component76 = new AEGIS.Component76(new List<Landing__c>(), new List<AEGIS.Component76Item>(), new List<Landing__c>(), null);
		Component76.create(new Landing__c());
		System.assert(true);
	}
	
	private static testMethod void testComponent50() {
		AEGIS.Component50 Component50 = new AEGIS.Component50(new List<AEGIS2__c>(), new List<AEGIS.Component50Item>(), new List<AEGIS2__c>(), null);
		Component50.create(new AEGIS2__c());
		System.assert(true);
	}
	
}