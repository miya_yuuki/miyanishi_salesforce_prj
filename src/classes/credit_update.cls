global with sharing class credit_update extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public Account record{get;set;}	
			
	
		public Component3 Component3 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component11_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component11_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component11_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component11_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component52_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component52_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component52_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component52_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component6_val {get;set;}	
		public SkyEditor2.TextHolder Component6_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component8_val {get;set;}	
		public SkyEditor2.TextHolder Component8_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component10_val {get;set;}	
		public SkyEditor2.TextHolder Component10_op{get;set;}	
			
	public String recordTypeRecordsJSON_Account {get; private set;}
	public String defaultRecordTypeId_Account {get; private set;}
	public String metadataJSON_Account {get; private set;}
	{
	setApiVersion(31.0);
	}
		public credit_update(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = Account.fields.Name;
		f = Account.fields.accounts_receivable__c;
		f = Account.fields.account_CreditLevel_Copy__c;
		f = Account.fields.Client_No1__c;
		f = Account.fields.BillingPostalCode;
		f = Account.fields.BillingState;
		f = Account.fields.BillingCity;
		f = Account.fields.BillingStreet;
		f = Account.fields.account_BuildingName__c;
		f = Account.fields.President__c;
		f = Account.fields.account_CreditLevel__c;
		f = Account.fields.account_CreditDay__c;
		f = Account.fields.CompanyCheck__c;
		f = Account.fields.hansyaDay__c;
		f = Account.fields.hansyaMemo__c;
 f = Account.fields.hansyaDay__c;
 f = Account.fields.account_CreditDay__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = Account.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component11_from = new SkyEditor2__SkyEditorDummy__c();	
				Component11_to = new SkyEditor2__SkyEditorDummy__c();	
				Component11_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component11_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component52_from = new SkyEditor2__SkyEditorDummy__c();	
				Component52_to = new SkyEditor2__SkyEditorDummy__c();	
				Component52_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component52_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				Component6_val = new SkyEditor2__SkyEditorDummy__c();	
				Component6_op = new SkyEditor2.TextHolder();	
					
				Component8_val = new SkyEditor2__SkyEditorDummy__c();	
				Component8_op = new SkyEditor2.TextHolder();	
					
				Component10_val = new SkyEditor2__SkyEditorDummy__c();	
				Component10_op = new SkyEditor2.TextHolder();	
					
				queryMap.put(	
					'Component3',	
					new SkyEditor2.Query('Account')
						.addFieldAsOutput('Client_No1__c')
						.addFieldAsOutput('Name')
						.addFieldAsOutput('BillingPostalCode')
						.addFieldAsOutput('BillingState')
						.addFieldAsOutput('BillingCity')
						.addFieldAsOutput('BillingStreet')
						.addFieldAsOutput('account_BuildingName__c')
						.addFieldAsOutput('President__c')
						.addFieldAsOutput('accounts_receivable__c')
						.addField('account_CreditLevel__c')
						.addField('account_CreditDay__c')
						.addField('CompanyCheck__c')
						.addField('hansyaDay__c')
						.addField('hansyaMemo__c')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component11_from, 'SkyEditor2__Date__c', 'hansyaDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component11_to, 'SkyEditor2__Date__c', 'hansyaDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component11_isNull, 'SkyEditor2__Date__c', 'hansyaDay__c', Component11_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component52_from, 'SkyEditor2__Date__c', 'account_CreditDay__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component52_to, 'SkyEditor2__Date__c', 'account_CreditDay__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component52_isNull, 'SkyEditor2__Date__c', 'account_CreditDay__c', Component52_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component6_val, 'SkyEditor2__Text__c', 'Name', Component6_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component8_val, 'SkyEditor2__Text__c', 'accounts_receivable__c', Component8_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component10_val, 'SkyEditor2__Text__c', 'account_CreditLevel_Copy__c', Component10_op, true, 0, false ))
				);	
					
					Component3 = new Component3(new List<Account>(), new List<Component3Item>(), new List<Account>(), null);
				listItemHolders.put('Component3', Component3);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(Account.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = true;
			execInitialSearch = false;
			presetSystemParams();
			Component3.extender = this.extender;
			initSearch();
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_Account_Name() { 
			return getOperatorOptions('Account', 'Name');	
		}	
		public List<SelectOption> getOperatorOptions_Account_accounts_receivable_c() { 
			return getOperatorOptions('Account', 'accounts_receivable__c');	
		}	
		public List<SelectOption> getOperatorOptions_Account_account_CreditLevel_Copy_c() { 
			return getOperatorOptions('Account', 'account_CreditLevel_Copy__c');	
		}	
			
			
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public Account record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, Account record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.ListItemHolder {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (Account)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	public Account Component3_table_Conversion { get { return new Account();}}
	
	public String Component3_table_selectval { get; set; }
	
	
			
	}