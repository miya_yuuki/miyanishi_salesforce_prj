global with sharing class operation_control extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public operator__c record{get;set;}	
			
	
		public Component3 Component3 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component52_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component52_to{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component52_isNull{get;set;}	
		public SkyEditor2.TextHolder.OperatorHolder Component52_isNull_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component43_val {get;set;}	
		public SkyEditor2.TextHolder Component43_op{get;set;}	
		public List<SelectOption> valueOptions_teamspirit_AtkApply_c_contract_naiyou_c {get;set;}
			
		public teamspirit__AtkApply__c Component45_val {get;set;}	
		public SkyEditor2.TextHolder Component45_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component47_val {get;set;}	
		public SkyEditor2.TextHolder Component47_op{get;set;}	
		public List<SelectOption> valueOptions_teamspirit_AtkApply_c_teamspirit_Status_c {get;set;}
			
	public String recordTypeRecordsJSON_operator_c {get; private set;}
	public String defaultRecordTypeId_operator_c {get; private set;}
	public String metadataJSON_operator_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public operation_control(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = teamspirit__AtkApply__c.fields.contract_naiyou__c;
		f = teamspirit__AtkApply__c.fields.OutsourcingAccount__c;
		f = teamspirit__AtkApply__c.fields.teamspirit__Status__c;
		f = operator__c.fields.orijginal_order__c;
		f = operator__c.fields.Name;
		f = operator__c.fields.unit__c;
		f = operator__c.fields.contract_company__c;
		f = operator__c.fields.pay_contract_start__c;
		f = operator__c.fields.pay_deadline__c;
		f = operator__c.fields.pay_method__c;
		f = operator__c.fields.income__c;
		f = operator__c.fields.margine__c;
		f = operator__c.fields.charge_dept_name__c;
 f = operator__c.fields.pay_contract_start__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = operator__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component52_from = new SkyEditor2__SkyEditorDummy__c();	
				Component52_to = new SkyEditor2__SkyEditorDummy__c();	
				Component52_isNull = new SkyEditor2__SkyEditorDummy__c();	
				Component52_isNull_op = new SkyEditor2.TextHolder.OperatorHolder('eq'); 	
					
				teamspirit__AtkApply__c lookupObjComponent38orijginalorderc = new teamspirit__AtkApply__c();	
				Component43_val = new SkyEditor2__SkyEditorDummy__c();	
				Component43_op = new SkyEditor2.TextHolder();	
				valueOptions_teamspirit_AtkApply_c_contract_naiyou_c = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : teamspirit__AtkApply__c.contract_naiyou__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_teamspirit_AtkApply_c_contract_naiyou_c.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component45_val = lookupObjComponent38orijginalorderc;	
				Component45_op = new SkyEditor2.TextHolder();	
					
				Component47_val = new SkyEditor2__SkyEditorDummy__c();	
				Component47_op = new SkyEditor2.TextHolder();	
				valueOptions_teamspirit_AtkApply_c_teamspirit_Status_c = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : teamspirit__AtkApply__c.teamspirit__Status__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_teamspirit_AtkApply_c_teamspirit_Status_c.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				queryMap.put(	
					'Component3',	
					new SkyEditor2.Query('operator__c')
						.addFieldAsOutput('orijginal_order__c')
						.addFieldAsOutput('Name')
						.addFieldAsOutput('unit__c')
						.addFieldAsOutput('contract_company__c')
						.addField('pay_contract_start__c')
						.addField('pay_deadline__c')
						.addField('pay_method__c')
						.addField('income__c')
						.addField('margine__c')
						.addField('charge_dept_name__c')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component52_from, 'SkyEditor2__Date__c', 'pay_contract_start__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component52_to, 'SkyEditor2__Date__c', 'pay_contract_start__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component52_isNull, 'SkyEditor2__Date__c', 'pay_contract_start__c', Component52_isNull_op, true,0,false )) 
						
						.addListener(new SkyEditor2.QueryWhereRegister(Component43_val, 'SkyEditor2__Text__c', 'orijginal_order__r.contract_naiyou__c',teamspirit__AtkApply__c.fields.contract_naiyou__c, Component43_op, true, false,true,0,false,'orijginal_order__c',operator__c.fields.orijginal_order__c )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component45_val, 'OutsourcingAccount__c', 'orijginal_order__r.OutsourcingAccount__c',teamspirit__AtkApply__c.fields.OutsourcingAccount__c, Component45_op, true, false,true,0,false,'orijginal_order__c',operator__c.fields.orijginal_order__c )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component47_val, 'SkyEditor2__Text__c', 'orijginal_order__r.teamspirit__Status__c',teamspirit__AtkApply__c.fields.teamspirit__Status__c, Component47_op, true, false,true,0,false,'orijginal_order__c',operator__c.fields.orijginal_order__c )) 
				);	
					
					Component3 = new Component3(new List<operator__c>(), new List<Component3Item>(), new List<operator__c>(), null);
				listItemHolders.put('Component3', Component3);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(operator__c.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = false;
			execInitialSearch = false;
			presetSystemParams();
			Component3.extender = this.extender;
			initSearch();
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_teamspirit_AtkApply_c_contract_naiyou_c() { 
			return getOperatorOptions('teamspirit__AtkApply__c', 'contract_naiyou__c');	
		}	
		public List<SelectOption> getOperatorOptions_teamspirit_AtkApply_c_OutsourcingAccount_c() { 
			return getOperatorOptions('teamspirit__AtkApply__c', 'OutsourcingAccount__c');	
		}	
		public List<SelectOption> getOperatorOptions_teamspirit_AtkApply_c_teamspirit_Status_c() { 
			return getOperatorOptions('teamspirit__AtkApply__c', 'teamspirit__Status__c');	
		}	
			
			
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public operator__c record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, operator__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.ListItemHolder {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (operator__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	public operator__c Component3_table_Conversion { get { return new operator__c();}}
	
	public String Component3_table_selectval { get; set; }
	
	
			
	}