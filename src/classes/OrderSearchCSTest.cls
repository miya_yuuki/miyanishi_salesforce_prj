@isTest
private with sharing class OrderSearchCSTest{
		private static testMethod void testPageMethods() {	
			OrderSearchCS page = new OrderSearchCS(new ApexPages.StandardController(new Order__c()));	
			page.getOperatorOptions_Order_c_Order_Phase_c_multi();	
			page.getOperatorOptions_Order_c_ProductType_c();	
			page.getOperatorOptions_Order_c_Contract_SalesPerson1_c();	
			page.getOperatorOptions_Order_c_CSStatusInput_c();	
			page.getOperatorOptions_Order_c_Opportunity_Support_c();	
			page.getOperatorOptions_Order_c_CSDay_c();	
			page.getOperatorOptions_Order_c_AccountName_c();	
			page.getOperatorOptions_Order_c_CSUpcellInput_c();	
			page.getOperatorOptions_Order_c_karute_c();	
			page.getOperatorOptions_Order_c_CSUpcellDay_c();	
			page.getOperatorOptions_Order_c_Order_StartDate_c();	
			page.getOperatorOptions_Order_c_Order_EndClient1_c();	
			page.getOperatorOptions_Order_c_Order_EndDate_c();	
			page.getOperatorOptions_Order_c_DeliveryNoTotalAmount_c();	
			System.assert(true);
		}	
			
	private static testMethod void testComponent3() {
		OrderSearchCS.Component3 Component3 = new OrderSearchCS.Component3(new List<Order__c>(), new List<OrderSearchCS.Component3Item>(), new List<Order__c>(), null);
		Component3.create(new Order__c());
		Component3.doDeleteSelectedItems();
		System.assert(true);
	}
	
}