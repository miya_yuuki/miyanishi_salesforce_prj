global class OppItemCopy {
	webService static String OppItemId(String id, String origin_id, String owner_team, String owner_name, String StageName) {
		String resultText = '';
		Integer resCount = 0;

		if(StageName == '受注' || StageName == 'デッド') {
			resultText += 'ERROR: 受注 または デッド フェーズの商談では「商談商品コピー」を実行出来ません';
			return resultText;
		}

		if(origin_id != null && origin_id != '') {
			for(AggregateResult ar : [
				SELECT
					OP.Id,
					OP.RecordTypeId RTId,
					OP.RecordType.Name RTname,
					SUM(OP.ContractMonths__c) CMon,
					OP.BillingTiming__c BillT,
					OP.AutomaticUpdate__c AUpd,
					OP.Item_Detail_ProductView__c IDV,
					OP.Item_Production__c IpD,
					SUM(OP.Price__c) price,
					SUM(OP.Quantity__c) quantity,
					SUM(OP.BuyingupPrice__c) BPrice,
					OP.Item_Classification__c ItC,
					OP.Item_Strategy_url_del__c ItS,
					OP.TextType__c TxT,
					OP.TextWarranty__c TxW,
					OP.TextCalculation__c TxCL,
					SUM(OP.Item_keyword_letter_count__c) TxL,
					OP.Text_Accep_TedNorm__c TxAT,
					OP.Text_standard__c TxS,
					OP.Item_Keyword_strategy_keyword__c ItK,
					OP.Item_Estimate_level__c ItE,
					OP.Item_DeliveryDate__c ItDD,
					OP.Item_Commission_rate__c ItCr,
					P.Id pId,
					P.Name pName,
					P.Product_DeliveryExistence__c PDE,
					P.ProductGroup__c PG
				FROM
					OpportunityProduct__c OP,
					OpportunityProduct__c.Opportunity__r O,
					OpportunityProduct__c.Product__r P
				WHERE
					O.Id = :origin_id AND P.validity__c = true
				GROUP BY
					OP.Id,
					OP.RecordTypeId,
					OP.RecordType.Name,
					OP.BillingTiming__c,
					OP.AutomaticUpdate__c,
					P.Id,
					P.Name,
					P.Product_DeliveryExistence__c,
					P.ProductGroup__c,
					OP.Item_Detail_ProductView__c,
					OP.Item_Production__c,
					OP.Item_Classification__c,
					OP.Item_Strategy_url_del__c,
					OP.TextType__c,
					OP.TextWarranty__c,
					OP.TextCalculation__c,
					OP.Text_Accep_TedNorm__c,
					OP.Text_standard__c,
					OP.Item_Keyword_strategy_keyword__c,
					OP.Item_Estimate_level__c,
					OP.Item_DeliveryDate__c,
					OP.Item_Commission_rate__c
				order by
					OP.Id,
					P.Id
				limit
					2000
			]) {

				String opId = String.valueOf(ar.get('Id'));								// 商談商品ID
				String RTId = String.valueOf(ar.get('RTId'));							// レコードタイプ(Id)
				String RTname = String.valueOf(ar.get('RTname'));						// レコードタイプ(Name)
				Integer Cmon = Integer.valueOf(ar.get('CMon'));							// 契約月数
				String BillT = String.valueOf(ar.get('BillT'));							// 請求タイミング
				String AUpd = String.valueOf(ar.get('AUpd'));							// 自動更新有無
				String PId = String.valueOf(ar.get('PId'));								// 商 品(Id)
				String Pname = String.valueOf(ar.get('Pname'));							// 商 品(Name)
				String PDE = String.valueOf(ar.get('PDE'));								// 商 品(納品有無)
				String PG = String.valueOf(ar.get('PG'));								// 商品グループ
				String Item_Detail_ProductView = String.valueOf(ar.get('IDV'));			// 商品内容.
				String Item_Production = String.valueOf(ar.get('IpD'));					// サイト制作/品名
				Integer Price = Integer.valueOf(ar.get('price'));						// 単価
				Integer Quantity = Integer.valueOf(ar.get('quantity'));					// 数量
				Integer BPrice = Integer.valueOf(ar.get('BPrice'));						// 仕入金額
				String Item_Classification = String.valueOf(ar.get('ItC'));				// キーワード種別
				String Item_Strategy_url_del = String.valueOf(ar.get('ItS'));			// 対策URL
				String TextType = String.valueOf(ar.get('TxT'));						// 記事タイプ
				String TextWarranty = String.valueOf(ar.get('TxW'));					// 送客保証
				Boolean TextCalculation = Boolean.valueOf(ar.get('TxCL'));				// 文字数金額集計
				Integer Item_keyword_letter_count = Integer.valueOf(ar.get('TxL'));		// 文字数
				String Text_Accep_TedNorm = String.valueOf(ar.get('TxAT'));				// 承認レベル
				String Text_standard = String.valueOf(ar.get('TxS'));					// 記事企画有無
				String Item_Keyword_strategy_keyword = String.valueOf(ar.get('ItK'));	// キーワード
				String Item_Estimate_level = String.valueOf(ar.get('ItE'));				// 見積レベル
				Date Item_DeliveryDate = Date.valueOf(ar.get('ItDD'));					// 初回納品予定日
				String Item_Commission_rate = String.valueOf(ar.get('ItCr'));			// 代理店手数料率

				OpportunityProduct__c copyOppProduct = new OpportunityProduct__c (
					Opportunity__c = id,												// 商談ID
					RecordTypeId = RTId,												// レコードタイプ(Id)
					ContractMonths__c = CMon,											// 契約月数
					BillingTiming__c = BillT,											// 請求タイミング
					AutomaticUpdate__c = AUpd,											// 自動更新有無
					Product__c = PId,													// 商 品(Id)
					Item_Detail_ProductView__c = Item_Detail_ProductView,				// 商品内容.
					Item_Production__c = Item_Production,								// サイト制作/品名
					Price__c = Price,													// 単価
					Quantity__c = Quantity,												// 数量
					BuyingupPrice__c = BPrice,											// 仕入金額
					Item_Classification__c = Item_Classification,						// キーワード種別
					Item_Strategy_url_del__c = Item_Strategy_url_del,					// 対策URL
					TextCalculation__c = TextCalculation,								// 文字数金額集計
					Item_keyword_letter_count__c = Item_keyword_letter_count,			// 文字数
					Text_standard__c = Text_standard,									// 記事企画有無
					Item_Commission_rate__c = Item_Commission_rate,						// 代理店手数料率

					SalesPercentage1__c = 100,											// 担当① 案分比率
					SalesPercentage2__c = 0,											// 担当② 案分比率
					SalesPersonMain__c = owner_name,									// 案分主担当
					SalesPersonMainUnit__c = owner_team									// 案分主担当ユニット
				);

				if(PDE == '有') {
					copyOppProduct.Item_DeliveryDate__c = Item_DeliveryDate;			// 初回納品予定日
				}
				if(PG == 'SEO') {
					copyOppProduct.Item_Keyword_strategy_keyword__c = Item_Keyword_strategy_keyword;	// キーワード
				}
				if(RTname == 'SEO用') {
					copyOppProduct.Item_Estimate_level__c = Item_Estimate_level;			// 見積レベル
				}
				if(RTname == 'サグーワークス/KIJITASU用') {
					copyOppProduct.TextType__c = TextType;								// 記事タイプ
					copyOppProduct.TextWarranty__c = TextWarranty;						// 送客保証
					copyOppProduct.Text_Accep_TedNorm__c = Text_Accep_TedNorm;			// 承認レベル
				}

				List<OpportunityProduct__c> insOppProduct = new List<OpportunityProduct__c>();

				insOppProduct.add(copyOppProduct);
				if(insOppProduct.size() > 0) {
					insert insOppProduct;
					resCount ++;
				}
			}
			System.debug('商談商品コピーを完了しました' + resultText);
			resultText = '商談商品' + resCount + '件をコピーしました';
			return resultText;
			//return null;
		}
		else {
			resultText += 'ERROR: コピー商談名が入力されていません';
			return resultText;
		}
	}
}