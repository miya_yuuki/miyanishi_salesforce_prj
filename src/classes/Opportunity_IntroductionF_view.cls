global with sharing class Opportunity_IntroductionF_view extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public OpportunityProduct__c record {get{return (OpportunityProduct__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	
	
	{
	setApiVersion(31.0);
	}
	public Opportunity_IntroductionF_view(ApexPages.StandardController controller) {
		super(controller);


		SObjectField f;

		f = OpportunityProduct__c.fields.Account__c;
		f = OpportunityProduct__c.fields.sisakuompany__c;
		f = OpportunityProduct__c.fields.Opportunity__c;
		f = OpportunityProduct__c.fields.Price__c;
		f = OpportunityProduct__c.fields.Product__c;
		f = OpportunityProduct__c.fields.Quantity__c;
		f = OpportunityProduct__c.fields.Item_Detail_Product__c;
		f = OpportunityProduct__c.fields.TotalPrice__c;
		f = OpportunityProduct__c.fields.ProductType__c;
		f = OpportunityProduct__c.fields.SalesPerson__c;
		f = OpportunityProduct__c.fields.Opportunity_DeliveryExistence__c;
		f = OpportunityProduct__c.fields.DeliveryStandards__c;
		f = OpportunityProduct__c.fields.Name;
		f = OpportunityProduct__c.fields.Item_Special_instruction__c;
		f = OpportunityProduct__c.fields.SalesPersonMainUnit__c;
		f = OpportunityProduct__c.fields.SalesPersonSubUnit__c;
		f = OpportunityProduct__c.fields.SalesPersonMain__c;
		f = OpportunityProduct__c.fields.SalesPersonSub__c;
		f = OpportunityProduct__c.fields.SalesPercentage1__c;
		f = OpportunityProduct__c.fields.SalesPercentage2__c;
		f = OpportunityProduct__c.fields.CreatedById;
		f = OpportunityProduct__c.fields.LastModifiedById;
		f = OpportunityProduct__c.fields.CreatedDate;
		f = OpportunityProduct__c.fields.LastModifiedDate;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = OpportunityProduct__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('OpportunityProduct__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Account__c');
			mainQuery.addFieldAsOutput('sisakuompany__c');
			mainQuery.addFieldAsOutput('Opportunity__c');
			mainQuery.addFieldAsOutput('Price__c');
			mainQuery.addFieldAsOutput('Product__c');
			mainQuery.addFieldAsOutput('Quantity__c');
			mainQuery.addFieldAsOutput('Item_Detail_Product__c');
			mainQuery.addFieldAsOutput('TotalPrice__c');
			mainQuery.addFieldAsOutput('ProductType__c');
			mainQuery.addFieldAsOutput('SalesPerson__c');
			mainQuery.addFieldAsOutput('Opportunity_DeliveryExistence__c');
			mainQuery.addFieldAsOutput('DeliveryStandards__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('Item_Special_instruction__c');
			mainQuery.addFieldAsOutput('SalesPersonMainUnit__c');
			mainQuery.addFieldAsOutput('SalesPersonSubUnit__c');
			mainQuery.addFieldAsOutput('SalesPersonMain__c');
			mainQuery.addFieldAsOutput('SalesPersonSub__c');
			mainQuery.addFieldAsOutput('SalesPercentage1__c');
			mainQuery.addFieldAsOutput('SalesPercentage2__c');
			mainQuery.addFieldAsOutput('CreatedById');
			mainQuery.addFieldAsOutput('LastModifiedById');
			mainQuery.addFieldAsOutput('CreatedDate');
			mainQuery.addFieldAsOutput('LastModifiedDate');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			
			p_showHeader = true;
			p_sidebar = true;
			addInheritParameter('Opportunity__c', 'CF00N10000005jJYH_lkid');
			addInheritParameter('RecordTypeId', 'RecordType');
			init();
			
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}