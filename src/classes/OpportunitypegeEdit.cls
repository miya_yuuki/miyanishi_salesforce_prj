global with sharing class OpportunitypegeEdit extends SkyEditor2.SkyEditorPageBaseWithSharing {

	public String recordTypeDevName{get; set;}
	public List<RecordType> recordTypes{get; set;}
	public String getRecordTypesJSON() {
		return System.JSON.serialize(recordTypes);
	}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}

	{
		setApiVersion(31.0);
	}

	public OpportunitypegeEdit(ApexPages.StandardController controller) {
		super(controller);
		recordTypeSelector = new SkyEditor2.RecordTypeSelector(Opportunity.SObjectType);
		Map<String, String> params = ApexPages.currentPage().getParameters();
		String recordTypeId = params.get('RecordType');
		hidePageBody=false;
		recordTypeDevName = params.get('RecordTypeName');
		DescribeSObjectResult describe = SObjectType.Opportunity;
		List<Schema.RecordTypeInfo> types = describe.getRecordTypeInfos();
		SkyEditor2.Messages.clear();
		List<Id> ids = new List<Id>();
		Set<Id> availableIds = new Set<Id>();
		for (RecordTypeInfo info : types) {
			Id id = info.getRecordTypeId();
			if (id != '012000000000000AAA' && info.isAvailable()) {
				availableIds.add(id);
			}
		}
		recordTypes = new List<RecordType>();
		p_showHeader = true;
		p_sidebar = true;
		presetSystemParams();
		List<RecordTYpe> allTypes = [SELECT Id, Name, Description, DeveloperName FROM RecordType WHERE SobjectType = 'Opportunity' AND IsActive = true];
		for (RecordType type : allTypes) {
			if (recordTypeId != null && type.Id == (Id)recordTypeId) {
				recordTypeDevName = type.DeveloperName;
			}
			if (availableIds.contains(type.Id)) {
				recordTypes.add(type);
			}
		}

		Id recordId = controller.getId();
		if (recordId != null) {
			Opportunity record = [SELECT RecordType.DeveloperName FROM Opportunity WHERE Id = :controller.getId()];
			recordTypeDevName = record.RecordType.DeveloperName;
		}

	}
}