@isTest
private with sharing class nouhinkanriDATATest{
		private static testMethod void testPageMethods() {	
			nouhinkanriDATA page = new nouhinkanriDATA(new ApexPages.StandardController(new TextManagement2__c()));	
			page.getOperatorOptions_TextManagement2_c_Name();	
			page.getOperatorOptions_TextManagement2_c_OrderNo_c();	
			page.getOperatorOptions_TextManagement2_c_RecordTypeId();	
			page.getOperatorOptions_TextManagement2_c_SalesPersonUnit1_c();	
			page.getOperatorOptions_TextManagement2_c_SalesPerson10_c();	
			page.getOperatorOptions_TextManagement2_c_DeliveryPerson_c();	
			page.getOperatorOptions_TextManagement2_c_Account1_c();	
			page.getOperatorOptions_TextManagement2_c_Product11_c();	
			page.getOperatorOptions_TextManagement2_c_ContractMonths_c();	
			page.getOperatorOptions_TextManagement2_c_CloudStatus_c_multi();	
			page.getOperatorOptions_TextManagement2_c_Status_c();	
			page.getOperatorOptions_TextManagement2_c_Item2_DeliveryExistence_c();	
			page.getOperatorOptions_TextManagement2_c_AutomaticUpdate_c();	
			page.getOperatorOptions_TextManagement2_c_BillingTiming_c();	
			page.getOperatorOptions_TextManagement2_c_StartDate_c();	
			page.getOperatorOptions_TextManagement2_c_EndDate_c();	
			System.assert(true);
		}	
			
	private static testMethod void testComponent2() {
		nouhinkanriDATA.Component2 Component2 = new nouhinkanriDATA.Component2(new List<TextManagement2__c>(), new List<nouhinkanriDATA.Component2Item>(), new List<TextManagement2__c>(), new SkyEditor2.RecordTypeSelector(TextManagement2__c.SObjectType), null);
		Component2.create(new TextManagement2__c());
		Component2.doDeleteSelectedItems();
		Component2.setPagesize(10);		Component2.doFirst();
		Component2.doPrevious();
		Component2.doNext();
		Component2.doLast();
		Component2.doSort();
		System.assert(true);
	}
	
}