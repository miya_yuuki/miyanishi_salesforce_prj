public with sharing class InvoiceFieldSetClass {

	public static void prcInvoiceCheckAttachment(List<Attachment> trgAttachment) 
    {
        // ParentId
        String strParentId = '';
        // 件名
        String strName = '';
        // 請求ID
        List<String> strInvoiceIds = new List<String>();
        // 請求オブジェクト
        List<Bill__c> objBill = new List<Bill__c>();
        
        // 添付ファイルのループ
        for (Attachment obj : trgAttachment) {
            // ParentIdを格納
            strParentId = obj.ParentId;
            // 件名
            strName = obj.Name;
            // ParentIdがnullの場合は処理しない
            if (obj.ParentId == null) continue;
            // ParentIdの頭文字が「a0t」以外
            if (strParentId.substring(0, 3) != 'a0t') continue;
            // pdfではない
            if (strName.indexOf('.pdf') == -1) continue;
            // 配列に追加
            strInvoiceIds.add(strParentId);
        }
        
        // 請求レコードを取得
        objBill = [SELECT Id, Bill_BillCheck__c FROM Bill__c WHERE Id IN :strInvoiceIds AND Bill_BillCheck__c = false];
        
        // 「請求書発行済み」にチェック
        for (Bill__c obj : objBill) {
            obj.Bill_BillCheck__c = true;
        }
        
        // 請求レコードをアップデート
        if (objBill.size() > 0) update objBill;
    }
    
	public static void prcInvoiceFieldSet(List<Bill__c> trgBill)
    {
        // 請求ID
        List<String> strIds = new List<String>();
        // 請求オブジェクト
        List<Bill__c> objBill = new List<Bill__c>();
        // 請求商品オブジェクト
        Map<Id, BillProduct__c> mapBill = new Map<Id, BillProduct__c>();
        
        // 請求ID追加
        for (Bill__c obj : trgBill) strIds.add(obj.Id);
        
        // 請求データ取得
        objBill = [
            SELECT 
                Id, 
                    (
                        select 
                            Bill2_Commission_rate__c, 
                            Bill2_SalesPerson__c,
                            Contract_FastOrdersSales__c,
                            SalesUnit1__c,
                            SalesUnit2__c,
                            Bill_BillMemo__c,
                            BillTo_Account__c
                        from Bill__r 
                        order by Name desc
                        limit 1
                    )
            FROM Bill__c
            WHERE Id IN :strIds
        ];
        
        // Mapに追加
        for (Bill__c obj : objBill) {
            if (obj.Bill__r.size() > 0) mapBill.put(obj.Id, obj.Bill__r);
        }
        // 項目に値セット
        for (Bill__c obj : trgBill) {
            // Mapに無ければ処理しない
            if (!mapBill.containsKey(obj.Id)) continue;
            // nullなら処理しない
            if (mapBill.get(obj.Id) == null) continue;
            // 代理店手数料
            obj.Bill_Commission_rate__c = prcSetDec(obj.Bill_Commission_rate__c, mapBill.get(obj.Id).Bill2_Commission_rate__c);
            // 担当営業
            obj.Bill_SalesPerson__c = prcSetStr(obj.Bill_SalesPerson__c, mapBill.get(obj.Id).Bill2_SalesPerson__c);
            // 担当営業ユニット
            obj.SalesUnit1__c = prcSetStr(obj.SalesUnit1__c, mapBill.get(obj.Id).SalesUnit1__c);
            // 受注時担当営業
            obj.Bill_SalesPersonfirst__c = prcSetStr(obj.Bill_SalesPersonfirst__c, mapBill.get(obj.Id).Contract_FastOrdersSales__c);
            // 受注時担当営業ユニット
            obj.SalesUnit2__c = prcSetStr(obj.SalesUnit2__c, mapBill.get(obj.Id).SalesUnit2__c);
            // 請求書備考
            obj.Bill_BillMemo__c = prcSetStr(obj.Bill_BillMemo__c, mapBill.get(obj.Id).Bill_BillMemo__c);
            // 請求取引先
            obj.BillAccountName__c = prcSetStr(obj.BillAccountName__c, mapBill.get(obj.Id).BillTo_Account__c);
        }
    }
    
    private static Decimal prcSetDec(Decimal val1, Decimal val2)
    {
        if (val1 != null) return val1;
        return val2;
    }
    
    private static String prcSetStr(String val1, String val2)
    {
        if (val1 != null) return val1;
        return val2;
    }
    
}