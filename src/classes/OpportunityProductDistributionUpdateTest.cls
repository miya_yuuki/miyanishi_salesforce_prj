@isTest
private class OpportunityProductDistributionUpdateTest {

	private static testMethod void testMethods()
	{
		Test.StartTest();

		// プロセスビルダー、入力チェックを停止
		User stopCheck = new User(Id = Userinfo.getUserId(), StopPB__c = true, StopInputCheck__c = true);
		update stopCheck;

		// 商品オブジェクト
		List<Product__c> testProduct = new List<Product__c>();

		// 商品
		Product__c testProduct1 = new Product__c(
			Name = 'SEO対策×6ヵ月',
			whet__c = 1,
			Classification__c = 'ストック',
			ClassificationSales__c = 'ストック',
			Category__c = 'ソリューション',
			validity__c = true,
			Update__c = '有',
			ProductRecordType__c = 'SEO用'
		);
		// 配列に追加
		testProduct.add(testProduct1);
		// 商品
		Product__c testProduct2 = new Product__c(
			Name = 'サイト制作',
			whet__c = null,
			Classification__c = 'スポット',
			ClassificationSales__c = 'スポット',
			Category__c = 'ソリューション',
			validity__c = true,
			ProductRecordType__c = 'サイト制作用'
		);
		// 配列に追加
		testProduct.add(testProduct2);
		// 商品
		Product__c testProduct3 = new Product__c(
			Name = 'KIJI TASU×3ヶ月',
			whet__c = 1,
			Classification__c = 'ストック',
			ClassificationSales__c = 'スポット',
			Category__c = 'ソリューション',
			validity__c = true,
			Update__c = '有',
			ProductRecordType__c = 'サグーワークス/KIJITASU用'
		);
		// 配列に追加
		testProduct.add(testProduct3);
		// 商品
		Product__c testProduct4 = new Product__c(
			Name = 'Search Cleaner×3ヶ月',
			whet__c = 1,
			Classification__c = 'ストック',
			ClassificationSales__c = 'ストック',
			Category__c = 'ソリューション',
			validity__c = true,
			Update__c = '有',
			ProductRecordType__c = 'Searchcleaner'
		);
		// 配列に追加
		testProduct.add(testProduct4);
		// 商品
		Product__c testProduct5 = new Product__c(
			Name = 'コンサルプラン×3ヶ月',
			whet__c = 1,
			Classification__c = 'ストック',
			ClassificationSales__c = 'ストック',
			Category__c = 'ソリューション',
			validity__c = true,
			Update__c = '有',
			ProductRecordType__c = 'WEBサービス全般（期間あり）'
		);
		// 配列に追加
		testProduct.add(testProduct5);

		// 商品を作成
		insert testProduct;

		List<Account> testAccounts = new List<Account>();
		
		// 取引先
		Account testAccount = new Account(
			Name = 'テスト株式会社',
			account_localName__c = 'テストフリガナ',
			account_Lead__c = '架電',
			account_Lead2__c = '架電',
			Type = '顧客'
		);
		testAccounts.add(testAccount);
		
		// 取引先（パートナー）
		Account testPartnerAccount = new Account(
			Name = 'テストパートナー株式会社',
			account_localName__c = 'テストパートナーフリガナ',
			account_Lead__c = '架電',
			account_Lead2__c = '架電',
			Type = '顧客'
		);
		testAccounts.add(testPartnerAccount);
		
		insert testAccounts;

		// 取引先責任者
		Contact testContact = new Contact(
			FirstName = 'テスト姓',
			LastName = 'テスト名',
			AccountId = testAccount.Id,
			report__c = '送信不要'
		);
		insert testContact;

		// 商談
		Opportunity testOpp = new Opportunity(
			Name = 'テスト商談',
			OwnerId = Userinfo.getUserId(),
			AccountId = testAccount.Id,
			Opportunity_Title__c = testContact.Id,
			Opportunity_Type__c = 'セールスP（クライアント請求/初回期間支払）',
			Opportunity_Partner__c = testPartnerAccount.Id,
			Opportunity_lead1__c = '架電',
			Opportunity_lead2__c = '架電',
			CloseDate = System.today().addDays(5),
			StageName = 'D（検討段階）',
			Opportunity_BillEmail__c = 'test@test.com',
			Opportunity_BillRole__c = '役割',
			Opportunity_BillTitle__c = '役職',
			Opportunity_BillPostalCode__c = '999-9999',
			Opportunity_BillPrefecture__c = '都道府県',
			Opportunity_BillPrefecture2__c = '都道府県',
			Opportunity_BillPhone__c = '03-999-9999',
			Opportunity_BillAddress__c = '住所',
			Opportunity_BillCity__c = '市区群'
		);
		insert testOpp;

		// レコードタイプオブジェクト
		List<RecordType> recType = new List<RecordType>();
		// レコードタイプのMap
		Map<String, Id> mapType = new Map<String, Id>();
		// レコードタイプを取得
		recType = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'OpportunityProduct__c'];
		// Mapにセット
		for (RecordType obj : recType) mapType.put(obj.DeveloperName, obj.Id);


		// 商談商品オブジェクト
		List<OpportunityProduct__c> testItem = new List<OpportunityProduct__c>();

		// 商談商品
		OpportunityProduct__c testItem1 = new OpportunityProduct__c(
			Opportunity__c = testOpp.Id,
			Item_Entry_date__c = System.today(),
			Item_Contract_start_date__c = System.today(),
			RecordTypeId = mapType.get('SEO'),
			Product__c = testProduct1.Id,
			Price__c = 100,
			Quantity__c = 1,
			Item_Commission_rate__c = '5%',
			Item_Keyword_strategy_keyword__c = 'test keyword',
			Item_Classification__c = 'メイン1',
			Item_Estimate_level__c = 'LV_00',
			Item_Automatic_updating__c = '有',
			Item_Link_set__c = 1,
			Item_Strategy_url_del__c = 'http://www.test.com/',
			SEO_sisaku__c = '受注額施策',
			OutsourcingApplication__c = '無',
			PictureCount__c = 0
		);
		// 配列に追加
		testItem.add(testItem1);
		// 商談商品
		OpportunityProduct__c testItem2 = new OpportunityProduct__c(
			Opportunity__c = testOpp.Id,
			Item_Entry_date__c = System.today(),
			Item_Contract_start_date__c = System.today(),
			RecordTypeId = mapType.get('SiteProduct'),
			Product__c = testProduct2.Id,
			Price__c = 100,
			Quantity__c = 1,
			Item_Commission_rate__c = '5%',
			BuyingupPrice__c = 10,
			OutsourcingApplication__c = '無',
			PictureCount__c = 0
		);
		// 配列に追加
		testItem.add(testItem2);
		// 商談商品
		OpportunityProduct__c testItem3 = new OpportunityProduct__c(
			Opportunity__c = testOpp.Id,
			Item_Entry_date__c = System.today(),
			Item_Contract_start_date__c = System.today(),
			RecordTypeId = mapType.get('KIJITASU'),
			Product__c = testProduct3.Id,
			Price__c = 100,
			TextCost2__c = 50,
			Quantity__c = 1,
			Item_Commission_rate__c = '5%',
			Item_keyword_letter_count__c = 10,
			Item_DeliveryDate__c = System.today(),
			OutsourcingApplication__c = '無',
			PictureCount__c = 0
		);
		// 配列に追加
		testItem.add(testItem3);
		// 商談商品
		OpportunityProduct__c testItem4 = new OpportunityProduct__c(
			Opportunity__c = testOpp.Id,
			Item_Entry_date__c = System.today(),
			Item_Contract_start_date__c = System.today(),
			RecordTypeId = mapType.get('SearchCleaner'),
			Product__c = testProduct4.Id,
			Price__c = 100,
			Quantity__c = 1,
			Item_Commission_rate__c = '5%',
			BuyingupPrice__c = 10,
			Item_Keyword_strategy_keyword__c = 'test keyword',
			Item_Automatic_updating__c = '有',
			OutsourcingApplication__c = '無',
			PictureCount__c = 0
		);
		// 配列に追加
		testItem.add(testItem4);
		// 商談商品
		OpportunityProduct__c testItem5 = new OpportunityProduct__c(
			Opportunity__c = testOpp.Id,
			Item_Entry_date__c = System.today(),
			Item_Contract_start_date__c = System.today(),
			RecordTypeId = mapType.get('WEBService'),
			Product__c = testProduct5.Id,
			Price__c = 100,
			Quantity__c = 1,
			Item_Commission_rate__c = '5%',
			OutsourcingApplication__c = '無',
			PictureCount__c = 0
		);
		// 配列に追加
		testItem.add(testItem5);
		// 商談商品を作成
		insert testItem;

		// 担当営業変更
		testOpp.OwnerId = [SELECT Id FROM User WHERE Id != :Userinfo.getUserId() LIMIT 1].Id;
		update testOpp;
	}
}