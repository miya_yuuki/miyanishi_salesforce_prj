global class BatchSetOrderItemPhaseEnd implements Database.Batchable<sObject> {

	private String query;

	// 時間ベースワークフロールール、フェーズ「契約終了」自動更新の置き換え
	// https://wg1.my.salesforce.com/01Q10000000DsfX
	global BatchSetOrderItemPhaseEnd(Date boundaryDate)
	{
		// SOQL を生成
		query =
			'SELECT ' +
				'Id, ' +
				'Item2_Keyword_phase__c ' +
			'FROM ' +
				'OrderItem__c ' +
			'WHERE ' +
				'AutomaticUpdate__c != \'有\' ' +
				'AND EndDate__c != null ' +
				'AND EndDate__c < ' + UtilityClass.retStringDt(boundaryDate) + ' ' +
				'AND Item2_Keyword_phase__c = \'契約中\'';
	}

	// バッチ開始処理
	// 開始するためにqueryを実行する。この実行されたSOQLのデータ分処理する。
	// 5千万件以上のレコードになるとエラーになる。
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		System.debug('*****************:' + query);
		return Database.getQueryLocator(query);
	}

	// バッチ処理内容
	// scopeにgetQueryLocatorの内容がバッチサイズ分格納されてくる
	global void execute(Database.BatchableContext BC, List<sObject> scope) {

		// 更新対象の注文商品リスト
		List<OrderItem__c> updatingOrderItems = new List<OrderItem__c>();

		for(sObject obj : scope) {
			// 注文商品のインスタンスを生成し「注文商品フェーズ」を「契約終了」にセット
			// 配列に追加
			updatingOrderItems.add(new OrderItem__c(
				Id = obj.Id,
				Item2_Keyword_phase__c = '契約終了'
			));
		}

		System.debug('************* updating OrderItem__c records: ' + updatingOrderItems.size());

		// 注文商品を更新
		if (updatingOrderItems.size() > 0) update updatingOrderItems;
	}

	global void finish(Database.BatchableContext BC){}
}