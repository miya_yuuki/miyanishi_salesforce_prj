trigger Notice2SlackTaskUpsertTrigger on Task (after insert, after update) {
    // 通知メッセージを格納するリスト
    List<String> msgItems = new List<String>();
    // ユーザ情報を取得
    Map<Id, Task> userIdMap = new Map<Id, Task>([select Id, Owner.Name from Task where Id In: Trigger.newmap.keyset()]);

    // オブジェクトのループ
    for (Task obj : Trigger.new) {
        // 配列に追加
        String objectUrl = URL.getSalesforceBaseUrl().toExternalForm();
        objectUrl += '/' + obj.ID;
        
        String msg = 'テレアポ情報の更新\n';
        msg += '>>>';
        msg += 'ID：' + '<' + objectUrl + '|' + obj.Id + '>' + '\n';
        msg += '架電種類：' + obj.Type__c + '\n';
        msg += '所有者：' + userIdMap.get(obj.Id).owner.name + '\n';
        msgItems.add(msg);
    }
    AnyObjectTriggerHandler.handleAfterUpdate(msgItems);
}