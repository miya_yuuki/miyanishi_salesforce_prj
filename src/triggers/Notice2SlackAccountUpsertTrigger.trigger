trigger Notice2SlackAccountUpsertTrigger on Account (after insert, after update) {
    // 通知メッセージを格納するリスト
    List<String> msgItems = new List<String>();
    // ユーザ情報を取得
    Map<Id, Account> userIdMap = new Map<Id, Account>([select Id, Owner.Name from Account where Id In: Trigger.newmap.keyset()]);

    // オブジェクトのループ
    for (Account obj : Trigger.new) {
        // 配列に追加
        String objectUrl = URL.getSalesforceBaseUrl().toExternalForm();
        objectUrl += '/' + obj.ID;
        
        String msg = '取引先情報の更新\n';
        msg += '>>>';
        msg += 'ID：' + '<' + objectUrl + '|' + obj.Id + '>' + '\n';
        msg += '取引先名：' + obj.name + '\n';
        msg += '所有者：' + userIdMap.get(obj.Id).owner.name + '\n';
        msgItems.add(msg);
    }
    AnyObjectTriggerHandler.handleAfterUpdate(msgItems);
}