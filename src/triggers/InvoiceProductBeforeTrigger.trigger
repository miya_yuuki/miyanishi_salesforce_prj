trigger InvoiceProductBeforeTrigger on BillProduct__c (before insert, before update) {
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////ワークフロールール→Trigger乗せ替え分//////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	for (BillProduct__c obj : Trigger.new) {
		// 新規作成時
		if (Trigger.isInsert) {
			// 商品コード商品名をセット
			obj.Bill2_ProductContent__c = obj.ProductItemNameMaster__c;

			// 請求金額消費税を計算
			// 請求金額（税抜）×消費税率（小数点以下切り捨て）
			obj.BillAmountTax__c = (obj.BillAmountPrint__c * obj.TaxRate__c / 100.0).round(System.RoundingMode.DOWN);
		}

		// 請求商品名セット
		obj.NameCopy__c = obj.Name;

		// D.代理店手数料額（積上げ用）セット
		obj.PartnerBillAccount2__c = obj.PartnerBillAccount__c;

		// 注文商品参照金額3項目セット
		obj.BillingAmount124View__c = obj.BillingAmount124__c;
		obj.BillingAmountNonouhinView__c = obj.BillingAmountNonouhin__c;
		obj.TotalPriceAmountOrderBillView__c = obj.TotalPriceAmountOrderBill__c;

		// 請求金額セット
		obj.Bill2_Amount11__c = obj.BillAmountPrint__c;

		// 請求対象開始日(請求書用）を自動入力
		if (obj.NoDay__c != '無') {
			obj.BillStartday2__c = obj.Bill2_ServiceDate__c;
			obj.Billendday2__c = obj.Bill2_EndDate__c;
		}
	}
}