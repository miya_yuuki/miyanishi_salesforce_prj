trigger Notice2SlackTextManagementUpsertTrigger on TextManagement2__c (after insert, after update) {
    // 通知メッセージを格納するリスト
    List<String> msgItems = new List<String>();
    // ユーザ情報を取得
    Map<Id, TextManagement2__c> userIdMap = new Map<Id, TextManagement2__c>([select Id, SalesPerson10__r.Name, DeliveryPerson__r.Name from TextManagement2__c where Id In: Trigger.newmap.keyset()]);

    // オブジェクトのループ
    for (TextManagement2__c obj : Trigger.new) {
        // 配列に追加
        String objectUrl = URL.getSalesforceBaseUrl().toExternalForm();
        objectUrl += '/' + obj.ID;
        
        String msg = '納品管理の更新\n';
        msg += '>>>';
        msg += 'ID：' + '<' + objectUrl + '|' + obj.Id + '>' + '\n';
        msg += '取引先名：' + obj.Account__c + '\n';
        msg += '納品管理ステータス：' + obj.CloudStatus__c + '\n';
        msg += '担当営業：' +  userIdMap.get(obj.Id).SalesPerson10__r.Name + '\n';
        msg += '納品担当：' + userIdMap.get(obj.Id).DeliveryPerson__r.Name + '\n';
        msgItems.add(msg);
    }
    AnyObjectTriggerHandler.handleAfterUpdate(msgItems);
}