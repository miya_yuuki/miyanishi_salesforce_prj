trigger oppItemBeforeTrigger on OrderItem__c (before insert, before update) {
    
    for (OrderItem__c obj : Trigger.new) {
        // 作成時のみ
        if (Trigger.isInsert) {
            // 請求レコード作成済み
            obj.Billchek__c = false;
            // 請求レコード作成済み①
            obj.Billcheck1__c = false;
            // 請求レコード作成済み②
            obj.Billcheck2__c = false;
            // 請求レコード作成済み③
            obj.Billcheck3__c = false;
            // 請求レコード作成済み④
            obj.Billcheck4__c = false;
            // 請求コピー実施日時
            obj.Item2_BillCopyDay__c = null;
            // 請求対象日 手動設定
            obj.BillSet__c = false;
        }
        
        // oldのインスタンス
        OrderItem__c old = new OrderItem__c();
        // 更新の場合はoldを取得
        if (Trigger.IsUpdate) {
            old = Trigger.oldMap.get(obj.Id);
        } else {
            old.Billcheck1__c = false;
            old.Billcheck2__c = false;
            old.Billcheck3__c = false;
            old.Billcheck4__c = false;
        }
        // 契約開始日を取得
        Date dt = obj.ServiceDate__c;
        // 契約期間を取得
        Integer i = UtilityClass.DtoI(obj.ContractMonths__c);
        // 契約終了日をセット
        if (obj.EndDateCheck__c == false) {
            if (obj.ContractMonths__c != null && obj.ContractMonths__c != 0)
                obj.EndDate__c = UtilityClass.prcAddDays(UtilityClass.prcAddMonths(dt, i), -1);
        }

        // TACT SEO商品の入力規則確認（同じ契約期間を持つTACT商品は登録できない）
        // 更新対象の商談商品ID
        /*List<String> targetTactAccountIds = new List<String>();
        List<OrderItem__c> targetNewObjs = new List<OrderItem__c>();
        for (OrderItem__c newObj : Trigger.new) {
            if (newObj.Item2_Product__c == null || !newObj.Item2_Product_naiyou__c.contains('TACT SEO') || newObj.TACT_Account_ID__c == null) {
                continue;
            }
            if (Trigger.isInsert) {
                targetTactAccountIds.add(newObj.TACT_Account_ID__c);
                targetNewObjs.add(newObj);
            } else {
                if ((newObj.Item2_ServiceDate__c == null || newObj.Item2_EndData__c >= date.today()) && newObj.Item2_ServiceDate__c != Trigger.oldMap.get(newObj.Id).Item2_EndData__c) {
                    targetTactAccountIds.add(newObj.TACT_Account_ID__c);
                    targetNewObjs.add(newObj);
                }
            }
        }
        if (targetTactAccountIds.size() > 0) {
            // 既存の注文商品の配列
            List<OrderItem__c> existingOrderProducts = new List<OrderItem__c>();
            // 既存の注文商品のMap
            Map<String, List<OrderItem__c>> mapExistingOrderProducts = new Map<String, List<OrderItem__c>>();
            existingOrderProducts = [
                SELECT Id,TACT_Account_ID__c,
                Item2_ServiceDate__c, Item2_EndData__c 
                FROM OrderItem__c
                WHERE TACT_Account_ID__c IN :targetTactAccountIds
                AND Item2_EndData__c > :date.today()
                AND Item2_Product_naiyou__c LIKE '%TACT SEO%'
                AND Item2_Keyword_phase__c = '契約中' 
            ];
            
            // map化
            for (OrderItem__c existingOrderProduct : existingOrderProducts) {
                if (!mapExistingOrderProducts.containsKey(existingOrderProduct.TACT_Account_ID__c)) {
                    mapExistingOrderProducts.put(existingOrderProduct.TACT_Account_ID__c, new List<OrderItem__c>());
                }
                mapExistingOrderProducts.get(existingOrderProduct.TACT_Account_ID__c).add(existingOrderProduct);
            }

            for (OrderItem__c targetNewObj : targetNewObjs) {
                if (!mapExistingOrderProducts.containsKey(targetNewObj.TACT_Account_ID__c)) continue;
                List<OrderItem__c> comparisonOrderProducts = mapExistingOrderProducts.get(targetNewObj.TACT_Account_ID__c);
                for (OrderItem__c comparisonOrderProduct : comparisonOrderProducts) {
                    if (comparisonOrderProduct.Id == targetNewObj.Id)
                    {
                        continue;
                    }
                    if (
                        (targetNewObj.Item2_ServiceDate__c > comparisonOrderProduct.Item2_EndData__c && targetNewObj.Item2_EndData__c > comparisonOrderProduct.Item2_EndData__c) || 
                        (targetNewObj.Item2_ServiceDate__c < comparisonOrderProduct.Item2_ServiceDate__c && targetNewObj.Item2_EndData__c < comparisonOrderProduct.Item2_ServiceDate__c)
                    ){
                        continue;
                    }
                    targetNewObj.addError('同じ契約期間を持つTACTの注文商品が存在しています。');
                }
            }
        }*/

        // 請求レコード作成済みセットされている場合は、請求コピー日時をセット
        if (!old.Billcheck1__c && obj.Billcheck1__c) obj.Item2_BillCopyDay__c = System.now();
        if (!old.Billcheck2__c && obj.Billcheck2__c) obj.Item2_BillCopyDay__c = System.now();
        if (!old.Billcheck3__c && obj.Billcheck3__c) obj.Item2_BillCopyDay__c = System.now();
        if (!old.Billcheck4__c && obj.Billcheck4__c) obj.Item2_BillCopyDay__c = System.now();
        // 分割納品: 支払期日をセット
        if (obj.ProductSeparateUMU__c == '有') {
            if (obj.Billcheck1__c) obj.Item2_PaymentDueDate__c = obj.Item2_PaymentDueDate1__c;
            if (obj.Billcheck2__c) obj.Item2_PaymentDueDate__c = obj.Item2_PaymentDueDate2__c;
            if (obj.Billcheck3__c) obj.Item2_PaymentDueDate__c = obj.Item2_PaymentDueDate3__c;
            if (obj.Billcheck4__c) obj.Item2_PaymentDueDate__c = obj.Item2_PaymentDueDate4__c;
        }
        // 支払期日（最新）をセット
        if (obj.Billchek__c) {
            obj.Item2_PaymentDueDateNew__c = obj.Item2_PaymentDueDate__c;
        } else if (obj.Billcheck4__c) {
            obj.Item2_PaymentDueDateNew__c = obj.Item2_PaymentDueDate4__c;
        } else if (obj.Billcheck3__c) {
            obj.Item2_PaymentDueDateNew__c = obj.Item2_PaymentDueDate3__c;
        } else if (obj.Billcheck2__c) {
            obj.Item2_PaymentDueDateNew__c = obj.Item2_PaymentDueDate2__c;
        } else if (obj.Billcheck1__c) {
            obj.Item2_PaymentDueDateNew__c = obj.Item2_PaymentDueDate1__c;
        }
    }
    
    // 支払い期日を算出(前払い処理含む)    
    PaymentCalculationClass.prcGetPaymentDueDate(Trigger.new);
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////ワークフロールール→Trigger乗せ替え分//////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    for (OrderItem__c obj : Trigger.new) {
        // 作成時のみ
        if (Trigger.isInsert) {
            // レコードタイプ = サグーワークス/KIJITASU用の場合
            if (obj.RecordTypeId == '01210000000AOjF') {
                // B.分割請求状況項目リセット2
                obj.Bill_Day1__c = null;
                obj.Bill_Day2__c = null;
                obj.Bill_Day3__c = null;
                obj.Bill_Day4__c = null;
                obj.PaymentQuantity1__c = null;
                obj.PaymentQuantity2__c = null;
                obj.PaymentQuantity3__c = null;
                obj.PaymentQuantity4__c = null;
                // B.分割請求状況項目リセット1
                obj.Billcheck1__c = false;
                obj.Billcheck2__c = false;
                obj.Billcheck3__c = false;
                obj.Billcheck4__c = false;
                obj.Item2_PaymentDueDate1__c = null;
                obj.Item2_PaymentDueDate2__c = null;
                obj.Item2_PaymentDueDate3__c = null;
                obj.Item2_PaymentDueDate4__c = null;
            }
            
            // ストック（数値）セット
            if (obj.Item2_Product_r_ClassificationSales__c == 'ストック') {
                obj.Stock1__c = 1;
            }
            
            // 注文商品：契約変更受付日リセット
            if (obj.ChangeDay__c != null) {
                obj.ChangeDay__c = null;
            }
            
            // 納品/自動更新：有　新規作成時：納品完了日　削除
            if (obj.AutomaticUpdate__c == '有' && obj.Item2_DeliveryExistence__c == '有') {
                obj.Item2_Delivery_date__c = null;
                obj.ProductionStatus__c = 'チェック待ち';
            }
        }
        
        // 納品有無/請求基準コピー
        obj.Item2_DeliveryExistence2__c = obj.Item2_DeliveryExistence__c;
        obj.Item2_DeliveryStandardsView__c = obj.BillingTiming__c;
        
        // 注文番号セット
        obj.Item2_RelationView__c = obj.Item2_RelationName__c;
        
        // 商品グループセット
        obj.ProductGroupVew__c = obj.ProductGroup__c;
        
        // 契約期間合計納品数（積上用）セット
        obj.Item2_DeliveryCount2__c = obj.Item2_DeliveryCount__c;
        
        // 合計/月額（積み上げ用）セット
        obj.TotalPrice11__c = obj.TotalPrice__c;
        
        // 解約可能額（積上用）代入
        obj.CancelableAmountCopy__c = obj.CancelableAmount__c;
        
        // 取引先名/施策企業名を自動セット
        obj.AccountName__c = obj.Account__c;
        obj.sisakukigyou2__c = obj.sisakukigyou__c;
        obj.Salesforce_ID1__c = obj.Salesforce_ID__c;
        obj.Item2_ProductTypeCopy__c = obj.Item2_ProductType1__c ;
        
        if (obj.DeliveryCountTotal__c >= 0) {
            // 納品完了日：未納品合計0以上で納品日を入力
            obj.Item2_Delivery_date__c = obj.Item2_Delivery_dateEnd__c;
            // 納品数0で納品完了ステータスに変更
            obj.ProductionStatus__c = '社外納品済み';
        }
        
        // 商品（帳票用）チェック
        if (obj.Item2_Detail_Productlist__c == null) {
            obj.Item2_Detail_Productlist__c = obj.Item2_Detail_ProductlistView__c;
        }
        
        //////////////////////////////////////商品マスタ改善対応//////////////////////////////////////
        // 請求タイミングセット
            // 納品有無が無なら、納品請求は選択不可
        if (obj.Item2_DeliveryExistence__c == '無') {
            obj.BillingTiming__c = '受注';
        }
            // 商品マスタ上の請求基準が「納品のみ」なら必ず納品請求にする
        if (obj.Item2_DeliveryStandards__c == '納品のみ') {
            obj.BillingTiming__c = '納品';
        }
        
        // 自動更新有無チェック
        if (obj.Item2_Automatic_updating1__c == '無') {
            obj.AutomaticUpdate__c = '無';
        }
        
        // 社内発注処理 / 月数 = 0
        if (obj.Item2_Product_r_ClassificationSales__c == '社内発注') {
            obj.ContractMonths__c = 0;
        }
    }
}