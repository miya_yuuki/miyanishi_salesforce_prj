trigger Notice2SlackContractUpsertTrigger on Contract (after insert, after update) {
    // 通知メッセージを格納するリスト
    List<String> msgItems = new List<String>();
    // ユーザ情報を取得
    Map<Id, Contract> userIdMap = new Map<Id, Contract>([select Id, Contract_SalesPerson__r.Name from Contract where Id In: Trigger.newmap.keyset()]);

    // オブジェクトのループ
    for (Contract obj : Trigger.new) {
        // 配列に追加
        String objectUrl = URL.getSalesforceBaseUrl().toExternalForm();
        objectUrl += '/' + obj.ID;
        
        String msg = '契約の更新\n';
        msg += '>>>';
        msg += 'ID：' + '<' + objectUrl + '|' + obj.Id + '>' + '\n';
        msg += '取引先名：' + obj.Account + '\n';
        msg += 'フェーズ：' + obj.Phase__c + '\n';
        msg += '担当営業：' + userIdMap.get(obj.Id).Contract_SalesPerson__r.name + '\n';
        msgItems.add(msg);
    }
    AnyObjectTriggerHandler.handleAfterUpdate(msgItems);
}