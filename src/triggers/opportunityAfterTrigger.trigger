trigger opportunityAfterTrigger on Opportunity (after insert, after update) {
    // 商談の承認時、「商談>商談商品」を「契約>注文>注文商品」に転記する
    OppOrderCopyClass.prcOppOrderCopy(Trigger.new);
    
    // 更新の場合
    if (Trigger.isUpdate) {
        if (OpportunityProductDistributionUpdate.firstRun) {
            // 商談の担当者が更新された場合、商談商品の担当者系も更新
            OpportunityProductDistributionUpdate.firstRun = false;
            OpportunityProductDistributionUpdate.prcDistributionUpdate(Trigger.new, Trigger.oldMap, Trigger.isUpdate);
        }
        if (AccountWebSiteUpdate.firstRun) {
            // 商談情報更新時に、取引先Webサイトの情報も更新
            AccountWebSiteUpdate.firstRun = false;
            AccountWebSiteUpdate.prcAccountWebSiteUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}