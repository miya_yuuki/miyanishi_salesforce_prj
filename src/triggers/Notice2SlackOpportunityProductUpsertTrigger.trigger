trigger Notice2SlackOpportunityProductUpsertTrigger on OpportunityProduct__c (after insert, after update) {
    // 通知メッセージを格納するリスト
    List<String> msgItems = new List<String>();

    // オブジェクトのループ
    for (OpportunityProduct__c obj : Trigger.new) {
        // 配列に追加
        String objectUrl = URL.getSalesforceBaseUrl().toExternalForm();
        objectUrl += '/' + obj.ID;
        
        String msg = '商談商品情報の更新\n';
        msg += '>>>';
        msg += 'ID：' + '<' + objectUrl + '|' + obj.Id + '>' + '\n';
        msg += '商談名：' + obj.Opportunity__c + '\n';
        msg += '商品：' + obj.productname__c + '\n';
        msg += '担当営業：' + obj.salesperson__c + '\n';
        msgItems.add(msg);
    }
    AnyObjectTriggerHandler.handleAfterUpdate(msgItems);
}