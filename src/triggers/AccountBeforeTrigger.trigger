trigger AccountBeforeTrigger on Account (before insert, before update) {
	for (Account obj: Trigger.new) {
		// エラーメッセージをクリア
		String strErrMsg = '';

		if(obj.PaymentSiteMaster__c == null) {
			obj.PaymentSiteMaster__c = 'a0w10000004pj34';
		}

		//【NG理由】は【取引先状況】が「アプローチ不可」の場合は必須
		if (obj.account_Status__c == 'アプローチ不可') {
			if (obj.account_NG__c == null) strErrMsg += '【取引先状況】が「アプローチ不可」の場合、【NG理由】は必須です。<br/>';
		}

		// エラーメッセージを表示
		if (strErrMsg != '') {
			obj.addError('<br/>' + strErrMsg);
		}
	}
}