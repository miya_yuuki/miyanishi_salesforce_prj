trigger operatorBeforeTrigger on operator__c (before insert, before update) {
    for (operator__c targetObject : Trigger.new) {
    	// 契約開始日＋契約月数を契約終了日にセットする
    	if (targetObject.keiyakukaishi__c != null && targetObject.keiyakushuryou__c != null && targetObject.keiyakugessu__c != null) {
    		targetObject.keiyakushuryou__c = UtilityClass.prcAddDays(UtilityClass.prcAddMonths(targetObject.keiyakukaishi__c, targetObject.keiyakugessu__c.intValue()), -1);
    	}
    }
}