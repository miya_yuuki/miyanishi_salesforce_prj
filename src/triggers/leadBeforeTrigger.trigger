trigger leadBeforeTrigger on Lead (before insert, before update) {
    for (Lead obj : Trigger.new) {
        // エラーメッセージをクリア
        String strErrMsg = '';

        // Trigger入力規則
        if (obj.RecordTypeId == '01210000000Zo99') {
            // 2019.11.25 FORCASが失敗するので入力規則削除
            /*
            if (obj.OwnerTeam__c != null && (obj.OwnerTeam__c.contains('アカウントプランニング') || obj.OwnerTeam__c.contains('マーケティング'))) {
                
                // SEO施策状況
                if (obj.SEOMeasureStatus__c == '外注' && obj.Products__c == null) strErrMsg += '<br/>SEO施策状況 = 外注 の時はリード情報の「商材」が入力必須です。<br/>';
                if (obj.SEOMeasureStatus__c == '外注' && obj.SEOBudgetPerMonth__c == null) strErrMsg += 'SEO施策状況 = 外注 の時はリード情報の「SEO予算/月」が入力必須です。<br/>';
                // 取引開始チェック
                if (obj.Startcheck__c == true && obj.lead_BusinessModel__c == null) strErrMsg += '【取引開始チェック】にチェック✓された場合、ビジネスモデルは入力必須です。<br/>';
                if (obj.Startcheck__c == true && obj.AppointmentDay__c == null) strErrMsg += '【取引開始チェック】にチェック✓された場合、アポ獲得日は入力必須です。<br/>';
                if (obj.Startcheck__c == true && obj.lead_Industry__c == null) strErrMsg += '【取引開始チェック】にチェック✓された場合、業種（大）は入力必須です。<br/>';
                if (obj.Startcheck__c == true && obj.lead_Industrycategory__c == null) strErrMsg += '【取引開始チェック】にチェック✓された場合、業種（小）は入力必須です。<br/>';
                
            }
            */
            if (obj.staff_approach_status__c != null && obj.staff_approach_status__c.contains('アプローチ不可') && obj.staff_approach_NGreason__c == null) strErrMsg += '【担当者状況】が「アプローチ不可」の場合、担当者のアプローチ不可理由は選択必須です。<br/>';
            if (obj.company_approach_status__c != null && obj.company_approach_status__c.contains('アプローチ不可') && obj.company_approach_NGreason__c == null) strErrMsg += '【取引先状況】が「アプローチ不可」の場合、取引先のアプローチ不可理由は選択必須です。<br/>';
        }

        // エラーメッセージを表示
        if (strErrMsg != '') {
            obj.addError('<br/>' + strErrMsg, false);
        }
    }
}