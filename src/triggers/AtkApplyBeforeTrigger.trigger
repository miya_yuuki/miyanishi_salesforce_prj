trigger AtkApplyBeforeTrigger on teamspirit__AtkApply__c (before insert, before update) {
    for (teamspirit__AtkApply__c targetObject : Trigger.new) {
    	// 「有効期限」=契約開始日 + 3ヵ月 - 1日
    	targetObject.expirationDate__c = UtilityClass.prcAddDays(UtilityClass.prcAddMonths(targetObject.contract_start__c, 3), -1);
    }
}