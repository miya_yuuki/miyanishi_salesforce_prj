trigger invoiceBeforeTrigger on Bill__c (before insert, before update) {
	// 請求商品から項目をコピー
	InvoiceFieldSetClass.prcInvoiceFieldSet(Trigger.new);
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////ワークフロールール→Trigger乗せ替え分//////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	for (Bill__c obj : Trigger.new) {
		// 請求名セット
		obj.NameCopy__c = obj.Name;
		
		// 請求担当自動入力（作成時のみ）
		if (Trigger.isInsert) obj.Bill_tantou__c = obj.Bill_tantouView__c;
		
		// 担当営業の上長をセット
		obj.boss_sales_person__c = obj.boss_sales_person_id__c;
		
		// 取引先名を自動セット
		obj.AccountName__c = obj.Bill_Account__c;
		
		// 有効な請求商品が0件の場合 → 営業確認チェックを下ろす
		if (obj.ActiveBillingItems__c == 0) obj.SalesCheck1__c = false;
	}
}