trigger Notice2SlackLeadUpsertTrigger on Lead (after insert, after update) {
    // 通知メッセージを格納するリスト
    List<String> msgItems = new List<String>();
    // ユーザ情報を取得
    Map<Id, Lead> userIdMap = new Map<Id, Lead>([select Id, Owner.Name, LastModifiedBy.Name from Lead where Id In: Trigger.newmap.keyset()]);

    // オブジェクトのループ
    for (Lead obj : Trigger.new) {
        // 配列に追加
        String objectUrl = URL.getSalesforceBaseUrl().toExternalForm();
        objectUrl += '/' + obj.ID;
        
        String msg = 'リード情報の更新\n';
        msg += '>>>';
        msg += 'ID：' + '<' + objectUrl + '|' + obj.Id + '>' + '\n';
        msg += '会社名：' + obj.company + '\n';
        msg += '所有者：' + userIdMap.get(obj.Id).owner.name + '\n';
        msg += '最終更新者：' + userIdMap.get(obj.Id).lastmodifiedby.name + '\n';
        msgItems.add(msg);
    }
    AnyObjectTriggerHandler.handleAfterUpdate(msgItems);
}