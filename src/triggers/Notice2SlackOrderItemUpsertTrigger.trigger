trigger Notice2SlackOrderItemUpsertTrigger on OrderItem__c (after insert, after update) {
    // 通知メッセージを格納するリスト
    List<String> msgItems = new List<String>();

    // オブジェクトのループ
    for (OrderItem__c obj : Trigger.new) {
        // 配列に追加
        String objectUrl = URL.getSalesforceBaseUrl().toExternalForm();
        objectUrl += '/' + obj.ID;
        
        String msg = '注文商品の更新\n';
        msg += '>>>';
        msg += 'ID：' + '<' + objectUrl + '|' + obj.Id + '>' + '\n';
        msg += '取引先名：' + obj.Account__c + '\n';
        msg += '請求基準日：' + obj.DeliveryDateCalc__c + '\n';
        msg += '担当営業：' + obj.SalesPerson1__c + '\n';
        msgItems.add(msg);
    }
    AnyObjectTriggerHandler.handleAfterUpdate(msgItems);
}