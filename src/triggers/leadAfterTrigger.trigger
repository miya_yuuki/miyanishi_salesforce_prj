trigger leadAfterTrigger on Lead (after insert, after update) {

    // Start ********** 取引先責任者のレコードタイプを「外部連携用」に変更 *************
    // 取引先責任者ID
    List<String> strContactIds = new List<String>();
    // 取引先責任者オブジェクト
    List<Contact> objContacts = new List<Contact>();

    // 取引先責任者IDをセット
    for (Lead obj : Trigger.new) {
        // TACTからのオンライン申し込みの場合は変更対象に加える
        if (obj.TACT_online_entry_flag__c) {
            strContactIds.add(obj.ConvertedContactId);
        }
    }

    // 取引先責任者を取得
    objContacts = [SELECT Id, RecordTypeId FROM Contact WHERE Id IN :strContactIds];
    // 取引先責任者のレコードタイプを変更
    for (Contact obj : objContacts) {
        obj.RecordTypeId = '0125F000000a5yz';
    }
    // 取引先責任者を更新
    if (objContacts.size() > 0) update objContacts;

    // End ********** 取引先責任者のレコードタイプを「外部連携用」に変更 *************

    // Start ********** 商談の取引先責任者の役割に取引先責任者を紐づけ *************
    // 取引先責任者の役割オブジェクト（Check用）
    List<OpportunityContactRole> chkOpportunityContactRole = new List<OpportunityContactRole>();
    // 取引先責任者の役割オブジェクト（INSERT用）
    List<OpportunityContactRole> insOpportunityContactRole = new List<OpportunityContactRole>();

    for (Lead obj : Trigger.new) {
        // 商談IDと取引先責任者IDがセットされている場合のみ
        if (obj.ConvertedOpportunityId == null || obj.ConvertedContactId == null) continue;

        // 取引先責任者の役割レコード作成
        OpportunityContactRole newContactRole = new OpportunityContactRole(
          OpportunityId = obj.ConvertedOpportunityId,
          ContactId = obj.ConvertedContactId
        );
        chkOpportunityContactRole = [SELECT Id FROM OpportunityContactRole WHERE OpportunityId = :obj.ConvertedOpportunityId AND ContactId = :obj.ConvertedContactId];
        if (chkOpportunityContactRole.size() == 0) {
          insOpportunityContactRole.add(newContactRole);
        }
    }
    if (insOpportunityContactRole.size() > 0) {
        insert insOpportunityContactRole;
    }

    // End ********** 商談の取引先責任者の役割に取引先責任者を紐づけ *************
}