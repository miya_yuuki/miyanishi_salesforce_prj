trigger textManagementAfterTrigger on TextManagement2__c (after delete, after insert, after update) {
	// 納品履歴オブジェクト
	List<TextManagement2__c> objDelivery = new List<TextManagement2__c>();
	
	// 削除の場合
	if (Trigger.isDelete) {
		for (TextManagement2__c obj : Trigger.old) objDelivery.add(obj);
	// 作成・更新の場合
	} else {
		for (TextManagement2__c obj : Trigger.new) objDelivery.add(obj);
	}
	
	// 請求・請求商品を作成
	if (CreateBillRecord.firstRun) {
		CreateBillRecord.firstRun = false;
		CreateBillRecord.prcCreateBill(Trigger.new, Trigger.oldMap, Trigger.isUpdate);
	}
	
	if (TextManagementSP2UnitUpdate.firstRun) {
		if (Trigger.isUpdate) {
	//		// 案分担当②ユニットをセット
	//		TextManagementSP2UnitUpdate.prcSalesPerson2UnitUpdate(Trigger.new, Trigger.oldMap, Trigger.isUpdate);
			// 担当営業ユニットを更新
			TextManagementSP2UnitUpdate.prcSalesPersonUnitLastUpdate(Trigger.new, Trigger.oldMap, Trigger.isUpdate);
		}
		TextManagementSP2UnitUpdate.firstRun = false;
	}

	if (Trigger.isUpdate) {
		// 請求レコード発行済みの納品管理が契約変更・取消された時、請求商品の無効Flagを上げる
		InvoiceRecordInvalidation.prcInvoiceRecordInvalidationFromTM(Trigger.new);
	}
}