trigger OpportunityProductAfterTrigger on OpportunityProduct__c (after insert, after update) {
    // 新規登録時のみ
    if (Trigger.isInsert) {
        // 商談商品に応じて商談の約款チェックを入れる
        OpportunityClauseCheck.OpportunityClauseCheck(Trigger.new);
    }
}