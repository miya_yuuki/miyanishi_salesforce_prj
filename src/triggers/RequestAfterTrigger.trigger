trigger RequestAfterTrigger on CustomObject1__c (after insert, after update) {
    // 解約レコードをチェック＋自動補完
    if (CancellationRecordsCheck.firstRun) {
        CancellationRecordsCheck.prcGetCancellationRecord(Trigger.New);
        CancellationRecordsCheck.firstRun = false;
    }

    // 解約対象注文商品の入力チェック
    if (CancellationSubRecordsCheck.firstRun) {
        CancellationSubRecordsCheck.prcCancellationSubRecordCheck(Trigger.New);
        CancellationSubRecordsCheck.firstRun = false;
    }

    // 解約総額・解約月請求額を自動計算
    if (CancellationAmountCalculation.firstRun) {
        CancellationAmountCalculation.prcGetCancellationAmount(Trigger.New);
        CancellationAmountCalculation.firstRun = false;
    }
}