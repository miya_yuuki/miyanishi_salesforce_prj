trigger Notice2SlackPaymentManagementUpsertTrigger on PaymentManagement__c (after insert, after update) {
    /*/ 通知メッセージを格納するリスト
    List<String> msgItems = new List<String>();

    // オブジェクトのループ
    for (PaymentManagement__c obj : Trigger.new) {
        // 配列に追加
        String objectUrl = URL.getSalesforceBaseUrl().toExternalForm();
        objectUrl += '/' + obj.ID;
        
        String msg = '入金管理情報の更新\n';
        msg += '>>>';
        msg += 'ID：' + '<' + objectUrl + '|' + obj.Id + '>' + '\n';
        msg += '取引先名：' + obj.AccountName__c + '\n';
        msgItems.add(msg);
    }
    AnyObjectTriggerHandler.handleAfterUpdate(msgItems);*/
}