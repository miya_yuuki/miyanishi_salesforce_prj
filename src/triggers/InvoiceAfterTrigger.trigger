trigger InvoiceAfterTrigger on Bill__c (after insert, after update) {
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////ワークフロールール→Trigger乗せ替え分//////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 各種チェック = TRUEになった時の処理
    if (InvoiceFlagCheck.firstRun) {
        InvoiceFlagCheck.firstRun = false;
        InvoiceFlagCheck.prcInvoiceSalesCheck(Trigger.new, Trigger.oldMap, Trigger.isUpdate);
        InvoiceFlagCheck.prcInvoiceIssuedCheck(Trigger.new, Trigger.oldMap, Trigger.isUpdate);
        InvoiceFlagCheck.prcInvoiceLastCheck(Trigger.new, Trigger.oldMap, Trigger.isUpdate);
        InvoiceFlagCheck.prcInvoiceSalesCheckRemoved(Trigger.new, Trigger.oldMap, Trigger.isUpdate);
    }    
}