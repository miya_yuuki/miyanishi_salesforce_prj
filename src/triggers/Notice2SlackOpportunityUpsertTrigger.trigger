trigger Notice2SlackOpportunityUpsertTrigger on Opportunity (after insert, after update) {
    // 通知メッセージを格納するリスト
    List<String> msgItems = new List<String>();
    // ユーザ情報を取得
    Map<Id, Opportunity> userIdMap = new Map<Id, Opportunity>([select Id, Owner.Name from Opportunity where Id In: Trigger.newmap.keyset()]);

    // オブジェクトのループ
    for (Opportunity obj : Trigger.new) {
        // 配列に追加
        String objectUrl = URL.getSalesforceBaseUrl().toExternalForm();
        objectUrl += '/' + obj.ID;
        
        String msg = '商談情報の更新\n';
        msg += '>>>';
        msg += 'ID：' + '<' + objectUrl + '|' + obj.Id + '>' + '\n';
        msg += '商談名：' + obj.name + '\n';
        msg += '所有者：' + userIdMap.get(obj.Id).owner.name + '\n';
        msgItems.add(msg);
    }
    AnyObjectTriggerHandler.handleAfterUpdate(msgItems);
}