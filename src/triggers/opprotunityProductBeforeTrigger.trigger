trigger opprotunityProductBeforeTrigger on OpportunityProduct__c (before insert, before update) {

    for (OpportunityProduct__c obj : Trigger.new) {
        // 案分系項目を埋める
        obj.SalesPersonMain__c = String.valueOf(obj.SalesPerson1__c);
        obj.SalesPersonMainUnit__c = String.valueOf(obj.SalesPersonUnit__c);
        // 新規登録の場合
        if (Trigger.isInsert) {
            if (obj.SalesPercentage1__c == null) {
                obj.SalesPercentage1__c = 100;
            }
            if (obj.SalesPercentage2__c == null) {
                obj.SalesPercentage2__c = 100 - obj.SalesPercentage1__c;
            }
            // 集客改善プラン・キーワード最適化プランの場合
            // 「コンサルティング売上相当額」のデフォルト値をセット
            if (obj.Item_Detail_Product__c == '集客改善プラン') {
                obj.consultingSalesEquivalent__c = 200000;
            }
            if (obj.Item_Detail_Product__c == 'キーワード最適化プラン') {
                obj.consultingSalesEquivalent__c = 10000;
            }
        }
        // 案分比率にnullが残らないようにする
        if (obj.SalesPercentage1__c == null && obj.SalesPercentage2__c == null) {
            obj.SalesPercentage1__c = 100;
            obj.SalesPercentage2__c = 0;
        }
        else if (obj.SalesPercentage1__c == null && obj.SalesPercentage2__c != null) {
            obj.SalesPercentage1__c = 100 - obj.SalesPercentage2__c;
        }
        else if (obj.SalesPercentage1__c != null && obj.SalesPercentage2__c == null) {
            obj.SalesPercentage2__c = 100 - obj.SalesPercentage1__c;
        }
    }

    // 契約終了日を算出
    for (OpportunityProduct__c obj : Trigger.new) {
        // 契約開始日を取得
        Date dt = obj.Item_Contract_start_date__c;
        // 契約期間を取得
        Integer i = UtilityClass.DtoI(obj.ContractMonths__c);
        // 契約終了日をセット
        if (obj.ContractMonths__c != null && obj.ContractMonths__c != 0)
            obj.Item_Contract_end_date__c = UtilityClass.prcAddDays(UtilityClass.prcAddMonths(dt, i), -1);
    }

    // TACT SEO商品の入力規則確認（同じ契約期間を持つTACT商品は登録できない）
    // 更新対象の商談商品ID
    List<String> targetTactAccountIds = new List<String>();
    List<OpportunityProduct__c> targetNewObjs = new List<OpportunityProduct__c>();
    for (OpportunityProduct__c newObj : Trigger.new) {
        if (newObj.Product__c == null || !newObj.ProductName__c.contains('TACT SEO') || newObj.TACT_Account_ID__c == null) {
            continue;
        }
        if (Trigger.isInsert) {
            targetTactAccountIds.add(newObj.TACT_Account_ID__c);
            targetNewObjs.add(newObj);
        } else {
            if ((newObj.Item_Contract_end_date__c == null || newObj.Item_Contract_end_date__c >= date.today()) && newObj.Item_Contract_start_date__c != Trigger.oldMap.get(newObj.Id).Item_Contract_start_date__c) {
                targetTactAccountIds.add(newObj.TACT_Account_ID__c);
                targetNewObjs.add(newObj);
            }
        }
    }
    if (targetTactAccountIds.size() > 0) {
        // 既存の商談商品の配列
        List<OpportunityProduct__c> existingOppProducts = new List<OpportunityProduct__c>();
        // 既存の商談商品のMap
        Map<String, List<OpportunityProduct__c>> mapExistingOppProducts = new Map<String, List<OpportunityProduct__c>>();
        existingOppProducts = [
            SELECT Id,TACT_Account_ID__c,
            Item_Contract_start_date__c, Item_Contract_end_date__c 
            FROM OpportunityProduct__c
            WHERE TACT_Account_ID__c IN :targetTactAccountIds
            AND Item_Contract_end_date__c > :date.today()
            AND Product__r.Name LIKE '%TACT SEO%'
            AND OrderDay__c = NULL
        ];
        
        // map化
        for (OpportunityProduct__c existingOppProduct : existingOppProducts) {
            if (!mapExistingOppProducts.containsKey(existingOppProduct.TACT_Account_ID__c)) {
                mapExistingOppProducts.put(existingOppProduct.TACT_Account_ID__c, new List<OpportunityProduct__c>());
            }
            mapExistingOppProducts.get(existingOppProduct.TACT_Account_ID__c).add(existingOppProduct);
        }

        for (OpportunityProduct__c targetNewObj : targetNewObjs) {
            if (!mapExistingOppProducts.containsKey(targetNewObj.TACT_Account_ID__c)) continue;
            List<OpportunityProduct__c> comparisonOppProducts = mapExistingOppProducts.get(targetNewObj.TACT_Account_ID__c);
            for (OpportunityProduct__c comparisonOppProduct : comparisonOppProducts) {
                if (comparisonOppProduct.Id == targetNewObj.Id)
                {
                    continue;
                }
                if (
                    (targetNewObj.Item_Contract_start_date__c > comparisonOppProduct.Item_Contract_end_date__c && targetNewObj.Item_Contract_end_date__c > comparisonOppProduct.Item_Contract_end_date__c) || 
                    (targetNewObj.Item_Contract_start_date__c < comparisonOppProduct.Item_Contract_start_date__c && targetNewObj.Item_Contract_end_date__c < comparisonOppProduct.Item_Contract_start_date__c)
                ){
                    continue;
                }
                targetNewObj.addError('同じ契約期間を持つTACTの商談商品が存在しています。' + comparisonOppProduct.Id + comparisonOppProduct.Item_Contract_start_date__c.format() + comparisonOppProduct.Item_Contract_end_date__c.format());
            }
        }
    }

    // 商品&記事タイプ（帳票用）を代入
    for (OpportunityProduct__c obj : Trigger.new) {
        obj.ProductNameAndTextType__c = obj.Item_Detail_Product__c + '\n' + obj.TextType__c;
    }

    for (OpportunityProduct__c obj : Trigger.new) {
        // 新規登録の場合
        if (Trigger.isInsert) {
            // 記事見積ステータス削除
            obj.kijimitumori__c = '';

            // 受注月納品額にセット
            if (obj.SalesForecast02__c == null) {
                obj.SalesForecast01__c = obj.TotalPrice__c;
            }

            // R&Dステータス リセット
            obj.R_D__c = '未対応';

            // 承認レベル リセット
            obj.Text_Accep_TedNorm__c = null;
        }

        // 商談商品：数量1自動入力（KIJITASU/サグーワークス以外）　　※明示的に数量0を入力された場合はレコードタイプに関わらず数量を1にする
        if ((obj.RecordTypeId != '01210000000AOXx' && obj.RecordTypeId != '01210000000AOXn' && obj.Quantity__c == null) || obj.Quantity__c == 0) {
            obj.Quantity__c = 1;
        }

        // 商品グループセット
        obj.ProductGroupCopy__c = obj.ProductGroup__c;

        // 合計/月額（積み上げ用）セット
        obj.AmountTotal11__c = obj.TotalPrice1__c;

        // レコードタイプ（参照）をセット
        obj.RecordTypeText__c = obj.RecordTypeName__c;

        // 合計/月額(参照）　自動セット
        obj.TotalPriceReference__c = obj.TotalPrice1__c;

        // 計上予定日Set
        if (obj.AmountCountDay__c == null) {
            obj.AmountCountDay__c = obj.CloseDate__c;
        }

        // テキスト仕入れ金額自動セット
        if (obj.RecordTypeId == '01210000000AOXx' && obj.Quantity__c != null && obj.TextCost2__c != null) {
            if (obj.TextCalculation__c == true) {
                obj.BuyingupPrice__c = obj.Quantity__c * obj.TextCost2__c * obj.Item_keyword_letter_count__c;
            }
            else {
                obj.BuyingupPrice__c = obj.Quantity__c * obj.TextCost2__c;
            }
        }
        else {
            obj.BuyingupPrice__c = obj.BuyingupPrice__c;
        }

        // SEOレコードタイプ（参照用）
        if (obj.RecordTypeId == '01210000000AOWp') {
            obj.workflow__c = 1;
        }

        // 請求タイミングセット
            // 納品有無が無なら、納品請求は選択不可
        if (obj.Opportunity_DeliveryExistence__c == '無') {
            obj.BillingTiming__c = '受注';
        }
            // 商品マスタ上の請求基準が「納品のみ」なら必ず納品請求にする
        if (obj.DeliveryStandards__c == '納品のみ') {
            obj.BillingTiming__c = '納品';
        }

        // 自動更新有無チェック
        if (obj.Update__c == '無') {
            obj.AutomaticUpdate__c = '無';
        }

        // 社内発注処理 / 月数 = 0
        if (obj.ProductType__c == '社内発注') {
            obj.ContractMonths__c = 0;
        }

        // 承認ルート用のフラグをコピー
        obj.hasLink__c = obj.hasLinkAsFormula__c;
        obj.hasCRO__c = obj.hasCROAsFormula__c;
        
        // イレギュラー係数=nullの場合、100%にする
        if (obj.IrregularCoefficient__c == null) obj.IrregularCoefficient__c = 100;

        // 評価額・案分粗利を算出
        obj.gross_profit__c = obj.gross_profit_view__c;
    }
}