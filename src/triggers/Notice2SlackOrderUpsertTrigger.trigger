trigger Notice2SlackOrderUpsertTrigger on Order__c (after insert, after update) {
    // 通知メッセージを格納するリスト
    List<String> msgItems = new List<String>();

    // オブジェクトのループ
    for (Order__c obj : Trigger.new) {
        // 配列に追加
        String objectUrl = URL.getSalesforceBaseUrl().toExternalForm();
        objectUrl += '/' + obj.ID;
        
        String msg = '注文情報の更新\n';
        msg += '>>>';
        msg += 'ID：' + '<' + objectUrl + '|' + obj.Id + '>' + '\n';
        msg += '担当者：' + obj.Contract_SalesPerson1__c + '\n';
        msg += '取引先：' + obj.Order_account__c + '\n';
        msgItems.add(msg);
    }
    AnyObjectTriggerHandler.handleAfterUpdate(msgItems);
}