trigger TaskAfterTrigger on Task (after insert, after update) {
    if (Trigger.isInsert) {
        // 商談もしくはリードの最終架電日/最終架電メモを更新する
        TaskInformationSet.prcTelephoneAppointmentInformationUpdate(Trigger.new);
    }
}