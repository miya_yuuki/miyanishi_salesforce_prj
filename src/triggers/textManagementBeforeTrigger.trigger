trigger textManagementBeforeTrigger on TextManagement2__c (before insert, before update) {
    Id UserProfileId = UserInfo.getProfileId();
    System.debug('UserProfile Id' + UserProfileId);

    // レコードタイプが未納品の場合、社外納品日をセット
    for (TextManagement2__c obj : Trigger.new) {
        if (obj.RecordTypeId == '01210000000APip' && obj.ContractMonths__c >= 1) {
            obj.DeliveryDay__c = obj.Item2_StartData__c;
            System.debug('社外納品日セット ' + obj.Item2_StartData__c);
        }
    }

    /* 営業担当の上長を紐づけ */
    // ロールIDの配列
    List<String> strRoleIds = new List<String>();
    // 親ロールIDの配列
    List<String> strParentRoleIds = new List<String>();
    // ロールIDをセット
    for (TextManagement2__c obj : Trigger.new) strRoleIds.add(obj.SalesRoleId__c);
    // ロールIDを取得
    List<UserRole> objRole = [SELECT Id, ParentRoleId FROM UserRole WHERE Id IN :strRoleIds];
    System.debug('#######:' + objRole);
    // ロールのMap
    Map<String, UserRole> mapParent = new Map<String, UserRole>();
    // ロールをMapに追加
    for (UserRole obj : objRole) {
        // Mapに追加
        mapParent.put(obj.Id, obj);
        // 親ロールIDを追加
        strParentRoleIds.add(obj.ParentRoleId);
    }
    System.debug('#######:' + strParentRoleIds);
    // 納品管理の親ID
    for (TextManagement2__c obj : Trigger.new) {
        // 親ロールが存在しない場合
        if (!mapParent.containsKey(obj.SalesRoleId__c)) continue;
        // 親ロールをセット
        obj.ParentRoleId__c = mapParent.get(obj.SalesRoleId__c).ParentRoleId;
    }

    // 営業担当のロールIDを取得
    List<UserRole> objBoss = [
        SELECT Id,
            (
                SELECT Id
                FROM Users
            )
        FROM UserRole
        WHERE Id IN :strParentRoleIds
    ];

    // 上長を取得
    Map<String, UserRole> mapBoss = new Map<String, UserRole>();
    // 上長をMapに追加
    for (UserRole obj : objBoss) mapBoss.put(obj.Id, obj);
    // 上長をセット
    for (TextManagement2__c obj : Trigger.new) {
        // 上長クリア
        obj.Boss1__c = null;
        obj.Boss2__c = null;
        obj.Boss3__c = null;

        // 上長が存在しない場合
        if (!mapBoss.containsKey(obj.ParentRoleId__c)) continue;
        // 上長ユーザのループ
        for (User us : mapBoss.get(obj.ParentRoleId__c).Users) {
            // 上長１
            if (obj.Boss1__c == null) {
                obj.Boss1__c = us.Id;
            // 上長２
            } else if (obj.Boss2__c == null) {
                obj.Boss2__c = us.Id;
            // 上長３
            } else if (obj.Boss3__c == null) {
                obj.Boss3__c = us.Id;
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////ワークフロールール→Trigger乗せ替え分//////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    for (TextManagement2__c obj : Trigger.new) {
        System.debug('##########RecordCreatedCount__c' + obj.RecordCreatedCount__c);
        // 作成時のみ
        if (Trigger.isInsert) {
            // 納品管理作成回数チェック
            if (obj.RecordCreatedCount__c == null) {
                obj.RecordCreatedCount__c = 1;
            }
            else {
                obj.RecordCreatedCount__c ++;
            }

            // 前払いじゃない納品請求の納品有りレコードコピー時は、請求レコード作成済みフラグを下ろす
            if (obj.RecordCreatedCount__c > 1 && obj.Billchek__c == true && obj.BillingTiming__c == '納品' && obj.Order_PaymentSiteMaster__c != '前払い') {
                obj.Billchek__c = false;
            }
            
            // レコードタイプ = 未納品の場合
            if (obj.RecordTypeId == '01210000000APip') {
                if (obj.CloudStatus__c != '前払い入金待ち') {
                    obj.CloudStatus__c = '社外納品済み';
                }
                // 支払期日セット　未納品 && DeliveryDaySet
                if (obj.DeliveryDay__c == null) {
                    if (obj.Item2_StartData__c != null) {
                        // 社外納品日 = 請求対象開始日
                        obj.DeliveryDay__c = obj.Item2_StartData__c;
                    }
                    else {
                        // 社外納品日 = 請求対象日
                        obj.DeliveryDay__c = obj.Bill_Main__c;
                    }
                }
            }
            else {
                // 納品管理コピーで項目クリア / クラウド・システムプロファイル対象外
                if (UserProfileId != '00e5F000000eKAy' && UserProfileId != '00e5F000000eKAZ' && UserProfileId != '00e5F000000FWpC') {
                    obj.DeliveryCount__c = null;
                    obj.GroupID__c = null;
                    obj.DeliveryDay__c = null;
                    obj.CloudBox__c = null;
                    obj.Password__c = null;
                    obj.SalesDeliveryDay__c = null;
                    obj.DeliveryScheduleDay__c = null;
                    obj.Space1__c = null;
                    if (obj.CloudStatus__c != '前払い入金待ち') {
                        obj.CloudStatus__c = 'チェック待ち';
                    }
                }

                // 社外納品予定日　自動セット
                // ソースはどちらも注文商品の納品予定日 ← 元ソースは商談商品の初回納品予定日の月ずらし
                obj.DeliveryDaySales1__c = obj.DeliveryDaySales__c;
            }
            
            // 担当系代入
            obj.SalesPerson10__c = obj.SalesPerson10IdView__c;
            obj.SalesPersonSub__c = obj.SalesPersonSubIDView__c;
            obj.DeliveryPerson__c = obj.DeliveryPersonIDView__c;
            obj.SalesPercentage1__c = obj.SalesPercentage1View__c;
            obj.SalesPercentage2__c = obj.SalesPercentage2View__c;
        }

        // 納品管理作成回数チェック（既存レコード用）
        if (obj.RecordCreatedCount__c == null) obj.RecordCreatedCount__c = 1;

        // 納品金額（参照）　セット
        if (obj.Item2_DeliveryExistence__c == '有') {
            obj.BillminouhinAmountView__c = obj.BillminouhinAmount__c;
        }

        // 請求金額（参照）　セット
        obj.BillAmountView__c = obj.BillAmount__c;

        // 売上高（参照）　セット
        if (obj.recorded_date__c >= date.newInstance(2016, 1, 1) && obj.recorded_date__c <= date.today()) {
            obj.BillAmountPrintView__c = obj.BillAmountPrint__c;
        } else {
            obj.BillAmountPrintView__c = 0;
        }

        // 売上高消費税が null または
        // 売上計上日が null もしくは 今日以降ならば、売上高消費税を計算・更新
        if (obj.billAmountTax__c == null || obj.recorded_date__c == null || Date.today().daysBetween(obj.recorded_date__c) >= 0) {
            // 売上高×消費税率（小数点以下切り捨て）
            obj.billAmountTax__c = (obj.BillAmountPrint__c * obj.taxRate__c / 100.0).round(System.RoundingMode.DOWN);
        }

        // 請求基準/納品有無テキストセット
        obj.Item2_DeliveryStandardsView__c = obj.BillingTiming__c;
        obj.Item2_DeliveryExistenceView__c = obj.Item2_DeliveryExistence__c;

        // 社外納品済み<納品完了数をセット
        if (obj.CloudStatus__c == '社外納品済み') {
            obj.DeliveryCount__c = obj.SalesDeliveryCount__c;
        }

        // SalesforceID自動セット(更新時のみ)
        obj.SalesforceIDSearch__c = obj.SalesforceID__c;

        // サポート担当ユニット自動セット
        if (obj.DeliveryPerson__c != null) {
            obj.DeliveryPersonUnit__c = String.valueOf(obj.DeliveryPersonUnit10__c);
        }

        // 取引先　自動セット
        obj.Account1__c = obj.Account__c;
        
        // 納品予定額代入
        obj.DeliverySummaryCopy__c = obj.DeliverySummary__c;

        // 社内発注/納品数がnullの場合、注文商品から取得した値を代入
        if (obj.SalesDeliveryCount__c == null) {
            obj.SalesDeliveryCount__c = obj.OrderItemQuantity__c;
        }

        // 売上計上日に値が入ったタイミング = 売上計上日が非 null かつ 計上時担当営業が null
        // 計上時担当営業・計上時担当営業ユニットに値を代入する
        if (obj.recorded_date__c != null && obj.RecordedSalesPerson__c == null) {
            obj.RecordedSalesPerson__c = obj.SalesPerson10__c;
            obj.SalesPersonUnit1__c = obj.SalesPerson10team__c;
        }
        
        // 納品請求でなければ請求レコード作成済みフラグを下ろす
        if (obj.BillingTiming__c != '納品') obj.Billchek__c = false;
    }

    // 支払い期日を算出
    PaymentCalculationClass.prcGetDeliveryPaymentDueDate(Trigger.new);
}