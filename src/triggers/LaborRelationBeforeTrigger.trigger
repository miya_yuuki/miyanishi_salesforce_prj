trigger LaborRelationBeforeTrigger on labor_relation__c (before insert, before update) {
	for (labor_relation__c targetObject : Trigger.new) {
		// 「試用期間終了日」= 入社予定日 + 3ヵ月 - 1日
		targetObject.TrialPeriodEndDate__c =  UtilityClass.prcAddDays(UtilityClass.prcAddMonths(targetObject.expected_join_date__c, 3), -1);
	}
}