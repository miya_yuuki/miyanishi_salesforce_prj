trigger ContractBeforeTrigger on Contract (before insert, before update) {
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////ワークフロールール→Trigger乗せ替え分//////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// oldインスタンス
	Contract old = new Contract();

	for (Contract obj: Trigger.new) {
		if (Trigger.IsInsert) {
			// 受注時商談に商談IDを代入
			obj.OriginOpportunity__c = obj.Opportunity__c;
		}

		// 会社名（請求先）自動セット
		obj.Contract_Billaccount1__c = obj.Contract_Billaccount__c;
		obj.Contract_Patnername1__c = obj.Contract_PatnernameView__c;
		obj.Contract_EndClient__c = obj.Contract_EndClientView__c;

		// 契約形態　( null の場合は商談から取得 )
		if (obj.ContractType__c == null && obj.Opportunity_Type__c != null) obj.ContractType__c = obj.Opportunity_Type__c;

		// 取引先Webサイト　( null の場合は商談から取ってくる ）
		if (obj.account_web_site__c == null && obj.account_web_site_IDview__c != null) obj.account_web_site__c = obj.account_web_site_IDview__c;

		// 更新の場合はoldを取得
		if (Trigger.IsUpdate) {
			old = Trigger.oldMap.get(obj.Id);

			// 契約：受注時担当営業ユニット自動挿入
			if ((obj.Contract_FastOrdersSales__c != old.Contract_FastOrdersSales__c && obj.Contract_FastOrdersSales__c != null) || obj.SalesUnit2__c == null) {
				obj.SalesUnit2__c = obj.SalesUnit2View__c;
			}

		}
	}
}