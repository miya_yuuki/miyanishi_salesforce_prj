trigger attachmentAfterTrigger on Attachment (after insert, after update) {
	// 添付ファイルオブジェクトを元に、請求レコードの「請求書発行済み」フラグを更新する
	InvoiceFieldSetClass.prcInvoiceCheckAttachment(Trigger.new);
}