trigger AccountWebSiteAfterTrigger on AccountWebSite__c (after insert, after update) {
	AccountWebSiteDuplicationCheck.prcAccountWebSiteDuplicationCheckClass(Trigger.new,Trigger.oldMap, Trigger.isInsert);
}