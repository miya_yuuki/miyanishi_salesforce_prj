trigger AtkApplyAfterTrigger on teamspirit__AtkApply__c (after insert, after update) {
    // 更新の場合のみ
    if (Trigger.isUpdate) {
        if (ApplyApprovalInformationToAccounts.firstRun) {
            // 与信・反社チェック＆支払いサイトの変更結果を取引先に引き継ぐ        
            ApplyApprovalInformationToAccounts.applyApprovalInformation(Trigger.new, Trigger.oldMap);
            ApplyApprovalInformationToAccounts.firstRun = false;
        }
    }
}