trigger contractAfterTrigger on Contract (after insert, after update) {
	// 契約の担当営業更新に基づいて、注文・注文商品・納品管理の担当営業も変更する
	if (Trigger.isUpdate && ContractOwnerChangeClass.firstRun) {
		ContractOwnerChangeClass.firstRun = false;
		ContractOwnerChangeClass.prcOwnerChange(Trigger.new, Trigger.oldMap);
		ContractOwnerChangeClass.prcSupportOwnerChange(Trigger.new, Trigger.oldMap);
	}
}