trigger opportunityBeforeTrigger on Opportunity (before insert, before update) {
    // 更新元のURLを取得
    String strUrl = '';
    String strHost = '';
    strUrl = URL.getCurrentRequestUrl().getPath();
    strHost = URL.getCurrentRequestUrl().getHost();

    /////////////////////////////////////////////////////////////////////
    // 取引先および取引先責任者のルックアップ                              //
    /////////////////////////////////////////////////////////////////////
    // 
    // 対象の商談
    List<Opportunity> targetOppObjects = new List<Opportunity>();
    // 取引先ID
    List<String> lookUpAccountIds = new List<String>();
    // 取引先オブジェクト
    List<Account> lookUpAccounts = new List<Account>();
    // 取引先オブジェクトMap
    Map<Id, Account> lookUpAccountMap = new Map<Id, Account>();
    // 取引先責任者ID
    List<String> lookUpContactIds = new List<String>();
    // 取引先責任者オブジェクト
    List<Contact> lookUpContacts = new List<Contact>();
    // 取引先責任者Map
    Map<Id, Contact> lookUpContactMap = new Map<Id, Contact>();


    // 項目代入
    for (Opportunity obj : Trigger.new) {
        ////// ルックアップ対応を変更 //////
        if (obj.StageName != '受注' && obj.StageName != 'デッド' && obj.syouninn__c == null && obj.RecordTypeId != '01210000000APYu' || obj.RecordTypeId != '0125F000000a65a') {
            // 取引先ルックアップ対応
            if (obj.AccountId != null) {
                obj.Opportunity_BillPostalCode2__c = obj.Account_BillingPostalCode__c;    // 郵便番号（取引先）
                obj.Opportunity_BillPrefecture2__c = obj.Account_BillingState__c;    // 都道府県(取引先)
                obj.Opportunity_BillCity2__c = obj.Account_Bill_Address__c;    // 市区郡(取引先）
                obj.Opportunity_BillBuilding2__c = obj.Account_BuildingName__c;    // 建物名(取引先)
                obj.Phone__c = obj.Account_Phone__c;    // 電話
                obj.account_BusinessModel__c = obj.Account_BusinessModelview__c;    // ビジネスモデル
                obj.account_Industry__c = obj.Account_Industryview__c;    // 業種（大）
                obj.account_Industrycategory__c = obj.Account_Industrycategoryview__c;    // 業種（小）
                obj.Website__c = obj.Account_Website__c;    // WEBサイト（取引先）
                obj.TransferAccountNumber1__c = obj.Account_kouza2__c;    // 振込口座番号
                obj.account_CreditLevel__c = obj.account_CreditLevelView__c;    // 格付
                obj.CreditSpace__c = obj.Account_CreditSpace__c;    // 与信枠
                obj.CreditBalance__c = obj.CreditBalanceView__c;    // 与信残額
                obj.account_CreditDay__c = obj.LastCreditRatingDay__c;    // 最終格付日
                obj.CompanyCheck__c = obj.Account_CompanyCheck__c;    // 反社チェック
                obj.hansyaMemo__c = obj.Account_hansyaMemo__c;    // 反社チェック備考
                obj.hansyaDay__c = obj.AntisocialCheckDay__c;     // 反社チェック日
            }
            // 取引先責任者ルックアップ対応
            if (obj.Opportunity_Title__c != null) {
                obj.Opportunity_BillKana__c = obj.Contact_kana__c;    // ふりがな（請求先）
                obj.Opportunity_BillDepartment__c = obj.Contact_Department__c;    // 部署（請求先）
                obj.Opportunity_BillTitle__c = obj.Contact_Title__c;    // 役職名（請求先）
                obj.Opportunity_BillRole__c = obj.Contact_Role__c;    // 役割（請求先）
                obj.Opportunity_BillPhone__c = obj.Contact_Phone__c;    // 電話（請求先）
                obj.Opportunity_BillFax__c = obj.Contact_FAX__c;    // FAX（請求先）
                obj.Opportunity_BillEmail__c = obj.Contact_Email__c;    // メール（請求先）
                obj.Opportunity_Billaccount__c = obj.Contact_AcountName__c;    // 取引先（請求先）
                obj.Opportunity_BillPostalCode__c = obj.Contact_MailingPostalCode__c;    // 郵便番号（請求先）
                obj.Opportunity_BillPrefecture__c = obj.Contact_MailingState__c;    // 都道府県(請求先)
                obj.Opportunity_BillCity__c = obj.Contact_MailingCity__c;    // 市区群(請求先）
                obj.Opportunity_BillAddress__c = obj.Contact_MailingStreet__c;    // 町名・番地（請求先）
                obj.Opportunity_BillBuilding__c = obj.Contact_BuildingName__c; // 建物名(請求先)
            }
        }

        ////// サポート契約変更商談に変更元注文の情報を代入 //////
        // 変更元注文から情報を取得 にチェックの場合のみ
        if (obj.RecordTypeId == '0125F000000a65a' && obj.OriginOrder_ImportCheck__c) {
            if (obj.OriginOrder_Account__c != null) obj.AccountId = Id.valueOf(obj.OriginOrder_Account__c);                     // 取引先
            if (obj.OriginOrder_CustomerID__c != null) obj.Opportunity_Title__c = Id.valueOf(obj.OriginOrder_CustomerID__c);    // 取引先責任者
            if (obj.OriginOrder_Support__c != null) obj.Opportunity_Support__c = Id.valueOf(obj.OriginOrder_Support__c);        // サポート担当
            if (obj.OriginOrder_FastOrdersSales__c != null) obj.Opportunity_FastOrdersSales__c = Id.valueOf(obj.OriginOrder_FastOrdersSales__c);              // 受注時担当営業
            if (obj.OriginOrder_PaymentMethod__c != null) obj.Opportunity_PaymentMethod__c = obj.OriginOrder_PaymentMethod__c;                            // 支払い方法
            if (obj.OriginOrder_PaymentSiteMaster__c != null) obj.Opportunity_PaymentSiteMaster__c = Id.valueOf(obj.OriginOrder_PaymentSiteMaster__c);    // 支払サイト
            if (obj.OriginOrder_BillingMethod__c != null) obj.Opportunity_BillingMethod__c = obj.OriginOrder_BillingMethod__c;                            // 請求方法
            if (obj.OriginOrder_Type__c != null) obj.Opportunity_Type__c = obj.OriginOrder_Type__c;                             // 契約形態
            if (obj.OriginOrder_Patnername__c != null) obj.Opportunity_Partner__c = Id.valueOf(obj.OriginOrder_Patnername__c);     // パートナー名
            if (obj.OriginOrder_EndClient1__c != null) obj.Opportunity_endClient1__c = Id.valueOf(obj.OriginOrder_EndClient1__c);  // 施策企業名
            if (obj.OriginOrder_karute__c != null) obj.karute__c = Id.valueOf(obj.OriginOrder_karute__c);                       // ヒアリングシート
            if (obj.OriginOrder_Lead1__c != null) obj.Opportunity_lead1__c = obj.OriginOrder_Lead1__c;                          // リード（大）
            if (obj.OriginOrder_Lead2__c != null) obj.Opportunity_lead2__c = obj.OriginOrder_Lead2__c;                          // リード（小）
            if (obj.OriginOrder_AccountWebSiteID__c != null) obj.account_web_site__c = obj.OriginOrder_AccountWebSiteID__c;     // 取引先Webサイト
        }
    }

    /////////////////////////////////////////////////////////////////////
    // 商談・商談商品の入力規則チェック（商談画面からの更新時のみ対応）//
    /////////////////////////////////////////////////////////////////////
    // Classic + 標準ページ → my.salesforce.com
    // Classic + VFページ → visual.force.com
    // Lightning → lightning.force.com
    //if (strUrl.indexOf('syoudan_edit') > -1 || strUrl.indexOf('syoudan_for_existing_customers') > -1) {
    if(strHost.indexOf('my.salesforce.com') > -1  || strHost.indexOf('visual.force.com') > -1 || strHost.indexOf('lightning.force.com') > -1) {
        // 商談ID
        List<String> strOppIds = new List<String>();
        // 商談オブジェクト
        List<Opportunity> objOpp1 = new List<Opportunity>();
        // 商談オブジェクト
        List<Opportunity> objOpp2 = new List<Opportunity>();
        // 商談オブジェクトのMap
        Map<String, Opportunity> objMap1 = new Map<String, Opportunity>();
        // 商談オブジェクトのMap
        Map<String, Opportunity> objMap2 = new Map<String, Opportunity>();
        // エラーメッセージ
        String strErrMsg = '';

        // 受注フェーズの商談を取得
        for (Opportunity obj : Trigger.new) {
            if (obj.StageName != '受注') continue;
            strOppIds.add(obj.Id);
        }

        // 商談商品チェック1: 契約月数1以上かつ契約開始日
        objOpp1 = [
            SELECT Id,
                (
                    select Id, Name
                    from Opportunityname__r
                    where ContractMonths__c >= 1
                    AND Item_Contract_start_date__c = null
                )
            FROM Opportunity WHERE Id IN :strOppIds
        ];

        // 商談商品チェック2: 各InputCheckに該当
        objOpp2 = [
            SELECT Id,
                (
                    select
                        Id, Name,
                        InputCheck1__c, Err1__c,
                        InputCheck2__c, Err2__c,
                        // 3, 4 は不要になった
                        InputCheck5__c, Err5__c,
                        // 6 は不要になった
                        InputCheck7__c, Err7__c,
                        InputCheck8__c, Err8__c,
                        InputCheck9__c, Err9__c,
                        InputCheck10__c, Err10__c,
                        InputCheck11__c, Err11__c,
                        InputCheck12__c, Err12__c
                    from Opportunityname__r
                    where
                        InputCheck1__c = true or
                        InputCheck2__c = true or
                        // 3, 4 は不要になった
                        InputCheck5__c = true or
                        // 6 は不要になった
                        InputCheck7__c = true or
                        InputCheck8__c = true or
                        InputCheck9__c = true or
                        InputCheck10__c = true or
                        InputCheck11__c = true or
                        InputCheck12__c = true
                )
            FROM Opportunity WHERE Id IN :strOppIds
        ];

        // Mapに追加
        for (Opportunity obj : objOpp1) objMap1.put(obj.Id, obj);
        for (Opportunity obj : objOpp2) objMap2.put(obj.Id, obj);

        // 商談商品チェック1を実施する商談のループ
        for (Opportunity obj : Trigger.new) {
            // Keyのチェック
            if (!objMap1.containsKey(obj.Id)) continue;
            // フェーズチェック
            if (obj.StageName != '受注') continue;
            // エラーメッセージをクリア
            strErrMsg = '';
            // 入力チェック
            for (OpportunityProduct__c itm : objMap1.get(obj.Id).Opportunityname__r) {
                strErrMsg += '　→<a href="/' + itm.Id + '/e" target="_blank">' + itm.Name + '</a>を編集<br/>';
            }
            // エラーメッセージを表示
            if (strErrMsg != '') {
                obj.addError('商談商品の契約開始日を入力して下さい。<br/>' + strErrMsg);
            }
        }

        // 商談商品チェック2を実施する商談のループ
        for (Opportunity obj : Trigger.new) {
            // Keyのチェック
            if (!objMap2.containsKey(obj.Id)) continue;
            // フェーズチェック
            if (obj.StageName != '受注') continue;
            // エラーメッセージをクリア
            strErrMsg = '';
            // 入力チェック
            for (OpportunityProduct__c itm : objMap2.get(obj.Id).Opportunityname__r) {
                if (itm.InputCheck1__c) strErrMsg += itm.Err1__c + '<br/>';
                if (itm.InputCheck2__c) strErrMsg += itm.Err2__c + '<br/>';
                // 3・4 は廃止
                if (itm.InputCheck5__c) strErrMsg += itm.Err5__c + '<br/>';
                // 6 は廃止
                if (itm.InputCheck7__c) strErrMsg += itm.Err7__c + '<br/>';
                if (itm.InputCheck8__c) strErrMsg += itm.Err8__c + '<br/>';
                if (itm.InputCheck9__c) strErrMsg += itm.Err9__c + '<br/>';
                if (itm.InputCheck10__c) strErrMsg += itm.Err10__c + '<br/>';
                if (itm.InputCheck11__c) strErrMsg += itm.Err11__c + '<br/>';
                if (itm.InputCheck12__c) strErrMsg += itm.Err12__c + '<br/>';
            }
            // エラーメッセージを表示
            if (strErrMsg != '') obj.addError(strErrMsg);
        }

        // 商談オブジェクトの入力規則チェック
        for (Opportunity obj : Trigger.new) {
            // エラーメッセージをクリア
            strErrMsg = '';
            // エラーフラグをセット
            Integer ErrMsgFlg1 = 0;
            Integer ErrMsgFlg2 = 0;
            Integer ErrMsgFlg3 = 0;

            // 通常商談かつ担当営業ユニットが「アカウントプランニング」「データ企画」「企画運営」「インサイドセールス」を含む場合の入力規則
            if (Trigger.isUpdate && obj.syouninn__c == null && ((obj.settlement_date__c == null && obj.CreatedDate > date.parse('2018/06/30')) || obj.settlement_date__c >= date.today().addMonths(-3)) && obj.RecordTypeId == '01210000000Zm5n' && obj.SalesUnit__c != null
                && (obj.SalesUnit__c.contains('コンサルティング') || obj.SalesUnit__c.contains('アカウントプランニング') || obj.SalesUnit__c.contains('データ戦略') || obj.SalesUnit__c.contains('企画チーム') || obj.SalesUnit__c.contains('インサイドセールス') || obj.SalesUnit__c.contains('TACT') || obj.SalesUnit__c.contains('事業管理') || obj.SalesUnit__c.contains('ソリューションチーム'))) {
                // ①商談
                // フェーズに依らない入力規則
                if (obj.SQLType__c == '記事SQL' && obj.contents_order_status__c == null) StrErrMsg += '<br /> ①商談・「記事SQL種別」を選択してください<br />'; //記事SQL種別
                if (obj.AppointmentType__c == 'MQL') {
                    if (obj.MQLSalesPerson__c == null) StrErrMsg += '<br /> ①商談・「MQL担当営業」を入力してください<br />'; //MQL担当営業
                    if (obj.MQLDate__c == null) StrErrMsg += '<br /> ①商談・「MQL獲得日」を入力してください<br />'; //MQL獲得日
                    if (obj.MQL_first_visit_date__c == null) StrErrMsg += '<br /> ①商談・「MQL初回訪問日」を入力してください<br />'; //MQL初回訪問日
                }
                if (obj.AppointmentType__c == 'MQL' && obj.LeadEvaluation__c != 'MKT') StrErrMsg += '<br /> ①商談・「獲得時アポ種別」がMQLの場合、「リード評価」はMKTを選択してください<br />'; // リード評価

                // 再アプローチ時以外の入力規則
                if (obj.StageName != '再アプローチ') {
                    if (obj.account_web_site_unnecessary__c == false) {
                        if (obj.account_web_site__c == null) StrErrMsg += '<br /> ①商談・「取引先Webサイト」を入力してください。<br />　→取引先Webサイトを登録しない場合は、「取引先Webサイト対象外」をチェックしてください'; //取引先Webサイト
                        if (obj.WebSiteTypeView__c == null) StrErrMsg += '<br /> ①商談・「サイトタイプ（参照）」を入力してください（取引先Webサイトから入力できます）<br />'; // サイトタイプ（参照）
                        if (obj.business_model_type_view__c == null) StrErrMsg += '<br /> ①商談・「ビジネスモデルタイプ（参照）」を入力してください（取引先Webサイトから入力できます）<br />'; // ビジネスモデルタイプ（参照）
                    }
                    
                    if (obj.CloseDate == null) StrErrMsg += '<br /> ①商談・「完了予定日」を入力してください<br />'; // 完了予定日

                    //保留
                    if (obj.Opportunity_lead1__c == null) StrErrMsg += '<br /> ①商談・「リード（大）」を選択してください<br />'; // リード（大）
                    if (obj.Opportunity_lead2__c == null) StrErrMsg += '<br /> ①商談・「リード（小）」を選択してください<br />'; // リード（小）
                    if (obj.not_campaign_flg__c == true && obj.Opportunity_lead1__c == 'イベント') StrErrMsg += '<br /> ①商談・「リード（大）」がイベントの場合は「主キャンペーン無し」のチェックを外し、「主キャンペーン名」を入力してください<br />';// 主キャンペーン名
                    if (obj.not_campaign_flg__c == false && obj.CampaignId == null) StrErrMsg += '<br /> ①商談・「主キャンペーン名」を入力してください<br />';// 主キャンペーン名
                    if (obj.AppointmentType__c == null) StrErrMsg += '<br /> ①商談・「獲得時アポ種別」を選択してください<br />'; // 獲得時アポ種別
                }

                // 「0.初回日程調整中」 以上の入力規則
                if (obj.StageName.indexOf('MQL') == -1 && obj.StageName != '再アプローチ') {
                    if (obj.SQLType__c == null) StrErrMsg += '<br /> ①商談・「獲得SQL種別」を選択してください<br />'; // 獲得SQL種別
                    if (obj.SQLType__c != null && obj.SQLType__c != 'なし' && obj.AppointmentDay__c == null) StrErrMsg += '<br /> ①商談・「SQL獲得日」を入力してください<br />';//SQL獲得日
                    if ((obj.AppointmentType__c != null && obj.AppointmentType__c != 'MQL') || (obj.SQLType__c != null && obj.SQLType__c != 'なし')) {
                        if (obj.Post_evaluation_summary__c == null) StrErrMsg += '<br /> ①商談・「部署評価（大）」を選択してください<br />';// 部署評価（大）
                        if (obj.Post_evaluation__c == null) StrErrMsg += '<br /> ①商談・「部署評価（小）」を選択してください<br />';// 部署評価（小）
                    }
                    // 部署評価（小）==「AP」|| リード評価 ==「MKT」
                    if ((obj.Post_evaluation__c != null && obj.Post_evaluation__c == 'AP') || (obj.LeadEvaluation__c != null && obj.LeadEvaluation__c == 'MKT')) {
                        if (obj.Opportunity_WEBsiteRank__c == null) StrErrMsg += '<br /> ①商談・「SEO見込み」を選択してください<br />'; // SEO見込み
                        if (obj.SEOMeasureStatus__c == null) StrErrMsg += '<br /> ①商談・「SEO施策状況」を選択してください<br />'; // SEO施策状況
                        if (obj.SEO_contents_budget_per_month__c == null)StrErrMsg += '<br /> ①商談・「SEO・コンテンツ予算/月」を選択してください<br />'; // SEO・コンテンツ予算/月
                        if (obj.Products__c == null) StrErrMsg += '<br /> ①商談・「他社施策中のSEO商材」を選択してください<br />'; // 他社施策中のSEO商材
                        if (obj.writing_resource_summary__c == null) StrErrMsg += '<br /> ①商談・「執筆リソース（大）」を選択してください<br />'; // 執筆リソース（大）
                        if (obj.contents_measure_status__c == null) StrErrMsg += '<br /> ①商談・「執筆リソース（小）」を選択してください<br />'; // 執筆リソース（小）
                    }
                }

                // 「3.提案完了」 以上の入力規則
                if (obj.StageName.indexOf('MQL') == -1 && obj.StageName != '0.初訪日程調整中' && obj.StageName != '1.初回商談日程確定' && obj.StageName != '2.提案実施確定' && obj.StageName != '再アプローチ') {
                    // 提案なしフラグがfalseの場合
                    if (obj.non_recorded_flg__c == false) {
                        if (obj.suggest_date__c == null) StrErrMsg += '<br /> ①商談・「提案日」を入力してください<br />'; // 提案日
                        if (obj.suggest_date__c != null && obj.AppointmentDay__c != null) {
                            if (obj.suggest_date__c <= obj.AppointmentDay__c) StrErrMsg += '<br /> ①商談・「提案日」は「SQL獲得日」より後の日付を入力してください<br />'; // 提案日
                        }
                    }

                    // 部署評価（小）==「AP」
                    if (obj.Post_evaluation__c != null && obj.Post_evaluation__c == 'AP') {
                        if (obj.karuteCheck__c == false && obj.karute__c == null) StrErrMsg += '<br /> ①商談・「ヒアリングシート」を入力してください<br />　→ヒアリングシートを登録しない場合は、「ヒアリングシート登録非対象」をチェックしてください<br />'; //ヒアリングシート
                        if (obj.suggestion_importance_summary__c == null) StrErrMsg += '<br /> ①商談・「提案重要度（大）」を選択してください<br />'; // 提案重要度（大）
                        if (obj.suggestion_importance__c == null) StrErrMsg += '<br /> ①商談・「提案重要度（小）」を選択してください<br />'; // 提案重要度（小）
                        if (obj.review_practitioner__c == null) StrErrMsg += '<br /> ①商談・「レビュー実施者」を選択してください<br />'; // レビュー実施者
                        if (obj.review_status__c == null) StrErrMsg += '<br /> ①商談・「レビューステータス」を選択してください<br />'; // レビューステータス
                        if (obj.document__c == null && obj.non_recorded_flg__c == false) StrErrMsg += '<br /> ①商談・「資料作成種別」を選択してください<br />'; // 資料作成種別
                        // 資料作成種別 ==「営業企画」
                        if (obj.document__c != null && obj.non_recorded_flg__c == false && obj.document__c == '営業企画') {
                            if (obj.document_creating_user__c == null) StrErrMsg += '<br /> ①商談・「資料作成担当者」を入力してください<br />'; // 資料作成担当者
                            if (obj.documents_delivery_date__c == null) StrErrMsg += '<br /> ①商談・「資料納品予定日」を入力してください<br />'; // 資料納品予定日 
                        }
                    }
                }

                // 受注とデッドの場合
                if (obj.StageName == '受注' || obj.StageName == 'デッド') {
                    if (obj.Opportunity_DeadMemo__c == null) StrErrMsg += '<br /> ①商談・「決着理由」を入力してください<br />'; // 決着理由

                    // 部署評価（小）==「AP」or (獲得時アポ種別 == 「MQL」&& SQL獲得日 == null)
                    if ((obj.Post_evaluation__c != null && obj.Post_evaluation__c == 'AP') || (obj.AppointmentType__c != null && obj.AppointmentType__c == 'MQL' && obj.AppointmentDay__c == null)) {
                        if (obj.Budget_acquisition__c == null) StrErrMsg += '<br /> ①商談・「予算獲得状況」を選択してください<br />'; // 予算獲得状況
                        if (obj.Customer_level__c == null) StrErrMsg += '<br /> ①商談・「担当者レベル」を選択してください<br />'; // 担当者レベル
                        if (obj.Customer_needs_level__c == null) StrErrMsg += '<br /> ①商談・「検討レベル」を選択してください<br />'; // 検討レベル
                        if (obj.Introduction_start_timing__c == null) StrErrMsg += '<br /> ①商談・「導入タイミング」を選択してください<br />'; // 導入タイミング
                        if (obj.Settled_type__c == null) StrErrMsg += '<br /> ①商談・「決着要因分類」を選択してください<br />'; // 決着要因分類
                    }  
                }

                // デッドの場合
                if (obj.StageName == 'デッド') {
                    if (obj.Dead_type__c == null) StrErrMsg += '<br /> ①商談・「デッド種別」を選択してください<br />'; //デッド種別
                    // 部署評価（小）==「AP」
                    if (obj.Post_evaluation__c != null && obj.Post_evaluation__c == 'AP') {
                        if (obj.account_web_site_unnecessary__c == false && obj.Opportunity_NoRecord__c == false && obj.Record_view__c == null) {
                            StrErrMsg += '<br /> ①商談・「次回商談設定日（参照）」を入力してください（取引先Webサイトから入力できます）<br />'; //次回商談設定日（参照）
                        }
                    }
                }

                // 受注・デッド「以外」の場合
                if (obj.StageName != 'デッド' && obj.StageName != '受注') {
                    if (obj.CloseDate != null && obj.CloseDate < date.today()) StrErrMsg += '<br /> ①商談・「完了予定日」は本日を含む未来の日付を入力してください<br />'; // 完了予定日
                }
            }

            // 受注承認時の入力規則
            if (obj.StageName == '受注' && obj.syouninn__c == null) {
                // 商談新規作成時は「受注」フェーズを許可しない
                if (Trigger.isInsert) {
                    StrErrMsg += '新規作成時には商談フェーズ「受注」を選択できません　別のフェーズに変更して下さい<br />';
                }
                
                String APUnit = 'アカウントプランニング';
                String MarketingUnit = 'マーケティング';
                String PromotionUnit = 'プロモーション';
                String CSUnit = 'コンサルティング';
                String exceptionCSUnit1 = '（クラウドG）';
                String exceptionCSUnit2 = 'メンバー';
                String LifeMedia = '暮らしニスタ';
                String Media = 'メディア';
                // ①商談
                if (obj.account_web_site_unnecessary__c == false && obj.WebSiteTypeView__c == null && obj.RecordTypeId == '01210000000Zm5n' && (obj.SalesUnit__c.indexOf(APUnit) != -1 || obj.SalesUnit__c.indexOf(MarketingUnit) != -1 || obj.SalesUnit__c.indexOf(PromotionUnit) != -1))  StrErrMsg += '<br />①商談・「サイトタイプ（参照）」が入力されていません<br />　→取引先Webサイトの「サイトタイプ」を選択してください。<br />';
                if (obj.Opportunity_WEBsiteRank__c == null && obj.RecordTypeId == '01210000000Zm5n' && (obj.SalesUnit__c.indexOf(APUnit) != -1 || obj.SalesUnit__c.indexOf(MarketingUnit) != -1 || obj.SalesUnit__c.indexOf(PromotionUnit) != -1))  StrErrMsg += '<br />①商談・「SEO見込み」を選択して下さい<br />';
                // if (obj.ServiceWEBSite__c == null && (obj.RecordTypeId == '01210000000Zm5n' || obj.RecordTypeId == '0125F000000AV1Z')) StrErrMsg += '<br />「サービス提供WEBサイト」を入力して下さい<br />';
                if (obj.Opportunity_Support__c == null && (obj.RecordTypeId == '01210000000Zm5n' || obj.RecordTypeId == '0125F000000AV1Z') && obj.SalesUnit__c.indexOf(CSUnit) != -1 && obj.SalesUnit__c.indexOf(exceptionCSUnit1) == -1 && obj.SalesUnit__c.indexOf(exceptionCSUnit2) == -1)  StrErrMsg += '<br />「サポート担当」を入力して下さい<br />　→サポート担当がいない場合は、自分の名前をセットしてください。<br />';
                if (obj.Opportunity_Type__c == '直販' && obj.Opportunity_endClient1__c != null && (obj.RecordTypeId == '01210000000Zm5n' || obj.RecordTypeId == '0125F000000AV1Z'))  StrErrMsg += '<br />「契約形態」が直販の場合、「施策企業名」の入力は不要です<br />';
                // エクセレントPの場合、施策企業の入力必須
                if (obj.Opportunity_Type__c == 'エクセレントP（原価請求）' && obj.Opportunity_endClient1__c == null && (obj.RecordTypeId == '01210000000Zm5n' || obj.RecordTypeId == '0125F000000AV1Z'))  StrErrMsg += '<br />「契約形態」が「エクセレントP（原価請求）」の場合、「施策企業名」の入力は必須です<br />';
                // セールスPを含む場合、セールスパートナー名の入力必須
                if ((obj.Opportunity_Type__c == 'セールスP（パートナー請求/永続支払）' || obj.Opportunity_Type__c == 'セールスP（クライアント請求/永続支払）' || obj.Opportunity_Type__c == 'セールスP（パートナー請求/初回期間支払）' || obj.Opportunity_Type__c == 'セールスP（クライアント請求/初回期間支払）') && obj.Opportunity_Partner__c == null && (obj.RecordTypeId == '01210000000Zm5n' || obj.RecordTypeId == '0125F000000AV1Z'))  StrErrMsg += '<br />「契約形態」が「セールスP」を含む場合、「セールスパートナー名」の入力は必須です<br />';
                if (obj.CaseApproval__c == null) StrErrMsg += '<br />「事例承諾」を選択して下さい<br />';

                // ②取引先/エクセレントP
                if (obj.account_CreditLevel__c == null && obj.Type == '新規') strErrMsg += '<br />「格付」が入力されていません<br />　→新規取引の場合は、与信/反社申請を提出して下さい。サポートチームが取引先に「格付」をセットします<br />';
                if (obj.account_CreditLevel__c != '無格' && (obj.CreditBalance__c - (obj.MonthlyAmount__c * obj.PaymentTerm__c / 30) - obj.TextAmountTotal__c) < 0) strErrMsg += '<br/>「与信額」がオーバーしています<br/>　→稟議&申請から「与信額超過」の申請を行って下さい<br />';

                // ④受注申請内容
                if (obj.Opportunity_BillingMethod__c == null) {
                    strErrMsg += '<br />「請求方法」を選択して下さい<br/>';
                    ErrMsgFlg1 = 1;
                }
                if (obj.Opportunity_PaymentMethod__c == null) {
                    strErrMsg += '<br />「支払い方法」を選択して下さい<br/>';
                    ErrMsgFlg1 = 1;
                }
                if (obj.Opportunity_PaymentSiteMaster__c == null) {
                    strErrMsg += '<br />「支払サイト」を入力して下さい<br/>';
                    ErrMsgFlg1 = 1;
                }
                if (ErrMsgFlg1 == 1) StrErrMsg += '　→ <a href="/' + obj.AccountId + '/e" target="_blank">' + obj.AccountNameView__c + '</a> も併せて「請求方法」「支払い方法」「支払サイト」の入力状況を確認して下さい<br />';

                // 取引先
                if (obj.Opportunity_BillPrefecture2__c == null) {
                    strErrMsg += '<br />受注フェーズでは取引先の「都道府県」は入力必須です<br/>';
                    ErrMsgFlg2 = 1;
                }
                if (obj.TransferAccountNumber1__c == null || obj.account_Industrycategory__c == null || obj.account_BusinessModel__c == null) {
                    strErrMsg += '<br />受注フェーズでは取引先の「振込口座番号」「業種」「ビジネスモデル」は入力必須です<br/>';
                    ErrMsgFlg2 = 1;
                }
                if (ErrMsgFlg2 == 1) strErrMsg += '　→ <a href="/' + obj.AccountId + '/e" target="_blank">' + obj.AccountNameView__c + '</a> を編集し、商談画面で取引先を再セットして下さい<br />';
                if (obj.LastCreditRatingDay__c == null) {
                    strErrMsg += '<br />取引先の「最終格付日」が入力されていません<br />　→与信/反社申請を提出して下さい。サポートチームが取引先に「格付」をセットします<br />';
                }
                else {
                    Date ExpiredCreditRatingDay = UtilityClass.prcAddDays(obj.LastCreditRatingDay__c, 365);
                    if (ExpiredCreditRatingDay < date.today()) strErrMsg += '<br />取引先の「最終格付日」から1年以上経過しています<br />　→与信/反社申請を提出して下さい。サポートチームが改めて取引先に「格付」をセットします<br />';
                }
                if (obj.AntisocialCheckDay__c == null) {
                    strErrMsg += '<br />取引先の「反社チェック日」が入力されていません<br />　→取引先の反社チェックを実施しますので、与信/反社申請を提出して下さい<br />';
                }
                else {
                    Date ExpiredAntisocialCheckDay = UtilityClass.prcAddDays(obj.AntisocialCheckDay__c, 365);
                    if (ExpiredAntisocialCheckDay < date.today()) strErrMsg += '<br />取引先の「反社チェック日」から1年以上経過しています<br />　→改めて取引先の反社チェックを実施しますので、与信/反社申請を提出して下さい<br />';
                }

                // 取引先責任者
                if (obj.Opportunity_BillPostalCode__c == null || obj.Opportunity_BillPrefecture__c == null || obj.Opportunity_BillCity__c == null || obj.Opportunity_BillAddress__c == null) {
                    strErrMsg += '<br />受注フェーズでは請求先の「郵便番号」「都道府県」「市区群」「町名・番地」は入力必須です<br />';
                    ErrMsgFlg3 = 1;
                }
                if (obj.Contact_LastName__c == null || obj.Opportunity_BillRole__c == null || obj.Opportunity_BillPhone__c == null) {
                    strErrMsg += '<br />受注フェーズでは請求先の「名前」「役割」「電話番号」は入力必須です<br />';
                    ErrMsgFlg3 = 1;
                }
                if (ErrMsgFlg3 == 1) strErrMsg += '　→ <a href="/' + obj.Opportunity_Title__c + '/e" target="_blank">取引先責任者</a> を編集し、商談画面で取引先責任者を再セットして下さい<br />';
            }

            // サポート契約変更の入力規則　　フェーズ = 契約変更申請 で発動
            if (obj.StageName == '契約変更申請' && obj.syouninn__c == null) {
                if (obj.Opportunity_BillingMethod__c == null) strErrMsg += '<br />「請求方法」を選択して下さい<br />';
                if (obj.Opportunity_PaymentMethod__c == null) strErrMsg += '<br />「支払い方法」を選択して下さい<br />';
                if (obj.Opportunity_PaymentSiteMaster__c == null) strErrMsg += '<br />「支払サイト」を入力して下さい<br />';
                if (obj.AccountId == null) strErrMsg += '<br />「取引先」を入力して下さい<br/>';
                if (obj.Opportunity_Title__c == null) strErrMsg += '<br />「取引先責任者（請求先）」を入力して下さい<br />';

                // 契約形態 = 直販
                if (obj.Opportunity_Type__c == '直販' && obj.Opportunity_endClient1__c != null) StrErrMsg += '<br />「契約形態」が直販の場合、「施策企業名」の入力は不要です<br />';
                // 契約形態 = エクセレントP　施策企業名必須
                if (obj.Opportunity_Type__c == 'エクセレントP（原価請求）' && obj.Opportunity_endClient1__c == null) StrErrMsg += '<br />「契約形態」が「エクセレントP（原価請求）」の場合、「施策企業名」の入力は必須です<br />';
                // 契約形態 = セールスP　セールスパートナー名必須
                if ((obj.Opportunity_Type__c == 'セールスP（パートナー請求/永続支払）' || obj.Opportunity_Type__c == 'セールスP（クライアント請求/永続支払）' || obj.Opportunity_Type__c == 'セールスP（パートナー請求/初回期間支払）' || obj.Opportunity_Type__c == 'セールスP（クライアント請求/初回期間支払）') && obj.Opportunity_Partner__c == null) StrErrMsg += '<br />「契約形態」が「セールスP」を含む場合、「セールスパートナー名」の入力は必須です<br />';
            }

            // エラーメッセージを表示
            if (strErrMsg != '') obj.addError('<br/>' + strErrMsg, false);
        }
    }

    /////////////////////////////////////////////
    //// 商談オブジェクト各項目の代入・クリア////
    /////////////////////////////////////////////
    for (Opportunity obj : Trigger.new) {
        // 契約変更商談は下記実施しない
        if (obj.RecordTypeId != '0125F000000a65a') {
            // 支払方法変換（口座振替)
            if (obj.Opportunity_PaymentMethod__c == '口座振替') obj.siharaihouhou__c = '口座振替';

            // 支払方法変換（振り込みWG負担申込書印刷用）
            if (obj.Opportunity_PaymentMethod__c == '振り込み（WG手数料負担）') obj.siharaihouhou__c = '振り込み（ウィルゲート手数料負担）';

            // 支払方法変換（申込書印刷用）
            if (obj.Opportunity_PaymentMethod__c == '振り込み【通常】') obj.siharaihouhou__c = '振り込み';

            // 支払サイトセット（社内発注除く, 商談の支払サイトが空欄の場合は取引先の支払サイトから取得）
            if (obj.syouninn__c == null && obj.Opportunity_PaymentSiteMaster__c == null && obj.Opportunity_PaymentSiteMasterLookupId__c != null && obj.RecordTypeId != '01210000000APYu') obj.Opportunity_PaymentSiteMaster__c = obj.Opportunity_PaymentSiteMasterLookupId__c;

            // 請求方法セット（社内発注除く, 商談の請求方法が空欄の場合は取引先の請求方法から取得）
            if (obj.syouninn__c == null && obj.Opportunity_BillingMethod__c == null && obj.BillingMethodLookUp__c != null && obj.RecordTypeId != '01210000000APYu') obj.Opportunity_BillingMethod__c = obj.BillingMethodLookUp__c;

            // 支払方法セット（社内発注除く, 商談の支払方法が空欄の場合は取引先の支払方法から取得）
            if (obj.syouninn__c == null && obj.Opportunity_PaymentMethod__c == null && obj.PaymentMethodLookUp__c != null && obj.RecordTypeId != '01210000000APYu') obj.Opportunity_PaymentMethod__c = obj.PaymentMethodLookUp__c;

            // 口座振替申込希望　チェックを入れる
            if (obj.Opportunity_Transfer2__c == '申込む') obj.Opportunity_Transfer__c = true;

            // 口座振替申込希望　チェック外す
            if (obj.Opportunity_Transfer2__c == null) obj.Opportunity_Transfer__c = false;

            // 施策企業/パートナー企業名自動挿入
            obj.Partner__c = obj.Opportunity_Partnertext__c;
            obj.sisakukigyo__c = obj.Opportunity_endClient1Name__c;
        }

        // 商談所有者の上長セット
        obj.owner_boss__c = obj.Owner_BossID__c;

        // ルックアップ出来ていない反社チェック日・最終格付日を取引先から代入
        if (obj.hansyaDay__c == null) obj.hansyaDay__c = obj.AntisocialCheckDay__c;
        if (obj.account_CreditDay__c == null) obj.account_CreditDay__c = obj.LastCreditRatingDay__c;

        // 更新時のみ
        if (Trigger.isUpdate) {
            // 受注担当セット　Ver2 （プロセスビルダー乗せ替え）　契約変更商談は除外
            if (obj.Opportunity_FastOrdersSales__c == null && obj.syouninn__c != null && obj.RecordTypeId != '0125F000000a65a') {
                obj.Opportunity_FastOrdersSales__c = obj.OwnerId;
                obj.SalesUnit2__c = String.ValueOf(obj.SalesUnit__c);
            }

            // 与信枠・与信残高更新
            if (obj.StageName != '受注' && obj.StageName != 'デッド' && obj.StageName != '途中解約' && obj.StageName != '契約終了') {
                obj.CreditSpace__c = obj.CreditSpaceView__c;
                obj.CreditBalance__c = obj.CreditBalanceView__c;
            }

            // 既存レコードとしてチェックを入れる
            obj.ExistingOpportunityCheck__c = true;

            // 受注日付・デッド日付の代入
            if (obj.PhaseWon__c == null && obj.StageName == '受注') obj.PhaseWon__c = date.TODAY();
            if (obj.PhaseLost__c == null && obj.StageName == 'デッド') obj.PhaseLost__c = date.TODAY();

            // 入力が不要な項目はnullを代入する
            // 部署評価（小）が「AP」以外の場合
            if (obj.Post_evaluation__c != null && obj.Post_evaluation__c != 'AP') {
                obj.karute__c = null; // ヒアリングシート
                obj.karuteCheck__c = false; // ヒアリングシート登録対象外
                obj.suggestion_importance_summary__c = null; // 提案重要度（大）
                obj.suggestion_importance__c = null; // 提案重要度（小）
                obj.review_practitioner__c = null; // レビュー実施者
                obj.review_status__c = null; // レビューステータス
                obj.document__c = null; // 資料作成種別

                // 獲得時アポ種別 が「MQL」以外、もしくは SQL獲得日 が 空欄でない場合
                if ((obj.AppointmentType__c != null && obj.AppointmentType__c != 'MQL') || obj.AppointmentDay__c != null) {
                    obj.Budget_acquisition__c = null; // 予算獲得状況
                    obj.Customer_level__c = null; // 担当者レベル
                    obj.Customer_needs_level__c = null; // 検討レベル
                    obj.Introduction_start_timing__c = null; // 導入タイミング
                    obj.Settled_type__c = null; // 決着要因分類
                }
            }
            
            // 部署評価（小）が「AP」以外、もしくはデッド種別 が 「競合サービスを導入」 以外の場合
            if ((obj.Post_evaluation__c != null && obj.Post_evaluation__c != 'AP') || (obj.Dead_type__c != null && obj.Dead_type__c != '競合サービスを導入')) {
                obj.Competition_winner__c = null; // 導入先
            }
            
            // 部署評価（小）が「AP」以外、もしくは資料作成種別が「営業企画」以外の場合
            if ((obj.Post_evaluation__c != null && obj.Post_evaluation__c != 'AP') || (obj.document__c == null && obj.document__c != '営業企画')) {
                obj.document_creating_user__c = null; // 資料作成担当者
                obj.documents_delivery_date__c = null; // 資料納品予定日
            }

            // 獲得時アポ種別 が「MQL」以外の場合
            if (obj.AppointmentType__c != null && obj.AppointmentType__c != 'MQL') {
                obj.MQLSalesPerson__c = null; // MQL担当営業
                obj.MQLDate__c = null; // MQL獲得日
                obj.MQL_first_visit_date__c = null; // MQL初回訪問日
            }

            // 獲得時アポ種別が「MQL」で、獲得SQL種別が「なし」の場合
            if (obj.AppointmentType__c == 'MQL' && obj.SQLType__c == 'なし') {
                obj.Post_evaluation_summary__c = null; // 部署評価（大）
                obj.Post_evaluation__c = null; // 部署評価（小）
            }

            // 提案なしフラグがtrueの場合
            if (obj.non_recorded_flg__c == true) {
                obj.suggest_date__c = null; // 提案日
            }

            // リード評価が「MKT」以外の場合
            if (obj.LeadEvaluation__c != null && obj.LeadEvaluation__c != 'MKT') {
                obj.AppointmentSalesPerson__c = null; // アポ獲得者
            }
        }
        
        // 商談フェーズが「受注」「デッド」の場合、決着時担当営業に値を代入
        if (obj.Opportunity_SalesPerson_Unit__c != null && (obj.StageName == 'デッド' || obj.StageName == '受注') && obj.syouninn__c == null) {
          obj.settled_sales_person_unit__c = String.ValueOf(obj.Opportunity_SalesPerson_Unit__c);
        }

        // 新規登録時の代入・クリア
        if (Trigger.isInsert) {
            // Y.約款チェッククリア
            obj.Agreement1__c = false;
            obj.Agreement2__c = false;
            obj.Agreement3__c = false;
            obj.Agreement4__c = false;
            obj.Agreement5__c = false;
            obj.Agreement6__c = false;
            obj.Agreement7__c = false;
            obj.Agreement8__c = false;
            obj.Agreement9__c = false;
            obj.Agreement10__c = false;
            obj.Agreement11__c = false;
            obj.Agreement12__c = false;
            obj.Agreement13__c = false;
            obj.Agreement14__c = false;
            obj.Agreement15__c = false;
            obj.Agreement16__c = false;
            obj.Agreement17__c = false;
            obj.Agreement18__c = false;
            obj.Agreement19__c = false;
            obj.Agreement20__c = false;

            // 商談 フェーズ　リセット
            if (obj.RecordTypeId == '01210000000APYu')  obj.StageName = '社内発注';     // 社内発注
            else if (obj.RecordTypeId == '0125F000000a65a')  obj.StageName = '契約変更前';      // サポート契約変更
            else obj.StageName = '再アプローチ';        // 通常商談

            // 支払サイトセット（社内発注・契約変更を除く, 初回は強制的に取引先の値を取ってくる）
            if (obj.RecordTypeId != '01210000000APYu' && obj.RecordTypeId != '0125F000000a65a') obj.Opportunity_PaymentSiteMaster__c = obj.Opportunity_PaymentSiteMasterLookupId__c;

            // 商談新規作成/コピーでクリア
            obj.syouninn__c = null; // 受注承認日
            obj.PhaseLost__c = null; // フェーズロスト
            obj.PhaseWon__c = null; // フェーズ受注
            obj.settled_sales_person_unit__c = null; // 決着時担当営業
            obj.OrderCreate__c = false; // 注文作成済
            obj.Opportunity_FastOrdersSales__c = null; // 受注時担当営業
            obj.SalesUnit2__c = null; // 受注時担当営業ユニット
            obj.Type = null; // 種別
            obj.CaseApproval__c = null; // 事例承諾
            obj.Opportunity_BillingMethod__c = obj.BillingMethodLookUp__c; // 請求方法
            obj.Opportunity_PaymentMethod__c = obj.PaymentMethodLookUp__c; // 支払い方法
            obj.Opportunity_Memo__c = null; // 申込書備考
            obj.Opportunity_EstimateMemo__c = null; // 見積書備考
            obj.Opportunity_internal_strategy_method__c = null; // 内部対策方法
            obj.QuoteExpirationDate__c = null; // 見積有効期限
            obj.OrderProvisional__c = false; // 仮申込み
            obj.OrderProvisionalMemo__c = null; // 仮申込理由
            obj.AppointmentDay__c = null; // SQL獲得日
            obj.Dead_type__c = null; // デッド種別
            obj.Opportunity_DeadMemo__c = null; // 決着理由
            obj.Opportunity_Support__c = null; // サポート担当
            obj.Opportunity_deadDay__c = null; // 次回連絡日
            obj.NA_memo__c = null; // NAメモ
            obj.LeadEvaluation__c = null; // リード評価
            obj.Opportunity_lead1__c = null; // リード（大）
            obj.Opportunity_lead2__c = null; // リード（小）
            obj.CampaignId = null; // 主キャンペーンソース
            obj.not_campaign_flg__c = false; // 主キャンペーン無し
            obj.Post_evaluation_summary__c = null; // 部署評価（大）
            obj.Post_evaluation__c = null; // 部署評価（小）
            obj.AppointmentType__c = null; // 獲得時アポ種別
            obj.SQLType__c = null; // 獲得SQL種別
            obj.contents_order_status__c = null; // 記事SQL種別
            obj.has_not_appointment_counts__c = false; // アポカウントなし
            obj.sub_sales_person__c = null; // 提案同行者
            obj.recorded_date__c = null; // ヨミ化日
            obj.non_recorded_flg__c = false; // ヨミ化なしフラグ
            obj.ServiceWEBSite__c = null; // WEBサイト
            obj.WEBTitle__c = null; // サイトタイトル
            obj.lastTereapoMemo__c = null; // 最終架電メモ
            obj.ProposalProducts__c = null; // 提案商材
            obj.Opportunity_WEBsiteRank__c = null; // SEO見込み
            obj.SEOMeasureStatus__c = null; // SEO施策状況
            obj.SEO_contents_budget_per_month__c = null; // SEO・コンテンツ予算/月
            obj.Products__c = null; // 他社施策中のSEO商材
            obj.writing_resource_summary__c = null; // 執筆リソース（大）
            obj.contents_measure_status__c = null; // 執筆リソース（小）
            obj.Budget_acquisition__c = null; // 予算獲得状況
            obj.Customer_level__c = null;// 担当者レベル
            obj.Customer_needs_level__c = null; // 検討レベル
            obj.Introduction_start_timing__c = null; // 導入タイミング
            obj.Settled_type__c = null; // 決着要因分類
            obj.Competition_winner__c = null; // 導入先
            //obj.karute__c = null; // ヒアリングシート
            obj.karuteCheck__c = false; // ヒアリングシート登録対象外
            obj.suggestion_importance_summary__c = null; // 提案重要度（大）
            obj.suggestion_importance__c = null; // 提案重要度（小）
            obj.review_practitioner__c = null; // レビュー実施者
            obj.review_status__c = null; // レビューステータス
            obj.document__c = null; // 資料作成種別
            obj.document_creating_user__c = null; // 資料作成担当者
            obj.documents_delivery_date__c = null; // 資料納品予定日
            obj.MQLSalesPerson__c = null; // MQL担当営業
            obj.MQLDate__c = null; // MQL獲得日
            obj.MQL_first_visit_date__c = null; // MQL初回訪問日
            obj.AppointmentSalesPerson__c = null; // アポ獲得者
            obj.Opportunity_NoRecord__c = false; // 次回商談設定日無し
            obj.this_month_approach__c = null; // 当月決着見込み
            obj.agreement_probability__c = null; // 受注確度
            obj.first_visit_date__c = null; // SQL初回訪問日

            // 既存レコードチェック=TRUE時のみ、項目クリア　　2016.09.05 add
            if (obj.ExistingOpportunityCheck__c == true) {
                obj.AppointmentDay__c = null;
                obj.Opportunity_ActionDay__c = null;
                obj.Opportunity_deadDay__c = null;
            }
        }
    }

    // 申込書用の約款テキストをセット
    OpportunityClauseSet.prcOpportunityClauseSet(Trigger.new);
}