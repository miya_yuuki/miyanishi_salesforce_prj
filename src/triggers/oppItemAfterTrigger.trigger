trigger oppItemAfterTrigger on OrderItem__c (after insert, after update) {
	// 注文商品のフェーズがすべて"契約終了"になったら、注文、契約のフェーズをすべて"契約終了"に変更する
	PhaseEndClass.prcSetPhaseEnd(Trigger.new);
	
	// 請求・請求商品を作成
	if (CreateBillRecordFromOrderItem.firstRun) {
		CreateBillRecordFromOrderItem.firstRun = false;
		CreateBillRecordFromOrderItem.prcCreateBill(Trigger.new, Trigger.oldMap, Trigger.isUpdate);
	}

	// 納品管理を自動作成する
	if (CreateDeliveryRecordClass.firstRun) {
		CreateDeliveryRecordClass.firstRun = false;
		CreateDeliveryRecordClass.prcCreateDelivery(Trigger.new);
	}
	
	if (Trigger.isUpdate) {
		// 注文商品の変更を納品管理に反映させる
		UpdateDeliveryRecordClass.prcUpdateDeliveryRecord(Trigger.new, Trigger.oldMap);
		// 請求レコード発行済みの注文商品が途中解約された時、請求商品の無効Flagを上げる
		InvoiceRecordInvalidation.prcInvoiceRecordInvalidation(Trigger.new);
	}
}