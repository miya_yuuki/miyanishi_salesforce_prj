trigger OrderAfterTrigger on Order__c (after insert, after update) {
	// 更新時のみ
	if (Trigger.isUpdate) {
		// 注文レコードが更新されたら、配下にある注文商品を更新する
		OrderItemUpdate.prcOrderItemUpdate(Trigger.new, Trigger.oldMap, Trigger.isUpdate);
	}
	OrderConsultantUpdateClass.prcOrderConsultantUpdateClass(Trigger.new);
}