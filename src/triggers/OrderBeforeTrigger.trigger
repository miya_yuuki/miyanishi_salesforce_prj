trigger OrderBeforeTrigger on Order__c (before insert, before update) {
	// 支払サイト = 前払い に変更された時は、元の支払いサイトに戻す
	for (Order__c obj : Trigger.new) {
		// oldのインスタンス
		Order__c old = new Order__c();
		
		// 更新時のみ
		if (Trigger.IsUpdate) {
			old = Trigger.oldMap.get(obj.Id);
			if (old.Order_PaymentSiteMaster__c != obj.Order_PaymentSiteMaster__c) {
				System.debug('支払サイト更新検知' + obj.Id);
				if (obj.Order_PaymentSiteMasterName__c == '前払い') {
					System.debug('支払サイト 前払い変更検知' + obj.Id);
					// 前払いへの支払サイト変更を禁止し、元の支払サイトに戻す
					obj.Order_PaymentSiteMaster__c = old.Order_PaymentSiteMaster__c;
				}
			}
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////ワークフロールール→Trigger乗せ替え分//////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	for (Order__c obj : Trigger.new) {
		// 支払サイトマスター　セット
		obj.Order_PaymentSiteMaster2__c = obj.Order_PaymentSiteMasterName__c;
		
		// 取引先（請求）　自動セット
		obj.BillAccountName__c = obj.BillAccount__c;
		
		// 取引先名を自動セット
		obj.AccountName__c = obj.Order_account__c;
		obj.Order_Patnername__c = obj.Order_Patnername1__c;
		obj.Order_EndClient__c = obj.Order_EndClient1__c;
	}
}