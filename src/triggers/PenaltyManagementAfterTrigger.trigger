trigger PenaltyManagementAfterTrigger on PenaltyManagement__c (after insert) {

	// 更新用の顧客カルテID集合
	Set<String> penaltyGivenKaruteIds = new Set<String>();	// ペナルティ確認
	Set<String> penaltyRemovedKaruteIds = new Set<String>();	// ペナルティ解除
	Set<String> updateOnlyKaruteIds = new Set<String>();		// CS対応ステータスの更新のみ


	// 一度 Trigger.new から select しなおし
	List<String> targetPenaltyManagementIds = new List<String>();
	for (PenaltyManagement__c penaltyManagement: Trigger.new) {
		targetPenaltyManagementIds.add(penaltyManagement.Id);
	}
	List<PenaltyManagement__c> targetPenaltyManagements = [
		SELECT
			p.Id,
			p.karute__c,
			p.regularStatus__c,
			p.penaltyStatus__c,
			p.date__c,
			p.LastModifiedDate,
			k.hasPenalty__c
		FROM
			PenaltyManagement__c p,
			PenaltyManagement__c.karute__r k
		WHERE
			p.Id IN :targetPenaltyManagementIds
		ORDER BY
			p.date__c,
			p.LastModifiedDate
	];

	// 顧客カルテID→ペナルティ管理オブジェクトのマップ
	// ペナルティ管理オブジェクトは最新のものが選ばれる
	Map<String, PenaltyManagement__c> karuteId2PenaltyManagement = new Map<String, PenaltyManagement__c>();

	for (PenaltyManagement__c penaltyManagement: targetPenaltyManagements) {

		//--------------------------------------------------------------------------------
		// 顧客カルテが重複していた場合、日付昇順なので後勝ちで最新のものが選ばれる
		//--------------------------------------------------------------------------------
		karuteId2PenaltyManagement.put(penaltyManagement.karute__c, penaltyManagement);

		//--------------------------------------------------------------------------------
		// 入力されたステータスの値に応じて、親の顧客カルテの「ペナルティ有り」の値を更新
		//--------------------------------------------------------------------------------
		// 定期ステータスに'③ペナルティ有り' が入力された→ペナルティ確認
		if (
			penaltyManagement.regularStatus__c == '③ペナルティ有り' &&
			penaltyManagement.penaltyStatus__c != '④ペナルティ解除' &&
			! penaltyManagement.karute__r.hasPenalty__c
		) {
			penaltyGivenKaruteIds.add(penaltyManagement.karute__c);

		// 対応ステータスに'④ペナルティ解除' が入力された→ペナルティ解除
		} else if (penaltyManagement.penaltyStatus__c == '④ペナルティ解除' && penaltyManagement.karute__r.hasPenalty__c) {
			penaltyRemovedKaruteIds.add(penaltyManagement.karute__c);

		// それ以外は CS対応ステータスのみ更新
		} else {
			updateOnlyKaruteIds.add(penaltyManagement.karute__c);
		}
	}

	//------------------------------
	// ペナルティが確認された場合
	//------------------------------
	List<karute__c> penaltyGivenKarutes = [
		SELECT
			Id,
			hasPenalty__c,
			penaltyStatusRD__c,
			penaltyStatusCS__c
		FROM
			karute__c
		WHERE
			Id IN :penaltyGivenKaruteIds
	];
	// 「ペナルティ有り」にチェックを入れる
	// R&D対応ステータスを'未対応'に更新
	// CS対応ステータスを更新
	for (karute__c karute: penaltyGivenKarutes) {
		karute.hasPenalty__c = true;
		karute.penaltyStatusRD__c = '未対応';
		karute.penaltyStatusCS__c = karuteId2PenaltyManagement.get(karute.Id).penaltyStatus__c;
	}
	// 対象があれば、更新
	if (penaltyGivenKarutes.size() > 0) {
		update penaltyGivenKarutes;
	}


	//------------------------------
	// ペナルティが解除された場合
	//------------------------------
	List<karute__c> penaltyRemovedKarutes = [
		SELECT
			Id,
			hasPenalty__c,
			penaltyStatusRD__c,
			penaltyStatusCS__c
		FROM
			karute__c
		WHERE Id IN :penaltyRemovedKaruteIds
	];
	// 「ペナルティ有り」のチェックを外す
	// R&D対応ステータスを null に更新
	// CS対応ステータスを更新
	for (karute__c karute: penaltyRemovedKarutes) {
		karute.hasPenalty__c = false;
		karute.penaltyStatusRD__c = null;
		karute.penaltyStatusCS__c = karuteId2PenaltyManagement.get(karute.Id).penaltyStatus__c;
	}
	// 対象があれば、更新
	if (penaltyRemovedKarutes.size() > 0) {
		update penaltyRemovedKarutes;
	}


	//------------------------------
	// それ以外
	//------------------------------
	List<karute__c> updateOnlyKarutes = [
		SELECT
			Id,
			penaltyStatusCS__c
		FROM
			karute__c
		WHERE Id IN :updateOnlyKaruteIds
	];
	// CS対応ステータスの更新のみ
	for (karute__c karute: updateOnlyKarutes) {
		karute.penaltyStatusCS__c = karuteId2PenaltyManagement.get(karute.Id).penaltyStatus__c;
	}
	// 対象があれば、更新
	if (updateOnlyKarutes.size() > 0) {
		update updateOnlyKarutes;
	}
}